package com.star.common.ach;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.hibernate.Session;

import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.CommonFunctions;
import com.star.dao.CheckDAO;
import com.star.dao.ExecCnfgDAO;
import com.star.dao.GetACHDetailsDAO;
import com.star.dao.VendorInfoDAO;
import com.star.linkage.ach.CompanyAddress;
import com.star.linkage.ach.CompanyBankInfo;
import com.star.linkage.ach.InvoiceInfo;
import com.star.linkage.ach.PaymentTotals;
import com.star.linkage.ach.VendorAddress;
import com.star.linkage.ach.VendorBankInfo;
import com.star.linkage.execcnfg.ExecCnfgBrowseOutput;
import com.star.linkage.vendor.VendorInfo;
import com.star.modal.ChkLotUsage;
import com.star.modal.ElecPaySeq;

public class GenerateAPLinkFile {

	String tmpVendorId = ""; /*
								 * REQUIRED FOR CHECK PROCESSING TO ADD ALL THE SINGLE VENDOR PAYMENT IN SINGLE
								 * CHECK NO
								 */
	String tmpChkNo = "";
	int tmpInstrChkId = 0;
	int tmpInstrWirACHId = 0;

	public String getInvoiceDetails(Session session, String proposalId, String bnkCode, String acctNo) throws Exception{

		String fileData="";
		String headerRecord="";
		String detailRecord="";
		
		String outputXML = "";
		double totalPropAmnt = 0.0;
		double totACHAmount = 0.0;
		double totWireAmount = 0.0;
		double totChkAmount = 0.0;
		int iACHCount = 0;
		int iWireCount = 0;
		int iChkCount = 0;
		String cmpyId = "";
		tmpInstrChkId = 0 ;
		tmpInstrWirACHId = 0;

		CommonFunctions functions = new CommonFunctions();
		
		SimpleDateFormat formatDttm = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		SimpleDateFormat formatDt = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat formatYear = new SimpleDateFormat("yyyy");

		Date date = new Date();
		String createdDttm = formatDttm.format(date);
		String createdDt = formatDt.format(date);
		String crtdYear = formatYear.format(date);

		System.out.println(createdDttm);

		GetACHDetailsDAO detailsDAO = new GetACHDetailsDAO();
		VendorInfoDAO vendorInfoDAO= new VendorInfoDAO();
		VendorInfo venInfo=null;
		String venID="";
		String CTPNumber="";
		String paymentDate="";
		List<InvoiceInfo> invVchrList = new ArrayList<InvoiceInfo>();
		
		List<String> venListCount = new ArrayList<String>();
		
		if (proposalId.length() > 0) {
			invVchrList = detailsDAO.getPropInvoices(proposalId);

			Collections.sort(invVchrList, new Comparator<InvoiceInfo>() {
				public int compare(InvoiceInfo s1, InvoiceInfo s2) {
					return s1.getVchrVenId().compareToIgnoreCase(s2.getVchrVenId());
				}
			});
			
			venListCount = getDistinctVendors(invVchrList);
			for (int j = 0; j < venListCount.size(); j++) {				
				//Detailed Record
				String fileDataD="";
				double HnetAmount=0;
				for (int i = 0; i < invVchrList.size(); i++) {
					if (i == 0) {
						cmpyId = invVchrList.get(i).getCmpyId();
					}
					
					if(invVchrList.get(i).getVchrVenId().equalsIgnoreCase(venListCount.get(j)))
					{	
						//if (invVchrList.get(i).getPayMthd().equals("CTX") || invVchrList.get(i).getPayMthd().equals("CCD")
						//	|| invVchrList.get(i).getPayMthd().equals("PPD")) {
						//totACHAmount = totACHAmount + invVchrList.get(i).getVchrAmt() - invVchrList.get(i).getVchrDiscAmt();
						//iACHCount = iACHCount + 1;
						//}
					
					//Detail Record
					//1 Tag 1 AN
					
					detailRecord="D";

					//HnetAmount+=invVchrList.get(i).getVchrAmt()-invVchrList.get(i).getVchrDiscAmt();
					HnetAmount=invVchrList.get(i).getChkNetAmt();
					//2 Reference Number 24 AN LJ SF
					//String refNo="RF123"+i;
					String refNo=invVchrList.get(i).getVchrInvNo();
					refNo=formatValue(refNo,refNo.length(),24,' ','L');
					detailRecord+=refNo;			

					//3 Reference Description 30 AN LJ SF
					String refDesc=invVchrList.get(i).getVchrPfx()+"-"+invVchrList.get(i).getVchrNo();
					refDesc=formatValue(refDesc,refDesc.length(),30,' ','L');
					detailRecord+=refDesc;

					//4 Invoice Date
					SimpleDateFormat fdh = new SimpleDateFormat("yyyyMMdd");			
					//String invoiceDate=fdh.format(invVchrList.get(i).getInvDt().replaceAll("-",""));
					String invoiceDate=invVchrList.get(i).getInvDt().replaceAll("-","");
					detailRecord+=invoiceDate;

					//5 Net Amount 15 N RJ ZF
					double na=invVchrList.get(i).getVchrAmt()-invVchrList.get(i).getVchrDiscAmt();
					String netAmount=Double.toString(HnetAmount);
							//formatValue(Double.toString(na));
					netAmount=formatValue(netAmount,netAmount.length(),15,'0','R');
					detailRecord+=netAmount;
					

					//6 Gross Amount 15 N RJ ZF
					double ga=invVchrList.get(i).getVchrAmt();
					String grossAmount=Double.toString(HnetAmount);
							//formatValue(Double.toString(ga));
					grossAmount=formatValue(grossAmount,grossAmount.length(),15,'0','R');
					detailRecord+=grossAmount;

					//7 Discount Amount 15 N RJ ZF
					double da=invVchrList.get(i).getVchrDiscAmt();
					String discountAmount=formatValue(Double.toString(da));
					discountAmount=formatValue(discountAmount,discountAmount.length(),15,'0','R');
					detailRecord+=discountAmount;

					//8 Reference Information 80 AN LJ SF
					String refInfodh=invVchrList.get(i).getVchrInvNo()+" "+invVchrList.get(i).getVchrPfx()+"-"+invVchrList.get(i).getVchrNo();
					refInfodh=formatValue(refInfodh,refInfodh.length(),80,' ','L');
					detailRecord+=refInfodh;

					//fileout.println(detailRecord);
					venID=invVchrList.get(i).getVchrVenId();
					
					CTPNumber=invVchrList.get(i).getChkNo();
					paymentDate=invVchrList.get(i).getPaymentDt();
					fileDataD+=detailRecord+"\n";
				}						
			}
				//Header Record
				//1
				headerRecord="H";
				
				//2
				String paymentType="E";
				if (invVchrList.get(j).getPayMthd().equals("CTX") || invVchrList.get(j).getPayMthd().equals("CCD")
						|| invVchrList.get(j).getPayMthd().equals("PPD"))
				{
					paymentType="E";
				}
				/*
				else if (invVchrList.get(j).getPayMthd().equals("WIR"))
				{
					paymentType="P";
				}
				else if (invVchrList.get(j).getPayMthd().equals("CHK"))
				{
					paymentType="L";
				}
				*/
				headerRecord+=paymentType;

				//3 N RJ/ZF
				double pa=HnetAmount;
				String paymentAmount=formatValue(Double.toString(pa));
				if (paymentType.matches("(?i)A|B|C|D|P|Q|R|S|V|W")) {
					paymentAmount=formatValue(paymentAmount,paymentAmount.length(),15,'0','R');
				}
				else if (paymentType.matches("(?i)E|F|G|H|I|J|K|L|M|N|O|T|U")) {
					paymentAmount=formatValue(paymentAmount,paymentAmount.length(),15,'0','R');
				}
				else
					paymentAmount=formatValue(paymentAmount,paymentAmount.length(),15,'0','R');
				
				headerRecord+=paymentAmount;

				//4 Payment Date
				//SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd");
				//String paymentDate=f.format(new Date());
				headerRecord+=paymentDate;

				//5 Cheque/Trace/Payment Number
				//String CTPNumber="CTPNumber1234"+j;proposalId
				//String CTPNumber=proposalId+"000"+j;
				//String CTPNumber=invVchrList.get(j).getChkNo();
				
				if (paymentType.matches("(?i)A|B|C|D|V|W")) {
					CTPNumber=formatValue(CTPNumber,CTPNumber.length(),30,' ','L');
				}
				else if (paymentType.matches("(?i)E|F|G|H|I|J|K")) {
					CTPNumber=formatValue(CTPNumber,CTPNumber.length(),30,' ','L');
				}
				else if (paymentType.matches("(?i)P|Q|R")) {
					CTPNumber=formatValue(CTPNumber,CTPNumber.length(),30,' ','L');
				}
				else if (paymentType.matches("(?i)L|M|N|O|S|T|U")) {
					CTPNumber=formatValue(CTPNumber,CTPNumber.length(),30,' ','L');
				}
				else
					CTPNumber=formatValue(CTPNumber,CTPNumber.length(),30,' ','L');
				
				headerRecord+=CTPNumber;

				//6 Reference Information
				//String refInfo="Reference Information 123456"+j;
				String refInfo="";
				if (paymentType.matches("(?i)A|B|C|D|V")) {
					refInfo=formatValue(refInfo,refInfo.length(),30,' ','L');
				}
				else if (paymentType.matches("(?i)L|M|N|T")) {
					refInfo=formatValue(refInfo,refInfo.length(),30,' ','L');
				}
				else
					refInfo=formatValue(refInfo,refInfo.length(),30,' ','L');
				headerRecord+=refInfo;

				//7 Currency (Originator) AN LJ/SF
				//String curO="CAD";
				String curO="";
				curO=formatValue(curO,curO.length(),3,' ','L');
				headerRecord+=curO;

				//8 Source Bank/Branch Number
				//String bankNumber="3";
				//String branchNumber="2";
				String bankNumber="";
				String branchNumber="";
				bankNumber=formatValue(bankNumber,bankNumber.length(),4,'0','R');
				branchNumber=formatValue(branchNumber,branchNumber.length(),5,'0','R');
				headerRecord+=bankNumber+branchNumber;

				//9 Source Bank Account Number
				//String sBankAccNo="123456"+j;
				String sBankAccNo="";
				sBankAccNo=formatValue(sBankAccNo,sBankAccNo.length(),7,'0','R');
				headerRecord+=sBankAccNo;

				//10 Vendor ID 15 AN LJ/SF
				//String venID=invVchrList.get(j).getVchrVenId();
				venInfo=vendorInfoDAO.readVendorInfoESP("",venID);
				venID=formatValue(venID,venID.length(),15,' ','L');
				headerRecord+=venID;

				//11 Vendor Name 60 AN LJ/SF				
				String venNm=venInfo.getVenLongNm();
				if (paymentType.matches("(?i)E|F|G|H|I|J|K|L|M|N|T|P|Q|R|T|V|W")) {
					venNm=formatValue(venNm,venNm.length(),60,' ','L');
				}
				else
					venNm=formatValue(venNm,venNm.length(),60,' ','L');
				headerRecord+=venNm;

				//12 Additional Vendor Name 60 AN LJ/SF
				String venAddNm=venInfo.getVenNm();
				venAddNm=formatValue(venAddNm,venAddNm.length(),60,' ','L');
				headerRecord+=venAddNm;

				//13 Vendor Address Line 1 55 AN LJ/SF
				String venAddLine1=venInfo.getVenAdd1();
				if (paymentType.matches("(?i)S|U")) {
					venAddLine1=formatValue(venAddLine1,venAddLine1.length(),55,' ','L');
				}
				else
					venAddLine1=formatValue(venAddLine1,venAddLine1.length(),55,' ','L');
				headerRecord+=venAddLine1;

				//14 Vendor Address Line 2 OR GSAN (ACH)55 AN LJ/SF
				String venAddLine2=venInfo.getVenAdd2();
				if (paymentType.matches("(?i)E|F|G|H|I|J|K")) {
					venAddLine2=formatValue(venAddLine2,venAddLine2.length(),55,' ','L');
				}
				else if (paymentType.matches("(?i)L|M|N|T|U")) {
					venAddLine2=formatValue(venAddLine2,venAddLine2.length(),55,' ','L');
				}
				else
					venAddLine2=formatValue(venAddLine2,venAddLine2.length(),55,' ','L');
				headerRecord+=venAddLine2;

				//15 Vendor Address Line 3 OR Short Name (ACH) 55 AN LJ/SF
				String venAddLine3=venInfo.getVenAdd3();
				venAddLine3=formatValue(venAddLine3,venAddLine3.length(),55,' ','L');
				headerRecord+=venAddLine3;

				//16 Vendor city 30 AN LJ/SF
				String venCity=venInfo.getVenCity();
				if (paymentType.matches("(?i)P|Q|R")) {
					venCity=formatValue(venCity,venCity.length(),30,' ','L');
				}
				else
					venCity=formatValue(venCity,venCity.length(),30,' ','L');
				headerRecord+=venCity;

				//17 Vendor Province/State 2 AN LJ/SF
				//String venState="BC";
				String venState="";
				venState=formatValue(venState,venState.length(),2,' ','L');
				headerRecord+=venState;

				//18 Vendor Postal/zip code 15 AN LJ/SF
				String venPostCode=venInfo.getVenPostCode();
				venPostCode=formatValue(venPostCode,venPostCode.length(),15,' ','L');
				headerRecord+=venPostCode;

				//19 Vendor country code 2 AN LJ/SF
				//String venCntryCode="CA";
				String venCntryCode="";
				venCntryCode=formatValue(venCntryCode,venCntryCode.length(),2,' ','L');
				headerRecord+=venCntryCode;

				//20 Vendor contact name 35 AN LJ/SF
				String venContNm=venInfo.getVenContName();
				venContNm=formatValue(venContNm,venContNm.length(),35,' ','L');
				headerRecord+=venContNm;

				//21 Vendor Fax number 15 AN LJ/SF
				String venFaxNo=venInfo.getVenFaxNo();
				venFaxNo=formatValue(venFaxNo,venFaxNo.length(),15,' ','L');
				headerRecord+=venFaxNo;	    

				//22 Vendor email 80 AN LJ/SF
				//String venEmail="john.smith@abccompany.com";
				String venEmail="";
				venEmail=formatValue(venEmail,venEmail.length(),80,' ','L');
				headerRecord+=venEmail;

				//23 Remittance language 1 AN E = English, F = French, Blank = bilingual
				String remitLang="";
				if(venInfo.getVenLang().equalsIgnoreCase("en"))
				 remitLang="E";
				else if(venInfo.getVenLang().equalsIgnoreCase("fr"))
					 remitLang="F";
				else
					remitLang=" ";
				remitLang=formatValue(remitLang,remitLang.length(),1,' ','L');
				headerRecord+=remitLang;

				//24 Vendor Bank/Branch Number  9
				//String venBankNumber="3";
				//String venBranchNumber="2";
				String venBankNumber="";
				String venBranchNumber="";
				venBankNumber=formatValue(venBankNumber,venBankNumber.length(),4,'0','R');
				venBranchNumber=formatValue(venBranchNumber,venBranchNumber.length(),5,'0','R');
				headerRecord+=venBankNumber+venBranchNumber;

				//25 Vendor Bank Account Number / IBAN #  35 AN LJ SF
				//String venBankAccNo="123456"+j;
				String venBankAccNo="";
				venBankAccNo=formatValue(venBankAccNo,venBankAccNo.length(),35,' ','L');
				headerRecord+=venBankAccNo;

				//26 Cheque Issuance Delivery Options/  OR ACH PDS/PAD Outside Canada Transaction Code 3 AN RJ SF
				//String CIDOptions="999";
				String CIDOptions="";
				CIDOptions=formatValue(CIDOptions,CIDOptions.length(),3,' ','R');
				headerRecord+=CIDOptions;

				//27 D/A and Payee Match Transaction Code/ OR ACH PDS/PAD Outside Canada Transaction Type 3 AN RJ SF
				//String DAPMTransCode="BUS";
				String DAPMTransCode="";
				DAPMTransCode=formatValue(DAPMTransCode,DAPMTransCode.length(),3,' ','R');
				headerRecord+=DAPMTransCode;

				//28 Currency (Receiver) AN LJ/SF
				//String curR="CAD";
				String curR="";
				curR=formatValue(curR,curR.length(),3,' ','L');
				headerRecord+=curR;

				//29 Currency Conversion Code 1 AN LJ/SF
				//Mandatory for Wire Payments Only (Services = P,Q,R)
				//“1” = No currency conversion required
				//“2” = Currency conversion required
				/*if(curO.equalsIgnoreCase(curR))
					headerRecord+="1";
				else
					headerRecord+="2";*/
				String ccc="";
				ccc=formatValue(ccc,ccc.length(),1,' ','L');
				headerRecord+=ccc;
				
				//30 Special Instruction 1 35 AN LJ/SF
				//String speInst1="Sp inst 1"+j;
				String speInst1="";
				speInst1=formatValue(speInst1,speInst1.length(),35,' ','L');
				headerRecord+=speInst1;

				//31 Special Instruction 2 35 AN LJ/SF
				//String speInst2="Sp inst 2"+j;
				String speInst2="";
				speInst2=formatValue(speInst2,speInst2.length(),35,' ','L');
				headerRecord+=speInst2;

				//32 Special Instruction 3 35 AN LJ/SF
				//String speInst3="Sp inst 3"+j;
				String speInst3="";
				speInst3=formatValue(speInst3,speInst3.length(),35,' ','L');
				headerRecord+=speInst3;

				//33 Vendor Bank Name 35 AN LJ/SF
				//String venBankNm="Vendor Bank";
				String venBankNm="";
				venBankNm=formatValue(venBankNm,venBankNm.length(),35,' ','L');
				headerRecord+=venBankNm;

				//34 Vendor Bank Address Line 1 35 AN LJ/SF
				//String venBankAddLine1="Vendor bank Address Line 1";
				String venBankAddLine1="";
				venBankAddLine1=formatValue(venBankAddLine1,venBankAddLine1.length(),35,' ','L');
				headerRecord+=venBankAddLine1;

				//35 Vendor Bank Address Line 2 35 AN LJ/SF
				//String venBankAddLine2="Vendor bank Address Line 1";
				String venBankAddLine2="";
				venBankAddLine2=formatValue(venBankAddLine2,venBankAddLine2.length(),35,' ','L');
				headerRecord+=venBankAddLine2;

				//36 Vendor Bank city 30 AN LJ/SF
				//String venBankCity="vendor bank City";
				String venBankCity="";
				if (paymentType.matches("(?i)P|Q|R")) {
					venBankCity=formatValue(venBankCity,venBankCity.length(),30,' ','L');
				}
				else
					venBankCity=formatValue(venBankCity,venBankCity.length(),30,' ','L');
				headerRecord+=venBankCity;

				//37 Vendor Bank Province/State 2 AN LJ/SF
				//String venBankState="BC";
				String venBankState="";
				venBankState=formatValue(venBankState,venBankState.length(),2,' ','L');
				headerRecord+=venBankState;

				//38 Vendor bank Postal/zip code 9 AN LJ/SF
				//String venBankPostCode="462042";
				String venBankPostCode="";
				venBankPostCode=formatValue(venBankPostCode,venBankPostCode.length(),9,' ','L');
				headerRecord+=venBankPostCode;

				//39 Vendor bank country code 2 AN LJ/SF
				//String venBankCntryCode="CA";
				String venBankCntryCode="";
				venBankCntryCode=formatValue(venBankCntryCode,venBankCntryCode.length(),2,' ','L');
				headerRecord+=venBankCntryCode;
				//fileout.println(headerRecord);
				fileData+=headerRecord+"\n";
				//totalPropAmnt = totalPropAmnt + invVchrList.get(i).getVchrAmt() - invVchrList.get(i).getVchrDiscAmt();
				totalPropAmnt = totalPropAmnt + totACHAmount;
				fileData+=fileDataD;
			}
			
		}
		return fileData;
	}

	/* CREATE GROUP HEADER FOR PAYMENT */
	public String getGrpHdr(ElecPaySeq elecPaySeq, CompanyAddress address, CompanyBankInfo bankInfo, double totAmount,
			int txnCount, String crtdDttm, int iChkCount) throws Exception {
		
		String grpHdr = "";
		String msgId = String.valueOf(elecPaySeq.getMsgIdSeq());
		String dateStr = crtdDttm;
		String code = "AUTH";
		String prop = "TEST";
		String noOfTxn = String.valueOf(txnCount);
		String totSum = String.valueOf(totAmount);
		String iniParty = address.getCmpyNm();
		String iniPartyId = bankInfo.getRoutingNo();
		
		/* EXEC Config DAO to set Prop TEST or LIVE*/
		ExecCnfgDAO execCnfg = new ExecCnfgDAO();
		BrowseResponse<ExecCnfgBrowseOutput> starBrowseResponse = new BrowseResponse<ExecCnfgBrowseOutput>();
		starBrowseResponse.setOutput(new ExecCnfgBrowseOutput());
		execCnfg.getExecInfo(starBrowseResponse);
		
		if(starBrowseResponse.output.fldTblExec.size() > 0)
		{
			if(starBrowseResponse.output.fldTblExec.get(0).getClasses().contains("LIV"))
			{
				prop = "PROD";
			}
			else
			{
				prop = "TEST";
			}
		}
		
		grpHdr = grpHdr + CommonConstants.GRP_HDR_STRT;
		grpHdr = grpHdr + CommonConstants.MSG_ID_STRT;

		/* ADD MSG ID */ grpHdr = grpHdr + msgId;

		grpHdr = grpHdr + CommonConstants.MSG_ID_END;
		grpHdr = grpHdr + CommonConstants.CRE_DTTM_STRT;

		/* ADD DATE TIME */ grpHdr = grpHdr + dateStr;

		grpHdr = grpHdr + CommonConstants.CRE_DTTM_END;
		grpHdr = grpHdr + CommonConstants.AUTH_STN_STRT;

		if (iChkCount == 0) {
			grpHdr = grpHdr + CommonConstants.CODE_STRT;
			grpHdr = grpHdr + code;
			grpHdr = grpHdr + CommonConstants.CODE_END;
		}

		grpHdr = grpHdr + CommonConstants.PROP_STRT;

		/* ADD PROPRIEATRY */grpHdr = grpHdr + prop;

		grpHdr = grpHdr + CommonConstants.PROP_END;
		grpHdr = grpHdr + CommonConstants.AUTH_STN_END;
		grpHdr = grpHdr + CommonConstants.NO_OF_TXN_STRT;

		/* ADD TOTAL TRANSACTIONS */grpHdr = grpHdr + noOfTxn;

		grpHdr = grpHdr + CommonConstants.NO_OF_TXN_END;
		grpHdr = grpHdr + CommonConstants.CONTROL_SUM_STRT;

		/* ADD TOTAL TRANSACTIONS SUM */grpHdr = grpHdr + formatValue(totSum);

		grpHdr = grpHdr + CommonConstants.CONTROL_SUM_END;
		grpHdr = grpHdr + CommonConstants.INI_PARTY_STRT;
		grpHdr = grpHdr + CommonConstants.NM_STRT;

		/* NAME OF THE COMPANY */grpHdr = grpHdr + iniParty;

		grpHdr = grpHdr + CommonConstants.NM_END;
		grpHdr = grpHdr + CommonConstants.ID_STRT;
		grpHdr = grpHdr + CommonConstants.ORG_ID_STRT;
		grpHdr = grpHdr + CommonConstants.OTHER_STRT;
		grpHdr = grpHdr + CommonConstants.ID_STRT;

		/* ID OF THE COMPANY */grpHdr = grpHdr + iniPartyId;

		grpHdr = grpHdr + CommonConstants.ID_END;
		grpHdr = grpHdr + CommonConstants.OTHER_END;
		grpHdr = grpHdr + CommonConstants.ORG_ID_END;
		grpHdr = grpHdr + CommonConstants.ID_END;
		grpHdr = grpHdr + CommonConstants.INI_PARTY_END;
		grpHdr = grpHdr + CommonConstants.GRP_HDR_END;

		return grpHdr;
	}

	/* CREATE PAYMENT INFORMATION */

	public String getPaymentInfo(Session session, ElecPaySeq elecPaySeq, CompanyAddress address, GetACHDetailsDAO detailsDAO,
			CompanyBankInfo bankInfo, String createdDt, InvoiceInfo info, PaymentTotals paymentTotals) {

		String payInfo = "";
		String paymentId = "";
		String payMthd = "TRF";
		String noOfTxn = "";
		String trnSum = "";
		String svcLvlCode = "";
		String executionDt = createdDt;
		String cmpyNm = address.getCmpyNm().toUpperCase();
		String debAcctId = bankInfo.getAcctNo();
		String cry = "USD";
		String mmbId = bankInfo.getBnkMmbId();
		String cty = "US";
		int iACHFlg = 0;
		int iChkFlg = 0;
		String catPurpose = "VENDOR PAY";

		if (info.getPayMthd().equals("WIR")) {
			svcLvlCode = "URGP";

			trnSum = String.valueOf(paymentTotals.getTotWireAmount());
			noOfTxn = String.valueOf(paymentTotals.getiWireCount());

			paymentId = String.valueOf(elecPaySeq.getPmtIdWir());

			detailsDAO.updateWireSeq(session, elecPaySeq.getPmtIdWir());

		} else if (info.getPayMthd().equals("PPD") || info.getPayMthd().equals("CCD")
				|| info.getPayMthd().equals("CTX")) {
			svcLvlCode = "NURG";

			paymentId = String.valueOf(elecPaySeq.getPmtIdAch());

			trnSum = String.valueOf(paymentTotals.getTotACHAmount());
			noOfTxn = String.valueOf(paymentTotals.getiACHCount());

			iACHFlg = 1;

			detailsDAO.updateACHSeq(session, elecPaySeq.getPmtIdAch());

		} else if (info.getPayMthd().equals("CHK")) {
			trnSum = String.valueOf(paymentTotals.getTotChkAmount());
			noOfTxn = String.valueOf(paymentTotals.getiChkCount());

			paymentId = String.valueOf(elecPaySeq.getPmtIdChk());

			detailsDAO.updateChkSeq(session, elecPaySeq.getPmtIdChk());

			iChkFlg = 1;
		}

		payInfo = payInfo + CommonConstants.PMNT_INF_ID_STRT;
		/* ADD UNIQUE PAYMENT ID */ payInfo = payInfo + paymentId;
		payInfo = payInfo + CommonConstants.PMNT_INF_ID_END;
		payInfo = payInfo + CommonConstants.PMNT_MTHD_STRT;
		/* PAYMENT METHOD - TRF */ payInfo = payInfo + payMthd;
		payInfo = payInfo + CommonConstants.PMNT_MTHD_END;
		payInfo = payInfo + CommonConstants.NO_OF_TXN_STRT;
		/* NO OF TRANSACTIONS */payInfo = payInfo + noOfTxn;
		payInfo = payInfo + CommonConstants.NO_OF_TXN_END;
		payInfo = payInfo + CommonConstants.CONTROL_SUM_STRT;
		/* NO OF TRANSACTIONS SUM */payInfo = payInfo + formatValue(trnSum);
		payInfo = payInfo + CommonConstants.CONTROL_SUM_END;

		payInfo = payInfo + CommonConstants.PMNT_TYP_STRT;

		if (iChkFlg != 1) {
			payInfo = payInfo + CommonConstants.SVC_LVL_STRT;
			payInfo = payInfo + CommonConstants.CODE_STRT;
			/* SERVICE LEVEL CODE BASED ON PAYMENT METHOD */ payInfo = payInfo + svcLvlCode;
			payInfo = payInfo + CommonConstants.CODE_END;
			payInfo = payInfo + CommonConstants.SVC_LVL_END;
		}

		if (iACHFlg == 1) {
			payInfo = payInfo + CommonConstants.LCL_INSTRM_STRT;
			payInfo = payInfo + CommonConstants.CODE_STRT;
			payInfo = payInfo + info.getPayMthd();
			payInfo = payInfo + CommonConstants.CODE_END;
			payInfo = payInfo + CommonConstants.LCL_INSTRM_END;
			payInfo = payInfo + CommonConstants.CTG_PRPS_STRT;
			payInfo = payInfo + CommonConstants.PROP_STRT;
			payInfo = payInfo + catPurpose;
			payInfo = payInfo + CommonConstants.PROP_END;
			payInfo = payInfo + CommonConstants.CTG_PRPS_END;

		}

		if (iChkFlg == 1) {
			payInfo = payInfo + CommonConstants.LCL_INSTRM_STRT;
			payInfo = payInfo + CommonConstants.PROP_STRT;
			payInfo = payInfo + info.getPayMthd();
			payInfo = payInfo + CommonConstants.PROP_END;
			payInfo = payInfo + CommonConstants.LCL_INSTRM_END;
		}

		payInfo = payInfo + CommonConstants.PMNT_TYP_END;
		payInfo = payInfo + CommonConstants.READ_EXEC_DT_STRT;
		/* EXECUTION DATE */ payInfo = payInfo + executionDt;
		payInfo = payInfo + CommonConstants.READ_EXEC_DT_END;

		/* DEBATOR INFORMATION */
		payInfo = payInfo + CommonConstants.DEBTR_STRT;
		payInfo = payInfo + CommonConstants.NM_STRT;
		/* COMPANY NAME */ payInfo = payInfo + cmpyNm;
		payInfo = payInfo + CommonConstants.NM_END;
		payInfo = payInfo + CommonConstants.PSTL_ADDR_STRT;

		/* COMPANY POSTAL ADDRESS */
		if (address.getTownNm().length() > 0) {
			payInfo = payInfo + CommonConstants.STRT_NM_STRT;
			payInfo = payInfo + address.getTownNm();
			payInfo = payInfo + CommonConstants.STRT_NM_END;
		}

		if (address.getPostalCode().length() > 0) {
			payInfo = payInfo + CommonConstants.PSTL_CD_STRT;
			payInfo = payInfo + address.getPostalCode();
			payInfo = payInfo + CommonConstants.PSTL_CD_END;
		}

		if (address.getTownNm().length() > 0) {
			payInfo = payInfo + CommonConstants.TWN_NM_STRT;
			payInfo = payInfo + address.getTownNm().toUpperCase();
			payInfo = payInfo + CommonConstants.TWN_NM_END;
		}

		if (address.getCtrySubDvsn().length() > 0) {
			payInfo = payInfo + CommonConstants.CNTRY_SUB_DVSN_STRT;
			payInfo = payInfo + address.getCtrySubDvsn().toUpperCase();
			payInfo = payInfo + CommonConstants.CNTRY_SUB_DVSN_END;
		}

		if (address.getCty().length() > 0) {
			payInfo = payInfo + CommonConstants.CNTRY_STRT;
			payInfo = payInfo + address.getCty().toUpperCase().substring(0, 2);
			payInfo = payInfo + CommonConstants.CNTRY_END;
		}

		if (address.getAddr().length() > 0) {
			payInfo = payInfo + CommonConstants.ADDR_LINE_STRT;
			payInfo = payInfo + address.getAddr().toUpperCase();
			payInfo = payInfo + CommonConstants.ADDR_LINE_END;
		}

		payInfo = payInfo + CommonConstants.PSTL_ADDR_END;

		/* ADD ORIGIN ID IF PAYMENT TYPE IS ACH */
		if (info.getPayMthd().equals("PPD") || info.getPayMthd().equals("CCD") || info.getPayMthd().equals("CTX")) {
			payInfo = payInfo + CommonConstants.ID_STRT;
			payInfo = payInfo + CommonConstants.ORG_ID_STRT;
			payInfo = payInfo + CommonConstants.OTHER_STRT;
			payInfo = payInfo + CommonConstants.ID_STRT;
			payInfo = payInfo + bankInfo.getBnkAchId();
			payInfo = payInfo + CommonConstants.ID_END;
			payInfo = payInfo + CommonConstants.OTHER_END;
			payInfo = payInfo + CommonConstants.ORG_ID_END;
			payInfo = payInfo + CommonConstants.ID_END;
		}

		payInfo = payInfo + CommonConstants.DEBTR_END;

		/* DEBATOR ACCOUNT */
		payInfo = payInfo + CommonConstants.DBTR_ACCT_STRT;
		payInfo = payInfo + CommonConstants.ID_STRT;
		payInfo = payInfo + CommonConstants.OTHER_STRT;
		payInfo = payInfo + CommonConstants.ID_STRT;
		/* DEBATOR ACCOUNT ID */ payInfo = payInfo + debAcctId;
		payInfo = payInfo + CommonConstants.ID_END;
		payInfo = payInfo + CommonConstants.OTHER_END;
		payInfo = payInfo + CommonConstants.ID_END;

		if (!info.getPayMthd().equals("CHK")) {
			payInfo = payInfo + CommonConstants.CCY_STRT;
			/* CURRENCY INFO */ payInfo = payInfo + cry;
			payInfo = payInfo + CommonConstants.CCY_END;
		}
		payInfo = payInfo + CommonConstants.DBTR_ACCT_END;

		/* DEBATOR AGENT */
		payInfo = payInfo + CommonConstants.DBTR_AGNT_STRT;
		payInfo = payInfo + CommonConstants.FIN_INSTN_ID_STRT;
		payInfo = payInfo + CommonConstants.CLR_SYS_MMB_ID_STRT;
		payInfo = payInfo + CommonConstants.MMB_ID_STRT;
		/* MEMBER ID AGENT */ payInfo = payInfo + mmbId;
		payInfo = payInfo + CommonConstants.MMB_ID_END;
		payInfo = payInfo + CommonConstants.CLR_SYS_MMB_ID_END;
		payInfo = payInfo + CommonConstants.PSTL_ADDR_STRT;
		payInfo = payInfo + CommonConstants.CNTRY_STRT;
		/* COUNTRY */ payInfo = payInfo + cty;
		payInfo = payInfo + CommonConstants.CNTRY_END;
		payInfo = payInfo + CommonConstants.PSTL_ADDR_END;
		payInfo = payInfo + CommonConstants.FIN_INSTN_ID_END;
		payInfo = payInfo + CommonConstants.DBTR_AGNT_END;

		if (iChkFlg == 1) {
			payInfo = payInfo + CommonConstants.DBTR_AGNT_ACT_STRT;
			payInfo = payInfo + CommonConstants.ID_STRT;
			payInfo = payInfo + CommonConstants.OTHER_STRT;
			payInfo = payInfo + CommonConstants.ID_STRT;
			payInfo = payInfo + "PL06694951";
			payInfo = payInfo + CommonConstants.ID_END;
			payInfo = payInfo + CommonConstants.OTHER_END;
			payInfo = payInfo + CommonConstants.ID_END;
			payInfo = payInfo + CommonConstants.DBTR_AGNT_ACT_END;
		}

		return payInfo;
	}

	/* CREATE TRANSACTION INFO BASED ON INVOICES */

	/* GET TRANSACTION DETAILS BY WIRE PAYMENT */
	public String getTransationsInfo(Session session, ElecPaySeq elecPaySeq, String year, GetACHDetailsDAO detailsDAO,
			InvoiceInfo vchrInfo) {

		String trnInfo = "";
		String instrId = "";
		String endToEndId = "";

		elecPaySeq = detailsDAO.getPaySequence();

		if (vchrInfo.getPayMthd().equals("WIR")) {
			
			String trnId = "";
			
			if(tmpInstrWirACHId == 0)
			{
				tmpInstrWirACHId = elecPaySeq.getInstrIdWire();
			}
			
			if (!tmpVendorId.equals(vchrInfo.getVchrVenId()) && tmpVendorId.length() > 0) {
			
				if(tmpInstrWirACHId != 0)
				{
					tmpInstrWirACHId = tmpInstrWirACHId + 1;
				}	
			}
			
			trnId = String.valueOf(tmpInstrWirACHId);
			instrId = "01-" + trnId + year;

			endToEndId = trnId;

			detailsDAO.updateInstrWireSeq(session, tmpInstrWirACHId);
			
			/*
			 * trnId = String.valueOf(elecPaySeq.getInstrIdWire());
			 * 
			 * instrId = "01-" + trnId + year;
			 * 
			 * detailsDAO.updateInstrWireSeq(elecPaySeq.getInstrIdWire());
			 * 
			 * endToEndId = trnId;
			 */

			/*instrId = vchrInfo.getVchrInvNo() + year;
			endToEndId = vchrInfo.getVchrPfx() + "-" + vchrInfo.getVchrNo() + year;*/

		} else if (vchrInfo.getPayMthd().equals("PPD") || vchrInfo.getPayMthd().equals("CCD")
				|| vchrInfo.getPayMthd().equals("CTX")) {
			
			String trnId = "";
			
			if(tmpInstrWirACHId == 0)
			{
				tmpInstrWirACHId = elecPaySeq.getInstrIdWire();
			}
			
			if (!tmpVendorId.equals(vchrInfo.getVchrVenId()) && tmpVendorId.length() > 0) {
			
				if(tmpInstrWirACHId != 0)
				{
					tmpInstrWirACHId = tmpInstrWirACHId + 1;
				}	
			}
			
			trnId = String.valueOf(tmpInstrWirACHId);
			instrId = "11-" + trnId + year;

			endToEndId = "NOTPROVIDED";

			detailsDAO.updateInstrWireSeq(session, tmpInstrWirACHId);
			
			/*
			 * trnId = String.valueOf(elecPaySeq.getInstrIdACH()); instrId = "01-" + trnId +
			 * year;
			 * 
			 * endToEndId = "NOTPROVIDED";
			 * detailsDAO.updateInstrACHSeq(elecPaySeq.getInstrIdACH());
			 */

			/*instrId = vchrInfo.getVchrInvNo() + year;
			endToEndId = vchrInfo.getVchrPfx() + "-" + vchrInfo.getVchrNo() + year;*/

		} else if (vchrInfo.getPayMthd().equals("CHK")) {

			/*
			 * trnId = String.valueOf(elecPaySeq.getInstrIdChk()); instrId = "01-" + trnId +
			 * year;
			 * 
			 * endToEndId = "NOTPROVIDED";
			 * 
			 * detailsDAO.updateInstrChkSeq(elecPaySeq.getInstrIdChk());
			 */

			instrId = vchrInfo.getVchrInvNo() + year;
			endToEndId = vchrInfo.getVchrPfx() + "-" + vchrInfo.getVchrNo() + year;
		}

		VendorAddress vendorAddress = detailsDAO.getVendorAddress(vchrInfo.getCmpyId(), vchrInfo.getVchrVenId());

		VendorBankInfo bankInfo = detailsDAO.getBankInfoByVendor(vchrInfo.getCmpyId(), vchrInfo.getVchrVenId());

		trnInfo = trnInfo + CommonConstants.CREDIT_TRF_TRN_STRT;
		trnInfo = trnInfo + CommonConstants.PMT_ID_STRT;
		trnInfo = trnInfo + CommonConstants.INSTR_ID_STRT;
		/* INSTR ID */trnInfo = trnInfo + instrId;
		trnInfo = trnInfo + CommonConstants.INSTR_ID_END;
		trnInfo = trnInfo + CommonConstants.END_TO_END_ID_STRT;
		/* END TO END ID */trnInfo = trnInfo + endToEndId;
		trnInfo = trnInfo + CommonConstants.END_TO_END_ID_END;
		trnInfo = trnInfo + CommonConstants.PMT_ID_END;
		trnInfo = trnInfo + CommonConstants.AMT_STRT;

		trnInfo = trnInfo + (CommonConstants.INSTRUC_AMT_STRT).replace(">", " Ccy=\"" + vchrInfo.getVchrCry() + "\">");
		double vchrFinalAmt = vchrInfo.getVchrAmt() - vchrInfo.getVchrDiscAmt();
		trnInfo = trnInfo + formatValue(String.valueOf(vchrFinalAmt));
		trnInfo = trnInfo + CommonConstants.INSTRUC_AMT_END;

		trnInfo = trnInfo + CommonConstants.AMT_END;

		if (vchrInfo.getPayMthd().equals("WIR") || vchrInfo.getPayMthd().equals("PPD")
				|| vchrInfo.getPayMthd().equals("CCD") || vchrInfo.getPayMthd().equals("CTX")) {
			trnInfo = trnInfo + CommonConstants.CRED_AGNT_STRT;
			trnInfo = trnInfo + CommonConstants.FIN_INSTN_ID_STRT;
			trnInfo = trnInfo + CommonConstants.CLR_SYS_MMB_ID_STRT;
			trnInfo = trnInfo + CommonConstants.CLEARING_SYS_ID_STRT;
			trnInfo = trnInfo + CommonConstants.CODE_STRT;
			/* CODE VALUE */trnInfo = trnInfo + "USABA";
			trnInfo = trnInfo + CommonConstants.CODE_END;
			trnInfo = trnInfo + CommonConstants.CLEARING_SYS_ID_END;
			trnInfo = trnInfo + CommonConstants.MMB_ID_STRT;
			trnInfo = trnInfo + bankInfo.getBnkRoutingNo();
			trnInfo = trnInfo + CommonConstants.MMB_ID_END;
			trnInfo = trnInfo + CommonConstants.CLR_SYS_MMB_ID_END;
			trnInfo = trnInfo + CommonConstants.NM_STRT;
			/* VENDOR BANK NAME */trnInfo = trnInfo + bankInfo.getBnkNm();
			trnInfo = trnInfo + CommonConstants.NM_END;
			trnInfo = trnInfo + CommonConstants.PSTL_ADDR_STRT;
			trnInfo = trnInfo + CommonConstants.CNTRY_STRT;
			/* COUNTRY CODE */trnInfo = trnInfo + "US";
			trnInfo = trnInfo + CommonConstants.CNTRY_END;
			trnInfo = trnInfo + CommonConstants.PSTL_ADDR_END;
			trnInfo = trnInfo + CommonConstants.FIN_INSTN_ID_END;
			trnInfo = trnInfo + CommonConstants.CRED_AGNT_END;
		} else if (vchrInfo.getPayMthd().equals("CHK")) {

			trnInfo = trnInfo + CommonConstants.CHK_INSTR_STRT;
			trnInfo = trnInfo + CommonConstants.CHK_NB_STRT;
			trnInfo = trnInfo + CommonConstants.CHK_NB_END;
			trnInfo = trnInfo + CommonConstants.DLVRY_MTHD_STRT;
			trnInfo = trnInfo + CommonConstants.PROP_STRT;
			trnInfo = trnInfo + "001";
			trnInfo = trnInfo + CommonConstants.PROP_END;
			trnInfo = trnInfo + CommonConstants.DLVRY_MTHD_END;
			trnInfo = trnInfo + CommonConstants.CHK_INSTR_END;

		}

		trnInfo = trnInfo + CommonConstants.CREDITOR_STRT;
		trnInfo = trnInfo + CommonConstants.NM_STRT;
		/* VENDOR NAME */trnInfo = trnInfo + vendorAddress.getVendorNm().toUpperCase();
		trnInfo = trnInfo + CommonConstants.NM_END;

		/* POSTAl ADDRESS */
		trnInfo = trnInfo + CommonConstants.PSTL_ADDR_STRT;

		if (vendorAddress.getPostalCode().length() > 0) {
			trnInfo = trnInfo + CommonConstants.PSTL_CD_STRT;
			trnInfo = trnInfo + vendorAddress.getPostalCode();
			trnInfo = trnInfo + CommonConstants.PSTL_CD_END;
		}

		if (vendorAddress.getTownNm().length() > 0) {
			trnInfo = trnInfo + CommonConstants.TWN_NM_STRT;
			trnInfo = trnInfo + vendorAddress.getTownNm().toUpperCase();
			trnInfo = trnInfo + CommonConstants.TWN_NM_END;
		}

		if (vendorAddress.getCtrySubDvsn().length() > 0) {
			trnInfo = trnInfo + CommonConstants.CNTRY_SUB_DVSN_STRT;
			trnInfo = trnInfo + vendorAddress.getCtrySubDvsn().toUpperCase();
			trnInfo = trnInfo + CommonConstants.CNTRY_SUB_DVSN_END;
		}

		if (vendorAddress.getCty().length() > 0) {
			trnInfo = trnInfo + CommonConstants.CNTRY_STRT;
			trnInfo = trnInfo + vendorAddress.getCty().toUpperCase().substring(0, 2);
			trnInfo = trnInfo + CommonConstants.CNTRY_END;
		}

		if (vendorAddress.getAddr().length() > 0) {
			trnInfo = trnInfo + CommonConstants.ADDR_LINE_STRT;
			trnInfo = trnInfo + vendorAddress.getAddr().toUpperCase();
			trnInfo = trnInfo + CommonConstants.ADDR_LINE_END;
		}

		trnInfo = trnInfo + CommonConstants.PSTL_ADDR_END;

		trnInfo = trnInfo + CommonConstants.CREDITOR_END;
		trnInfo = trnInfo + CommonConstants.CREDITOR_ACCT_STRT;
		trnInfo = trnInfo + CommonConstants.ID_STRT;
		trnInfo = trnInfo + CommonConstants.OTHER_STRT;
		trnInfo = trnInfo + CommonConstants.ID_STRT;
		/* VENDOR ID USED IN BANK */trnInfo = trnInfo + bankInfo.getBnkAcctNo();
		trnInfo = trnInfo + CommonConstants.ID_END;
		trnInfo = trnInfo + CommonConstants.OTHER_END;
		trnInfo = trnInfo + CommonConstants.ID_END;

		if (vchrInfo.getPayMthd().equals("PPD") || vchrInfo.getPayMthd().equals("CCD")
				|| vchrInfo.getPayMthd().equals("CTX")) {

			trnInfo = trnInfo + CommonConstants.TP_STRT;
			trnInfo = trnInfo + CommonConstants.PROP_STRT;
			trnInfo = trnInfo + "22";
			trnInfo = trnInfo + CommonConstants.PROP_END;
			trnInfo = trnInfo + CommonConstants.TP_END;
		}

		trnInfo = trnInfo + CommonConstants.CREDITOR_ACCT_END;

		trnInfo = trnInfo + CommonConstants.RMT_INF_STRT;

		if (vchrInfo.getPayMthd().equals("WIR") || vchrInfo.getPayMthd().equals("PPD")
				|| vchrInfo.getPayMthd().equals("CCD") || vchrInfo.getPayMthd().equals("CTX")) {

			trnInfo = trnInfo + CommonConstants.UNSTRUCTURED_STRT;
			/* REFERENCE INFO */ 
			/*trnInfo = trnInfo + vchrInfo.getVchrPfx() + "-" + vchrInfo.getVchrNo() + "/" + vchrInfo.getVchrInvNo() + "/" + vchrInfo.getNotesToPayee() + "/";*/
			
			trnInfo = trnInfo + vchrInfo.getVchrNo();
			
			trnInfo = trnInfo + CommonConstants.UNSTRUCTURED_END;
		} else {
			trnInfo = trnInfo + CommonConstants.STRD_STRT;

			trnInfo = trnInfo + CommonConstants.REF_DOC_INFO_STRT;

			trnInfo = trnInfo + CommonConstants.TP_STRT;

			trnInfo = trnInfo + CommonConstants.CD_PRTRY_STRT;

			trnInfo = trnInfo + CommonConstants.CODE_STRT;

			trnInfo = trnInfo + "CINV"; /* FIXED FOR CHK PAYMENT TYPE */

			trnInfo = trnInfo + CommonConstants.CODE_END;

			trnInfo = trnInfo + CommonConstants.CD_PRTRY_END;

			trnInfo = trnInfo + CommonConstants.TP_END;

			trnInfo = trnInfo + CommonConstants.NB_STRT;

			trnInfo = trnInfo + instrId;

			trnInfo = trnInfo + CommonConstants.NB_END;

			trnInfo = trnInfo + CommonConstants.RLTD_DT_STRT;
			trnInfo = trnInfo + CommonConstants.RLTD_DT_END;

			trnInfo = trnInfo + CommonConstants.REF_DOC_INFO_END;

			trnInfo = trnInfo + CommonConstants.REF_DOC_AMT_STRT;

			trnInfo = trnInfo
					+ (CommonConstants.DUE_PAY_AMT_STRT).replace(">", " Ccy=\"" + vchrInfo.getVchrCry() + "\">");

			trnInfo = trnInfo + formatValue(String.valueOf(vchrInfo.getVchrAmt()));

			trnInfo = trnInfo + CommonConstants.DUE_PAY_AMT_END;

			trnInfo = trnInfo
					+ (CommonConstants.DSCNT_APLD_AMT_STRT).replace(">", " Ccy=\"" + vchrInfo.getVchrCry() + "\">");

			trnInfo = trnInfo + formatValue(String.valueOf(vchrInfo.getVchrDiscAmt()));

			trnInfo = trnInfo + CommonConstants.DSCNT_APLD_AMT_END;

			trnInfo = trnInfo + (CommonConstants.RMTD_AMT_STRT).replace(">", " Ccy=\"" + vchrInfo.getVchrCry() + "\">");

			double rmtAmt = vchrInfo.getVchrAmt() - vchrInfo.getVchrDiscAmt();

			trnInfo = trnInfo + formatValue(String.valueOf(rmtAmt));

			trnInfo = trnInfo + CommonConstants.RMTD_AMT_END;

			trnInfo = trnInfo + CommonConstants.REF_DOC_AMT_END;

			trnInfo = trnInfo + CommonConstants.STRD_END;
		}
		trnInfo = trnInfo + CommonConstants.RMT_INF_END;

		trnInfo = trnInfo + CommonConstants.CREDIT_TRF_TRN_END;
		
		tmpVendorId = vchrInfo.getVchrVenId();
		
		return trnInfo;
	}

	/* GET TRANSACTION DETAILS BY CHECK */
	public String getTransationsInfoChk(Session session, ElecPaySeq elecPaySeq, String year, GetACHDetailsDAO detailsDAO,
			InvoiceInfo vchrInfo, CompanyBankInfo cmpybankInfo, Map<String, Double> vendTotalMap) throws Exception{

		String trnInfo = "";
		String instrId = "";
		String endToEndId = "";
		CommonFunctions commonFunctions = new CommonFunctions();

		elecPaySeq = detailsDAO.getPaySequence();

		if (vchrInfo.getPayMthd().equals("CHK")) {

			String trnId = "";
			
			/* CHANGE FOR UNIQUE INSTR NUMBER FOR EACH TRANSACTION */
			if(tmpInstrChkId == 0)
			{
				tmpInstrChkId = elecPaySeq.getInstrIdChk();
			}
			
			if (!tmpVendorId.equals(vchrInfo.getVchrVenId()) && tmpVendorId.length() > 0) {
			
				if(tmpInstrChkId != 0)
				{
					tmpInstrChkId = tmpInstrChkId + 1;
				}	
			}
			
			trnId = String.valueOf(tmpInstrChkId);
			instrId = "01-" + trnId + year;

			endToEndId = "NOTPROVIDED";

			detailsDAO.updateInstrChkSeq(session, tmpInstrChkId);

			/*
			 * instrId = vchrInfo.getVchrInvNo() + year; endToEndId = vchrInfo.getVchrPfx()
			 * + "-" + vchrInfo.getVchrNo() + year;
			 */
		}

		VendorAddress vendorAddress = detailsDAO.getVendorAddress(vchrInfo.getCmpyId(), vchrInfo.getVchrVenId());

		VendorBankInfo bankInfo = detailsDAO.getBankInfoByVendor(vchrInfo.getCmpyId(), vchrInfo.getVchrVenId());

		if (!tmpVendorId.equals(vchrInfo.getVchrVenId()) && tmpVendorId.length() > 0) {
			trnInfo = trnInfo + CommonConstants.RMT_INF_END;

			trnInfo = trnInfo + CommonConstants.CREDIT_TRF_TRN_END;
		}

		CheckDAO checkDAO = new CheckDAO();
		ChkLotUsage chkLotUsage = new ChkLotUsage();

		if (!tmpVendorId.equals(vchrInfo.getVchrVenId())) {
			trnInfo = trnInfo + CommonConstants.CREDIT_TRF_TRN_STRT;
			trnInfo = trnInfo + CommonConstants.PMT_ID_STRT;
			trnInfo = trnInfo + CommonConstants.INSTR_ID_STRT;
			/* INSTR ID */trnInfo = trnInfo + instrId;
			trnInfo = trnInfo + CommonConstants.INSTR_ID_END;
			trnInfo = trnInfo + CommonConstants.END_TO_END_ID_STRT;
			/* END TO END ID */trnInfo = trnInfo + endToEndId;
			trnInfo = trnInfo + CommonConstants.END_TO_END_ID_END;
			trnInfo = trnInfo + CommonConstants.PMT_ID_END;
			trnInfo = trnInfo + CommonConstants.AMT_STRT;

			trnInfo = trnInfo
					+ (CommonConstants.INSTRUC_AMT_STRT).replace(">", " Ccy=\"" + vchrInfo.getVchrCry() + "\">");
			double vchrFinalAmt = vchrInfo.getVchrAmt() - vchrInfo.getVchrDiscAmt();
			
			trnInfo = trnInfo + formatValue(String.valueOf(vendTotalMap.get(vchrInfo.getVchrVenId())));
			
		//	trnInfo = trnInfo + formatValue(String.valueOf(vchrFinalAmt));
			trnInfo = trnInfo + CommonConstants.INSTRUC_AMT_END;

			trnInfo = trnInfo + CommonConstants.AMT_END;

			trnInfo = trnInfo + CommonConstants.CHK_INSTR_STRT;
			trnInfo = trnInfo + CommonConstants.CHK_NB_STRT;

			chkLotUsage = checkDAO.getCurrentCheckNo(cmpybankInfo.getBnkCode(), cmpybankInfo.getAcctNo());

			/* SEET TEMPRORY CHECK NO VARIABLE TO UPDATE VOUCHER REFERNCE */
			tmpChkNo = chkLotUsage.getCurChk();
			
			/* UPDATE REFERENCE CHECK NUMBER FOR INVOICE */
			checkDAO.updCheckForInvoice(session, vchrInfo.getVchrPfx() + "-" + vchrInfo.getVchrNo(), chkLotUsage.getCurChk());
			
			trnInfo = trnInfo + tmpChkNo; /* CHECK NUMBER */
			
			nextChkNo(session, cmpybankInfo, chkLotUsage);
			
			trnInfo = trnInfo + CommonConstants.CHK_NB_END;
			trnInfo = trnInfo + CommonConstants.DLVRY_MTHD_STRT;
			trnInfo = trnInfo + CommonConstants.PROP_STRT;
			trnInfo = trnInfo + "001";
			trnInfo = trnInfo + CommonConstants.PROP_END;
			trnInfo = trnInfo + CommonConstants.DLVRY_MTHD_END;
			trnInfo = trnInfo + CommonConstants.CHK_INSTR_END;

			trnInfo = trnInfo + CommonConstants.CREDITOR_STRT;
			trnInfo = trnInfo + CommonConstants.NM_STRT;
			/* VENDOR NAME */trnInfo = trnInfo + vendorAddress.getVendorNm().toUpperCase();
			trnInfo = trnInfo + CommonConstants.NM_END;

			/* POSTAl ADDRESS */
			trnInfo = trnInfo + CommonConstants.PSTL_ADDR_STRT;

			if (vendorAddress.getPostalCode().length() > 0) {
				trnInfo = trnInfo + CommonConstants.PSTL_CD_STRT;
				trnInfo = trnInfo + vendorAddress.getPostalCode();
				trnInfo = trnInfo + CommonConstants.PSTL_CD_END;
			}

			if (vendorAddress.getTownNm().length() > 0) {
				trnInfo = trnInfo + CommonConstants.TWN_NM_STRT;
				trnInfo = trnInfo + vendorAddress.getTownNm().toUpperCase();
				trnInfo = trnInfo + CommonConstants.TWN_NM_END;
			}

			if (vendorAddress.getCtrySubDvsn().length() > 0) {
				trnInfo = trnInfo + CommonConstants.CNTRY_SUB_DVSN_STRT;
				trnInfo = trnInfo + vendorAddress.getCtrySubDvsn().toUpperCase();
				trnInfo = trnInfo + CommonConstants.CNTRY_SUB_DVSN_END;
			}

			if (vendorAddress.getCty().length() > 0) {
				trnInfo = trnInfo + CommonConstants.CNTRY_STRT;
				trnInfo = trnInfo + vendorAddress.getCty().toUpperCase().substring(0, 2);
				trnInfo = trnInfo + CommonConstants.CNTRY_END;
			}

			if (vendorAddress.getAddr().length() > 0) {
				trnInfo = trnInfo + CommonConstants.ADDR_LINE_STRT;
				trnInfo = trnInfo + vendorAddress.getAddr().toUpperCase();
				trnInfo = trnInfo + CommonConstants.ADDR_LINE_END;
			}

			trnInfo = trnInfo + CommonConstants.PSTL_ADDR_END;

			trnInfo = trnInfo + CommonConstants.CREDITOR_END;
			trnInfo = trnInfo + CommonConstants.CREDITOR_ACCT_STRT;
			trnInfo = trnInfo + CommonConstants.ID_STRT;
			trnInfo = trnInfo + CommonConstants.OTHER_STRT;
			trnInfo = trnInfo + CommonConstants.ID_STRT;
			/* VENDOR ID USED IN BANK */trnInfo = trnInfo + vchrInfo.getVchrVenId();
			trnInfo = trnInfo + CommonConstants.ID_END;
			trnInfo = trnInfo + CommonConstants.OTHER_END;
			trnInfo = trnInfo + CommonConstants.ID_END;

			trnInfo = trnInfo + CommonConstants.CREDITOR_ACCT_END;

			trnInfo = trnInfo + CommonConstants.RMT_INF_STRT;
			trnInfo = trnInfo + CommonConstants.STRD_STRT;
			trnInfo = trnInfo + CommonConstants.REF_DOC_INFO_STRT;
			trnInfo = trnInfo + CommonConstants.TP_STRT;
			trnInfo = trnInfo + CommonConstants.CD_PRTRY_STRT;
			trnInfo = trnInfo + CommonConstants.CODE_STRT;
			trnInfo = trnInfo + "CINV"; /* FIXED FOR CHK PAYMENT TYPE */
			trnInfo = trnInfo + CommonConstants.CODE_END;
			trnInfo = trnInfo + CommonConstants.CD_PRTRY_END;
			trnInfo = trnInfo + CommonConstants.TP_END;
			trnInfo = trnInfo + CommonConstants.NB_STRT;
			
			String temp = commonFunctions.whiteEscapeCharacter(vchrInfo.getVchrInvNo());
			
			trnInfo = trnInfo + temp + year;
			trnInfo = trnInfo + CommonConstants.NB_END;
			trnInfo = trnInfo + CommonConstants.RLTD_DT_STRT;
			trnInfo = trnInfo + vchrInfo.getInvDt();
			trnInfo = trnInfo + CommonConstants.RLTD_DT_END;
			trnInfo = trnInfo + CommonConstants.REF_DOC_INFO_END;
			trnInfo = trnInfo + CommonConstants.REF_DOC_AMT_STRT;
			trnInfo = trnInfo
					+ (CommonConstants.DUE_PAY_AMT_STRT).replace(">", " Ccy=\"" + vchrInfo.getVchrCry() + "\">");
			trnInfo = trnInfo + formatValue(String.valueOf(vchrInfo.getVchrAmt()));
			trnInfo = trnInfo + CommonConstants.DUE_PAY_AMT_END;
			trnInfo = trnInfo
					+ (CommonConstants.DSCNT_APLD_AMT_STRT).replace(">", " Ccy=\"" + vchrInfo.getVchrCry() + "\">");
			trnInfo = trnInfo + formatValue(String.valueOf(vchrInfo.getVchrDiscAmt()));
			trnInfo = trnInfo + CommonConstants.DSCNT_APLD_AMT_END;
			trnInfo = trnInfo + (CommonConstants.RMTD_AMT_STRT).replace(">", " Ccy=\"" + vchrInfo.getVchrCry() + "\">");

			double rmtAmt = vchrInfo.getVchrAmt() - vchrInfo.getVchrDiscAmt();

			trnInfo = trnInfo + formatValue(String.valueOf(rmtAmt));
			trnInfo = trnInfo + CommonConstants.RMTD_AMT_END;
			trnInfo = trnInfo + CommonConstants.REF_DOC_AMT_END;
			trnInfo = trnInfo + CommonConstants.STRD_END;
		} else {
			trnInfo = trnInfo + CommonConstants.STRD_STRT;
			trnInfo = trnInfo + CommonConstants.REF_DOC_INFO_STRT;
			trnInfo = trnInfo + CommonConstants.TP_STRT;
			trnInfo = trnInfo + CommonConstants.CD_PRTRY_STRT;
			trnInfo = trnInfo + CommonConstants.CODE_STRT;
			trnInfo = trnInfo + "CINV"; /* FIXED FOR CHK PAYMENT TYPE */
			trnInfo = trnInfo + CommonConstants.CODE_END;
			trnInfo = trnInfo + CommonConstants.CD_PRTRY_END;
			trnInfo = trnInfo + CommonConstants.TP_END;
			trnInfo = trnInfo + CommonConstants.NB_STRT;
			
			String temp = commonFunctions.whiteEscapeCharacter(vchrInfo.getVchrInvNo());
			
			trnInfo = trnInfo + temp + year;
			trnInfo = trnInfo + CommonConstants.NB_END;
			trnInfo = trnInfo + CommonConstants.RLTD_DT_STRT;
			trnInfo = trnInfo + vchrInfo.getInvDt();
			trnInfo = trnInfo + CommonConstants.RLTD_DT_END;
			trnInfo = trnInfo + CommonConstants.REF_DOC_INFO_END;
			trnInfo = trnInfo + CommonConstants.REF_DOC_AMT_STRT;
			trnInfo = trnInfo
					+ (CommonConstants.DUE_PAY_AMT_STRT).replace(">", " Ccy=\"" + vchrInfo.getVchrCry() + "\">");
			trnInfo = trnInfo + formatValue(String.valueOf(vchrInfo.getVchrAmt()));
			trnInfo = trnInfo + CommonConstants.DUE_PAY_AMT_END;
			trnInfo = trnInfo
					+ (CommonConstants.DSCNT_APLD_AMT_STRT).replace(">", " Ccy=\"" + vchrInfo.getVchrCry() + "\">");
			trnInfo = trnInfo + formatValue(String.valueOf(vchrInfo.getVchrDiscAmt()));
			trnInfo = trnInfo + CommonConstants.DSCNT_APLD_AMT_END;
			trnInfo = trnInfo + (CommonConstants.RMTD_AMT_STRT).replace(">", " Ccy=\"" + vchrInfo.getVchrCry() + "\">");

			double rmtAmt = vchrInfo.getVchrAmt() - vchrInfo.getVchrDiscAmt();

			trnInfo = trnInfo + formatValue(String.valueOf(rmtAmt));
			trnInfo = trnInfo + CommonConstants.RMTD_AMT_END;
			trnInfo = trnInfo + CommonConstants.REF_DOC_AMT_END;
			trnInfo = trnInfo + CommonConstants.STRD_END;

			/* UPDATE REFERENCE CHECK NUMBER FOR INVOICE */
			checkDAO.updCheckForInvoice(session, vchrInfo.getVchrPfx() + "-" + vchrInfo.getVchrNo(), tmpChkNo);
		}

		tmpVendorId = vchrInfo.getVchrVenId();

		return trnInfo;
	}

	/* GET NEXT CHECK NUMBER BASED ON CURRENT ONE */
	private void nextChkNo(Session session,CompanyBankInfo bankInfo, ChkLotUsage checkInfo) throws Exception {
		CheckDAO checkDAO = new CheckDAO();

		String rtnValue = "";
		int orgLngth = checkInfo.getCurChk().length();

		int newChckvalue = Integer.parseInt(checkInfo.getCurChk()) + 1;

		while (1 == 1) {
			int iFlg = checkDAO.validateVoidCheck(bankInfo.getBnkCode(), bankInfo.getAcctNo(),
					String.valueOf(newChckvalue));

			if (iFlg == 0) {
				break;
			} else {
				newChckvalue = newChckvalue + 1;
			}
		}

		if (String.valueOf(newChckvalue).length() == orgLngth) {
			rtnValue = String.valueOf(newChckvalue);
		} else {
			for (int i = 0; i < orgLngth - String.valueOf(newChckvalue).length(); i++) {
				rtnValue = rtnValue + "0";
			}

			rtnValue = rtnValue + String.valueOf(newChckvalue);
		}

		checkInfo.setCurChk(rtnValue);

		checkDAO.updCheckNo(checkInfo);

	}

	/* FORMAT VALUE TO TWO DECIMAL PLACES */
	private String formatValue(String amount) {
		DecimalFormat df = new DecimalFormat("#.00");
		String formattedValue = df.format(Double.parseDouble(amount));

		formattedValue = String.format("%.2f", Double.parseDouble(amount));

		return formattedValue;
	}


	private static String formatValue(String value, int len, int totallen, char ch, char lrj ) {
		String chAdd="";
		if(lrj=='R')
		{
			if(totallen>len)
			{
			for(int i=1;i<=totallen-len;i++)
			{
				chAdd+=ch;
			}
			value=chAdd+value;
			}
			else
			{
				for(int i=0;i<totallen;i++)
				{
					chAdd+=value.charAt(i);
				}
				value=chAdd;
			}
		}
		else
		{
			if(totallen>len)
			{
			for(int i=1;i<=totallen-len;i++)
			{
				chAdd+=ch;
			}
			value=value+chAdd;
			}
			else
			{
				for(int i=0;i<totallen;i++)
				{
					chAdd+=value.charAt(i);
				}
				value=chAdd;
			}
		}

		return value;
	}
	
	private List<String> getDistinctVendors(List<InvoiceInfo> info) {
		List<String> venList = new ArrayList<String>();

		for (int i = 0; i < info.size(); i++) {
			if (!venList.contains(info.get(i).getVchrVenId())) {
				venList.add(info.get(i).getVchrVenId());
			}
		}

		return venList;
	}

	
	private List<InvoiceInfo> getDistinctVendorInfo(List<InvoiceInfo> info)
	{
		double totalAmnt = 0.0;
		String vchrInvNo = "";
		String vchrRef = "";
		String vchrPayMthd = "";
		String vchrCry = "";
		String cmpyId = "";
		String vchrNotes = "";
		List<InvoiceInfo> invTotals = new ArrayList<InvoiceInfo>();
		CommonFunctions functions = new CommonFunctions();
		
		List<String> distVenList = getDistinctVendors(info);
		
		for(int i =0; i < distVenList.size(); i++)
		{
			InvoiceInfo invoiceInfo = new InvoiceInfo();
			totalAmnt = 0.0;
			vchrInvNo = "";
			vchrRef = "";
			for(int j =0; j < info.size(); j++)
			{
				if(info.get(j).getVchrVenId().equals(distVenList.get(i)))
				{
					totalAmnt = totalAmnt + info.get(j).getVchrAmt() - info.get(j).getVchrDiscAmt();
					vchrInvNo = vchrInvNo + info.get(j).getVchrInvNo() + " ";
					vchrRef = vchrRef + info.get(j).getVchrPfx() + info.get(j).getVchrNo() ;
					
					if(info.get(j).getVchrInvNo() != null)
					{
						if(info.get(j).getVchrInvNo().trim().length() > 0)
						{
							vchrRef = vchrRef + " " + info.get(j).getVchrInvNo().trim();
						}
					}
					
					if(info.get(j).getNotesToPayee() != null)
					{
						if(info.get(j).getNotesToPayee().trim().length() > 0)
						{
							vchrRef = vchrRef + " " + info.get(j).getNotesToPayee().trim();
						}
					}
					
					vchrRef = vchrRef + "/";
					
					vchrPayMthd = info.get(j).getPayMthd();
					vchrCry = info.get(j).getVchrCry();
					cmpyId = info.get(j).getCmpyId();
				}
			}
						
			invoiceInfo.setCmpyId(cmpyId);
			invoiceInfo.setVchrVenId(distVenList.get(i));
			invoiceInfo.setVchrAmt(totalAmnt);
			
			String temp = functions.whiteEscapeCharacter(vchrInvNo);
			if(temp.length() > 140)
			{
				temp = temp.substring(0,139);
				temp = temp + "/";
			}
			
			invoiceInfo.setVchrInvNo(temp);
			
			temp = functions.whiteEscapeCharacter(vchrRef);
			
			if(temp.length() > 140)
			{
				temp = temp.substring(0,139);
				temp = temp + "/";
			}
			
			invoiceInfo.setVchrNo(temp);
			invoiceInfo.setPayMthd(vchrPayMthd);
			invoiceInfo.setVchrCry(vchrCry);
			
			invTotals.add(invoiceInfo);
			
		}
		
		return invTotals;
	}
	
	


}
