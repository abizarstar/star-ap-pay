package com.star.common.ach;

public class BatchHeaderRecord {

	private String recordType = "5"; // Size - 1
	private String serviceCode = ""; // Size - 3
	private String companyNm = ""; // Size - 16
	private String companyDiscretionaryData = ""; // Size - 20
	private String companyIdentification = ""; // Size - 10
	private String companyEntryClassCode = ""; // Size - 3
	private String companyEntryDesc = ""; // Size - 10
	private String companyDescriptiveDate = ""; // Size - 6
	private String effectiveEntryDate = ""; // Size - 6
	private String settlementDate = ""; // Size - 3
	private String originatorStatusCode = "1"; // Size - 1
	private String odfiIdentity = ""; // Size - 8
	private String batchNumber = ""; // Size - 7
	public String getRecordType() {
		return recordType;
	}
	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	public String getCompanyNm() {
		return companyNm;
	}
	public void setCompanyNm(String companyNm) {
		this.companyNm = companyNm;
	}
	public String getCompanyDiscretionaryData() {
		return companyDiscretionaryData;
	}
	public void setCompanyDiscretionaryData(String companyDiscretionaryData) {
		this.companyDiscretionaryData = companyDiscretionaryData;
	}
	public String getCompanyIdentification() {
		return companyIdentification;
	}
	public void setCompanyIdentification(String companyIdentification) {
		this.companyIdentification = companyIdentification;
	}
	public String getCompanyEntryClassCode() {
		return companyEntryClassCode;
	}
	public void setCompanyEntryClassCode(String companyEntryClassCode) {
		this.companyEntryClassCode = companyEntryClassCode;
	}
	public String getCompanyEntryDesc() {
		return companyEntryDesc;
	}
	public void setCompanyEntryDesc(String companyEntryDesc) {
		this.companyEntryDesc = companyEntryDesc;
	}
	public String getCompanyDescriptiveDate() {
		return companyDescriptiveDate;
	}
	public void setCompanyDescriptiveDate(String companyDescriptiveDate) {
		this.companyDescriptiveDate = companyDescriptiveDate;
	}
	public String getEffectiveEntryDate() {
		return effectiveEntryDate;
	}
	public void setEffectiveEntryDate(String effectiveEntryDate) {
		this.effectiveEntryDate = effectiveEntryDate;
	}
	public String getSettlementDate() {
		return settlementDate;
	}
	public void setSettlementDate(String settlementDate) {
		this.settlementDate = settlementDate;
	}
	public String getOriginatorStatusCode() {
		return originatorStatusCode;
	}
	public void setOriginatorStatusCode(String originatorStatusCode) {
		this.originatorStatusCode = originatorStatusCode;
	}
	public String getOdfiIdentity() {
		return odfiIdentity;
	}
	public void setOdfiIdentity(String odfiIdentity) {
		this.odfiIdentity = odfiIdentity;
	}
	public String getBatchNumber() {
		return batchNumber;
	}
	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}
}
