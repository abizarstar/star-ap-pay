package com.star.common.ach;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.star.dao.GetACHDetailsDAO;
import com.star.linkage.ach.CompanyBankInfo;
import com.star.linkage.ach.VendorBankInfo;

public class GenerateACH {

	public static void main(String args[])
	{
		
		GetACHDetailsDAO detailsDAO = new GetACHDetailsDAO();
		
		CompanyBankInfo companyBankInfo = detailsDAO.getBankInfoByCompany("AP3", "", "");
		VendorBankInfo venorBankInfo = detailsDAO.getBankInfoByVendor("AP3", "1010");
		
		
		formatAmount(2512121.60521);
		
		System.out.println("-------------------------------------------");
		System.out.println("-------------------------------------------");
		System.out.println("-------------------------------------------");
		
		
		/* FILE HEADER */
		FileHeaderRecord fileHeaderRecord = new FileHeaderRecord();
		
		fileHeaderRecord.setImmediateOrigin(companyBankInfo.getBnkName());
		fileHeaderRecord.setImmediateOrigNm(companyBankInfo.getRoutingNo());
		fileHeaderRecord.setImmediateDest(companyBankInfo.getAcctNo());
		fileHeaderRecord.setImmediateDestNm(companyBankInfo.getAcctNm());
		String fileHeader = createFileHeader(fileHeaderRecord);
		System.out.println(fileHeader);
		
		/*BATCH HEADER*/
		BatchHeaderRecord batchHeaderRecord = new BatchHeaderRecord();
		String batchHeader = createBatchHeader(batchHeaderRecord);
		System.out.println(batchHeader);
		
		/*ENTRY DETAIL*/
		EntryDetailRecord entryDetailRecord = new EntryDetailRecord();
		String entryDetail = createEntryDetail(entryDetailRecord);
		System.out.println(entryDetail);
		
		/*BATCH CONTROL*/
		BatchControlRecord batchControlRecord = new BatchControlRecord();
		String batchControl = batchControlRecord(batchControlRecord);
		System.out.println(batchControl);
		
		/*FILE CONTROL*/
		FileControlRecord fileControlRecord = new FileControlRecord();
		String fileControl = fileControlRecord(fileControlRecord);
		System.out.println(fileControl);
	}
	
	public static String createFileHeader(FileHeaderRecord header)
	{
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMdd");
		SimpleDateFormat timeFormat = new SimpleDateFormat("HHmm");
		String currentDate = dateFormat.format(date);
		String currentTime = timeFormat.format(date);
		header.setFileCreationDt(currentDate);
		header.setFileCreationTime(currentTime);
		String fileHeaderOutput = "";
		
		fileHeaderOutput = fileHeaderOutput + header.getRecordType();
		fileHeaderOutput = fileHeaderOutput + header.getPriorityCode();
		fileHeaderOutput = fileHeaderOutput + addSpaces(header.getImmediateDest(), 10, "B");
		fileHeaderOutput = fileHeaderOutput + addSpaces(header.getImmediateOrigin(), 10, "B");
		fileHeaderOutput = fileHeaderOutput + header.getFileCreationDt();
		fileHeaderOutput = fileHeaderOutput + header.getFileCreationTime();
		fileHeaderOutput = fileHeaderOutput + header.getFileIdModifier();
		fileHeaderOutput = fileHeaderOutput + header.getRecordSize();
		fileHeaderOutput = fileHeaderOutput + header.getBlockingFactor();
		fileHeaderOutput = fileHeaderOutput + header.getFormatCode();
		fileHeaderOutput = fileHeaderOutput + addSpaces(header.getImmediateDestNm(), 23, "A");
		fileHeaderOutput = fileHeaderOutput + addSpaces(header.getImmediateOrigNm(), 23, "A");
		fileHeaderOutput = fileHeaderOutput + addSpaces(header.getReferenceCode(), 8, "A");
		
		return fileHeaderOutput;
		
	}
	
	public static String createBatchHeader(BatchHeaderRecord batch)
	{
		Date date = new Date();
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMdd");
		String currentDate = dateFormat.format(date);
		batch.setCompanyDescriptiveDate(currentDate);
		String batchHeaderOutput = "";

		batchHeaderOutput = batch.getRecordType();
		batchHeaderOutput = batchHeaderOutput + batch.getServiceCode();
		batchHeaderOutput = batchHeaderOutput + addSpaces(batch.getCompanyNm(), 16, "A");
		batchHeaderOutput = batchHeaderOutput + addSpaces(batch.getCompanyDiscretionaryData(), 20, "A");
		batchHeaderOutput = batchHeaderOutput + addSpaces(batch.getCompanyIdentification(), 10, "B");
		batchHeaderOutput = batchHeaderOutput + batch.getCompanyEntryClassCode();
		batchHeaderOutput = batchHeaderOutput + addSpaces(batch.getCompanyEntryDesc(), 10, "A");
		batchHeaderOutput = batchHeaderOutput + batch.getCompanyDescriptiveDate();
		batchHeaderOutput = batchHeaderOutput + addSpaces(batch.getSettlementDate(), 3, "A");
		batchHeaderOutput = batchHeaderOutput + batch.getOriginatorStatusCode();
		batchHeaderOutput = batchHeaderOutput + addSpaces(batch.getOdfiIdentity(), 8, "B");
		batchHeaderOutput = batchHeaderOutput + addSpaces(batch.getBatchNumber(), 7, "B");
		
		return batchHeaderOutput;
		
	}
	
	public static String createEntryDetail(EntryDetailRecord entry)
	{
		String entryDetailOutput = "";
		
		entryDetailOutput = entry.getRecordType();
		entryDetailOutput = entryDetailOutput + entry.getTransactionCode();
		entryDetailOutput = entryDetailOutput + entry.getRdfiIdentity();
		entryDetailOutput = entryDetailOutput + entry.getCheckDigit();
		entryDetailOutput = entryDetailOutput + addSpaces(entry.getDfiAccountNumber(), 17, "A");
		entryDetailOutput = entryDetailOutput + entry.getAmount();
		entryDetailOutput = entryDetailOutput + addSpaces(entry.getIndividualIdentityNumber(), 15, "A");
		entryDetailOutput = entryDetailOutput + addSpaces(entry.getIndividualName(), 22, "A");
		entryDetailOutput = entryDetailOutput + addSpaces(entry.getDiscretionaryData(), 2, "A");
		entryDetailOutput = entryDetailOutput + entry.getAddendaRecord();
		entryDetailOutput = entryDetailOutput + addSpaces(entry.getTraceNumber(), 15, "A");
		
		return entryDetailOutput;
	}
	
	public static String batchControlRecord(BatchControlRecord batchControl)
	{
		String batchControlOutput = "";
		
		batchControlOutput = batchControl.getRecordType();
		batchControlOutput = batchControlOutput + batchControl.getServiceCode();
		batchControlOutput = batchControlOutput + batchControl.getEntryCount();
		batchControlOutput = batchControlOutput + batchControl.getEntryHash();
		batchControlOutput = batchControlOutput + batchControl.getTotalDebitEntryDollar();
		batchControlOutput = batchControlOutput + batchControl.getTotalCreditEntryDollar();
		batchControlOutput = batchControlOutput + batchControl.getCompanyIdentity();
		batchControlOutput = batchControlOutput + batchControl.getMeesageAuthCode();
		batchControlOutput = batchControlOutput + batchControl.getReserved();
		batchControlOutput = batchControlOutput + batchControl.getOdfiIdentity();
		batchControlOutput = batchControlOutput + batchControl.getBatchNumber();
		
		return batchControlOutput;
	}
	
	public static String fileControlRecord(FileControlRecord fileControl)
	{
		String fileControlOutput = "";
		
		fileControlOutput = fileControl.getRecordType();
		fileControlOutput = fileControlOutput + fileControl.getBatchCount();
		fileControlOutput = fileControlOutput + fileControl.getBlockCount();
		fileControlOutput = fileControlOutput + fileControl.getEntryCount();
		fileControlOutput = fileControlOutput + fileControl.getEntryHash();
		fileControlOutput = fileControlOutput + fileControl.getToalDebitEntryDollar();
		fileControlOutput = fileControlOutput + fileControl.getToalCreditEntryDollar();
		fileControlOutput = fileControlOutput + fileControl.getReserved();
		
		return fileControlOutput;
	}
	
	/* ADD ADDITIONAL SPACES BEFORE THE STRING */
	public static String addSpaces(String input, int size, String flag)
	{
		String formatStr = "";
		
		for(int i = 0 ; i < size - input.length(); i++)
		{
			formatStr = formatStr + " ";
		}
		
		if(flag.equals("A"))
		{
			formatStr =  input + formatStr;
		}
		else if(flag.equals("B"))
		{
			formatStr = formatStr + input;
		}
		
		return formatStr;
	}
	
	public static String formatAmount(double amount)
	{
		
		amount = (double) Math.round(amount * 100) / 100;
		
		String amountStr = "";
		
		double doubleNumber = amount;
		String doubleAsString = String.valueOf(doubleNumber);
		int indexOfDecimal = doubleAsString.indexOf(".");
		
		String beforeDecimal = doubleAsString.substring(0, indexOfDecimal);
		String afterDecimal = doubleAsString.substring(indexOfDecimal+1);
		
		System.out.println("Double Number: " + doubleNumber);
		System.out.println("Integer Part: " + beforeDecimal);
		System.out.println("Decimal Part: " + afterDecimal);
		
		for(int i = 0; i < 8 - beforeDecimal.length(); i++)
		{
			amountStr = amountStr + "0";
		}
		
		amountStr = amountStr + beforeDecimal;
		
		if(afterDecimal.length() == 0)
		{
			amountStr = amountStr + "00";
		}
		else if(afterDecimal.length() == 1)
		{
			amountStr = amountStr + afterDecimal + "0";
		}
		else if(afterDecimal.length() >= 2)
		{
			amountStr = amountStr + afterDecimal;
		}
		
		System.out.println(amountStr);
		
		
		return amountStr;
		
	}
}
