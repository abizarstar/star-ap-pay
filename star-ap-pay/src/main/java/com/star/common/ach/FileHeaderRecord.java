package com.star.common.ach;

public class FileHeaderRecord {

	private String recordType="1"; // Size - 1
	private String priorityCode="01"; // Size - 2
	private String immediateDest=""; // Size - 10
	private String immediateOrigin=""; // Size - 10
	private String fileCreationDt=""; // Size - 6
	private String fileCreationTime=""; // Size - 4
	private String fileIdModifier="A"; // Size - 1
	
	private String recordSize="094"; // Size - 3
	private String blockingFactor="10"; // Size - 2
	private String formatCode="1"; // Size - 1
	
	private String immediateDestNm=""; // Size - 23
	private String immediateOrigNm=""; // Size - 23
	private String referenceCode=""; // Size - 8
	
	
	public String getRecordType() {
		return recordType;
	}
	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}
	public String getPriorityCode() {
		return priorityCode;
	}
	public void setPriorityCode(String priorityCode) {
		this.priorityCode = priorityCode;
	}
	public String getImmediateDest() {
		return immediateDest;
	}
	public void setImmediateDest(String immediateDest) {
		this.immediateDest = immediateDest;
	}
	public String getImmediateOrigin() {
		return immediateOrigin;
	}
	public void setImmediateOrigin(String immediateOrigin) {
		this.immediateOrigin = immediateOrigin;
	}
	public String getFileCreationDt() {
		return fileCreationDt;
	}
	public void setFileCreationDt(String fileCreationDt) {
		this.fileCreationDt = fileCreationDt;
	}
	public String getFileCreationTime() {
		return fileCreationTime;
	}
	public void setFileCreationTime(String fileCreationTime) {
		this.fileCreationTime = fileCreationTime;
	}
	public String getFileIdModifier() {
		return fileIdModifier;
	}
	public void setFileIdModifier(String fileIdModifier) {
		this.fileIdModifier = fileIdModifier;
	}
	public String getRecordSize() {
		return recordSize;
	}
	public void setRecordSize(String recordSize) {
		this.recordSize = recordSize;
	}
	public String getBlockingFactor() {
		return blockingFactor;
	}
	public void setBlockingFactor(String blockingFactor) {
		this.blockingFactor = blockingFactor;
	}
	public String getFormatCode() {
		return formatCode;
	}
	public void setFormatCode(String formatCode) {
		this.formatCode = formatCode;
	}
	public String getImmediateDestNm() {
		return immediateDestNm;
	}
	public void setImmediateDestNm(String immediateDestNm) {
		this.immediateDestNm = immediateDestNm;
	}
	public String getImmediateOrigNm() {
		return immediateOrigNm;
	}
	public void setImmediateOrigNm(String immediateOrigNm) {
		this.immediateOrigNm = immediateOrigNm;
	}
	public String getReferenceCode() {
		return referenceCode;
	}
	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}
	
	
	
	
	
}
