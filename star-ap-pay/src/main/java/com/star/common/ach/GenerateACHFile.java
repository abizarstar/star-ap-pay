package com.star.common.ach;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.star.dao.GetACHDetailsDAO;
import com.star.linkage.ach.CompanyBankInfo;
import com.star.linkage.ach.InvoiceInfo;
import com.star.linkage.ach.VendorBankInfo;

public class GenerateACHFile {

	int iTransactionRecordCount = 0;
	double totalAmount = 0;
	
	public String getACHData(List<String> invoiceList) {
		String finalOutput = "";
		int hashRoutingNumber = 0;
		int iLineCount = 0;
		
		GetACHDetailsDAO detailsDAO = new GetACHDetailsDAO();

		CompanyBankInfo companyBankInfo = detailsDAO.getBankInfoByCompany("AP3", "", "");
		formatAmount(2512121.60521);

		
		if(invoiceList.size() > 0)
		{
			detailsDAO.getInvoiceDetails(invoiceList.get(0));
		}
		
		
		System.out.println("-------------------------------------------");
		System.out.println("-------------------------------------------");
		System.out.println("-------------------------------------------");

		/* FILE HEADER */
		FileHeaderRecord fileHeaderRecord = new FileHeaderRecord();

		fileHeaderRecord.setImmediateOrigin(companyBankInfo.getRoutingNo());
		fileHeaderRecord.setImmediateOrigNm(companyBankInfo.getBnkName());
		fileHeaderRecord.setImmediateDest(companyBankInfo.getAcctNo());
		fileHeaderRecord.setImmediateDestNm(companyBankInfo.getAcctNm());
		String fileHeader = createFileHeader(fileHeaderRecord);

		finalOutput = finalOutput + fileHeader;
		finalOutput = finalOutput + "##\n";

		iLineCount = iLineCount + 1;

		/* BATCH HEADER */
		BatchHeaderRecord batchHeaderRecord = new BatchHeaderRecord();
		batchHeaderRecord.setServiceCode("200");
		batchHeaderRecord.setCompanyIdentification(companyBankInfo.getAcctNo());
		batchHeaderRecord.setCompanyNm(companyBankInfo.getBnkName());
		batchHeaderRecord.setCompanyEntryClassCode("CCD");

		if (companyBankInfo.getRoutingNo().length() == 8) {
			batchHeaderRecord.setOdfiIdentity(companyBankInfo.getRoutingNo());
		} else if (companyBankInfo.getRoutingNo().length() == 9) {
			batchHeaderRecord.setOdfiIdentity(
					companyBankInfo.getRoutingNo().substring(0, companyBankInfo.getRoutingNo().length() - 1));
		} else if (companyBankInfo.getRoutingNo().length() == 10) {
				
			batchHeaderRecord.setOdfiIdentity(companyBankInfo.getRoutingNo().substring(1, companyBankInfo.getRoutingNo().length() - 1));
		}

		String batchHeader = createBatchHeader(batchHeaderRecord);

		finalOutput = finalOutput + batchHeader;
		finalOutput = finalOutput + "##\n";
		iLineCount = iLineCount + 1;
		
		for(int i = 0; i < invoiceList.size(); i++)
		{
			iTransactionRecordCount = i + 1;
			
			InvoiceInfo invoiceInfo = detailsDAO.getInvoiceDetails(invoiceList.get(i));
			
			VendorBankInfo vendorBankInfo = detailsDAO.getBankInfoByVendor("AP3", invoiceInfo.getVchrVenId());
			
			/* ENTRY DETAIL */
			EntryDetailRecord entryDetailRecord = new EntryDetailRecord();
			entryDetailRecord.setTransactionCode("22");
			entryDetailRecord.setRdfiIdentity(vendorBankInfo.getBnkRoutingNo());
			entryDetailRecord.setDfiAccountNumber(vendorBankInfo.getBnkAcctNo());
			entryDetailRecord.setIndividualName(vendorBankInfo.getVenNm());
			entryDetailRecord.setIndividualIdentityNumber(invoiceInfo.getVchrVenId());
			entryDetailRecord.setAmount(formatAmount(invoiceInfo.getVchrAmt()));
			entryDetailRecord.setAddendaRecord("1");
			entryDetailRecord.setTraceNumber(String.valueOf(i+1));
			
			System.out.println("ROUTING NO -------->" + vendorBankInfo.getBnkRoutingNo());
			
			
			totalAmount = totalAmount + invoiceInfo.getVchrAmt();
			
			hashRoutingNumber = hashRoutingNumber + Integer.parseInt(vendorBankInfo.getBnkRoutingNo());

			String entryDetail = createEntryDetail(entryDetailRecord);

			finalOutput = finalOutput + entryDetail;
			finalOutput = finalOutput + "##\n";
			iLineCount = iLineCount + 1;

			AddendaRecord addendaRecord = new AddendaRecord();
			addendaRecord.setAddendaType("05");
			addendaRecord.setPaymentInformation("INV-"+invoiceInfo.getVchrInvNo() + " CTL-NO-"+invoiceInfo.getVchrCtlNo());
			addendaRecord.setSeqNo("1");
			addendaRecord.setEntrySeqNo(String.valueOf(i+1));
			String addenda = createAddendaRecord(addendaRecord);
			
			finalOutput = finalOutput + addenda;
			finalOutput = finalOutput + "##\n";
			
		}
		
		
		

		/* BATCH CONTROL */
		BatchControlRecord batchControlRecord = new BatchControlRecord();
		batchControlRecord.setServiceCode("200");
		batchControlRecord.setEntryCount(String.valueOf(iTransactionRecordCount));
		batchControlRecord.setEntryHash(String.valueOf(hashRoutingNumber));
		batchControlRecord.setTotalDebitEntryDollar("0");
		batchControlRecord.setTotalCreditEntryDollar(formatAmount(totalAmount));
		batchControlRecord.setCompanyIdentity(companyBankInfo.getAcctNo());
		batchControlRecord.setOdfiIdentity(companyBankInfo.getRoutingNo());
		batchControlRecord.setBatchNumber("1");
		String batchControl = batchControlRecord(batchControlRecord);

		finalOutput = finalOutput + batchControl;
		finalOutput = finalOutput + "##\n";
		iLineCount = iLineCount + 1;

		/* FILE CONTROL */

		iLineCount = iLineCount + 1;

		FileControlRecord fileControlRecord = new FileControlRecord();
		fileControlRecord.setBatchCount("1");
		String blockCount = getBlockCount(iLineCount);

		fileControlRecord.setBlockCount(blockCount);
		fileControlRecord.setEntryHash(String.valueOf(hashRoutingNumber));
		fileControlRecord.setEntryCount(String.valueOf(iTransactionRecordCount));
		fileControlRecord.setToalDebitEntryDollar("0");
		fileControlRecord.setToalCreditEntryDollar(formatAmount(totalAmount));
		String fileControl = fileControlRecord(fileControlRecord);

		finalOutput = finalOutput + fileControl;
		finalOutput = finalOutput + "##\n";

		finalOutput = finalOutput + addAdditionalLines(iLineCount);

		System.out.println();
		System.out.println();
		System.out.println();

		System.out.println(finalOutput);

		return finalOutput;
	}

	public String createFileHeader(FileHeaderRecord header) {
		Date date = new Date();
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMdd");
		SimpleDateFormat timeFormat = new SimpleDateFormat("HHmm");
		String currentDate = dateFormat.format(date);
		String currentTime = timeFormat.format(date);
		header.setFileCreationDt(currentDate);
		header.setFileCreationTime(currentTime);
		String fileHeaderOutput = "";

		fileHeaderOutput = fileHeaderOutput + header.getRecordType();
		fileHeaderOutput = fileHeaderOutput + header.getPriorityCode();
		fileHeaderOutput = fileHeaderOutput + addChars(header.getImmediateDest(), 10, "B", " ");
		fileHeaderOutput = fileHeaderOutput + addChars(header.getImmediateOrigin(), 10, "B", " ");
		fileHeaderOutput = fileHeaderOutput + header.getFileCreationDt();
		fileHeaderOutput = fileHeaderOutput + header.getFileCreationTime();
		fileHeaderOutput = fileHeaderOutput + header.getFileIdModifier();
		fileHeaderOutput = fileHeaderOutput + header.getRecordSize();
		fileHeaderOutput = fileHeaderOutput + header.getBlockingFactor();
		fileHeaderOutput = fileHeaderOutput + header.getFormatCode();
		fileHeaderOutput = fileHeaderOutput + addChars(header.getImmediateDestNm(), 23, "A", " ");
		fileHeaderOutput = fileHeaderOutput + addChars(header.getImmediateOrigNm(), 23, "A", " ");
		fileHeaderOutput = fileHeaderOutput + addChars(header.getReferenceCode(), 8, "A", " ");

		return fileHeaderOutput;

	}

	public String createBatchHeader(BatchHeaderRecord batch) {
		Date date = new Date();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyMMdd");
		String currentDate = dateFormat.format(date);
		batch.setCompanyDescriptiveDate(currentDate);
		batch.setEffectiveEntryDate(currentDate);
		String batchHeaderOutput = "";

		batchHeaderOutput = batch.getRecordType();
		batchHeaderOutput = batchHeaderOutput + batch.getServiceCode();
		batchHeaderOutput = batchHeaderOutput + addChars(batch.getCompanyNm(), 16, "A", " ");
		batchHeaderOutput = batchHeaderOutput + addChars(batch.getCompanyDiscretionaryData(), 20, "A", " ");
		batchHeaderOutput = batchHeaderOutput + addChars(batch.getCompanyIdentification(), 10, "B", " ");
		batchHeaderOutput = batchHeaderOutput + batch.getCompanyEntryClassCode();
		batchHeaderOutput = batchHeaderOutput + addChars(batch.getCompanyEntryDesc(), 10, "A", " ");
		batchHeaderOutput = batchHeaderOutput + batch.getCompanyDescriptiveDate();
		batchHeaderOutput = batchHeaderOutput + batch.getEffectiveEntryDate();
		batchHeaderOutput = batchHeaderOutput + addChars(batch.getSettlementDate(), 3, "A", " ");
		batchHeaderOutput = batchHeaderOutput + batch.getOriginatorStatusCode();
		batchHeaderOutput = batchHeaderOutput + addChars(batch.getOdfiIdentity(), 8, "B", " ");
		batchHeaderOutput = batchHeaderOutput + addChars(batch.getBatchNumber(), 7, "B", " ");

		return batchHeaderOutput;

	}

	public String createEntryDetail(EntryDetailRecord entry) {
		String entryDetailOutput = "";

		entryDetailOutput = entry.getRecordType();
		entryDetailOutput = entryDetailOutput + entry.getTransactionCode();
		entryDetailOutput = entryDetailOutput + addChars(entry.getRdfiIdentity(), 8, "B", "0");
		entryDetailOutput = entryDetailOutput + entry.getCheckDigit();
		entryDetailOutput = entryDetailOutput + addChars(entry.getDfiAccountNumber(), 17, "A", " ");
		entryDetailOutput = entryDetailOutput + addChars(entry.getAmount(), 10, "B", "0");
		entryDetailOutput = entryDetailOutput + addChars(entry.getIndividualIdentityNumber(), 15, "A", " ");
		entryDetailOutput = entryDetailOutput + addChars(entry.getIndividualName(), 22, "A", " ");
		entryDetailOutput = entryDetailOutput + addChars(entry.getDiscretionaryData(), 2, "A", " ");
		entryDetailOutput = entryDetailOutput + entry.getAddendaRecord();
		entryDetailOutput = entryDetailOutput + addChars(entry.getTraceNumber(), 15, "B", "0");

		return entryDetailOutput;
	}
	
	public String createAddendaRecord(AddendaRecord addenda) {
		String addendaOutput = "";

		addendaOutput = addenda.getRecordType();
		addendaOutput = addendaOutput + addenda.getAddendaType();
		addendaOutput = addendaOutput + addChars(addenda.getPaymentInformation(), 80, "A", " ");
		addendaOutput = addendaOutput + addChars(addenda.getSeqNo(), 4, "B", "0");
		addendaOutput = addendaOutput + addChars(addenda.getEntrySeqNo(), 7, "B", "0");

		return addendaOutput;
	}

	public String batchControlRecord(BatchControlRecord batchControl) {
		String batchControlOutput = "";

		batchControlOutput = batchControl.getRecordType();
		batchControlOutput = batchControlOutput + addChars(batchControl.getServiceCode(), 3, "B", "0");
		batchControlOutput = batchControlOutput + addChars(batchControl.getEntryCount(), 6, "B", "0");
		batchControlOutput = batchControlOutput + addChars(batchControl.getEntryHash(), 10, "B", "0");
		batchControlOutput = batchControlOutput + addChars(batchControl.getTotalDebitEntryDollar(), 12, "B", "0");
		batchControlOutput = batchControlOutput + addChars(batchControl.getTotalCreditEntryDollar(), 12, "B", "0");
		batchControlOutput = batchControlOutput + addChars(batchControl.getCompanyIdentity(), 10, "B", "0");
		batchControlOutput = batchControlOutput + addChars(batchControl.getMeesageAuthCode(), 19, "B", " ");
		batchControlOutput = batchControlOutput + addChars(batchControl.getReserved(), 6, "B", " ");
		batchControlOutput = batchControlOutput + addChars(batchControl.getOdfiIdentity(), 8, "B", "0");
		batchControlOutput = batchControlOutput + addChars(batchControl.getBatchNumber(), 7, "B", "0");

		return batchControlOutput;
	}

	public String fileControlRecord(FileControlRecord fileControl) {
		String fileControlOutput = "";

		fileControlOutput = fileControl.getRecordType();
		fileControlOutput = fileControlOutput + addChars(fileControl.getBatchCount(), 6, "B", "0");
		fileControlOutput = fileControlOutput + addChars(fileControl.getBlockCount(), 6, "B", "0");
		fileControlOutput = fileControlOutput + addChars(fileControl.getEntryCount(), 8, "B", "0");
		fileControlOutput = fileControlOutput + addChars(fileControl.getEntryHash(), 10, "B", "0");
		fileControlOutput = fileControlOutput + addChars(fileControl.getToalDebitEntryDollar(), 12, "B", "0");
		fileControlOutput = fileControlOutput + addChars(fileControl.getToalCreditEntryDollar(), 12, "B", "0");
		fileControlOutput = fileControlOutput + addChars(fileControl.getReserved(), 39, "A", " ");

		return fileControlOutput;
	}

	/* ADD ADDITIONAL SPACES BEFORE THE STRING */
	public String addChars(String input, int size, String flag, String strVal) {
		String formatStr = "";

		for (int i = 0; i < size - input.length(); i++) {
			formatStr = formatStr + strVal;
		}

		if (flag.equals("A")) {
			formatStr = input + formatStr;
		} else if (flag.equals("B")) {
			formatStr = formatStr + input;
		}

		return formatStr;
	}

	public String formatAmount(double amount) {

		amount = (double) Math.round(amount * 100) / 100;

		String amountStr = "";

		double doubleNumber = amount;
		String doubleAsString = String.valueOf(doubleNumber);
		int indexOfDecimal = doubleAsString.indexOf(".");

		String beforeDecimal = doubleAsString.substring(0, indexOfDecimal);
		String afterDecimal = doubleAsString.substring(indexOfDecimal + 1);

		System.out.println("Double Number: " + doubleNumber);
		System.out.println("Integer Part: " + beforeDecimal);
		System.out.println("Decimal Part: " + afterDecimal);

		for (int i = 0; i < 8 - beforeDecimal.length(); i++) {
			amountStr = amountStr + "0";
		}

		amountStr = amountStr + beforeDecimal;

		if (afterDecimal.length() == 0) {
			amountStr = amountStr + "00";
		} else if (afterDecimal.length() == 1) {
			amountStr = amountStr + afterDecimal + "0";
		} else if (afterDecimal.length() >= 2) {
			amountStr = amountStr + afterDecimal;
		}

		System.out.println(amountStr);

		return amountStr;

	}

	public String addAdditionalLines(int iLineCount) {
		String reaminingOutput = "";
		int remainingCount = 10 - iLineCount % 10;

		for (int i = 0; i < remainingCount; i++) {
			reaminingOutput = reaminingOutput
					+ "9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999";

			if (i == remainingCount - 1) {
				reaminingOutput = reaminingOutput;
			} else {
				reaminingOutput = reaminingOutput + "\n";
			}
		}
		return reaminingOutput;
	}

	public String getBlockCount(int iLineCount) {
		int remainingCount = 10 - iLineCount % 10;

		remainingCount = (remainingCount + iLineCount) / 10;

		return String.valueOf(remainingCount);
	}
}
