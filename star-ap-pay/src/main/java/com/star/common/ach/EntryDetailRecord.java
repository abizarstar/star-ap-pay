package com.star.common.ach;

public class EntryDetailRecord {
	
	private String recordType = "6"; // Size - 1
	private String transactionCode = ""; // Size - 2 
	private String rdfiIdentity = ""; // Size - 8
	private String checkDigit = ""; // Size - 1
	private String dfiAccountNumber = ""; // Size - 17
	private String amount = ""; // Size - 10
	private String individualIdentityNumber = ""; // Size - 15
	private String individualName = ""; // Size - 22
	private String discretionaryData = ""; // Size - 2
	private String addendaRecord = "0"; // Size - 1
	private String traceNumber = ""; // Size - 15
	public String getRecordType() {
		return recordType;
	}
	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}
	public String getTransactionCode() {
		return transactionCode;
	}
	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}
	public String getRdfiIdentity() {
		return rdfiIdentity;
	}
	public void setRdfiIdentity(String rdfiIdentity) {
		this.rdfiIdentity = rdfiIdentity;
	}
	public String getCheckDigit() {
		return checkDigit;
	}
	public void setCheckDigit(String checkDigit) {
		this.checkDigit = checkDigit;
	}
	public String getDfiAccountNumber() {
		return dfiAccountNumber;
	}
	public void setDfiAccountNumber(String dfiAccountNumber) {
		this.dfiAccountNumber = dfiAccountNumber;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getIndividualIdentityNumber() {
		return individualIdentityNumber;
	}
	public void setIndividualIdentityNumber(String individualIdentityNumber) {
		this.individualIdentityNumber = individualIdentityNumber;
	}
	public String getIndividualName() {
		return individualName;
	}
	public void setIndividualName(String individualName) {
		this.individualName = individualName;
	}
	public String getDiscretionaryData() {
		return discretionaryData;
	}
	public void setDiscretionaryData(String discretionaryData) {
		this.discretionaryData = discretionaryData;
	}
	public String getAddendaRecord() {
		return addendaRecord;
	}
	public void setAddendaRecord(String addendaRecord) {
		this.addendaRecord = addendaRecord;
	}
	public String getTraceNumber() {
		return traceNumber;
	}
	public void setTraceNumber(String traceNumber) {
		this.traceNumber = traceNumber;
	}
	
	

}
