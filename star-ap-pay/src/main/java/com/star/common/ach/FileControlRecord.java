package com.star.common.ach;

public class FileControlRecord {

	private String recordType = "9"; // Size - 1
	private String batchCount = ""; // Size - 6
	private String blockCount = ""; // Size - 6
	private String entryCount = ""; // Size - 8
	private String entryHash = ""; // Size - 10
	private String toalDebitEntryDollar = ""; // Size - 12
	private String toalCreditEntryDollar = ""; // Size - 12
	private String reserved = ""; // Size - 39
	public String getRecordType() {
		return recordType;
	}
	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}
	public String getBatchCount() {
		return batchCount;
	}
	public void setBatchCount(String batchCount) {
		this.batchCount = batchCount;
	}
	public String getBlockCount() {
		return blockCount;
	}
	public void setBlockCount(String blockCount) {
		this.blockCount = blockCount;
	}
	public String getEntryCount() {
		return entryCount;
	}
	public void setEntryCount(String entryCount) {
		this.entryCount = entryCount;
	}
	public String getEntryHash() {
		return entryHash;
	}
	public void setEntryHash(String entryHash) {
		this.entryHash = entryHash;
	}
	public String getToalDebitEntryDollar() {
		return toalDebitEntryDollar;
	}
	public void setToalDebitEntryDollar(String toalDebitEntryDollar) {
		this.toalDebitEntryDollar = toalDebitEntryDollar;
	}
	public String getToalCreditEntryDollar() {
		return toalCreditEntryDollar;
	}
	public void setToalCreditEntryDollar(String toalCreditEntryDollar) {
		this.toalCreditEntryDollar = toalCreditEntryDollar;
	}
	public String getReserved() {
		return reserved;
	}
	public void setReserved(String reserved) {
		this.reserved = reserved;
	}
	
	
	
	
}
