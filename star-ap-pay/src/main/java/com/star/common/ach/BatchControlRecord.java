package com.star.common.ach;

public class BatchControlRecord {

	private String recordType = "8"; // Size - 1
	private String serviceCode = ""; // Size - 3
	private String entryCount = ""; // Size - 6
	private String entryHash = "";  // Size - 10
	private String totalDebitEntryDollar = ""; // Size - 12
	private String totalCreditEntryDollar = ""; // Size - 12
	private String companyIdentity = ""; // Size - 10
	private String meesageAuthCode = ""; // Size - 19
	private String reserved = ""; // Size - 6
	private String odfiIdentity = ""; // Size - 8
	private String batchNumber = ""; // Size - 7
	public String getRecordType() {
		return recordType;
	}
	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}
	public String getServiceCode() {
		return serviceCode;
	}
	public void setServiceCode(String serviceCode) {
		this.serviceCode = serviceCode;
	}
	public String getEntryCount() {
		return entryCount;
	}
	public void setEntryCount(String entryCount) {
		this.entryCount = entryCount;
	}
	public String getEntryHash() {
		return entryHash;
	}
	public void setEntryHash(String entryHash) {
		this.entryHash = entryHash;
	}
	public String getTotalDebitEntryDollar() {
		return totalDebitEntryDollar;
	}
	public void setTotalDebitEntryDollar(String totalDebitEntryDollar) {
		this.totalDebitEntryDollar = totalDebitEntryDollar;
	}
	public String getTotalCreditEntryDollar() {
		return totalCreditEntryDollar;
	}
	public void setTotalCreditEntryDollar(String totalCreditEntryDollar) {
		this.totalCreditEntryDollar = totalCreditEntryDollar;
	}
	public String getCompanyIdentity() {
		return companyIdentity;
	}
	public void setCompanyIdentity(String companyIdentity) {
		this.companyIdentity = companyIdentity;
	}
	public String getMeesageAuthCode() {
		return meesageAuthCode;
	}
	public void setMeesageAuthCode(String meesageAuthCode) {
		this.meesageAuthCode = meesageAuthCode;
	}
	public String getReserved() {
		return reserved;
	}
	public void setReserved(String reserved) {
		this.reserved = reserved;
	}
	public String getOdfiIdentity() {
		return odfiIdentity;
	}
	public void setOdfiIdentity(String odfiIdentity) {
		this.odfiIdentity = odfiIdentity;
	}
	public String getBatchNumber() {
		return batchNumber;
	}
	public void setBatchNumber(String batchNumber) {
		this.batchNumber = batchNumber;
	}
	
	
}
