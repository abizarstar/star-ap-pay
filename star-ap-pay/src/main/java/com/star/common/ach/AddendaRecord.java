package com.star.common.ach;

public class AddendaRecord {

	private String recordType = "7";
	private String addendaType = "";
	private String paymentInformation = "";
	private String seqNo = "";
	private String entrySeqNo = "";
	public String getRecordType() {
		return recordType;
	}
	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}
	public String getAddendaType() {
		return addendaType;
	}
	public void setAddendaType(String addendaType) {
		this.addendaType = addendaType;
	}
	public String getPaymentInformation() {
		return paymentInformation;
	}
	public void setPaymentInformation(String paymentInformation) {
		this.paymentInformation = paymentInformation;
	}
	public String getSeqNo() {
		return seqNo;
	}
	public void setSeqNo(String seqNo) {
		this.seqNo = seqNo;
	}
	public String getEntrySeqNo() {
		return entrySeqNo;
	}
	public void setEntrySeqNo(String entrySeqNo) {
		this.entrySeqNo = entrySeqNo;
	}
	
	
}
