package com.star.common.ach;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.hibernate.Session;

import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.CommonFunctions;
import com.star.dao.CheckDAO;
import com.star.dao.ExecCnfgDAO;
import com.star.dao.GetACHDetailsDAO;
import com.star.linkage.ach.CompanyAddress;
import com.star.linkage.ach.CompanyBankInfo;
import com.star.linkage.ach.InvoiceInfo;
import com.star.linkage.ach.PaymentTotals;
import com.star.linkage.ach.VendorAddress;
import com.star.linkage.ach.VendorBankInfo;
import com.star.linkage.execcnfg.ExecCnfgBrowseOutput;
import com.star.modal.ChkLotUsage;
import com.star.modal.ElecPaySeq;

public class GeneratePain001 {

	String tmpVendorId = ""; /*
								 * REQUIRED FOR CHECK PROCESSING TO ADD ALL THE SINGLE VENDOR PAYMENT IN SINGLE
								 * CHECK NO
								 */
	String tmpChkNo = "";
	int tmpInstrChkId = 0;
	int tmpInstrWirACHId = 0;

	public String getInvoiceDetails(Session session, String proposalId, String bnkCode, String acctNo) throws Exception{

		String outputXML = "";
		double totalPropAmnt = 0.0;
		double totACHAmount = 0.0;
		double totWireAmount = 0.0;
		double totChkAmount = 0.0;
		int iACHCount = 0;
		int iWireCount = 0;
		int iChkCount = 0;
		String cmpyId = "";
		tmpInstrChkId = 0 ;
		tmpInstrWirACHId = 0;

		CommonFunctions functions = new CommonFunctions();
		
		SimpleDateFormat formatDttm = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		SimpleDateFormat formatDt = new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat formatYear = new SimpleDateFormat("yyyy");

		Date date = new Date();
		String createdDttm = formatDttm.format(date);
		String createdDt = formatDt.format(date);
		String crtdYear = formatYear.format(date);

		System.out.println(createdDttm);

		GetACHDetailsDAO detailsDAO = new GetACHDetailsDAO();
		PaymentTotals paymentTotals = new PaymentTotals();/* TO STORE PAYMENT INFORMATION BASED ON PAYMENT METHOD */
		ElecPaySeq elecPaySeq = detailsDAO.getPaySequence(); /* TO GET SEQUENCE TRN ID FOR CHECK WIRE OR MESSAGE */

		List<InvoiceInfo> invVchrList = new ArrayList<InvoiceInfo>();
		
		List<String> venListCount = new ArrayList<String>();
		
		if (proposalId.length() > 0) {
			invVchrList = detailsDAO.getPropInvoices(proposalId);

			Collections.sort(invVchrList, new Comparator<InvoiceInfo>() {
				public int compare(InvoiceInfo s1, InvoiceInfo s2) {
					return s1.getVchrVenId().compareToIgnoreCase(s2.getVchrVenId());
				}
			});
			
			venListCount = getDistinctVendors(invVchrList);

			for (int i = 0; i < invVchrList.size(); i++) {
				if (i == 0) {
					cmpyId = invVchrList.get(i).getCmpyId();
				}

				if (invVchrList.get(i).getPayMthd().equals("WIR")) {
					totWireAmount = totWireAmount + invVchrList.get(i).getVchrAmt()
							- invVchrList.get(i).getVchrDiscAmt();
					iWireCount = iWireCount + 1;
				}

				if (invVchrList.get(i).getPayMthd().equals("CTX") || invVchrList.get(i).getPayMthd().equals("CCD")
						|| invVchrList.get(i).getPayMthd().equals("PPD")) {
					totACHAmount = totACHAmount + invVchrList.get(i).getVchrAmt() - invVchrList.get(i).getVchrDiscAmt();
					iACHCount = iACHCount + 1;
				}

				if (invVchrList.get(i).getPayMthd().equals("CHK")) {
					totChkAmount = totChkAmount + invVchrList.get(i).getVchrAmt() - invVchrList.get(i).getVchrDiscAmt();
					iChkCount = iChkCount + 1;
				}

				totalPropAmnt = totalPropAmnt + invVchrList.get(i).getVchrAmt() - invVchrList.get(i).getVchrDiscAmt();
			}

			paymentTotals.setiACHCount(iACHCount);
			//paymentTotals.setiChkCount(iChkCount);
			paymentTotals.setiChkCount(venListCount.size());
			paymentTotals.setiWireCount(iWireCount);
			paymentTotals.setTotACHAmount(totACHAmount);
			paymentTotals.setTotChkAmount(totChkAmount);
			paymentTotals.setTotWireAmount(totWireAmount);

		}

		CompanyAddress companyAddress = detailsDAO.getCompanyAddress(cmpyId);
		CompanyBankInfo bankInfo = detailsDAO.getBankInfoByCompany(cmpyId, bnkCode, acctNo);

		outputXML = outputXML + CommonConstants.PAY_HDR;
		
		/* CHANGE FOR CHECK FOR TRANSACTION COUNT*/
		int trnCount = 0;
		if(iChkCount == 0)
		{
			Collections.sort(invVchrList, new Comparator<InvoiceInfo>() {
				public int compare(InvoiceInfo s1, InvoiceInfo s2) {
					return s1.getPayMthd().compareToIgnoreCase(s2.getPayMthd());
				}
			});
			
			invVchrList = getDistinctVendorInfo(invVchrList);
			
			totWireAmount = 0;
			totACHAmount = 0;
			iWireCount = 0;
			iACHCount = 0;
			
			for (int i = 0; i < invVchrList.size(); i++) {
				if (i == 0) {
					cmpyId = invVchrList.get(i).getCmpyId();
				}

				if (invVchrList.get(i).getPayMthd().equals("WIR")) {
					totWireAmount = totWireAmount + invVchrList.get(i).getVchrAmt()
							- invVchrList.get(i).getVchrDiscAmt();
					iWireCount = iWireCount + 1;
				}

				if (invVchrList.get(i).getPayMthd().equals("CTX") || invVchrList.get(i).getPayMthd().equals("CCD")
						|| invVchrList.get(i).getPayMthd().equals("PPD")) {
					totACHAmount = totACHAmount + invVchrList.get(i).getVchrAmt() - invVchrList.get(i).getVchrDiscAmt();
					iACHCount = iACHCount + 1;
				}
			}
			
			paymentTotals.setiACHCount(iACHCount);
			paymentTotals.setiWireCount(iWireCount);
			paymentTotals.setTotACHAmount(totACHAmount);
			paymentTotals.setTotWireAmount(totWireAmount);
			
			trnCount = invVchrList.size();
		}
		else
		{
			trnCount = venListCount.size();
		}
		
		outputXML = outputXML + getGrpHdr(elecPaySeq, companyAddress, bankInfo, totalPropAmnt, trnCount,
				createdDttm, iChkCount);

		String tempPayMthd = "";
		tmpChkNo = "";
		tmpVendorId = "";
		
		/* CHECK TO CALCULATE TOTAL AGAINST EACH VENDOR */
		Map<String, Double> vendTotalMap = new HashMap<String, Double>();
		double venChkTotal = 0;

		List<String> venList = getDistinctVendors(invVchrList);
		for (int k = 0; k < venList.size(); k++) {
			
			venChkTotal = 0;
			
			for (int j = 0; j < invVchrList.size(); j++) {
				
				if(venList.get(k).equals(invVchrList.get(j).getVchrVenId()))
				{
					venChkTotal = venChkTotal + (invVchrList.get(j).getVchrAmt() - invVchrList.get(j).getVchrDiscAmt());
				}

			}
			
			vendTotalMap.put(venList.get(k), venChkTotal);
		}
		
		for (int i = 0; i < invVchrList.size(); i++) {
			if (!invVchrList.get(i).getPayMthd().equals(tempPayMthd)) {
				if (tempPayMthd.length() > 0) {
					outputXML = outputXML + CommonConstants.PMNT_INF_END;
				}

				outputXML = outputXML + CommonConstants.PMNT_INF_STRT;

				outputXML = outputXML + getPaymentInfo(session, elecPaySeq, companyAddress, detailsDAO, bankInfo, createdDt,
						invVchrList.get(i), paymentTotals);
			}

			/* CHECK ID THE PAYMENT METHOD IS CHECK OR WIRE */
			if (invVchrList.get(i).getPayMthd().equals("CHK")) {

				

				outputXML = outputXML
						+ getTransationsInfoChk(session, elecPaySeq, crtdYear, detailsDAO, invVchrList.get(i), bankInfo, vendTotalMap);
			} else {
				outputXML = outputXML + getTransationsInfo(session, elecPaySeq, crtdYear, detailsDAO, invVchrList.get(i));
			}

			tempPayMthd = invVchrList.get(i).getPayMthd();

			if (i == invVchrList.size() - 1 && invVchrList.get(i).getPayMthd().equals("CHK")) {
				outputXML = outputXML + CommonConstants.RMT_INF_END;

				outputXML = outputXML + CommonConstants.CREDIT_TRF_TRN_END;
			}

		}

		outputXML = outputXML + CommonConstants.PMNT_INF_END;

		outputXML = outputXML + CommonConstants.PAY_END;

		detailsDAO.updateMsgSeq(session, elecPaySeq.getMsgIdSeq());
		
		return outputXML;
	}

	/* CREATE GROUP HEADER FOR PAYMENT */
	public String getGrpHdr(ElecPaySeq elecPaySeq, CompanyAddress address, CompanyBankInfo bankInfo, double totAmount,
			int txnCount, String crtdDttm, int iChkCount) throws Exception {
		
		String grpHdr = "";
		String msgId = String.valueOf(elecPaySeq.getMsgIdSeq());
		String dateStr = crtdDttm;
		String code = "AUTH";
		String prop = "TEST";
		String noOfTxn = String.valueOf(txnCount);
		String totSum = String.valueOf(totAmount);
		String iniParty = address.getCmpyNm();
		String iniPartyId = bankInfo.getRoutingNo();
		
		/* EXEC Config DAO to set Prop TEST or LIVE*/
		ExecCnfgDAO execCnfg = new ExecCnfgDAO();
		BrowseResponse<ExecCnfgBrowseOutput> starBrowseResponse = new BrowseResponse<ExecCnfgBrowseOutput>();
		starBrowseResponse.setOutput(new ExecCnfgBrowseOutput());
		execCnfg.getExecInfo(starBrowseResponse);
		
		if(starBrowseResponse.output.fldTblExec.size() > 0)
		{
			if(starBrowseResponse.output.fldTblExec.get(0).getClasses().contains("LIV"))
			{
				prop = "PROD";
			}
			else
			{
				prop = "TEST";
			}
		}
		
		grpHdr = grpHdr + CommonConstants.GRP_HDR_STRT;
		grpHdr = grpHdr + CommonConstants.MSG_ID_STRT;

		/* ADD MSG ID */ grpHdr = grpHdr + msgId;

		grpHdr = grpHdr + CommonConstants.MSG_ID_END;
		grpHdr = grpHdr + CommonConstants.CRE_DTTM_STRT;

		/* ADD DATE TIME */ grpHdr = grpHdr + dateStr;

		grpHdr = grpHdr + CommonConstants.CRE_DTTM_END;
		grpHdr = grpHdr + CommonConstants.AUTH_STN_STRT;

		if (iChkCount == 0) {
			grpHdr = grpHdr + CommonConstants.CODE_STRT;
			grpHdr = grpHdr + code;
			grpHdr = grpHdr + CommonConstants.CODE_END;
		}

		grpHdr = grpHdr + CommonConstants.PROP_STRT;

		/* ADD PROPRIEATRY */grpHdr = grpHdr + prop;

		grpHdr = grpHdr + CommonConstants.PROP_END;
		grpHdr = grpHdr + CommonConstants.AUTH_STN_END;
		grpHdr = grpHdr + CommonConstants.NO_OF_TXN_STRT;

		/* ADD TOTAL TRANSACTIONS */grpHdr = grpHdr + noOfTxn;

		grpHdr = grpHdr + CommonConstants.NO_OF_TXN_END;
		grpHdr = grpHdr + CommonConstants.CONTROL_SUM_STRT;

		/* ADD TOTAL TRANSACTIONS SUM */grpHdr = grpHdr + formatValue(totSum);

		grpHdr = grpHdr + CommonConstants.CONTROL_SUM_END;
		grpHdr = grpHdr + CommonConstants.INI_PARTY_STRT;
		grpHdr = grpHdr + CommonConstants.NM_STRT;

		/* NAME OF THE COMPANY */grpHdr = grpHdr + iniParty;

		grpHdr = grpHdr + CommonConstants.NM_END;
		grpHdr = grpHdr + CommonConstants.ID_STRT;
		grpHdr = grpHdr + CommonConstants.ORG_ID_STRT;
		grpHdr = grpHdr + CommonConstants.OTHER_STRT;
		grpHdr = grpHdr + CommonConstants.ID_STRT;

		/* ID OF THE COMPANY */grpHdr = grpHdr + iniPartyId;

		grpHdr = grpHdr + CommonConstants.ID_END;
		grpHdr = grpHdr + CommonConstants.OTHER_END;
		grpHdr = grpHdr + CommonConstants.ORG_ID_END;
		grpHdr = grpHdr + CommonConstants.ID_END;
		grpHdr = grpHdr + CommonConstants.INI_PARTY_END;
		grpHdr = grpHdr + CommonConstants.GRP_HDR_END;

		return grpHdr;
	}

	/* CREATE PAYMENT INFORMATION */

	public String getPaymentInfo(Session session, ElecPaySeq elecPaySeq, CompanyAddress address, GetACHDetailsDAO detailsDAO,
			CompanyBankInfo bankInfo, String createdDt, InvoiceInfo info, PaymentTotals paymentTotals) {

		String payInfo = "";
		String paymentId = "";
		String payMthd = "TRF";
		String noOfTxn = "";
		String trnSum = "";
		String svcLvlCode = "";
		String executionDt = createdDt;
		String cmpyNm = address.getCmpyNm().toUpperCase();
		String debAcctId = bankInfo.getAcctNo();
		String cry = "USD";
		String mmbId = bankInfo.getBnkMmbId();
		String cty = "US";
		int iACHFlg = 0;
		int iChkFlg = 0;
		String catPurpose = "VENDOR PAY";

		if (info.getPayMthd().equals("WIR")) {
			svcLvlCode = "URGP";

			trnSum = String.valueOf(paymentTotals.getTotWireAmount());
			noOfTxn = String.valueOf(paymentTotals.getiWireCount());

			paymentId = String.valueOf(elecPaySeq.getPmtIdWir());

			detailsDAO.updateWireSeq(session, elecPaySeq.getPmtIdWir());

		} else if (info.getPayMthd().equals("PPD") || info.getPayMthd().equals("CCD")
				|| info.getPayMthd().equals("CTX")) {
			svcLvlCode = "NURG";

			paymentId = String.valueOf(elecPaySeq.getPmtIdAch());

			trnSum = String.valueOf(paymentTotals.getTotACHAmount());
			noOfTxn = String.valueOf(paymentTotals.getiACHCount());

			iACHFlg = 1;

			detailsDAO.updateACHSeq(session, elecPaySeq.getPmtIdAch());

		} else if (info.getPayMthd().equals("CHK")) {
			trnSum = String.valueOf(paymentTotals.getTotChkAmount());
			noOfTxn = String.valueOf(paymentTotals.getiChkCount());

			paymentId = String.valueOf(elecPaySeq.getPmtIdChk());

			detailsDAO.updateChkSeq(session, elecPaySeq.getPmtIdChk());

			iChkFlg = 1;
		}

		payInfo = payInfo + CommonConstants.PMNT_INF_ID_STRT;
		/* ADD UNIQUE PAYMENT ID */ payInfo = payInfo + paymentId;
		payInfo = payInfo + CommonConstants.PMNT_INF_ID_END;
		payInfo = payInfo + CommonConstants.PMNT_MTHD_STRT;
		/* PAYMENT METHOD - TRF */ payInfo = payInfo + payMthd;
		payInfo = payInfo + CommonConstants.PMNT_MTHD_END;
		payInfo = payInfo + CommonConstants.NO_OF_TXN_STRT;
		/* NO OF TRANSACTIONS */payInfo = payInfo + noOfTxn;
		payInfo = payInfo + CommonConstants.NO_OF_TXN_END;
		payInfo = payInfo + CommonConstants.CONTROL_SUM_STRT;
		/* NO OF TRANSACTIONS SUM */payInfo = payInfo + formatValue(trnSum);
		payInfo = payInfo + CommonConstants.CONTROL_SUM_END;

		payInfo = payInfo + CommonConstants.PMNT_TYP_STRT;

		if (iChkFlg != 1) {
			payInfo = payInfo + CommonConstants.SVC_LVL_STRT;
			payInfo = payInfo + CommonConstants.CODE_STRT;
			/* SERVICE LEVEL CODE BASED ON PAYMENT METHOD */ payInfo = payInfo + svcLvlCode;
			payInfo = payInfo + CommonConstants.CODE_END;
			payInfo = payInfo + CommonConstants.SVC_LVL_END;
		}

		if (iACHFlg == 1) {
			payInfo = payInfo + CommonConstants.LCL_INSTRM_STRT;
			payInfo = payInfo + CommonConstants.CODE_STRT;
			payInfo = payInfo + info.getPayMthd();
			payInfo = payInfo + CommonConstants.CODE_END;
			payInfo = payInfo + CommonConstants.LCL_INSTRM_END;
			payInfo = payInfo + CommonConstants.CTG_PRPS_STRT;
			payInfo = payInfo + CommonConstants.PROP_STRT;
			payInfo = payInfo + catPurpose;
			payInfo = payInfo + CommonConstants.PROP_END;
			payInfo = payInfo + CommonConstants.CTG_PRPS_END;

		}

		if (iChkFlg == 1) {
			payInfo = payInfo + CommonConstants.LCL_INSTRM_STRT;
			payInfo = payInfo + CommonConstants.PROP_STRT;
			payInfo = payInfo + info.getPayMthd();
			payInfo = payInfo + CommonConstants.PROP_END;
			payInfo = payInfo + CommonConstants.LCL_INSTRM_END;
		}

		payInfo = payInfo + CommonConstants.PMNT_TYP_END;
		payInfo = payInfo + CommonConstants.READ_EXEC_DT_STRT;
		/* EXECUTION DATE */ payInfo = payInfo + executionDt;
		payInfo = payInfo + CommonConstants.READ_EXEC_DT_END;

		/* DEBATOR INFORMATION */
		payInfo = payInfo + CommonConstants.DEBTR_STRT;
		payInfo = payInfo + CommonConstants.NM_STRT;
		/* COMPANY NAME */ payInfo = payInfo + cmpyNm;
		payInfo = payInfo + CommonConstants.NM_END;
		payInfo = payInfo + CommonConstants.PSTL_ADDR_STRT;

		/* COMPANY POSTAL ADDRESS */
		if (address.getTownNm().length() > 0) {
			payInfo = payInfo + CommonConstants.STRT_NM_STRT;
			payInfo = payInfo + address.getTownNm();
			payInfo = payInfo + CommonConstants.STRT_NM_END;
		}

		if (address.getPostalCode().length() > 0) {
			payInfo = payInfo + CommonConstants.PSTL_CD_STRT;
			payInfo = payInfo + address.getPostalCode();
			payInfo = payInfo + CommonConstants.PSTL_CD_END;
		}

		if (address.getTownNm().length() > 0) {
			payInfo = payInfo + CommonConstants.TWN_NM_STRT;
			payInfo = payInfo + address.getTownNm().toUpperCase();
			payInfo = payInfo + CommonConstants.TWN_NM_END;
		}

		if (address.getCtrySubDvsn().length() > 0) {
			payInfo = payInfo + CommonConstants.CNTRY_SUB_DVSN_STRT;
			payInfo = payInfo + address.getCtrySubDvsn().toUpperCase();
			payInfo = payInfo + CommonConstants.CNTRY_SUB_DVSN_END;
		}

		if (address.getCty().length() > 0) {
			payInfo = payInfo + CommonConstants.CNTRY_STRT;
			payInfo = payInfo + address.getCty().toUpperCase().substring(0, 2);
			payInfo = payInfo + CommonConstants.CNTRY_END;
		}

		if (address.getAddr().length() > 0) {
			payInfo = payInfo + CommonConstants.ADDR_LINE_STRT;
			payInfo = payInfo + address.getAddr().toUpperCase();
			payInfo = payInfo + CommonConstants.ADDR_LINE_END;
		}

		payInfo = payInfo + CommonConstants.PSTL_ADDR_END;

		/* ADD ORIGIN ID IF PAYMENT TYPE IS ACH */
		if (info.getPayMthd().equals("PPD") || info.getPayMthd().equals("CCD") || info.getPayMthd().equals("CTX")) {
			payInfo = payInfo + CommonConstants.ID_STRT;
			payInfo = payInfo + CommonConstants.ORG_ID_STRT;
			payInfo = payInfo + CommonConstants.OTHER_STRT;
			payInfo = payInfo + CommonConstants.ID_STRT;
			payInfo = payInfo + bankInfo.getBnkAchId();
			payInfo = payInfo + CommonConstants.ID_END;
			payInfo = payInfo + CommonConstants.OTHER_END;
			payInfo = payInfo + CommonConstants.ORG_ID_END;
			payInfo = payInfo + CommonConstants.ID_END;
		}

		payInfo = payInfo + CommonConstants.DEBTR_END;

		/* DEBATOR ACCOUNT */
		payInfo = payInfo + CommonConstants.DBTR_ACCT_STRT;
		payInfo = payInfo + CommonConstants.ID_STRT;
		payInfo = payInfo + CommonConstants.OTHER_STRT;
		payInfo = payInfo + CommonConstants.ID_STRT;
		/* DEBATOR ACCOUNT ID */ payInfo = payInfo + debAcctId;
		payInfo = payInfo + CommonConstants.ID_END;
		payInfo = payInfo + CommonConstants.OTHER_END;
		payInfo = payInfo + CommonConstants.ID_END;

		if (!info.getPayMthd().equals("CHK")) {
			payInfo = payInfo + CommonConstants.CCY_STRT;
			/* CURRENCY INFO */ payInfo = payInfo + cry;
			payInfo = payInfo + CommonConstants.CCY_END;
		}
		payInfo = payInfo + CommonConstants.DBTR_ACCT_END;

		/* DEBATOR AGENT */
		payInfo = payInfo + CommonConstants.DBTR_AGNT_STRT;
		payInfo = payInfo + CommonConstants.FIN_INSTN_ID_STRT;
		payInfo = payInfo + CommonConstants.CLR_SYS_MMB_ID_STRT;
		payInfo = payInfo + CommonConstants.MMB_ID_STRT;
		/* MEMBER ID AGENT */ payInfo = payInfo + mmbId;
		payInfo = payInfo + CommonConstants.MMB_ID_END;
		payInfo = payInfo + CommonConstants.CLR_SYS_MMB_ID_END;
		payInfo = payInfo + CommonConstants.PSTL_ADDR_STRT;
		payInfo = payInfo + CommonConstants.CNTRY_STRT;
		/* COUNTRY */ payInfo = payInfo + cty;
		payInfo = payInfo + CommonConstants.CNTRY_END;
		payInfo = payInfo + CommonConstants.PSTL_ADDR_END;
		payInfo = payInfo + CommonConstants.FIN_INSTN_ID_END;
		payInfo = payInfo + CommonConstants.DBTR_AGNT_END;

		if (iChkFlg == 1) {
			payInfo = payInfo + CommonConstants.DBTR_AGNT_ACT_STRT;
			payInfo = payInfo + CommonConstants.ID_STRT;
			payInfo = payInfo + CommonConstants.OTHER_STRT;
			payInfo = payInfo + CommonConstants.ID_STRT;
			payInfo = payInfo + "PL06694951";
			payInfo = payInfo + CommonConstants.ID_END;
			payInfo = payInfo + CommonConstants.OTHER_END;
			payInfo = payInfo + CommonConstants.ID_END;
			payInfo = payInfo + CommonConstants.DBTR_AGNT_ACT_END;
		}

		return payInfo;
	}

	/* CREATE TRANSACTION INFO BASED ON INVOICES */

	/* GET TRANSACTION DETAILS BY WIRE PAYMENT */
	public String getTransationsInfo(Session session, ElecPaySeq elecPaySeq, String year, GetACHDetailsDAO detailsDAO,
			InvoiceInfo vchrInfo) {

		String trnInfo = "";
		String instrId = "";
		String endToEndId = "";

		elecPaySeq = detailsDAO.getPaySequence();

		if (vchrInfo.getPayMthd().equals("WIR")) {
			
			String trnId = "";
			
			if(tmpInstrWirACHId == 0)
			{
				tmpInstrWirACHId = elecPaySeq.getInstrIdWire();
			}
			
			if (!tmpVendorId.equals(vchrInfo.getVchrVenId()) && tmpVendorId.length() > 0) {
			
				if(tmpInstrWirACHId != 0)
				{
					tmpInstrWirACHId = tmpInstrWirACHId + 1;
				}	
			}
			
			trnId = String.valueOf(tmpInstrWirACHId);
			instrId = "01-" + trnId + year;

			endToEndId = trnId;

			detailsDAO.updateInstrWireSeq(session, tmpInstrWirACHId);
			
			/*
			 * trnId = String.valueOf(elecPaySeq.getInstrIdWire());
			 * 
			 * instrId = "01-" + trnId + year;
			 * 
			 * detailsDAO.updateInstrWireSeq(elecPaySeq.getInstrIdWire());
			 * 
			 * endToEndId = trnId;
			 */

			/*instrId = vchrInfo.getVchrInvNo() + year;
			endToEndId = vchrInfo.getVchrPfx() + "-" + vchrInfo.getVchrNo() + year;*/

		} else if (vchrInfo.getPayMthd().equals("PPD") || vchrInfo.getPayMthd().equals("CCD")
				|| vchrInfo.getPayMthd().equals("CTX")) {
			
			String trnId = "";
			
			if(tmpInstrWirACHId == 0)
			{
				tmpInstrWirACHId = elecPaySeq.getInstrIdWire();
			}
			
			if (!tmpVendorId.equals(vchrInfo.getVchrVenId()) && tmpVendorId.length() > 0) {
			
				if(tmpInstrWirACHId != 0)
				{
					tmpInstrWirACHId = tmpInstrWirACHId + 1;
				}	
			}
			
			trnId = String.valueOf(tmpInstrWirACHId);
			instrId = "11-" + trnId + year;

			endToEndId = "NOTPROVIDED";

			detailsDAO.updateInstrWireSeq(session, tmpInstrWirACHId);
			
			/*
			 * trnId = String.valueOf(elecPaySeq.getInstrIdACH()); instrId = "01-" + trnId +
			 * year;
			 * 
			 * endToEndId = "NOTPROVIDED";
			 * detailsDAO.updateInstrACHSeq(elecPaySeq.getInstrIdACH());
			 */

			/*instrId = vchrInfo.getVchrInvNo() + year;
			endToEndId = vchrInfo.getVchrPfx() + "-" + vchrInfo.getVchrNo() + year;*/

		} else if (vchrInfo.getPayMthd().equals("CHK")) {

			/*
			 * trnId = String.valueOf(elecPaySeq.getInstrIdChk()); instrId = "01-" + trnId +
			 * year;
			 * 
			 * endToEndId = "NOTPROVIDED";
			 * 
			 * detailsDAO.updateInstrChkSeq(elecPaySeq.getInstrIdChk());
			 */

			instrId = vchrInfo.getVchrInvNo() + year;
			endToEndId = vchrInfo.getVchrPfx() + "-" + vchrInfo.getVchrNo() + year;
		}

		VendorAddress vendorAddress = detailsDAO.getVendorAddress(vchrInfo.getCmpyId(), vchrInfo.getVchrVenId());

		VendorBankInfo bankInfo = detailsDAO.getBankInfoByVendor(vchrInfo.getCmpyId(), vchrInfo.getVchrVenId());

		trnInfo = trnInfo + CommonConstants.CREDIT_TRF_TRN_STRT;
		trnInfo = trnInfo + CommonConstants.PMT_ID_STRT;
		trnInfo = trnInfo + CommonConstants.INSTR_ID_STRT;
		/* INSTR ID */trnInfo = trnInfo + instrId;
		trnInfo = trnInfo + CommonConstants.INSTR_ID_END;
		trnInfo = trnInfo + CommonConstants.END_TO_END_ID_STRT;
		/* END TO END ID */trnInfo = trnInfo + endToEndId;
		trnInfo = trnInfo + CommonConstants.END_TO_END_ID_END;
		trnInfo = trnInfo + CommonConstants.PMT_ID_END;
		trnInfo = trnInfo + CommonConstants.AMT_STRT;

		trnInfo = trnInfo + (CommonConstants.INSTRUC_AMT_STRT).replace(">", " Ccy=\"" + vchrInfo.getVchrCry() + "\">");
		double vchrFinalAmt = vchrInfo.getVchrAmt() - vchrInfo.getVchrDiscAmt();
		trnInfo = trnInfo + formatValue(String.valueOf(vchrFinalAmt));
		trnInfo = trnInfo + CommonConstants.INSTRUC_AMT_END;

		trnInfo = trnInfo + CommonConstants.AMT_END;

		if (vchrInfo.getPayMthd().equals("WIR") || vchrInfo.getPayMthd().equals("PPD")
				|| vchrInfo.getPayMthd().equals("CCD") || vchrInfo.getPayMthd().equals("CTX")) {
			trnInfo = trnInfo + CommonConstants.CRED_AGNT_STRT;
			trnInfo = trnInfo + CommonConstants.FIN_INSTN_ID_STRT;
			trnInfo = trnInfo + CommonConstants.CLR_SYS_MMB_ID_STRT;
			trnInfo = trnInfo + CommonConstants.CLEARING_SYS_ID_STRT;
			trnInfo = trnInfo + CommonConstants.CODE_STRT;
			/* CODE VALUE */trnInfo = trnInfo + "USABA";
			trnInfo = trnInfo + CommonConstants.CODE_END;
			trnInfo = trnInfo + CommonConstants.CLEARING_SYS_ID_END;
			trnInfo = trnInfo + CommonConstants.MMB_ID_STRT;
			trnInfo = trnInfo + bankInfo.getBnkRoutingNo();
			trnInfo = trnInfo + CommonConstants.MMB_ID_END;
			trnInfo = trnInfo + CommonConstants.CLR_SYS_MMB_ID_END;
			trnInfo = trnInfo + CommonConstants.NM_STRT;
			/* VENDOR BANK NAME */trnInfo = trnInfo + bankInfo.getBnkNm();
			trnInfo = trnInfo + CommonConstants.NM_END;
			trnInfo = trnInfo + CommonConstants.PSTL_ADDR_STRT;
			trnInfo = trnInfo + CommonConstants.CNTRY_STRT;
			/* COUNTRY CODE */trnInfo = trnInfo + "US";
			trnInfo = trnInfo + CommonConstants.CNTRY_END;
			trnInfo = trnInfo + CommonConstants.PSTL_ADDR_END;
			trnInfo = trnInfo + CommonConstants.FIN_INSTN_ID_END;
			trnInfo = trnInfo + CommonConstants.CRED_AGNT_END;
		} else if (vchrInfo.getPayMthd().equals("CHK")) {

			trnInfo = trnInfo + CommonConstants.CHK_INSTR_STRT;
			trnInfo = trnInfo + CommonConstants.CHK_NB_STRT;
			trnInfo = trnInfo + CommonConstants.CHK_NB_END;
			trnInfo = trnInfo + CommonConstants.DLVRY_MTHD_STRT;
			trnInfo = trnInfo + CommonConstants.PROP_STRT;
			trnInfo = trnInfo + "001";
			trnInfo = trnInfo + CommonConstants.PROP_END;
			trnInfo = trnInfo + CommonConstants.DLVRY_MTHD_END;
			trnInfo = trnInfo + CommonConstants.CHK_INSTR_END;

		}

		trnInfo = trnInfo + CommonConstants.CREDITOR_STRT;
		trnInfo = trnInfo + CommonConstants.NM_STRT;
		/* VENDOR NAME */trnInfo = trnInfo + vendorAddress.getVendorNm().toUpperCase();
		trnInfo = trnInfo + CommonConstants.NM_END;

		/* POSTAl ADDRESS */
		trnInfo = trnInfo + CommonConstants.PSTL_ADDR_STRT;

		if (vendorAddress.getPostalCode().length() > 0) {
			trnInfo = trnInfo + CommonConstants.PSTL_CD_STRT;
			trnInfo = trnInfo + vendorAddress.getPostalCode();
			trnInfo = trnInfo + CommonConstants.PSTL_CD_END;
		}

		if (vendorAddress.getTownNm().length() > 0) {
			trnInfo = trnInfo + CommonConstants.TWN_NM_STRT;
			trnInfo = trnInfo + vendorAddress.getTownNm().toUpperCase();
			trnInfo = trnInfo + CommonConstants.TWN_NM_END;
		}

		if (vendorAddress.getCtrySubDvsn().length() > 0) {
			trnInfo = trnInfo + CommonConstants.CNTRY_SUB_DVSN_STRT;
			trnInfo = trnInfo + vendorAddress.getCtrySubDvsn().toUpperCase();
			trnInfo = trnInfo + CommonConstants.CNTRY_SUB_DVSN_END;
		}

		if (vendorAddress.getCty().length() > 0) {
			trnInfo = trnInfo + CommonConstants.CNTRY_STRT;
			trnInfo = trnInfo + vendorAddress.getCty().toUpperCase().substring(0, 2);
			trnInfo = trnInfo + CommonConstants.CNTRY_END;
		}

		if (vendorAddress.getAddr().length() > 0) {
			trnInfo = trnInfo + CommonConstants.ADDR_LINE_STRT;
			trnInfo = trnInfo + vendorAddress.getAddr().toUpperCase();
			trnInfo = trnInfo + CommonConstants.ADDR_LINE_END;
		}

		trnInfo = trnInfo + CommonConstants.PSTL_ADDR_END;

		trnInfo = trnInfo + CommonConstants.CREDITOR_END;
		trnInfo = trnInfo + CommonConstants.CREDITOR_ACCT_STRT;
		trnInfo = trnInfo + CommonConstants.ID_STRT;
		trnInfo = trnInfo + CommonConstants.OTHER_STRT;
		trnInfo = trnInfo + CommonConstants.ID_STRT;
		/* VENDOR ID USED IN BANK */trnInfo = trnInfo + bankInfo.getBnkAcctNo();
		trnInfo = trnInfo + CommonConstants.ID_END;
		trnInfo = trnInfo + CommonConstants.OTHER_END;
		trnInfo = trnInfo + CommonConstants.ID_END;

		if (vchrInfo.getPayMthd().equals("PPD") || vchrInfo.getPayMthd().equals("CCD")
				|| vchrInfo.getPayMthd().equals("CTX")) {

			trnInfo = trnInfo + CommonConstants.TP_STRT;
			trnInfo = trnInfo + CommonConstants.PROP_STRT;
			trnInfo = trnInfo + "22";
			trnInfo = trnInfo + CommonConstants.PROP_END;
			trnInfo = trnInfo + CommonConstants.TP_END;
		}

		trnInfo = trnInfo + CommonConstants.CREDITOR_ACCT_END;

		trnInfo = trnInfo + CommonConstants.RMT_INF_STRT;

		if (vchrInfo.getPayMthd().equals("WIR") || vchrInfo.getPayMthd().equals("PPD")
				|| vchrInfo.getPayMthd().equals("CCD") || vchrInfo.getPayMthd().equals("CTX")) {

			trnInfo = trnInfo + CommonConstants.UNSTRUCTURED_STRT;
			/* REFERENCE INFO */ 
			/*trnInfo = trnInfo + vchrInfo.getVchrPfx() + "-" + vchrInfo.getVchrNo() + "/" + vchrInfo.getVchrInvNo() + "/" + vchrInfo.getNotesToPayee() + "/";*/
			
			trnInfo = trnInfo + vchrInfo.getVchrNo();
			
			trnInfo = trnInfo + CommonConstants.UNSTRUCTURED_END;
		} else {
			trnInfo = trnInfo + CommonConstants.STRD_STRT;

			trnInfo = trnInfo + CommonConstants.REF_DOC_INFO_STRT;

			trnInfo = trnInfo + CommonConstants.TP_STRT;

			trnInfo = trnInfo + CommonConstants.CD_PRTRY_STRT;

			trnInfo = trnInfo + CommonConstants.CODE_STRT;

			trnInfo = trnInfo + "CINV"; /* FIXED FOR CHK PAYMENT TYPE */

			trnInfo = trnInfo + CommonConstants.CODE_END;

			trnInfo = trnInfo + CommonConstants.CD_PRTRY_END;

			trnInfo = trnInfo + CommonConstants.TP_END;

			trnInfo = trnInfo + CommonConstants.NB_STRT;

			trnInfo = trnInfo + instrId;

			trnInfo = trnInfo + CommonConstants.NB_END;

			trnInfo = trnInfo + CommonConstants.RLTD_DT_STRT;
			trnInfo = trnInfo + CommonConstants.RLTD_DT_END;

			trnInfo = trnInfo + CommonConstants.REF_DOC_INFO_END;

			trnInfo = trnInfo + CommonConstants.REF_DOC_AMT_STRT;

			trnInfo = trnInfo
					+ (CommonConstants.DUE_PAY_AMT_STRT).replace(">", " Ccy=\"" + vchrInfo.getVchrCry() + "\">");

			trnInfo = trnInfo + formatValue(String.valueOf(vchrInfo.getVchrAmt()));

			trnInfo = trnInfo + CommonConstants.DUE_PAY_AMT_END;

			trnInfo = trnInfo
					+ (CommonConstants.DSCNT_APLD_AMT_STRT).replace(">", " Ccy=\"" + vchrInfo.getVchrCry() + "\">");

			trnInfo = trnInfo + formatValue(String.valueOf(vchrInfo.getVchrDiscAmt()));

			trnInfo = trnInfo + CommonConstants.DSCNT_APLD_AMT_END;

			trnInfo = trnInfo + (CommonConstants.RMTD_AMT_STRT).replace(">", " Ccy=\"" + vchrInfo.getVchrCry() + "\">");

			double rmtAmt = vchrInfo.getVchrAmt() - vchrInfo.getVchrDiscAmt();

			trnInfo = trnInfo + formatValue(String.valueOf(rmtAmt));

			trnInfo = trnInfo + CommonConstants.RMTD_AMT_END;

			trnInfo = trnInfo + CommonConstants.REF_DOC_AMT_END;

			trnInfo = trnInfo + CommonConstants.STRD_END;
		}
		trnInfo = trnInfo + CommonConstants.RMT_INF_END;

		trnInfo = trnInfo + CommonConstants.CREDIT_TRF_TRN_END;
		
		tmpVendorId = vchrInfo.getVchrVenId();
		
		return trnInfo;
	}

	/* GET TRANSACTION DETAILS BY CHECK */
	public String getTransationsInfoChk(Session session, ElecPaySeq elecPaySeq, String year, GetACHDetailsDAO detailsDAO,
			InvoiceInfo vchrInfo, CompanyBankInfo cmpybankInfo, Map<String, Double> vendTotalMap) throws Exception{

		String trnInfo = "";
		String instrId = "";
		String endToEndId = "";
		CommonFunctions commonFunctions = new CommonFunctions();

		elecPaySeq = detailsDAO.getPaySequence();

		if (vchrInfo.getPayMthd().equals("CHK")) {

			String trnId = "";
			
			/* CHANGE FOR UNIQUE INSTR NUMBER FOR EACH TRANSACTION */
			if(tmpInstrChkId == 0)
			{
				tmpInstrChkId = elecPaySeq.getInstrIdChk();
			}
			
			if (!tmpVendorId.equals(vchrInfo.getVchrVenId()) && tmpVendorId.length() > 0) {
			
				if(tmpInstrChkId != 0)
				{
					tmpInstrChkId = tmpInstrChkId + 1;
				}	
			}
			
			trnId = String.valueOf(tmpInstrChkId);
			instrId = "01-" + trnId + year;

			endToEndId = "NOTPROVIDED";

			detailsDAO.updateInstrChkSeq(session, tmpInstrChkId);

			/*
			 * instrId = vchrInfo.getVchrInvNo() + year; endToEndId = vchrInfo.getVchrPfx()
			 * + "-" + vchrInfo.getVchrNo() + year;
			 */
		}

		VendorAddress vendorAddress = detailsDAO.getVendorAddress(vchrInfo.getCmpyId(), vchrInfo.getVchrVenId());

		VendorBankInfo bankInfo = detailsDAO.getBankInfoByVendor(vchrInfo.getCmpyId(), vchrInfo.getVchrVenId());

		if (!tmpVendorId.equals(vchrInfo.getVchrVenId()) && tmpVendorId.length() > 0) {
			trnInfo = trnInfo + CommonConstants.RMT_INF_END;

			trnInfo = trnInfo + CommonConstants.CREDIT_TRF_TRN_END;
		}

		CheckDAO checkDAO = new CheckDAO();
		ChkLotUsage chkLotUsage = new ChkLotUsage();

		if (!tmpVendorId.equals(vchrInfo.getVchrVenId())) {
			trnInfo = trnInfo + CommonConstants.CREDIT_TRF_TRN_STRT;
			trnInfo = trnInfo + CommonConstants.PMT_ID_STRT;
			trnInfo = trnInfo + CommonConstants.INSTR_ID_STRT;
			/* INSTR ID */trnInfo = trnInfo + instrId;
			trnInfo = trnInfo + CommonConstants.INSTR_ID_END;
			trnInfo = trnInfo + CommonConstants.END_TO_END_ID_STRT;
			/* END TO END ID */trnInfo = trnInfo + endToEndId;
			trnInfo = trnInfo + CommonConstants.END_TO_END_ID_END;
			trnInfo = trnInfo + CommonConstants.PMT_ID_END;
			trnInfo = trnInfo + CommonConstants.AMT_STRT;

			trnInfo = trnInfo
					+ (CommonConstants.INSTRUC_AMT_STRT).replace(">", " Ccy=\"" + vchrInfo.getVchrCry() + "\">");
			double vchrFinalAmt = vchrInfo.getVchrAmt() - vchrInfo.getVchrDiscAmt();
			
			trnInfo = trnInfo + formatValue(String.valueOf(vendTotalMap.get(vchrInfo.getVchrVenId())));
			
		//	trnInfo = trnInfo + formatValue(String.valueOf(vchrFinalAmt));
			trnInfo = trnInfo + CommonConstants.INSTRUC_AMT_END;

			trnInfo = trnInfo + CommonConstants.AMT_END;

			trnInfo = trnInfo + CommonConstants.CHK_INSTR_STRT;
			trnInfo = trnInfo + CommonConstants.CHK_NB_STRT;

			chkLotUsage = checkDAO.getCurrentCheckNo(cmpybankInfo.getBnkCode(), cmpybankInfo.getAcctNo());

			/* SEET TEMPRORY CHECK NO VARIABLE TO UPDATE VOUCHER REFERNCE */
			tmpChkNo = chkLotUsage.getCurChk();
			
			/* UPDATE REFERENCE CHECK NUMBER FOR INVOICE */
			checkDAO.updCheckForInvoice(session, vchrInfo.getVchrPfx() + "-" + vchrInfo.getVchrNo(), chkLotUsage.getCurChk());
			
			trnInfo = trnInfo + tmpChkNo; /* CHECK NUMBER */
			
			nextChkNo(session, cmpybankInfo, chkLotUsage);
			
			trnInfo = trnInfo + CommonConstants.CHK_NB_END;
			trnInfo = trnInfo + CommonConstants.DLVRY_MTHD_STRT;
			trnInfo = trnInfo + CommonConstants.PROP_STRT;
			trnInfo = trnInfo + "001";
			trnInfo = trnInfo + CommonConstants.PROP_END;
			trnInfo = trnInfo + CommonConstants.DLVRY_MTHD_END;
			trnInfo = trnInfo + CommonConstants.CHK_INSTR_END;

			trnInfo = trnInfo + CommonConstants.CREDITOR_STRT;
			trnInfo = trnInfo + CommonConstants.NM_STRT;
			/* VENDOR NAME */trnInfo = trnInfo + vendorAddress.getVendorNm().toUpperCase();
			trnInfo = trnInfo + CommonConstants.NM_END;

			/* POSTAl ADDRESS */
			trnInfo = trnInfo + CommonConstants.PSTL_ADDR_STRT;

			if (vendorAddress.getPostalCode().length() > 0) {
				trnInfo = trnInfo + CommonConstants.PSTL_CD_STRT;
				trnInfo = trnInfo + vendorAddress.getPostalCode();
				trnInfo = trnInfo + CommonConstants.PSTL_CD_END;
			}

			if (vendorAddress.getTownNm().length() > 0) {
				trnInfo = trnInfo + CommonConstants.TWN_NM_STRT;
				trnInfo = trnInfo + vendorAddress.getTownNm().toUpperCase();
				trnInfo = trnInfo + CommonConstants.TWN_NM_END;
			}

			if (vendorAddress.getCtrySubDvsn().length() > 0) {
				trnInfo = trnInfo + CommonConstants.CNTRY_SUB_DVSN_STRT;
				trnInfo = trnInfo + vendorAddress.getCtrySubDvsn().toUpperCase();
				trnInfo = trnInfo + CommonConstants.CNTRY_SUB_DVSN_END;
			}

			if (vendorAddress.getCty().length() > 0) {
				trnInfo = trnInfo + CommonConstants.CNTRY_STRT;
				trnInfo = trnInfo + vendorAddress.getCty().toUpperCase().substring(0, 2);
				trnInfo = trnInfo + CommonConstants.CNTRY_END;
			}

			if (vendorAddress.getAddr().length() > 0) {
				trnInfo = trnInfo + CommonConstants.ADDR_LINE_STRT;
				trnInfo = trnInfo + vendorAddress.getAddr().toUpperCase();
				trnInfo = trnInfo + CommonConstants.ADDR_LINE_END;
			}

			trnInfo = trnInfo + CommonConstants.PSTL_ADDR_END;

			trnInfo = trnInfo + CommonConstants.CREDITOR_END;
			trnInfo = trnInfo + CommonConstants.CREDITOR_ACCT_STRT;
			trnInfo = trnInfo + CommonConstants.ID_STRT;
			trnInfo = trnInfo + CommonConstants.OTHER_STRT;
			trnInfo = trnInfo + CommonConstants.ID_STRT;
			/* VENDOR ID USED IN BANK */trnInfo = trnInfo + vchrInfo.getVchrVenId();
			trnInfo = trnInfo + CommonConstants.ID_END;
			trnInfo = trnInfo + CommonConstants.OTHER_END;
			trnInfo = trnInfo + CommonConstants.ID_END;

			trnInfo = trnInfo + CommonConstants.CREDITOR_ACCT_END;

			trnInfo = trnInfo + CommonConstants.RMT_INF_STRT;
			trnInfo = trnInfo + CommonConstants.STRD_STRT;
			trnInfo = trnInfo + CommonConstants.REF_DOC_INFO_STRT;
			trnInfo = trnInfo + CommonConstants.TP_STRT;
			trnInfo = trnInfo + CommonConstants.CD_PRTRY_STRT;
			trnInfo = trnInfo + CommonConstants.CODE_STRT;
			trnInfo = trnInfo + "CINV"; /* FIXED FOR CHK PAYMENT TYPE */
			trnInfo = trnInfo + CommonConstants.CODE_END;
			trnInfo = trnInfo + CommonConstants.CD_PRTRY_END;
			trnInfo = trnInfo + CommonConstants.TP_END;
			trnInfo = trnInfo + CommonConstants.NB_STRT;
			
			String temp = commonFunctions.whiteEscapeCharacter(vchrInfo.getVchrInvNo());
			
			trnInfo = trnInfo + temp + year;
			trnInfo = trnInfo + CommonConstants.NB_END;
			trnInfo = trnInfo + CommonConstants.RLTD_DT_STRT;
			trnInfo = trnInfo + vchrInfo.getInvDt();
			trnInfo = trnInfo + CommonConstants.RLTD_DT_END;
			trnInfo = trnInfo + CommonConstants.REF_DOC_INFO_END;
			trnInfo = trnInfo + CommonConstants.REF_DOC_AMT_STRT;
			trnInfo = trnInfo
					+ (CommonConstants.DUE_PAY_AMT_STRT).replace(">", " Ccy=\"" + vchrInfo.getVchrCry() + "\">");
			trnInfo = trnInfo + formatValue(String.valueOf(vchrInfo.getVchrAmt()));
			trnInfo = trnInfo + CommonConstants.DUE_PAY_AMT_END;
			trnInfo = trnInfo
					+ (CommonConstants.DSCNT_APLD_AMT_STRT).replace(">", " Ccy=\"" + vchrInfo.getVchrCry() + "\">");
			trnInfo = trnInfo + formatValue(String.valueOf(vchrInfo.getVchrDiscAmt()));
			trnInfo = trnInfo + CommonConstants.DSCNT_APLD_AMT_END;
			trnInfo = trnInfo + (CommonConstants.RMTD_AMT_STRT).replace(">", " Ccy=\"" + vchrInfo.getVchrCry() + "\">");

			double rmtAmt = vchrInfo.getVchrAmt() - vchrInfo.getVchrDiscAmt();

			trnInfo = trnInfo + formatValue(String.valueOf(rmtAmt));
			trnInfo = trnInfo + CommonConstants.RMTD_AMT_END;
			trnInfo = trnInfo + CommonConstants.REF_DOC_AMT_END;
			trnInfo = trnInfo + CommonConstants.STRD_END;
		} else {
			trnInfo = trnInfo + CommonConstants.STRD_STRT;
			trnInfo = trnInfo + CommonConstants.REF_DOC_INFO_STRT;
			trnInfo = trnInfo + CommonConstants.TP_STRT;
			trnInfo = trnInfo + CommonConstants.CD_PRTRY_STRT;
			trnInfo = trnInfo + CommonConstants.CODE_STRT;
			trnInfo = trnInfo + "CINV"; /* FIXED FOR CHK PAYMENT TYPE */
			trnInfo = trnInfo + CommonConstants.CODE_END;
			trnInfo = trnInfo + CommonConstants.CD_PRTRY_END;
			trnInfo = trnInfo + CommonConstants.TP_END;
			trnInfo = trnInfo + CommonConstants.NB_STRT;
			
			String temp = commonFunctions.whiteEscapeCharacter(vchrInfo.getVchrInvNo());
			
			trnInfo = trnInfo + temp + year;
			trnInfo = trnInfo + CommonConstants.NB_END;
			trnInfo = trnInfo + CommonConstants.RLTD_DT_STRT;
			trnInfo = trnInfo + vchrInfo.getInvDt();
			trnInfo = trnInfo + CommonConstants.RLTD_DT_END;
			trnInfo = trnInfo + CommonConstants.REF_DOC_INFO_END;
			trnInfo = trnInfo + CommonConstants.REF_DOC_AMT_STRT;
			trnInfo = trnInfo
					+ (CommonConstants.DUE_PAY_AMT_STRT).replace(">", " Ccy=\"" + vchrInfo.getVchrCry() + "\">");
			trnInfo = trnInfo + formatValue(String.valueOf(vchrInfo.getVchrAmt()));
			trnInfo = trnInfo + CommonConstants.DUE_PAY_AMT_END;
			trnInfo = trnInfo
					+ (CommonConstants.DSCNT_APLD_AMT_STRT).replace(">", " Ccy=\"" + vchrInfo.getVchrCry() + "\">");
			trnInfo = trnInfo + formatValue(String.valueOf(vchrInfo.getVchrDiscAmt()));
			trnInfo = trnInfo + CommonConstants.DSCNT_APLD_AMT_END;
			trnInfo = trnInfo + (CommonConstants.RMTD_AMT_STRT).replace(">", " Ccy=\"" + vchrInfo.getVchrCry() + "\">");

			double rmtAmt = vchrInfo.getVchrAmt() - vchrInfo.getVchrDiscAmt();

			trnInfo = trnInfo + formatValue(String.valueOf(rmtAmt));
			trnInfo = trnInfo + CommonConstants.RMTD_AMT_END;
			trnInfo = trnInfo + CommonConstants.REF_DOC_AMT_END;
			trnInfo = trnInfo + CommonConstants.STRD_END;

			/* UPDATE REFERENCE CHECK NUMBER FOR INVOICE */
			checkDAO.updCheckForInvoice(session, vchrInfo.getVchrPfx() + "-" + vchrInfo.getVchrNo(), tmpChkNo);
		}

		tmpVendorId = vchrInfo.getVchrVenId();

		return trnInfo;
	}

	/* GET NEXT CHECK NUMBER BASED ON CURRENT ONE */
	private void nextChkNo(Session session,CompanyBankInfo bankInfo, ChkLotUsage checkInfo) throws Exception {
		CheckDAO checkDAO = new CheckDAO();

		String rtnValue = "";
		int orgLngth = checkInfo.getCurChk().length();

		int newChckvalue = Integer.parseInt(checkInfo.getCurChk()) + 1;

		while (1 == 1) {
			int iFlg = checkDAO.validateVoidCheck(bankInfo.getBnkCode(), bankInfo.getAcctNo(),
					String.valueOf(newChckvalue));

			if (iFlg == 0) {
				break;
			} else {
				newChckvalue = newChckvalue + 1;
			}
		}

		if (String.valueOf(newChckvalue).length() == orgLngth) {
			rtnValue = String.valueOf(newChckvalue);
		} else {
			for (int i = 0; i < orgLngth - String.valueOf(newChckvalue).length(); i++) {
				rtnValue = rtnValue + "0";
			}

			rtnValue = rtnValue + String.valueOf(newChckvalue);
		}

		checkInfo.setCurChk(rtnValue);

		checkDAO.updCheckNo(checkInfo);

	}

	/* FORMAT VALUE TO TWO DECIMAL PLACES */
	private String formatValue(String amount) {
		DecimalFormat df = new DecimalFormat("#.00");
		String formattedValue = df.format(Double.parseDouble(amount));

		formattedValue = String.format("%.2f", Double.parseDouble(amount));

		return formattedValue;
	}

	private List<String> getDistinctVendors(List<InvoiceInfo> info) {
		List<String> venList = new ArrayList<String>();

		for (int i = 0; i < info.size(); i++) {
			if (!venList.contains(info.get(i).getVchrVenId())) {
				venList.add(info.get(i).getVchrVenId());
			}
		}

		return venList;
	}

	
	private List<InvoiceInfo> getDistinctVendorInfo(List<InvoiceInfo> info)
	{
		double totalAmnt = 0.0;
		String vchrInvNo = "";
		String vchrRef = "";
		String vchrPayMthd = "";
		String vchrCry = "";
		String cmpyId = "";
		String vchrNotes = "";
		List<InvoiceInfo> invTotals = new ArrayList<InvoiceInfo>();
		CommonFunctions functions = new CommonFunctions();
		
		List<String> distVenList = getDistinctVendors(info);
		
		for(int i =0; i < distVenList.size(); i++)
		{
			InvoiceInfo invoiceInfo = new InvoiceInfo();
			totalAmnt = 0.0;
			vchrInvNo = "";
			vchrRef = "";
			for(int j =0; j < info.size(); j++)
			{
				if(info.get(j).getVchrVenId().equals(distVenList.get(i)))
				{
					totalAmnt = totalAmnt + info.get(j).getVchrAmt() - info.get(j).getVchrDiscAmt();
					vchrInvNo = vchrInvNo + info.get(j).getVchrInvNo() + " ";
					vchrRef = vchrRef + info.get(j).getVchrPfx() + info.get(j).getVchrNo() ;
					
					if(info.get(j).getVchrInvNo() != null)
					{
						if(info.get(j).getVchrInvNo().trim().length() > 0)
						{
							vchrRef = vchrRef + " " + info.get(j).getVchrInvNo().trim();
						}
					}
					
					if(info.get(j).getNotesToPayee() != null)
					{
						if(info.get(j).getNotesToPayee().trim().length() > 0)
						{
							vchrRef = vchrRef + " " + info.get(j).getNotesToPayee().trim();
						}
					}
					
					vchrRef = vchrRef + "/";
					
					vchrPayMthd = info.get(j).getPayMthd();
					vchrCry = info.get(j).getVchrCry();
					cmpyId = info.get(j).getCmpyId();
				}
			}
						
			invoiceInfo.setCmpyId(cmpyId);
			invoiceInfo.setVchrVenId(distVenList.get(i));
			invoiceInfo.setVchrAmt(totalAmnt);
			
			String temp = functions.whiteEscapeCharacter(vchrInvNo);
			if(temp.length() > 140)
			{
				temp = temp.substring(0,139);
				temp = temp + "/";
			}
			
			invoiceInfo.setVchrInvNo(temp);
			
			temp = functions.whiteEscapeCharacter(vchrRef);
			
			if(temp.length() > 140)
			{
				temp = temp.substring(0,139);
				temp = temp + "/";
			}
			
			invoiceInfo.setVchrNo(temp);
			invoiceInfo.setPayMthd(vchrPayMthd);
			invoiceInfo.setVchrCry(vchrCry);
			
			invTotals.add(invoiceInfo);
			
		}
		
		return invTotals;
	}
	


}
