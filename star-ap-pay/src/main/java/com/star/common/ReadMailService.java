package com.star.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Properties;
import java.util.TimeZone;

import javax.mail.Address;
import javax.mail.BodyPart;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.NoSuchProviderException;
import javax.mail.Part;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.URLName;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.search.AndTerm;
import javax.mail.search.ComparisonTerm;
import javax.mail.search.DateTerm;
import javax.mail.search.FromTerm;
import javax.mail.search.ReceivedDateTerm;
import javax.mail.search.SearchTerm;
import javax.mail.search.SentDateTerm;
import javax.mail.search.SubjectTerm;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.dao.UsrEmlConfigDAO;
import com.star.linkage.eml.EmailConfigMan;
import com.star.modal.UsrEmlConfigDtls;

/**
 * This class is used to receive email with attachment.
 * 
 * @author w3spoint
 */
public class ReadMailService {
	
	private static Logger logger = LoggerFactory.getLogger(ReadMailService.class);
	
	public void receiveEmail(String pop3Host, String mailStoreType, String userName, String password) {
		// Set properties
		Properties props = new Properties();
		props.put("mail.store.protocol", "imaps");
		props.put("mail.imap.host", pop3Host);
		props.put("mail.imap.port", "995");
		// props.put("mail.pop3.starttls.enable", "true");

		// Get the Session object.
		Session session = Session.getInstance(props);

		try {
			// Create the POP3 store object and connect to the pop store.
			Store store = session.getStore("imap");
			store.connect(pop3Host, userName, password);

			SearchTerm searchTerm = new FromTerm(new InternetAddress("abizarh@starsoftware.co"));

			SearchTerm searchTerm2 = new SubjectTerm("Invoice");

			SearchTerm searchTerm4 = new ReceivedDateTerm(DateTerm.LE, new Date());

			SearchTerm[] searchTermArry = null;

			searchTermArry = new SearchTerm[3];

			searchTermArry[0] = searchTerm;
			searchTermArry[1] = searchTerm2;
			searchTermArry[2] = searchTerm4;

			SearchTerm searchTerm3 = new AndTerm(searchTermArry);

			SearchTerm term = null;
			Calendar cal = null;
			cal = Calendar.getInstance();
			Date minDate = new Date(cal.getTimeInMillis()); // get today date

			cal.add(Calendar.DAY_OF_MONTH, 1); // add 1 day
			Date maxDate = new Date(cal.getTimeInMillis()); // get tomorrow date
			ReceivedDateTerm minDateTerm = new ReceivedDateTerm(ComparisonTerm.GE, minDate);
			ReceivedDateTerm maxDateTerm = new ReceivedDateTerm(ComparisonTerm.LE, maxDate);

			term = new AndTerm(minDateTerm, maxDateTerm); // concat the search terms
			// term = new AndTerm(term, maxDateTerm);

			
			logger.debug("MAX" + maxDate);
			
			logger.debug("MIN" + minDate);
			
			// Create the folder object and open it in your mailbox.
			Folder emailFolder = store.getFolder("INBOX");
			emailFolder.open(Folder.READ_ONLY);

			// Retrieve the messages from the folder object.
			// Message[] messages = emailFolder.getMessages();

			Message[] messages = emailFolder.search(term);

			System.out.println("Total Message" + messages.length);
			
			logger.debug("Total Message" + messages.length);
			
			// Iterate the messages
			for (int i = 0; i < messages.length; i++) {
				Message message = messages[i];

				System.out.println("RECEVICED ---->" + message.getSentDate());

				System.out.println("RECEVICED ---->" + message.getFrom()[0]);
				
				
				logger.debug("RECEVICED ---->" + message.getSentDate());
				
				logger.debug("RECEVICED ---->" + message.getFrom()[0]);
				
				String emlDt = new SimpleDateFormat("yyyy/MM/dd").format(message.getSentDate());

				String curDt = new SimpleDateFormat("yyyy/MM/dd").format(new Date());
				
				logger.debug("RECEVICED ---->" + emlDt);
				
				logger.debug("RECEVICED ---->" + curDt);
				
				
				if (emlDt.equals(curDt)) {

					Address[] toAddress = message.getRecipients(Message.RecipientType.TO);
					System.out.println("---------------------------------");
					System.out.println("Details of Email Message " + (i + 1) + " :");
					System.out.println("Subject: " + message.getSubject());
					System.out.println("From: " + message.getFrom()[0]);

					// Iterate recipients
					System.out.println("To: ");
					for (int j = 0; j < toAddress.length; j++) {
						System.out.println(toAddress[j].toString());
					}

					// Iterate multiparts
					Multipart multipart = (Multipart) message.getContent();
					for (int k = 0; k < multipart.getCount(); k++) {
						BodyPart bodyPart = multipart.getBodyPart(k);
						InputStream stream = (InputStream) bodyPart.getInputStream();
						BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));

						while (bufferedReader.ready()) {
							// System.out.println(bufferedReader.readLine());
						}
						// System.out.println();
					}
				}
			}

			// close the folder and store objects
			emailFolder.close(false);
			store.close();
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public void readEmail(String usr, String chkSum, UsrEmlConfigDtls usrEmlConfig, String usrId, String cmpyId, String emlTyp) {
		Session session = null;
		Store store;
		Folder folder;
		URLName url = null;
		
		if(emlTyp.equals("GML"))
		{
			url = new URLName("imaps", "imap.gmail.com", 993, "INBOX", usr, chkSum);
		}
		else if(emlTyp.equals("EXC"))
		{
			url = new URLName("imaps", "outlook.office365.com", 993, "INBOX", usr, chkSum);
		}

		if (session == null) {
			Properties props = null;
			try {
				props = System.getProperties();

				props.setProperty("mail.imaps.partialfetch", "false");
			} catch (SecurityException sex) {
				props = new Properties();
			}
			session = Session.getInstance(props, null);
		}
		try {
			store = session.getStore(url);

			store.connect();
			folder = store.getFolder(url);

			folder.open(Folder.READ_WRITE);

			SearchTerm term = null;
			Calendar cal = null;
			Calendar minDatCal = null;
			TimeZone.setDefault( TimeZone.getTimeZone("UTC"));
			
			minDatCal = Calendar.getInstance();
			minDatCal.setTimeInMillis(usrEmlConfig.getLstRfshTm());
			
			Date minDate = new Date(minDatCal.getTimeInMillis());
						
			//Date minDate = new Date(usrEmlConfig.getLstRfshTm()); // get today date
			
			cal = Calendar.getInstance();
			cal.add(Calendar.DAY_OF_MONTH, 0); // add 1 day
			Date maxDate = new Date(cal.getTimeInMillis()); // get tomorrow date
			
			
			
			ReceivedDateTerm minDateTerm = new ReceivedDateTerm(ComparisonTerm.GE, minDate);
			ReceivedDateTerm maxDateTerm = new ReceivedDateTerm(ComparisonTerm.LE, maxDate);

			SentDateTerm minDateTerm1 = new SentDateTerm(ComparisonTerm.GE, minDate);
			SentDateTerm maxDateTerm1 = new SentDateTerm(ComparisonTerm.LE, maxDate);
			
			
			logger.debug("MAX" + maxDate);
			
			logger.debug("MIN" + minDate);
			
			
			term = new AndTerm(minDateTerm1, maxDateTerm1);

			Message[] messages = folder.search(term);

			// Message[] messages = folder.getMessages();

			System.out.println(messages.length);
			
			logger.debug("Total Message" + messages.length);

			for (int iCount = 0; iCount < messages.length; iCount++) {

				Message message = messages[iCount];

				Date date = message.getSentDate();
				
				logger.debug("RECEVICED ---->" + message.getSentDate());
				
				if (minDate.getTime() >= date.getTime()) {
					continue;
				}

				System.out.println("RECEVICED ---->" + message.getSentDate());

				System.out.println("RECEVICED ---->" + message.getFrom()[0]);
				
				logger.debug("RECEVICED ---->" + message.getSentDate());
				
				logger.debug("RECEVICED ---->" + message.getFrom()[0]);

				Address[] toAddress = message.getRecipients(Message.RecipientType.TO);
				System.out.println("---------------------------------");
				System.out.println("Details of Email Message " + (iCount + 1) + " :");
				System.out.println("Subject: " + message.getSubject());
				System.out.println("From: " + message.getFrom()[0]);

				// Iterate recipients
				System.out.println("To: ");
				for (int j = 0; j < toAddress.length; j++) {
					System.out.println(toAddress[j].toString());
				}

				// Iterate multiparts

				Multipart multipart = (Multipart) message.getContent();
				for (int k = 0; k < multipart.getCount(); k++) {
					BodyPart bodyPart = multipart.getBodyPart(k);

					MimeBodyPart part = (MimeBodyPart) multipart.getBodyPart(k);

					if (Part.ATTACHMENT.equalsIgnoreCase(part.getDisposition())) {
						// this part is attachment

						// part.saveFile("G://" + File.separator + fileName);

						UpldFileToApp fileToApp = new UpldFileToApp();

						byte[] bytes = IOUtils.toByteArray(part.getInputStream());

						fileToApp.processFile(cmpyId, usrId, "M", part.getFileName(), bytes);

					}
				}

				if (iCount == messages.length - 1) {

					UsrEmlConfigDAO configDAO = new UsrEmlConfigDAO();

					configDAO.updLstRefreshTime(usr, date.getTime());
				}
			}

			// close the folder and store objects
			folder.close(false);
			store.close();

		} catch (NoSuchProviderException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void validateEmail(MaintanenceResponse<EmailConfigMan> starManResponse, String usr, String chkSum, String emlTyp) {
		Session session = null;
		Store store;
		Folder folder;
		URLName url = null;
		
		if(emlTyp.equals("GML"))
		{
			url = new URLName("imaps", "imap.gmail.com", 993, "INBOX", usr, chkSum);
		}
		else if(emlTyp.equals("EXC"))
		{
			url = new URLName("imaps", "outlook.office365.com", 993, "INBOX", usr, chkSum);
		}
		

		if (session == null) {
			Properties props = null;
			try {
				props = System.getProperties();

				props.setProperty("mail.imaps.partialfetch", "false");
			} catch (SecurityException sex) {
				props = new Properties();
			}
			session = Session.getInstance(props, null);
		}
		try {
			store = session.getStore(url);

			store.connect();

			store.close();

		} catch (NoSuchProviderException e) {
			// TODO Auto-generated catch block

			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(e.getMessage());

		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(e.getMessage());
		}

	}
}