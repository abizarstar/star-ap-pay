package com.star.common;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

import com.star.common.abbyy.venTemplate.InvoiceDetailModel;
import com.star.common.abbyy.venTemplate.Vendor;
import com.star.dao.CstmVchrInfoDAO;
import com.star.dao.VendorInfoDAO;
import com.star.linkage.cstmvchrinfo.CstmVchrInfoInput;
import com.star.linkage.vendor.VendorDefaultSettingBrowseOutput;

public class ParseInvoice {
	Vendor vd = new Vendor();
	List<InvoiceDetailModel> idmList = new ArrayList<InvoiceDetailModel>();

	public void parseInvoiceData(String cmpyId, String usrId, String filePath, List<CstmVchrInfoInput> cstmVchrInfos,
			int ctlNo, CstmVchrInfoDAO cstmVchrInfoDAO) {
		List<String> list = new ArrayList<String>();

		File file = new File(CommonConstants.ROOT_DIR + filePath);
		if (file.exists()) {
			try {
				// System.out.println(filePath);
				// list = Files.readAllLines(file.toPath(), Charset.defaultCharset());
				idmList = vd.getVendor(file.toString());

			} catch (Exception ex) {
				ex.printStackTrace();
			}
		}
		String venNm = "ZAC PROCESSORS";
		String invDate = "";
		String invNo = "";
		String dueDate = "";
		String totalDue = "";
		String terms = "";
		String currency = "";
		String[] rel = null;
		String[] ter = null;
		String firstLine = null;
		Boolean flg = false;
		for (String line : list) {
			if (line != null) {
				if (flg == false) {
					firstLine = line.toString().trim();
					Boolean flgChr = false;
					String chr = "";
					Integer j = 0;
					int count = 0;
					for (int i = 3; i < firstLine.length(); i++) {
						chr = String.valueOf(firstLine.charAt(i));
						count = (int) firstLine.charAt(i);
						if (count != 32) {
							venNm = venNm + chr;
							flgChr = true;
							j = 0;
						} else {
							j += 1;
							if (j == 1 && flgChr == true) {
								venNm = venNm + " ";
							}
						}

						if (j >= 2 && flgChr == true) {
							break;
						}

						// j = (int)venNm.charAt(i);
					}

					if (venNm != null) {
						venNm = venNm.trim();
					}
					flg = true;
				}
				rel = line.split(":");
				if (rel.length > 1) {
					if (rel[0].toString().contains("Invoice date")) {
						if (rel[1] != null) {
							invDate = rel[1].toString().trim();
						}
					} else if (rel[0].toString().contains("Invoice number")) {
						if (rel[1] != null) {
							invNo = rel[1].toString().trim();
						}
					} else if (rel[0].toString().contains("Due date")) {
						if (rel[1] != null) {
							dueDate = rel[1].toString().trim();
						}
					} else if (rel[0].toString().contains("Terms")) {
						if (rel[1] != null) {
							terms = rel[1].toString().trim();

							ter = terms.split(";");
							if (ter.length > 1) {
								terms = ter[1].toString().trim();
							}
						}
					} else if (rel[0].toString().contains("Total due")) {

						if (rel[1] != null) {
							totalDue = rel[1].toString().trim();

							Boolean isDigit;
							char c = totalDue.charAt(0);
							isDigit = (c >= '0' && c <= '9');

							if (isDigit == false) {
								if (totalDue.startsWith(".") == false) {
									totalDue = totalDue.substring(1, totalDue.length());
								}
							}
						}
					}
				}
			}
		}

		for (int i = 0; i < idmList.size(); i++) {
			InvoiceDetailModel idm = idmList.get(i);
			CstmVchrInfoInput cstmVchrInfo = new CstmVchrInfoInput();
			
			if (idm != null) {
				List<String> venDtls = cstmVchrInfoDAO.getVenderID(idm.getVendorName());

				CommonFunctions objCom = new CommonFunctions();

				cstmVchrInfo.setVchrCtlNo(ctlNo);
				cstmVchrInfo.setVchrInvNo(idm.getInvoice());
				if (venDtls.size() > 0) {
					cstmVchrInfo.setVchrVenId(venDtls.get(0));

					setVendorAdditionalInfo(cmpyId, usrId, cstmVchrInfo);

					cstmVchrInfo.setVchrVenNm(venDtls.get(1));
				}

				if (cstmVchrInfo.getVchrBrh() == null) {
					cstmVchrInfo.setVchrBrh("");
				}
				cstmVchrInfo.setVchrInvDt(objCom.formatDateWithoutTime(idm.getInvoiceDate()));
				cstmVchrInfo.setVchrDueDt(objCom.formatDateWithoutTime(idm.getDueDate()));
				cstmVchrInfo.setVchrExtRef("");
				cstmVchrInfo.setVchrPayTerm(terms);
				if (idm.getTotalamount().length() > 0) {
					
					if (!isNumeric(idm.getTotalamount().replace(",", ""))) {
						
						cstmVchrInfo.setVchrAmt(0);
					}
					else
					{
						cstmVchrInfo.setVchrAmt(Double.parseDouble(idm.getTotalamount().replace(",", "")));
					}
					
					
				}
				cstmVchrInfo.setVchrCry(idm.getCurrency());
			} else {
				cstmVchrInfo.setVchrCtlNo(ctlNo);
				cstmVchrInfo.setVchrInvNo("");
				cstmVchrInfo.setVchrVenId("");
				cstmVchrInfo.setVchrVenNm("");
				cstmVchrInfo.setVchrBrh("");
				cstmVchrInfo.setVchrExtRef("");
				cstmVchrInfo.setVchrPayTerm(terms);
				cstmVchrInfo.setVchrCry("");
			}
			
			cstmVchrInfos.add(cstmVchrInfo);
		}
		// ------- Data comming from vendor invoice detail-----
		if(idmList.size() == 0)
		{
			CstmVchrInfoInput cstmVchrInfo = new CstmVchrInfoInput();
			
			cstmVchrInfo.setVchrCtlNo(ctlNo);
			cstmVchrInfo.setVchrInvNo("");
			cstmVchrInfo.setVchrVenId("");
			cstmVchrInfo.setVchrVenNm("");
			cstmVchrInfo.setVchrBrh("");
			cstmVchrInfo.setVchrExtRef("");
			cstmVchrInfo.setVchrPayTerm(terms);
			cstmVchrInfo.setVchrCry("");
			
			cstmVchrInfos.add(cstmVchrInfo);
		}
		
	}
	
	public boolean isNumeric(String str) { 
		  try {  
		    Double.parseDouble(str);  
		    return true;
		  } catch(NumberFormatException e){  
		    return false;  
		  }  
		}

	public void setVendorAdditionalInfo(String cmpyId, String usrId, CstmVchrInfoInput cstmVchrInfo) {
		BrowseResponse<VendorDefaultSettingBrowseOutput> starBrowseResponse = new BrowseResponse<VendorDefaultSettingBrowseOutput>();
		starBrowseResponse.setOutput(new VendorDefaultSettingBrowseOutput());

		VendorInfoDAO vendorInfoDAO = new VendorInfoDAO();
		vendorInfoDAO.readVendorDefaultSetting(starBrowseResponse, cmpyId, cstmVchrInfo.getVchrVenId(), usrId);

		if (starBrowseResponse.output.fldTblVndrDfltStng.size() > 0) {
			if (starBrowseResponse.output.fldTblVndrDfltStng.get(0).getVchrBrh() != null) {
				cstmVchrInfo.setVchrBrh(starBrowseResponse.output.fldTblVndrDfltStng.get(0).getVchrBrh());
			}

			if (starBrowseResponse.output.fldTblVndrDfltStng.get(0).getCry() != null) {
				cstmVchrInfo.setVchrCry(starBrowseResponse.output.fldTblVndrDfltStng.get(0).getCry());
			}
		}
	}
}
