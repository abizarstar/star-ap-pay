package com.star.common;
 
import org.glassfish.jersey.server.ResourceConfig;
 
public class StarOcrApp extends ResourceConfig 
{
    public StarOcrApp() 
    {
        packages("com.star.service.main");
        register(AuthenticationFilter.class);
        register(CustomLoggingFilter.class);
    }
}