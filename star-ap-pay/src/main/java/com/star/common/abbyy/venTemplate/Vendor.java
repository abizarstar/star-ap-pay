package com.star.common.abbyy.venTemplate;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Vendor {
	String data;

	public List<InvoiceDetailModel> getVendor(String str) throws Exception {
		// TODO Auto-generated constructor stub
		this.data = str;
		List<InvoiceDetailModel> detailModels = new ArrayList<InvoiceDetailModel>();
		
		String input = new String(Files.readAllBytes(Paths.get(data)));
		if (input.contains(PascoInvoiceDetail.getVendor())) {
			PascoInvoiceDetail vendor = new PascoInvoiceDetail();
			detailModels.add(vendor.readData(data));
			return detailModels;
		}
		if (input.contains(AKSteelInvoiceDetail.getVendor())) {
			AKSteelInvoiceDetail vendor = new AKSteelInvoiceDetail();
			detailModels.add(vendor.readData(data));
			return detailModels;
		}
		if (input.contains(Waterway21Invoice.getVendor())) {
			Waterway21Invoice vendor = new Waterway21Invoice();
			detailModels.add(vendor.readData(data));
			return detailModels;
		}
		if (input.contains(RingCentral.getVendor())) {
			RingCentral vendor = new RingCentral();
			detailModels.add(vendor.readData(data));
			return detailModels;
		}
		if (input.contains(ADSInvoice.getVendor())) {
			ADSInvoice vendor = new ADSInvoice();
			detailModels.add(vendor.readData(data));
			return detailModels;
		}
		if (input.contains(Amazon.getVendor())) {
			Amazon vendor = new Amazon();
			detailModels.add(vendor.readData(data));
			return detailModels;
		}
		/*if (input.contains(SDIInvoice.getVendor())) {
			SDIInvoice vendor = new SDIInvoice();
			return vendor.readData(data);
		}*/
		if (input.contains(WasteManagementInvoice.getVendor())) {
			WasteManagementInvoice vendor = new WasteManagementInvoice();
			detailModels.add(vendor.readData(data));
			return detailModels;
		}
		if (input.contains(BigRiver_Invoice.getVendor())) {
			BigRiver_Invoice vendor = new BigRiver_Invoice();
			detailModels.add(vendor.readData(data));
			return detailModels;
		}
		if (input.contains(Fastenal_Invoice.getVendor())) {
			Fastenal_Invoice vendor = new Fastenal_Invoice();
			detailModels.add(vendor.readData(data));
			return detailModels;
		}
		if (input.contains(Cintas_Invoice.getVendor())) {
			Cintas_Invoice vendor = new Cintas_Invoice();
			detailModels.add(vendor.readData(data));
			return detailModels;
		}
		if (input.contains(Bayou_Processing_Invoice.getVendor())) {
			Bayou_Processing_Invoice vendor = new Bayou_Processing_Invoice();
			detailModels.add(vendor.readData(data));
			return detailModels;
		}
		if (input.contains(Grainger_Invoice.getVendor())) {
			Grainger_Invoice vendor = new Grainger_Invoice();
			detailModels.add(vendor.readData(data));
			return detailModels;
		} if (input.contains(JSW_Invoice.getVendor())) {
			JSW_Invoice vendor = new JSW_Invoice();
			detailModels.add(vendor.readData(data));
			return detailModels;
		}if (input.contains(Leeco_Invoice.getVendor())) {
			Leeco_Invoice vendor = new Leeco_Invoice();
			detailModels.add(vendor.readData(data));
			return detailModels;
		} /*if (input.contains(Vulcan_Invoice.getVendor())) {
			Vulcan_Invoice vendor = new Vulcan_Invoice();
			return vendor.readData(data);
		}if (input.contains(Donnelley_Invoice.getVendor())) {
			Donnelley_Invoice vendor = new Donnelley_Invoice();
			return vendor.readData(data);
		}*/else {
			
			VendorInvoice invoice = new VendorInvoice();
			detailModels = invoice.readData(data);
			
			/*if(detailModel != null)
			{
				if (!isNumeric(detailModel.getTotalamount())) {
					detailModel = null;
				}
			}*/
			
			return detailModels;
		}
	}
	
	
	public boolean isNumeric(String str) { 
		  try {  
		    Double.parseDouble(str);  
		    return true;
		  } catch(NumberFormatException e){  
		    return false;  
		  }  
		}
}
