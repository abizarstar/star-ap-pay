package com.star.common.abbyy.venTemplate;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.Scanner;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.dao.OCRProcessDAO;
import com.star.modal.CstmOcrVenMap;

public class VendorInvoice {

	private static Logger logger = LoggerFactory.getLogger(VendorInvoice.class);

	private static Pattern DATE_PATTERN = Pattern.compile("^\\d{2}/\\d{1,2}/\\d{4}$");
	private static Pattern DATE_PATTERN1 = Pattern.compile("^[0-3][0-9]/[0-3][0-9]/(?:[0-9][0-9])?[0-9][0-9]$");
	private static Pattern DATE_PATTERN2 = Pattern.compile(
			"(Jan(?:uary)?|Feb(?:ruary)?|Mar(?:ch)?|Apr(?:il)?|May|Jun(?:e)?|Jul(?:y)?|Aug(?:ust)?|Sep(?:tember)?|Oct(?:ober)?|Nov(?:ember)?|Dec(?:ember)?) (\\d{1,2}), (\\d{4})");
	private static Pattern DATE_PATTERN3 = Pattern.compile("^\\d{1}/\\d{1,2}/\\d{4}$");

	public InvoiceDetailModel readData(String fileName) throws Exception {

		String currency[] = { "$", "USD", "$", "$" };
		String InvoiceNumber = "-";
		String InvoiceDate = "-";
		String InvoiceDueDate = "-";
		String TotalAmount = "-";
		String Currency = "USD";

		String line = null;
		int lineCount = 0;

		String flNm = fileName;
		Scanner sc = new Scanner(new File(flNm));

		OCRProcessDAO ocrProcessDAO = new OCRProcessDAO();

		List<CstmOcrVenMap> vendorMapList = ocrProcessDAO.getVendorList("");

		CstmOcrVenMap vendorMap = null;

		boolean venSts = false;

		while (sc.hasNext()) {

			line = sc.nextLine();

			venSts = containsName(vendorMapList, line);

			if (venSts == true) {
				vendorMap = getSelectedValue(vendorMapList, line);
				logger.debug("VENDOR FOUND ----->" + vendorMap.getVenNm());
				break;
			}

		}

		sc = new Scanner(new File(flNm));
		while (sc.hasNext() && venSts == true) {

			line = sc.nextLine();

			if (line.contains(vendorMap.getVenInvDtFld())) {

				String fldNm = vendorMap.getVenInvDtFld();

				String after = line.trim().replaceAll(" +", " ");

				String[] arrOfStr = after.split(" ");

				for (int i = 0; i < arrOfStr.length; i++) {
					if (DATE_PATTERN.matcher(arrOfStr[i]).matches()) {
						InvoiceDate = arrOfStr[i];

						String[] temp = InvoiceDate.split("/");

						String newDate = "";
						for (int j = 0; j < temp.length; j++) {

							if (j == temp.length - 1) {
								if (temp[j].length() < 3) {
									newDate = newDate + "20";
								}
							}

							if (temp[j].length() < 2) {
								newDate = newDate + "0" + temp[j] + "/";
							} else {
								newDate = newDate + temp[j] + "/";
							}
						}

						if (newDate.length() > 0) {
							newDate = newDate.substring(0, newDate.length() - 1);
						}

						InvoiceDate = newDate;
					}
				}

				if (InvoiceDate.equals("-")) {

					InvoiceDate = readFile_date(flNm, fldNm, lineCount);
					InvoiceDate = InvoiceDate.trim();

					if (status_check(InvoiceDate) > 3) {
						InvoiceDate = date_format(InvoiceDate);

					}

					if (InvoiceDate.contains("-")) {
						String[] temp = InvoiceDate.split("-");
						String newDate = "";
						for (int j = 0; j < temp.length; j++) {

							if (j == temp.length - 1) {
								if (temp[j].length() < 3) {
									newDate = newDate + "20";
								}
							}

							if (temp[j].length() < 2) {
								newDate = newDate + "0" + temp[j] + "/";
							} else {
								newDate = newDate + temp[j] + "/";
							}

						}

						if (newDate.length() > 0) {
							newDate = newDate.substring(0, newDate.length() - 1);
						}

						InvoiceDate = newDate;
					} else {
						if (InvoiceDate.contains("/")) {
							String[] temp = InvoiceDate.split("/");
							String newDate = "";
							for (int j = 0; j < temp.length; j++) {

								if (j == temp.length - 1) {
									if (temp[j].length() < 3) {
										newDate = newDate + "20";
									}
								}

								if (temp[j].length() < 2) {
									newDate = newDate + "0" + temp[j] + "/";
								} else {
									newDate = newDate + temp[j] + "/";
								}

							}

							if (newDate.length() > 0) {
								newDate = newDate.substring(0, newDate.length() - 1);
							}

							InvoiceDate = newDate;
						}
					}

				}

			}

			// ====================================INVOICE DUE
			// DATE========================================================
			if (line.contains(vendorMap.getVenDueDtFld()) && vendorMap.getVenDueDtFld().length() > 0) {

				String fldNm = vendorMap.getVenDueDtFld();
				String after = line.trim().replace(",", "").replaceAll(" +", " ");

				int pos = line.indexOf(fldNm) + fldNm.length();
				String after_test = line.substring(pos, line.length());
				String[] arrOfStr_test = after_test.replace(".", "").trim().split("  +");
				for (int k = 0; k < arrOfStr_test.length; k++) {
					if (DATE_PATTERN2.matcher(arrOfStr_test[k]).matches()) {
						InvoiceDueDate = arrOfStr_test[k];
						InvoiceDueDate = date_format(InvoiceDueDate);
					}
				}

				String[] arrOfStr = after.split(" ");

				for (int i = 0; i < arrOfStr.length; i++) {
					// System.out.println(DATE_PATTERN2.matcher(arrOfStr[i]).matches()+""+(arrOfStr[i]));
					if (DATE_PATTERN.matcher(arrOfStr[i]).matches() || DATE_PATTERN1.matcher(arrOfStr[i]).matches()) {

						InvoiceDueDate = arrOfStr[i];

						String[] temp = InvoiceDueDate.split("/");

						String newDate = "";
						for (int j = 0; j < temp.length; j++) {

							if (j == temp.length - 1) {
								if (temp[j].length() < 3) {
									newDate = newDate + "20";
								}
							}

							if (temp[j].length() < 2) {
								newDate = newDate + "0" + temp[j] + "/";
							} else {
								newDate = newDate + temp[j] + "/";
							}
						}

						if (newDate.length() > 0) {
							newDate = newDate.substring(0, newDate.length() - 1);
						}

						InvoiceDueDate = newDate;

					}
				}

				if (InvoiceDueDate.equals("-") && (!"NA".equals(fldNm))) {

					InvoiceDueDate = readFile_due_date(flNm, fldNm, lineCount);
					InvoiceDueDate = InvoiceDueDate.trim();

					if (status_check(InvoiceDueDate) > 3 && (!"NA".equals(fldNm))) {

						InvoiceDueDate = date_format(InvoiceDueDate);
					}

					if (InvoiceDueDate.contains("-")) {
						String[] temp = InvoiceDueDate.split("-");
						String newDate = "";
						for (int j = 0; j < temp.length; j++) {

							if (j == temp.length - 1) {
								if (temp[j].length() < 3) {
									newDate = newDate + "20";
								}
							}

							if (temp[j].length() < 2) {
								newDate = newDate + "0" + temp[j] + "/";
							} else {
								newDate = newDate + temp[j] + "/";
							}

						}

						if (newDate.length() > 0) {
							newDate = newDate.substring(0, newDate.length() - 1);
						}

						InvoiceDueDate = newDate;

					} else {
						if (InvoiceDueDate.contains("/")) {
							String[] temp = InvoiceDueDate.split("/");
							String newDate = "";
							for (int j = 0; j < temp.length; j++) {
								// System.out.println(temp[j]);
								if (j == temp.length - 1) {
									if (temp[j].length() < 3) {
										newDate = newDate + "20";
									}
								}

								if (temp[j].length() < 2) {
									newDate = newDate + "0" + temp[j] + "/";
								} else {
									newDate = newDate + temp[j] + "/";
								}

							}

							if (newDate.length() > 0) {
								newDate = newDate.substring(0, newDate.length() - 1);
							}

							InvoiceDueDate = newDate;

						}
					}

				}

			}

			if (line.contains(vendorMap.getVenInvNoFld()) && InvoiceNumber.equals("-")) {

				String fldNm = vendorMap.getVenInvNoFld();

				String after = line.trim().replaceAll(" +", " ");

				after = after.substring(after.indexOf(fldNm), after.length());

				String[] arrOfStr = after.replace(fldNm, "").trim().split(" ");

				for (int i = 0; i < arrOfStr.length; i++) {
					if (arrOfStr[i].toString().length() > 0) {
						InvoiceNumber = arrOfStr[i];
						InvoiceNumber = InvoiceNumber.trim();
						break;
					}

				}

				if (InvoiceNumber.equals("-")
						|| status_check(InvoiceNumber) == InvoiceNumber.replaceAll(" ", "").length()) {

					InvoiceNumber = readFile(flNm, fldNm, lineCount);
					InvoiceNumber = InvoiceNumber.trim();

				}

			}
			/*-------------------INVOICE TOTAL AMOUNT---------------------*/
			if (line.contains(vendorMap.getVenAmtFld())) {

				String fldNm = vendorMap.getVenAmtFld();

				String after = line.trim().replaceAll(" +", " ");

				after = after.substring(after.indexOf(fldNm), after.length());

				String[] arrOfStr = after.split(fldNm);

				for (int i = 0; i < arrOfStr.length; i++) {
					if (arrOfStr[i].toString().length() > 0 && status_check(arrOfStr[i]) == 0) {

						TotalAmount = arrOfStr[i];
						TotalAmount = TotalAmount.trim();

						TotalAmount = TotalAmount.replace("$", "");
						TotalAmount = TotalAmount.replace(",", "");
						break;
					}

				}

				if (TotalAmount.equals("-") && vendorMap.getVenNm().equals("Flack Global Metals")) {
					TotalAmount = readFile_amt(flNm, fldNm, lineCount);
					TotalAmount = TotalAmount.trim().replace("$", "").replace(",", "");

				}

				String cry = stringContainsItemFromListValue(line, currency);

				if (cry.contains("$")) {
					Currency = "USD";
				} else {
					if (cry.length() != 0) {
						Currency = cry;
					}
				}
			}

			lineCount = lineCount + 1;
		}
		/*if (vendorMap.getVenDueDtFld().equals("NA")) {
			InvoiceDueDate = "NA";
		}*/

		sc.close();

		logger.debug("========INVOICE DETAILS========");
		logger.debug("INVOICE NUMBER : " + InvoiceNumber);
		logger.debug("INVOICE DATE : " + InvoiceDate);
		logger.debug("INVOICE TOTAL AMOUNT : " + TotalAmount);
		logger.debug("CURRENCY : " + Currency);

		InvoiceDetailModel dtl = null;
		
		if(vendorMap != null)
		{
			dtl = new InvoiceDetailModel(InvoiceNumber, vendorMap.getVenId(), InvoiceDate, "", Currency,
				TotalAmount);
		}
		
		return dtl;

	}

	public static boolean containsName(List<CstmOcrVenMap> list, String line) {
		return list.stream().filter(o -> line.contains(o.getVenNm())).findFirst().isPresent();
	}

	public static CstmOcrVenMap getSelectedValue(List<CstmOcrVenMap> list, String name) {
		CstmOcrVenMap mapC = null;
		Optional<CstmOcrVenMap> matchingObject = list.stream().filter(p -> name.contains(p.getVenNm())).findFirst();

		if (matchingObject.isPresent()) {// Check whether optional has element you are looking for
			mapC = matchingObject.get();// get it from optional
		}

		return mapC;
	}

	public static String readFile(String flNm, String searchStr, int lineNo) {
		String line = "";
		Scanner sc;
		int lineCount = 0;
		int strtPos = 0;
		int endPos = 0;
		String output = "";

		try {
			sc = new Scanner(new File(flNm));

			while (sc.hasNext()) {
				line = sc.nextLine();
				int lastindex = line.length();

				if (lineCount == lineNo) {

					strtPos = line.indexOf(searchStr);

					if (searchStr.length() < 10) {
						endPos = strtPos + 15;
					} else {
						endPos = strtPos + searchStr.length();
					}
				}

				if (lineCount > lineNo) {

					if (line.length() > 0) {
						output = line.substring(strtPos - 5, endPos);
						output = output.trim();

						if (output.length() > 0) {
							break;
						}
					}
				}

				lineCount = lineCount + 1;
			}

			sc.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return output;
	}

	public static String readFile_date(String flNm, String searchStr, int lineNo) {
		String line = "";
		Scanner sc;
		int lineCount = 0;
		int strtPos = 0;
		int endPos = 0;
		String output = "";
		try {
			sc = new Scanner(new File(flNm));

			while (sc.hasNext()) {
				line = sc.nextLine();
				int lastindex = line.length();

				if (lineCount == lineNo) {

					strtPos = line.indexOf(searchStr);
					// System.out.println(strtPos);
					if (searchStr.length() < 10) {
						endPos = strtPos + 15;
					} else {
						endPos = strtPos + searchStr.length();
						// System.out.println("check1"+endPos);

					}
				}

				if (lineCount > lineNo) {
					// System.out.println(line.length());
					if (line.length() == 0) {
						continue;
					} else {
						output = line.substring(strtPos - 10, endPos + 1); // endpos added +1 for big river

						// System.out.println("LINE---->" + output);
						output = output.trim();
					}
					if (output.length() > 0) {
						break;
					}

				}

				lineCount = lineCount + 1;
			}

			sc.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return output;
	}

	public static String stringContainsItemFromListValue(String inputStr, String[] items) {

		String p = "";
		Optional<String> optional = Arrays.stream(items).filter(x -> inputStr.contains(x)).findFirst();

		if (optional.isPresent()) {// Check whether optional has element you are looking for
			p = optional.get();// get it from optional
		}

		return p;
	}


	private static int status_check(String invoiceNumber) {
		int status_count = 0;
		for (int s = 0; s < invoiceNumber.length(); s++) {
			// System.out.println("status for loop");
			char ch = invoiceNumber.charAt(s);
			// System.out.println("status for loop"+ch);
			if (((ch >= 'A' && ch <= 'Z')) || ((ch >= 'a' && ch <= 'z'))) {
				// System.out.println("status loop");
				status_count++;
			}
		}
		System.out.println("status count====" + status_count);
		return status_count;
	}

	public static String date_format(String invdate) {
		DateFormat inputFormat = new SimpleDateFormat("MMMM d, yyyy");
		DateFormat outputFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date d1;
		try {
			d1 = inputFormat.parse(invdate);
			invdate = outputFormat.format(d1);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return invdate;
	}

	public static String readFile_due_date(String flNm, String searchStr, int lineNo) {
		String line = "";
		Scanner sc;
		int lineCount = 0;
		int strtPos = 0;
		int endPos = 0;
		String output = "";
		try {
			sc = new Scanner(new File(flNm));

			while (sc.hasNext()) {
				line = sc.nextLine();
				int lastindex = line.length();

				if (lineCount == lineNo) {

					strtPos = line.indexOf(searchStr);
					// System.out.println(strtPos);
					if (searchStr.length() < 10) {
						endPos = strtPos + 15;
					} else {
						endPos = strtPos + searchStr.length();
						// System.out.println("check1"+endPos);

					}
				}

				if (lineCount > lineNo) {
					// System.out.println(line.length());
					if (line.length() == 0) {
						continue;
					} else {
						output = line.substring(strtPos - 10, endPos + 6); // changed -5 to -11 --- added +5(big
																			// river)/+1(amazon) in end position

						// System.out.println("LINE---->" + output);
						output = output.trim();
					}
					if (output.length() > 0) {
						break;
					}

				}

				lineCount = lineCount + 1;
			}

			sc.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return output;
	}

	public static String readFile_amt(String flNm, String searchStr, int lineNo) {
		String line = "";
		Scanner sc;
		int lineCount = 0;
		int strtPos = 0;
		int endPos = 0;
		String output = "";

		try {
			sc = new Scanner(new File(flNm));

			while (sc.hasNext()) {
				line = sc.nextLine();
				int lastindex = line.length();

				if (lineCount == lineNo) {

					strtPos = line.indexOf(searchStr);

					if (searchStr.length() < 10) {
						endPos = strtPos + 15;
					} else {
						endPos = strtPos + searchStr.length();
					}
				}

				if (lineCount > lineNo) {

					output = line.substring(strtPos - 11, endPos); // changed -5 to -11 --- added +5(big
																	// river)/+1(amazon) in end position

					// System.out.println("LINE---->" + output);
					output = output.trim();

					if (output.length() > 0) {
						break;
					}

				}

				lineCount = lineCount + 1;
			}

			sc.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return output;
	}


	/*public InvoiceDetailModel readData(String fileName) throws Exception {

		String currency[] = { "$", "USD", "$", "$" };
		String InvoiceNumber = "-";
		String InvoiceDate = "-";
		String InvoiceDueDate = "-";
		String TotalAmount = "-";
		String Currency = "USD";

		String line = null;
		int lineCount = 0;

		String flNm = fileName;
		Scanner sc = new Scanner(new File(flNm));

		OCRProcessDAO ocrProcessDAO = new OCRProcessDAO();

		List<CstmOcrVenMap> vendorMapList = ocrProcessDAO.getVendorList("");

		CstmOcrVenMap vendorMap = null;

		boolean venSts = false;

		while (sc.hasNext()) {

			line = sc.nextLine();

			venSts = containsName(vendorMapList, line);

			if (venSts == true) {
				vendorMap = getSelectedValue(vendorMapList, line);
				logger.debug("VENDOR FOUND ----->" + vendorMap.getVenNm());
				break;
			}

		}

		sc = new Scanner(new File(flNm));
		while (sc.hasNext() && venSts == true) {

			line = sc.nextLine();

			if (line.contains(vendorMap.getVenInvDtFld())) {

				String fldNm = vendorMap.getVenInvDtFld();

				String after = line.trim().replaceAll(" +", " ");

				String[] arrOfStr = after.split(" ");

				for (int i = 0; i < arrOfStr.length; i++) {
					if (DATE_PATTERN.matcher(arrOfStr[i]).matches() || DATE_PATTERN3.matcher(arrOfStr[i]).matches()) {
						InvoiceDate = arrOfStr[i];

						String[] temp = InvoiceDate.split("/");

						String newDate = "";
						for (int j = 0; j < temp.length; j++) {

							if (j == temp.length - 1) {
								if (temp[j].length() < 3) {
									newDate = newDate + "20";
								}
							}

							if (temp[j].length() < 2) {
								newDate = newDate + "0" + temp[j] + "/";
							} else {
								newDate = newDate + temp[j] + "/";
							}
						}

						if (newDate.length() > 0) {
							newDate = newDate.substring(0, newDate.length() - 1);
						}

						InvoiceDate = newDate;
					}
				}

				if (InvoiceDate.equals("-")) {

					InvoiceDate = readFile_date(flNm, fldNm, lineCount);

					if (DATE_PATTERN2.matcher(InvoiceDate).matches()) {
						InvoiceDate = date_format(InvoiceDate);
					}

					String after_invdt = InvoiceDate.trim().replaceAll(" +", " ");
					// System.out.println(after_invdt);
					String after_invdt_split[] = after_invdt.split(" ");

					for (int indt = 0; indt < after_invdt_split.length; indt++) {
						String invdt = after_invdt_split[indt];

						if (invdt.length() > 0) // added && status_check(invdt)==0
						{
							InvoiceDate = after_invdt_split[indt];
							// System.out.println("INvoicedate-->"+InvoiceDate);
							break;
						}
					} // new end

					InvoiceDate = InvoiceDate.trim();

					if (InvoiceDate.contains("-")) {
						String[] temp = InvoiceDate.split("-");
						String newDate = "";
						for (int j = 0; j < temp.length; j++) {

							if (j == temp.length - 1) {
								if (temp[j].length() < 3) {
									newDate = newDate + "20";
								}
							}

							if (temp[j].length() < 2) {
								newDate = newDate + "0" + temp[j] + "/";
							} else {
								newDate = newDate + temp[j] + "/";
							}

						}

						if (newDate.length() > 0) {
							newDate = newDate.substring(0, newDate.length() - 1);
						}

						InvoiceDate = newDate;
					} else {
						if (InvoiceDate.contains("/")) {
							String[] temp = InvoiceDate.split("/");
							String newDate = "";
							for (int j = 0; j < temp.length; j++) {

								if (j == temp.length - 1) {
									if (temp[j].length() < 3) {
										newDate = newDate + "20";
									}
								}

								if (temp[j].length() < 2) {
									newDate = newDate + "0" + temp[j] + "/";
								} else {
									newDate = newDate + temp[j] + "/";
								}

							}

							if (newDate.length() > 0) {
								newDate = newDate.substring(0, newDate.length() - 1);
							}

							InvoiceDate = newDate;
						}
					}

				}

			}

			// ====================================INVOICE DUE
			// DATE========================================================
			if (line.contains(vendorMap.getVenDueDtFld()) && vendorMap.getVenDueDtFld().length() > 0) {

				String fldNm = vendorMap.getVenDueDtFld();
				String after = line.trim().replace(",", "").replaceAll(" +", " ");

				int pos = line.indexOf(fldNm) + fldNm.length();
				String after_test = line.substring(pos, line.length());
				String[] arrOfStr_test = after_test.replace(".", "").trim().split("  +");
				for (int k = 0; k < arrOfStr_test.length; k++) {
					if (DATE_PATTERN2.matcher(arrOfStr_test[k]).matches()) {
						InvoiceDueDate = arrOfStr_test[k];
						InvoiceDueDate = date_format(InvoiceDueDate);
					}
				}

				String[] arrOfStr = after.split(" ");

				for (int i = 0; i < arrOfStr.length; i++) {
					// System.out.println(DATE_PATTERN2.matcher(arrOfStr[i]).matches()+""+(arrOfStr[i]));
					if (DATE_PATTERN.matcher(arrOfStr[i]).matches() || DATE_PATTERN1.matcher(arrOfStr[i]).matches()) {

						InvoiceDueDate = arrOfStr[i];

						String[] temp = InvoiceDueDate.split("/");

						String newDate = "";
						for (int j = 0; j < temp.length; j++) {

							if (j == temp.length - 1) {
								if (temp[j].length() < 3) {
									newDate = newDate + "20";
								}
							}

							if (temp[j].length() < 2) {
								newDate = newDate + "0" + temp[j] + "/";
							} else {
								newDate = newDate + temp[j] + "/";
							}
						}

						if (newDate.length() > 0) {
							newDate = newDate.substring(0, newDate.length() - 1);
						}

						InvoiceDueDate = newDate;

					}
				}

				if (InvoiceDueDate.equals("-") && (!"NA".equals(fldNm))) {

					InvoiceDueDate = readFile_due_date(flNm, fldNm, lineCount);
					InvoiceDueDate = InvoiceDueDate.trim();

					if (status_check(InvoiceDueDate) > 3 && (!"NA".equals(fldNm))) {

						InvoiceDueDate = date_format(InvoiceDueDate);
					}

					if (InvoiceDueDate.contains("-")) {
						String[] temp = InvoiceDueDate.split("-");
						String newDate = "";
						for (int j = 0; j < temp.length; j++) {

							if (j == temp.length - 1) {
								if (temp[j].length() < 3) {
									newDate = newDate + "20";
								}
							}

							if (temp[j].length() < 2) {
								newDate = newDate + "0" + temp[j] + "/";
							} else {
								newDate = newDate + temp[j] + "/";
							}

						}

						if (newDate.length() > 0) {
							newDate = newDate.substring(0, newDate.length() - 1);
						}

						InvoiceDueDate = newDate;

					} else {
						if (InvoiceDueDate.contains("/")) {
							String[] temp = InvoiceDueDate.split("/");
							String newDate = "";
							for (int j = 0; j < temp.length; j++) {
								// System.out.println(temp[j]);
								if (j == temp.length - 1) {
									if (temp[j].length() < 3) {
										newDate = newDate + "20";
									}
								}

								if (temp[j].length() < 2) {
									newDate = newDate + "0" + temp[j] + "/";
								} else {
									newDate = newDate + temp[j] + "/";
								}

							}

							if (newDate.length() > 0) {
								newDate = newDate.substring(0, newDate.length() - 1);
							}

							InvoiceDueDate = newDate;

						}
					}

				}

			}

			if (line.contains(vendorMap.getVenInvNoFld()) && InvoiceNumber.equals("-")) {

				String fldNm = vendorMap.getVenInvNoFld();

				String after = line.trim().replaceAll(" +", " ");

				after = after.substring(after.indexOf(fldNm), after.length());

				String[] arrOfStr = after.replace(fldNm, "").trim().split(" ");

				for (int i = 0; i < arrOfStr.length; i++) {
					if (arrOfStr[i].toString().length() > 0) {
						InvoiceNumber = arrOfStr[i];
						InvoiceNumber = InvoiceNumber.trim();
						break;
					}

				}

				if (InvoiceNumber.equals("-")
						|| status_check(InvoiceNumber) == InvoiceNumber.replaceAll(" ", "").length()) {

					InvoiceNumber = readFile(flNm, fldNm, lineCount);
					InvoiceNumber = InvoiceNumber.trim();

					if (status_check(InvoiceNumber) == InvoiceNumber.length()) {
						// InvoiceNumber = readFile(flNm, fldNm, lineCount);
						InvoiceNumber = "";
					} else {
						String after_invno = InvoiceNumber.trim().replaceAll(" +", " ");
						// System.out.println("afterno."+after_invno);
						String after_invno_split[] = after_invno.split(" ");
						for (int ino = 0; ino < after_invno_split.length; ino++) {
							String invno = after_invno_split[ino];
							if (invno.length() > 0) {
								InvoiceNumber = after_invno_split[ino];
								break;
							}
						} // new end
					}

					InvoiceNumber = InvoiceNumber.trim();

				}

			}
			
			if (line.contains(vendorMap.getVenAmtFld())) {

				String fldNm = vendorMap.getVenAmtFld();

				String after = line.replace("$", "").trim().replaceAll(" +", " ");

				after = after.substring(after.indexOf(fldNm), after.length());

				String[] arrOfStr = after.replace(fldNm, "").trim().split(" ");

				for (int i = 0; i < arrOfStr.length; i++) {
					if (arrOfStr[i].toString().length() > 0 && status_check(arrOfStr[i]) == 0) {

						TotalAmount = arrOfStr[i];
						TotalAmount = TotalAmount.trim();

						TotalAmount = TotalAmount.replace("$", "");
						TotalAmount = TotalAmount.replace(",", "");
						break;
					}

				}

				if (TotalAmount.equals("-")) {
					TotalAmount = readFile_amt(flNm, fldNm, lineCount);
					TotalAmount = TotalAmount.trim().replaceAll("[USD$,]+", "");

				}

				String cry = stringContainsItemFromListValue(line, currency);

				if (cry.contains("$")) {
					Currency = "USD";
				} else {
					if (cry.length() != 0) {
						Currency = cry;
					}
				}
			}

			lineCount = lineCount + 1;
		}

		if (vendorMap != null) {
			if (vendorMap.getVenDueDtFld().equals("NA")) {
				InvoiceDueDate = "NA";
			}
			if(status_check(InvoiceDate)==3)
			{
				InvoiceDate = date_format1(InvoiceDate);
			}
		}

		sc.close();

		logger.debug("========INVOICE DETAILS========");
		logger.debug("INVOICE NUMBER : " + InvoiceNumber);
		logger.debug("INVOICE DATE : " + InvoiceDate);
		logger.debug("INVOICE TOTAL AMOUNT : " + TotalAmount);
		logger.debug("CURRENCY : " + Currency);

		InvoiceDetailModel dtl = null;

		if (vendorMap != null) {
			dtl = new InvoiceDetailModel(InvoiceNumber, vendorMap.getVenId(), InvoiceDate, "", Currency, TotalAmount);
		}

		return dtl;

	}

	public static boolean containsName(List<CstmOcrVenMap> list, String line) {
		return list.stream().filter(o -> line.contains(o.getVenNm())).findFirst().isPresent();
	}

	public static CstmOcrVenMap getSelectedValue(List<CstmOcrVenMap> list, String name) {
		CstmOcrVenMap mapC = null;
		Optional<CstmOcrVenMap> matchingObject = list.stream().filter(p -> name.contains(p.getVenNm())).findFirst();

		if (matchingObject.isPresent()) {// Check whether optional has element you are looking for
			mapC = matchingObject.get();// get it from optional
		}

		return mapC;
	}

	public static String readFile(String flNm, String searchStr, int lineNo) {
		String line = "";
		Scanner sc;
		int lineCount = 0;
		int strtPos = 0;
		int endPos = 0;
		String output = "";

		try {
			sc = new Scanner(new File(flNm));

			while (sc.hasNext()) {
				line = sc.nextLine();
				int lastindex = line.length();

				if (lineCount == lineNo) {

					strtPos = line.indexOf(searchStr);

					if (searchStr.length() < 10) {
						endPos = strtPos + 15;
					} else {
						endPos = strtPos + searchStr.length();
					}
				}

				if (lineCount > lineNo) {
					
					if(line.length()==0)
					{
						continue;
					}
					else {
						if(lastindex-endPos>43)
						{
							output = line.substring(strtPos -11,endPos+5);
						}
						else
						{
					output = line.substring(strtPos -11,lastindex); //changed -5 to -11 --- added +5(big river)/+1(amazon) in end position --- endPos+5 to lastindex
						}
					output = output.trim();
					}
					if (output.length() > 0) {
						break;
					}
				}

				lineCount = lineCount + 1;
			}

			sc.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return output;
	}

	public static String readFile_date(String flNm, String searchStr, int lineNo) {
		String line = "";
		Scanner sc;
		int lineCount = 0;
		int strtPos = 0;
		int endPos = 0;
		String output = "";
		try {
			sc = new Scanner(new File(flNm));

			while (sc.hasNext()) {
				line = sc.nextLine();
				int lastindex = line.length();

				if (lineCount == lineNo) {

					strtPos = line.indexOf(searchStr);
					// System.out.println(strtPos);
					if (searchStr.length() < 10) {
						endPos = strtPos + 15;
					} else {
						endPos = strtPos + searchStr.length();
						// System.out.println("check1"+endPos);

					}
				}

				if (lineCount > lineNo) {
					// System.out.println(line.length());
					if (line.length() == 0) {
						continue;
					} else {
						output = line.substring(strtPos - 10, lastindex); // endpos added +1 for big river

						// System.out.println("LINE---->" + output);
						output = output.trim();
					}
					if (output.length() > 0) {
						break;
					}

				}

				lineCount = lineCount + 1;
			}

			sc.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return output;
	}

	public static String stringContainsItemFromListValue(String inputStr, String[] items) {

		String p = "";
		Optional<String> optional = Arrays.stream(items).filter(x -> inputStr.contains(x)).findFirst();

		if (optional.isPresent()) {// Check whether optional has element you are looking for
			p = optional.get();// get it from optional
		}

		return p;
	}

	private static int status_check(String invoiceNumber) {
		int status_count = 0;
		for (int s = 0; s < invoiceNumber.length(); s++) {
			// System.out.println("status for loop");
			char ch = invoiceNumber.charAt(s);
			// System.out.println("status for loop"+ch);
			if (((ch >= 'A' && ch <= 'Z')) || ((ch >= 'a' && ch <= 'z'))) {
				// System.out.println("status loop");
				status_count++;
			}
		}
		System.out.println("status count====" + status_count);
		return status_count;
	}

	public static String date_format(String invdate) {
		DateFormat inputFormat = new SimpleDateFormat("MMMM d, yyyy");
		DateFormat outputFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date d1;
		try {
			d1 = inputFormat.parse(invdate);
			invdate = outputFormat.format(d1);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return invdate;
	}
	
	public static String date_format1(String invdate) throws ParseException
	{
		DateFormat inputFormat = new SimpleDateFormat("dd/MMM/yyyy");
		DateFormat outputFormat = new SimpleDateFormat("MM/dd/yyyy");
		Date d1 = inputFormat.parse(invdate);
		invdate = outputFormat.format(d1);
		
		return invdate;
	}

	public static String readFile_due_date(String flNm, String searchStr, int lineNo) {
		String line = "";
		Scanner sc;
		int lineCount = 0;
		int strtPos = 0;
		int endPos = 0;
		String output = "";
		try {
			sc = new Scanner(new File(flNm));

			while (sc.hasNext()) {
				line = sc.nextLine();
				int lastindex = line.length();

				if (lineCount == lineNo) {

					strtPos = line.indexOf(searchStr);
					// System.out.println(strtPos);
					if (searchStr.length() < 10) {
						endPos = strtPos + 15;
					} else {
						endPos = strtPos + searchStr.length();
						// System.out.println("check1"+endPos);

					}
				}

				if (lineCount > lineNo) {
					// System.out.println(line.length());
					if (line.length() == 0) {
						continue;
					} else {
						output = line.substring(strtPos - 10, endPos + 6); // changed -5 to -11 --- added +5(big
																			// river)/+1(amazon) in end position

						// System.out.println("LINE---->" + output);
						output = output.trim();
					}
					if (output.length() > 0) {
						break;
					}

				}

				lineCount = lineCount + 1;
			}

			sc.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return output;
	}

	public static String readFile_amt(String flNm, String searchStr, int lineNo) {
		String line = "";
		Scanner sc;
		int lineCount = 0;
		int strtPos = 0;
		int endPos = 0;
		String output = "";

		try {
			sc = new Scanner(new File(flNm));

			while (sc.hasNext()) {
				line = sc.nextLine();
				int lastindex = line.length();

				if (lineCount == lineNo) {

					strtPos = line.indexOf(searchStr);

					if (searchStr.length() < 10) {
						endPos = strtPos + 15;
					} else {
						endPos = strtPos + searchStr.length();
					}
				}

				if (lineCount > lineNo) {

					output = line.substring(strtPos - 11, endPos); // changed -5 to -11 --- added +5(big
																	// river)/+1(amazon) in end position

					// System.out.println("LINE---->" + output);
					output = output.trim();

					if (output.length() > 0) {
						break;
					}

				}

				lineCount = lineCount + 1;
			}

			sc.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return output;
	}*/

}
