package com.star.common;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;
import com.star.dao.PaymentDAO;
import com.star.linkage.bank.BankManOutput;

public class FTPDownloadUpload {

	public boolean downloadFile(String flNm) {
		String server = "172.20.35.73";
		int port = 21;
		String user = "ap10203";
		String pass = "ap10203";
		boolean success = false;

		FTPClient ftpClient = new FTPClient();
		try {

			ftpClient.connect(server, port);
			ftpClient.login(user, pass);
			ftpClient.enterLocalPassiveMode();
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

			// APPROACH #1: using retrieveFile(String, OutputStream)
			String remoteFile1 = "/workdir/ap10203wk/spool/" + flNm;
			File downloadFile1 = new File(CommonConstants.ROOT_DIR + flNm);
			OutputStream outputStream1 = new BufferedOutputStream(new FileOutputStream(downloadFile1));
			success = ftpClient.retrieveFile(remoteFile1, outputStream1);
			outputStream1.close();

			if (success) {
				System.out.println("File #1 has been downloaded successfully.");
			}

		} catch (IOException ex) {
			System.out.println("Error: " + ex.getMessage());
			ex.printStackTrace();
		} finally {
			try {
				if (ftpClient.isConnected()) {
					ftpClient.logout();
					ftpClient.disconnect();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		return success;
	}

	public boolean uploadFile(String flNm, String host, String port, String user, String pass, String loc) {
		boolean success = false;

		FTPClient ftpClient = new FTPClient();
		Session session = null;
		Channel channel = null;

		String localFlNm = CommonConstants.ROOT_DIR + CommonConstants.ELEC_DIR + "/" + flNm;
		String remoteFile = loc + flNm;

		try {
			if (port.equals("21")) {
				ftpClient.connect(host, Integer.parseInt(port));
				ftpClient.login(user, pass);
				ftpClient.enterLocalPassiveMode();
				ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

				// APPROACH #1: uploads first file using an InputStream
				File localFile = new File(localFlNm);
				/* String remoteFile = CommonConstants.FTP_UPLD_DIR + flNm; */

				InputStream inputStream = new FileInputStream(localFile);

				System.out.println("Start uploading local file");
				success = ftpClient.storeFile(remoteFile, inputStream);
				inputStream.close();
				if (success) {
					PaymentDAO paymentDAO = new PaymentDAO();
					paymentDAO.updateAchFileSentStatus(flNm);
					System.out.println("File uploaded successfully.");
				} else {
					System.out.println("File not uploaded successfully.");
				}
			} else if (port.equals("22")) {

				SftpATTRS attrs = null;

				JSch ssh = new JSch();
				session = ssh.getSession(user, host, Integer.parseInt(port));
				session.setConfig("StrictHostKeyChecking", "no");
				session.setPassword(pass);
				session.connect();
				channel = session.openChannel("sftp");
				channel.connect();

				ChannelSftp channelSftp = (ChannelSftp) channel;

				String curDirectory = channelSftp.pwd();

				remoteFile = curDirectory + "/" + remoteFile;

				channelSftp.put(localFlNm, remoteFile);

				success = true;

				if (success == true) {
					PaymentDAO paymentDAO = new PaymentDAO();
					paymentDAO.updateAchFileSentStatus(flNm);
				}
			}

		} catch (IOException ex) {
			System.out.println("Error: " + ex.getMessage());
			ex.printStackTrace();
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SftpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if (ftpClient.isConnected()) {
					ftpClient.logout();
					ftpClient.disconnect();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

		return success;
	}

	public boolean validateFTP(MaintanenceResponse<BankManOutput> starManResponse, String host, String port,
			String user, String pass) {

		boolean success = false;

		FTPClient ftpClient = new FTPClient();
		Session session = null;
		Channel channel = null;

		try {

			if (port.equals("21")) {
				ftpClient.connect(host, Integer.parseInt(port));
				boolean loginSts = ftpClient.login(user, pass);

				if (loginSts == false) {
					starManResponse.output.rtnSts = 1;
					starManResponse.output.messages.add(CommonConstants.FTP_USR_ERR);

					success = false;
				} else {
					success = true;
					/*
					 * ftpClient.changeWorkingDirectory(spool); int returnCode =
					 * ftpClient.getReplyCode(); if (returnCode == 250) { success = true; } else {
					 * starManResponse.output.rtnSts = 1;
					 * starManResponse.output.messages.add(ftpClient.getReplyString()); success =
					 * false; }
					 */
				}
			} else if (port.equals("22")) {

				SftpATTRS attrs = null;

				JSch ssh = new JSch();
				session = ssh.getSession(user, host, Integer.parseInt(port));
				session.setConfig("StrictHostKeyChecking", "no");
				session.setPassword(pass);
				session.connect();
				channel = session.openChannel("sftp");
				channel.connect();

				ChannelSftp channelSftp = (ChannelSftp) channel;

				String curDirectory = channelSftp.pwd();

				String dir = "/BTMU_ISO_IN_T:/*BIN";

				channelSftp.cd(curDirectory + "/" + dir);

				success = true;
			}

		} catch (JSchException e) {

			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(e.getMessage());
			System.out.println(e.getMessage());

		} catch (Exception e) {
			// TODO: handle exception
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(e.getMessage());
			System.out.println(e.getMessage());
		} finally {

			if (channel != null) {
				channel.disconnect();
			}
			if (session != null) {
				session.disconnect();
			}

			if (ftpClient.isConnected()) {
				try {
					ftpClient.disconnect();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return success;
	}
}
