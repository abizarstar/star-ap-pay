package com.star.common.bai;

import java.util.Date;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.StarResponseBuilder;
import com.star.common.storage.StorageType;
import com.star.dao.GLInfoDAO;
import com.star.dao.VendorInfoDAO;
import com.star.linkage.gl.GLBrowseOutput;
import com.star.linkage.vendor.CompanyBrowseOutput;
import com.star.linkage.vendor.VendorInfo;

public class GetStatementSch implements Job {

	private static Logger logger = LoggerFactory.getLogger(GetStatementSch.class);

	public void execute(JobExecutionContext context) throws JobExecutionException {

		StorageType.getStorgaeType();

		logger.debug("----- GET BANK STATEMENT STARTED ------");
		logger.debug(new Date().toString());
		getBankStatement();
		logger.debug(new Date().toString());
		logger.debug("----- GET BANK STATEMENT END ------");

	}

	public void getBankStatement() {
		
		GetStmtFTP ftp = new GetStmtFTP();
		
		BrowseResponse<CompanyBrowseOutput> starBrowseResponse = new BrowseResponse<CompanyBrowseOutput>();
		starBrowseResponse.setOutput(new CompanyBrowseOutput());
		
		VendorInfoDAO dao = new VendorInfoDAO();
			
		dao.readCompanyList(starBrowseResponse);
		
		if(starBrowseResponse.output.fldTblCompany.size() > 0)
		{
			ftp.getEBS("star", starBrowseResponse.output.fldTblCompany.get(0).getCmpyId());
			ftp.getLockBox("star", starBrowseResponse.output.fldTblCompany.get(0).getCmpyId());
		}
	}
}