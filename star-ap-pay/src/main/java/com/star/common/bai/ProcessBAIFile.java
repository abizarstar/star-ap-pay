package com.star.common.bai;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.star.common.CommonConstants;
import com.star.linkage.ebs.BankHdrDtls;
import com.star.linkage.ebs.BankStmtDetails;
import com.star.linkage.ebs.BankTrnDetails;

public class ProcessBAIFile {

	public static int lstProcessedLine = 0;

	public void readBAIFile(String fileNm, BankStmtDetails bankStmtDetails) {

		List<BankTrnDetails> bankTrnDetails = new ArrayList<BankTrnDetails>();

		List<BankHdrDtls> bankHdrList = new ArrayList<BankHdrDtls>();

		BankHdrDtls bankHdrDtls = new BankHdrDtls();

		try {

			File file = new File(fileNm);

			FileReader fr = new FileReader(file); // reads the file
			BufferedReader br = new BufferedReader(fr); // creates a buffering character input stream
			StringBuffer sb = new StringBuffer(); // constructs a string buffer with no characters
			String line;
			String addtnRef = "";
			while ((line = br.readLine()) != null) {
				
				String recordType = line.substring(0, 2);
				
				if(recordType.equals("02"))
				{
					bankHdrDtls = new BankHdrDtls();
				}
				else if (recordType.equals("98")) // HEADER END
				{
					bankHdrDtls.setListBnkTrn(bankTrnDetails);
					
					bankHdrList.add(bankHdrDtls);
					
					bankTrnDetails = new ArrayList<BankTrnDetails>();
				}
				else if(recordType.equals("16"))
				{
					addtnRef = getTransactionDetail(fileNm, line);
				}
				
				processLine(line, bankStmtDetails, bankHdrDtls, bankTrnDetails, bankHdrList, addtnRef);
			}
			fr.close();
			System.out.println("Contents of File: ");
			System.out.println(sb.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		bankStmtDetails.setBnkHdrList(bankHdrList);
	}

	public void processLine(String line, BankStmtDetails bankStmtDetails, BankHdrDtls bankHdrDtls,
			List<BankTrnDetails> bankTrnDetails, List<BankHdrDtls> bankHdrList, String addtnRef) {
		
		int iTrnsFlg = 0;
		
		String recordType = line.substring(0, 2);

		if (recordType.equals("01")) // FILE HEADER
		{
			parseFileHeader(line, bankStmtDetails);
		}
		else if (recordType.equals("02")) // FILE HEADER
		{
			String[] fileHeaderArray = line.split(",");
		
			String creationDt = fileHeaderArray[4];
			String creationTime = "0000";
		
			bankStmtDetails.setCrtdDt(formatDateWithoutTime(creationDt));
			bankStmtDetails.setCrtdTime(formatTime(creationTime));
			
		}
		else if (recordType.equals("03")) // ACCOUNT IDENTIFIER
		{
			parseAccountIdentifier(line, bankHdrDtls);
			lstProcessedLine = 3;
		} else if (recordType.equals("88")) // CONTINUATION
		{
			parseAdditionalDetals(line, bankHdrDtls);
			
		
		} else if (recordType.equals("16")) // TRANSACTION DETAIL
		{
			BankTrnDetails trnDetails = new BankTrnDetails();
			
			parseTransactionDetail(line, bankStmtDetails, bankTrnDetails, trnDetails);
			lstProcessedLine = 16;
			
			trnDetails.setAdtnRef(addtnRef);
			
			bankTrnDetails.add(trnDetails);
		
		} 
	}

	public void parseTransactionDetail(String line, BankStmtDetails bankStmtDetails,
			List<BankTrnDetails> bankTrnDetails, BankTrnDetails trnDetails) {

		List<BAICodes> baiList = new ArrayList<BAICodes>();

		CommonBAICodes baiCodes = new CommonBAICodes();

		baiCodes.getCodeList(baiList);

		String[] transactionArray = line.split(",");
		String recordType = transactionArray[0];
		String typCode = transactionArray[1];
		String amount = transactionArray[2];
		String fundsType = transactionArray[3];

		if (fundsType.equals("Z")) {
			String bankReference = transactionArray[4];
			String customerReference = transactionArray[5];
			
			bankReference = bankReference.replace("/", "");
			trnDetails.setBnkRef(bankReference);
			
			customerReference = customerReference.replace("/", "");
			trnDetails.setCustRef(customerReference);
		}

		trnDetails.setTrnAMount(formatAmount(amount));
		trnDetails.setTypeCode(typCode);
	}

	public void parseAdditionalDetals(String line, BankHdrDtls bankHdrDtls) {
		/* System.out.println(); */

		if (lstProcessedLine == 3) {
			getAccountDetails(line, bankHdrDtls);
		}
	}
	
	public String getTransactionDetail(String flNm, String searchLn)
	{
		int iFlg = 0;
		String textRemark = "";
		
		try {

			File file = new File(flNm);

			FileReader fr = new FileReader(file); // reads the file
			BufferedReader br = new BufferedReader(fr); // creates a buffering character input stream
			StringBuffer sb = new StringBuffer(); // constructs a string buffer with no characters
			String line;
			while ((line = br.readLine()) != null) {
				
				String recordType = line.substring(0, 2);
				
				if(recordType.equals("16"))
				{
					iFlg = 0;
				}
				
				if(line.equals(searchLn))
				{
					iFlg = 1; 
				}
				
				if(recordType.equals("88") && iFlg == 1)
				{
					String[] transactionArray = line.split(",");
					
					textRemark = textRemark + transactionArray[1] + " ";
				}
				
				if(recordType.equals("49"))
				{
					iFlg = 0;
				}

			}
			fr.close();
			System.out.println("Contents of File: ");
			System.out.println(sb.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
				
		if(textRemark.length() > 0)
		{
			textRemark = textRemark.replace("/", "");
			
			textRemark = textRemark.replace("NAME", "<br><br><b>NAME</b>");
			
			textRemark = textRemark.replace("ACH CR:", "<br><br><b>ACH CR:</b>");
			
			textRemark = textRemark.replace("ACH DR:", "<br><br><b>ACH DR:</b>");
			
			textRemark = textRemark.replace("CMS REF", "<br><br><b>CMS REF</b>");
			
			textRemark = textRemark.replace("BENEFICIARY:", "<br><br><b>BENEFICIARY:</b>");
			
			
		}
		
		
		
		
		return textRemark;
	}

	public void getAccountDetails(String line, BankHdrDtls bankHdrDtls) {
		String accCodeDesc = "";

		List<BAICodes> baiList = new ArrayList<BAICodes>();

		CommonBAICodes baiCodes = new CommonBAICodes();

		baiCodes.getCodeList(baiList);

		String[] array = line.split(",");

		int iLimit = (array.length - 1) / 4;

		int j = 1;

		for (int i = 0; i < iLimit; i++) {
			accCodeDesc = "";

			String typeCode = array[j];
			String amount = array[j + 1];
			String itemCount = array[j + 2];
			String fundsType = array[j + 3];

			j = j + 4;

			for (BAICodes codes : baiList) {
				if (codes.getCode().equals(typeCode)) {
					accCodeDesc = codes.getDescription();
					break;
				}
			}

			setBankStatementDetails(typeCode, amount, itemCount, bankHdrDtls);

			System.out.println(accCodeDesc + "------" + typeCode + "-" + amount + "-" + itemCount + "-" + fundsType);
		}
	}

	public void parseAccountIdentifier(String line, BankHdrDtls bankHdrDtls) {
		/* System.out.println(line); */

		List<BAICodes> baiList = new ArrayList<BAICodes>();

		CommonBAICodes baiCodes = new CommonBAICodes();

		baiCodes.getCodeList(baiList);

		String accCodeDesc = "";

		String[] accountIdentityArray = line.split(",");
		
		
		if(accountIdentityArray.length > 1)
		{
			bankHdrDtls.setRcvrId(accountIdentityArray[1]);
		}
		
		
		int iLimit = (accountIdentityArray.length - 3) / 4;

		int j = 3;

		for (int i = 0; i < iLimit; i++) {
			accCodeDesc = "";

			String typeCode = accountIdentityArray[j];
			String amount = accountIdentityArray[j + 1];
			String itemCount = accountIdentityArray[j + 2];
			String fundsType = accountIdentityArray[j + 3];

			j = j + 4;

			setBankStatementDetails(typeCode, amount, itemCount, bankHdrDtls);

			for (BAICodes codes : baiList) {
				if (codes.getCode().equals(typeCode)) {
					accCodeDesc = codes.getDescription();
					break;
				}
			}

			System.out.println(accCodeDesc + "---->" + typeCode + "-" + amount + "-" + itemCount + "-" + fundsType);
			System.out.println();
		}
	}

	public String formatAmount(String amount) {
		String main = "";
		String decimalPart = "";
		String strPattern = "^0+(?!$)";
		amount = amount.replaceAll(strPattern, "");

		if (!amount.equals("0")) {
			main = amount.substring(0, amount.length() - 2);
			decimalPart = amount.substring(amount.length() - 2, amount.length());
		} else {
			main = "0";
			decimalPart = "0";
		}

		amount = main + "." + decimalPart;

		return amount;

	}

	public void parseFileHeader(String line, BankStmtDetails bankStmtDetails) {
		/* System.out.println(line); */

		String[] fileHeaderArray = line.split(",");
		String recordType = fileHeaderArray[0];
		String senderIndetity = fileHeaderArray[1];
		String receiverIndetity = fileHeaderArray[2];
		String creationDt = fileHeaderArray[3];
		String creationTime = fileHeaderArray[4];
		String identificationNo = fileHeaderArray[5];
		String recordLength = fileHeaderArray[6];
		String blockSize = fileHeaderArray[7];
		String versionNo = fileHeaderArray[8];

		bankStmtDetails.setStmtUpldDt(formatDateWithoutTime(creationDt));
		bankStmtDetails.setStmtUpldTime(formatTime(creationTime));

	}

	public void setBankStatementDetails(String typeCode, String amount, String itemCount, BankHdrDtls bankHdrDtls) {
		if (typeCode.equals(CommonConstants.OPEN_BAL)) {
			bankHdrDtls.setOpnBal(formatAmount(amount));
		} else if (typeCode.equals(CommonConstants.CLOSE_BAL)) {
			bankHdrDtls.setCloBal(formatAmount(amount));
		} else if (typeCode.equals(CommonConstants.AVLBL_CLOSE_BAL)) {
			bankHdrDtls.setAvlblClsBal(formatAmount(amount));

		} else if (typeCode.equals(CommonConstants.TOT_CRED_TRN)) {
			bankHdrDtls.setTotCreditTrn(formatAmount(amount));

			if (itemCount.trim().length() > 0) {
				bankHdrDtls.setCrdTrnCount(itemCount);
			}
		}
		if (typeCode.equals(CommonConstants.TOT_DEBIT_TRN)) {
			bankHdrDtls.setTotDebitTrn(formatAmount(amount));

			if (itemCount.trim().length() > 0) {
				bankHdrDtls.setDebTrnCount(itemCount);
			}

		}
	}

	public String formatDateWithoutTime(String dateVal) {
		String pattern = "yyMMdd";

		String formatPattern = "yyyy-MM-dd";

		String formattedDate = "";

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

		SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(formatPattern);

		try {
			Date date = simpleDateFormat.parse(dateVal);

			formattedDate = simpleDateFormat1.format(date);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return formattedDate;
	}

	public String formatTime(String time) {
		String hour = time.substring(0, 2);
		String min = time.substring(2, 4);

		return hour + ":" + min;
	}

}
