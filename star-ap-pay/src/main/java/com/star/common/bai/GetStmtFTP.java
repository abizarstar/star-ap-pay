package com.star.common.bai;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;
import com.star.common.CommonConstants;
import com.star.common.MaintanenceResponse;
import com.star.common.ManageDirectory;
import com.star.dao.BankStmtDAO;
import com.star.dao.GLInfoDAO;
import com.star.dao.GetStmtSchDAO;
import com.star.linkage.bank.BankInfo;
import com.star.linkage.ebs.BankHdrDtls;
import com.star.linkage.ebs.BankStmtDetails;
import com.star.linkage.ebs.BankStmtList;
import com.star.linkage.ebs.BankTrnDetails;
import com.star.linkage.ebs.LockBoxDetails;
import com.star.linkage.stmt.BnkStmtInput;
import com.star.linkage.stmt.BnkStmtManOutput;

public class GetStmtFTP {

	public void getEBS(String usrId, String cmpyId) {

		String FTP_HOST = "";
		String FTP_USR = "";
		String FTP_PASS = "";
		int FTP_PORT = 0;
		String FTP_DIR = "";

		GetStmtSchDAO schDAO = new GetStmtSchDAO();

		Session session = null;
		Channel channel = null;
		boolean flg = false;
		SimpleDateFormat f = new SimpleDateFormat("MM/dd/yyyy");
		String curDateStr = "";
		String prevDateStr = "";
		String prevDate2Str = "";

		Calendar c = Calendar.getInstance();
		c.setTime(new Date());

		// manipulate date
		c.add(Calendar.DATE, 0); // same with c.add(Calendar.DAY_OF_MONTH, 1);

		Date currentDate = c.getTime();

		c.add(Calendar.DATE, -1);

		Date prevDate = c.getTime();

		c.add(Calendar.DATE, -1);

		Date prevDate2 = c.getTime();

		curDateStr = f.format(currentDate);
		prevDateStr = f.format(prevDate);
		prevDate2Str = f.format(prevDate2);

		try {

			List<BankInfo> bankInfos = schDAO.getFTPLocation("EBS");

			for (int i = 0; i < bankInfos.size(); i++) {

				FTP_HOST = bankInfos.get(i).getBnkHost();
				FTP_USR = bankInfos.get(i).getBnkFtpUser();
				FTP_PASS = bankInfos.get(i).getBnkFtpPass();
				FTP_PORT = Integer.parseInt(bankInfos.get(i).getBnkPort());
				FTP_DIR = bankInfos.get(i).getBnkSftpFilePath();

				JSch ssh = new JSch();

				session = ssh.getSession(FTP_USR, FTP_HOST, FTP_PORT);
				session.setConfig("StrictHostKeyChecking", "no");
				session.setPassword(FTP_PASS);

				session.connect();
				channel = session.openChannel("sftp");
				channel.connect();

				ChannelSftp channelSftp = (ChannelSftp) channel;

				String curDirectory = channelSftp.pwd();

				Vector filelist = channelSftp.ls(curDirectory);

				for (int j = 0; j < 3; j++) {

					BankStmtList bankStmtList = new BankStmtList();

					LsEntry entry = (LsEntry) filelist.get(j);
					System.out.println(entry.getFilename());

					bankStmtList.setFlNm(entry.getFilename());

					Vector vec = channelSftp.ls(entry.getFilename());

					if (vec != null && vec.size() == 1) {
						LsEntry details = (LsEntry) vec.get(0);
						SftpATTRS attrs = details.getAttrs();

						int t = attrs.getMTime();
						Date modTime = new Date(t * 1000L);
						System.out.println(modTime);

						String pattern = "EEE MMM dd HH:mm:ss z yyyy";

						String formatPattern = "MM/dd/yyyy";

						String formattedDate = "";

						SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

						SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(formatPattern);

						try {
							Date date = simpleDateFormat.parse(modTime.toString());

							formattedDate = simpleDateFormat1.format(date);

							System.out.println(formattedDate);

						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						BankStmtDAO bankStmtDAO = new BankStmtDAO();

						int rcrdCount = bankStmtDAO.getBankTrnExist(formattedDate, "EBS");

						if (rcrdCount == 0 && (curDateStr.equals(formattedDate) || prevDateStr.equals(formattedDate)
								|| prevDate2Str.equals(formattedDate))) {

							processStmt(usrId, cmpyId, "EBS", entry.getFilename(), channelSftp);

						}
					}

				}
			}
		} catch (JSchException e) {
			e.printStackTrace();
		} catch (SftpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (channel != null) {
				channel.disconnect();
			}
			if (session != null) {
				session.disconnect();
			}
		}

	}

	public void processStmt(String usrId, String cmpyId, String flTyp, String flNm, ChannelSftp channelSftp)
			throws Exception {
		BankStmtDAO bankStmtDAO = new BankStmtDAO();
		BankStmtDetails bankStmtDetails = new BankStmtDetails();
		LockBoxDetails lockBoxDetails = new LockBoxDetails();
		ManageDirectory directory = new ManageDirectory();
		List<BnkStmtInput> documnetNmList = new ArrayList<BnkStmtInput>();
		MaintanenceResponse<BnkStmtManOutput> starMaintenanceResponse = new MaintanenceResponse<BnkStmtManOutput>();
		starMaintenanceResponse.setOutput(new BnkStmtManOutput());
		boolean flg = false;

		BnkStmtInput bnkStmtInp = new BnkStmtInput();
		String rtnMsg = "";
		String flExtn = "txt";

		int lstBnkId = bankStmtDAO.getBnkStmtCount();

		lstBnkId = lstBnkId + 1;

		String sysFlNm = "";

		if (flTyp.equals("EBS")) {
			sysFlNm = "BAI_" + lstBnkId + "_" + formattedDate();
		} else if (flTyp.equals("LCK")) {
			sysFlNm = "Lockbox_" + lstBnkId + "_" + formattedDate();
		}

		bnkStmtInp.setFlExt(flExtn);
		bnkStmtInp.setFlNm(sysFlNm + ".txt");
		bnkStmtInp.setFlOrgNm(flNm);
		bnkStmtInp.setId(lstBnkId);

		documnetNmList.add(bnkStmtInp);

		channelSftp.get(flNm, CommonConstants.ROOT_DIR + "/" + CommonConstants.BAI_DIR + "/" + bnkStmtInp.getFlNm());

		flg = true;

		if (flg == true) {
			if (flTyp.equals("EBS")) {
				ProcessBAIFile baiFile = new ProcessBAIFile();
				baiFile.readBAIFile(CommonConstants.ROOT_DIR + CommonConstants.BAI_DIR + "/" + sysFlNm + "." + flExtn,
						bankStmtDetails);
			} else if (flTyp.equals("LCK")) {
				ProcessLockbox lockbox = new ProcessLockbox();
				lockbox.getLockboxDetails(
						CommonConstants.ROOT_DIR + CommonConstants.BAI_DIR + "/" + sysFlNm + "." + flExtn,
						lockBoxDetails);
			}

			if (documnetNmList.size() > 0 && rtnMsg.length() == 0) {

				for (int i = 0; i < documnetNmList.size(); i++) {

					if (flTyp.equals("EBS")) {
						bankStmtDAO.addBnkStmtDtls(starMaintenanceResponse, documnetNmList.get(i), bankStmtDetails,
								usrId, cmpyId);
					} else if (flTyp.equals("LCK")) {
						bankStmtDAO.addLockBoxDtls(starMaintenanceResponse, documnetNmList.get(i), lockBoxDetails,
								usrId, cmpyId);
					}

					if (starMaintenanceResponse.output.rtnSts == 0) {

						for (int j = 0; j < bankStmtDetails.getBnkHdrList().size(); j++) {
							BankHdrDtls bankHdrDtls = bankStmtDetails.getBnkHdrList().get(j);

							for (int k = 0; k < bankHdrDtls.getListBnkTrn().size(); k++) {
								BankTrnDetails bankTrnDetails = bankHdrDtls.getListBnkTrn().get(k);

								bankTrnDetails.getTypeCode();

								/* AUTO LEDGER ENTRY FOR ZBA CREDIT AND ZBA DEBIT TRANSACIONS */
								if (bankTrnDetails.getTypeCode().equals("275")) {
									GLInfoDAO dao = new GLInfoDAO();
									dao.addLedgerMain(bankStmtDetails.getUpldId(), bankHdrDtls.getHdrId(),
											bankTrnDetails, cmpyId, usrId, "ZBA CREDIT 275", bankStmtDetails.getCrtdDt());

								} else if (bankTrnDetails.getTypeCode().equals("575")) {
									GLInfoDAO dao = new GLInfoDAO();
									dao.addLedgerMain(bankStmtDetails.getUpldId(), bankHdrDtls.getHdrId(),
											bankTrnDetails, cmpyId, usrId, "ZBA DEBIT 575", bankStmtDetails.getCrtdDt());
								} else if (bankTrnDetails.getTypeCode().equals("581")) {
									GLInfoDAO dao = new GLInfoDAO();
									dao.addLedgerMain(bankStmtDetails.getUpldId(), bankHdrDtls.getHdrId(),
											bankTrnDetails, cmpyId, usrId, "Disbursing Debit 581", bankStmtDetails.getCrtdDt());
								}

							}

						}

					}
				}
			}
		}
	}

	private String formattedDate() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MMddyyyyHHmmss");
		LocalDateTime now = LocalDateTime.now();
		return dtf.format(now);
	}

	public void getLockBox(String usrId, String cmpyId) {
		
		String FTP_HOST = "";
		String FTP_USR = "";
		String FTP_PASS = "";
		int FTP_PORT = 0;
		String FTP_DIR = "";

		GetStmtSchDAO schDAO = new GetStmtSchDAO();

		Session session = null;
		Channel channel = null;
		boolean flg = false;
		SimpleDateFormat f = new SimpleDateFormat("MM/dd/yyyy");
		String curDateStr = "";
		String prevDateStr = "";
		String prevDate2Str = "";

		Calendar c = Calendar.getInstance();
		c.setTime(new Date());

		// manipulate date
		c.add(Calendar.DATE, 0); // same with c.add(Calendar.DAY_OF_MONTH, 1);

		Date currentDate = c.getTime();

		c.add(Calendar.DATE, -1);

		Date prevDate = c.getTime();

		c.add(Calendar.DATE, -1);

		Date prevDate2 = c.getTime();

		curDateStr = f.format(currentDate);
		prevDateStr = f.format(prevDate);
		prevDate2Str = f.format(prevDate2);

		try {

			List<BankInfo> bankInfos = schDAO.getFTPLocation("LCK");

			for (int i = 0; i < bankInfos.size(); i++) {

				FTP_HOST = bankInfos.get(i).getBnkHost();
				FTP_USR = bankInfos.get(i).getBnkFtpUser();
				FTP_PASS = bankInfos.get(i).getBnkFtpPass();
				FTP_PORT = Integer.parseInt(bankInfos.get(i).getBnkPort());
				FTP_DIR = bankInfos.get(i).getBnkSftpFilePath();

				JSch ssh = new JSch();

				session = ssh.getSession(FTP_USR, FTP_HOST, FTP_PORT);
				session.setConfig("StrictHostKeyChecking", "no");
				session.setPassword(FTP_PASS);

				session.connect();
				channel = session.openChannel("sftp");
				channel.connect();

				ChannelSftp channelSftp = (ChannelSftp) channel;

				String curDirectory = channelSftp.pwd();
				
				/* CHANGE DIRECTORY TO REQUIRED FILE */
				channelSftp.cd(FTP_DIR);
				curDirectory = FTP_DIR;
				
				Vector filelist = channelSftp.ls(curDirectory);

				for (int j = 0; j < 3; j++) {

					BankStmtList bankStmtList = new BankStmtList();

					LsEntry entry = (LsEntry) filelist.get(j);
					System.out.println(entry.getFilename());

					bankStmtList.setFlNm(entry.getFilename());

					Vector vec = channelSftp.ls(entry.getFilename());

					if (vec != null && vec.size() == 1) {
						LsEntry details = (LsEntry) vec.get(0);
						SftpATTRS attrs = details.getAttrs();

						int t = attrs.getMTime();
						Date modTime = new Date(t * 1000L);
						System.out.println(modTime);

						String pattern = "EEE MMM dd HH:mm:ss z yyyy";

						String formatPattern = "MM/dd/yyyy";

						String formattedDate = "";

						SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

						SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(formatPattern);

						try {
							Date date = simpleDateFormat.parse(modTime.toString());

							formattedDate = simpleDateFormat1.format(date);

							System.out.println(formattedDate);

						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

						BankStmtDAO bankStmtDAO = new BankStmtDAO();

						int rcrdCount = bankStmtDAO.getBankTrnExist(formattedDate, "LCK");

						if (rcrdCount == 0 && (curDateStr.equals(formattedDate) || prevDateStr.equals(formattedDate)
								|| prevDate2Str.equals(formattedDate))) {

							processStmt(usrId, cmpyId, "LCK", entry.getFilename(), channelSftp);

						}
					}

				}
			}
		} catch (JSchException e) {
			e.printStackTrace();
		} catch (SftpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (channel != null) {
				channel.disconnect();
			}
			if (session != null) {
				session.disconnect();
			}
		}

	
		
	}

}
