package com.star.common.bai;

import java.util.List;

public class CommonBAICodes {

	public void getCodeList(List<BAICodes> baiList)
	{
		addField("010", "Opening Balance", baiList);
		addField("015", "Closing Balance", baiList);
		addField("045", "Available Closing Balance", baiList);
		addField("175", "Check Deposit Package", baiList);
	}
	
	
	public void addField(String code, String desc, List<BAICodes> baiList)
	{
		BAICodes baiCodes = new BAICodes();
		
		baiCodes.setCode(code);
		baiCodes.setDescription(desc);
		
		baiList.add(baiCodes);
	}
	
	
}
