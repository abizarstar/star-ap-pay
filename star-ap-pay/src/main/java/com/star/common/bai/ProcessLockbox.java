package com.star.common.bai;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.star.linkage.ebs.LockBoxDetails;
import com.star.linkage.ebs.LockBoxHdrInfo;
import com.star.linkage.ebs.LockBoxTrnInfo;

public class ProcessLockbox {

	/*public static void main(String args[])
	{
		getLockboxDetails("C:\\Users\\ABIZAR\\Desktop\\Plateplus\\893858_20200929060110.txt");
	}
	*/
	public void getLockboxDetails(String fileNm, LockBoxDetails boxDetails)
	{
		List<LockBoxHdrInfo> lckBoxHdrList = new ArrayList<LockBoxHdrInfo>();
		
		List<LockBoxTrnInfo> lckBoxTrnList = new ArrayList<LockBoxTrnInfo>();
		
		LockBoxTrnInfo lockBoxTrnInfo = new LockBoxTrnInfo();
		
		try {

			File file = new File(fileNm);

			FileReader fr = new FileReader(file); // reads the file
			BufferedReader br = new BufferedReader(fr); // creates a buffering character input stream
			StringBuffer sb = new StringBuffer(); // constructs a string buffer with no characters
			String line;
			while ((line = br.readLine()) != null) {
			
				String recordType = line.substring(0, 1);
				
				if(recordType.equals("1"))
				{
					getHeader(line);
				}
				else if(recordType.equals("2"))
				{
					getServiceHeader(line);
				}
				else if(recordType.equals("4"))
				{
					String overflowEnd = line.substring(10,11);
				
					getLockboxOverflow(line, lockBoxTrnInfo, lckBoxTrnList);
					
					if(!overflowEnd.equals("9"))
					{
						String tempChkNo = lockBoxTrnInfo.getChkNo();
						
						lockBoxTrnInfo = new LockBoxTrnInfo();
						
						lockBoxTrnInfo.setChkNo(tempChkNo);
					}
				}
				else if(recordType.equals("5"))
				{
					getLockBoxHeader(line);
				}
				else if(recordType.equals("6"))
				{
					lockBoxTrnInfo = new LockBoxTrnInfo();
					
					getLockboxDetailRecord(line, lockBoxTrnInfo);
				}
				else if(recordType.equals("7"))
				{
					getBatchTotal(line, lckBoxHdrList);
				}
				else if(recordType.equals("8"))
				{
					getServiceTotal(line);
				}
				else if(recordType.equals("9"))
				{
					getDestinationTrailer(line);
				}
				
				
			}
			fr.close();
			
			boxDetails.setHdrInfoLst(lckBoxHdrList);
			boxDetails.setTrnInfoLst(lckBoxTrnList);
			
			System.out.println(lckBoxHdrList.size());
			System.out.println(lckBoxTrnList.size());
			
			System.out.println("Contents of File: ");
			System.out.println(sb.toString());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void getHeader(String line)
	{
		
		String destIdentity = line.substring(3,13);
		String routingNo = line.substring(13,23);
		String prsDt = line.substring(23,29);
		String prsTm = line.substring(29,33);
		
		
		System.out.println(destIdentity + "---");
		System.out.println(routingNo);
		System.out.println(prsDt);
		System.out.println(prsTm);
		
		
		
		
	}
	
	public void getServiceHeader(String line)
	{
		
		String destIdentity = line.substring(1,11);
		String routingNo = line.substring(11,21);
		String serviceType = line.substring(31,34);
		
		System.out.println();
		System.out.println("---SERVICE HEADER---");
		System.out.println(destIdentity + "---");
		System.out.println(routingNo);
		System.out.println(serviceType);
	}
	
	public void getLockBoxHeader(String line)
	{
		String lockBoxNo = line.substring(7,14);
		String depositDt = line.substring(14,20);
		String detination = line.substring(20,40);
		
		System.out.println();
		System.out.println("---LOCKBOX HEADER---");
		System.out.println(lockBoxNo + "---");
		System.out.println(depositDt);
		System.out.println(detination);
	}
	
	
	public void getLockboxDetailRecord(String line, LockBoxTrnInfo lockBoxTrnInfo)
	{
		String batchNo = line.substring(1,4);
		String seqNo = line.substring(4,7);
		String amount = line.substring(7,17);
		String transitRoutNo = line.substring(17,26);
		String lockboxAcctNo = line.substring(26,36);
		String checkNo = line.substring(36,45);
		
		System.out.println();
		System.out.println("---LOCKBOX DETAIL HEADER---");
		System.out.println(batchNo + "---");
		System.out.println(seqNo);
		System.out.println(amount);
		System.out.println(transitRoutNo);
		System.out.println(lockboxAcctNo + "--");
		System.out.println(checkNo);
		
		lockBoxTrnInfo.setChkNo(checkNo);
	}
	
	public void getLockboxOverflow(String line, LockBoxTrnInfo lockBoxTrnInfo, List<LockBoxTrnInfo> lockBoxTrnInfoLst)
	{
		String batchNo = line.substring(1,4);
		String seqNo = line.substring(4,7);
		String overflowRecordType = line.substring(7,8);
		String overflowSeqNo = line.substring(8,10);
		String overflowEnd = line.substring(10,11);
		String invNo = line.substring(11,27);
		String invAmount = line.substring(27,37);
		
		System.out.println();
		System.out.println("---LOCKBOX DETAIL OVERFLOW HEADER---");
		System.out.println(batchNo + "---");
		System.out.println(seqNo);
		System.out.println(overflowRecordType);
		System.out.println(overflowSeqNo);
		System.out.println(overflowEnd + "--");
		System.out.println(invNo + "--");
		System.out.println(invAmount);
		
		lockBoxTrnInfo.setInvNo(invNo);
		lockBoxTrnInfo.setSeqNo(seqNo+overflowSeqNo);
		lockBoxTrnInfo.setAmount(formatAmount(invAmount));
		lockBoxTrnInfo.setBatchId(batchNo);
		
		lockBoxTrnInfoLst.add(lockBoxTrnInfo);
		
	}
	
	
	public void getBatchTotal(String line, List<LockBoxHdrInfo> lockBoxHdrInfos)
	{
		LockBoxHdrInfo boxHdrInfo = new LockBoxHdrInfo();
		
		String batchNo = line.substring(1,4);
		String lockBoxNo = line.substring(7,14);
		String depositDt = line.substring(14,20);
		String noOfRemittance = line.substring(20,23);
		String totAmount = line.substring(23,33);
		
		System.out.println();
		System.out.println("---BATCH TOTAL RECORD---");
		System.out.println(batchNo + "---");
		System.out.println(lockBoxNo);
		System.out.println(depositDt);
		System.out.println(noOfRemittance);
		System.out.println(totAmount + "--");
		
		boxHdrInfo.setBatchId(batchNo);
		boxHdrInfo.setLockBoxNo(lockBoxNo);
		boxHdrInfo.setRcrdCount(Integer.parseInt(noOfRemittance));
		boxHdrInfo.setBatchTot(Double.parseDouble(formatAmount(totAmount)));
		boxHdrInfo.setDpstDt(formatDateWithoutTime(depositDt));
		
		lockBoxHdrInfos.add(boxHdrInfo);
		
	}
	
	public void getServiceTotal(String line)
	{
		String batchNo = line.substring(1,4);
		String lockBoxNo = line.substring(7,14);
		String depositDt = line.substring(14,20);
		String noOfRemittance = line.substring(20,24);
		String totAmount = line.substring(24,34);
		
		System.out.println();
		System.out.println("---SERVICE TOTAL RECORD---");
		System.out.println(batchNo + "---");
		System.out.println(lockBoxNo);
		System.out.println(depositDt);
		System.out.println(noOfRemittance);
		System.out.println(noOfRemittance + "--");
		
	}
	
	
	public void getDestinationTrailer(String line)
	{
	
		String noOfRecords = line.substring(1,7);
		
		System.out.println();
		System.out.println("---DESTINATION TRAILER RECORD---");

		System.out.println(noOfRecords);
		
	}
	
	public String formatDateWithoutTime(String dateVal) {
		String pattern = "yyMMdd";

		String formatPattern = "yyyy-MM-dd";

		String formattedDate = "";

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

		SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(formatPattern);

		try {
			Date date = simpleDateFormat.parse(dateVal);

			formattedDate = simpleDateFormat1.format(date);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return formattedDate;
	}

	public String formatTime(String time) {
		String hour = time.substring(0, 2);
		String min = time.substring(2, 4);

		return hour + ":" + min;
	}
	
	public String formatAmount(String amount) {
		String main = "";
		String decimalPart = "";
		String strPattern = "^0+(?!$)";
		amount = amount.replaceAll(strPattern, "");

		if (!amount.equals("0")) {
			main = amount.substring(0, amount.length() - 2);
			decimalPart = amount.substring(amount.length() - 2, amount.length());
		} else {
			main = "0";
			decimalPart = "0";
		}

		amount = main + "." + decimalPart;

		return amount;

	}
	
}
