package com.star.common.bai;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReadBAIFile {
	
	public static int lstProcessedLine = 0;
	
	public static void main(String args[]) {
	
		try {			
			File file = new File("C:\\Users\\ABIZAR\\Desktop\\BAI-Sample-2.txt"); // creates a new file instance
			FileReader fr = new FileReader(file); // reads the file
			BufferedReader br = new BufferedReader(fr); // creates a buffering character input stream
			StringBuffer sb = new StringBuffer(); // constructs a string buffer with no characters
			String line;
			while ((line = br.readLine()) != null) {
				
				processLine(line);
				
				/*sb.append(line); // appends line to string buffer
				sb.append("\n"); // line feed
*/			}
			fr.close(); // closes the stream and release the resources
			System.out.println("Contents of File: ");
			System.out.println(sb.toString()); // returns a string that textually represents the object
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void processLine(String line)
	{
		String recordType = line.substring(0,2);
		
		if(recordType.equals("01")) // FILE HEADER
		{
			parseFileHeader(line);
		}
		else if(recordType.equals("02")) // GROUP HEADER
		{
			parseGroupHeader(line);
		}
		else if(recordType.equals("03")) // ACCOUNT IDENTIFIER
		{	
			parseAccountIdentifier(line);
			lstProcessedLine= 3;
		}
		else if(recordType.equals("16")) // TRANSACTION DETAIL
		{
			parseTransactionDetail(line);
			lstProcessedLine= 16;
		}	
		else if(recordType.equals("88")) // CONTINUATION
		{
			parseAdditionalDetals(line);
		}
		else if(recordType.equals("49")) // ACCOUNT TRAILER
		{
			parseAccountTrailer(line);
			lstProcessedLine= 0;
		}
		else if(recordType.equals("98")) // GROUP TRAILER
		{
			parseGroupTrailer(line);
			lstProcessedLine= 0;
		}
		else if(recordType.equals("99")) // FILE TRAILER
		{
			parseFileTrailer(line);
			lstProcessedLine= 0;
		}	
		
	}
	
	public static void parseFileHeader(String line)
	{
		/*System.out.println(line);*/
		
		String[] fileHeaderArray = line.split(","); 
		String recordType = fileHeaderArray[0];
		String senderIndetity = fileHeaderArray[1];
		String receiverIndetity = fileHeaderArray[2];
		String creationDt = fileHeaderArray[3];
		String creationTime = fileHeaderArray[4];
		String identificationNo = fileHeaderArray[5];
		String recordLength = fileHeaderArray[6];
		String blockSize = fileHeaderArray[7];
		String versionNo = fileHeaderArray[8];		
	}
	
	public static void parseGroupHeader(String line)
	{
		/*System.out.println(line);*/
		
		String[] groupHeaderArray = line.split(","); 
		String recordType = groupHeaderArray[0];
		String receiverIndetity = groupHeaderArray[1];
		String originatorIdentity = groupHeaderArray[2];
		String groupStatus = groupHeaderArray[3];
		String asOfDate = groupHeaderArray[4];
		String asOfTime = groupHeaderArray[5];
		String currencyCode = groupHeaderArray[6];
		String asOfDataModifier = groupHeaderArray[7];		
	}
	
	public static void parseAccountIdentifier(String line)
	{
		/*System.out.println(line);*/
		
		List<BAICodes> baiList = new ArrayList<BAICodes>();
		
		CommonBAICodes baiCodes = new CommonBAICodes();
		
		baiCodes.getCodeList(baiList);
		
		String accCodeDesc = "";
		
		String[] accountIdentityArray = line.split(","); 
		
		String recordType = accountIdentityArray[0];
		String customerAccount = accountIdentityArray[1];
		String currencyCode = accountIdentityArray[2];
		
		
		/*System.out.println(accountIdentityArray.length - 3);*/
		
		int iLimit = (accountIdentityArray.length - 3) / 4 ;
		
		int j = 3;
		
		for(int i = 0 ; i < iLimit ; i++)
		{
			accCodeDesc = "";
			
			String typeCode = accountIdentityArray[j];
			String amount = accountIdentityArray[j+1];
			String itemCount = accountIdentityArray[j+2];
			String fundsType = accountIdentityArray[j+3];
			
			j= j + 4;
			
			for (BAICodes codes : baiList){
		         if (codes.getCode().equals(typeCode)){
		        	 accCodeDesc = codes.getDescription();
		        	 break;
		         }
		      }
			
			
			System.out.println(accCodeDesc + "---->" + typeCode + "-" + amount + "-" + itemCount + "-" + fundsType);
			System.out.println();
		}
	}
	
	public static void parseTransactionDetail(String line)	{
		
		List<BAICodes> baiList = new ArrayList<BAICodes>();
		
		CommonBAICodes baiCodes = new CommonBAICodes();
		
		baiCodes.getCodeList(baiList);
		
		/*System.out.println(line);*/
		
		String[] transactionArray = line.split(","); 
		String recordType = transactionArray[0];
		String typCode = transactionArray[1];
		String amount = transactionArray[2];
		String fundsType = transactionArray[3];
		
		if(fundsType.equals("Z"))
		{
			String bankReference = transactionArray[4];
			String customerReference = transactionArray[5];		
		}
	}
	
	public static void parseAccountTrailer(String line)	{
		
		System.out.println(line);
		
		String[] array = line.split(","); 
		String recordType = array[0];
		String accountControlTotal = array[1];
		String noOfRecords = array[2];		
	}
	
	
	public static void parseGroupTrailer(String line)	{
		
		System.out.println(line);
		
		String[] array = line.split(","); 
		String recordType = array[0];
		String groupControlTotal = array[1];
		String noOfAccounts = array[2];
		String noOfRecords = array[3];
	}
	
	
public static void parseFileTrailer(String line)	{
		
		/*System.out.println(line);*/
		
		String[] array = line.split(","); 
		String recordType = array[0];
		String fileControlTotal = array[1];
		String noOfGroups= array[2];
		String noOfRecords = array[3];
	}
	
	public static void parseAdditionalDetals(String line)
	{
		/*System.out.println();*/
		
		if(lstProcessedLine == 3)
		{
			getAccountDetails(line);
		}
		else if(lstProcessedLine == 16)
		{
			getTransactionDetail(line);
		}
	}
	
	public static void getAccountDetails(String line)
	{
		String accCodeDesc = "";
		
		List<BAICodes> baiList = new ArrayList<BAICodes>();
		
		CommonBAICodes baiCodes = new CommonBAICodes();
		
		baiCodes.getCodeList(baiList);
		
		String[] array = line.split(","); 
		
		int iLimit = (array.length - 1) / 4 ;
		
		int j = 1;
		
		for(int i = 0 ; i < iLimit ; i++)
		{
			accCodeDesc = "";
			
			String typeCode = array[j];
			String amount = array[j+1];
			String itemCount = array[j+2];
			String fundsType = array[j+3];
			
			j= j + 4;
			
			for (BAICodes codes : baiList){
		         if (codes.getCode().equals(typeCode)){
		        	 accCodeDesc = codes.getDescription();
		        	 break;
		         }
		      }
			
			System.out.println(accCodeDesc + "------" + typeCode + "-" + amount + "-" + itemCount + "-" + fundsType);
		}
	}
	
	public static void getTransactionDetail(String line)
	{
		String[] array = line.split(","); 
		
		int iLimit = (array.length - 1) ;
		
		int j = 1;
		
		for(int i = 0 ; i < iLimit ; i++)
		{
			String textRemark = array[i+1];
			
			j= j + 4;
			
			System.out.println(textRemark);
		}
	}

}
