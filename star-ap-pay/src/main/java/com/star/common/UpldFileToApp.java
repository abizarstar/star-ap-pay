package com.star.common;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import com.star.common.abbyy.PerformOCR;
import com.star.dao.CstmDocInfoDAO;
import com.star.dao.CstmVchrInfoDAO;
import com.star.linkage.cstmdocinfo.CstmDocInfoInput;
import com.star.linkage.cstmdocinfo.CstmDocInfoManOutput;
import com.star.linkage.cstmvchrinfo.CstmVchrInfoInput;
import com.star.linkage.cstmvchrinfo.CstmVchrInfoManOutput;
import com.star.service.WkfMapService;

public class UpldFileToApp {

	public void processFile(String cmpyId, String usrId, String srcFlg, String flNm, byte[] fileByteArray)
	{
		ManageDirectory directory = new ManageDirectory();

		int fileCount = 0;

		ValidateValue validateValue = new ValidateValue();
		
		InputStream is = new ByteArrayInputStream(fileByteArray);
		
		/* Calling Doc Sequence to generate unique Doc Name */
		CstmDocInfoDAO cstmDocInfoDAO = new CstmDocInfoDAO();

		int ctlNo = cstmDocInfoDAO.getCtlNo();

		String strCtlNo = String.valueOf(ctlNo);

		String fileName = "0000000000".substring(strCtlNo.length()) + strCtlNo;

		String docNmIntrnl = "upload" + "-" + fileName;

		String fileExtn = validateValue.getFileExtension(flNm);

		/* CREATE FILE BASED ON THE INPUT */
		directory.createFile(docNmIntrnl, fileExtn, is);

		String imgFlPath = docNmIntrnl + "." + fileExtn;
		String txtFlPath = docNmIntrnl + ".txt";

		PerformOCR performOCR = new PerformOCR();

		String[] inputArray = new String[3];

		inputArray[0] = "recognize";
		inputArray[1] = CommonConstants.ROOT_DIR + imgFlPath;
		inputArray[2] = CommonConstants.ROOT_DIR + txtFlPath;

		performOCR.performOcrOperation(inputArray);

		MaintanenceResponse<CstmDocInfoManOutput> starMaintenanceDocResponse = new MaintanenceResponse<CstmDocInfoManOutput>();

		starMaintenanceDocResponse.setOutput(new CstmDocInfoManOutput());

		CstmDocInfoInput cstmDocInfoInput = new CstmDocInfoInput();

		cstmDocInfoInput.setCtlNo(ctlNo);
		// TODO
		cstmDocInfoInput.setGatCtlNo(1);
		cstmDocInfoInput.setFlPdf(imgFlPath);
		cstmDocInfoInput.setFlPdfName(flNm);
		cstmDocInfoInput.setFlTxt(txtFlPath);
		cstmDocInfoInput.setSrcFlg(srcFlg);
		cstmDocInfoInput.setIsPrs(false);
		cstmDocInfoInput.setUpldBy(usrId);

		cstmDocInfoDAO.addCstmDocument(starMaintenanceDocResponse, cstmDocInfoInput);

		MaintanenceResponse<CstmVchrInfoManOutput> starMaintenanceResponse = new MaintanenceResponse<CstmVchrInfoManOutput>();

		starMaintenanceResponse.setOutput(new CstmVchrInfoManOutput());

		CstmVchrInfoDAO cstmVchrInfoDAO = new CstmVchrInfoDAO();

		List<CstmVchrInfoInput> cstmVchrInfo = new ArrayList<CstmVchrInfoInput>();

		ParseInvoice parseInvoice = new ParseInvoice();

		parseInvoice.parseInvoiceData(cmpyId, usrId, txtFlPath, cstmVchrInfo, ctlNo, cstmVchrInfoDAO);

		WkfMapService mapService = new WkfMapService();

		
		for(int i =0; i < cstmVchrInfo.size(); i++)
		{
		
		/* VALIDATE WORK FLOW SERVICE */
			mapService.validateVouWkf(cmpyId, usrId, cstmVchrInfo.get(i));

			cstmVchrInfoDAO.addCstmVoucher(starMaintenanceResponse, cstmVchrInfo.get(i));
		}

		fileCount = fileCount + 1;
	
	}
	
}
