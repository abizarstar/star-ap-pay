package com.star.common;

import java.io.IOException;
import java.security.AlgorithmParameters;
import java.security.Key;
import java.util.Base64;
import java.util.StringTokenizer;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import com.star.linkage.auth.AuthBrowseOutput;

public class ValidateExecParams {

	String CIPHER_NAME = "Blowfish";
	String CIPHER_CONFIG = "CBC/PKCS5Padding";
	String SECRETKEY = "85C9D9182EDF7FE4C9108641B40DC2F2";
	String IV = "313C824124AE4239";
	String SERIALIZATION_DELIMITER = "^";
	String KEY_VALUE_DELIMITER = "=";
	String KEY_TOKEN_NO = "TknNo";
	String KEY_COMPANY_ID = "CmpyId";
	String KEY_LOGIN_ID = "LgnId";
	String AUTH_TOKEN = "Auth";

	public void deciferJobString(String encryptedJobOrderString, BrowseResponse<AuthBrowseOutput> starBrowseAuthToken) {
		String serializedString = null;
		try {
			Cipher decrypter = getDecrypter();

			byte[] ciphertext = Base64.getDecoder().decode(encryptedJobOrderString);

			// byte[] ciphertext = new
			// BASE64Decoder().decodeBuffer(encryptedJobOrderString);

			serializedString = new String(decrypter.doFinal(ciphertext));
		}

		catch (BadPaddingException bpe) {
			System.err.println(">>>>>>>>>>>> Encrypted JobOrder String that caused BadPaddingException:"
					+ encryptedJobOrderString);
			throw new RuntimeException(bpe);
		} catch (IllegalBlockSizeException ibe) {
			System.err.println(">>>>>>>>>>>> Encrypted JobOrder String that caused IllegalBlockSizeException:"
					+ encryptedJobOrderString);
			throw new RuntimeException(ibe);
		}

		StringTokenizer tokenizer = new StringTokenizer(serializedString, SERIALIZATION_DELIMITER);

		int tokenCount = tokenizer.countTokens();

		if (tokenCount == 0) {
			throw new IllegalArgumentException("Incomplete Exec Reference: " + serializedString);
		}

		while (tokenizer.hasMoreTokens()) {
			String keyValueToken = tokenizer.nextToken();

			if (keyValueToken.contains(AUTH_TOKEN)) {
				String value = keyValueToken.substring(AUTH_TOKEN.length() + 1, keyValueToken.length());
				starBrowseAuthToken.output.authToken = value;
			} else if (keyValueToken.contains(KEY_COMPANY_ID)) {
				String value = keyValueToken.substring(KEY_COMPANY_ID.length() + 1, keyValueToken.length());
				starBrowseAuthToken.output.usrTyp = value;
			} else if (keyValueToken.contains(KEY_LOGIN_ID)) {
				String value = keyValueToken.substring(KEY_LOGIN_ID.length() + 1, keyValueToken.length());
				starBrowseAuthToken.output.loginUser = value;
			}

			/*
			 * StringTokenizer valueTokenizer = new StringTokenizer(keyValueToken,
			 * KEY_VALUE_DELIMITER);
			 * 
			 * String key = valueTokenizer.nextToken(); String value =
			 * valueTokenizer.nextToken();
			 * 
			 * if (key.equalsIgnoreCase(AUTH_TOKEN)) { System.out.println(value);
			 * starBrowseAuthToken.output.authToken = value;
			 * 
			 * } else if (key.equalsIgnoreCase(KEY_COMPANY_ID)) {
			 * starBrowseAuthToken.output.usrTyp = value; } else if
			 * (key.equalsIgnoreCase(KEY_LOGIN_ID)) { System.out.println(value);
			 * starBrowseAuthToken.output.loginUser = value; }
			 */
		}

	}

	public Cipher getDecrypter() {

		Cipher decrypter = null;

		try {

			byte[] keyBytes = parseHexString(SECRETKEY);
			byte[] ivBytes = parseHexString(IV);

			Key key = new SecretKeySpec(keyBytes, CIPHER_NAME);

			IvParameterSpec iv = new IvParameterSpec(ivBytes);
			AlgorithmParameters params = AlgorithmParameters.getInstance(CIPHER_NAME);
			params.init(iv);

			decrypter = Cipher.getInstance(CIPHER_NAME + "/" + CIPHER_CONFIG);
			decrypter.init(Cipher.DECRYPT_MODE, key, params);

		} catch (Exception e) {
			throw new ExceptionInInitializerError(e);
		}

		return decrypter;
	}

	public byte[] parseHexString(String hexString) {

		if (hexString != null) {

			if (hexString.length() % 2 == 1) {
				throw new IllegalArgumentException("Hex string parameter must contain an even number of characters.");
			}

			byte[] bytes = new byte[hexString.length() / 2];
			int end = hexString.length() - 2;
			int index = 0;
			try {
				for (int pos = 0; pos <= end; pos += 2) {
					bytes[index++] = (byte) Integer.parseInt(hexString.substring(pos, pos + 2), 16);
				}
			} catch (NumberFormatException e) {
				throw e;
			}
			return bytes;
		}

		return new byte[0];
	}

}
