package com.star.common.remit;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.star.common.CommonConstants;
import com.star.common.CommonFunctions;
import com.star.linkage.company.Address;
import com.star.linkage.cstmdocinfo.VchrInfo;

public class RemitDoc {

	double totValue = 0;
	double totdiscVal = 0;
	String cry = "";

	public String genDocument(List<VchrInfo> vchrInfoList, Address cmpyAddress, Address venAddress, String pmntDt) {

		totValue = 0;
		totdiscVal = 0;
		String cry = "";
		
		Document document = new Document();

		String venId = "";
		String venNm = "";

		String flNm = CommonConstants.ROOT_DIR + "/Remittance_" + (new Date()).getTime() + ".pdf";

		try {
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(flNm));
			document.open();

			venId = vchrInfoList.get(0).getVchrVenId();
			venNm = vchrInfoList.get(0).getVchrVenNm();

			BaseColor color = new BaseColor(199, 211, 216);
			PdfPTable tableHeader = new PdfPTable(new float[] { 1, 1 });
			tableHeader.setWidthPercentage(100); // Width 100%
			tableHeader.setSpacingBefore(1f); // Space before table
			tableHeader.setSpacingAfter(1f); // Space after table

			PdfPCell cell = new PdfPCell(addShipFromSection(cmpyAddress));
			cell.setPadding(3);
			cell.setBorderColor(BaseColor.WHITE);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_MIDDLE);
			tableHeader.addCell(cell);

			cell = new PdfPCell(addHeaderSection(venId, venNm, pmntDt));
			cell.setBorderColor(BaseColor.WHITE);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_TOP);
			tableHeader.addCell(cell);

			Rectangle rect = new Rectangle(577, 825, 18, 15); // you can resize rectangle
			rect.enableBorderSide(1);
			rect.enableBorderSide(2);
			rect.enableBorderSide(4);
			rect.enableBorderSide(8);
			rect.setBorderColor(color);
			rect.setBorderWidth(1);

			int totalRows = vchrInfoList.size();
			int pageCount = 1;

			for (int i = 0; i < totalRows; i++) {
				if (i % 22 == 0 && i == 0) {
					document.add(rect);

					document.add(tableHeader);
					document.add(new Paragraph(" "));
					/* document.add(addBillToSection(venAddress)); */
					document.add(new Paragraph(" "));

					if (totalRows < 22) {
						document.add(addInvoiceDetails(vchrInfoList, 0, totalRows));
					} else {
						document.add(addInvoiceDetails(vchrInfoList, 0, 22));
					}
					document.add(new Paragraph(" "));

					Date date = new Date();
					BaseColor colorDt = new BaseColor(112, 113, 113);
					Font dateTmFont = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL);
					Phrase field = new Phrase(date.toString(), dateTmFont);
					dateTmFont.setColor(colorDt);
					ColumnText ct = new ColumnText(writer.getDirectContent());
					ct.setSimpleColumn(820, 20, 450, 0);
					ct.setText(field);
					ct.go();

					field = new Phrase("Page " + pageCount, dateTmFont);
					ct = new ColumnText(writer.getDirectContent());
					ct.setSimpleColumn(450, 35, 280, 15);
					ct.setText(field);
					ct.go();

					// document.newPage();/* FOR NEW PAGE */

				} else if (i % 22 == 0 && i > 0) {
					document.newPage();
					document.add(rect);

					document.add(tableHeader);
					document.add(new Paragraph(" "));
					/*document.add(addBillToSection(venAddress));*/
					document.add(new Paragraph(" "));

					int invRang = 0;

					if (totalRows < i + 22) {
						invRang = totalRows;
					} else {
						invRang = i + 22;
					}

					pageCount = pageCount + 1;

					document.add(addInvoiceDetails(vchrInfoList, i, invRang));
					document.add(new Paragraph(" "));
					// document.newPage();/* FOR NEW PAGE */

					Date date = new Date();
					BaseColor colorDt = new BaseColor(112, 113, 113);
					Font dateTmFont = new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL);
					Phrase field = new Phrase(date.toString(), dateTmFont);
					dateTmFont.setColor(colorDt);
					ColumnText ct = new ColumnText(writer.getDirectContent());
					ct.setSimpleColumn(820, 20, 450, 0);
					ct.setText(field);
					ct.go();

					field = new Phrase("Page.." + pageCount, dateTmFont);
					ct = new ColumnText(writer.getDirectContent());
					ct.setSimpleColumn(450, 35, 280, 15);
					ct.setText(field);
					ct.go();
				}
			}

			PdfPTable tableFooter = new PdfPTable(new float[] { 1, 1 });
			tableFooter.setWidthPercentage(100); // Width 100%
			tableFooter.setSpacingBefore(2f); // Space before table
			tableFooter.setSpacingAfter(2f); // Space after table

			cell = new PdfPCell(addRemarks());
			cell.setBorderColor(color);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_TOP);
			cell.setMinimumHeight(100);
			tableFooter.addCell(cell);

			cell = new PdfPCell(addFooterSection());
			cell.setBorderColor(color);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_TOP);
			tableFooter.addCell(cell);

			cell = new PdfPCell(new Paragraph());
			cell.setBorderColor(color);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_TOP);
			tableFooter.addCell(cell);

			cell = new PdfPCell(new Paragraph());
			cell.setBorderColor(color);
			cell.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell.setVerticalAlignment(Element.ALIGN_TOP);
			tableFooter.addCell(cell);

			document.add(tableFooter);

			/*
			 * document.add(rect);
			 * 
			 * document.add(tableHeader); document.add(new Paragraph(" "));
			 * document.add(addBillToSection()); document.add(new Paragraph(" "));
			 * document.add(addInvoiceDetails(0, 8)); document.add(new Paragraph(" "));
			 */
			// document.newPage();/* FOR NEW PAGE */

			/* document.add(addFooterSection()); */

			document.close();
			writer.close();
		} catch (DocumentException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return flNm;

	}

	public PdfPTable addInvoiceDetails(List<VchrInfo> vchrInfoList, int start, int end) {
		PdfPTable tableVoucher = new PdfPTable(new float[] { 0.7f, 1.2f, 1.5f, 2.5f, 1.25f, 1, 1 }); // 3 columns.
		tableVoucher.setWidthPercentage(100); // Width 100%
		tableVoucher.setSpacingBefore(2f); // Space before table
		tableVoucher.setSpacingAfter(2f); // Space after table

		BaseColor color = new BaseColor(199, 211, 216);

		Font boldFont = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD);

		Phrase field = new Phrase("S.No.", boldFont);
		PdfPCell cellSNo = new PdfPCell(field);
		cellSNo.setBackgroundColor(color);
		cellSNo.setPadding(5);
		cellSNo.setBorderColor(color);
		cellSNo.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellSNo.setVerticalAlignment(Element.ALIGN_MIDDLE);

		field = new Phrase("Date", boldFont);
		PdfPCell cellDate = new PdfPCell(field);
		cellDate.setBackgroundColor(color);
		cellDate.setBorderColor(color);
		cellDate.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellDate.setVerticalAlignment(Element.ALIGN_MIDDLE);

		field = new Phrase("Our Reference", boldFont);
		PdfPCell cellOurRef = new PdfPCell(field);
		cellOurRef.setBackgroundColor(color);
		cellOurRef.setBorderColor(color);
		cellOurRef.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellOurRef.setVerticalAlignment(Element.ALIGN_MIDDLE);

		field = new Phrase("Your Reference", boldFont);
		PdfPCell cellYourRef = new PdfPCell(field);
		cellYourRef.setBackgroundColor(color);
		cellYourRef.setBorderColor(color);
		cellYourRef.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellYourRef.setVerticalAlignment(Element.ALIGN_MIDDLE);

		field = new Phrase("Amount", boldFont);
		PdfPCell cellInvAmt = new PdfPCell(field);
		cellInvAmt.setBackgroundColor(color);
		cellInvAmt.setBorderColor(color);
		cellInvAmt.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cellInvAmt.setVerticalAlignment(Element.ALIGN_MIDDLE);

		field = new Phrase("Discount", boldFont);
		PdfPCell cellDiscAmt = new PdfPCell(field);
		cellDiscAmt.setBackgroundColor(color);
		cellDiscAmt.setBorderColor(color);
		cellDiscAmt.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cellDiscAmt.setVerticalAlignment(Element.ALIGN_MIDDLE);

		field = new Phrase("Currency", boldFont);
		PdfPCell cellCry = new PdfPCell(field);
		cellCry.setBackgroundColor(color);
		cellCry.setBorderColor(color);
		cellCry.setHorizontalAlignment(Element.ALIGN_LEFT);
		cellCry.setVerticalAlignment(Element.ALIGN_MIDDLE);

		tableVoucher.addCell(cellSNo);
		tableVoucher.addCell(cellDate);
		tableVoucher.addCell(cellOurRef);
		tableVoucher.addCell(cellYourRef);
		tableVoucher.addCell(cellInvAmt);
		tableVoucher.addCell(cellDiscAmt);
		tableVoucher.addCell(cellCry);

		Font normalFont = new Font(Font.FontFamily.HELVETICA, 9, Font.NORMAL);

		for (int i = start; i < end; i++) {
			int k = i + 1;

			totValue = totValue + vchrInfoList.get(i).getVchrAmt();
			
			if(vchrInfoList.get(i).getVchrAmt() != vchrInfoList.get(i).getTotAmt())
			{
				totdiscVal = totdiscVal + vchrInfoList.get(i).getDiscAmt();
			}
			
			if (vchrInfoList.get(i).getVchrCry().equals("USD")) {
				cry = "$";
			} else {
				cry = "";
			}

			field = new Phrase(String.valueOf(k), normalFont);
			cellSNo = new PdfPCell(field);
			cellSNo.setPadding(6);
			cellSNo.setBorderColor(color);
			cellSNo.setPaddingLeft(5);
			cellSNo.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellSNo.setVerticalAlignment(Element.ALIGN_MIDDLE);

			field = new Phrase(vchrInfoList.get(i).getVchrInvDtStr(), normalFont);
			cellDate = new PdfPCell(field);
			cellDate.setBorderColor(color);
			cellDate.setPaddingLeft(5);
			cellDate.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellDate.setVerticalAlignment(Element.ALIGN_MIDDLE);

			field = new Phrase(vchrInfoList.get(i).getVchrPfx() + "-" + vchrInfoList.get(i).getVchrNo(), normalFont);
			cellOurRef = new PdfPCell(field);
			cellOurRef.setBorderColor(color);
			cellOurRef.setPaddingRight(5);
			cellOurRef.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellOurRef.setVerticalAlignment(Element.ALIGN_MIDDLE);

			field = new Phrase(vchrInfoList.get(i).getVchrInvNo(), normalFont);
			cellYourRef = new PdfPCell(field);
			cellYourRef.setBorderColor(color);
			cellYourRef.setPaddingRight(5);
			cellYourRef.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellYourRef.setVerticalAlignment(Element.ALIGN_MIDDLE);

			field = new Phrase(vchrInfoList.get(i).getVchrAmtStr(), normalFont);
			cellInvAmt = new PdfPCell(field);
			cellInvAmt.setBorderColor(color);
			cellInvAmt.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cellInvAmt.setVerticalAlignment(Element.ALIGN_MIDDLE);
			
			if(vchrInfoList.get(i).getVchrAmt() == vchrInfoList.get(i).getTotAmt())
			{
				field = new Phrase("-", normalFont);
			}
			else
			{
				field = new Phrase(vchrInfoList.get(i).getDiscAmtStr(), normalFont);
			}
			
			
			cellDiscAmt = new PdfPCell(field);
			cellDiscAmt.setBorderColor(color);
			cellDiscAmt.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cellDiscAmt.setVerticalAlignment(Element.ALIGN_MIDDLE);

			field = new Phrase(vchrInfoList.get(i).getVchrCry(), normalFont);
			cellCry = new PdfPCell(field);
			cellCry.setBorderColor(color);
			cellCry.setHorizontalAlignment(Element.ALIGN_LEFT);
			cellCry.setVerticalAlignment(Element.ALIGN_MIDDLE);

			tableVoucher.addCell(cellSNo);
			tableVoucher.addCell(cellDate);
			tableVoucher.addCell(cellOurRef);
			tableVoucher.addCell(cellYourRef);
			tableVoucher.addCell(cellInvAmt);
			tableVoucher.addCell(cellDiscAmt);
			tableVoucher.addCell(cellCry);
		}

		return tableVoucher;
	}

	public PdfPTable addHeaderSection(String venId, String venNm, String pmntDt) {
		PdfPTable table = new PdfPTable(new float[] { 1f, 1.5f }); // 3 columns.
		table.setWidthPercentage(50); // Width 100%
		table.setSpacingBefore(10f); // Space before table
		table.setSpacingAfter(10f); // Space after table
		BaseColor color = new BaseColor(199, 211, 216);
		table.setHorizontalAlignment(Element.ALIGN_RIGHT);

		Font boldFont = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		Phrase firstLine = new Phrase("REMITTANCE ADVICE", boldFont);

		PdfPCell cell1 = new PdfPCell(firstLine);
		cell1.setBorderColor(color);
		cell1.setBackgroundColor(BaseColor.WHITE);
		cell1.setPaddingLeft(10);
		cell1.setPadding(8);
		cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		cell1.setColspan(2);
		table.addCell(cell1);

		cell1 = new PdfPCell(new Paragraph("Date"));
		cell1.setBorderColor(color);
		cell1.setBackgroundColor(color);
		cell1.setPaddingLeft(10);
		cell1.setPadding(8);
		cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

		table.addCell(cell1);

		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		String strDate = formatter.format(date);

		cell1 = new PdfPCell(new Paragraph(pmntDt));
		cell1.setBorderColor(color);
		cell1.setPaddingLeft(10);
		cell1.setPadding(8);
		cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

		table.addCell(cell1);

		PdfPCell cell2 = new PdfPCell(new Paragraph("Vendor ID"));
		cell2.setBorderColor(color);
		cell2.setBackgroundColor(color);
		cell2.setPaddingLeft(10);
		cell2.setPadding(8);
		cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

		table.addCell(cell2);

		cell2 = new PdfPCell(new Paragraph(venId));
		cell2.setBorderColor(color);
		cell2.setPaddingLeft(10);
		cell2.setPadding(8);
		cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

		table.addCell(cell2);

		cell2 = new PdfPCell(new Paragraph("Vendor Name"));
		cell2.setBorderColor(color);
		cell2.setBackgroundColor(color);
		cell2.setPaddingLeft(10);
		cell2.setPadding(8);
		cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

		table.addCell(cell2);

		cell2 = new PdfPCell(new Paragraph(venNm));
		cell2.setBorderColor(color);
		cell2.setPaddingLeft(10);
		cell2.setPadding(8);
		cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

		table.addCell(cell2);

		cell2 = new PdfPCell(new Paragraph(""));
		cell2.setBorderColorTop(color);
		cell2.setPaddingLeft(10);
		cell2.setPadding(8);
		cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

		table.addCell(cell2);

		cell2 = new PdfPCell(new Paragraph(""));
		cell2.setBorderColorTop(color);
		cell2.setPaddingLeft(10);
		cell2.setPadding(8);
		cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

		table.addCell(cell2);

		cell2 = new PdfPCell(new Paragraph(""));
		cell2.setBorderColor(BaseColor.WHITE);
		cell2.setPaddingLeft(10);
		cell2.setPadding(8);
		cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

		table.addCell(cell2);

		cell2 = new PdfPCell(new Paragraph(""));
		cell2.setBorderColor(BaseColor.WHITE);
		cell2.setPaddingLeft(10);
		cell2.setPadding(8);
		cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

		table.addCell(cell2);

		cell2 = new PdfPCell(new Paragraph(""));
		cell2.setBorderColor(BaseColor.WHITE);
		cell2.setPaddingLeft(10);
		cell2.setPadding(8);
		cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

		table.addCell(cell2);

		cell2 = new PdfPCell(new Paragraph(""));
		cell2.setBorderColor(BaseColor.WHITE);
		cell2.setPaddingLeft(10);
		cell2.setPadding(8);
		cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

		table.addCell(cell2);

		return table;

	}

	public PdfPTable addShipFromSection(Address cmpyAddress) {
		PdfPTable table = new PdfPTable(1); // 3 columns.
		table.setWidthPercentage(40); // Width 100%
		table.setSpacingBefore(5f); // Space before table
		table.setSpacingAfter(5f); // Space after table
		table.setHorizontalAlignment(Element.ALIGN_LEFT);

		BaseColor color = new BaseColor(199, 211, 216);
		Font boldFont = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		Phrase firstLine = new Phrase("Bill To", boldFont);

		/*
		 * PdfPCell cell1 = new PdfPCell(firstLine);
		 * cell1.setBorderColor(BaseColor.WHITE); cell1.setBackgroundColor(color);
		 * cell1.setPadding(6); cell1.setPaddingLeft(10);
		 * cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		 * cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);
		 * 
		 * table.addCell(cell1);
		 */

		Image image1 = null;
		try {

			ClassLoader classLoader = getClass().getClassLoader();
			File file = new File(classLoader.getResource("logo-cmpy.jpg").getFile());

			String flPath = URLDecoder.decode(file.getAbsolutePath(), "UTF-8");
			
			System.out.println(file.getAbsolutePath());

			image1 = Image.getInstance(flPath);

			// Scale to new height and new width of image
			image1.scaleAbsolute(200, 50);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadElementException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		PdfPCell cell1 = new PdfPCell(image1);
		cell1.setBorderColor(BaseColor.WHITE);
		cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

		table.addCell(cell1);

		if (cmpyAddress.getAddr1() != null) {
			cell1 = new PdfPCell(new Paragraph(cmpyAddress.getAddr1() + ", " + cmpyAddress.getAddr2()));
			cell1.setBorderColor(BaseColor.WHITE);
			cell1.setPaddingLeft(10);
			cell1.setPadding(6);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

			table.addCell(cell1);
		}

		if (cmpyAddress.getAddr2() != null) {
			cell1 = new PdfPCell(
					new Paragraph(cmpyAddress.getCity() + " " + cmpyAddress.getStProv() + " " + cmpyAddress.getPcd()));
			cell1.setBorderColor(BaseColor.WHITE);
			cell1.setPaddingLeft(10);
			cell1.setPadding(6);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

			table.addCell(cell1);
		}

		if (cmpyAddress.getAddr3() != null) {
			cell1 = new PdfPCell(new Paragraph("accountspayable@plateplus.com"));
			cell1.setBorderColor(BaseColor.WHITE);
			cell1.setPaddingLeft(10);
			cell1.setPadding(6);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

			table.addCell(cell1);
		}

		if (cmpyAddress.getCity() != null) {
			cell1 = new PdfPCell(new Paragraph(""));
			cell1.setBorderColor(BaseColor.WHITE);
			cell1.setPaddingLeft(10);
			cell1.setPadding(6);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

			table.addCell(cell1);
		}

		if (cmpyAddress.getCty() != null) {
			cell1 = new PdfPCell(new Paragraph(""));
			cell1.setBorderColor(BaseColor.WHITE);
			cell1.setPaddingLeft(10);
			cell1.setPadding(6);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

			table.addCell(cell1);
		}

		if (cmpyAddress.getPcd() != null) {
			cell1 = new PdfPCell(new Paragraph(""));
			cell1.setBorderColor(BaseColor.WHITE);
			cell1.setPaddingLeft(10);
			cell1.setPadding(6);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

			table.addCell(cell1);
		}

		return table;

	}

	public PdfPTable addBillToSection(Address venAddress) {
		PdfPTable table = new PdfPTable(1); // 3 columns.
		table.setWidthPercentage(40); // Width 100%
		table.setSpacingBefore(10f); // Space before table
		table.setSpacingAfter(10f); // Space after table
		table.setHorizontalAlignment(Element.ALIGN_LEFT);
		table.setPaddingTop(10);

		BaseColor color = new BaseColor(199, 211, 216);
		Font boldFont = new Font(Font.FontFamily.HELVETICA, 14, Font.BOLD);
		Phrase firstLine = new Phrase("Bill To", boldFont);

		PdfPCell cell1 = new PdfPCell(firstLine);
		cell1.setBorderColor(BaseColor.WHITE);
		cell1.setBackgroundColor(color);
		cell1.setPadding(6);
		cell1.setPaddingLeft(10);
		cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
		cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

		table.addCell(cell1);

		if (venAddress.getAddr1() != null) {
			cell1 = new PdfPCell(new Paragraph(venAddress.getAddr1()));
			cell1.setBorderColor(BaseColor.WHITE);
			cell1.setPaddingLeft(10);
			cell1.setPadding(6);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

			table.addCell(cell1);
		}

		if (venAddress.getAddr2() != null) {
			cell1 = new PdfPCell(new Paragraph(venAddress.getAddr2()));
			cell1.setBorderColor(BaseColor.WHITE);
			cell1.setPaddingLeft(10);
			cell1.setPadding(6);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

			table.addCell(cell1);
		}

		if (venAddress.getAddr3() != null) {
			cell1 = new PdfPCell(new Paragraph(venAddress.getAddr3()));
			cell1.setBorderColor(BaseColor.WHITE);
			cell1.setPaddingLeft(10);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

			table.addCell(cell1);
		}

		if (venAddress.getCity() != null) {

			cell1 = new PdfPCell(new Paragraph(venAddress.getCity()));
			cell1.setBorderColor(BaseColor.WHITE);
			cell1.setPaddingLeft(10);
			cell1.setPadding(6);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

			table.addCell(cell1);
		}

		if (venAddress.getCty() != null) {
			cell1 = new PdfPCell(new Paragraph(venAddress.getCty()));
			cell1.setBorderColor(BaseColor.WHITE);
			cell1.setPaddingLeft(10);
			cell1.setPadding(6);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

			table.addCell(cell1);
		}

		if (venAddress.getPcd() != null) {
			cell1 = new PdfPCell(new Paragraph(venAddress.getPcd()));
			cell1.setBorderColor(BaseColor.WHITE);
			cell1.setPaddingLeft(10);
			cell1.setPadding(6);
			cell1.setHorizontalAlignment(Element.ALIGN_LEFT);
			cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

			table.addCell(cell1);
		}

		return table;

	}

	public PdfPTable addFooterSection() {

		CommonFunctions commonFunctions = new CommonFunctions();

		BaseColor color = new BaseColor(199, 211, 216);

		PdfPTable tableTotals = new PdfPTable(2); // 3 columns.
		tableTotals.setWidthPercentage(50); // Width 100%
		tableTotals.setSpacingAfter(10f); // Space after table
		tableTotals.setHorizontalAlignment(Element.ALIGN_RIGHT);

		PdfPCell cell1 = new PdfPCell(new Paragraph("Sub Total"));
		cell1.setBackgroundColor(color);
		cell1.setBorderColor(color);
		cell1.setPadding(6);
		cell1.setPaddingLeft(10);
		cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

		tableTotals.addCell(cell1);

		cell1 = new PdfPCell(new Paragraph(
				cry + " " + commonFunctions.formatAmount(Double.parseDouble(formatValue(String.valueOf(totValue))))));
		cell1.setBorderColor(color);
		cell1.setPaddingLeft(10);
		cell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell1.setVerticalAlignment(Element.ALIGN_MIDDLE);

		tableTotals.addCell(cell1);

		PdfPCell cell2 = new PdfPCell(new Paragraph("Discount"));
		cell2.setBackgroundColor(color);
		cell2.setBorderColor(color);
		cell2.setPaddingLeft(10);
		cell2.setPadding(6);
		cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

		tableTotals.addCell(cell2);

		cell2 = new PdfPCell(new Paragraph(
				cry + " " + commonFunctions.formatAmount(Double.parseDouble(formatValue(String.valueOf(totdiscVal))))));
		cell2.setBorderColor(color);
		cell2.setPaddingLeft(10);
		cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

		tableTotals.addCell(cell2);

		cell2 = new PdfPCell(new Paragraph("Total"));
		cell2.setBackgroundColor(color);
		cell2.setBorderColor(color);
		cell2.setPaddingLeft(10);
		cell2.setPadding(6);
		cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

		tableTotals.addCell(cell2);

		double finalTotal = totValue - totdiscVal;

		cell2 = new PdfPCell(new Paragraph(
				cry + " " + commonFunctions.formatAmount(Double.parseDouble(formatValue(String.valueOf(finalTotal))))));
		cell2.setBorderColor(color);
		cell2.setPaddingLeft(10);
		cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

		tableTotals.addCell(cell2);

		cell2 = new PdfPCell(new Paragraph(""));
		cell2.setUseVariableBorders(true);
		cell2.setBorderColorTop(color);
		cell2.setBorderColorBottom(color);
		cell2.setBorderColorLeft(color);
		cell2.setBorderColorRight(BaseColor.WHITE);
		cell2.setPaddingLeft(10);
		cell2.setPadding(50);
		cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

		tableTotals.addCell(cell2);

		cell2 = new PdfPCell(new Paragraph(""));
		cell2.setUseVariableBorders(true);
		cell2.setBorderColorTop(color);
		cell2.setBorderColorBottom(color);
		cell2.setBorderColorLeft(BaseColor.WHITE);
		cell2.setBorderColorRight(color);
		cell2.setPaddingLeft(10);
		cell2.setPadding(50);
		cell2.setHorizontalAlignment(Element.ALIGN_RIGHT);
		cell2.setVerticalAlignment(Element.ALIGN_MIDDLE);

		tableTotals.addCell(cell2);

		return tableTotals;
	}

	public static PdfPTable addRemarks() {
		BaseColor color = new BaseColor(199, 211, 216);

		PdfPTable tableTotals = new PdfPTable(1); // 3 columns.
		tableTotals.setWidthPercentage(60); // Width 100%
		tableTotals.setSpacingAfter(10f); // Space after table

		tableTotals.setHorizontalAlignment(Element.ALIGN_LEFT);

		PdfPCell cell1 = new PdfPCell(new Paragraph("Remarks/Comments"));
		cell1.setBackgroundColor(BaseColor.WHITE);
		cell1.setPaddingLeft(10);
		cell1.setBorderColor(color);
		cell1.setHorizontalAlignment(Element.ALIGN_LEFT);

		tableTotals.addCell(cell1);

		return tableTotals;
	}

	/* FORMAT VALUE TO TWO DECIMAL PLACES */
	private String formatValue(String amount) {
		DecimalFormat df = new DecimalFormat("#.00");
		String formattedValue = df.format(Double.parseDouble(amount));

		formattedValue = String.format("%.2f", Double.parseDouble(amount));

		return formattedValue;
	}

}
