package com.star.common.remit;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.util.Date;
import java.util.List;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfGState;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;
import com.star.common.CommonConstants;
import com.star.common.EngNumToWords;
import com.star.linkage.company.Address;
import com.star.linkage.cstmdocinfo.VchrInfo;


public class CheckPrntDoc {

	static double totValue = 0;
	static double totdiscVal = 0;
	static String cry = "";

	public String genDocument(List<VchrInfo> vchrInfoList, Address cmpyAddress, Address venAddress, String pmntDt) {
		totValue = 0;
		totdiscVal = 0;
		String cry = "";
		
		Document document = new Document();

		String venId = "";
		String venNm = "";
		String venAd = "";

		String flNm = CommonConstants.ROOT_DIR + "/Check_" + (new Date()).getTime() + ".pdf";
		
		
		venId = vchrInfoList.get(0).getVchrVenId();
		venNm = vchrInfoList.get(0).getVchrVenNm();
		venAd = venAddress.getAddr1();   
		try {
			
			PdfWriter writer=  PdfWriter.getInstance(document, new FileOutputStream(flNm));
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		Rectangle one = new Rectangle(612,792);
		
		document.setPageSize(one);
		document.setMargins(0.01f,0.01f,0.01f,0.01f);
		document.open();
		try {
			
			//STUB Area
			PdfPTable pdfPTable = new PdfPTable(1);
	        pdfPTable.setWidthPercentage(98); // Width %
	        //pdfPTable.setTotalHeight(540);
	        //table.setLockedWidth(true);
	        PdfPCell pdfPCell1 = new PdfPCell(addHeaderInfo(venNm,venAd));
	        pdfPCell1.setBorderColor(BaseColor.WHITE);
	        pdfPCell1.setFixedHeight(52f);
	        pdfPTable.addCell(pdfPCell1);
	        
	        int apCase=134;
	        switch (apCase)
	        {
	        case 134:
	        	pdfPCell1 = new PdfPCell(addDetailsCase1(vchrInfoList,1));
	        	break;
	        case 2:
	        	pdfPCell1 = new PdfPCell(addDetailsCase2(vchrInfoList));
	        	break;
	        
	        }	        
	       	        
	        pdfPCell1.setFixedHeight(488f);
	        pdfPTable.addCell(pdfPCell1);
	        
	        document.add(pdfPTable);
			
	        BaseColor color = new BaseColor(199, 211, 216);
	        
	        //Check Area
	        PdfPTable pdfPTable1 = new PdfPTable(1);
	        pdfPTable1.setWidthPercentage(90.5f); // Width %
	        pdfPTable1.setSpacingBefore(12); // Space before table
	        pdfPTable1.setSpacingAfter(17); // Space after table
	        pdfPTable1.setHorizontalAlignment(Element.ALIGN_LEFT);
	         pdfPCell1 = new PdfPCell(new Paragraph("462042"));
	         pdfPCell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	         pdfPCell1.setFixedHeight(17);
	         pdfPCell1.setBorderColor(color.WHITE);
	        pdfPTable1.addCell(pdfPCell1);
	        document.add(pdfPTable1);
	        
	        
	        PdfPTable pdfPTable2 = new PdfPTable(1);
	        pdfPTable2.setWidthPercentage(90.5f); // Width %
	        
	        pdfPTable2.setSpacingAfter(5); // Space after table
	        pdfPTable2.setHorizontalAlignment(Element.ALIGN_LEFT);
	         pdfPCell1 = new PdfPCell(new Paragraph("28-04-2021"));
	       pdfPCell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	         pdfPCell1.setFixedHeight(17);
	         pdfPCell1.setBorderColor(color.WHITE);
	        pdfPTable2.addCell(pdfPCell1);
	        document.add(pdfPTable2);
	        
	        PdfPTable pdfPTable3 = new PdfPTable(1);
	        pdfPTable3.setWidthPercentage(90.5f); // Width %
	        
	        pdfPTable3.setSpacingAfter(5); // Space after table
	        pdfPTable3.setHorizontalAlignment(Element.ALIGN_LEFT);
	         //pdfPCell1 = new PdfPCell(new Paragraph(vchrInfoList.get(0).getVchrCry()));
	        pdfPCell1 = new PdfPCell(new Paragraph("USD"));
	         pdfPCell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	         pdfPCell1.setFixedHeight(17);
	         pdfPCell1.setBorderColor(color.WHITE);
	        pdfPTable3.addCell(pdfPCell1);
	        document.add(pdfPTable3);
	        
	        
	        PdfPTable pdfPTable4 = new PdfPTable(3);
	        pdfPTable4.setWidthPercentage(90.5f); // Width %  90.5f
	        float[] columnWidths = new float[]{67f, 17f,4.5f};
	        pdfPTable4.setWidths(columnWidths);
	        pdfPTable4.setSpacingAfter(5); // Space after table
	        
	        
	        double value=Double.parseDouble(formatValue((totValue-totdiscVal)+""));
	        double value2 = value * Math.pow(10, 0);
	        value2 = Math.floor(value2);
	        value2 = value2 / Math.pow(10, 0);
	        
	        double pvn= (value-value2)*100;
	        String fpvn=formatValue(pvn+"");
	        int dpoints=Integer.parseInt(fpvn.substring(0, fpvn.indexOf('.')));
	        int num=(int)value2;
	        if(dpoints==0)
	        pdfPCell1 = new PdfPCell(new Paragraph(EngNumToWords.convert(Long.parseLong(num+""))+" and 00/100"));
	        else if(dpoints<10)
	        	pdfPCell1 = new PdfPCell(new Paragraph(EngNumToWords.convert(Long.parseLong(num+""))+" and 0"+dpoints+"/100"));
	        else
	        	pdfPCell1 = new PdfPCell(new Paragraph(EngNumToWords.convert(Long.parseLong(num+""))+" and "+dpoints+"/100"));
	       
	        
	         pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
	         pdfPCell1.setFixedHeight(17f);
	         pdfPCell1.setBorderColor(color.WHITE);
	         pdfPCell1.setPaddingLeft(60f);
	        pdfPTable4.addCell(pdfPCell1);	
	        
	        
	        pdfPCell1 = new PdfPCell(new Paragraph("******"+formatValue((totValue-totdiscVal)+"")));
	        
	         pdfPCell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	         pdfPCell1.setFixedHeight(17f);
	         pdfPCell1.setBorderColor(color.WHITE);
	        pdfPTable4.addCell(pdfPCell1);	
	        
	        pdfPCell1 = new PdfPCell(new Paragraph(""));
	         pdfPCell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	         pdfPCell1.setFixedHeight(17f);
	         
	         pdfPCell1.setBorderColor(color.WHITE);
	         
	        pdfPTable4.addCell(pdfPCell1);	
	        document.add(pdfPTable4);
	        
	        
	        PdfPTable pdfPTable5 = new PdfPTable(1);
	         //pdfPCell1 = new PdfPCell(new Paragraph("Payee Name and Address"));
	        pdfPCell1 = new PdfPCell(new Paragraph(venNm+"\r\n" + 
	        		venAddress.getAddr1()));
	         pdfPTable5.setWidthPercentage(78.5f); // Width %
	         pdfPTable5.setSpacingBefore(30f); 
	         pdfPTable5.setSpacingAfter(6f); 
	         pdfPCell1.setFixedHeight(40f);
	         pdfPCell1.setBorderColor(color.WHITE);
	        pdfPTable5.addCell(pdfPCell1);
	        document.add(pdfPTable5);
	        
	        PdfPTable pdfPTable6 = new PdfPTable(1);
	        pdfPTable6.setWidthPercentage(90); 
	         pdfPCell1 = new PdfPCell(new Paragraph("Vendor Check remarks (Optional)"));
	         pdfPCell1.setFixedHeight(17);
	         pdfPCell1.setPaddingLeft(50f);
	         pdfPCell1.setBorderColor(color.WHITE);
	        pdfPTable6.addCell(pdfPCell1);
	        document.add(pdfPTable6);
	        
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		document.close();		
		
		return flNm;

	}
	
	public String waterMarkPDF(String flNm)
	{
		 String flNmStamp ="";
		
		try {
		
		 PdfReader reader = new PdfReader(flNm);
		 
		 flNmStamp = CommonConstants.ROOT_DIR + "/Check_" + (new Date()).getTime() + ".pdf";
		 
	     PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(flNmStamp));
	        
	        ClassLoader classLoader = getClass().getClassLoader();
			File file = new File(classLoader.getResource("check.png").getFile());

			String flPath = URLDecoder.decode(file.getAbsolutePath(), "UTF-8");
			
			System.out.println(file.getAbsolutePath());
	        
	        Image img = Image.getInstance(flPath);
	        float w = img.getScaledWidth();
	        float h = img.getScaledHeight();
	        
	        img.scaleAbsolute(100, 50);
	        // properties
	        PdfContentByte over;
	        Rectangle pagesize;
	        float x, y;

	        // loop over every page
	        int n = reader.getNumberOfPages();
	        for (int i = 1; i <= n; i++) {

	            // get page size and position
	            pagesize = reader.getPageSizeWithRotation(i);
	            x = (pagesize.getLeft() + pagesize.getRight()) / 2;
	            y = (pagesize.getTop() + pagesize.getBottom()) / 2;
	            over = stamper.getOverContent(i);
	            over.saveState();

	            // set transparency
	            PdfGState state = new PdfGState();
	            state.setFillOpacity(0.6f);
	            over.setGState(state);

	            // add watermark text and image
	            /*if (i % 2 == 1) {
	                ColumnText.showTextAligned(over, Element.ALIGN_CENTER, p, x, y, 0);
	            } else {*/
	                over.addImage(img, w, 0, 0, h, 10, 10);
	            /*}*/

	           // over.restoreState();
	        }
	        stamper.close();
	        reader.close();
		}
	        catch (Exception e) {
				// TODO: handle exception
			}
		
		return flNmStamp;
	}
	
	
	/* FORMAT VALUE TO TWO DECIMAL PLACES */
	private static String formatValue(String amount) {
		DecimalFormat df = new DecimalFormat("#.00");
		String formattedValue = df.format(Double.parseDouble(amount));

		formattedValue = String.format("%.2f", Double.parseDouble(amount));

		return formattedValue;
	}
	
	
	public static PdfPTable addHeaderInfo(String venNm, String venAddress) {
		//public static PdfPTable addHeaderInfo() {	
		
		PdfPTable pdfPTable = new PdfPTable(1);
		pdfPTable.setSpacingBefore(12);
		
		
		PdfPTable pdfPTable2 = new PdfPTable(3);
		try {
			pdfPTable2.setWidths(new int[]{4, 1, 1});
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//PdfPTable pdfPTable2 = new PdfPTable(3);
        pdfPTable2.setWidthPercentage(100); // Width %
        
        //pdfPTable2.setSpacingAfter(12); // Space after table
        pdfPTable2.setHorizontalAlignment(Element.ALIGN_LEFT);
        PdfPCell pdfPCell1 = new PdfPCell(new Paragraph(venNm));
       pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
          
       pdfPCell1.setBorderColor(BaseColor.LIGHT_GRAY);
       pdfPCell1.setBackgroundColor(BaseColor.LIGHT_GRAY);
        pdfPTable2.addCell(pdfPCell1);
        
         pdfPCell1 = new PdfPCell(new Paragraph("Date 28/04/2021"));
        pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
         // pdfPCell1.setFixedHeight(17);
        pdfPCell1.setBorderColor(BaseColor.LIGHT_GRAY);
        pdfPCell1.setBackgroundColor(BaseColor.LIGHT_GRAY);
         pdfPTable2.addCell(pdfPCell1);
         
          pdfPCell1 = new PdfPCell(new Paragraph("No 00000189"));
         pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            
           pdfPCell1.setBorderColor(BaseColor.LIGHT_GRAY);
           pdfPCell1.setBackgroundColor(BaseColor.LIGHT_GRAY);
          pdfPTable2.addCell(pdfPCell1);
		                 
           
           pdfPCell1 = new PdfPCell(pdfPTable2);
           pdfPTable.addCell(pdfPCell1);
           
           pdfPCell1 = new PdfPCell(new Paragraph(venAddress));
           pdfPCell1.setBorderColor(BaseColor.LIGHT_GRAY);
           pdfPCell1.setBackgroundColor(BaseColor.LIGHT_GRAY);
           pdfPTable.addCell(pdfPCell1);
        return pdfPTable;
	
	}
	
	
	public static PdfPTable addDetailsCase1(List<VchrInfo> vchrInfoList,int cs) {
		
		PdfPTable pdfPTable2 = new PdfPTable(6);
        pdfPTable2.setWidthPercentage(100); // Width %
        
        float[] columnWidths = new float[]{15f, 30f, 10f, 10f,10f, 15f};
        try {
			pdfPTable2.setWidths(columnWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        //pdfPTable2.setSpacingAfter(12); // Space after table
        pdfPTable2.setHorizontalAlignment(Element.ALIGN_LEFT);
        PdfPCell pdfPCell1 = new PdfPCell(new Paragraph("Ref No"));
       pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
          
         pdfPCell1.setBorderColor(BaseColor.WHITE);
        pdfPTable2.addCell(pdfPCell1);
        
         pdfPCell1 = new PdfPCell(new Paragraph("Inv No/Ref"));
        pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
         // pdfPCell1.setFixedHeight(17);
        pdfPCell1.setBorderColor(BaseColor.WHITE);
         pdfPTable2.addCell(pdfPCell1);
         
          pdfPCell1 = new PdfPCell(new Paragraph("Inv Dt"));
         pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            
         pdfPCell1.setBorderColor(BaseColor.WHITE);
          pdfPTable2.addCell(pdfPCell1);
		
          pdfPCell1 = new PdfPCell(new Paragraph("Amount"));
          pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
             
          pdfPCell1.setBorderColor(BaseColor.WHITE);
           pdfPTable2.addCell(pdfPCell1);
           
           pdfPCell1 = new PdfPCell(new Paragraph("Discount"));
           pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
              
           pdfPCell1.setBorderColor(BaseColor.WHITE);
            pdfPTable2.addCell(pdfPCell1);
           
            pdfPCell1 = new PdfPCell(new Paragraph("Net"));
            pdfPCell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
               
            pdfPCell1.setBorderColor(BaseColor.WHITE);
             pdfPTable2.addCell(pdfPCell1);
             
             int noOfRows=vchrInfoList.size();
             for(int i=0;i<noOfRows;i++)
             {
            	 totValue = totValue + vchrInfoList.get(i).getVchrAmt();
     			totdiscVal = totdiscVal + vchrInfoList.get(i).getDiscAmt();

     			            	 
            	 pdfPCell1 = new PdfPCell(new Paragraph(vchrInfoList.get(i).getVchrPfx() + "-" + vchrInfoList.get(i).getVchrNo()));
            	 
                 pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                    
                 pdfPCell1.setBorderColor(BaseColor.WHITE);
                  pdfPTable2.addCell(pdfPCell1);
                  
                   pdfPCell1 = new PdfPCell(new Paragraph(vchrInfoList.get(i).getVchrInvNo()));
                  pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                   // pdfPCell1.setFixedHeight(17);
                  pdfPCell1.setBorderColor(BaseColor.WHITE);
                   pdfPTable2.addCell(pdfPCell1);
                   
                    pdfPCell1 = new PdfPCell(new Paragraph(vchrInfoList.get(i).getVchrInvDtStr()));
                   pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                      
                   pdfPCell1.setBorderColor(BaseColor.WHITE);
                    pdfPTable2.addCell(pdfPCell1);
          		
                    pdfPCell1 = new PdfPCell(new Paragraph(vchrInfoList.get(i).getVchrAmt()+""));
                    pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                       
                    pdfPCell1.setBorderColor(BaseColor.WHITE);
                     pdfPTable2.addCell(pdfPCell1);
                     
                     pdfPCell1 = new PdfPCell(new Paragraph(vchrInfoList.get(i).getDiscAmt()+""));
                     pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                        
                     pdfPCell1.setBorderColor(BaseColor.WHITE);
                      pdfPTable2.addCell(pdfPCell1);
                      
                      
                     
                      pdfPCell1 = new PdfPCell(new Paragraph(formatValue((vchrInfoList.get(i).getVchrAmt()-vchrInfoList.get(i).getDiscAmt())+"")+" "+vchrInfoList.get(i).getVchrCry()));
                      pdfPCell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         
                      pdfPCell1.setBorderColor(BaseColor.WHITE);
                       pdfPTable2.addCell(pdfPCell1);
             }  
           
             //ROW ABOVE TOTAL
             pdfPCell1 = new PdfPCell(new Paragraph(""));
             pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                
             pdfPCell1.setBorderColor(BaseColor.WHITE);
              pdfPTable2.addCell(pdfPCell1);
              
              if(cs==1) 
            	  pdfPCell1 = new PdfPCell(new Paragraph("Partial Payment"));
              else
            	  pdfPCell1 = new PdfPCell(new Paragraph(" "));
              
              pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
               // pdfPCell1.setFixedHeight(17);
              pdfPCell1.setBorderColor(BaseColor.WHITE);
               pdfPTable2.addCell(pdfPCell1);
               
                pdfPCell1 = new PdfPCell(new Paragraph(""));
               pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                  
               pdfPCell1.setBorderColor(BaseColor.WHITE);
                pdfPTable2.addCell(pdfPCell1);
      		
                pdfPCell1 = new PdfPCell(new Paragraph(""));
                pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                   
                pdfPCell1.setBorderColor(BaseColor.WHITE);
                 pdfPTable2.addCell(pdfPCell1);
                 
                 pdfPCell1 = new PdfPCell(new Paragraph(""));
                 pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                    
                 pdfPCell1.setBorderColor(BaseColor.WHITE);
                  pdfPTable2.addCell(pdfPCell1);
                 
                  pdfPCell1 = new PdfPCell(new Paragraph(""));
                  pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                     
                  pdfPCell1.setBorderColor(BaseColor.WHITE);
                   pdfPTable2.addCell(pdfPCell1);
                   
                   
                   //ROW TOTAL
                   
                   pdfPCell1 = new PdfPCell(new Paragraph(""));
                   pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                      
                   pdfPCell1.setBorderColor(BaseColor.WHITE);
                    pdfPTable2.addCell(pdfPCell1);
                    
                     pdfPCell1 = new PdfPCell(new Paragraph(""));
                    pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                     // pdfPCell1.setFixedHeight(17);
                    pdfPCell1.setBorderColor(BaseColor.WHITE);
                     pdfPTable2.addCell(pdfPCell1);
                     
                      pdfPCell1 = new PdfPCell(new Paragraph(""));
                     pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                        
                     pdfPCell1.setBorderColor(BaseColor.WHITE);
                      pdfPTable2.addCell(pdfPCell1);
            		
                      pdfPCell1 = new PdfPCell(new Paragraph(""));
                      pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         
                      pdfPCell1.setBorderColor(BaseColor.WHITE);
                       pdfPTable2.addCell(pdfPCell1);
                       
                       pdfPCell1 = new PdfPCell(new Paragraph("Total"));
                       pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                          
                       pdfPCell1.setBorderColor(BaseColor.WHITE);
                        pdfPTable2.addCell(pdfPCell1);
                       
                       
                        
                        pdfPCell1 = new PdfPCell(new Paragraph(formatValue((totValue-totdiscVal)+"")+" "+vchrInfoList.get(0).getVchrCry()));
                        pdfPCell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                           
                        pdfPCell1.setBorderColor(BaseColor.WHITE);
                         pdfPTable2.addCell(pdfPCell1);
         
        return pdfPTable2;

	
	}
	
	
	//CASE2
public static PdfPTable addDetailsCase2(List<VchrInfo> vchrInfoList) {
		
		double usdTotal=0;
		PdfPTable pdfPTable2 = new PdfPTable(7);
        pdfPTable2.setWidthPercentage(100); // Width %
        
        float[] columnWidths = new float[]{15f, 23f, 12f, 10f,10f, 13f,13f};
        try {
			pdfPTable2.setWidths(columnWidths);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        //pdfPTable2.setSpacingAfter(12); // Space after table
        pdfPTable2.setHorizontalAlignment(Element.ALIGN_LEFT);
        PdfPCell pdfPCell1 = new PdfPCell(new Paragraph("Ref No"));
       pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
          
         pdfPCell1.setBorderColor(BaseColor.WHITE);
        pdfPTable2.addCell(pdfPCell1);
        
         pdfPCell1 = new PdfPCell(new Paragraph("Inv No/Ref"));
        pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
         // pdfPCell1.setFixedHeight(17);
        pdfPCell1.setBorderColor(BaseColor.WHITE);
         pdfPTable2.addCell(pdfPCell1);
         
          pdfPCell1 = new PdfPCell(new Paragraph("Inv Dt"));
         pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
            
         pdfPCell1.setBorderColor(BaseColor.WHITE);
          pdfPTable2.addCell(pdfPCell1);
		
          pdfPCell1 = new PdfPCell(new Paragraph("Amount"));
          pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
             
          pdfPCell1.setBorderColor(BaseColor.WHITE);
           pdfPTable2.addCell(pdfPCell1);
           
           pdfPCell1 = new PdfPCell(new Paragraph("Discount"));
           pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
              
           pdfPCell1.setBorderColor(BaseColor.WHITE);
            pdfPTable2.addCell(pdfPCell1);
           
            pdfPCell1 = new PdfPCell(new Paragraph("Net"));
            pdfPCell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
               
            pdfPCell1.setBorderColor(BaseColor.WHITE);
             pdfPTable2.addCell(pdfPCell1);
             
             pdfPCell1 = new PdfPCell(new Paragraph("Paid In"));
	            pdfPCell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
	               
	            pdfPCell1.setBorderColor(BaseColor.WHITE);
	             pdfPTable2.addCell(pdfPCell1);
             
             int noOfRows=vchrInfoList.size();
             for(int i=0;i<noOfRows;i++)
             {
            	 totValue = totValue + vchrInfoList.get(i).getVchrAmt();
     			totdiscVal = totdiscVal + vchrInfoList.get(i).getDiscAmt();

     			            	 
            	 pdfPCell1 = new PdfPCell(new Paragraph(vchrInfoList.get(i).getVchrPfx() + "-" + vchrInfoList.get(i).getVchrNo()));
            	 
                 pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                    
                 pdfPCell1.setBorderColor(BaseColor.WHITE);
                  pdfPTable2.addCell(pdfPCell1);
                  
                   pdfPCell1 = new PdfPCell(new Paragraph(vchrInfoList.get(i).getVchrInvNo()));
                  pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                   // pdfPCell1.setFixedHeight(17);
                  pdfPCell1.setBorderColor(BaseColor.WHITE);
                   pdfPTable2.addCell(pdfPCell1);
                   
                    pdfPCell1 = new PdfPCell(new Paragraph(vchrInfoList.get(i).getVchrInvDtStr()));
                   pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                      
                   pdfPCell1.setBorderColor(BaseColor.WHITE);
                    pdfPTable2.addCell(pdfPCell1);
          		
                    pdfPCell1 = new PdfPCell(new Paragraph(vchrInfoList.get(i).getVchrAmt()+""));
                    pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                       
                    pdfPCell1.setBorderColor(BaseColor.WHITE);
                     pdfPTable2.addCell(pdfPCell1);
                     
                     pdfPCell1 = new PdfPCell(new Paragraph(vchrInfoList.get(i).getDiscAmt()+""));
                     pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                        
                     pdfPCell1.setBorderColor(BaseColor.WHITE);
                      pdfPTable2.addCell(pdfPCell1);
                      
                      
                     
                      pdfPCell1 = new PdfPCell(new Paragraph(formatValue((vchrInfoList.get(i).getVchrAmt()-vchrInfoList.get(i).getDiscAmt())+"")+" "+vchrInfoList.get(i).getVchrCry()));
                      pdfPCell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                         
                      pdfPCell1.setBorderColor(BaseColor.WHITE);
                       pdfPTable2.addCell(pdfPCell1);
                       
                       double val=vchrInfoList.get(i).getVchrAmt()-vchrInfoList.get(i).getDiscAmt();
                       
	                     String cur=vchrInfoList.get(i).getVchrCry();
	                     usdTotal+=myMethod(val,cur);
	                      
	                       pdfPCell1 = new PdfPCell(new Paragraph(formatValue(myMethod(val,cur)+"")+" USD"));
		                      pdfPCell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
		                         
		                      pdfPCell1.setBorderColor(BaseColor.WHITE);
		                       pdfPTable2.addCell(pdfPCell1);
             }  
           
             //ROW ABOVE TOTAL
             pdfPCell1 = new PdfPCell(new Paragraph(""));
             pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                
             pdfPCell1.setBorderColor(BaseColor.WHITE);
              pdfPTable2.addCell(pdfPCell1);
              
              
              pdfPCell1 = new PdfPCell(new Paragraph(" "));             
              pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
               // pdfPCell1.setFixedHeight(17);
              pdfPCell1.setBorderColor(BaseColor.WHITE);
               pdfPTable2.addCell(pdfPCell1);
               
                pdfPCell1 = new PdfPCell(new Paragraph(""));
               pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                  
               pdfPCell1.setBorderColor(BaseColor.WHITE);
                pdfPTable2.addCell(pdfPCell1);
      		
                pdfPCell1 = new PdfPCell(new Paragraph(""));
                pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                   
                pdfPCell1.setBorderColor(BaseColor.WHITE);
                 pdfPTable2.addCell(pdfPCell1);
                 
                 pdfPCell1 = new PdfPCell(new Paragraph(""));
                 pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                    
                 pdfPCell1.setBorderColor(BaseColor.WHITE);
                  pdfPTable2.addCell(pdfPCell1);
                 
                  pdfPCell1 = new PdfPCell(new Paragraph(""));
                  pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                     
                  pdfPCell1.setBorderColor(BaseColor.WHITE);
                   pdfPTable2.addCell(pdfPCell1);
                   
                   pdfPCell1 = new PdfPCell(new Paragraph(""));
                   pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                      
                   pdfPCell1.setBorderColor(BaseColor.WHITE);
                    pdfPTable2.addCell(pdfPCell1);
                   
                   //ROW TOTAL
                   
                   pdfPCell1 = new PdfPCell(new Paragraph(""));
                   pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                      
                   pdfPCell1.setBorderColor(BaseColor.WHITE);
                    pdfPTable2.addCell(pdfPCell1);
                    
                     pdfPCell1 = new PdfPCell(new Paragraph(""));
                    pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                     // pdfPCell1.setFixedHeight(17);
                    pdfPCell1.setBorderColor(BaseColor.WHITE);
                     pdfPTable2.addCell(pdfPCell1);
                     
                      pdfPCell1 = new PdfPCell(new Paragraph(""));
                     pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                        
                     pdfPCell1.setBorderColor(BaseColor.WHITE);
                      pdfPTable2.addCell(pdfPCell1);
            		
                      pdfPCell1 = new PdfPCell(new Paragraph(""));
                      pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                         
                      pdfPCell1.setBorderColor(BaseColor.WHITE);
                       pdfPTable2.addCell(pdfPCell1);
                       
                       pdfPCell1 = new PdfPCell(new Paragraph(""));
                       pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                          
                       pdfPCell1.setBorderColor(BaseColor.WHITE);
                        pdfPTable2.addCell(pdfPCell1); 
                        
                       pdfPCell1 = new PdfPCell(new Paragraph("Total"));
                       pdfPCell1.setHorizontalAlignment(Element.ALIGN_LEFT);
                          
                       pdfPCell1.setBorderColor(BaseColor.WHITE);
                        pdfPTable2.addCell(pdfPCell1);
                       
                       
                        
                        pdfPCell1 = new PdfPCell(new Paragraph(formatValue(usdTotal+"")+" USD"));
                        pdfPCell1.setHorizontalAlignment(Element.ALIGN_RIGHT);
                           
                        pdfPCell1.setBorderColor(BaseColor.WHITE);
                         pdfPTable2.addCell(pdfPCell1);
         
                         totValue=usdTotal;
                         totdiscVal=0;
        return pdfPTable2;

	
	}
		

 public static double myMethod(double val,String cur) {

if (cur.equals("USD")) {
val=val*1;
}
if(cur.equalsIgnoreCase("EUR")) {
val=val*0.87;
}
if(cur.equals("CAD")) {
val=val*0.81;
}
if(cur.equals("PND")) {
val=val*0.78;
}
if(cur.equals("YEN")) {
val=val*111.087;
}
if(cur.equals("INR")) {
val=val*70;
}
return val;
}



}
