package com.star.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.CommonFunctions;
import com.star.common.MaintanenceResponse;
import com.star.common.SessionUtil;
import com.star.linkage.cstmdocinfo.CstmDocInfoBrowseOutput;
import com.star.linkage.cstmdocinfo.GlInfo;
import com.star.linkage.cstmdocinfo.ReconInfo;
import com.star.linkage.cstmdocinfo.VchrInfo;
import com.star.linkage.ocr.OCRManOutput;
import com.star.modal.CstmOcrVenMap;

public class OCRProcessDAO {

	private static Logger logger = LoggerFactory.getLogger(OCRProcessDAO.class);

	public void saveOCRData(MaintanenceResponse<OCRManOutput> starManResponse, String venId, String venNm,
			String venNmStx, String invNo, String invDt, String invAmt, String invCry) {
		Session session = null;
		Transaction tx = null;
		try {

			session = SessionUtil.getSession();
			tx = session.beginTransaction();
			CstmOcrVenMap params = new CstmOcrVenMap();

			params.setVenId(venId);
			params.setVenNm(venNm);
			params.setVenNmStx(venNmStx);
			params.setVenInvNoFld(invNo);
			params.setVenInvDtFld(invDt);
			params.setVenAmtFld(invAmt);
			params.setVenCryFld(invCry);
			params.setVenDueDtFld("");

			session.save(params);

			tx.commit();
		} catch (Exception e) {

			logger.debug("OCR Info : {}", e.getMessage(), e);
			starManResponse.output.messages.add(e.getMessage());
			starManResponse.output.rtnSts = 1;

			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
	
	
	public void updateOCRData(MaintanenceResponse<OCRManOutput> starManResponse, String venId, String venNm,
			String venNmStx, String invNo, String invDt, String invAmt, String invCry) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();
			
			hql = "UPDATE cstm_ocr_ven_map set ven_nm=:ven_nm, ven_inv_no_fld=:inv_no, "
					+ "ven_inv_dt_fld=:inv_dt, ven_amt_fld=:inv_amt, "
					+ "ven_cry_fld=:inv_cry, ven_due_dt_fld=:inv_due"
					+ " WHERE ven_id=:ven";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("ven_nm", venNm);
			queryValidate.setParameter("inv_no", invNo);
			queryValidate.setParameter("inv_dt", invDt);
			queryValidate.setParameter("inv_amt", invAmt);
			queryValidate.setParameter("inv_cry", invCry);
			queryValidate.setParameter("inv_due", "");
			queryValidate.setParameter("ven", venId);
			
			queryValidate.executeUpdate();
			
			tx.commit();
			
		} catch (Exception e) {
			logger.debug("OCR Info : {}", e.getMessage(), e);
			tx.rollback();
		} finally {
			session.close();
		}
	}

	public void deleteOcrInfo(MaintanenceResponse<OCRManOutput> starManResponse, String venId) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();
			
			hql = "delete from cstm_ocr_ven_map WHERE ven_id=:ven";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("ven", venId);
			
			queryValidate.executeUpdate();
			
			tx.commit();
			
		} catch (Exception e) {
			logger.debug("OCR Info : {}", e.getMessage(), e);
			tx.rollback();
		} finally {
			session.close();
		}
	}
	
	/* GET specific THE DEFINE FIELDS LIST */
	public List<CstmOcrVenMap> getVendorList(String venId) {
		Session session = null;
		String hql = "";

		List<CstmOcrVenMap> cstmOcrVenMaps = new ArrayList<CstmOcrVenMap>();
		try {
			session = SessionUtil.getSession();

			hql = " select ven_id, ven_nm, ven_nm_stx, ven_inv_no_fld, "
					+ "ven_inv_dt_fld, ven_amt_fld, ven_cry_fld, ven_due_dt_fld, inv_pos from cstm_ocr_ven_map where 1=1";

			if (venId.length() > 0) {
				hql = hql + " and ven_id=:ven";
			}

			Query queryValidate = session.createSQLQuery(hql);

			if (venId.length() > 0) {
				queryValidate.setParameter("ven", venId);
			}

			List<Object[]> listVendMap = queryValidate.list();

			for (Object[] vendInfo : listVendMap) {
				CstmOcrVenMap vendorMap = new CstmOcrVenMap();

				vendorMap.setVenId(vendInfo[0].toString());
				vendorMap.setVenNm(vendInfo[1].toString());
				vendorMap.setVenNmStx(vendInfo[2].toString());
				vendorMap.setVenInvNoFld(vendInfo[3].toString());
				vendorMap.setVenInvDtFld(vendInfo[4].toString());
				vendorMap.setVenAmtFld(vendInfo[5].toString());
				vendorMap.setVenCryFld(vendInfo[6].toString());
				vendorMap.setVenDueDtFld(vendInfo[7].toString());
				vendorMap.setInvPos(Integer.parseInt(vendInfo[8].toString()));

				cstmOcrVenMaps.add(vendorMap);
			}

		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);

			session.close();
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return cstmOcrVenMaps;

	}

}
