package com.star.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.CommonFunctions;
import com.star.common.MaintanenceResponse;
import com.star.common.SessionUtil;
import com.star.common.SessionUtilInformix;
import com.star.common.SessionUtilInformixESP;
import com.star.linkage.common.PaymentMethod;
import com.star.linkage.cstmdocinfo.CstmDocInfoBrowseOutput;
import com.star.linkage.cstmdocinfo.VchrInfo;
import com.star.linkage.payment.PaymentInfo;
import com.star.linkage.payment.RemitInvoiceInfo;
import com.star.linkage.payment.ReversePayManOutput;
import com.star.modal.CstmParamInv;

public class ReversePayDAO {

	private static Logger logger = LoggerFactory.getLogger(RemitDAO.class);

	/* GET ALL THE DEFINE FIELDS LIST */
	public void readVouchers(BrowseResponse<CstmDocInfoBrowseOutput> starBrowseRes, String venId, String reqId, String pmntDt, String cry, List<String> vchrList) throws Exception {
		Session session = null;
		String hql = "";
		List<String> voucherList = new ArrayList<String>();

		List<RemitInvoiceInfo> invsListApproved = new ArrayList<RemitInvoiceInfo>();

		invsListApproved = getVouchers("A", "", pmntDt, reqId);

		PaymentDAO dao = new PaymentDAO();

		voucherList = getVoucherList(invsListApproved);

		try {
			session = SessionUtilInformix.getSession();

			List<PaymentMethod> paymentMethods = dao.readVendorPaymentMethod();

			/*hql = "SELECT trim(vch_ven_id) as ven_id, trim(ven_ven_long_nm) as ven_long_nm, trim(vch_ven_inv_no) as ven_inv_no,"
					+ " trim(vch_lgn_id) as lgn_id, vch_ven_inv_dt, vch_due_dt, trim(vch_po_pfx) as po_pfx, vch_po_no, vch_po_itm,"
					+ " trim(vch_cry) as cry, trim(vch_vchr_pfx) as vchr_pfx, vch_vchr_no, trim(vch_vchr_brh) as vchr_brh,"
					+ " vch_ent_dt, vch_vchr_amt, trim(vct_vchr_cat) as vchr_cat, (select tsa_sts_actn from tcttsa_rec"
					+ " where tsa_cmpy_id=vch_cmpy_id  and tsa_ref_pfx=vch_vchr_pfx and tsa_ref_no=vch_vchr_no and tsa_ref_itm=0 and"
					+ " tsa_ref_sbitm=0 and tsa_sts_typ='T') trn_sts, (select tsa_sts_actn from tcttsa_rec where tsa_cmpy_id=vch_cmpy_id and"
					+ " tsa_ref_pfx=vch_vchr_pfx and tsa_ref_no=vch_vchr_no and tsa_ref_itm=0  and tsa_ref_sbitm=0 and tsa_sts_typ='N') pay_sts, "
					+ "cast((select rsn_desc30 from tcttsa_rec, scrrsn_rec 	where tsa_cmpy_id=vch_cmpy_id  and tsa_ref_pfx=vch_vchr_pfx and tsa_ref_no=vch_vchr_no and tsa_ref_itm=0 and 	"
					+ "tsa_ref_sbitm=0 and tsa_sts_typ='T' and rsn_rsn_typ=tsa_rsn_typ and rsn_rsn=tsa_rsn) as varchar(30)) trn_rsn, 	"
					+ "cast((select rsn_desc30 from tcttsa_rec, scrrsn_rec 	where tsa_cmpy_id=vch_cmpy_id  and tsa_ref_pfx=vch_vchr_pfx and tsa_ref_no=vch_vchr_no and tsa_ref_itm=0 "
					+ "and 	tsa_ref_sbitm=0 and tsa_sts_typ='N' and rsn_rsn_typ=tsa_rsn_typ and rsn_rsn=tsa_rsn) as varchar(30)) pay_rsn, trim(vch_cmpy_id) as cmpy_id, round(vch_disc_amt, 2) vch_disc_amt"
					+ " FROM aptvch_rec, aprven_rec, aprvct_rec  WHERE 1=1 and ven_cmpy_id = vch_cmpy_id and ven_ven_id = vch_ven_id and"
					+ " vct_vchr_cat = vch_vchr_cat";*/
			
			/*hql = "select trim(pyh_ven_id) as ven_id, trim(ven_ven_long_nm) as ven_long_nm, trim(pyh_ven_inv_no) as ven_inv_no, "
					+ "( select vch_lgn_id from aptvch_rec where vch_cmpy_id = pyh_cmpy_id and vch_vchr_pfx = pyh_opa_pfx "
					+ "and vch_vchr_no = pyh_opa_no) as lgn_id, pyh_ven_inv_dt, pyh_due_dt, trim(pyh_po_pfx) as po_pfx, pyh_po_no, pyh_po_itm, "
					+ "trim(pyh_cry) as cry, trim(pyh_opa_pfx) as vchr_pfx, pyh_opa_no, trim(pyh_ap_brh) as vchr_brh, pyh_ent_dt, "
					+ "pyh_orig_amt,  ( select vch_vchr_cat from aptvch_rec where vch_cmpy_id = pyh_cmpy_id and vch_vchr_pfx = pyh_opa_pfx "
					+ "and vch_vchr_no = pyh_opa_no) vch_cat, 'C' trn_sts, 'C' pay_sts, '' trn_rsn, '' pay_rsn, "
					+ "trim(pyh_cmpy_id) as cmpy_id, round(pyh_disc_amt, 2) vch_disc_amt, pyh_disc_dt,"
					+ "case when TO_DATE('"+ pmntDt +"','MM/DD/YYYY') > pyh_disc_dt then (pyh_orig_amt - (case when (pyh_disc_dt - date(now())) is null or "
							+ "(pyh_disc_dt - date(now()) < 0) then 0 else pyh_disc_amt end)) else (pyh_orig_amt - pyh_disc_amt) end tot_amt "
					+ "from aptpyh_rec inner join aprven_rec on ven_cmpy_id = pyh_cmpy_id "
					+ "and ven_ven_id = pyh_ven_id inner join tcttsa_rec on tsa_cmpy_id = pyh_cmpy_id and tsa_ref_pfx = pyh_ap_pfx "
					+ "and tsa_ref_no = pyh_ap_no and tsa_ref_itm = 0 and tsa_ref_sbitm = 0 and tsa_sts_typ = 'N' ";
			*/
			
			hql = "select ven_ven_no ,trim(ven_lkp_nm) as ven_long_nm, "+ 
			"trim(pyh_ven_inv_no) as ven_inv_no, "+ 
			"(select vch_lgn_id from aptvch_rec where vch_ref_pfx = pyh_ref_cd "+ 
			"and vch_vch_no = pyh_ref_no),  "+
			"cast(concat(concat(concat(left(cast (pyh_ref_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_ref_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_ref_dt as varchar(8)),2))as date) refDt, "+
			"cast(concat(concat(concat(left(cast (pyh_due_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_due_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_due_dt as varchar(8)),2))as date) dueDt, "+
			"'' po_pfx, "+
			"'' vch_po_no, '' vch_po_itm, "+ 
			"trim(ven_cry) cry, "+ 
			"trim(aptpyh_rec.pyh_ref_cd)  as vchr_pfx, "+ 
			"aptpyh_rec.pyh_ref_no ,  "+
			"trim(pyh_brh) as vchr_brh, "+ 
			"cast(concat(concat(concat(left(cast (pyh_ref_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_ref_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_ref_dt as varchar(8)),2))as date) reffDt, "+
			"pyh_amt_ap, '' vch_cat, 'C' trn_sts, 'C' pay_sts, '' trn_rsn, '' pay_rsn, "+
			"'' cmpy_id, "+
			"round(pyh_disc_amt_ap, 2) vch_disc_amt, "+ 
			"cast(concat(concat(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_disc_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_disc_dt as varchar(8)),2))as date) discDt, "+
			"pyh_amt_ap ,  djh_chq_no, "+
			"cast(concat(concat(concat(left(cast (djh_jnl_dt as varchar(8)),4),'-'),  concat(substr(cast (djh_jnl_dt as varchar(8)),5,2) ,'-')),right(cast (djh_jnl_dt as varchar(8)),2))as date) jnlDt, "+
			"cast(concat(concat(concat(left(cast (djh_issue_dt as varchar(8)),4),'-'),  concat(substr(cast (djh_issue_dt as varchar(8)),5,2) ,'-')),right(cast (djh_issue_dt as varchar(8)),2))as date) issuDt, djo_djh_no "+ 
			"from apjdjo_rec "+ 
			"inner join aptpyh_rec on aptpyh_rec.pyh_ref_no=apjdjo_rec.djo_ref_no "+  
			"inner join aprven_rec on aprven_rec.ven_ven_no=aptpyh_rec.pyh_ven_no "+ 
			"inner join apjdjh_rec on djo_ssn_brh=djh_brh and djo_djh_no=djh_djh_no and djh_trs_type='V' "+  
			"where  pyh_disc_dt is not null AND pyh_ref_dt is not null AND pyh_due_dt is not null ";
			
			if (venId.length() > 0) {
				hql = hql + " and ven_ven_no = :ven_id ";
			}

			/*if (CommonConstants.DB_TYP.equals("POS")) {

				if (invcDt.length() > 0) {
					hql = hql + " and pyh_ven_inv_dt >= to_date(:invc_dt, 'mm/dd/yyyy')";
				}

				if (invcDtTo.length() > 0) {
					hql = hql + " and pyh_ven_inv_dt <= to_date(:invc_dt_to, 'mm/dd/yyyy')";
				}

			} else if (CommonConstants.DB_TYP.equals("INF")) {

				if (invcDt.length() > 0) {
					hql = hql + " and pyh_ven_inv_dt >= to_date(:invc_dt, '%m/%d/%Y')";
				}

				if (invcDtTo.length() > 0) {
					hql = hql + " and pyh_ven_inv_dt <= to_date(:invc_dt_to, '%m/%d/%Y')";
				}
			}
*/
			hql = hql + " and aptpyh_rec.pyh_ref_cd ||'-'||aptpyh_rec.pyh_ref_no in (:vchrList)";

			/*if (voucherList.size() > 0) {
				hql = hql + " and aptpyh_rec.pyh_ref_cd || '-' || aptpyh_rec.pyh_ref_no in (:vchrListSel)";
			}*/
			
			if (cry.length() > 0) {
				hql = hql + " and ven_cry = :cry";
			}
			
			hql = hql + " order by djh_chq_no,pyh_ref_no, ven_ven_no ";

			// TODO trn_sts is not NULL
			Query queryValidate = session.createSQLQuery(hql);

			if (venId.length() > 0) {
				queryValidate.setParameter("ven_id", Integer.parseInt(venId));
			}

			/*if (invcDt.length() > 0) {
				queryValidate.setParameter("invc_dt", invcDt);
			}

			if (invcDtTo.length() > 0) {
				queryValidate.setParameter("invc_dt_to", invcDtTo);
			}*/
			
			if (cry.length() > 0) {
				queryValidate.setParameter("cry", cry);
			}

			/*List<String> items = Arrays.asList(voucherList.split("\\s*,\\s*"));*/
			
			if(voucherList.size() == 0)
			{
				voucherList.add("0");
			}
			
			queryValidate.setParameterList("vchrList", voucherList);

			/*if (voucherList.size() > 0) {
				queryValidate.setParameterList("vchrListSel", voucherList);
			}*/

			List<Object[]> listCstmDocVal = queryValidate.list();
			CommonFunctions objCom = new CommonFunctions();
			for (Object[] cstmDocVal : listCstmDocVal) {

				VchrInfo vchrInfo = new VchrInfo();

				vchrInfo.setVchrVenId(String.valueOf(cstmDocVal[0]));
				vchrInfo.setVchrVenNm(String.valueOf(cstmDocVal[1]));
				vchrInfo.setVchrInvNo(String.valueOf(cstmDocVal[2]));
				vchrInfo.setUpldBy(String.valueOf(cstmDocVal[3]));
				if (cstmDocVal[4] != null) {
					vchrInfo.setVchrInvDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[4])));
					vchrInfo.setVchrInvDt((Date) (cstmDocVal[4]));
				}
				if (cstmDocVal[5] != null) {
					vchrInfo.setVchrDueDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[5])));
					vchrInfo.setVchrDueDt((Date) (cstmDocVal[5]));
				}
				vchrInfo.setPoPfx(String.valueOf(cstmDocVal[6]));
				vchrInfo.setPoNo(String.valueOf(cstmDocVal[7]));
				vchrInfo.setPoItm(String.valueOf(cstmDocVal[8]));
				vchrInfo.setVchrCry(String.valueOf(cstmDocVal[9]));
				vchrInfo.setVchrPfx(String.valueOf(cstmDocVal[10]));
				vchrInfo.setVchrNo(String.valueOf(cstmDocVal[11]));
				vchrInfo.setVchrBrh(String.valueOf(cstmDocVal[12]));
				if (cstmDocVal[13] != null) {
					vchrInfo.setCrtDttsStr((objCom.formatDateWithoutTime((Date) cstmDocVal[13])));
					vchrInfo.setCrtDtts((Date) (cstmDocVal[13]));
				}
				else
				{
					vchrInfo.setCrtDttsStr("");
				}
				vchrInfo.setVchrAmt(Double.parseDouble(String.valueOf(cstmDocVal[14])));
				vchrInfo.setVchrAmtStr(objCom.formatAmount(Double.parseDouble(String.valueOf(cstmDocVal[14]))));
				vchrInfo.setVchrCat(String.valueOf(cstmDocVal[15]));
				if (cstmDocVal[16] != null) {
					vchrInfo.setTransSts(String.valueOf(cstmDocVal[16]));
				} else {
					vchrInfo.setTransSts("");
				}

				if (cstmDocVal[17] != null) {
					vchrInfo.setPymntSts(String.valueOf(cstmDocVal[17]));
				} else {
					vchrInfo.setPymntSts("");
				}

				if (cstmDocVal[18] != null) {
					vchrInfo.setTransStsRsn(String.valueOf(cstmDocVal[18]));
				} else {
					vchrInfo.setTransStsRsn("");
				}

				if (cstmDocVal[19] != null) {
					vchrInfo.setPymntStsRsn(String.valueOf(cstmDocVal[19]));
				} else {
					vchrInfo.setPymntStsRsn("");
				}

				vchrInfo.setCmpyId(cstmDocVal[20].toString());
				vchrInfo.setDiscAmt(Double.parseDouble(String.valueOf(cstmDocVal[21])));
				vchrInfo.setDiscAmtStr(objCom.formatAmount(Double.parseDouble(String.valueOf(cstmDocVal[21]))));
				
				if (cstmDocVal[22] != null) {
					vchrInfo.setVchrDiscDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[22])));
					vchrInfo.setVchrDiscDt((Date) (cstmDocVal[22]));
				}
				else
				{
					vchrInfo.setVchrDiscDtStr("");
				}
				
				vchrInfo.setTotAmt(Double.parseDouble(String.valueOf(cstmDocVal[23])));
				vchrInfo.setTotAmtStr(objCom.formatAmount(Double.parseDouble(cstmDocVal[23].toString())));
				
				/* PAYMENT METHOD LIST AGAINST VENDOR */
				List<PaymentMethod> methods = new ArrayList<PaymentMethod>();

				int iPayMthd = 0;

				for (int i = 0; i < paymentMethods.size(); i++) {
					if (paymentMethods.get(i).getCmpyId().equals(vchrInfo.getCmpyId())
							&& paymentMethods.get(i).getVenId().equals(vchrInfo.getVchrVenId())) {
						methods.add(paymentMethods.get(i));
					}
				}
				
				for(int i =0; i < invsListApproved.size(); i++)
				{
					if(invsListApproved.get(i).getInvNo().equals(vchrInfo.getVchrPfx() + "-" + vchrInfo.getVchrNo()))
					{
						vchrInfo.setChkNo(invsListApproved.get(i).getChkNo());
						vchrInfo.setPmntDtStr(invsListApproved.get(i).getPmntDt());
						vchrInfo.setReqId(invsListApproved.get(i).getReqId());
					}
				}

				vchrInfo.setPayMthds(methods);

				
				starBrowseRes.output.fldTblDoc.add(vchrInfo);

			}

		} catch (Exception e) {

			logger.debug("Payment Info : {}", e.getMessage(), e);
			starBrowseRes.output.rtnSts = 1;
			starBrowseRes.output.messages.add(CommonConstants.SERVER_ERR);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	
	/* GET ALL THE DEFINE FIELDS LIST */
	public List<RemitInvoiceInfo> getVouchers(String sts, String year, String crtdDt, String reqId) {
		Session session = null;
		String hql = "";
		String voucherList = "";
		CommonFunctions objCom = new CommonFunctions();
		List<RemitInvoiceInfo> cstmParamInvsList = new ArrayList<RemitInvoiceInfo>();

		try {
			session = SessionUtil.getSession();

			hql = "select inv_no, chk_no, inv.req_id, param.pay_crtd_on from cstm_pay_params param,cstm_param_inv inv "
					+ "where inv.req_id=param.req_id and inv.inv_sts=true and vchr_pay_mthd <> 5";

			if (sts.length() > 0) {
				hql += " and pay_sts=:sts";
			}
			
			if (reqId.length() > 0) {
				hql += " and inv.req_id=:req_id";
			}


			if (year.length() > 0) {
				hql += " and date_part('year', crtd_on)=:year";
			}
			
			if (crtdDt.length() > 0) {
				hql = hql + " and to_char(param.pay_crtd_on,'mm/dd/yyyy') = :crtd_dt";
			}

			// TODO trn_sts is not NULL
			Query queryValidate = session.createSQLQuery(hql);

			if (sts.length() > 0) {
				queryValidate.setParameter("sts", sts);
			}

			if (year.length() > 0) {
				queryValidate.setParameter("year", Double.parseDouble(year));
			}
			
			if (crtdDt.length() > 0) {
				queryValidate.setParameter("crtd_dt", crtdDt);
			}
			
			if (reqId.length() > 0) {
				queryValidate.setParameter("req_id", reqId);
			}

			List<Object[]> listVouchers = queryValidate.list();

			for (Object[] vouchers : listVouchers) {

				RemitInvoiceInfo cstmParamInv = new RemitInvoiceInfo();

				if (vouchers[0] != null) {
					cstmParamInv.setInvNo(vouchers[0].toString());
				} else {
					cstmParamInv.setInvNo("");
				}

				if (vouchers[1] != null) {
					cstmParamInv.setChkNo(vouchers[1].toString());
				} else {
					cstmParamInv.setChkNo("");
				}

				if (vouchers[2] != null) {
					cstmParamInv.setReqId(vouchers[2].toString());
				} else {
					cstmParamInv.setReqId("");
				}
				
				if (vouchers[3] != null) {
					cstmParamInv.setPmntDt((objCom.formatDateWithoutTime((Date) vouchers[3])));
				}
				else
				{
					cstmParamInv.setPmntDt("");
				}

				cstmParamInvsList.add(cstmParamInv);

				voucherList = voucherList + vouchers[0].toString() + ",";
			}

			if (voucherList.length() > 0) {
				voucherList = voucherList.substring(0, voucherList.length() - 1);
			}
		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return cstmParamInvsList;
	}
	
	
	public List<String> getVoucherList(List<RemitInvoiceInfo> remitInvoiceInfo) {
		
		List<String> vchrList = new ArrayList<String>();
		
		for (int i = 0; i < remitInvoiceInfo.size(); i++) {

			vchrList.add(remitInvoiceInfo.get(i).getInvNo());
		}

		return vchrList;

	}
	
	public void updateInvoice(MaintanenceResponse<ReversePayManOutput> starManResponse, List<CstmParamInv> invList,
			String usrId, String cmpyId) {
		Session session = null;
		Transaction tx = null;
		String hql = "";

		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();
			
			for(int i =0; i < invList.size(); i++)
			{
				hql = "update cstm_param_inv set inv_sts=false where req_id = :req_id and inv_no = :inv_no";
			
				Query queryValidate = session.createSQLQuery(hql);
	
				queryValidate.setParameter("req_id", invList.get(i).getReqId());
				queryValidate.setParameter("inv_no", invList.get(i).getInvNo());
	
				queryValidate.executeUpdate();
			}
			
			tx.commit();

		} catch (Exception e) {

			logger.debug("Payment Info : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
	
}
