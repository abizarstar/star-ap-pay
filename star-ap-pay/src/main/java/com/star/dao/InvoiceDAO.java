package com.star.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.SessionUtilInformix;
import com.star.linkage.invoice.InvoiceInfo;
import com.star.linkage.invoice.InvoiceInfoBrowseOutput;

public class InvoiceDAO {

	private static Logger logger = LoggerFactory.getLogger(InvoiceDAO.class);
	
	public void getInvoiceDetails(BrowseResponse<InvoiceInfoBrowseOutput> starBrowseResponse, String invNo)
	{
		Session session= null;
		String hql = "";
		
    	try{
    		
    		session = SessionUtilInformix.getSession();
    		
			hql = "select cast(rvh_upd_ref as varchar(22)) rvh_upd_ref, cast(rvh_ar_brh as varchar(3)) rvh_ar_brh, rvh_orig_amt, cast(rvh_sld_cus_id as varchar(8)) rvh_sld_cus_id from artrvh_rec where 1=1";
	    	
			if(invNo.length() > 0)
			{
				hql = hql + " and rvh_upd_ref=:upd_ref";
			}
			
		    Query queryValidate= session.createSQLQuery(hql);
		    
		    if(invNo.length() > 0)
			{
		    	queryValidate.setParameter("upd_ref", invNo);
			}
		    
			 List<Object[]> listInvoice =  queryValidate.list();
			 
			 for (Object[] invoice : listInvoice)
		     {
				 InvoiceInfo invInfo = new InvoiceInfo();
				 
				invInfo.setInvoiceNo(invoice[0].toString());
				invInfo.setArBrh(invoice[1].toString());
				invInfo.setAmount(invoice[2].toString());
				invInfo.setCusId(invoice[3].toString());
				
				getSessionInfo(session,invInfo);
				
				starBrowseResponse.output.fldTblInvoice.add(invInfo);
		     }
			 
			 hql = "select csh_ssn_id from artcsd_rec, artcsh_rec, artrvh_rec where csd_cmpy_id = csh_cmpy_id and csd_ref_pfx = csh_crcp_pfx and csd_ref_no = csh_crcp_no and rvh_cmpy_id = csd_cmpy_id and rvh_ar_pfx = csd_ar_pfx and rvh_ar_no = csd_ar_no";
		    	
			if(invNo.length() > 0)
			{
				hql = hql + " and rvh_upd_ref=:upd_ref";
			}		 
		    	   
    	}
    	catch (Exception e) {
    		logger.debug("Invoice Info : {}", e.getMessage(), e);
    		
		}
    	finally {
			session.close();
		}
	}
	
	public void getSessionInfo(Session session, InvoiceInfo invInfo)
	{
		String hql = "";
		int iFlg = 0;
		
		 hql = "select csh_ssn_id, 1 from artcsd_rec, artcsh_rec, artrvh_rec where csd_cmpy_id = csh_cmpy_id and csd_ref_pfx = csh_crcp_pfx and csd_ref_no = csh_crcp_no and rvh_cmpy_id = csd_cmpy_id and rvh_ar_pfx = csd_ar_pfx and rvh_ar_no = csd_ar_no";
	    	
		if(invInfo.getInvoiceNo().length() > 0)
		{
			hql = hql + " and rvh_upd_ref=:upd_ref";
		}		
		
		Query queryValidate= session.createSQLQuery(hql);
	    
	    if(invInfo.getInvoiceNo().length() > 0)
		{
	    	queryValidate.setParameter("upd_ref", invInfo.getInvoiceNo());
		}
	    
		 List<Object[]> listInvoice =  queryValidate.list();
		 
		 for (Object[] invoice : listInvoice)
	     {
			invInfo.setSsnId(invoice[0].toString());
			iFlg = 1;
	     }
		 
		 if(iFlg == 0)
		 {
			 invInfo.setSsnId("");
		 }
		
	}
	
	
}
