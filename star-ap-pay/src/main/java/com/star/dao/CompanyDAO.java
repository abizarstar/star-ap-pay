package com.star.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.MaintanenceResponse;
import com.star.common.SessionUtil;
import com.star.linkage.bank.BankInfo;
import com.star.linkage.company.CompanyBrowseOutput;
import com.star.linkage.company.CompanyInfo;
import com.star.linkage.company.CompanyManOutput;
import com.star.modal.CstmCmpyBnkInfo;

public class CompanyDAO {

	private static Logger logger = LoggerFactory.getLogger(CompanyDAO.class);
	
	public void getBankDetails(BrowseResponse<CompanyBrowseOutput> starBrowseResponse, String cmpyId)
	{
		Session session = null;
		String hql = "";
		
		try {
			session = SessionUtil.getSession();

			hql = " select * from cstm_cmpy_bnk_info where 1=1";
			
			if(cmpyId.length() > 0)
			{
				hql = hql + " and cmpy_id = :cmpy";
			}
			
			Query queryValidate = session.createSQLQuery(hql);
			
			if(cmpyId.length() > 0)
			{
				queryValidate.setParameter("cmpy", cmpyId);
			}
			
			
			List<Object[]> listCompany = queryValidate.list();
			
			BankDAO bankDAO = new BankDAO();
			
			for (Object[] company : listCompany) {

				CompanyInfo output = new CompanyInfo();
				
				output.setCmpyId(company[0].toString());
				output.setBankCode(company[1].toString());
				
				BankInfo bnkInfo = new BankInfo(); 
				
				bankDAO.getBankAccountDetails(bnkInfo, output, session);
				
				output.setAccoutnList(bnkInfo.getBankAcctList());
				
				starBrowseResponse.output.fldTblCompany.add(output);

			}

		} catch (Exception e) {
			logger.debug("Company Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}
	}
	
	public void addCompanyBank(MaintanenceResponse<CompanyManOutput> starManResponse, String cmpyId, String bnkCode) {
		Session session = null;
		Transaction tx = null;

		try {

			session = SessionUtil.getSession();
			tx = session.beginTransaction();
			
			delCompanyBank(cmpyId, bnkCode, session);
			
			CstmCmpyBnkInfo bnkInfo = new CstmCmpyBnkInfo();

			bnkInfo.setCmpyId(cmpyId);
			bnkInfo.setBnkCode(bnkCode);
			
			session.save(bnkInfo);
			
			tx.commit();
		} catch (Exception e) {

			logger.debug("Company Info : {}", e.getMessage(), e);
			starManResponse.output.messages.add(e.getMessage());
			starManResponse.output.rtnSts = 1;

			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
	
	public void delCompanyBank(String cmpyId, String bnkId, Session session) throws Exception
	{
		String hql = "";
		
		hql = "delete from cstm_cmpy_bnk_info where cmpy_id=:cmpy and bnk_code=:bnkcode";

		Query queryValidate = session.createSQLQuery(hql);
		queryValidate.setParameter("cmpy", cmpyId);
		queryValidate.setParameter("bnkcode", bnkId);
		queryValidate.executeUpdate();
	}
	
	
}
