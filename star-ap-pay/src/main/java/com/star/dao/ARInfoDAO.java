package com.star.dao;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.CommonFunctions;
import com.star.common.MaintanenceResponse;
import com.star.common.SessionUtilInformix;
import com.star.linkage.ar.ARDesboard;
import com.star.linkage.ar.ARDesboardBrowseOutput;
import com.star.linkage.ar.AREnquiryBrowseOutput;
import com.star.linkage.ar.AREnquiryInfo;
import com.star.linkage.ar.ARPaymentBrowseOutput;
import com.star.linkage.ar.ARPaymentInfo;
import com.star.linkage.ar.ARPmtRecvData;
import com.star.linkage.ar.ARPmtRecvDataBrowseOutput;
import com.star.linkage.ar.AdvancePaymentBrowseOutput;
import com.star.linkage.ar.AdvancePaymentInfo;
import com.star.linkage.ar.CashReceiptBrowseOutput;
import com.star.linkage.ar.CashReceiptInfo;
import com.star.linkage.ar.GLDistributionBrowseOutput;
import com.star.linkage.ar.GLDistributionInfo;
import com.star.linkage.ar.SessionBrowseOutput;
import com.star.linkage.ar.SessionInfo;
import com.star.linkage.ar.UnappliedDebitBrowseOutput;
import com.star.linkage.ar.UnappliedDebitInfo;
import com.star.linkage.ar.ValidateDiscountDateManOutput;

public class ARInfoDAO {

	private static Logger logger = LoggerFactory.getLogger(ARInfoDAO.class);

	public void readSessions(BrowseResponse<SessionBrowseOutput> starBrowseResponse, String cmpyId, String ssnId,
			String crcpNo, String cusId) {
		logger.info("AR Info : {}", new Object() {
		}.getClass().getEnclosingMethod().getName());

		Session session = null;
		
		String hql = "";
		CommonFunctions objCom = new CommonFunctions();

		try {
			session = SessionUtilInformix.getSession();
		

			hql = "SELECT CAST(crs_cmpy_id AS VARCHAR(3)) crs_cmpy_id, crs_ssn_id, CAST(crs_crcp_brh AS VARCHAR(3)) crs_crcp_brh, crs_jrnl_dt, CAST(usr_nm AS VARCHAR(35)) usr_nm, "
					+ "CAST(crs_bnk AS VARCHAR(3)) crs_bnk, crs_tot_nbr_chk, crs_tot_dep_amt, (select count(tsa_sts_actn)  FROM artcsh_rec, tcttsa_rec "
					+ "WHERE csh_ssn_id=crs_ssn_id and tsa_ref_pfx=csh_crcp_pfx and tsa_ref_no=csh_crcp_no and tsa_ref_itm=0 and tsa_ref_sbitm=0 "
					+ " and tsa_sts_typ='T' and tsa_sts_actn='A') AS sts_count, CAST(crs_dep_cry AS VARCHAR(3)) crs_dep_cry, CAST(crs_dep_exrt AS VARCHAR(14)) AS dep_exrt, crs_dep_dt, "
					+ "CAST(crs_ar_cry1 AS VARCHAR(3)) crs_ar_cry1, crs_ar_xexrt1, CAST(crs_ar_cry2 AS VARCHAR(3)) crs_ar_cry2, CAST(crs_ar_xexrt2 AS VARCHAR(14)) AS ar_xexrt2, CAST(crs_ar_cry3 AS VARCHAR(3)) crs_ar_cry3, "
					+ "CAST(crs_ar_xexrt3 AS VARCHAR(14)) AS ar_xexrt3 FROM artcrs_rec S, mxrusr_rec U where S.crs_lgn_id = U.usr_lgn_id AND crs_cmpy_id=:cmpy_id";

			if (ssnId.trim().length() > 0 || crcpNo.trim().length() > 0 || cusId.trim().length() > 0) {
				hql += " AND crs_ssn_id in (SELECT csh_ssn_id FROM artcsh_rec WHERE 1=1 ";

				if (ssnId.trim().length() > 0) {
					hql += " AND csh_ssn_id=:ssn_id";
				}
				if (crcpNo.trim().length() > 0) {
					hql += " AND csh_crcp_no=:crcp_no";
				}
				if (cusId.trim().length() > 0) {
					hql += " AND csh_cr_ctl_cus_id=:cus_id";
				}

				hql += ")";
			}

			hql += " order by crs_jrnl_dt desc";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", cmpyId);

			if (ssnId.trim().length() > 0) {
				queryValidate.setParameter("ssn_id", Integer.parseInt(ssnId));
			}
			if (crcpNo.trim().length() > 0) {
				queryValidate.setParameter("crcp_no", Integer.parseInt(crcpNo));
			}
			if (cusId.trim().length() > 0) {
				queryValidate.setParameter("cus_id", cusId);
			}

			List<Object[]> listSession = queryValidate.list();

			for (Object[] ssn : listSession) {

				SessionInfo output = new SessionInfo();

				output.setCmpyId(ssn[0].toString());
				output.setSsnId(ssn[1].toString());
				output.setBrh(ssn[2].toString());
				if (ssn[3] != null) {
					// output.setJrnlDtStr((commonFunctions.formatDateWithoutTime((Date)
					// request[2])));

					SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
					String formatDate = formatter.format((Date) ssn[3]);

					output.setJrnlDt(formatDate);
				}
				output.setLgnNm(ssn[4].toString());
				output.setBnk(ssn[5].toString());
				output.setTotNbrChk(ssn[6].toString());
				output.setTotDepAmt(ssn[7].toString());

				output.setTotDepAmtStr(objCom.formatAmount(Double.parseDouble(ssn[7].toString())));

				if (Integer.parseInt(ssn[8].toString()) == 0) {
					output.setSts("C");
				} else {
					output.setSts("A");
				}
				if (ssn[9] != null) {
					output.setDepCry(ssn[9].toString());
				}
				if (ssn[10] != null) {
					output.setDepExrt(ssn[10].toString());
				}
				if (ssn[11] != null) {
					// output.setJrnlDtStr((commonFunctions.formatDateWithoutTime((Date)
					// request[2])));

					SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
					String formatDate = formatter.format((Date) ssn[11]);

					output.setDepDt(formatDate);
				}
				if (ssn[12] != null) {
					output.setArCry1(ssn[12].toString());
				}
				if (ssn[13] != null) {
					output.setArXexrt1(ssn[13].toString());
				}
				if (ssn[14] != null) {
					output.setArCry2(ssn[14].toString());
				}
				if (ssn[15] != null) {
					output.setArXexrt2(ssn[15].toString());
				}
				if (ssn[16] != null) {
					output.setArCry3(ssn[16].toString());
				}
				if (ssn[17] != null) {
					output.setArXexrt3(ssn[17].toString());
				}

				starBrowseResponse.output.fldTblSsn.add(output);

			}

		} catch (Exception e) {
			logger.debug("AR Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void readCashReceipt(BrowseResponse<CashReceiptBrowseOutput> starBrowseResponse, String ssnId) {
		logger.info("AR Info : {}", new Object() {
		}.getClass().getEnclosingMethod().getName());

		Session session = null;
		String hql = "";
		CommonFunctions objCom = new CommonFunctions();

		try {
			session = SessionUtilInformix.getSession();

			hql = "SELECT CAST(csh_cmpy_id AS VARCHAR(3)) csh_cmpy_id, CAST(csh_crcp_pfx AS VARCHAR(2)) csh_crcp_pfx, csh_crcp_no, CAST(csh_cr_ctl_cus_id AS VARCHAR(8)) csh_cr_ctl_cus_id, "
					+ " csh_cus_chk_amt, CAST(csh_cus_chk_no AS VARCHAR(10)) csh_cus_chk_no, CAST(csh_desc30 AS VARCHAR(30)) csh_desc30,( select tsa_sts_actn from tcttsa_rec "
					+ " where tsa_ref_pfx=csh_crcp_pfx and tsa_ref_no=csh_crcp_no and tsa_ref_itm=0 and tsa_ref_sbitm=0 and tsa_sts_typ='T') rcpt_sts "
					+ "FROM artcsh_rec WHERE csh_ssn_id=:ssn_id";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("ssn_id", Integer.parseInt(ssnId));

			List<Object[]> listSession = queryValidate.list();

			for (Object[] ssn : listSession) {

				CashReceiptInfo output = new CashReceiptInfo();

				output.setCmpyId(ssn[0].toString());
				output.setCrPfx(ssn[1].toString());
				output.setCrNo(ssn[2].toString());
				output.setCusId(ssn[3].toString());
				output.setChkAmt(ssn[4].toString());
				output.setChkAmtStr(objCom.formatAmount(Double.parseDouble(ssn[4].toString())));
				output.setChkNo(ssn[5].toString());
				output.setDesc(ssn[6].toString());
				output.setSts(ssn[7].toString());
				output.setChkProof(calcCashRcptCheckProof(ssn[0].toString(), ssn[1].toString(), ssn[2].toString()));

				starBrowseResponse.output.fldTblCashRcpt.add(output);

			}

		} catch (Exception e) {
			// TODO: handle exception
			logger.debug("AR Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void readARPayments(BrowseResponse<ARPaymentBrowseOutput> starBrowseResponse, String cmpyId, String refPfx,
			String refNo) {
		logger.info("AR Info : {}", new Object() {
		}.getClass().getEnclosingMethod().getName());

		Session session = null;
		
		String hql = "";
		String cashRcptSts = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = "select CAST(csd_cmpy_id AS VARCHAR(3)) csd_cmpy_id, CAST(csd_ref_pfx AS VARCHAR(2)) csd_ref_pfx, csd_ref_no, csd_ref_itm, CAST(csd_ar_pfx AS VARCHAR(2)) csd_ar_pfx, "
					+ "csd_ar_no, csd_dsamt, csd_rcvd_amt, cast(rvh_upd_ref as varchar(22)) upd_ref, rvh_due_dt, rvh_disc_amt, "
					+ "CAST(rvh_sld_cus_id AS VARCHAR(8)) rvh_sld_cus_id, CAST(rvh_ar_brh AS VARCHAR(3)) rvh_ar_brh,( select tsa_sts_actn from tcttsa_rec "
					+ "  where tsa_cmpy_id=csd_cmpy_id and tsa_ref_pfx=csd_ref_pfx and tsa_ref_no=csd_ref_no and tsa_ref_itm=0 and tsa_ref_sbitm=0 and tsa_sts_typ='T') rcpt_sts, csd_disc_tkn_amt "
					+ "from artcsd_rec, artrvh_rec where csd_cmpy_id = rvh_cmpy_id "
					+ "and csd_ar_pfx=rvh_ar_pfx and csd_ar_no=rvh_ar_no and csd_ref_pfx=:ref_pfx and csd_ref_no=:ref_no";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("ref_pfx", refPfx);
			queryValidate.setParameter("ref_no", Integer.parseInt(refNo));

			List<Object[]> listSession = queryValidate.list();

			for (Object[] ssn : listSession) {

				ARPaymentInfo output = new ARPaymentInfo();

				output.setCmpyId(ssn[0].toString());
				output.setRefPfx(ssn[1].toString());
				output.setRefNo(ssn[2].toString());
				output.setRefItm(ssn[3].toString());
				output.setArPfx(ssn[4].toString());
				output.setArNo(ssn[5].toString());
				output.setDsAmt(ssn[6].toString());
				output.setRcvdAmt(ssn[7].toString());
				output.setUpdRef(ssn[8].toString());
				output.setDueDt(ssn[9].toString());
				output.setDiscAmt(ssn[10].toString());
				output.setCusId(ssn[11].toString());
				output.setBrh(ssn[12].toString());
				cashRcptSts = ssn[13].toString();
				output.setDiscTknAmt(ssn[14].toString());
				starBrowseResponse.output.fldTblARPymnt.add(output);
			}

			starBrowseResponse.output.rcptSts = cashRcptSts;

			if (refNo.length() > 0) {
				starBrowseResponse.output.chkProof = calcCashRcptCheckProof(cmpyId, refPfx, refNo);
			} else {
				starBrowseResponse.output.chkProof = new BigDecimal("0");
			}
			
			/* GET CUSTOMER ID */
			hql = " select cast(csh_cr_ctl_cus_id as varchar(8)) cus_id,cast(crs_crcp_brh as varchar(3)) brh,"
					+ " (select tsa_sts_actn from tcttsa_rec where "
					+ " tsa_cmpy_id=csh_cmpy_id and tsa_ref_pfx=csh_crcp_pfx and tsa_ref_no=csh_crcp_no and tsa_ref_itm=0 "
					+ " and tsa_ref_sbitm=0 and tsa_sts_typ='T') rcpt_sts,"
					+ " cast((select usr_nm from mxrusr_rec where usr_lgn_id=crs_lgn_id) as varchar(35)) usr_nm,"
					+ " (select trim(to_char(tsa_crtd_dtts, 'Month')) || ' ' || to_char(tsa_crtd_dtts, 'dd, yyyy')  from tcttsa_rec " 
					+ "	where tsa_cmpy_id = csh_cmpy_id and tsa_ref_pfx = csh_crcp_pfx and tsa_ref_no = csh_crcp_no"
					+ " and tsa_ref_itm = 0 and tsa_ref_sbitm = 0 and tsa_sts_typ = 'T') rcpt_crt_dt, to_char(crs_jrnl_dt,'MM/dd/yyyy') jrnl_dt"
					+ " from artcsh_rec, artcrs_rec where crs_ssn_id=csh_ssn_id and csh_cmpy_id=crs_cmpy_id "
					+ "and csh_cmpy_id=:cmpy_id and csh_crcp_pfx=:ref_pfx and csh_crcp_no=:ref_no";

			queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("ref_pfx", refPfx);
			queryValidate.setParameter("cmpy_id", cmpyId);
			queryValidate.setParameter("ref_no", Integer.parseInt(refNo));

			List<Object[]> cusLst = queryValidate.list();

			for (Object[] customer : cusLst) {

				starBrowseResponse.output.customerId = customer[0].toString();
				starBrowseResponse.output.ssnBrh = customer[1].toString();
				starBrowseResponse.output.rcptSts = customer[2].toString();
				starBrowseResponse.output.crtdUsr = customer[3].toString();
				starBrowseResponse.output.crtdDt = customer[4].toString();
				starBrowseResponse.output.jrnlDt = customer[5].toString();
			}

		} catch (Exception e) {
			// TODO: handle exception
			logger.debug("AR Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void readAREnquiry(BrowseResponse<AREnquiryBrowseOutput> starBrowseResponse, String cusId, String brh,
			String dueDt, String invNo, String cmpyId, String refPfx, String refNo, String cry) {
		logger.info("AR Info : {}", new Object() {
		}.getClass().getEnclosingMethod().getName());

		Session session = null;
		String gracePreiod = "0";
		String hql = "";
		CommonFunctions objCom = new CommonFunctions();

		try {
			session = SessionUtilInformix.getSession();
			
			hql = "select rpv_rto_val,1 from mxrrpv_rec where rpv_pgm_nm='arzcsd' and rpv_rto_nm='CDISC-GRC-DY' and rpv_rto_lvl='G' limit 1";
			
			Query queryValidate = session.createSQLQuery(hql);
			
			List<Object[]> listExo = queryValidate.list();
			
			for (Object[] exo : listExo) {
				
				gracePreiod = exo[0].toString();
			}
			
			
			hql = "SELECT CAST(rvh_cmpy_id AS VARCHAR(3)) rvh_cmpy_id, CAST(rvh_ar_pfx AS VARCHAR(2)) rvh_ar_pfx,"
					+ " rvh_ar_no, CAST(rvh_sld_cus_id AS VARCHAR(8)) rvh_sld_cus_id, "
					+ "CAST(rvh_desc30 AS VARCHAR(30)) rvh_desc30, "
					+ "CAST(rvh_cry AS VARCHAR(3)) rvh_cry, rvh_due_dt, "
					+ "CAST(rvh_ar_brh AS VARCHAR(3)) rvh_ar_brh, " + "CAST(rvh_upd_ref AS VARCHAR(22)) rvh_upd_ref, "
					+ "rvh_inv_dt, rvh_orig_amt, rvh_ip_amt, rvh_balamt, rvh_disc_amt, rvh_disc_dt, "
					+ "(case when (rvh_disc_dt - date(now() - INTERVAL '"+ gracePreiod +"' DAY)) is null OR (rvh_disc_dt - date(now() - INTERVAL '"+ gracePreiod +"' DAY)) < 0 then 'N' else 'Y' end) disc_apl"
					+ " FROM artrvh_rec WHERE rvh_balamt <> 0 ";

			if (!cusId.trim().equalsIgnoreCase("")) {
				hql += " AND rvh_sld_cus_id=:cus_id";
			}
			if (!brh.trim().equalsIgnoreCase("")) {
				hql += " AND rvh_ar_brh=:brh";
			}
			if (!dueDt.trim().equalsIgnoreCase("")) {
				hql += " AND to_char(rvh_due_dt, 'mm/dd/yyyy')=:due_dt";
			}
			if (!invNo.trim().equalsIgnoreCase("")) {
				hql += " AND trim(rvh_upd_ref)=trim(:upd_ref)";
			}

			if (cry.length() > 0) {
				hql += " AND rvh_cry=:cry";
			}
			
			queryValidate = session.createSQLQuery(hql);

			if (!cusId.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("cus_id", cusId);
			}
			if (!brh.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("brh", brh);
			}
			if (!dueDt.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("due_dt", dueDt);
			}
			if (!invNo.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("upd_ref", invNo);
			}

			if (cry.length() > 0) {
				queryValidate.setParameter("cry", cry);
			}
			

			List<Object[]> listEnquiry = queryValidate.list();

			for (Object[] enquiry : listEnquiry) {

				AREnquiryInfo output = new AREnquiryInfo();

				output.setCmpyId(enquiry[0].toString());
				output.setArPfx(enquiry[1].toString());
				output.setArNo(enquiry[2].toString());
				output.setCusId(enquiry[3].toString());
				output.setDesc(enquiry[4].toString());
				output.setCry(enquiry[5].toString());
				if (enquiry[6] != null) {
					SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
					String formatDate = formatter.format((Date) enquiry[6]);

					output.setDueDt(formatDate);
				}
				output.setBrh(enquiry[7].toString());
				output.setUpdRef(enquiry[8].toString());
				if (enquiry[9] != null) {
					SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
					String formatDate = formatter.format((Date) enquiry[9]);

					output.setInvDt(formatDate);
				}
				output.setOrigAmt(enquiry[10].toString());
				output.setOrigAmtStr(objCom.formatAmount(Double.parseDouble(enquiry[10].toString())));
				output.setIpAmt(enquiry[11].toString());
				output.setIpAmtStr(objCom.formatAmount(Double.parseDouble(enquiry[11].toString())));
				output.setBalamt(enquiry[12].toString());
				output.setBalamtStr(objCom.formatAmount(Double.parseDouble(enquiry[12].toString())));
				output.setDiscAmt(enquiry[13].toString());
				output.setDiscAmtStr(objCom.formatAmount(Double.parseDouble(enquiry[13].toString())));

				if (Double.parseDouble(output.getBalamt()) == Double.parseDouble(output.getIpAmt())) {
					output.setProcessFlg(true);
				} else {
					output.setProcessFlg(false);
				}
				
				if (enquiry[14] != null) {
					SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
					String formatDate = formatter.format((Date) enquiry[14]);

					output.setDiscDt(formatDate);
				}
				else
				{
					output.setDiscDt("");
				}
				
				output.setDiscFlg(enquiry[15].toString());

				starBrowseResponse.output.fldTblAREnqry.add(output);

			}
			
			
			
			

			if (refNo.length() > 0) {
				starBrowseResponse.output.chkProof = calcCashRcptCheckProof(cmpyId, refPfx, refNo);
			} else {
				starBrowseResponse.output.chkProof = new BigDecimal("0");
			}

		} catch (Exception e) {
			// TODO: handle exception
			logger.debug("AR Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void readAdvancePayment(BrowseResponse<AdvancePaymentBrowseOutput> starBrowseResponse, String cmpyId,
			String refPfx, String refNo) {
		logger.info("AR Info : {}", new Object() {
		}.getClass().getEnclosingMethod().getName());

		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = "select CAST(adp_desc30 AS VARCHAR(30)) adp_desc30, adp_adv_amt, CAST(crs_dep_cry AS VARCHAR(3)) crs_dep_cry,"
					+ "CAST(adp_ref_pfx AS VARCHAR(2)) adp_ref_pfx, adp_ref_no, adp_ref_itm "
					+ "from artadp_rec, artcrs_rec, artcsh_rec "
					+ "where crs_cmpy_id = csh_cmpy_id and crs_ssn_id = csh_ssn_id and csh_cmpy_id = adp_cmpy_id and "
					+ "csh_crcp_pfx = adp_ref_pfx and csh_crcp_no = adp_ref_no and adp_adv_ref_pfx='CJ' ";

			if (!cmpyId.trim().equalsIgnoreCase("")) {
				hql += " and adp_cmpy_id=:cmpy_id";
			}
			if (!refPfx.trim().equalsIgnoreCase("")) {
				hql += " and adp_ref_pfx=:ref_pfx";
			}
			if (!refNo.trim().equalsIgnoreCase("")) {
				hql += " and adp_ref_no=:ref_no";
			}

			Query queryValidate = session.createSQLQuery(hql);

			if (!cmpyId.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("cmpy_id", cmpyId);
			}
			if (!refPfx.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("ref_pfx", refPfx);
			}
			if (!refNo.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("ref_no", Integer.parseInt(refNo));
			}

			List<Object[]> listRecords = queryValidate.list();

			for (Object[] record : listRecords) {

				AdvancePaymentInfo output = new AdvancePaymentInfo();

				output.setDesc(record[0].toString());
				output.setAdvAmt(record[1].toString());
				output.setCry(record[2].toString());
				output.setRefPfx(record[3].toString());
				output.setRefNo(record[4].toString());
				output.setRefItm(record[5].toString());

				starBrowseResponse.output.fldTblAdvPmt.add(output);

			}

			starBrowseResponse.output.chkProof = calcCashRcptCheckProof(cmpyId, refPfx, refNo);

		} catch (Exception e) {
			// TODO: handle exception
			logger.debug("AR Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void readUnappliedDebit(BrowseResponse<UnappliedDebitBrowseOutput> starBrowseResponse, String cmpyId,
			String refPfx, String refNo) {
		logger.info("AR Info : {}", new Object() {
		}.getClass().getEnclosingMethod().getName());

		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = "select CAST(adp_desc30 AS VARCHAR(30)) adp_desc30, adp_adv_amt, CAST(crs_dep_cry AS VARCHAR(3)) crs_dep_cry, "
					+ "CAST(adp_ref_pfx AS VARCHAR(2)) adp_ref_pfx, adp_ref_no, adp_ref_itm "
					+ "from artadp_rec, artcrs_rec, artcsh_rec "
					+ "where crs_cmpy_id = csh_cmpy_id and crs_ssn_id = csh_ssn_id and csh_cmpy_id = adp_cmpy_id and "
					+ "csh_crcp_pfx = adp_ref_pfx and csh_crcp_no = adp_ref_no and adp_adv_ref_pfx='DA' ";

			if (!cmpyId.trim().equalsIgnoreCase("")) {
				hql += " and adp_cmpy_id=:cmpy_id";
			}
			if (!refPfx.trim().equalsIgnoreCase("")) {
				hql += " and adp_ref_pfx=:ref_pfx";
			}
			if (!refNo.trim().equalsIgnoreCase("")) {
				hql += " and adp_ref_no=:ref_no";
			}

			Query queryValidate = session.createSQLQuery(hql);

			if (!cmpyId.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("cmpy_id", cmpyId);
			}
			if (!refPfx.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("ref_pfx", refPfx);
			}
			if (!refNo.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("ref_no", Integer.parseInt(refNo));
			}

			List<Object[]> listRecords = queryValidate.list();

			for (Object[] record : listRecords) {

				UnappliedDebitInfo output = new UnappliedDebitInfo();

				output.setDesc(record[0].toString());
				output.setDrAmt(record[1].toString());
				output.setCry(record[2].toString());
				output.setRefPfx(record[3].toString());
				output.setRefNo(record[4].toString());
				output.setRefItm(record[5].toString());

				starBrowseResponse.output.fldTblUnappliedDr.add(output);

			}

			starBrowseResponse.output.chkProof = calcCashRcptCheckProof(cmpyId, refPfx, refNo);

		} catch (Exception e) {
			// TODO: handle exception
			logger.debug("AR Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void readGLDistribution(BrowseResponse<GLDistributionBrowseOutput> starBrowseResponse, String cmpyId,
			String refPfx, String refNo) {
		logger.info("AR Info : {}", new Object() {
		}.getClass().getEnclosingMethod().getName());

		Session session = null;
		String hql = "";
		GLInfoDAO dao = new GLInfoDAO();

		try {
			session = SessionUtilInformix.getSession();

			hql = "select CAST(gds_bsc_gl_acct AS VARCHAR(8)), CAST(bga_desc30 AS VARCHAR(30)), CAST(gds_sacct AS VARCHAR(30)), gds_dr_amt, "
					+ " gds_cr_amt, CAST(gds_dist_rmk AS VARCHAR(50)) gds_dist_rmk, "
					+ "CAST(gds_ref_pfx AS VARCHAR(2)) gds_ref_pfx, gds_ref_no, gds_ref_itm "
					+ "from tctgds_rec, glrbga_rec where bga_bsc_gl_acct = gds_bsc_gl_acct ";

			if (!cmpyId.trim().equalsIgnoreCase("")) {
				hql += " and gds_cmpy_id=:cmpy_id";
			}
			if (!refPfx.trim().equalsIgnoreCase("")) {
				hql += " and gds_ref_pfx=:ref_pfx";
			}
			if (!refNo.trim().equalsIgnoreCase("")) {
				hql += " and gds_ref_no=:ref_no";
			}

			Query queryValidate = session.createSQLQuery(hql);

			if (!cmpyId.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("cmpy_id", cmpyId);
			}
			if (!refPfx.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("ref_pfx", refPfx);
			}
			if (!refNo.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("ref_no", Integer.parseInt(refNo));
			}

			List<Object[]> listRecords = queryValidate.list();

			for (Object[] record : listRecords) {

				GLDistributionInfo output = new GLDistributionInfo();

				output.setGlAcct(record[0].toString());
				output.setDesc(record[1].toString());

				if (record[2].toString().matches("^[0]+$")) {
					output.setSacct("");
				} else {

					String subAcct = dao.getSubAcctClnt(record[2].toString());

					output.setSacct(subAcct);
				}

				output.setDrAmt(record[3].toString());
				output.setCrAmt(record[4].toString());
				output.setRmk(record[5].toString());
				output.setRefPfx(record[6].toString());
				output.setRefNo(record[7].toString());
				output.setRefItm(record[8].toString());

				starBrowseResponse.output.fldTblGLDist.add(output);

			}

			starBrowseResponse.output.chkProof = calcCashRcptCheckProof(cmpyId, refPfx, refNo);

		} catch (Exception e) {
			logger.debug("AR Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public BigDecimal calcCashRcptCheckProof(String cmpyId, String refPfx, String refNo) {
		logger.info("AR Info : {}", new Object() {
		}.getClass().getEnclosingMethod().getName());

		Session session = null;
		String hql = "";
		BigDecimal chkProof = new BigDecimal(0);
		BigDecimal chkAmt = new BigDecimal(0);
		BigDecimal arPayment = new BigDecimal(0);
		BigDecimal advPymnt = new BigDecimal(0);
		BigDecimal unappliedDebit = new BigDecimal(0);
		BigDecimal glDebit = new BigDecimal(0);
		BigDecimal glCredit = new BigDecimal(0);

		try {
			session = SessionUtilInformix.getSession();

			// Get Check Amount
			hql = "SELECT csh_cus_chk_amt, 1 FROM artcsh_rec WHERE 1=1 ";

			if (!cmpyId.trim().equalsIgnoreCase("")) {
				hql += " AND csh_cmpy_id=:cmpy_id";
			}
			if (!refPfx.trim().equalsIgnoreCase("")) {
				hql += " AND csh_crcp_pfx=:ref_pfx";
			}
			if (!refNo.trim().equalsIgnoreCase("")) {
				hql += " AND csh_crcp_no=:ref_no";
			}

			Query queryValidate = session.createSQLQuery(hql);

			if (!cmpyId.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("cmpy_id", cmpyId);
			}
			if (!refPfx.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("ref_pfx", refPfx);
			}
			if (!refNo.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("ref_no", Integer.parseInt(refNo));
			}

			List<Object[]> listRecords = queryValidate.list();

			for (Object[] record : listRecords) {
				if (record[0] != null) {
					chkAmt = new BigDecimal(record[0].toString());
				}
			}
			System.out.println("chkAmt--->" + chkAmt);

			hql = "SELECT sum(csd_dsamt-csd_disc_tkn_amt), 1 FROM artcsd_rec WHERE 1=1 ";

			if (!cmpyId.trim().equalsIgnoreCase("")) {
				hql += " AND csd_cmpy_id=:cmpy_id";
			}
			if (!refPfx.trim().equalsIgnoreCase("")) {
				hql += " AND csd_ref_pfx=:ref_pfx";
			}
			if (!refNo.trim().equalsIgnoreCase("")) {
				hql += " AND csd_ref_no=:ref_no";
			}

			queryValidate = session.createSQLQuery(hql);

			if (!cmpyId.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("cmpy_id", cmpyId);
			}
			if (!refPfx.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("ref_pfx", refPfx);
			}
			if (!refNo.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("ref_no", Integer.parseInt(refNo));
			}

			listRecords = queryValidate.list();

			for (Object[] record : listRecords) {
				if (record[0] != null) {
					arPayment = new BigDecimal(record[0].toString());
				}
			}
			System.out.println("chkAmt--->" + chkAmt);

			// Get Advance Payment
			hql = "select SUM(adp_adv_amt) AS tot_adv_amt, 1 from artadp_rec, artcrs_rec, artcsh_rec where crs_cmpy_id = csh_cmpy_id and "
					+ "crs_ssn_id = csh_ssn_id and csh_cmpy_id = adp_cmpy_id and csh_crcp_pfx = adp_ref_pfx and csh_crcp_no = adp_ref_no "
					+ "and adp_adv_ref_pfx='CJ' ";

			if (!cmpyId.trim().equalsIgnoreCase("")) {
				hql += " AND adp_cmpy_id=:cmpy_id";
			}
			if (!refPfx.trim().equalsIgnoreCase("")) {
				hql += " AND adp_ref_pfx=:ref_pfx";
			}
			if (!refNo.trim().equalsIgnoreCase("")) {
				hql += " AND adp_ref_no=:ref_no";
			}

			queryValidate = session.createSQLQuery(hql);

			if (!cmpyId.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("cmpy_id", cmpyId);
			}
			if (!refPfx.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("ref_pfx", refPfx);
			}
			if (!refNo.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("ref_no", Integer.parseInt(refNo));
			}

			listRecords = queryValidate.list();

			for (Object[] record : listRecords) {
				if (record[0] != null) {
					advPymnt = new BigDecimal(record[0].toString());
				}
			}
			System.out.println("advPymnt--->" + advPymnt);

			// Get Unapplied Debit
			hql = "select SUM(adp_adv_amt) AS tot_adv_amt, 1 from artadp_rec, artcrs_rec, artcsh_rec where crs_cmpy_id = csh_cmpy_id and "
					+ "crs_ssn_id = csh_ssn_id and csh_cmpy_id = adp_cmpy_id and csh_crcp_pfx = adp_ref_pfx and csh_crcp_no = adp_ref_no "
					+ "and adp_adv_ref_pfx='DA' ";

			if (!cmpyId.trim().equalsIgnoreCase("")) {
				hql += " AND adp_cmpy_id=:cmpy_id";
			}
			if (!refPfx.trim().equalsIgnoreCase("")) {
				hql += " AND adp_ref_pfx=:ref_pfx";
			}
			if (!refNo.trim().equalsIgnoreCase("")) {
				hql += " AND adp_ref_no=:ref_no";
			}

			queryValidate = session.createSQLQuery(hql);

			if (!cmpyId.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("cmpy_id", cmpyId);
			}
			if (!refPfx.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("ref_pfx", refPfx);
			}
			if (!refNo.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("ref_no", Integer.parseInt(refNo));
			}

			listRecords = queryValidate.list();

			for (Object[] record : listRecords) {
				if (record[0] != null) {
					unappliedDebit = new BigDecimal(record[0].toString());
				}
			}
			System.out.println("unappliedDebit--->" + unappliedDebit);

			// Get GL Distribution
			hql = "select SUM(gds_dr_amt) AS tot_dr_amt, SUM(gds_cr_amt) AS tot_cr_amt from tctgds_rec, glrbga_rec where "
					+ "bga_bsc_gl_acct = gds_bsc_gl_acct ";

			if (!cmpyId.trim().equalsIgnoreCase("")) {
				hql += " AND gds_cmpy_id=:cmpy_id";
			}
			if (!refPfx.trim().equalsIgnoreCase("")) {
				hql += " AND gds_ref_pfx=:ref_pfx";
			}
			if (!refNo.trim().equalsIgnoreCase("")) {
				hql += " AND gds_ref_no=:ref_no";
			}

			queryValidate = session.createSQLQuery(hql);

			if (!cmpyId.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("cmpy_id", cmpyId);
			}
			if (!refPfx.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("ref_pfx", refPfx);
			}
			if (!refNo.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("ref_no", Integer.parseInt(refNo));
			}

			listRecords = queryValidate.list();

			for (Object[] record : listRecords) {
				if (record[0] != null) {
					glDebit = new BigDecimal(record[0].toString());
				}
				if (record[1] != null) {
					glCredit = new BigDecimal(record[1].toString());
				}
			}
			System.out.println("glDebit--->" + glDebit);
			System.out.println("glCredit--->" + glCredit);
			System.out.println("chkProof--->" + chkProof);
			chkProof = chkAmt;
			chkProof = chkProof.subtract(advPymnt);
			chkProof = chkProof.add(unappliedDebit);
			chkProof = chkProof.add(glDebit);
			chkProof = chkProof.subtract(glCredit);
			chkProof = chkProof.subtract(arPayment);
			System.out.println("chkProof--->" + chkProof);

		} catch (Exception e) {

			logger.debug("AR Info : {}", e.getMessage(), e);

		} finally {
			if (session != null) {
				session.close();
			}
		}

		return chkProof;
	}

	public void readDesboard(BrowseResponse<ARDesboardBrowseOutput> starBrowseResponse, String cusId, String agng,
			String brh, String invcDt) {
		logger.info("AR Info : {}", new Object() {
		}.getClass().getEnclosingMethod().getName());

		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = "select CAST(s.cus_cus_nm AS VARCHAR(15))cus_nm, CAST(h.rvh_cmpy_id AS VARCHAR(3))cmpy_id , CAST(h.rvh_ar_pfx AS VARCHAR(2))ar_pfx , h.rvh_ar_no, CAST(h.rvh_desc30 AS VARCHAR(30))desc30, CAST(h.rvh_sld_cus_id AS VARCHAR(8))cus_id , h.rvh_due_dt,CASE WHEN (CURRENT_DATE- h.rvh_due_dt)> -1 AND (CURRENT_DATE- h.rvh_due_dt)<= 30  then '0-30' when (CURRENT_DATE- h.rvh_due_dt)>30 AND (CURRENT_DATE- h.rvh_due_dt)<= 60 THEN '31-60' WHEN (CURRENT_DATE- h.rvh_due_dt)> 60 AND (CURRENT_DATE- h.rvh_due_dt) <= 90 THEN '61-90' WHEN (CURRENT_DATE- h.rvh_due_dt)> 90 THEN '90+' END AS aging_column, CAST(h.rvh_ar_brh AS VARCHAR(3))ar_brh, CAST(h.rvh_upd_ref AS VARCHAR(22))upd_ref, h.rvh_inv_dt , h.rvh_orig_amt , h.rvh_ip_amt , h.rvh_balamt from artrvh_rec h inner join arrcus_rec s on h.rvh_sld_cus_id= s.cus_cus_id and h.rvh_cmpy_id=s.cus_cmpy_id where h.rvh_balamt>0  ";
			if (!cusId.trim().equalsIgnoreCase("")) {
				hql += "  AND h.rvh_sld_cus_id =:cus_id";
			}

			if (!agng.trim().equalsIgnoreCase("")) {
				hql += " AND  (CURRENT_DATE- h.rvh_due_dt) BETWEEN :strt AND :end";
			}
			if (!brh.trim().equalsIgnoreCase("")) {
				hql += "  AND h.rvh_ar_brh =:brh";
			}
			if (!invcDt.trim().equalsIgnoreCase("")) {
				hql += "  AND h.rvh_inv_dt =:invcDt";
			}

			hql += "  ORDER BY h.rvh_due_dt desc";
			Query queryValidate = session.createSQLQuery(hql);
			if (!cusId.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("cus_id", cusId);
			}

			if (!agng.trim().equalsIgnoreCase("")) {
				int age = Integer.parseInt(agng.trim());
				int strt = -1;
				int end = -1;
				if (age > -1 && age <= 30) {
					strt = 0;
					end = 30;
				} else {
					if (age > 30 && age <= 60) {
						strt = 31;
						end = 60;
					} else {
						if (age > 60 && age <= 90) {
							strt = 61;
							end = 90;
						} else {
							if (age > 90) {
								strt = 91;
								end = 10000;
							}
						}
					}
				}
				queryValidate.setParameter("strt", strt);
				queryValidate.setParameter("end", end);
			}
			if (!brh.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("brh", brh);
			}
			if (!invcDt.trim().equalsIgnoreCase("")) {
				Date dt = new SimpleDateFormat("yyyy-MM-dd").parse(invcDt);
				System.out.println(dt);
				queryValidate.setParameter("invcDt", dt);
			}
			System.out.println(hql);
			List<Object[]> listRecords = queryValidate.list();

			for (Object[] record : listRecords) {

				ARDesboard output = new ARDesboard();

				if (record[0].toString() != null)
					output.setCusNm(record[0].toString());
				if (record[1].toString() != null)
					output.setCmpyId(record[1].toString());
				if (record[2].toString() != null)
					output.setArPfx(record[2].toString());
				if (record[3].toString() != null)
					output.setArNo(record[3].toString());
				if (record[4].toString() != null)
					output.setDesc30(record[4].toString());
				if (record[5].toString() != null)
					output.setCusId(record[5].toString());
				if (record[6].toString() != null) {
					String dt1 = new SimpleDateFormat("MM-dd-yyyy")
							.format(new SimpleDateFormat("yyyy-MM-dd").parse(record[6].toString()));
					output.setDueDt(dt1);
				}
				if (record[7] != null)
					output.setAging(record[7].toString());
				if (record[8].toString() != null)
					output.setBrh(record[8].toString());
				if (record[9].toString() != null)
					output.setUpdRef(record[9].toString());
				if (record[10].toString() != null) {
					String dt2 = new SimpleDateFormat("MM-dd-yyyy")
							.format(new SimpleDateFormat("yyyy-MM-dd").parse(record[10].toString()));
					output.setInvDt(dt2);
				}
				if (record[11].toString() != null)
					output.setOrigAmt(record[11].toString());
				if (record[12].toString() != null)
					output.setIpAmt(record[12].toString());
				if (record[13].toString() != null)
					output.setBalamt(record[13].toString());

				starBrowseResponse.output.fldTblARdsbrd.add(output);

			}

		} catch (Exception e) {
			// TODO: handle exception
			logger.debug("AR Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			if (session != null) {
				// session.close();
			}
		}
	}

	public List<ARPmtRecvData> readPmtRecvData(BrowseResponse<ARPmtRecvDataBrowseOutput> starBrowseResponse,
			String cusId, String agng, String brh, String stDt, String enDt) {
		logger.info("AR Info : {}", new Object() {
		}.getClass().getEnclosingMethod().getName());

		Session session = null;
		String hql = "";

		List<ARPmtRecvData> exportList = new ArrayList<ARPmtRecvData>();
		try {
			session = SessionUtilInformix.getSession();

			hql = "select CAST(s.cus_cus_nm AS VARCHAR(15))cus_nm, CAST(h.jsh_cmpy_id AS VARCHAR(3))cmpy_id , CAST(h.jsh_crcp_pfx AS VARCHAR(2))crcp_pfx , h.jsh_crcp_no,"
					+ " CAST(h.jsh_crcp_brh AS VARCHAR(3))ar_brh, h.jsh_jrnl_dt, h.jsh_upd_dt,CAST(h.jsh_lgn_id AS VARCHAR(8))lgnId, CAST(h.jsh_dep_cry AS VARCHAR(3))cry, h.jsh_dep_exrt , h.jsh_dep_dt ,"
					+ " CAST(h.jsh_cr_ctl_cus_id AS VARCHAR(8))cus_id , CAST(h.jsh_cus_chk_no AS VARCHAR(10))cus_chk_no , CAST(h.jsh_desc30 AS VARCHAR(30))desc30, "
					+ "h.jsh_cus_chk_amt from arjjsh_rec h inner join arrcus_rec s on h.jsh_cr_ctl_cus_id= s.cus_cus_id and h.jsh_cmpy_id=s.cus_cmpy_id where h.jsh_cus_chk_amt>0  ";

			if (!cusId.trim().equalsIgnoreCase("")) {
				hql += "  AND h.jsh_cr_ctl_cus_id =:cus_id";
			}
			if (!brh.trim().equalsIgnoreCase("")) {
				hql += "  AND h.jsh_crcp_brh =:brh";
			}
			if (!stDt.trim().equalsIgnoreCase("") && !enDt.trim().equalsIgnoreCase("")) {
				hql += "  AND h.jsh_dep_dt  BETWEEN :strtdt AND :enddt";
			}

			hql += "  ORDER BY h.jsh_dep_dt desc";
			Query queryValidate = session.createSQLQuery(hql);

			if (!cusId.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("cus_id", cusId);
			}
			if (!brh.trim().equalsIgnoreCase("")) {
				queryValidate.setParameter("brh", brh);
			}
			if (!stDt.trim().equalsIgnoreCase("") && !enDt.trim().equalsIgnoreCase("")) {
				Date st = new SimpleDateFormat("yyyy-MM-dd").parse(stDt);
				Date en = new SimpleDateFormat("yyyy-MM-dd").parse(enDt);
				queryValidate.setParameter("strtdt", st);
				queryValidate.setParameter("enddt", en);
			}
			System.out.println(hql);
			List<Object[]> listRecords = queryValidate.list();
			for (Object[] record : listRecords) {

				ARPmtRecvData output = new ARPmtRecvData();

				if (record[0] != null)
					output.setCusNm(record[0].toString());
				if (record[1] != null)
					output.setCmpyId(record[1].toString());
				if (record[2] != null)
					output.setArPfx(record[2].toString());
				if (record[3] != null)
					output.setCrcpNo(record[3].toString());
				if (record[4] != null)
					output.setCrcpBrh(record[4].toString());
				if (record[5] != null) {
					String jrnlDt = new SimpleDateFormat("MM-dd-yyyy")
							.format(new SimpleDateFormat("yyyy-MM-dd").parse(record[5].toString()));
					output.setJrnlDt(jrnlDt);
				}
				if (record[6] != null) {
					String updtDt = new SimpleDateFormat("MM-dd-yyyy")
							.format(new SimpleDateFormat("yyyy-MM-dd").parse(record[6].toString()));
					output.setUpdtDt(updtDt);
				}
				if (record[7] != null)
					output.setLgnId(record[7].toString());
				if (record[8] != null)
					output.setDepCry(record[8].toString());
				if (record[9] != null)
					output.setDepExrt(record[9].toString());
				if (record[10] != null) {
					String depDt = new SimpleDateFormat("MM-dd-yyyy")
							.format(new SimpleDateFormat("yyyy-MM-dd").parse(record[10].toString()));
					output.setDepDt(depDt);
				}
				if (record[11] != null)
					output.setCusId(record[11].toString());
				if (record[12] != null)
					output.setChkNo(record[12].toString());
				if (record[13] != null)
					output.setDesc30(record[13].toString());
				if (record[14] != null)
					output.setChkAmt(record[14].toString());
				exportList.add(output);
				starBrowseResponse.output.fldTblARPmtRecvdata.add(output);

			}

		} catch (Exception e) {
			// TODO: handle exception
			logger.debug("AR Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			if (session != null) {
				// session.close();
			}
		}
		return exportList;
	}
	
	public String vldtDiscDt(MaintanenceResponse<ValidateDiscountDateManOutput> starManResponse, String postDtStr, String discDtStr) {
		logger.info("AR Info : {}", new Object() {}.getClass().getEnclosingMethod().getName());

		Session session = null;
		Integer gracePeriod = 0;
		String hql = "";
		String rtnStr = "N";

		try {
			session = SessionUtilInformix.getSession();
			
			hql = "select cast(rpv_rto_val as varchar(50)) grace_prd,1 from mxrrpv_rec where rpv_pgm_nm='arzcsd' and rpv_rto_nm='CDISC-GRC-DY' and rpv_rto_lvl='G' limit 1";
			
			Query queryValidate = session.createSQLQuery(hql);
			
			List<Object[]> listExo = queryValidate.list();
			
			for (Object[] exo : listExo) {
				gracePeriod = Integer.parseInt( exo[0].toString() );
			}

		    
		    hql = "SELECT to_date('"+ discDtStr +"','MM/DD/YY') - (to_date('"+ postDtStr +"','MM/DD/YYYY') - "+ gracePeriod +") disc_apl";
					
			queryValidate = session.createSQLQuery(hql);
					
			int dateDiff = Integer.parseInt(queryValidate.list().get(0).toString());
			
			if( dateDiff >= 0 ) {
				rtnStr = "Y";
			}

		} catch (Exception e) {
			// TODO: handle exception
			logger.debug("AR Info : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
		
		return rtnStr;
	}
	
	public int vldJournalDays(String postDtStr) {
		logger.info("AR Info : {}", new Object() {}.getClass().getEnclosingMethod().getName());

		Session session = null;
		Integer journalDays = 0;
		String hql = "";
		String rtnStr = "N";

		try {
			session = SessionUtilInformix.getSession();
			
			hql = "select cast(rpv_rto_val as varchar(50)) jrnl_days,1 from mxrrpv_rec where rpv_pgm_nm='arzcrs' and rpv_rto_nm='CR-JRNL-DY' and rpv_rto_lvl in ('G','C') limit 1";
			
			Query queryValidate = session.createSQLQuery(hql);
			
			List<Object[]> listExo = queryValidate.list();
			
			for (Object[] exo : listExo) {
				journalDays = Integer.parseInt( exo[0].toString() );
			}
		    
		    /*hql = "SELECT to_date('"+ postDtStr +"','MM/DD/YYYY') - (CURRENT_DATE - "+ journalDays +") disc_apl";*/
			
		    hql = "SELECT (case when to_date('"+ postDtStr +"','MM/DD/YYYY') > CURRENT_DATE then "
		    		+ "to_date('"+ postDtStr +"','MM/DD/YYYY') - (CURRENT_DATE) else to_date('"+ postDtStr +"','MM/DD/YYYY') - (CURRENT_DATE - "+ journalDays +") end )disc_apl ";
		    
			queryValidate = session.createSQLQuery(hql);
					
			int dateDiff = Integer.parseInt(queryValidate.list().get(0).toString());
			
			if( dateDiff >= 0 && dateDiff < journalDays) {
				rtnStr = "Y";
				journalDays = 0;
			}

		} catch (Exception e) {
			// TODO: handle exception
			logger.debug("AR Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
		
		return journalDays;
	}
}
