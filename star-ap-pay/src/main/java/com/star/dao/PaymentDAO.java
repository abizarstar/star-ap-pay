package com.star.dao;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.CommonFunctions;
import com.star.common.FTPDownloadUpload;
import com.star.common.MaintanenceResponse;
import com.star.common.ManageDirectory;
import com.star.common.SessionUtil;
import com.star.common.SessionUtilInformix;
import com.star.common.SessionUtilInformixESP;
import com.star.common.ach.GenerateAPLinkFile;
import com.star.linkage.ach.InvoiceInfo;
import com.star.linkage.bank.BankInfo;
import com.star.linkage.common.PaymentMethod;
import com.star.linkage.company.CompanyInfo;
import com.star.linkage.cstmdocinfo.CstmDocInfoBrowseOutput;
import com.star.linkage.cstmdocinfo.VchrInfo;
import com.star.linkage.gl.JournalInfo;
import com.star.linkage.payment.PaymentBrowseOutput;
import com.star.linkage.payment.PaymentInfo;
import com.star.linkage.payment.PaymentInfoOutput;
import com.star.linkage.payment.PaymentManOutput;
import com.star.linkage.vendor.VendorOutput;
import com.star.modal.CstmParamInv;
import com.star.modal.CstmPayParams;
import com.star.modal.CstmPayParamsHis;

public class PaymentDAO {

	private static Logger logger = LoggerFactory.getLogger(PaymentDAO.class);

	public void addPayment(MaintanenceResponse<PaymentManOutput> starManResponse, PaymentInfo paymentInfo,
			String usrId) {
		Session session = null;
		Transaction tx = null;
		Date date = new Date();
		try {

			session = SessionUtil.getSession();
			tx = session.beginTransaction();
			CstmPayParams params = new CstmPayParams();
			params.setReqId(paymentInfo.getReqId());
			params.setCmpyId(paymentInfo.getCmpyId());
			params.setReqDt(date);
			params.setCrtdOn(date);
			params.setCrtdBy(usrId);
			params.setReqActnBy(paymentInfo.getReqActnBy());
			params.setReqUserBy(paymentInfo.getReqUserBy());
			params.setPaySts(paymentInfo.getPaySts());

			if (paymentInfo.getPaySts().equals("S")) {
				params.setReqUserOn(date);
			}			
			params.setIssueDt(new SimpleDateFormat("yyyy-MM-dd").format(new Date(paymentInfo.getIssueDate())));
			session.save(params);

			addPaymentInv(paymentInfo, session);

			/* ADDED USER ID WHILE SAVING IT */
			paymentInfo.setReqUserBy(usrId);
			addPropHistory(session, paymentInfo, date);

			/*
			 * if (paymentInfo.getPaySts().equals("S")) { String sSendToId =
			 * paymentInfo.getReqActnBy(); String emlFrm = "star@starsoftware.co"; String
			 * emlFrmNm = "Star Software"; String emlTo = "yogeshb@starsoftware.co"; String
			 * emlCc = ""; String emlBcc = ""; String emlSub =
			 * "New Invoice Payment Proposal Received."; String emlBody =
			 * "Hello Yogesh\r\n\r\nYou have a new invoice payment proposal received please look into it and take action(Approve/Reject) on it.\r\n\r\nRegards,\r\nStar Software"
			 * ; String emlDocId = "";
			 * 
			 * SendEmail sendmail = new SendEmail();
			 * 
			 * MailInfoInput mailInfoInput = new MailInfoInput();
			 * 
			 * mailInfoInput.setEmlFrm(emlFrm); mailInfoInput.setEmlFrmUsrNm(emlFrmNm);
			 * mailInfoInput.setEmlTo(emlTo); mailInfoInput.setEmlCc(emlCc);
			 * mailInfoInput.setEmlBcc(emlBcc); mailInfoInput.setEmlSub(emlSub);
			 * mailInfoInput.setEmlBody(emlBody); mailInfoInput.setEmlAttachIds(emlDocId);
			 * 
			 * sendmail.sendEmail(mailInfoInput); }
			 */

			tx.commit();
		} catch (Exception e) {

			logger.debug("Payment Info : {}", e.getMessage(), e);
			starManResponse.output.messages.add(e.getMessage());
			starManResponse.output.rtnSts = 1;

			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updatePayment(MaintanenceResponse<PaymentManOutput> starManResponse, PaymentInfo paymentInfo,
			String usrId, String cmpyId) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		Date date = new Date();
		PaymentInfo paymentInfoExist;

		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			paymentInfoExist = getReqIdStatus(paymentInfo.getReqId());

			paymentInfoExist.setPaySts(paymentInfo.getPaySts());
			paymentInfoExist.setRmk(paymentInfo.getRmk());

			if (paymentInfo.getReqActnBy().length() > 0) {
				paymentInfoExist.setReqActnBy(paymentInfo.getReqActnBy());
			}

			addPropHistory(session, paymentInfoExist, date);

			if (!paymentInfo.getPaySts().equals("A") && !paymentInfo.getPaySts().equals("R")) {
				hql = "update cstm_pay_params set req_user_by = :user, req_user_on = :req_date,req_actn_by=:actn_by, pay_sts=:sts "
						+ "where req_id in (:req)";
			} else {

				// hql = "update cstm_pay_params set req_actn_on=:actn_on, pay_sts=:sts,
				// req_actn_rmk=:rmk, pay_bnk_code=:bnk_code, pay_acct_no=:acct_no "
				// + "where req_id in (:req)";
				hql = "update cstm_pay_params set req_actn_by=:actn_by, req_actn_on=:actn_on, pay_sts=:sts, req_actn_rmk=:rmk, "
						+ "pay_bnk_code=:bnk_code, pay_acct_no=:acct_no where req_id in (:req)";
			}

			Query queryValidate = session.createSQLQuery(hql);

			if (!paymentInfo.getPaySts().equals("A") && !paymentInfo.getPaySts().equals("R")) {
				queryValidate.setParameter("user", paymentInfo.getReqUserBy());
				queryValidate.setParameter("req_date", date);
				queryValidate.setParameter("actn_by", paymentInfo.getReqActnBy());
			} else {
				queryValidate.setParameter("actn_by", usrId);
				queryValidate.setParameter("actn_on", date);
				queryValidate.setParameter("rmk", paymentInfo.getRmk());
				queryValidate.setParameter("bnk_code", paymentInfo.getBankCd());
				queryValidate.setParameter("acct_no", paymentInfo.getAcctNo());
			}

			queryValidate.setParameter("sts", paymentInfo.getPaySts());
			queryValidate.setParameter("req", paymentInfo.getReqId());

			queryValidate.executeUpdate();

			if (paymentInfo.getPaySts().equals("S") || paymentInfo.getPaySts().equals("P")) {
				delPaymentInv(paymentInfo, session);

				addPaymentInv(paymentInfo, session);
			}

			if (paymentInfo.getPaySts().equals("A")) {

				ManageDirectory directory = new ManageDirectory();

				GenerateAPLinkFile generateAPLink = new GenerateAPLinkFile();

				//String achOutput = generatePain001.getInvoiceDetails(session, paymentInfo.getReqId(), paymentInfo.getBankCd(),
				//		paymentInfo.getAcctNo());
				
				String apOutput = generateAPLink.getInvoiceDetails(session, paymentInfo.getReqId(), paymentInfo.getBankCd(),
								paymentInfo.getAcctNo());
				
				//PrintWriter fileout = new PrintWriter("d:\\APFileNew1983.txt");
				//fileout.print(apOutput);
				//fileout.close();
				
				SimpleDateFormat formatter = new SimpleDateFormat("MM_dd_yyyy_hh_mm_ss");
				String formatDate = formatter.format(new Date());

				String flNm = "ELEC_PAY_" + formatDate;

				String flExtn = ".txt";

				//boolean fileSts = directory.writeToFile(CommonConstants.ELEC_DIR + "/", flNm, flExtn, apOutput);
				boolean fileSts = directory.writeToFile("", flNm, flExtn, apOutput);

				if (fileSts == true) {

					/* PERFORM GL TRANSACTION */
					List<InvoiceInfo> invVchrList = new ArrayList<InvoiceInfo>();

					GetACHDetailsDAO detailsDAO = new GetACHDetailsDAO();
					invVchrList = detailsDAO.getPropInvoices(paymentInfo.getReqId());
					
					Collections.sort(invVchrList, new Comparator<InvoiceInfo>() {
						public int compare(InvoiceInfo s1, InvoiceInfo s2) {
							return s1.getVchrVenId().compareToIgnoreCase(s2.getVchrVenId());
						}
					});
					
					ValidateGLDAO gldao = new ValidateGLDAO();
					
					List<InvoiceInfo> invVchrs = gldao.getDistinctVendorInfo(invVchrList);
					
					/* NOT REQUIRED AS WE ARE SHOWING UPDATED DISBURSEMENT IN PROPOSAL SCREEN */
					/* glInfoDAO = new GLInfoDAO();*/

					//for (int i = 0; i < invVchrs.size(); i++) {
					//	gldao.addLedgerEntryOnLoad(starManResponse, cmpyId, usrId, invVchrs.get(i), paymentInfo.getReqId(), "");
					//}

					if (starManResponse.output.rtnSts == 0) {
						hql = "update cstm_pay_params set pay_fl_id = :fl_nm, pay_crtd_on = :crtd_on , pay_fl_sent_sts=:fl_snt_sts, batch_total_amt=:total_amt "
								+ " where req_id in (:req)";

						queryValidate = session.createSQLQuery(hql);

						//queryValidate.setParameter("fl_nm", flNm + ".xml");
						queryValidate.setParameter("fl_nm", flNm + ".txt");
						queryValidate.setParameter("crtd_on", date);
						queryValidate.setParameter("fl_snt_sts", false);
						queryValidate.setParameter("total_amt", paymentInfo.getBatchTotalAmt());
						//queryValidate.setParameter("issu_dt", paymentInfo.getIssueDate());
						//System.out.println("issu_dt "+ paymentInfo.getIssueDate());
						queryValidate.setParameter("req", paymentInfo.getReqId());

						queryValidate.executeUpdate();
					}
				}
			}

			String emlSub = "";
			String emlBody = "";

			/*
			 * if (paymentInfo.getPaySts().equals("A")) { emlSub =
			 * "Proposal(REQYB003) has been approved by Star Software."; emlBody =
			 * "Hello Yogesh\r\n\r\nYour proposal(REQYB003) has been approved by Star Software.\r\n\r\nRegards,\r\nStar Software"
			 * ; } else if (paymentInfo.getPaySts().equals("R")) { emlSub =
			 * "Proposal(REQYB003) has been rejected by Star Software."; emlBody =
			 * "Hello Yogesh\r\n\r\nYourproposal(REQYB003) has been rejected by Star Software, please check it.\r\n\r\nRegards,\r\nStar Software"
			 * ; }
			 * 
			 * if (paymentInfo.getPaySts().equals("A") ||
			 * paymentInfo.getPaySts().equals("R")) { String emlFrm =
			 * "star@starsoftware.co"; String emlFrmNm = "Star Software"; String emlTo =
			 * "yogeshb@starsoftware.co"; String emlCc = ""; String emlBcc = ""; String
			 * emlDocId = "";
			 * 
			 * SendEmail sendmail = new SendEmail();
			 * 
			 * MailInfoInput mailInfoInput = new MailInfoInput();
			 * 
			 * mailInfoInput.setEmlFrm(emlFrm); mailInfoInput.setEmlFrmUsrNm(emlFrmNm);
			 * mailInfoInput.setEmlTo(emlTo); mailInfoInput.setEmlCc(emlCc);
			 * mailInfoInput.setEmlBcc(emlBcc); mailInfoInput.setEmlSub(emlSub);
			 * mailInfoInput.setEmlBody(emlBody); mailInfoInput.setEmlAttachIds(emlDocId);
			 * 
			 * sendmail.sendEmail(mailInfoInput); }
			 */
			if (starManResponse.output.rtnSts == 0) {
				tx.commit();
			}
			else
			{
				tx.rollback();
			}

		} catch (Exception e) {

			logger.debug("Payment Info : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
	
	public String getInvoiceDetails(Session session, String proposalId, String bnkCode, String acctNo) 
	{
		//PrintWriter fileout = new PrintWriter("d:\\APFile.txt");
		String fileData="";
		String headerRecord="";
		String detailRecord="";
		for(int i=1;i<=5;i++)
		{
			//Header Record
			//1
			headerRecord="H";
			//2
			String paymentType="E";
			headerRecord+=paymentType;

			//3 N RJ/ZF
			double pa=555.455;
			String paymentAmount=formatValue(Double.toString(pa));
			if (paymentType.matches("(?i)A|B|C|D|P|Q|R|S|V|W")) {
				paymentAmount=formatValue(paymentAmount,paymentAmount.length(),15,'0','R');
			}
			else if (paymentType.matches("(?i)E|F|G|H|I|J|K|L|M|N|O|T|U")) {
				paymentAmount=formatValue(paymentAmount,paymentAmount.length(),15,'0','R');
			}
			else
				paymentAmount=formatValue(paymentAmount,paymentAmount.length(),15,'0','R');
			
			headerRecord+=paymentAmount;

			//4 Payment Date
			SimpleDateFormat f = new SimpleDateFormat("yyyyMMdd");
			String paymentDate=f.format(new Date());
			headerRecord+=paymentDate;

			//5 Cheque/Trace/Payment Number
			String CTPNumber="CTPNumber1234"+i;
			
			if (paymentType.matches("(?i)A|B|C|D|V|W")) {
				CTPNumber=formatValue(CTPNumber,CTPNumber.length(),30,' ','L');
			}
			else if (paymentType.matches("(?i)E|F|G|H|I|J|K")) {
				CTPNumber=formatValue(CTPNumber,CTPNumber.length(),30,' ','L');
			}
			else if (paymentType.matches("(?i)P|Q|R")) {
				CTPNumber=formatValue(CTPNumber,CTPNumber.length(),30,' ','L');
			}
			else if (paymentType.matches("(?i)L|M|N|O|S|T|U")) {
				CTPNumber=formatValue(CTPNumber,CTPNumber.length(),30,' ','L');
			}
			else
				CTPNumber=formatValue(CTPNumber,CTPNumber.length(),30,' ','L');
			
			headerRecord+=CTPNumber;

			//6 Reference Information
			String refInfo="Reference Information 123456"+i;
			if (paymentType.matches("(?i)A|B|C|D|V")) {
				refInfo=formatValue(refInfo,refInfo.length(),30,' ','L');
			}
			else if (paymentType.matches("(?i)L|M|N|T")) {
				refInfo=formatValue(refInfo,refInfo.length(),30,' ','L');
			}
			else
				refInfo=formatValue(refInfo,refInfo.length(),30,' ','L');
			headerRecord+=refInfo;

			//7 Currency (Originator) AN LJ/SF
			String curO="CAD";
			curO=formatValue(curO,curO.length(),3,' ','L');
			headerRecord+=curO;

			//8 Source Bank/Branch Number
			String bankNumber="3";
			String branchNumber="2";
			bankNumber=formatValue(bankNumber,bankNumber.length(),4,'0','R');
			branchNumber=formatValue(branchNumber,branchNumber.length(),5,'0','R');
			headerRecord+=bankNumber+branchNumber;

			//9 Source Bank Account Number
			String sBankAccNo="123456"+i;
			sBankAccNo=formatValue(sBankAccNo,sBankAccNo.length(),7,'0','R');
			headerRecord+=sBankAccNo;

			//10 Vendor ID 15 AN LJ/SF
			String venID="1010"+i;
			venID=formatValue(venID,venID.length(),15,' ','L');
			headerRecord+=venID;

			//11 Vendor Name 60 AN LJ/SF
			String venNm="A.K.Steel"+i;
			if (paymentType.matches("(?i)E|F|G|H|I|J|K|L|M|N|T|P|Q|R|T|V|W")) {
				venNm=formatValue(venNm,venNm.length(),60,' ','L');
			}
			else
				venNm=formatValue(venNm,venNm.length(),60,' ','L');
			headerRecord+=venNm;

			//12 Additional Vendor Name 60 AN LJ/SF
			String venAddNm="A.K"+i;
			venAddNm=formatValue(venAddNm,venAddNm.length(),60,' ','L');
			headerRecord+=venAddNm;

			//13 Vendor Address Line 1 55 AN LJ/SF
			String venAddLine1="Address Line 1"+i;
			if (paymentType.matches("(?i)S|U")) {
				venAddLine1=formatValue(venAddLine1,venAddLine1.length(),55,' ','L');
			}
			else
				venAddLine1=formatValue(venAddLine1,venAddLine1.length(),55,' ','L');
			headerRecord+=venAddLine1;

			//14 Vendor Address Line 2 OR GSAN (ACH)55 AN LJ/SF
			String venAddLine2="Address Line 2"+i;
			if (paymentType.matches("(?i)E|F|G|H|I|J|K")) {
				venAddLine2=formatValue(venAddLine2,venAddLine2.length(),55,' ','L');
			}
			else if (paymentType.matches("(?i)L|M|N|T|U")) {
				venAddLine2=formatValue(venAddLine2,venAddLine2.length(),55,' ','L');
			}
			else
				venAddLine2=formatValue(venAddLine2,venAddLine2.length(),55,' ','L');
			headerRecord+=venAddLine2;

			//15 Vendor Address Line 3 OR Short Name (ACH) 55 AN LJ/SF
			String venAddLine3="Address Line 3"+i;
			venAddLine3=formatValue(venAddLine3,venAddLine3.length(),55,' ','L');
			headerRecord+=venAddLine3;

			//16 Vendor city 30 AN LJ/SF
			String venCity="City";
			if (paymentType.matches("(?i)P|Q|R")) {
				venCity=formatValue(venCity,venCity.length(),30,' ','L');
			}
			else
				venCity=formatValue(venCity,venCity.length(),30,' ','L');
			headerRecord+=venCity;

			//17 Vendor Province/State 2 AN LJ/SF
			String venState="BC";
			venState=formatValue(venState,venState.length(),2,' ','L');
			headerRecord+=venState;

			//18 Vendor Postal/zip code 15 AN LJ/SF
			String venPostCode="462042";
			venPostCode=formatValue(venPostCode,venPostCode.length(),15,' ','L');
			headerRecord+=venPostCode;

			//19 Vendor country code 2 AN LJ/SF
			String venCntryCode="CA";
			venCntryCode=formatValue(venCntryCode,venCntryCode.length(),2,' ','L');
			headerRecord+=venCntryCode;

			//20 Vendor contact name 35 AN LJ/SF
			String venContNm="abcd";
			venContNm=formatValue(venContNm,venContNm.length(),35,' ','L');
			headerRecord+=venContNm;

			//21 Vendor Fax number 15 AN LJ/SF
			String venFaxNo="1212"+i;
			venFaxNo=formatValue(venFaxNo,venFaxNo.length(),15,' ','L');
			headerRecord+=venFaxNo;	    

			//22 Vendor email 80 AN LJ/SF
			String venEmail="john.smith@abccompany.com";
			venEmail=formatValue(venEmail,venEmail.length(),80,' ','L');
			headerRecord+=venEmail;

			//23 Remittance language 1 AN E = English, F = French, Blank = bilingual
			String remitLang="E";
			remitLang=formatValue(remitLang,remitLang.length(),1,' ','L');
			headerRecord+=remitLang;

			//24 Vendor Bank/Branch Number  9
			String venBankNumber="3";
			String venBranchNumber="2";
			venBankNumber=formatValue(venBankNumber,venBankNumber.length(),4,'0','R');
			venBranchNumber=formatValue(venBranchNumber,venBranchNumber.length(),5,'0','R');
			headerRecord+=venBankNumber+venBranchNumber;

			//25 Vendor Bank Account Number / IBAN #  35 AN LJ SF
			String venBankAccNo="123456"+i;
			venBankAccNo=formatValue(venBankAccNo,venBankAccNo.length(),35,' ','L');
			headerRecord+=venBankAccNo;

			//26 Cheque Issuance Delivery Options/  OR ACH PDS/PAD Outside Canada Transaction Code 3 AN RJ SF
			String CIDOptions="999";
			CIDOptions=formatValue(CIDOptions,CIDOptions.length(),3,' ','R');
			headerRecord+=CIDOptions;

			//27 D/A and Payee Match Transaction Code/ OR ACH PDS/PAD Outside Canada Transaction Type 3 AN RJ SF
			String DAPMTransCode="BUS";
			DAPMTransCode=formatValue(DAPMTransCode,DAPMTransCode.length(),3,' ','R');
			headerRecord+=DAPMTransCode;

			//28 Currency (Receiver) AN LJ/SF
			String curR="CAD";
			curR=formatValue(curR,curR.length(),3,' ','L');
			headerRecord+=curR;

			//29 Currency Conversion Code 1 AN LJ/SF
			//Mandatory for Wire Payments Only (Services = P,Q,R)
			//“1” = No currency conversion required
			//“2” = Currency conversion required
			if(curO.equalsIgnoreCase(curR))
				headerRecord+="1";
			else
				headerRecord+="2";

			//30 Special Instruction 1 35 AN LJ/SF
			String speInst1="Sp inst 1"+i;
			speInst1=formatValue(speInst1,speInst1.length(),35,' ','L');
			headerRecord+=speInst1;

			//31 Special Instruction 2 35 AN LJ/SF
			String speInst2="Sp inst 2"+i;
			speInst2=formatValue(speInst2,speInst2.length(),35,' ','L');
			headerRecord+=speInst2;

			//32 Special Instruction 3 35 AN LJ/SF
			String speInst3="Sp inst 3"+i;
			speInst3=formatValue(speInst3,speInst3.length(),35,' ','L');
			headerRecord+=speInst3;

			//33 Vendor Bank Name 35 AN LJ/SF
			String venBankNm="Vendor Bank";
			venBankNm=formatValue(venBankNm,venBankNm.length(),35,' ','L');
			headerRecord+=venBankNm;

			//34 Vendor Bank Address Line 1 35 AN LJ/SF
			String venBankAddLine1="Vendor bank Address Line 1";
			venBankAddLine1=formatValue(venBankAddLine1,venBankAddLine1.length(),35,' ','L');
			headerRecord+=venBankAddLine1;

			//35 Vendor Bank Address Line 2 35 AN LJ/SF
			String venBankAddLine2="Vendor bank Address Line 1";
			venBankAddLine2=formatValue(venBankAddLine2,venBankAddLine2.length(),35,' ','L');
			headerRecord+=venBankAddLine2;

			//36 Vendor Bank city 30 AN LJ/SF
			String venBankCity="vendor bank City";
			if (paymentType.matches("(?i)P|Q|R")) {
				venBankCity=formatValue(venBankCity,venBankCity.length(),30,' ','L');
			}
			else
				venBankCity=formatValue(venBankCity,venBankCity.length(),30,' ','L');
			headerRecord+=venBankCity;

			//37 Vendor Bank Province/State 2 AN LJ/SF
			String venBankState="BC";
			venBankState=formatValue(venBankState,venBankState.length(),2,' ','L');
			headerRecord+=venBankState;

			//38 Vendor bank Postal/zip code 9 AN LJ/SF
			String venBankPostCode="462042";
			venBankPostCode=formatValue(venBankPostCode,venBankPostCode.length(),9,' ','L');
			headerRecord+=venBankPostCode;

			//39 Vendor bank country code 2 AN LJ/SF
			String venBankCntryCode="CA";
			venBankCntryCode=formatValue(venBankCntryCode,venBankCntryCode.length(),2,' ','L');
			headerRecord+=venBankCntryCode;
			//fileout.println(headerRecord);
			fileData+=headerRecord+"\n";

			//Detailed Record
			int sizeDR=3;
			for(int j=1;j<=sizeDR;j++)
			{
				//Detail Record
				//1 Tag 1 AN
				detailRecord="D";

				//2 Reference Number 24 AN LJ SF
				String refNo="RF123"+j;
				refNo=formatValue(refNo,refNo.length(),24,' ','L');
				detailRecord+=refNo;			

				//3 Reference Description 30 AN LJ SF
				String refDesc="RF123 desc"+j;
				refDesc=formatValue(refDesc,refDesc.length(),30,' ','L');
				detailRecord+=refDesc;

				//4 Invoice Date
				SimpleDateFormat fdh = new SimpleDateFormat("yyyyMMdd");		 
				String invoiceDate=fdh.format(new Date());
				detailRecord+=invoiceDate;

				//5 Net Amount 15 N RJ ZF
				double na=555.455;
				String netAmount=formatValue(Double.toString(na));
				netAmount=formatValue(netAmount,netAmount.length(),15,'0','R');
				detailRecord+=netAmount;

				//6 Gross Amount 15 N RJ ZF
				double ga=555.455;
				String grossAmount=formatValue(Double.toString(ga));
				grossAmount=formatValue(grossAmount,grossAmount.length(),15,'0','R');
				detailRecord+=grossAmount;

				//7 Discount Amount 15 N RJ ZF
				double da=555.455;
				String discountAmount=formatValue(Double.toString(da));
				discountAmount=formatValue(discountAmount,discountAmount.length(),15,'0','R');
				detailRecord+=discountAmount;

				//8 Reference Information 80 AN LJ SF
				String refInfodh="Reference Information 123456 dh"+j;
				refInfodh=formatValue(refInfodh,refInfodh.length(),80,' ','L');
				detailRecord+=refInfodh;

				//fileout.println(detailRecord);
				fileData+=detailRecord+"\n";
			}

		}
		//fileout.println(headerRecord.length());
		//fileout.println(detailRecord.length());
		//fileout.close();
		return fileData;

	}


	/* FORMAT VALUE TO TWO DECIMAL PLACES */
	private static String formatValue(String amount) {
		DecimalFormat df = new DecimalFormat("#.00");
		String formattedValue = df.format(Double.parseDouble(amount));

		formattedValue = String.format("%.2f", Double.parseDouble(amount));

		return formattedValue;
	}

	private static String formatValue(String value, int len, int totallen, char ch, char lrj ) {
		String chAdd="";
		if(lrj=='R')
		{
			if(totallen>len)
			{
			for(int i=1;i<=totallen-len;i++)
			{
				chAdd+=ch;
			}
			value=chAdd+value;
			}
			else
			{
				for(int i=0;i<totallen;i++)
				{
					chAdd+=value.charAt(i);
				}
				value=chAdd;
			}
		}
		else
		{
			if(totallen>len)
			{
			for(int i=1;i<=totallen-len;i++)
			{
				chAdd+=ch;
			}
			value=value+chAdd;
			}
			else
			{
				for(int i=0;i<totallen;i++)
				{
					chAdd+=value.charAt(i);
				}
				value=chAdd;
			}
		}

		return value;
	}

	public void delPaymentInv(PaymentInfo paymentInfo, Session session) throws Exception {
		String hql = "";

		hql = "delete from cstm_param_inv where req_id=:req";

		Query queryValidate = session.createSQLQuery(hql);

		queryValidate.setParameter("req", paymentInfo.getReqId());

		queryValidate.executeUpdate();

	}

	public void addPaymentInv(PaymentInfo paymentInfo, Session session) throws Exception {

		for (int i = 0; i < paymentInfo.getInvcAddtnlData().size(); i++) {

			/*
			 * if (paymentInfo.getInvcAddtnlData().get(i).getVchrNo().contains("VR") ||
			 * paymentInfo.getInvcAddtnlData().get(i).getVchrNo().contains("DM") ||
			 * paymentInfo.getInvcAddtnlData().get(i).getVchrNo().contains("CM")) {
			 * CstmVchrInfo cstmVchrInfo = new CstmVchrInfo();
			 * 
			 * CstmVchrInfoDAO cstmVchrInfoDAO = new CstmVchrInfoDAO();
			 * 
			 * if (i == 0) { vchrCtlNo = cstmVchrInfoDAO.getVchrCtlNo(); } else { vchrCtlNo
			 * = vchrCtlNo + 1; }
			 * 
			 * cstmVchrInfo.setVchrCtlNo(vchrCtlNo);
			 * cstmVchrInfo.setVchrInvNo(paymentInfo.getInvcAddtnlData().get(i).getVchrNo())
			 * ; cstmVchrInfo.setVchrPayMthd(paymentInfo.getInvcAddtnlData().get(i).
			 * getPymntMthd());
			 * cstmVchrInfo.setNotes(paymentInfo.getInvcAddtnlData().get(i).getNote());
			 * session.save(cstmVchrInfo); }
			 */

			CstmParamInv cstmParamInv = new CstmParamInv();

			cstmParamInv.setInvNo(paymentInfo.getInvcAddtnlData().get(i).getVchrNo());
			cstmParamInv.setReqId(paymentInfo.getReqId());
			//cstmParamInv.setVchrPayMthd(paymentInfo.getInvcAddtnlData().get(i).getPymntMthd());
			cstmParamInv.setVchrPayMthd(3);
			cstmParamInv.setNotes(paymentInfo.getInvcAddtnlData().get(i).getNote());
			cstmParamInv.setInvSts(true);
			//cstmParamInv.setChkNo(paymentInfo.getInvcAddtnlData().get(i).getChkNo());
			session.save(cstmParamInv);
		}
	}

	public void readPayment(BrowseResponse<PaymentBrowseOutput> starBrowseResponse, PaymentInfo paymentInfo,
			String userId, String venId, String vchrNo, String userGrp, String cmpyId, int pageNo,String issueDate ) throws Exception {
		Session session = null;
		CommonFunctions commonFunctions = new CommonFunctions();

		double vchrAmt = 0;

		try {

			String hql = "";

			session = SessionUtil.getSession();

			hql = "select param.req_id, cmpy_id, req_dt, crtd_on, dtls1.usr_nm, req_user_by, "
					+ "coalesce ((select usr_nm from usr_dtls where usr_id= req_actn_by), '') actn_by, "
					+ "pay_sts,req_actn_rmk, pay_fl_id, pay_fl_sent_sts, " 
					+ "(((req_actn_on) AT TIME ZONE 'UTC') AT TIME ZONE 'EST') actn_on, crtd_by, req_actn_by, "
					+ "string_agg(inv_no, ',') inv_no_lst, " + "string_agg(distinct mthd_nm, ',') mthd_nm_lst, pay_bnk_code, pay_acct_no, "
					+ "(select count(*) from cstm_pay_params) totalReqId, batch_total_amt, issue_dt "
					+ "from cstm_pay_params param, " + "usr_dtls dtls1, cstm_param_inv inv, pay_mthd pay "
					+ "where (trim(crtd_by) = trim(dtls1.usr_id)) " + "and inv.vchr_pay_mthd = pay.id "
					+ "and param.req_id = inv.req_id";

			if (paymentInfo.getReqId().length() > 0) {
				hql = hql + " and param.req_id=:req";
			}

			if (paymentInfo.getPaySts().length() > 0) {
				hql = hql + " and pay_sts=:sts";

				if (!userGrp.equals("1") && !userGrp.equals("4")) {
					hql = hql + " and (dtls1.usr_id=:crtdby or req_actn_by=:crtdby)";
				}
			} else {
				if (!userGrp.equals("1") && !userGrp.equals("3")) {
					hql = hql + " and (dtls1.usr_id=:crtdby or req_actn_by=:crtdby)";
				}
			}

			if (vchrNo.length() > 0) {
				hql = hql + " and inv_no like '%" + vchrNo + "%'";
			}

			if (cmpyId.length() > 0) {
				hql = hql + " and cmpy_id=:cmpy";
			}
			
			if(pageNo>=0)
			{
				int skipVal = pageNo * 10;			
				hql = hql + " group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14 order by crtd_on desc offset " + skipVal + " limit " + CommonConstants.DB_OUT_REC;
			}
			else
			{
				hql = hql + " group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14 order by crtd_on desc ";
			}
			//hql = hql + " group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14 order by crtd_on desc";

			Query queryValidate = session.createSQLQuery(hql);

			if (paymentInfo.getReqId().length() > 0) {
				queryValidate.setParameter("req", paymentInfo.getReqId());
			}

			if (paymentInfo.getPaySts().length() > 0) {
				queryValidate.setParameter("sts", paymentInfo.getPaySts());
			}

			if (cmpyId.length() > 0) {
				queryValidate.setParameter("cmpy", cmpyId);
			}

			if (paymentInfo.getPaySts().length() > 0) 
			{
				if (!userGrp.equals("1") && !userGrp.equals("4")) {
					queryValidate.setParameter("crtdby", userId);
				}
			}
			else
			{
				if (!userGrp.equals("1") && !userGrp.equals("3")) {
					queryValidate.setParameter("crtdby", userId);
				}
			}

			List<Object[]> listRequest = queryValidate.list();

			for (Object[] request : listRequest) {
				vchrAmt = 0;

				PaymentInfoOutput params = new PaymentInfoOutput();

				params.setReqId(request[0].toString());
				params.setCmpyId(request[1].toString());

				if (request[2] != null) {
					params.setReqDtStr((commonFunctions.formatDateWithoutTime((Date) request[2])));
					params.setReqDt((Date) request[2]);
				}

				if (request[3] != null) {
					params.setCrtdDtStr((commonFunctions.formatDate((Date) request[3])));
					params.setCrtdOn((Date) request[3]);
				}

				params.setCrtdBy(request[4].toString());
				params.setReqUserBy(request[5].toString());
				params.setReqActnBy(request[6].toString());
				params.setPaySts(request[7].toString());

				if (request[8] != null) {
					params.setActnRmk(request[8].toString());
				} else {
					params.setActnRmk("");
				}

				if (request[9] != null) {
					params.setFlNm(request[9].toString());
				} else {
					params.setFlNm("");
				}
				params.setFlSentSts((Boolean) request[10]);

				if (request[11] != null) {
					params.setReqActnOnStr((commonFunctions.formatDate((Date) request[11])));
				}

				if (request[12] != null) {
					params.setCrtdById(request[12].toString());
				}

				if (request[13] != null) {
					params.setActnById(request[13].toString());
				}

				if (paymentInfo.getReqId().length() > 0) {

					if (request[20] != null) {
						params.setIssueDtStr(request[20].toString());
						//params.setIssueDt((Date) request[20]);
						readInvoice(params, params.getReqId(), session,"");
					}else
					{						
						readInvoice(params, params.getReqId(), session,issueDate);
					}
					getBankDetails(params, params.getCmpyId());

					if (request[15] != null) {
						params.setPayMthd(request[15].toString());
					}

				} else {

					/* readInvoiceList(params, params.getReqId(), session); */
					if (request[14] != null) {
						//vchrAmt = readVoucherTotalESP(request[14].toString(), params);
						params.setAmtTotal(Double.parseDouble(request[19].toString()));

						params.setAmtTotalStr(commonFunctions.formatAmount(Double.parseDouble(request[19].toString())));
					}

					if (request[15] != null) {
						params.setPayMthd(request[15].toString());
					}
				}
				
				if (request[16] != null) {
					params.setBnkCode(request[16].toString());
				}
				else
				{
					params.setBnkCode("");
				}
				
				if (request[17] != null) {
					params.setAccountNo(request[17].toString());
				}
				else
				{
					params.setAccountNo("");
				}
				params.setTotalReqId(Integer.parseInt(request[18].toString()));
				
				starBrowseResponse.output.fldTblPayment.add(params);
			}

		} catch (Exception e) {
			logger.debug("Payment Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}
	}

	public void readInvoice(PaymentInfoOutput params, String reqId, Session session, String issueDate) {
		String hql = "";

		double vchrAmt = 0;

		session = SessionUtil.getSession();

		List<PaymentMethod> paymentMethods = readVendorPaymentMethod();

		hql = "select inv_no, chk_no, vchr_pay_mthd, vchr_notes,inv_sts from cstm_param_inv where 1=1";

		hql = hql + " and req_id=:req";

		Query queryValidate = session.createSQLQuery(hql);

		queryValidate.setParameter("req", reqId);

		List<Object[]> listRequest = queryValidate.list();

		List<CstmParamInv> list = new ArrayList<CstmParamInv>();

		List<VchrInfo> listVoucherInfo = new ArrayList<VchrInfo>();

		for (Object[] request : listRequest) {

			VchrInfo vchrInfo = new VchrInfo();

			VendorOutput vendorOutput = new VendorOutput();

			readSpecificVoucherESP(request[0].toString(), vchrInfo,params, issueDate);

			if (request[1] != null) {
				vchrInfo.setChkNo(request[1].toString());
			} else {
				vchrInfo.setChkNo("");
			}

			if (request[2] != null) {
				/* PAYMENT METHOD LIST AGAINST VENDOR */
				List<PaymentMethod> methods = new ArrayList<PaymentMethod>();

				for (int i = 0; i < paymentMethods.size(); i++) {
					if (paymentMethods.get(i).getCmpyId().equals(vchrInfo.getCmpyId())
							&& paymentMethods.get(i).getVenId().equals(vchrInfo.getVchrVenId())) {

						if (request[2].toString().equals(paymentMethods.get(i).getMthdId())) {
							paymentMethods.get(i).setMthdDflt(true);
						} else {
							paymentMethods.get(i).setMthdDflt(false);
						}

						methods.add(paymentMethods.get(i));
					}
				}

				vchrInfo.setPayMthds(methods);

			}

			if (request[3] != null) {

				vchrInfo.setNote(request[3].toString());
			} else {
				vchrInfo.setNote("");
			}
			
			if (request[4] != null) {

				vchrInfo.setVchrPrsSts(request[4].toString());
			} else {
				vchrInfo.setVchrPrsSts("");
			}
			
			/* VchrInfo docInfo = getInvoices(params, session, request[1].toString()); */

			listVoucherInfo.add(vchrInfo);

			vchrAmt = vchrAmt + vchrInfo.getVchrAmt();
		}
		
		Collections.sort(listVoucherInfo, new Comparator<VchrInfo>() {
			public int compare(VchrInfo s1, VchrInfo s2) {
				
				String x1 = s1.getVchrVenId();
	            String x2 = s2.getVchrVenId();
	            
	            int sComp = 0;
	            
	            if(x1 != null && x2 != null)
	            {
	            	sComp = x1.compareTo(x2);
	            }
	            
	            if (sComp != 0) {
	                return sComp;
	             } 
	            
	            Integer i1 = Integer.parseInt(s1.getVchrNo());
	            Integer i2 = Integer.parseInt(s2.getVchrNo());
	            
	            return i1.compareTo(i2);
			}
		});
		
		params.setParamInvList(listVoucherInfo);
		params.setAmtTotal(vchrAmt);

		CommonFunctions commonFunctions = new CommonFunctions();
		params.setAmtTotalStr(commonFunctions.formatAmount(vchrAmt));

	}

	public VchrInfo getInvoices(PaymentInfoOutput params, Session session, String ctlNo) {
		String hql = "";

		double vchrAmt = 0;

		VchrInfo cstmParamInv = new VchrInfo();

		CommonFunctions commonFunctions = new CommonFunctions();

		session = SessionUtil.getSession();

		/*
		 * hql =
		 * "select vchr.*, doc.crt_dtts from cstm_vchr_info vchr, cstm_doc_info doc where ctl_no = vchr_ctl_no "
		 * ;
		 */

		hql = "select vchr.* from cstm_vchr_info vchr where 1=1 ";

		hql = hql + " and vchr_ctl_no=:ctl_no";

		Query queryValidate = session.createSQLQuery(hql);

		queryValidate.setParameter("ctl_no", Integer.parseInt(ctlNo));

		List<Object[]> listInvoices = queryValidate.list();

		List<VchrInfo> list = new ArrayList<VchrInfo>();

		for (Object[] invoice : listInvoices) {
			cstmParamInv.setCtlNo(Integer.parseInt(invoice[0].toString()));
			cstmParamInv.setVchrCtlNo(Integer.parseInt(invoice[0].toString()));

			if (invoice[1].toString().contains("VR") || invoice[1].toString().contains("DM")
					|| invoice[1].toString().contains("CM")) {
				readSpecificVoucher(invoice[1].toString(), cstmParamInv);

				vchrAmt = vchrAmt + cstmParamInv.getVchrAmt();
			} else {
				cstmParamInv.setVchrInvNo(invoice[1].toString());
				cstmParamInv.setVchrVenId(invoice[2].toString());
				cstmParamInv.setVchrVenNm(invoice[3].toString());
				cstmParamInv.setVchrBrh(invoice[4].toString());
				cstmParamInv.setVchrAmt(Double.parseDouble(invoice[5].toString()));
				cstmParamInv.setVchrExtRef(invoice[6].toString());

				if (invoice[7] != null) {
					cstmParamInv.setVchrInvDtStr(commonFunctions.formatDateWithoutTime((Date) invoice[7]));
					cstmParamInv.setVchrInvDt((Date) invoice[7]);
				}

				if (invoice[8] != null) {
					cstmParamInv.setVchrDueDtStr(commonFunctions.formatDateWithoutTime((Date) invoice[8]));
					cstmParamInv.setVchrDueDt((Date) invoice[8]);
				}

				/*
				 * if (invoice[11] != null) {
				 * cstmParamInv.setCrtDttsStr(commonFunctions.formatDate((Date) invoice[11]));
				 * cstmParamInv.setCrtDtts((Date) invoice[11]); }
				 */

				vchrAmt = vchrAmt + cstmParamInv.getVchrAmt();
			}

		}

		return cstmParamInv;
	}

	public int validateReqId(PaymentInfo paymentInfo) {
		Session session = null;
		String hql = "";
		int ircrdCount = 0;

		try {
			session = SessionUtil.getSession();

			hql = "select count(*) from cstm_pay_params where req_id=:req and cmpy_id=:cmpy";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("req", paymentInfo.getReqId());
			queryValidate.setParameter("cmpy", paymentInfo.getCmpyId());

			ircrdCount = Integer.parseInt(String.valueOf(queryValidate.list().get(0)));

		} catch (Exception e) {
			logger.debug("Payment Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return ircrdCount;
	}

	public PaymentInfo getReqIdStatus(String reqId) {
		Session session = null;
		String hql = "";
		PaymentInfo params = new PaymentInfo();

		try {
			session = SessionUtil.getSession();

			hql = "select req_id, cmpy_id, pay_sts, req_actn_by, crtd_by, req_actn_on, req_actn_rmk  from cstm_pay_params where req_id=:req";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("req", reqId);

			List<Object[]> listRequest = queryValidate.list();

			for (Object[] request : listRequest) {

				params.setReqId(request[0].toString());
				params.setCmpyId(request[1].toString());
				params.setPaySts(request[2].toString());
				params.setReqActnBy(request[3].toString());
				params.setReqUserBy(request[4].toString());
			}

		} catch (Exception e) {
			logger.debug("Payment Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return params;
	}

	public void addPropHistory(Session session, PaymentInfo paymentInfo, Date date) throws Exception {
		int iCount = getHistoryId(paymentInfo);

		CstmPayParamsHis params = new CstmPayParamsHis();
		params.setId(iCount);
		params.setReqId(paymentInfo.getReqId());
		params.setCmpyId(paymentInfo.getCmpyId());
		params.setReqActnBy(paymentInfo.getReqActnBy());
		if (paymentInfo.getPaySts().equals("R") || paymentInfo.getPaySts().equals("A")) {
			params.setReqActnOn(date);
		}
		params.setReqUserBy(paymentInfo.getReqUserBy());
		params.setPaySts(paymentInfo.getPaySts());
		params.setReqUserOn(date);
		params.setReqActnRmk(paymentInfo.getRmk());

		session.save(params);
	}

	public int getHistoryId(PaymentInfo paymentInfo) {
		Session session = null;
		String hql = "";
		int ircrdCount = 0;

		try {
			session = SessionUtil.getSession();

			hql = "select count(*) from cstm_pay_params_his where req_id=:req";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("req", paymentInfo.getReqId());

			ircrdCount = Integer.parseInt(String.valueOf(queryValidate.list().get(0)));

		} catch (Exception e) {
			logger.debug("Payment Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		ircrdCount = ircrdCount + 1;

		return ircrdCount;
	}

	/* GET ALL THE DEFINE FIELDS LIST */
	public void readVouchers(BrowseResponse<CstmDocInfoBrowseOutput> starBrowseRes, String venIdFrm, String venIdTo,
			String payMthd) throws Exception {
		Session session = null;
		String hql = "";
		CommonFunctions commonFunctions = new CommonFunctions();

		try {
			session = SessionUtil.getSession();

			hql = " select ctl_no,gat_ctl_no,crt_dtts,prs_dtts,vchr_inv_no,vchr_ven_id,vchr_ven_nm,vchr_brh,vchr_amt,vchr_ext_ref,vchr_inv_dt,vchr_due_dt,vchr_pay_term\r\n"
					+ "			       from cstm_doc_info doc_info , cstm_vchr_info vchr_info where\r\n"
					+ "			       doc_info.gat_ctl_no = vchr_info.vchr_ctl_no and ctl_no not in (select ctl_no from cstm_param_inv)";

			if (venIdFrm.length() > 0) {
				hql = hql + " and vchr_ven_id >= :ven_id_frm";
			}

			if (venIdTo.length() > 0) {
				hql = hql + " and vchr_ven_id <= :ven_id_to";
			}

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("ven_id_frm", venIdFrm);
			queryValidate.setParameter("ven_id_to", venIdTo);

			List<Object[]> listCstmDocVal = queryValidate.list();

			for (Object[] cstmDocVal : listCstmDocVal) {
				VchrInfo cstmdocInfo = new VchrInfo();

				cstmdocInfo.setCtlNo((Integer) (cstmDocVal[0]));
				cstmdocInfo.setGatCtlNo((Integer) (cstmDocVal[1]));

				if (cstmDocVal[2] != null) {
					cstmdocInfo.setCrtDttsStr(commonFunctions.formatDate((Date) cstmDocVal[2]));
					cstmdocInfo.setCrtDtts((Date) cstmDocVal[2]);
				}

				if (cstmDocVal[3] != null) {
					cstmdocInfo.setPrsDttsStr(commonFunctions.formatDate((Date) cstmDocVal[3]));
					cstmdocInfo.setPrsDtts((Date) cstmDocVal[3]);
				}
				cstmdocInfo.setVchrInvNo(String.valueOf(cstmDocVal[4]));
				cstmdocInfo.setVchrVenId(String.valueOf(cstmDocVal[5]));
				cstmdocInfo.setVchrVenNm(String.valueOf(cstmDocVal[6]));
				cstmdocInfo.setVchrBrh(String.valueOf(cstmDocVal[7]));
				cstmdocInfo.setVchrAmt(Double.parseDouble(String.valueOf(cstmDocVal[8])));
				cstmdocInfo.setVchrExtRef(String.valueOf(cstmDocVal[9]));

				if (cstmDocVal[10] != null) {
					cstmdocInfo.setVchrInvDtStr(commonFunctions.formatDateWithoutTime((Date) cstmDocVal[10]));
					cstmdocInfo.setVchrInvDt((Date) cstmDocVal[10]);
				}

				if (cstmDocVal[11] != null) {
					cstmdocInfo.setVchrDueDtStr(commonFunctions.formatDateWithoutTime((Date) cstmDocVal[11]));
					cstmdocInfo.setVchrDueDt((Date) cstmDocVal[11]);
				}

				cstmdocInfo.setVchrPayTerm(String.valueOf(cstmDocVal[12]));

				starBrowseRes.output.fldTblDoc.add(cstmdocInfo);
			}

		} catch (Exception e) {

			logger.debug("Payment Info : {}", e.getMessage(), e);
			starBrowseRes.output.rtnSts = 1;
			starBrowseRes.output.messages.add(CommonConstants.SERVER_ERR);

			session.close();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	/* GET ALL THE DEFINE FIELDS LIST */
	public void readStxVouchers(BrowseResponse<CstmDocInfoBrowseOutput> starBrowseRes, String usrId, String cmpyId, String venIdFrm,
			String venIdTo, String payMthd, String invcDt, String invcDtTo, String dueDt, String dueDtTo, String discDt,
			String discDtTo, String disbNo) throws Exception {
		Session session = null;
		String hql = "";
		/*String voucherList = "";*/
		CstmDocInfoDAO docInfoDAO = new CstmDocInfoDAO();

		List<CstmParamInv> invsListPending = new ArrayList<CstmParamInv>();

		invsListPending = docInfoDAO.getPendingVouchersSearch("", "", usrId);

		List<String> voucherList = getVoucherList(invsListPending);
		
		/*voucherList = docInfoDAO.getVoucherList(invsListPending);*/

		try {
			session = SessionUtilInformix.getSession();

			List<PaymentMethod> paymentMethods = readVendorPaymentMethod();

			// CHANGE IN QUERY TO SHOW AP ITEMS FROM GATEWAY - 26 FEB 2020
			/*
			 * hql =
			 * "SELECT trim(vch_ven_id) as ven_id, trim(ven_ven_long_nm) as ven_long_nm, trim(vch_ven_inv_no) as ven_inv_no,"
			 * +
			 * " trim(vch_lgn_id) as lgn_id, vch_ven_inv_dt, vch_due_dt, trim(vch_po_pfx) as po_pfx, vch_po_no, vch_po_itm,"
			 * +
			 * " trim(vch_cry) as cry, trim(vch_vchr_pfx) as vchr_pfx, vch_vchr_no, trim(vch_vchr_brh) as vchr_brh,"
			 * +
			 * " vch_ent_dt, vch_vchr_amt, trim(vct_vchr_cat) as vchr_cat, (select tsa_sts_actn from tcttsa_rec"
			 * +
			 * " where tsa_cmpy_id=vch_cmpy_id  and tsa_ref_pfx=vch_vchr_pfx and tsa_ref_no=vch_vchr_no and tsa_ref_itm=0 and"
			 * +
			 * " tsa_ref_sbitm=0 and tsa_sts_typ='T') trn_sts, (select tsa_sts_actn from tcttsa_rec where tsa_cmpy_id=vch_cmpy_id and"
			 * +
			 * " tsa_ref_pfx=vch_vchr_pfx and tsa_ref_no=vch_vchr_no and tsa_ref_itm=0  and tsa_ref_sbitm=0 and tsa_sts_typ='N') pay_sts, "
			 * +
			 * "cast((select rsn_desc30 from tcttsa_rec, scrrsn_rec 	where tsa_cmpy_id=vch_cmpy_id  and tsa_ref_pfx=vch_vchr_pfx and tsa_ref_no=vch_vchr_no and tsa_ref_itm=0 and 	"
			 * +
			 * "tsa_ref_sbitm=0 and tsa_sts_typ='T' and rsn_rsn_typ=tsa_rsn_typ and rsn_rsn=tsa_rsn) as varchar(30)) trn_rsn, 	"
			 * +
			 * "cast((select rsn_desc30 from tcttsa_rec, scrrsn_rec 	where tsa_cmpy_id=vch_cmpy_id  and tsa_ref_pfx=vch_vchr_pfx and tsa_ref_no=vch_vchr_no and tsa_ref_itm=0 "
			 * +
			 * "and 	tsa_ref_sbitm=0 and tsa_sts_typ='N' and rsn_rsn_typ=tsa_rsn_typ and rsn_rsn=tsa_rsn) as varchar(30)) pay_rsn, trim(vch_cmpy_id) as cmpy_id, round(vch_disc_amt, 2) vch_disc_amt,"
			 * +
			 * " (select tsa_sts_actn from tcttsa_rec,aptpyh_rec where tsa_cmpy_id=pyh_cmpy_id and "
			 * +
			 * " tsa_ref_pfx=pyh_ap_pfx and tsa_ref_no=pyh_ap_no and tsa_ref_itm=0  and tsa_ref_sbitm=0 and tsa_sts_typ='N' and pyh_opa_pfx = vch_vchr_pfx and pyh_opa_no= vch_vchr_no) pay_ap_sts,"
			 * +
			 * " cast((select rsn_desc30 from tcttsa_rec, scrrsn_rec,aptpyh_rec 	where tsa_cmpy_id=pyh_cmpy_id  and tsa_ref_pfx=pyh_ap_pfx and tsa_ref_no=pyh_ap_no and tsa_ref_itm=0 "
			 * +
			 * "	and tsa_ref_sbitm=0 and tsa_sts_typ='N' and pyh_opa_pfx = vch_vchr_pfx and pyh_opa_no= vch_vchr_no and rsn_rsn_typ=tsa_rsn_typ and rsn_rsn=tsa_rsn) as varchar(30)) pay_ap_rsn"
			 * +
			 * " FROM aptvch_rec, aprven_rec, aprvct_rec  WHERE 1=1 and ven_cmpy_id = vch_cmpy_id and ven_ven_id = vch_ven_id and "
			 * + " vct_vchr_cat = vch_vchr_cat";
			 * 
			 * 
			 * 
			 * if (venIdFrm.length() > 0) { hql = hql + " and vch_ven_id >= :ven_id_frm"; }
			 * 
			 * if (venIdTo.length() > 0) { hql = hql + " and vch_ven_id <= :ven_id_to"; }
			 * 
			 * if (CommonConstants.DB_TYP.equals("POS")) {
			 * 
			 * if (invcDt.length() > 0) { hql = hql +
			 * " and vch_ven_inv_dt >= to_date(:invc_dt, 'mm/dd/yyyy')"; }
			 * 
			 * if (invcDtTo.length() > 0) { hql = hql +
			 * " and vch_ven_inv_dt <= to_date(:invc_dt_to, 'mm/dd/yyyy')"; }
			 * 
			 * if (dueDt.length() > 0) { hql = hql +
			 * " and vch_due_dt >= to_date(:due_dt, 'mm/dd/yyyy')"; }
			 * 
			 * if (dueDtTo.length() > 0) { hql = hql +
			 * " and vch_due_dt <= to_date(:due_dt_to, 'mm/dd/yyyy')"; } } else if
			 * (CommonConstants.DB_TYP.equals("INF")) {
			 * 
			 * if (invcDt.length() > 0) { hql = hql +
			 * " and vch_ven_inv_dt >= to_date(:invc_dt, '%m/%d/%Y')"; }
			 * 
			 * if (invcDtTo.length() > 0) { hql = hql +
			 * " and vch_ven_inv_dt <= to_date(:invc_dt_to, '%m/%d/%Y')"; }
			 * 
			 * if (dueDt.length() > 0) { hql = hql +
			 * " and vch_due_dt >= to_date(:due_dt, '%m/%d/%Y')"; }
			 * 
			 * if (dueDtTo.length() > 0) { hql = hql +
			 * " and vch_due_dt <= to_date(:due_dt_to, '%m/%d/%Y')"; } }
			 * 
			 * hql = hql + " and vch_vchr_pfx || '-' || vch_vchr_no not in (:vchrList)";
			 */

			/* SECOND TIME CHANGE IN QUERY TO SHOW DISBURSED RECORDS */
			/*
			 * hql =
			 * "select trim(pyh_ven_id) as ven_id,trim(ven_ven_long_nm) as ven_long_nm, trim(pyh_ven_inv_no) as ven_inv_no, "
			 * +
			 * "(select vch_lgn_id from aptvch_rec where vch_cmpy_id = pyh_cmpy_id and vch_vchr_pfx = pyh_opa_pfx "
			 * +
			 * "and vch_vchr_no = pyh_opa_no), pyh_ven_inv_dt, pyh_due_dt, trim(pyh_po_pfx) as po_pfx, pyh_po_no,pyh_po_itm, "
			 * +
			 * "trim(pyh_cry) as cry,trim(pyh_opa_pfx) as vchr_pfx, pyh_opa_no,trim(pyh_ap_brh) as vchr_brh, pyh_ent_dt, "
			 * +
			 * "pyh_balamt, (select vch_vchr_cat from aptvch_rec where vch_cmpy_id = pyh_cmpy_id and vch_vchr_pfx "
			 * +
			 * "= pyh_opa_pfx and vch_vchr_no = pyh_opa_no) vch_cat,'C' trn_sts,'C' pay_sts,'' trn_rsn,'' pay_rsn, "
			 * +
			 * "trim(pyh_cmpy_id) as cmpy_id, round(pyh_disc_amt, 2) vch_disc_amt, tsa_sts_actn, "
			 * +
			 * "cast((select rsn_desc30 from scrrsn_rec where rsn_rsn_typ = tsa_rsn_typ and rsn_rsn = tsa_rsn) as varchar(30)) rsn_desc "
			 * +
			 * "from aptpyh_rec inner join aprven_rec on ven_cmpy_id = pyh_cmpy_id and ven_ven_id = pyh_ven_id "
			 * +
			 * "inner join tcttsa_rec on tsa_cmpy_id = pyh_cmpy_id and tsa_ref_pfx = pyh_ap_pfx and tsa_ref_no = pyh_ap_no "
			 * +
			 * "and tsa_ref_itm = 0 and tsa_ref_sbitm = 0 and tsa_sts_typ = 'N' where 1=1 and pyh_arch_trs=0 and pyh_balamt<>0"
			 * ;
			 */

			hql = "select trim(pyh_ven_id) as ven_id,trim(ven_ven_long_nm) as ven_long_nm, "
					+ "trim(pyh_ven_inv_no) as ven_inv_no, "
					+ "(select vch_lgn_id from aptvch_rec where vch_cmpy_id = pyh_cmpy_id and vch_vchr_pfx = pyh_opa_pfx "
					+ "and vch_vchr_no = pyh_opa_no), pyh_ven_inv_dt, pyh_due_dt, trim(pyh_po_pfx) as po_pfx, pyh_po_no,"
					+ "pyh_po_itm, trim(pyh_cry) as cry,trim(pyh_opa_pfx) as vchr_pfx, pyh_opa_no,trim(pyh_ap_brh) as vchr_brh, "
					+ "pyh_ent_dt, pyh_orig_amt, (select vch_vchr_cat from aptvch_rec where vch_cmpy_id = pyh_cmpy_id and "
					+ "vch_vchr_pfx = pyh_opa_pfx and vch_vchr_no = pyh_opa_no) vch_cat,'C' trn_sts,'C' pay_sts,'' trn_rsn,'' "
					+ "pay_rsn, trim(pyh_cmpy_id) as cmpy_id, round(pyh_disc_amt, 2) vch_disc_amt, pay.tsa_sts_actn, "
					+ "cast((select rsn_desc30 from scrrsn_rec where rsn_rsn_typ = pay.tsa_rsn_typ and rsn_rsn = pay.tsa_rsn) as varchar(30))"
					+ " rsn_desc,trn.tsa_sts_actn trn_sts_actn, "
					+ "cast((select rsn_desc30 from scrrsn_rec where rsn_rsn_typ = trn.tsa_rsn_typ and rsn_rsn = trn.tsa_rsn) as varchar(30))"
					+ " rsn_trn_desc, pyh_disc_dt disc_dt,"
					+ " (case when (pyh_disc_dt - date(now())) is null or (pyh_disc_dt - date(now()) < 0) then 'N' else 'Y' end) disc_sts,"
					+ " pyh_orig_amt - (case when (pyh_disc_dt - date(now())) is null or (pyh_disc_dt - date(now()) < 0) then 0 else pyh_disc_amt end) tot_amt"
					+ " from aptpyh_rec inner join aprven_rec on ven_cmpy_id = pyh_cmpy_id and "
					+ "ven_ven_id = pyh_ven_id inner join tcttsa_rec pay on pay.tsa_cmpy_id = pyh_cmpy_id "
					+ "and pay.tsa_ref_pfx = pyh_ap_pfx and pay.tsa_ref_no = pyh_ap_no and pay.tsa_ref_itm = 0 "
					+ "and pay.tsa_ref_sbitm = 0 and pay.tsa_sts_typ = 'N' inner join tcttsa_rec trn on trn.tsa_cmpy_id = pyh_cmpy_id "
					+ "and trn.tsa_ref_pfx = pyh_ap_pfx and trn.tsa_ref_no = pyh_ap_no and trn.tsa_ref_itm = 0 and trn.tsa_ref_sbitm = 0 "
					+ "and trn.tsa_sts_typ = 'T' where pyh_cmpy_id=:cmpy_id";

			if (venIdFrm.length() > 0) {
				hql = hql + " and pyh_ven_id >= :ven_id_frm";
			}

			if (venIdTo.length() > 0) {
				hql = hql + " and pyh_ven_id <= :ven_id_to";
			}
			
			if(disbNo.length() > 0)
			{
				hql = hql + " and pyh_cmpy_id||pyh_ap_pfx||pyh_ap_no in "
						+ "(select dsd_cmpy_id||dsd_ap_pfx||dsd_ap_no from aptdsd_rec where "
						+ "dsd_cmpy_id=:cmpy_id and dsd_dsb_pfx='DS' and dsd_dsb_no=:disb_no)";
			}

			if (CommonConstants.DB_TYP.equals("POS")) {

				if (invcDt.length() > 0) {
					hql = hql + " and pyh_ven_inv_dt >= to_date(:invc_dt, 'mm/dd/yyyy')";
				}

				if (invcDtTo.length() > 0) {
					hql = hql + " and pyh_ven_inv_dt <= to_date(:invc_dt_to, 'mm/dd/yyyy')";
				}

				if (dueDt.length() > 0) {
					hql = hql + " and pyh_due_dt >= to_date(:due_dt, 'mm/dd/yyyy')";
				}

				if (dueDtTo.length() > 0) {
					hql = hql + " and pyh_due_dt <= to_date(:due_dt_to, 'mm/dd/yyyy')";
				}

				if (discDt.length() > 0) {
					hql = hql + " and pyh_disc_dt >= to_date(:disc_dt, 'mm/dd/yyyy')";
				}

				if (discDtTo.length() > 0) {
					hql = hql + " and pyh_disc_dt <= to_date(:disc_dt_to, 'mm/dd/yyyy')";
				}

			} else if (CommonConstants.DB_TYP.equals("INF")) {

				if (invcDt.length() > 0) {
					hql = hql + " and pyh_ven_inv_dt >= to_date(:invc_dt, '%m/%d/%Y')";
				}

				if (invcDtTo.length() > 0) {
					hql = hql + " and pyh_ven_inv_dt <= to_date(:invc_dt_to, '%m/%d/%Y')";
				}

				if (dueDt.length() > 0) {
					hql = hql + " and pyh_due_dt >= to_date(:due_dt, '%m/%d/%Y')";
				}

				if (dueDtTo.length() > 0) {
					hql = hql + " and pyh_due_dt <= to_date(:due_dt_to, '%m/%d/%Y')";
				}

				if (discDt.length() > 0) {
					hql = hql + " and pyh_disc_dt >= to_date(:disc_dt, '%m/%d/%Y')";
				}

				if (discDtTo.length() > 0) {
					hql = hql + " and pyh_disc_dt <= to_date(:disc_dt_to, '%m/%d/%Y')";
				}
			}

			hql = hql + " and pyh_opa_pfx || '-' || pyh_opa_no not in (:vchrList)";

			hql = hql + " order by pyh_ven_id asc, pyh_due_dt asc";

			// TODO trn_sts is not NULL
			Query queryValidate = session.createSQLQuery(hql);

			if (venIdFrm.length() > 0) {
				queryValidate.setParameter("ven_id_frm", venIdFrm);
			}

			if (venIdTo.length() > 0) {
				queryValidate.setParameter("ven_id_to", venIdTo);
			}

			if (invcDt.length() > 0) {
				queryValidate.setParameter("invc_dt", invcDt);
			}

			if (dueDt.length() > 0) {
				queryValidate.setParameter("due_dt", dueDt);
			}

			if (invcDtTo.length() > 0) {
				queryValidate.setParameter("invc_dt_to", invcDtTo);
			}

			if (dueDtTo.length() > 0) {
				queryValidate.setParameter("due_dt_to", dueDtTo);
			}

			if (discDt.length() > 0) {
				queryValidate.setParameter("disc_dt", discDt);
			}

			if (discDtTo.length() > 0) {
				queryValidate.setParameter("disc_dt_to", discDtTo);
			}
			
			if(disbNo.length() > 0)
			{
				queryValidate.setParameter("disb_no", Integer.parseInt(disbNo));
			}
			
			queryValidate.setParameter("cmpy_id", cmpyId);
			
			/*List<String> items = Arrays.asList(voucherList.split("\\s*,\\s*"));*/

			queryValidate.setParameterList("vchrList", voucherList);

			List<Object[]> listCstmDocVal = queryValidate.list();
			CommonFunctions objCom = new CommonFunctions();
			for (Object[] cstmDocVal : listCstmDocVal) {

				VchrInfo vchrInfo = new VchrInfo();

				vchrInfo.setVchrVenId(String.valueOf(cstmDocVal[0]));
				vchrInfo.setVchrVenNm(String.valueOf(cstmDocVal[1]));
				vchrInfo.setVchrInvNo(String.valueOf(cstmDocVal[2]));

				if (cstmDocVal[3] != null) {
					vchrInfo.setUpldBy(String.valueOf(cstmDocVal[3]));
				} else {
					vchrInfo.setUpldBy("");
				}

				if (cstmDocVal[4] != null) {
					vchrInfo.setVchrInvDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[4])));
					vchrInfo.setVchrInvDt((Date) (cstmDocVal[4]));
				}
				if (cstmDocVal[5] != null) {
					vchrInfo.setVchrDueDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[5])));
					vchrInfo.setVchrDueDt((Date) (cstmDocVal[5]));
				}
				vchrInfo.setPoPfx(String.valueOf(cstmDocVal[6]));
				vchrInfo.setPoNo(String.valueOf(cstmDocVal[7]));
				vchrInfo.setPoItm(String.valueOf(cstmDocVal[8]));
				vchrInfo.setVchrCry(String.valueOf(cstmDocVal[9]));
				vchrInfo.setVchrPfx(String.valueOf(cstmDocVal[10]));
				vchrInfo.setVchrNo(String.valueOf(cstmDocVal[11]));
				vchrInfo.setVchrBrh(String.valueOf(cstmDocVal[12]));
				if (cstmDocVal[13] != null) {
					vchrInfo.setCrtDttsStr((objCom.formatDateWithoutTime((Date) cstmDocVal[13])));
					vchrInfo.setCrtDtts((Date) (cstmDocVal[13]));
				} else {
					vchrInfo.setCrtDttsStr("");
				}
				vchrInfo.setVchrAmt(Double.parseDouble(String.valueOf(cstmDocVal[14])));
				vchrInfo.setVchrAmtStr(objCom.formatAmount(Double.parseDouble(String.valueOf(cstmDocVal[14]))));

				if (cstmDocVal[15] != null) {
					vchrInfo.setVchrCat(String.valueOf(cstmDocVal[15]));
				} else {
					vchrInfo.setVchrCat("");
				}

				if (cstmDocVal[16] != null) {
					vchrInfo.setTransSts(String.valueOf(cstmDocVal[16]));
				} else {
					vchrInfo.setTransSts("");
				}

				if (cstmDocVal[17] != null) {
					vchrInfo.setPymntSts(String.valueOf(cstmDocVal[17]));
				} else {
					vchrInfo.setPymntSts("");
				}

				if (cstmDocVal[18] != null) {
					vchrInfo.setTransStsRsn(String.valueOf(cstmDocVal[18]));
				} else {
					vchrInfo.setTransStsRsn("");
				}

				if (cstmDocVal[19] != null) {
					vchrInfo.setPymntStsRsn(String.valueOf(cstmDocVal[19]));
				} else {
					vchrInfo.setPymntStsRsn("");
				}

				vchrInfo.setCmpyId(cstmDocVal[20].toString());
				vchrInfo.setDiscAmt(Double.parseDouble(String.valueOf(cstmDocVal[21])));
				vchrInfo.setDiscAmtStr(objCom.formatAmount(Double.parseDouble(String.valueOf(cstmDocVal[21]))));

				if (cstmDocVal[22] != null) {
					vchrInfo.setPymntPaySts(cstmDocVal[22].toString());
				} else {
					vchrInfo.setPymntPaySts("");
				}

				if (cstmDocVal[23] != null) {
					vchrInfo.setPymntPayStsRsn(cstmDocVal[23].toString());
				} else {
					vchrInfo.setPymntPayStsRsn("");
				}

				if (cstmDocVal[24] != null) {
					vchrInfo.setTransPaySts(cstmDocVal[24].toString());
				} else {
					vchrInfo.setTransPaySts("");
				}

				if (cstmDocVal[25] != null) {
					vchrInfo.setTransPayStsRsn(cstmDocVal[25].toString());
				} else {
					vchrInfo.setTransPayStsRsn("");
				}
				
				if (cstmDocVal[26] != null) {
					vchrInfo.setVchrDiscDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[26])));
					vchrInfo.setVchrDiscDt((Date) (cstmDocVal[26]));
				}
				else
				{
					vchrInfo.setVchrDiscDtStr("");
				}
				
				vchrInfo.setDiscFlg(cstmDocVal[27].toString());
				
				vchrInfo.setTotAmt(Double.parseDouble(String.valueOf(cstmDocVal[28])));
				vchrInfo.setTotAmtStr(objCom.formatAmount(Double.parseDouble(String.valueOf(cstmDocVal[28]))));

				/* PAYMENT METHOD LIST AGAINST VENDOR */
				List<PaymentMethod> methods = new ArrayList<PaymentMethod>();

				int iPayMthd = 0;

				for (int i = 0; i < paymentMethods.size(); i++) {
					if (paymentMethods.get(i).getCmpyId().equals(vchrInfo.getCmpyId())
							&& paymentMethods.get(i).getVenId().equals(vchrInfo.getVchrVenId())) {
						methods.add(paymentMethods.get(i));
					}

					if (payMthd.length() > 0) {
						List<String> payMthdList = Arrays.asList(payMthd.split("\\s*,\\s*"));

						if (payMthdList.contains(paymentMethods.get(i).getMthdId())
								&& paymentMethods.get(i).getVenId().equals(vchrInfo.getVchrVenId())
								&& paymentMethods.get(i).getCmpyId().equals(vchrInfo.getCmpyId())) {
							iPayMthd = 1;
						}
					}
				}

				vchrInfo.setPayMthds(methods);

				// || (vchrInfo.getTransSts().equals("H") || vchrInfo.getPymntSts().equals("H"))

				if (vchrInfo.getPymntPaySts().equals("C") || vchrInfo.getTransPaySts().equals("C")) {

					if (iPayMthd == 1) {
						starBrowseRes.output.fldTblDoc.add(vchrInfo);
					} else if (iPayMthd == 0) {
						vchrInfo.setHoldRsn(CommonConstants.PAY_MTHD_ERR + vchrInfo.getVchrVenId());

						starBrowseRes.output.fldTblInvHold.add(vchrInfo);
					}
				}
				
				if (vchrInfo.getPymntPaySts().equals("A") && vchrInfo.getTransPaySts().equals("A")) {
					
						vchrInfo.setHoldRsn(CommonConstants.VCHR_ACTV_ERR);

						starBrowseRes.output.fldTblInvHold.add(vchrInfo);
				}

				if (vchrInfo.getTransPaySts().equals("H") || vchrInfo.getPymntPaySts().equals("H")) {

					if (vchrInfo.getPymntPaySts().equals("H")) {
						vchrInfo.setHoldRsn(vchrInfo.getPymntPayStsRsn());
					}
					
					if (vchrInfo.getTransPaySts().equals("H")) {
						vchrInfo.setHoldRsn(vchrInfo.getTransPayStsRsn());
					}
					starBrowseRes.output.fldTblInvHold.add(vchrInfo);
				}

				/*
				 * if (((vchrInfo.getTransSts().equals("C")) &&
				 * (vchrInfo.getPymntSts().equals("C")) || vchrInfo.getTransSts().equals("C"))
				 * && (vchrInfo.getPymntPaySts().equals("A") ||
				 * vchrInfo.getPymntPaySts().equals("C"))) {
				 * 
				 * if (iPayMthd == 1) { starBrowseRes.output.fldTblDoc.add(vchrInfo); } else if
				 * (iPayMthd == 0) { vchrInfo.setHoldRsn(CommonConstants.PAY_MTHD_ERR +
				 * vchrInfo.getVchrVenId());
				 * 
				 * starBrowseRes.output.fldTblInvHold.add(vchrInfo); } }
				 * 
				 * if ((vchrInfo.getTransSts().equals("A")) &&
				 * (vchrInfo.getPymntSts().equals("A"))) {
				 * 
				 * vchrInfo.setHoldRsn(CommonConstants.VCHR_ACTV_ERR);
				 * starBrowseRes.output.fldTblInvHold.add(vchrInfo); }
				 * 
				 * if (vchrInfo.getTransSts().equals("H") || vchrInfo.getPymntSts().equals("H")
				 * || vchrInfo.getPymntPaySts().equals("H")) {
				 * 
				 * if(vchrInfo.getPymntPaySts().equals("H")) {
				 * vchrInfo.setHoldRsn(vchrInfo.getPymntPayStsRsn()); }
				 * starBrowseRes.output.fldTblInvHold.add(vchrInfo); }
				 */
			}

		} catch (Exception e) {

			logger.debug("Payment Info : {}", e.getMessage(), e);
			starBrowseRes.output.rtnSts = 1;
			starBrowseRes.output.messages.add(CommonConstants.SERVER_ERR);

			session.close();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	/* GET ALL THE DEFINE FIELDS LIST */
	public void readESPVouchers(BrowseResponse<CstmDocInfoBrowseOutput> starBrowseRes, String usrId, String cmpyId, String venIdFrm,
			String venIdTo, String payMthd, String invcDt, String invcDtTo, String dueDt, String dueDtTo, String discDt,
			String discDtTo, String disbNo) throws Exception {
		Session session = null;
		String hql = "";
		CstmDocInfoDAO docInfoDAO = new CstmDocInfoDAO();

		List<CstmParamInv> invsListPending = new ArrayList<CstmParamInv>();

		invsListPending = docInfoDAO.getPendingVouchersSearchESP("", "", usrId);

		List<String> voucherList = getVoucherList(invsListPending);
		try {
			session = SessionUtilInformixESP.getSession();

			
			List<PaymentMethod> paymentMethods = readVendorPaymentMethod();
			/*
			hql = "select trim(pyh_ven_id) as ven_id,trim(ven_ven_long_nm) as ven_long_nm, "
					+ "trim(pyh_ven_inv_no) as ven_inv_no, "
					+ "(select vch_lgn_id from aptvch_rec where vch_cmpy_id = pyh_cmpy_id and vch_vchr_pfx = pyh_opa_pfx "
					+ "and vch_vchr_no = pyh_opa_no), pyh_ven_inv_dt, pyh_due_dt, trim(pyh_po_pfx) as po_pfx, pyh_po_no,"
					+ "pyh_po_itm, trim(pyh_cry) as cry,trim(pyh_opa_pfx) as vchr_pfx, pyh_opa_no,trim(pyh_ap_brh) as vchr_brh, "
					+ "pyh_ent_dt, pyh_orig_amt, (select vch_vchr_cat from aptvch_rec where vch_cmpy_id = pyh_cmpy_id and "
					+ "vch_vchr_pfx = pyh_opa_pfx and vch_vchr_no = pyh_opa_no) vch_cat,'C' trn_sts,'C' pay_sts,'' trn_rsn,'' "
					+ "pay_rsn, trim(pyh_cmpy_id) as cmpy_id, round(pyh_disc_amt, 2) vch_disc_amt, pay.tsa_sts_actn, "
					+ "cast((select rsn_desc30 from scrrsn_rec where rsn_rsn_typ = pay.tsa_rsn_typ and rsn_rsn = pay.tsa_rsn) as varchar(30))"
					+ " rsn_desc,trn.tsa_sts_actn trn_sts_actn, "
					+ "cast((select rsn_desc30 from scrrsn_rec where rsn_rsn_typ = trn.tsa_rsn_typ and rsn_rsn = trn.tsa_rsn) as varchar(30))"
					+ " rsn_trn_desc, pyh_disc_dt disc_dt,"
					+ " (case when (pyh_disc_dt - date(now())) is null or (pyh_disc_dt - date(now()) < 0) then 'N' else 'Y' end) disc_sts,"
					+ " pyh_orig_amt - (case when (pyh_disc_dt - date(now())) is null or (pyh_disc_dt - date(now()) < 0) then 0 else pyh_disc_amt end) tot_amt"
					+ " from aptpyh_rec inner join aprven_rec on ven_cmpy_id = pyh_cmpy_id and "
					+ "ven_ven_id = pyh_ven_id inner join tcttsa_rec pay on pay.tsa_cmpy_id = pyh_cmpy_id "
					+ "and pay.tsa_ref_pfx = pyh_ap_pfx and pay.tsa_ref_no = pyh_ap_no and pay.tsa_ref_itm = 0 "
					+ "and pay.tsa_ref_sbitm = 0 and pay.tsa_sts_typ = 'N' inner join tcttsa_rec trn on trn.tsa_cmpy_id = pyh_cmpy_id "
					+ "and trn.tsa_ref_pfx = pyh_ap_pfx and trn.tsa_ref_no = pyh_ap_no and trn.tsa_ref_itm = 0 and trn.tsa_ref_sbitm = 0 "
					+ "and trn.tsa_sts_typ = 'T' where pyh_cmpy_id=:cmpy_id";
			*/
			

			//hql = " select cast(ven_ven_no as varchar(8)) ven_id,cast(ven_lkp_nm as varchar(15)) ven_nm from aprven_rec ";
			//hql = " select ven_ven_no ,trim(ven_lkp_nm) as ven_long_nm from aprven_rec where ";
			
			
			/*hql = " select ven_ven_no ,trim(ven_lkp_nm) as ven_long_nm,  "+
					"trim(pyh_ven_inv_no) as ven_inv_no, " + 
					"(select vch_lgn_id from aptvch_rec where vch_ref_pfx = pyh_ref_cd  " + 
					"and vch_vch_no = pyh_ref_no), " + 
					"TO_DATE(CAST(aptpyh_rec.pyh_ref_dt as VARCHAR(8)) ,'YYYYMMDD') refDt, " + 
					"TO_DATE(CAST(aptpyh_rec.pyh_due_dt as VARCHAR(8)) ,'YYYYMMDD') dueDt, " + 
					"ven_ven_no,  " + 
					"ven_ven_no , " + 
					"ven_ven_no , " + 
					"trim('') cry, "+
					"trim(aptpyh_rec.pyh_ref_cd)  as vchr_pfx,  " + 
					"aptpyh_rec.pyh_ref_no , " + 
					"trim(pyh_brh) as vchr_brh,  " + 
					"TO_DATE(CAST(pyh_ref_dt as VARCHAR(8)) ,'YYYYMMDD') reffDt,  " + 
					"pyh_amt_ap, " + 
					"ven_ven_no, " + 
					"ven_ven_no,ven_ven_no,ven_ven_no,ven_ven_no, " + 
					"ven_ven_no,  " + 
					"round(pyh_disc_amt_ap, 2) vch_disc_amt,  " + 
					"ven_ven_no,  " + 
					"ven_ven_no, " + 
					"ven_ven_no,  " + 
					"ven_ven_no,  " + 
					"TO_DATE(CAST(pyh_disc_dt as VARCHAR(8)) ,'YYYYMMDD') discDt, " + 
					"(case when (cast(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-',substring(cast (pyh_disc_dt as varchar(8)),5,2) ,'-',right(cast (pyh_disc_dt as varchar(8)),2))  as date) - date(now())) is null or (cast(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-',substring(cast (pyh_disc_dt as varchar(8)),5,2) ,'-',right(cast (pyh_disc_dt as varchar(8)),2))  as date) - date(now()) < 0) then 'N' else 'Y' end) disc_sts, " + 
					" pyh_amt_ap - (case when (cast(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-',substring(cast (pyh_disc_dt as varchar(8)),5,2) ,'-',right(cast (pyh_disc_dt as varchar(8)),2))  as date) - date(now())) is null or (cast(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-',substring(cast (pyh_disc_dt as varchar(8)),5,2) ,'-',right(cast (pyh_disc_dt as varchar(8)),2))  as date) - date(now()) < 0) then 0 else round(pyh_disc_amt_ap, 2) end) tot_amt " + 
					"from aptpyh_rec   "+ 
					"inner join aprven_rec on aprven_rec.ven_ven_no=aptpyh_rec.pyh_ven_no"
					+ " where pyh_disc_dt>9999999 AND pyh_ref_dt>9999999 AND pyh_due_dt>9999999 and pyh_disc_dt is not null AND pyh_ref_dt is not null AND pyh_due_dt is not null ";
			*/
			/*ESP query*/
			/*hql =	"select ven_ven_no ,trim(ven_lkp_nm) as ven_long_nm, "+  
					"trim(pyh_ven_inv_no) as ven_inv_no,  "+
					"(select vch_lgn_id from aptvch_rec where vch_ref_pfx = pyh_ref_cd "+  
					"and vch_vch_no = pyh_ref_no),  "+
					"cast(concat(concat(concat(left(cast (pyh_ref_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_ref_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_ref_dt as varchar(8)),2))as date) refDt, "+  
					"cast(concat(concat(concat(left(cast (pyh_due_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_due_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_due_dt as varchar(8)),2))as date) dueDt, "+
					"ven_ven_no, "+  
					"ven_ven_no , "+ 
					"ven_ven_no ,  "+
					"trim('') cry,  "+
					"trim(aptpyh_rec.pyh_ref_cd)  as vchr_pfx, "+  
					"aptpyh_rec.pyh_ref_no ,  "+
					"trim(pyh_brh) as vchr_brh, "+  
					"cast(concat(concat(concat(left(cast (pyh_ref_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_ref_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_ref_dt as varchar(8)),2))as date) reffDt, "+  
					"pyh_amt_ap, "+ 
					"ven_ven_no,  "+
					"ven_ven_no,ven_ven_no,ven_ven_no,ven_ven_no, "+ 
					"ven_ven_no,   "+
					"round(pyh_disc_amt_ap, 2) vch_disc_amt, "+  
					"ven_ven_no,   "+
					"ven_ven_no,  "+
					"ven_ven_no,   "+
					"ven_ven_no,   "+
					"cast(concat(concat(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_disc_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_disc_dt as varchar(8)),2))as date) discDt, "+ 
					"(case when (cast(concat(concat(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-'), concat(substr(cast (pyh_disc_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_disc_dt as varchar(8)),2))as date) - date(sysdate)) is null "+  
					"or "+ 
					"(cast(concat(concat(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_disc_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_disc_dt as varchar(8)),2))as date) - date(sysdate) < 0) "+ 
					"then 'N' else 'Y' end) disc_sts, "+ 
					"pyh_amt_ap - (case when (cast(concat(concat(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-'), concat(substr(cast (pyh_disc_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_disc_dt as varchar(8)),2))as date) - date(sysdate)) is null "+  
					"or "+ 
					"(cast(concat(concat(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_disc_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_disc_dt as varchar(8)),2))as date) - date(sysdate) < 0) "+ 
					"then 0 else round(pyh_disc_amt_ap, 2) end) tot_amt,  "+
					" (select djh_chq_no from apjdjh_rec where apjdjh_rec.djh_brh=apjdjo_rec.djo_ssn_brh and apjdjh_rec.djh_djh_no= apjdjo_rec.djo_djh_no and aptpyh_rec.pyh_ref_no=apjdjo_rec.djo_ref_no) chkno "+
					"from aptpyh_rec    "+
					"inner join aprven_rec on aprven_rec.ven_ven_no=aptpyh_rec.pyh_ven_no "+
					"inner join apjdjo_rec on aptpyh_rec.pyh_ref_no=apjdjo_rec.djo_ref_no "+
					"where pyh_disc_dt>9999999 AND pyh_ref_dt>9999999 AND pyh_due_dt>9999999 and pyh_disc_dt is not null AND pyh_ref_dt is not null AND pyh_due_dt is not null "; 
			*/
			hql = 	"select ven_ven_no ,trim(ven_lkp_nm) as ven_long_nm, "+
					"trim(pyh_ven_inv_no) as ven_inv_no, "+ 
					"(select vch_lgn_id from aptvch_rec where vch_ref_pfx = pyh_ref_cd "+
					"and vch_vch_no = pyh_ref_no),  "+
					"cast(concat(concat(concat(left(cast (pyh_ref_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_ref_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_ref_dt as varchar(8)),2))as date) refDt,"+ 
					"cast(concat(concat(concat(left(cast (pyh_due_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_due_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_due_dt as varchar(8)),2))as date) dueDt, "+
					"ven_ven_no, "+
					"ven_ven_no , "+
					"ven_ven_no ,  "+
					"trim(ven_cry) cry,  "+
					"trim(aptpyh_rec.pyh_ref_cd)  as vchr_pfx, "+
					"aptpyh_rec.pyh_ref_no ,  "+
					"trim(pyh_brh) as vchr_brh, "+
					"cast(concat(concat(concat(left(cast (pyh_ref_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_ref_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_ref_dt as varchar(8)),2))as date) reffDt,"+ 
					"pyh_amt_ap, "+				
					"ven_ven_no,  "+
					"ven_ven_no,ven_ven_no,ven_ven_no,ven_ven_no, "+
					"ven_ven_no,   "+
					"round(pyh_disc_amt_ap, 2) vch_disc_amt, "+
					"ven_ven_no,   "+
					"ven_ven_no,  "+
					"ven_ven_no,   "+
					"ven_ven_no,   "+
					"cast(concat(concat(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_disc_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_disc_dt as varchar(8)),2))as date) discDt,"+ 
					"(case when (cast(concat(concat(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-'), concat(substr(cast (pyh_disc_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_disc_dt as varchar(8)),2))as date) - date(sysdate)) is null "+ 
					"or "+
					"(cast(concat(concat(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_disc_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_disc_dt as varchar(8)),2))as date) - date(sysdate) < 0) "+ 
					"then 'N' else 'Y' end) disc_sts, "+
					"pyh_amt_ap - (case when (cast(concat(concat(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-'), concat(substr(cast (pyh_disc_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_disc_dt as varchar(8)),2))as date) - date(sysdate)) is null "+ 
					"or "+
					"(cast(concat(concat(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_disc_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_disc_dt as varchar(8)),2))as date) - date(sysdate) < 0) "+ 
					"then 0 else round(pyh_disc_amt_ap, 2) end) tot_amt,  djh_chq_no,"+
					"cast(concat(concat(concat(left(cast (djh_jnl_dt as varchar(8)),4),'-'),  concat(substr(cast (djh_jnl_dt as varchar(8)),5,2) ,'-')),right(cast (djh_jnl_dt as varchar(8)),2))as date) jnlDt,"+
					"cast(concat(concat(concat(left(cast (djh_issue_dt as varchar(8)),4),'-'),  concat(substr(cast (djh_issue_dt as varchar(8)),5,2) ,'-')),right(cast (djh_issue_dt as varchar(8)),2))as date) issuDt, djo_djh_no,djh_chq_net_amt  "+
					"from apjdjo_rec "+
					"inner join aptpyh_rec on aptpyh_rec.pyh_ref_no=apjdjo_rec.djo_ref_no  "+
					"inner join aprven_rec on aprven_rec.ven_ven_no=aptpyh_rec.pyh_ven_no "+
					"inner join apjdjh_rec on djo_ssn_brh=djh_brh and djo_djh_no=djh_djh_no and djh_trs_type='V'  "+
					"where  pyh_disc_dt is not null AND pyh_ref_dt is not null AND pyh_due_dt is not null ";
			
			if (venIdFrm.length() > 0) {
				hql = hql + " and ven_ven_no >= :ven_id_frm";
			}

			if (venIdTo.length() > 0) {
				hql = hql + " and ven_ven_no <= :ven_id_to";
			}
		
			if(disbNo.length() > 0)
			{
				/*hql = hql + " and pyh_ref_cd||pyh_ref_no in "
						+ "(select dso_ref_cd||dso_ref_no from aptdso_rec where "
						+ "dso_dsd_no=:disb_no) ";*/
				hql = hql + " and djo_djh_no=:disb_no ";
			}
			
			
			if (CommonConstants.DB_TYP.equals("POS")) {

				if (invcDt.length() > 0) {
					hql = hql + " and aptpyh_rec.pyh_ref_dt >= to_date(:invc_dt, 'mm/dd/yyyy')";
				}

				if (invcDtTo.length() > 0) {
					hql = hql + " and aptpyh_rec.pyh_ref_dt <= to_date(:invc_dt_to, 'mm/dd/yyyy')";
				}

				if (dueDt.length() > 0) {
					hql = hql + " and aptpyh_rec.pyh_due_dt >= to_date(:due_dt, 'mm/dd/yyyy')";
				}

				if (dueDtTo.length() > 0) {
					hql = hql + " and aptpyh_rec.pyh_due_dt <= to_date(:due_dt_to, 'mm/dd/yyyy')";
				}

				if (discDt.length() > 0) {
					hql = hql + " and pyh_disc_dt >= to_date(:disc_dt, 'mm/dd/yyyy')";
				}

				if (discDtTo.length() > 0) {
					hql = hql + " and pyh_disc_dt <= to_date(:disc_dt_to, 'mm/dd/yyyy')";
				}

			} else if (CommonConstants.DB_TYP.equals("INF")) {

				if (invcDt.length() > 0) {
					//hql = hql + " and aptpyh_rec.pyh_ref_dt >= to_date(:invc_dt, '%m/%d/%Y')";
					hql = hql + " and cast(concat(concat(concat(left(cast (pyh_ref_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_ref_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_ref_dt as varchar(8)),2))as date)>=to_date(:invc_dt, '%m/%d/%Y')";
				}

				if (invcDtTo.length() > 0) {
					//hql = hql + " and aptpyh_rec.pyh_ref_dt <= to_date(:invc_dt_to, '%m/%d/%Y')";
					hql = hql + " and cast(concat(concat(concat(left(cast (pyh_ref_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_ref_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_ref_dt as varchar(8)),2))as date)<=to_date(:invc_dt_to, '%m/%d/%Y')";
				}

				if (dueDt.length() > 0) {
					//hql = hql + " and aptpyh_rec.pyh_due_dt >= to_date(:due_dt, '%m/%d/%Y')";
					hql = hql + " and cast(concat(concat(concat(left(cast (pyh_due_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_due_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_due_dt as varchar(8)),2))as date) >= to_date(:due_dt, '%m/%d/%Y')";
				}

				if (dueDtTo.length() > 0) {
					//hql = hql + " and aptpyh_rec.pyh_due_dt <= to_date(:due_dt_to, '%m/%d/%Y')";
					hql = hql + " and cast(concat(concat(concat(left(cast (pyh_due_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_due_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_due_dt as varchar(8)),2))as date) <= to_date(:due_dt_to, '%m/%d/%Y')";
				}

				if (discDt.length() > 0) {
					//hql = hql + " and pyh_disc_dt >= to_date(:disc_dt, '%m/%d/%Y')";
					hql = hql + " and cast(concat(concat(concat(left(cast (djh_issue_dt as varchar(8)),4),'-'),  concat(substr(cast (djh_issue_dt as varchar(8)),5,2) ,'-')),right(cast (djh_issue_dt as varchar(8)),2))as date) >= to_date(:disc_dt, '%m/%d/%Y')";
				}

				if (discDtTo.length() > 0) {
					//hql = hql + " and pyh_disc_dt <= to_date(:disc_dt_to, '%m/%d/%Y')";
					hql = hql + " and cast(concat(concat(concat(left(cast (djh_issue_dt as varchar(8)),4),'-'),  concat(substr(cast (djh_issue_dt as varchar(8)),5,2) ,'-')),right(cast (djh_issue_dt as varchar(8)),2))as date) <= to_date(:disc_dt_to, '%m/%d/%Y')";
				}
			}

			/*if(voucherList.size()>0)
			{
				hql = hql + " and aptpyh_rec.pyh_ref_cd || '-' || aptpyh_rec.pyh_ref_no not in (:vchrList) order by djh_chq_no,pyh_ref_no, ven_ven_no";
			}
			else*/
				hql = hql + "  order by djh_chq_no, pyh_ref_no, ven_ven_no ";
			System.out.println(hql);
			// TODO trn_sts is not NULL
			
			Query queryValidate = session.createSQLQuery(hql);
			
			if (venIdFrm.length() > 0) {
				queryValidate.setParameter("ven_id_frm", Integer.parseInt(venIdFrm));
			}

			if (venIdTo.length() > 0) {
				queryValidate.setParameter("ven_id_to", Integer.parseInt(venIdTo));
			}
			
			if (invcDt.length() > 0) {
				queryValidate.setParameter("invc_dt", invcDt);
			}

			if (dueDt.length() > 0) {
				queryValidate.setParameter("due_dt", dueDt);
			}

			if (invcDtTo.length() > 0) {
				queryValidate.setParameter("invc_dt_to", invcDtTo);
			}

			if (dueDtTo.length() > 0) {
				queryValidate.setParameter("due_dt_to", dueDtTo);
			}

			if (discDt.length() > 0) {
				queryValidate.setParameter("disc_dt", discDt);
			}
			
			if (discDtTo.length() > 0) {
				queryValidate.setParameter("disc_dt_to", discDtTo);
			}
			
			if(disbNo.length() > 0)
			{
				queryValidate.setParameter("disb_no", Integer.parseInt(disbNo));
			}
			
			//queryValidate.setParameter("cmpy_id", cmpyId);
			
			/*List<String> items = Arrays.asList(voucherList.split("\\s*,\\s*"));*/

			/*if(voucherList.size()>0)
			{
				System.out.println("***********"+voucherList.size());
				queryValidate.setParameterList("vchrList", voucherList);
			}*/

			List<Object[]> listCstmDocVal = queryValidate.list();
			CommonFunctions objCom = new CommonFunctions();
			for (Object[] cstmDocVal : listCstmDocVal) {

				VchrInfo vchrInfo = new VchrInfo();

				vchrInfo.setVchrVenId(String.valueOf(cstmDocVal[0]));
				vchrInfo.setVchrVenNm(String.valueOf(cstmDocVal[1]));
				
				vchrInfo.setVchrInvNo(String.valueOf(cstmDocVal[2]));

				if (cstmDocVal[3] != null) {
					vchrInfo.setUpldBy(String.valueOf(cstmDocVal[3]));
				} else {
					vchrInfo.setUpldBy("");
				}

				if (cstmDocVal[4] != null) {
					vchrInfo.setVchrInvDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[4])));
					vchrInfo.setVchrInvDt((Date) (cstmDocVal[4]));
				}
				if (cstmDocVal[5] != null) {
					vchrInfo.setVchrDueDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[5])));
					vchrInfo.setVchrDueDt((Date) (cstmDocVal[5]));
				}
				vchrInfo.setPoPfx(String.valueOf(cstmDocVal[6]));
				vchrInfo.setPoNo(String.valueOf(cstmDocVal[7]));
				vchrInfo.setPoItm(String.valueOf(cstmDocVal[8]));
				vchrInfo.setVchrCry(String.valueOf(cstmDocVal[9]));
				vchrInfo.setVchrPfx(String.valueOf(cstmDocVal[10]));
				vchrInfo.setVchrNo(String.valueOf(cstmDocVal[11]));
				vchrInfo.setVchrBrh(String.valueOf(cstmDocVal[12]));
				if (cstmDocVal[30] != null) {	//13
					vchrInfo.setCrtDttsStr((objCom.formatDateWithoutTime((Date) cstmDocVal[30])));
					vchrInfo.setCrtDtts((Date) (cstmDocVal[30]));
				} else {
					vchrInfo.setCrtDttsStr("");
				}
				vchrInfo.setVchrAmt(Double.parseDouble(String.valueOf(cstmDocVal[14])));
				vchrInfo.setVchrAmtStr(objCom.formatAmount(Double.parseDouble(String.valueOf(cstmDocVal[14]))));

				if (cstmDocVal[15] != null) {
					vchrInfo.setVchrCat(String.valueOf(cstmDocVal[15]));
				} else {
					vchrInfo.setVchrCat("");
				}

				if (cstmDocVal[16] != null) {
					vchrInfo.setTransSts(String.valueOf(cstmDocVal[16]));
				} else {
					vchrInfo.setTransSts("");
				}

				if (cstmDocVal[17] != null) {
					vchrInfo.setPymntSts(String.valueOf(cstmDocVal[17]));
				} else {
					vchrInfo.setPymntSts("");
				}

				if (cstmDocVal[18] != null) {
					vchrInfo.setTransStsRsn(String.valueOf(cstmDocVal[18]));
				} else {
					vchrInfo.setTransStsRsn("");
				}

				if (cstmDocVal[19] != null) {
					vchrInfo.setPymntStsRsn(String.valueOf(cstmDocVal[19]));
				} else {
					vchrInfo.setPymntStsRsn("");
				}

				vchrInfo.setCmpyId(cstmDocVal[20].toString());
				vchrInfo.setDiscAmt(Double.parseDouble(String.valueOf(cstmDocVal[21])));
				vchrInfo.setDiscAmtStr(objCom.formatAmount(Double.parseDouble(String.valueOf(cstmDocVal[21]))));

				if (cstmDocVal[22] != null) {
					vchrInfo.setPymntPaySts(cstmDocVal[22].toString());
				} else {
					vchrInfo.setPymntPaySts("");
				}

				if (cstmDocVal[23] != null) {
					vchrInfo.setPymntPayStsRsn(cstmDocVal[23].toString());
				} else {
					vchrInfo.setPymntPayStsRsn("");
				}

				if (cstmDocVal[24] != null) {
					vchrInfo.setTransPaySts(cstmDocVal[24].toString());
				} else {
					vchrInfo.setTransPaySts("");
				}

				if (cstmDocVal[25] != null) {
					vchrInfo.setTransPayStsRsn(cstmDocVal[25].toString());
				} else {
					vchrInfo.setTransPayStsRsn("");
				}

				if (cstmDocVal[31] != null) {	//26
					vchrInfo.setVchrDiscDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[31])));
					vchrInfo.setVchrDiscDt((Date) (cstmDocVal[31]));
				}
				else
				{
					vchrInfo.setVchrDiscDtStr("");
				}
				
				vchrInfo.setDiscFlg(cstmDocVal[27].toString());
				
				vchrInfo.setTotAmt(Double.parseDouble(String.valueOf(cstmDocVal[28])));
				vchrInfo.setTotAmtStr(objCom.formatAmount(Double.parseDouble(String.valueOf(cstmDocVal[28]))));
				
				if (cstmDocVal[29] != null) 
					vchrInfo.setChkNo(String.valueOf(cstmDocVal[29]));
				else
					vchrInfo.setChkNo("");
				
				vchrInfo.setDisbNo(cstmDocVal[32].toString());
				vchrInfo.setChkNetAmt(Double.parseDouble(String.valueOf(cstmDocVal[33])));
				/* PAYMENT METHOD LIST AGAINST VENDOR */
				
				List<PaymentMethod> methods = new ArrayList<PaymentMethod>();

				int iPayMthd = 0;
				/*
				for (int i = 0; i < paymentMethods.size(); i++) {
					if (paymentMethods.get(i).getCmpyId().equals(vchrInfo.getCmpyId())
							&& paymentMethods.get(i).getVenId().equals(vchrInfo.getVchrVenId())) {
						methods.add(paymentMethods.get(i));
					}

					if (payMthd.length() > 0) {
						List<String> payMthdList = Arrays.asList(payMthd.split("\\s*,\\s*"));

						if (payMthdList.contains(paymentMethods.get(i).getMthdId())
								&& paymentMethods.get(i).getVenId().equals(vchrInfo.getVchrVenId())
								&& paymentMethods.get(i).getCmpyId().equals(vchrInfo.getCmpyId())) {
							iPayMthd = 1;
						}
					}
				}
				*/
				for (int i = 0; i < paymentMethods.size(); i++) {
					if (paymentMethods.get(i).getVenId().equals(vchrInfo.getVchrVenId())) {
						methods.add(paymentMethods.get(i));
					}

					if (payMthd.length() > 0) {
						List<String> payMthdList = Arrays.asList(payMthd.split("\\s*,\\s*"));

						if (payMthdList.contains(paymentMethods.get(i).getMthdId())
								&& paymentMethods.get(i).getVenId().equals(vchrInfo.getVchrVenId())
								) {
							iPayMthd = 1;
						}
					}
				}

				vchrInfo.setPayMthds(methods);

				// || (vchrInfo.getTransSts().equals("H") || vchrInfo.getPymntSts().equals("H"))

				if (vchrInfo.getPymntPaySts().equals("C") || vchrInfo.getTransPaySts().equals("C")) {

					if (iPayMthd == 1) {
						starBrowseRes.output.fldTblDoc.add(vchrInfo);
					} else if (iPayMthd == 0) {
						vchrInfo.setHoldRsn(CommonConstants.PAY_MTHD_ERR + vchrInfo.getVchrVenId());

						starBrowseRes.output.fldTblInvHold.add(vchrInfo);
					}
				}
				
				if (vchrInfo.getPymntPaySts().equals("A") && vchrInfo.getTransPaySts().equals("A")) {
					
						vchrInfo.setHoldRsn(CommonConstants.VCHR_ACTV_ERR);

						starBrowseRes.output.fldTblInvHold.add(vchrInfo);
				}

				if (vchrInfo.getTransPaySts().equals("H") || vchrInfo.getPymntPaySts().equals("H")) {

					if (vchrInfo.getPymntPaySts().equals("H")) {
						vchrInfo.setHoldRsn(vchrInfo.getPymntPayStsRsn());
					}
					
					if (vchrInfo.getTransPaySts().equals("H")) {
						vchrInfo.setHoldRsn(vchrInfo.getTransPayStsRsn());
					}
					starBrowseRes.output.fldTblInvHold.add(vchrInfo);
				}
				
				starBrowseRes.output.fldTblDoc.add(vchrInfo);
				
			}

		} catch (Exception e) {

			logger.debug("Payment Info : {}", e.getMessage(), e);
			starBrowseRes.output.rtnSts = 1;
			starBrowseRes.output.messages.add(CommonConstants.SERVER_ERR);

			session.close();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	
	public void readSpecificVoucher(String vchrNo, VchrInfo cstmdocInfo) {
		Session session = null;
		String hql = "";

		CstmDocInfoDAO cstmDocInfoDAO = new CstmDocInfoDAO();

		try {
			session = SessionUtilInformix.getSession();

			/*
			 * hql =
			 * "SELECT trim(vch_ven_id) as ven_id, trim(ven_ven_long_nm) as ven_long_nm, trim(vch_ven_inv_no) as ven_inv_no,"
			 * +
			 * " trim(vch_lgn_id) as lgn_id, vch_ven_inv_dt, vch_due_dt, trim(vch_po_pfx) as po_pfx, vch_po_no, vch_po_itm,"
			 * +
			 * " trim(vch_cry) as cry, trim(vch_vchr_pfx) as vchr_pfx, vch_vchr_no, trim(vch_vchr_brh) as vchr_brh,"
			 * +
			 * " vch_ent_dt, vch_vchr_amt, trim(vct_vchr_cat) as vchr_cat, (select tsa_sts_actn from tcttsa_rec"
			 * +
			 * " where tsa_cmpy_id=vch_cmpy_id  and tsa_ref_pfx=vch_vchr_pfx and tsa_ref_no=vch_vchr_no and tsa_ref_itm=0 and"
			 * +
			 * " tsa_ref_sbitm=0 and tsa_sts_typ='T'), (select tsa_sts_actn from tcttsa_rec where tsa_cmpy_id=vch_cmpy_id and"
			 * +
			 * " tsa_ref_pfx=vch_vchr_pfx and tsa_ref_no=vch_vchr_no and tsa_ref_itm=0  and tsa_ref_sbitm=0 and tsa_sts_typ='N'), "
			 * +
			 * "cast(vch_cmpy_id as varchar(3)) cmpy_id, round(vch_disc_amt,2) vch_disc_amt"
			 * +
			 * " FROM aptvch_rec, aprven_rec, aprvct_rec  WHERE 1=1 and ven_cmpy_id = vch_cmpy_id and ven_ven_id = vch_ven_id and"
			 * + " vct_vchr_cat = vch_vchr_cat";
			 * 
			 * hql = hql + " and vch_vchr_pfx ||'-'||vch_vchr_no = :vchr";
			 */

			hql = "select trim(pyh_ven_id) as ven_id, trim(ven_ven_long_nm) as ven_long_nm, trim(pyh_ven_inv_no) as ven_inv_no, "
					+ "( select vch_lgn_id from aptvch_rec where vch_cmpy_id = pyh_cmpy_id and vch_vchr_pfx = pyh_opa_pfx "
					+ "and vch_vchr_no = pyh_opa_no), pyh_ven_inv_dt, pyh_due_dt, trim(pyh_po_pfx) as po_pfx, pyh_po_no, pyh_po_itm, "
					+ "trim(pyh_cry) as cry, trim(pyh_opa_pfx) as vchr_pfx, pyh_opa_no, trim(pyh_ap_brh) as vchr_brh, pyh_ent_dt, "
					+ "pyh_orig_amt,  ( select vch_vchr_cat from aptvch_rec where vch_cmpy_id = pyh_cmpy_id and vch_vchr_pfx = pyh_opa_pfx "
					+ "and vch_vchr_no = pyh_opa_no) vch_cat, 'C' trn_sts, 'C' pay_sts, trim(pyh_cmpy_id) as cmpy_id, "
					+ "round(pyh_disc_amt, 2) vch_disc_amt, pyh_disc_dt, "
					+ " pyh_orig_amt - (case when (pyh_disc_dt - date(now())) is null or (pyh_disc_dt - date(now()) < 0) then 0 else pyh_disc_amt end) tot_amt"
					+ " from aptpyh_rec inner join aprven_rec on ven_cmpy_id = pyh_cmpy_id "
					+ "and ven_ven_id = pyh_ven_id inner join tcttsa_rec on tsa_cmpy_id = pyh_cmpy_id and tsa_ref_pfx = pyh_ap_pfx "
					+ "and tsa_ref_no = pyh_ap_no and tsa_ref_itm = 0 and tsa_ref_sbitm = 0 and tsa_sts_typ = 'N' ";

			hql = hql + " and pyh_opa_pfx ||'-'||pyh_opa_no = :vchr";

			// TODO trn_sts is not NULL
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("vchr", vchrNo);

			List<Object[]> listCstmDocVal = queryValidate.list();
			CommonFunctions objCom = new CommonFunctions();
			for (Object[] cstmDocVal : listCstmDocVal) {

				cstmdocInfo.setVchrVenId(String.valueOf(cstmDocVal[0]));
				cstmdocInfo.setVchrVenNm(String.valueOf(cstmDocVal[1]));
				cstmdocInfo.setVchrInvNo(String.valueOf(cstmDocVal[2]));
				cstmdocInfo.setUpldBy(String.valueOf(cstmDocVal[3]));
				if (cstmDocVal[4] != null) {
					cstmdocInfo.setVchrInvDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[4])));
					cstmdocInfo.setVchrInvDt((Date) (cstmDocVal[4]));
				}
				if (cstmDocVal[5] != null) {
					cstmdocInfo.setVchrDueDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[5])));
					cstmdocInfo.setVchrDueDt((Date) (cstmDocVal[5]));
				}
				cstmdocInfo.setPoPfx(String.valueOf(cstmDocVal[6]));
				cstmdocInfo.setPoNo(String.valueOf(cstmDocVal[7]));
				cstmdocInfo.setPoItm(String.valueOf(cstmDocVal[8]));
				cstmdocInfo.setVchrCry(String.valueOf(cstmDocVal[9]));
				cstmdocInfo.setVchrPfx(String.valueOf(cstmDocVal[10]));
				cstmdocInfo.setVchrNo(String.valueOf(cstmDocVal[11]));
				cstmdocInfo.setVchrBrh(String.valueOf(cstmDocVal[12]));
				if (cstmDocVal[13] != null) {
					cstmdocInfo.setCrtDttsStr((objCom.formatDateWithoutTime((Date) cstmDocVal[13])));
					cstmdocInfo.setCrtDtts((Date) (cstmDocVal[13]));
				}
				else
				{
					cstmdocInfo.setCrtDttsStr("");
				}
				cstmdocInfo.setVchrAmt(Double.parseDouble(String.valueOf(cstmDocVal[14])));

				cstmdocInfo.setVchrAmtStr(objCom.formatAmount(Double.parseDouble(cstmDocVal[14].toString())));

				cstmdocInfo.setVchrCat(String.valueOf(cstmDocVal[15]));
				cstmdocInfo.setTransSts(String.valueOf(cstmDocVal[16]));
				cstmdocInfo.setPymntSts(String.valueOf(cstmDocVal[17]));
				cstmdocInfo.setCmpyId(String.valueOf(cstmDocVal[18]));

				/* DISCOUNT AMOUNT */
				
				cstmdocInfo.setDiscAmtStr(objCom.formatAmount(Double.parseDouble(String.valueOf(cstmDocVal[19]))));

				if (String.valueOf(cstmDocVal[10]).length() > 0 && String.valueOf(cstmDocVal[11]).length() > 0) {
					cstmDocInfoDAO.getFileNameByVchrNo(cstmdocInfo, String.valueOf(cstmDocVal[10]),
							String.valueOf(cstmDocVal[11]));
				}
				
				if (cstmDocVal[20] != null) {
					cstmdocInfo.setVchrDiscDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[20])));
					cstmdocInfo.setVchrDiscDt((Date) (cstmDocVal[20]));
				}
				else
				{
					cstmdocInfo.setVchrDiscDtStr("");
				}
				
				cstmdocInfo.setTotAmt(Double.parseDouble(String.valueOf(cstmDocVal[21])));

				cstmdocInfo.setTotAmtStr(objCom.formatAmount(Double.parseDouble(cstmDocVal[21].toString())));

				GLInfoDAO glInfoDAO = new GLInfoDAO();
				JournalInfo glRef = glInfoDAO
						.getJournalReference(cstmdocInfo.getVchrPfx() + "-" + cstmdocInfo.getVchrNo());

				if (glRef != null) {
					cstmdocInfo.setGlEntry(glRef.getJeEntry());
					cstmdocInfo.setGlEntryDt(glRef.getPostDt());
				} else {
					cstmdocInfo.setGlEntry("");
					cstmdocInfo.setGlEntryDt("");
				}

			}

		} catch (Exception e) {

			logger.debug("Payment Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}

	public void readSpecificVoucherESP(String vchrNo, VchrInfo cstmdocInfo,PaymentInfoOutput params, String issueDate) {
		Session session = null;
		String hql = "";

		CstmDocInfoDAO cstmDocInfoDAO = new CstmDocInfoDAO();

		try {
			session = SessionUtilInformixESP.getSession();

			/*
			 * hql =
			 * "SELECT trim(vch_ven_id) as ven_id, trim(ven_ven_long_nm) as ven_long_nm, trim(vch_ven_inv_no) as ven_inv_no,"
			 * +
			 * " trim(vch_lgn_id) as lgn_id, vch_ven_inv_dt, vch_due_dt, trim(vch_po_pfx) as po_pfx, vch_po_no, vch_po_itm,"
			 * +
			 * " trim(vch_cry) as cry, trim(vch_vchr_pfx) as vchr_pfx, vch_vchr_no, trim(vch_vchr_brh) as vchr_brh,"
			 * +
			 * " vch_ent_dt, vch_vchr_amt, trim(vct_vchr_cat) as vchr_cat, (select tsa_sts_actn from tcttsa_rec"
			 * +
			 * " where tsa_cmpy_id=vch_cmpy_id  and tsa_ref_pfx=vch_vchr_pfx and tsa_ref_no=vch_vchr_no and tsa_ref_itm=0 and"
			 * +
			 * " tsa_ref_sbitm=0 and tsa_sts_typ='T'), (select tsa_sts_actn from tcttsa_rec where tsa_cmpy_id=vch_cmpy_id and"
			 * +
			 * " tsa_ref_pfx=vch_vchr_pfx and tsa_ref_no=vch_vchr_no and tsa_ref_itm=0  and tsa_ref_sbitm=0 and tsa_sts_typ='N'), "
			 * +
			 * "cast(vch_cmpy_id as varchar(3)) cmpy_id, round(vch_disc_amt,2) vch_disc_amt"
			 * +
			 * " FROM aptvch_rec, aprven_rec, aprvct_rec  WHERE 1=1 and ven_cmpy_id = vch_cmpy_id and ven_ven_id = vch_ven_id and"
			 * + " vct_vchr_cat = vch_vchr_cat";
			 * 
			 * hql = hql + " and vch_vchr_pfx ||'-'||vch_vchr_no = :vchr";
			 */

			hql = 	" select ven_ven_no , trim(ven_lkp_nm) as ven_long_nm,"+
					"trim(pyh_ven_inv_no) as ven_inv_no, " + 
					"(select vch_lgn_id from aptvch_rec where vch_ref_pfx = pyh_ref_cd  " + 
					"and vch_vch_no = pyh_ref_no), " + 
					"TO_DATE(CAST(aptpyh_rec.pyh_ref_dt as VARCHAR(8)) ,'YYYYMMDD') refDt, " + 
					"TO_DATE(CAST(aptpyh_rec.pyh_due_dt as VARCHAR(8)) ,'YYYYMMDD') dueDt, " + 
					"ven_ven_no,ven_ven_no,ven_ven_no,trim('') cry,"+
					"trim(aptpyh_rec.pyh_ref_cd)  as vchr_pfx,  " + 
					"aptpyh_rec.pyh_ref_no , " + 
					"trim(pyh_brh) as vchr_brh,  " + 
					"TO_DATE(CAST(pyh_ref_dt as VARCHAR(8)) ,'YYYYMMDD') reffDt,  " + 
					"pyh_amt_ap,  ven_ven_no, ven_ven_no, ven_ven_no, ven_ven_no, "+
					"round(pyh_disc_amt_ap, 2) vch_disc_amt, TO_DATE(CAST(pyh_disc_dt as VARCHAR(8)) ,'YYYYMMDD') discDt, "+
					//" pyh_amt_ap - round(pyh_disc_amt_ap, 2) "+
					" pyh_amt_ap - (case when (cast(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-',substring(cast (pyh_disc_dt as varchar(8)),5,2) ,'-',right(cast (pyh_disc_dt as varchar(8)),2))  as date) - date(now())) is null or (cast(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-',substring(cast (pyh_disc_dt as varchar(8)),5,2) ,'-',right(cast (pyh_disc_dt as varchar(8)),2))  as date) - date(now()) < 0) then 0 else round(pyh_disc_amt_ap, 2) end) tot_amt " +
					"from aptpyh_rec   " + 
					"inner join aprven_rec on aprven_rec.ven_ven_no=aptpyh_rec.pyh_ven_no"+
					" where pyh_disc_dt>9999999 AND pyh_ref_dt>9999999 AND pyh_due_dt>9999999 and pyh_disc_dt is not null AND pyh_ref_dt is not null AND pyh_due_dt is not null ";
			/*ESP Query*/
			hql =	"select ven_ven_no , trim(ven_lkp_nm) as ven_long_nm, "+
					"trim(pyh_ven_inv_no) as ven_inv_no,  "+
					"(select vch_lgn_id from aptvch_rec where vch_ref_pfx = pyh_ref_cd "+  
					"and vch_vch_no = pyh_ref_no),  "+
					"cast(concat(concat(concat(left(cast (pyh_ref_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_ref_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_ref_dt as varchar(8)),2))as date) refDt, "+  
					"cast(concat(concat(concat(left(cast (pyh_due_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_due_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_due_dt as varchar(8)),2))as date) dueDt, "+
					"ven_ven_no,ven_ven_no,ven_ven_no,trim('') cry, "+
					"trim(aptpyh_rec.pyh_ref_cd)  as vchr_pfx,   "+
					"aptpyh_rec.pyh_ref_no ,  "+
					"trim(pyh_brh) as vchr_brh, "+  
					"cast(concat(concat(concat(left(cast (pyh_ref_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_ref_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_ref_dt as varchar(8)),2))as date) reffDt, "+  
					"pyh_amt_ap,  ven_ven_no, ven_ven_no, ven_ven_no, ven_ven_no,  "+
					"round(pyh_disc_amt_ap, 2) vch_disc_amt,  "+
					"cast(concat(concat(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_disc_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_disc_dt as varchar(8)),2))as date) discDt, "+ 
					"pyh_amt_ap - (case when (cast(concat(concat(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-'), concat(substr(cast (pyh_disc_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_disc_dt as varchar(8)),2))as date) - date(sysdate)) is null "+  
					"or  "+
					"(cast(concat(concat(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_disc_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_disc_dt as varchar(8)),2))as date) - date(sysdate) < 0) "+ 
					"then 0 else round(pyh_disc_amt_ap, 2) end) tot_amt   "+
					"from aptpyh_rec "+   
					"inner join aprven_rec on aprven_rec.ven_ven_no=aptpyh_rec.pyh_ven_no "+
					//"where pyh_disc_dt>9999999 AND pyh_ref_dt>9999999 AND pyh_due_dt>9999999 and pyh_disc_dt is not null AND pyh_ref_dt is not null AND pyh_due_dt is not null "; 
					"where pyh_disc_dt is not null AND pyh_ref_dt is not null AND pyh_due_dt is not null ";
			
			/*ESP Query*/
			hql =	"select ven_ven_no , trim(ven_lkp_nm) as ven_long_nm, "+
					"trim(pyh_ven_inv_no) as ven_inv_no,  "+
					"(select vch_lgn_id from aptvch_rec where vch_ref_pfx = pyh_ref_cd "+  
					"and vch_vch_no = pyh_ref_no),  "+
					"cast(concat(concat(concat(left(cast (pyh_ref_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_ref_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_ref_dt as varchar(8)),2))as date) refDt, "+  
					"cast(concat(concat(concat(left(cast (pyh_due_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_due_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_due_dt as varchar(8)),2))as date) dueDt, "+
					"ven_ven_no,ven_ven_no,ven_ven_no,trim('') cry, "+
					"trim(aptpyh_rec.pyh_ref_cd)  as vchr_pfx,   "+
					"aptpyh_rec.pyh_ref_no ,  "+
					"trim(pyh_brh) as vchr_brh, "+  
					"cast(concat(concat(concat(left(cast (pyh_ref_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_ref_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_ref_dt as varchar(8)),2))as date) reffDt, "+  
					"pyh_amt_ap,  ven_ven_no, ven_ven_no, ven_ven_no, ven_ven_no,  "+
					"round(pyh_disc_amt_ap, 2) vch_disc_amt,  "+
					"cast(concat(concat(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_disc_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_disc_dt as varchar(8)),2))as date) discDt, "+ 
					"pyh_amt_ap - (case when (cast(concat(concat(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-'), concat(substr(cast (pyh_disc_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_disc_dt as varchar(8)),2))as date) - date(sysdate)) is null "+  
					"or  "+
					"(cast(concat(concat(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_disc_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_disc_dt as varchar(8)),2))as date) - date(sysdate) < 0) "+ 
					"then 0 else round(pyh_disc_amt_ap, 2) end) tot_amt, djh_chq_net_amt   "+
					"from apjdjo_rec "+
					"inner join aptpyh_rec on aptpyh_rec.pyh_ref_no=apjdjo_rec.djo_ref_no  "+
					"inner join aprven_rec on aprven_rec.ven_ven_no=aptpyh_rec.pyh_ven_no "+
					"inner join apjdjh_rec on djo_ssn_brh=djh_brh and djo_djh_no=djh_djh_no  and djh_trs_type='V' "+ 
					"where  pyh_disc_dt is not null AND pyh_ref_dt is not null AND pyh_due_dt is not null ";
			
					
					hql = hql + " and pyh_ref_cd ||'-'||pyh_ref_no = :vchr";
					hql = hql + " and cast(concat(concat(concat(left(cast (djh_issue_dt as varchar(8)),4),'-'),  concat(substr(cast (djh_issue_dt as varchar(8)),5,2) ,'-')),right(cast (djh_issue_dt as varchar(8)),2))as varchar(10))=:issue_dt";					
			// TODO trn_sts is not NULL
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("vchr", vchrNo);
			/*if(issueDate.length()>0)
				queryValidate.setParameter("issue_dt", issueDate);
			else*/
				queryValidate.setParameter("issue_dt", params.getIssueDtStr());

			List<Object[]> listCstmDocVal = queryValidate.list();
			CommonFunctions objCom = new CommonFunctions();
			for (Object[] cstmDocVal : listCstmDocVal) {

				cstmdocInfo.setVchrVenId(String.valueOf(cstmDocVal[0]));
				cstmdocInfo.setVchrVenNm(String.valueOf(cstmDocVal[1]));
				cstmdocInfo.setVchrInvNo(String.valueOf(cstmDocVal[2]));
				cstmdocInfo.setUpldBy(String.valueOf(cstmDocVal[3]));
				if (cstmDocVal[4] != null) {
					cstmdocInfo.setVchrInvDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[4])));
					cstmdocInfo.setVchrInvDt((Date) (cstmDocVal[4]));
				}
				if (cstmDocVal[5] != null) {
					cstmdocInfo.setVchrDueDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[5])));
					cstmdocInfo.setVchrDueDt((Date) (cstmDocVal[5]));
				}
				cstmdocInfo.setPoPfx(String.valueOf(cstmDocVal[6]));
				cstmdocInfo.setPoNo(String.valueOf(cstmDocVal[7]));
				cstmdocInfo.setPoItm(String.valueOf(cstmDocVal[8]));
				cstmdocInfo.setVchrCry(String.valueOf(cstmDocVal[9]));
				cstmdocInfo.setVchrPfx(String.valueOf(cstmDocVal[10]));
				cstmdocInfo.setVchrNo(String.valueOf(cstmDocVal[11]));
				cstmdocInfo.setVchrBrh(String.valueOf(cstmDocVal[12]));
				if (cstmDocVal[13] != null) {
					cstmdocInfo.setCrtDttsStr((objCom.formatDateWithoutTime((Date) cstmDocVal[13])));
					cstmdocInfo.setCrtDtts((Date) (cstmDocVal[13]));
				}
				else
				{
					cstmdocInfo.setCrtDttsStr("");
				}
				cstmdocInfo.setVchrAmt(Double.parseDouble(String.valueOf(cstmDocVal[14])));

				cstmdocInfo.setVchrAmtStr(objCom.formatAmount(Double.parseDouble(cstmDocVal[14].toString())));

				cstmdocInfo.setVchrCat(String.valueOf(cstmDocVal[15]));
				cstmdocInfo.setTransSts(String.valueOf(cstmDocVal[16]));
				cstmdocInfo.setPymntSts(String.valueOf(cstmDocVal[17]));
				cstmdocInfo.setCmpyId(String.valueOf(cstmDocVal[18]));

				/* DISCOUNT AMOUNT */
				
				cstmdocInfo.setDiscAmtStr(objCom.formatAmount(Double.parseDouble(String.valueOf(cstmDocVal[19]))));
				
				if (String.valueOf(cstmDocVal[10]).length() > 0 && String.valueOf(cstmDocVal[11]).length() > 0) {
					cstmDocInfoDAO.getFileNameByVchrNo(cstmdocInfo, String.valueOf(cstmDocVal[10]),
							String.valueOf(cstmDocVal[11]));
				}
				
				if (cstmDocVal[20] != null) {
					cstmdocInfo.setVchrDiscDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[20])));
					cstmdocInfo.setVchrDiscDt((Date) (cstmDocVal[20]));
				}
				else
				{
					cstmdocInfo.setVchrDiscDtStr("");
				}
				
				cstmdocInfo.setTotAmt(Double.parseDouble(String.valueOf(cstmDocVal[21])));

				cstmdocInfo.setTotAmtStr(objCom.formatAmount(Double.parseDouble(cstmDocVal[21].toString())));
				cstmdocInfo.setChkNetAmt(Double.parseDouble(String.valueOf(cstmDocVal[22])));
				GLInfoDAO glInfoDAO = new GLInfoDAO();
				//JournalInfo glRef = glInfoDAO
				//		.getJournalReference(cstmdocInfo.getVchrPfx() + "-" + cstmdocInfo.getVchrNo());
				JournalInfo glRef = null;
				if (glRef != null) {
					cstmdocInfo.setGlEntry(glRef.getJeEntry());
					cstmdocInfo.setGlEntryDt(glRef.getPostDt());
				} else {
					cstmdocInfo.setGlEntry("");
					cstmdocInfo.setGlEntryDt("");
				}

			}

		} catch (Exception e) {

			logger.debug("Payment Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}
	
	public void getBankDetails(PaymentInfoOutput params, String cmpyId) {
		Session session = null;
		String hql = "";

		try {
			session = SessionUtil.getSession();

			hql = " select * from cstm_cmpy_bnk_info where 1=1";

			if (cmpyId.length() > 0) {
				hql = hql + " and cmpy_id = :cmpy";
			}

			Query queryValidate = session.createSQLQuery(hql);

			if (cmpyId.length() > 0) {
				queryValidate.setParameter("cmpy", cmpyId);
			}

			List<Object[]> listCompany = queryValidate.list();

			BankDAO bankDAO = new BankDAO();

			List<CompanyInfo> bnkCompanyList = new ArrayList<CompanyInfo>();

			for (Object[] company : listCompany) {

				CompanyInfo output = new CompanyInfo();

				output.setCmpyId(company[0].toString());
				output.setBankCode(company[1].toString());

				BankInfo bnkInfo = new BankInfo();

				bankDAO.getBankAccountDetails(bnkInfo, output, session);

				output.setAccoutnList(bnkInfo.getBankAcctList());

				bnkCompanyList.add(output);

			}

			params.setCompanyBnkList(bnkCompanyList);

		} catch (Exception e) {
			logger.debug("Payment Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}
	}

	public List<PaymentMethod> readVendorPaymentMethod() {
		Session session = null;
		String hql = "";

		List<PaymentMethod> vndrPymntMthds = new ArrayList<PaymentMethod>();

		try {
			session = SessionUtil.getSession();

			hql = "SELECT cmpy_id, ven_id, id, mthd_cd, mthd_nm, pmt_dflt FROM ven_pay_mthd, pay_mthd WHERE pmt_mthd_id = id order by 2 desc";

			/*
			 * hql =
			 * "select shf_cmpy_id, shf_ven_id, (case when mpt_mthd_pmt = 'CH' then '5' when mpt_mthd_pmt = 'WT' then '1' when mpt_mthd_pmt = 'AC' then '2' end), "
			 * +
			 * "(case when mpt_mthd_pmt = 'CH' then 'CHK' when mpt_mthd_pmt = 'WT' then 'WIR' when mpt_mthd_pmt = 'AC' then 'CTX' end), mpt_desc15, true from scrmpt_rec, aprshf_rec "
			 * +
			 * "where shf_shp_fm = 0 and shf_mthd_pmt = mpt_mthd_pmt and mpt_mthd_pmt in ('AC', 'WT', 'CH')"
			 * ;
			 */

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listMethods = queryValidate.list();

			for (Object[] method : listMethods) {

				PaymentMethod output = new PaymentMethod();

				output.setCmpyId(method[0].toString());
				output.setVenId(method[1].toString());
				output.setMthdId(method[2].toString());
				output.setMthdCd(method[3].toString());
				output.setMthdNm(method[4].toString());
				output.setMthdDflt(Boolean.parseBoolean(method[5].toString()));

				vndrPymntMthds.add(output);

			}
		} catch (Exception e) {
			logger.debug("Payment Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}

		return vndrPymntMthds;
	}

	public void updateAchFileSentStatus(String flNm) {
		Session session = null;
		String hql = "";

		try {
			session = SessionUtil.getSession();

			hql = "UPDATE cstm_pay_params SET pay_fl_sent_sts=true WHERE pay_fl_id=:fl_nm";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("fl_nm", flNm);

			queryValidate.executeUpdate();
		} catch (Exception e) {
			logger.debug("Payment Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}
	}

	public void sftpAchFile(MaintanenceResponse<PaymentManOutput> starManResponse, String cmpyId, String reqId,
			String flNm) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		String host = "";
		String port = "";
		String user = "";
		String pass = "";
		String loc = "";

		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			if (checkSftpSetup(session, cmpyId) == 0) {
				starManResponse.output.messages.add(CommonConstants.NO_SFTP_ERR);
				starManResponse.output.rtnSts = 1;
			} else {
				hql = "SELECT bnk_host, bnk_port, bnk_ftp_usr, bnk_ftp_pass, bnk_ftp_loc "
						+ "FROM cstm_ftp_info F, cstm_cmpy_bnk_info B WHERE F.bnk_code=B.bnk_code "
						+ "AND B.cmpy_id=:cmpy_id and bnk_ftp_typ='PAY' LIMIT 1";
				Query queryValidate = session.createSQLQuery(hql);
				queryValidate.setParameter("cmpy_id", cmpyId);

				List<Object[]> listMethods = queryValidate.list();

				for (Object[] method : listMethods) {
					host = method[0].toString();
					port = method[1].toString();
					user = method[2].toString();
					pass = method[3].toString();
					loc = method[4].toString();
				}

				FTPDownloadUpload FTPDownloadUpload = new FTPDownloadUpload();

				boolean ftpSts = FTPDownloadUpload.uploadFile(flNm, host, port, user, pass, loc);

				if (ftpSts == false) {
					starManResponse.output.messages.add(CommonConstants.SERVER_ERR);
					starManResponse.output.rtnSts = 1;
				} else {
					updateAchFileSentStatus(flNm);
				}
			}
		} catch (Exception e) {

			logger.debug("Payment Info : {}", e.getMessage(), e);
			starManResponse.output.messages.add(e.getMessage());
			starManResponse.output.rtnSts = 1;

			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public Integer checkSftpSetup(Session session, String cmpyId) throws Exception {
		String hql = "";
		Integer iRcrdCount = 0;

		hql = "SELECT COUNT(bnk_host) FROM cstm_ftp_info F, cstm_cmpy_bnk_info B WHERE F.bnk_code=B.bnk_code AND B.cmpy_id=:cmpy_id";
		Query queryValidate = session.createSQLQuery(hql);
		queryValidate.setParameter("cmpy_id", cmpyId);
		iRcrdCount = Integer.parseInt(String.valueOf(queryValidate.list().get(0)));

		return iRcrdCount;
	}

	/* METHOD TO GET TOTAL VALUES FOR ALL THE VOUCHERS */
	public void readInvoiceList(PaymentInfoOutput params, String reqId, Session session) {
		String hql = "";

		double vchrAmt = 0;

		session = SessionUtil.getSession();

		hql = "SELECT string_agg(inv_no, ',') inv_no_lst, string_agg(distinct mthd_nm, ',') mthd_nm_lst FROM cstm_param_inv inv, pay_mthd pay where inv.vchr_pay_mthd = pay.id ";

		hql = hql + " and req_id=:req";

		Query queryValidate = session.createSQLQuery(hql);

		queryValidate.setParameter("req", reqId);

		List<Object[]> listRequest = queryValidate.list();

		List<CstmParamInv> list = new ArrayList<CstmParamInv>();

		for (Object[] request : listRequest) {

			if (request[0] != null) {
				vchrAmt = readVoucherTotal(request[0].toString(), params);
			}

			if (request[1] != null) {
				params.setPayMthd(request[1].toString());
			}
		}

		params.setAmtTotal(vchrAmt);

		CommonFunctions commonFunctions = new CommonFunctions();
		params.setAmtTotalStr(commonFunctions.formatAmount(vchrAmt));
	}

	public Double readVoucherTotal(String vchrNo, PaymentInfoOutput params) {
		Session session = null;
		String hql = "";
		double totalAmount = 0;
		String cry = "";

		try {
			session = SessionUtilInformix.getSession();

			/*hql = "SELECT sum(pyh_orig_amt - pyh_disc_amt) tot_amt, cast(pyh_cry as varchar(3)) cry FROM aptpyh_rec WHERE  pyh_opa_pfx ||'-'||pyh_opa_no in (:vchrList) group by 2";*/
			
			if(params.getPaySts().equals("A"))
			{
				hql = "select 	sum(tot_amt),cry from (select case when TO_DATE('"+ params.getReqActnOnStr() +"','MM/DD/YYYY') > pyh_disc_dt then "
						+ "sum(pyh_orig_amt - (case when (pyh_disc_dt - date(now())) is null or (pyh_disc_dt - date(now()) < 0) then 0 else pyh_disc_amt end))"
						+ "else sum(pyh_orig_amt - pyh_disc_amt) end tot_amt, 	"
						+ "cast(pyh_cry as varchar(3)) cry from aptpyh_rec where "
						+ "pyh_opa_pfx || '-' || pyh_opa_no in (:vchrList) group by 2, pyh_disc_dt ) amnt group by 2";
			}
			else
			{
				hql = "select 	sum(pyh_orig_amt - (case when (pyh_disc_dt - date(now())) is null or (pyh_disc_dt - date(now()) < 0) then 0 else pyh_disc_amt end)) tot_amt, 	"
					+ "cast(pyh_cry as varchar(3)) cry from aptpyh_rec where pyh_opa_pfx || '-' || pyh_opa_no in (:vchrList) group by 2";
			}

			// TODO trn_sts is not NULL
			Query queryValidate = session.createSQLQuery(hql);

			List<String> items = Arrays.asList(vchrNo.split("\\s*,\\s*"));

			queryValidate.setParameterList("vchrList", items);

			List<Object[]> listCstmDocVal = queryValidate.list();

			for (Object[] cstmDocVal : listCstmDocVal) {

				if (cstmDocVal[0] != null) {
					totalAmount = Double.parseDouble(cstmDocVal[0].toString());
					cry = cstmDocVal[1].toString();
				}

			}

			params.setCry(cry);

		} catch (Exception e) {
			logger.debug("Payment Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return totalAmount;

	}
	
	public Double readVoucherTotalESP(String vchrNo, PaymentInfoOutput params) {
		Session session = null;
		String hql = "";
		double totalAmount = 0;
		String cry = "";

		try {
			session = SessionUtilInformixESP.getSession();

			/*hql = "SELECT sum(pyh_orig_amt - pyh_disc_amt) tot_amt, cast(pyh_cry as varchar(3)) cry FROM aptpyh_rec WHERE  pyh_opa_pfx ||'-'||pyh_opa_no in (:vchrList) group by 2";*/
			
			
			/*
			if(params.getPaySts().equals("A"))
			{
				hql = "select 	sum(tot_amt),cry from (select case when TO_DATE('"+ params.getReqActnOnStr() +"','MM/DD/YYYY') > pyh_disc_dt then "
						+ "sum(pyh_orig_amt - (case when (pyh_disc_dt - date(now())) is null or (pyh_disc_dt - date(now()) < 0) then 0 else pyh_disc_amt end))"
						+ "else sum(pyh_orig_amt - pyh_disc_amt) end tot_amt, 	"
						+ "cast(pyh_cry as varchar(3)) cry from aptpyh_rec where "
						+ "pyh_opa_pfx || '-' || pyh_opa_no in (:vchrList) group by 2, pyh_disc_dt ) amnt group by 2";
			}
			else*/
			{
				hql = "select 	sum(pyh_amt_ap - (case when (cast(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-',substring(cast (pyh_disc_dt as varchar(8)),5,2) ,'-',right(cast (pyh_disc_dt as varchar(8)),2))  as date) - date(now())) is null or (cast(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-',substring(cast (pyh_disc_dt as varchar(8)),5,2) ,'-',right(cast (pyh_disc_dt as varchar(8)),2))  as date) - date(now()) < 0) then 0 else round(pyh_disc_amt_ap, 2) end)) tot_amt, trim('')  cry	"						
					+ "from aptpyh_rec where pyh_disc_dt>0 and pyh_disc_dt is not null and pyh_ref_cd || '-' || pyh_ref_no in (:vchrList) ";
			}
			/*ESP query*/
				hql=  	"select sum(pyh_amt_ap - (case when (cast(concat(concat(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-'), concat(substr(cast (pyh_disc_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_disc_dt as varchar(8)),2))as date) - date(sysdate)) is null "+  
						"or "+
						"(cast(concat(concat(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_disc_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_disc_dt as varchar(8)),2))as date) - date(sysdate) < 0)  "+
						"then 0 else round(pyh_disc_amt_ap, 2) end)) tot_amt,  "+
						"trim('')  cry	 "+
						"from aptpyh_rec  "+
						"where pyh_disc_dt>0 and pyh_disc_dt is not null and pyh_ref_cd || '-' || pyh_ref_no in (:vchrList) ";
			
				hql=  	"select sum(pyh_amt_ap - (case when (cast(concat(concat(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-'), concat(substr(cast (pyh_disc_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_disc_dt as varchar(8)),2))as date) - date(sysdate)) is null "+  
						"or "+
						"(cast(concat(concat(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_disc_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_disc_dt as varchar(8)),2))as date) - date(sysdate) < 0)  "+
						"then 0 else round(pyh_disc_amt_ap, 2) end)) tot_amt,  "+
						"trim('')  cry	 "+						
						"from apjdjo_rec "+
						"inner join aptpyh_rec on aptpyh_rec.pyh_ref_no=apjdjo_rec.djo_ref_no  "+
						"inner join aprven_rec on aprven_rec.ven_ven_no=aptpyh_rec.pyh_ven_no "+
						"inner join apjdjh_rec on djo_ssn_brh=djh_brh and djo_djh_no=djh_djh_no and djh_trs_type='V'  "+
						"where  pyh_disc_dt is not null AND pyh_ref_dt is not null AND pyh_due_dt is not null and pyh_ref_cd || '-' || pyh_ref_no in (:vchrList) ";
			
				hql=  	"select distinct djh_chq_no	, djh_chq_net_amt,  "+
						"trim('')  cry	 "+						
						"from apjdjo_rec "+
						"inner join aptpyh_rec on aptpyh_rec.pyh_ref_no=apjdjo_rec.djo_ref_no  "+
						"inner join aprven_rec on aprven_rec.ven_ven_no=aptpyh_rec.pyh_ven_no "+
						"inner join apjdjh_rec on djo_ssn_brh=djh_brh and djo_djh_no=djh_djh_no and djh_trs_type='V'  "+
						"where  pyh_disc_dt is not null AND pyh_ref_dt is not null AND pyh_due_dt is not null and pyh_ref_cd || '-' || pyh_ref_no in (:vchrList) ";
			
				
			//hql = "select 	sum(pyh_orig_amt - (case when (pyh_disc_dt - date(now())) is null or (pyh_disc_dt - date(now()) < 0) then 0 else pyh_disc_amt end)) tot_amt, 	"
			//		+ "cast(pyh_cry as varchar(3)) cry from aptpyh_rec where pyh_opa_pfx || '-' || pyh_opa_no in (:vchrList) group by 2";
			
			// TODO trn_sts is not NULL
			Query queryValidate = session.createSQLQuery(hql);

			List<String> items = Arrays.asList(vchrNo.split("\\s*,\\s*"));

			queryValidate.setParameterList("vchrList", items);

			List<Object[]> listCstmDocVal = queryValidate.list();
			
			for (Object[] cstmDocVal : listCstmDocVal) {

				if (cstmDocVal[1] != null) {
					totalAmount += Double.parseDouble(cstmDocVal[1].toString());
					cry = cstmDocVal[2].toString();
				}

			}

			params.setCry(cry);

		} catch (Exception e) {
			logger.debug("Payment Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return totalAmount;

	}

	/* GET ALL THE DEFINE FIELDS LIST */
	public List<VchrInfo> readStxVouchersExport(String usrId, String venIdFrm,
			String venIdTo, String payMthd, String invcDt, String invcDtTo, String dueDt, String dueDtTo, String discDt,
			String discDtTo) throws Exception {
		Session session = null;
		String hql = "";
		String voucherList = "";
		CstmDocInfoDAO docInfoDAO = new CstmDocInfoDAO();
		List<VchrInfo> vchrInfoLists = new ArrayList<VchrInfo>();
		
		List<CstmParamInv> invsListPending = new ArrayList<CstmParamInv>();

		invsListPending = docInfoDAO.getPendingVouchersSearch("", "", usrId);

		try {
			session = SessionUtilInformix.getSession();

			List<PaymentMethod> paymentMethods = readVendorPaymentMethod();

			hql = "select trim(pyh_ven_id) as ven_id,trim(ven_ven_long_nm) as ven_long_nm, "
					+ "trim(pyh_ven_inv_no) as ven_inv_no, "
					+ "(select vch_lgn_id from aptvch_rec where vch_cmpy_id = pyh_cmpy_id and vch_vchr_pfx = pyh_opa_pfx "
					+ "and vch_vchr_no = pyh_opa_no), pyh_ven_inv_dt, pyh_due_dt, trim(pyh_po_pfx) as po_pfx, pyh_po_no,"
					+ "pyh_po_itm, trim(pyh_cry) as cry,trim(pyh_opa_pfx) as vchr_pfx, pyh_opa_no,trim(pyh_ap_brh) as vchr_brh, "
					+ "pyh_ent_dt, pyh_orig_amt, (select vch_vchr_cat from aptvch_rec where vch_cmpy_id = pyh_cmpy_id and "
					+ "vch_vchr_pfx = pyh_opa_pfx and vch_vchr_no = pyh_opa_no) vch_cat,'C' trn_sts,'C' pay_sts,'' trn_rsn,'' "
					+ "pay_rsn, trim(pyh_cmpy_id) as cmpy_id, round(pyh_disc_amt, 2) vch_disc_amt, pay.tsa_sts_actn, "
					+ "cast((select rsn_desc30 from scrrsn_rec where rsn_rsn_typ = pay.tsa_rsn_typ and rsn_rsn = pay.tsa_rsn) as varchar(30))"
					+ " rsn_desc,trn.tsa_sts_actn trn_sts_actn, "
					+ "cast((select rsn_desc30 from scrrsn_rec where rsn_rsn_typ = trn.tsa_rsn_typ and rsn_rsn = trn.tsa_rsn) as varchar(30))"
					+ " rsn_trn_desc, pyh_disc_dt disc_dt,"
					+ " (case when (pyh_disc_dt - date(now())) is null or (pyh_disc_dt - date(now()) < 0) then 'N' else 'Y' end) disc_sts,"
					+ " pyh_orig_amt - (case when (pyh_disc_dt - date(now())) is null or (pyh_disc_dt - date(now()) < 0) then 0 else pyh_disc_amt end) tot_amt"
					+ " from aptpyh_rec inner join aprven_rec on ven_cmpy_id = pyh_cmpy_id and "
					+ "ven_ven_id = pyh_ven_id inner join tcttsa_rec pay on pay.tsa_cmpy_id = pyh_cmpy_id "
					+ "and pay.tsa_ref_pfx = pyh_ap_pfx and pay.tsa_ref_no = pyh_ap_no and pay.tsa_ref_itm = 0 "
					+ "and pay.tsa_ref_sbitm = 0 and pay.tsa_sts_typ = 'N' inner join tcttsa_rec trn on trn.tsa_cmpy_id = pyh_cmpy_id "
					+ "and trn.tsa_ref_pfx = pyh_ap_pfx and trn.tsa_ref_no = pyh_ap_no and trn.tsa_ref_itm = 0 and trn.tsa_ref_sbitm = 0 "
					+ "and trn.tsa_sts_typ = 'T' where 1=1 ";

			if (venIdFrm.length() > 0) {
				hql = hql + " and pyh_ven_id >= :ven_id_frm";
			}

			if (venIdTo.length() > 0) {
				hql = hql + " and pyh_ven_id <= :ven_id_to";
			}

			if (CommonConstants.DB_TYP.equals("POS")) {

				if (invcDt.length() > 0) {
					hql = hql + " and pyh_ven_inv_dt >= to_date(:invc_dt, 'mm/dd/yyyy')";
				}

				if (invcDtTo.length() > 0) {
					hql = hql + " and pyh_ven_inv_dt <= to_date(:invc_dt_to, 'mm/dd/yyyy')";
				}

				if (dueDt.length() > 0) {
					hql = hql + " and pyh_due_dt >= to_date(:due_dt, 'mm/dd/yyyy')";
				}

				if (dueDtTo.length() > 0) {
					hql = hql + " and pyh_due_dt <= to_date(:due_dt_to, 'mm/dd/yyyy')";
				}

				if (discDt.length() > 0) {
					hql = hql + " and pyh_disc_dt >= to_date(:disc_dt, 'mm/dd/yyyy')";
				}

				if (discDtTo.length() > 0) {
					hql = hql + " and pyh_disc_dt <= to_date(:disc_dt_to, 'mm/dd/yyyy')";
				}

			} else if (CommonConstants.DB_TYP.equals("INF")) {

				if (invcDt.length() > 0) {
					hql = hql + " and pyh_ven_inv_dt >= to_date(:invc_dt, '%m/%d/%Y')";
				}

				if (invcDtTo.length() > 0) {
					hql = hql + " and pyh_ven_inv_dt <= to_date(:invc_dt_to, '%m/%d/%Y')";
				}

				if (dueDt.length() > 0) {
					hql = hql + " and pyh_due_dt >= to_date(:due_dt, '%m/%d/%Y')";
				}

				if (dueDtTo.length() > 0) {
					hql = hql + " and pyh_due_dt <= to_date(:due_dt_to, '%m/%d/%Y')";
				}

				if (discDt.length() > 0) {
					hql = hql + " and pyh_disc_dt >= to_date(:disc_dt, '%m/%d/%Y')";
				}

				if (discDtTo.length() > 0) {
					hql = hql + " and pyh_disc_dt <= to_date(:disc_dt_to, '%m/%d/%Y')";
				}
			}

			hql = hql + " order by pyh_ven_id asc, pyh_due_dt asc";

			// TODO trn_sts is not NULL
			Query queryValidate = session.createSQLQuery(hql);

			if (venIdFrm.length() > 0) {
				queryValidate.setParameter("ven_id_frm", venIdFrm);
			}

			if (venIdTo.length() > 0) {
				queryValidate.setParameter("ven_id_to", venIdTo);
			}

			if (invcDt.length() > 0) {
				queryValidate.setParameter("invc_dt", invcDt);
			}

			if (dueDt.length() > 0) {
				queryValidate.setParameter("due_dt", dueDt);
			}

			if (invcDtTo.length() > 0) {
				queryValidate.setParameter("invc_dt_to", invcDtTo);
			}

			if (dueDtTo.length() > 0) {
				queryValidate.setParameter("due_dt_to", dueDtTo);
			}

			if (discDt.length() > 0) {
				queryValidate.setParameter("disc_dt", discDt);
			}

			if (discDtTo.length() > 0) {
				queryValidate.setParameter("disc_dt_to", discDtTo);
			}

			List<Object[]> listCstmDocVal = queryValidate.list();
			CommonFunctions objCom = new CommonFunctions();
			for (Object[] cstmDocVal : listCstmDocVal) {

				VchrInfo vchrInfo = new VchrInfo();

				vchrInfo.setVchrVenId(String.valueOf(cstmDocVal[0]));
				vchrInfo.setVchrVenNm(String.valueOf(cstmDocVal[1]));
				vchrInfo.setVchrInvNo(String.valueOf(cstmDocVal[2]));

				if (cstmDocVal[3] != null) {
					vchrInfo.setUpldBy(String.valueOf(cstmDocVal[3]));
				} else {
					vchrInfo.setUpldBy("");
				}

				if (cstmDocVal[4] != null) {
					vchrInfo.setVchrInvDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[4])));
					vchrInfo.setVchrInvDt((Date) (cstmDocVal[4]));
				}
				if (cstmDocVal[5] != null) {
					vchrInfo.setVchrDueDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[5])));
					vchrInfo.setVchrDueDt((Date) (cstmDocVal[5]));
				}
				vchrInfo.setPoPfx(String.valueOf(cstmDocVal[6]));
				vchrInfo.setPoNo(String.valueOf(cstmDocVal[7]));
				vchrInfo.setPoItm(String.valueOf(cstmDocVal[8]));
				vchrInfo.setVchrCry(String.valueOf(cstmDocVal[9]));
				vchrInfo.setVchrPfx(String.valueOf(cstmDocVal[10]));
				vchrInfo.setVchrNo(String.valueOf(cstmDocVal[11]));
				vchrInfo.setVchrBrh(String.valueOf(cstmDocVal[12]));
				if (cstmDocVal[13] != null) {
					vchrInfo.setCrtDttsStr((objCom.formatDateWithoutTime((Date) cstmDocVal[13])));
					vchrInfo.setCrtDtts((Date) (cstmDocVal[13]));
				} else {
					vchrInfo.setCrtDttsStr("");
				}
				vchrInfo.setVchrAmt(Double.parseDouble(String.valueOf(cstmDocVal[14])));
				vchrInfo.setVchrAmtStr(objCom.formatAmount(Double.parseDouble(String.valueOf(cstmDocVal[14]))));

				if (cstmDocVal[15] != null) {
					vchrInfo.setVchrCat(String.valueOf(cstmDocVal[15]));
				} else {
					vchrInfo.setVchrCat("");
				}

				if (cstmDocVal[16] != null) {
					vchrInfo.setTransSts(String.valueOf(cstmDocVal[16]));
				} else {
					vchrInfo.setTransSts("");
				}

				if (cstmDocVal[17] != null) {
					vchrInfo.setPymntSts(String.valueOf(cstmDocVal[17]));
				} else {
					vchrInfo.setPymntSts("");
				}

				if (cstmDocVal[18] != null) {
					vchrInfo.setTransStsRsn(String.valueOf(cstmDocVal[18]));
				} else {
					vchrInfo.setTransStsRsn("");
				}

				if (cstmDocVal[19] != null) {
					vchrInfo.setPymntStsRsn(String.valueOf(cstmDocVal[19]));
				} else {
					vchrInfo.setPymntStsRsn("");
				}

				vchrInfo.setCmpyId(cstmDocVal[20].toString());
				vchrInfo.setDiscAmt(Double.parseDouble(String.valueOf(cstmDocVal[21])));
				vchrInfo.setDiscAmtStr(objCom.formatAmount(Double.parseDouble(String.valueOf(cstmDocVal[21]))));

				if (cstmDocVal[22] != null) {
					vchrInfo.setPymntPaySts(cstmDocVal[22].toString());
				} else {
					vchrInfo.setPymntPaySts("");
				}

				if (cstmDocVal[23] != null) {
					vchrInfo.setPymntPayStsRsn(cstmDocVal[23].toString());
				} else {
					vchrInfo.setPymntPayStsRsn("");
				}

				if (cstmDocVal[24] != null) {
					vchrInfo.setTransPaySts(cstmDocVal[24].toString());
				} else {
					vchrInfo.setTransPaySts("");
				}

				if (cstmDocVal[25] != null) {
					vchrInfo.setTransPayStsRsn(cstmDocVal[25].toString());
				} else {
					vchrInfo.setTransPayStsRsn("");
				}
				
				if (cstmDocVal[26] != null) {
					vchrInfo.setVchrDiscDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[26])));
					vchrInfo.setVchrDiscDt((Date) (cstmDocVal[26]));
				}
				else
				{
					vchrInfo.setVchrDiscDtStr("");
				}
				
				vchrInfo.setDiscFlg(cstmDocVal[27].toString());
				
				vchrInfo.setTotAmt(Double.parseDouble(String.valueOf(cstmDocVal[28])));
				vchrInfo.setTotAmtStr(objCom.formatAmount(Double.parseDouble(String.valueOf(cstmDocVal[28]))));

				/* PAYMENT METHOD LIST AGAINST VENDOR */
				List<PaymentMethod> methods = new ArrayList<PaymentMethod>();

				int iPayMthd = 0;
				String payMthdStr = "";
				for (int i = 0; i < paymentMethods.size(); i++) {
					if (paymentMethods.get(i).getCmpyId().equals(vchrInfo.getCmpyId())
							&& paymentMethods.get(i).getVenId().equals(vchrInfo.getVchrVenId())) {
						
						payMthdStr = payMthdStr + " " + paymentMethods.get(i).getMthdNm();
						
						methods.add(paymentMethods.get(i));
					}

					if (payMthd.length() > 0) {
						List<String> payMthdList = Arrays.asList(payMthd.split("\\s*,\\s*"));

						if (payMthdList.contains(paymentMethods.get(i).getMthdId())
								&& paymentMethods.get(i).getVenId().equals(vchrInfo.getVchrVenId())
								&& paymentMethods.get(i).getCmpyId().equals(vchrInfo.getCmpyId())) {
							
							iPayMthd = 1;
						}
					}
				}
				
				for(int j = 0; j < invsListPending.size(); j++)
				{
					if(invsListPending.get(j).getInvNo().equals(vchrInfo.getVchrPfx() + "-" + vchrInfo.getVchrNo()))
					{
						vchrInfo.setVchrPmntSts("Y");
						vchrInfo.setReqId(invsListPending.get(j).getReqId());
						vchrInfo.setChkNo(invsListPending.get(j).getChkNo());
						break;
					}
					else
					{
						vchrInfo.setVchrPmntSts("N");
						vchrInfo.setReqId("");
						vchrInfo.setChkNo("");
					}
				}
				
				if(!vchrInfo.getPoNo().equals("0") && !vchrInfo.getPoItm().equals("0"))
				{
					vchrInfo.setPoNo(vchrInfo.getPoPfx() + "-" + vchrInfo.getPoNo() + "-" + vchrInfo.getPoItm());
				}
				else if(!vchrInfo.getPoNo().equals("0") && vchrInfo.getPoItm().equals("0"))
				{
					vchrInfo.setPoNo(vchrInfo.getPoPfx() + "-" + vchrInfo.getPoNo());
				}
				else
				{
					vchrInfo.setPoNo("");
				}
				
				vchrInfo.setVchrPmntMthdStr(payMthdStr);
				vchrInfo.setPayMthds(methods);
				if (vchrInfo.getPymntPaySts().equals("C") || vchrInfo.getTransPaySts().equals("C")) 
				{
					vchrInfoLists.add(vchrInfo);
				}
					 
			}

		} catch (Exception e) {

			logger.debug("Payment Info : {}", e.getMessage(), e);

			session.close();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		
		return vchrInfoLists;

	}
	
	public int validateDiscountDate(PaymentInfo paymentInfo) {
		Session session = null;
		String hql = "";
		int ircrdCount = 0;

		try {
			session = SessionUtil.getSession();

			hql = "select count(*) from cstm_pay_params where req_id=:req and cmpy_id=:cmpy";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("req", paymentInfo.getReqId());
			queryValidate.setParameter("cmpy", paymentInfo.getCmpyId());

			ircrdCount = Integer.parseInt(String.valueOf(queryValidate.list().get(0)));

		} catch (Exception e) {
			logger.debug("Payment Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return ircrdCount;
	}
	
	public List<String> getVoucherList(List<CstmParamInv> cstmParamInvs) {
		
		List<String> vchrList = new ArrayList<String>();
		
		for (int i = 0; i < cstmParamInvs.size(); i++) {

			vchrList.add(cstmParamInvs.get(i).getInvNo());
		}

		return vchrList;

	}
	
	public void deleteProposal(MaintanenceResponse<PaymentManOutput> starManResponse, String reqId, String paySts) {
		
		Session session = null;
		Transaction tx = null;
		String hql = "";

		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();
			
			/* DELETE REFERENCE INVOICES AGAINST PROPOSAL */
			hql = "delete from cstm_param_inv where req_id=:req_id";
			
			Query queryValidate = session.createSQLQuery(hql);
			
			queryValidate.setParameter("req_id", reqId);
			
			queryValidate.executeUpdate();
			
			/* DELETE PROPOSAL */
			hql = "delete from cstm_pay_params where req_id=:req_id";
			
			queryValidate = session.createSQLQuery(hql);
			
			queryValidate.setParameter("req_id", reqId);
			
			queryValidate.executeUpdate();
			
			/* DELETE PROPOSAL HISTORY*/
			hql = "delete from cstm_pay_params_his where req_id=:req_id";
			
			queryValidate = session.createSQLQuery(hql);
			
			queryValidate.setParameter("req_id", reqId);
			
			queryValidate.executeUpdate();

			tx.commit();
		} catch (Exception e) {

			logger.debug("Payment Information : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	
}
