package com.star.dao;

import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.CommonFunctions;
import com.star.common.MaintanenceResponse;
import com.star.common.SessionUtil;
import com.star.linkage.cstmvchrinfo.NotesInfo;
import com.star.linkage.cstmvchrinfo.NotesInfoBrowse;
import com.star.linkage.cstmvchrinfo.NotesInfoManOutput;
import com.star.modal.CstmVchrNotes;

public class VchrNotesDAO {

	private static Logger logger = LoggerFactory.getLogger(VchrNotesDAO.class);

	public void readVoucherNote(BrowseResponse<NotesInfoBrowse> starBrowseResponse, int ctlNo) {
		Session session = null;

		String hql = "";
		CommonFunctions commonFunctions = new CommonFunctions();

		try {
			session = SessionUtil.getSession();

			hql = "select notes_id, note, crtd_on, crtd_by, usr_nm from cstm_vchr_notes, usr_dtls where crtd_by=usr_id ";
			
			hql += " AND ctl_no =:ctlNo";
			
			hql += " order by notes_id desc";

			Query queryValidate = session.createSQLQuery(hql);
			
			queryValidate.setParameter("ctlNo", ctlNo);
			
			List<Object[]> listOverview = queryValidate.list();

			for (Object[] record : listOverview) {

				NotesInfo noteInfo = new NotesInfo();

				noteInfo.setNoteId(record[0].toString());
				noteInfo.setNote(record[1].toString());

				if (record[2] != null) {
					noteInfo.setCrtdOnStr((commonFunctions.formatDate((Date) record[2])));
				}
				
				noteInfo.setUsrId(record[3].toString());
				noteInfo.setUsrNm(record[4].toString());

				starBrowseResponse.output.fldTblVchrNotes.add(noteInfo);
			}

		} catch (Exception e) {

			logger.debug("Voucher Information : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void addVoucherNote(MaintanenceResponse<NotesInfoManOutput> starManResponse, String usrId, int ctlNo,
			String comment) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			CstmVchrNotes note = new CstmVchrNotes();

			note.setCrtdOn(new Date());
			note.setCrtdBy(usrId);
			note.setCtlNo(ctlNo);
			note.setNote(comment);

			session.save(note);

			tx.commit();
		} catch (Exception e) {
			logger.debug("Voucher Information : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void dltVoucherNote(MaintanenceResponse<NotesInfoManOutput> starManResponse, int notesId) {
		// TODO Auto-generated method stub
		Session session = null;
		Transaction tx = null;
		String hql = "";

		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();
			
			hql = "delete from cstm_vchr_notes where notes_id=:notes_id ";
			
			Query queryValidate = session.createSQLQuery(hql);
			
			queryValidate.setParameter("notes_id", notesId);
			
			queryValidate.executeUpdate();

			tx.commit();
		} catch (Exception e) {

			logger.debug("Voucher Information : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
}
