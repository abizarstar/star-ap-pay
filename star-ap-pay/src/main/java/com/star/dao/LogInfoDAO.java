package com.star.dao;

import java.util.Date;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.SessionUtil;
import com.star.modal.AppLogInfo;

public class LogInfoDAO {

	private static Logger logger = LoggerFactory.getLogger(LogInfoDAO.class);
	
	public void logInfoDetails(String user, String uriPath, String inpReq)
	{
		Session session = null;
		Transaction tx = null;
		Date date = new Date();
		try {

			session = SessionUtil.getSession();
			tx = session.beginTransaction();
			AppLogInfo params = new AppLogInfo();
			
			params.setLogUriPath(uriPath);
			params.setLogReq(inpReq);
			params.setLogUsr(user);
			params.setLogDtts(date);
			
			session.save(params);

			tx.commit();
		} catch (Exception e) {
			logger.debug("Login Info : {}", e.getMessage(), e);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	
	}
	
}
