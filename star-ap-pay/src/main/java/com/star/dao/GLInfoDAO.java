package com.star.dao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.MaintanenceResponse;
import com.star.common.SessionUtil;
import com.star.common.SessionUtilGL;
import com.star.common.SessionUtilInformix;
import com.star.linkage.ach.InvoiceInfo;
import com.star.linkage.bank.BankInfo;
import com.star.linkage.ebs.BankTrnDetails;
import com.star.linkage.ebstypecode.EBSGLAccountInfo;
import com.star.linkage.gl.GLBrowseOutput;
import com.star.linkage.gl.GLInfo;
import com.star.linkage.gl.GLPostingData;
import com.star.linkage.gl.GLPostingDataBrowseOutput;
import com.star.linkage.gl.GlPostingDataManOutput;
import com.star.linkage.gl.JournalInfo;
import com.star.linkage.gl.LedgerInfo;
import com.star.linkage.gl.SubAcctComp;
import com.star.linkage.gl.SubAcctInfo;
import com.star.linkage.gl.SubAcctInfoBrowse;
import com.star.linkage.payment.PaymentManOutput;
import com.star.modal.GlPostingInfo;

public class GLInfoDAO {

	private static Logger logger = LoggerFactory.getLogger(GLInfoDAO.class);

	public void readGLList(BrowseResponse<GLBrowseOutput> starBrowseResponse) {
		Session session = null;
		Transaction tx = null;
		String hql = "";

		try {
			session = SessionUtilGL.getSession();

			//hql = "SELECT CAST(bga_bsc_gl_acct AS VARCHAR(8)) gl_acct, CAST(bga_desc30 AS VARCHAR(30)) gl_acct_desc, CAST(bga_acct_typ AS VARCHAR(1)) acct_typ FROM glrbga_rec ORDER BY bga_bsc_gl_acct";
			hql = "SELECT CAST(act_gl_no AS VARCHAR(8)) gl_acct, CAST(act_desc AS VARCHAR(30)) gl_acct_desc, CAST(act_acc_typ AS VARCHAR(1)) acct_typ FROM glract_rec ORDER BY act_gl_no";
			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listGL = queryValidate.list();

			for (Object[] gl : listGL) {

				GLInfo output = new GLInfo();

				output.setGlAcct(gl[0].toString());
				output.setDesc(gl[1].toString());
				output.setAcctTyp(gl[2].toString());

				starBrowseResponse.output.fldTblGL.add(output);

			}

		} catch (Exception e) {
			logger.debug("GL Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			session.close();
		}

	}

	/* PLATEPLUS */
	public void readSubAcctList(BrowseResponse<SubAcctInfoBrowse> starBrowseResponse, String glAcct, String cmpyId) {

		List<SubAcctComp> subAcctComps = readSubAcctSeq();

		Session session = null;
		String hql = "";
		String subAcctOutput = "";
		String subAcctOutputDesc = "";
		int cmptStart = 0;
		int vldSubAccCombo = 0;

		List<Object[]> listGL1 = new ArrayList<Object[]>();
		List<Object[]> listGL2 = new ArrayList<Object[]>();
		List<Object[]> listGL3 = new ArrayList<Object[]>();
		List<Object[]> listGL4 = new ArrayList<Object[]>();

		LedgerInfo info = getLedgerId(cmpyId);

		try {
			session = SessionUtilGL.getSession();

			hql = "select bga_cmpt_aplc_1, bga_cmpt_aplc_2, bga_cmpt_aplc_3, bga_cmpt_aplc_4, bga_vld_sacct_comb from glrbga_rec where bga_bsc_gl_acct=:gl_acct";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("gl_acct", glAcct);

			List<Object[]> listGL = queryValidate.list();

			for (Object[] gl : listGL) {

				vldSubAccCombo = Integer.parseInt(gl[4].toString());

				if (vldSubAccCombo == 0) {
					if (gl[0].toString().equals("1")) {
						hql = "select cast(scv_cmpt_val as varchar(30)) cmpt_val, cast(scv_desc30 as varchar(30)) cmpt_desc from glrscf_rec,glrscv_rec where scv_lgr_id=:ldgr and scv_cmpt_id=scf_cmpt_id and scv_actv=1 and scf_cmpt_seq=1";

						queryValidate = session.createSQLQuery(hql);

						queryValidate.setParameter("ldgr", info.getLgrId());

						listGL1 = queryValidate.list();
					}

					if (gl[1].toString().equals("1")) {
						hql = "select cast(scv_cmpt_val as varchar(30)) cmpt_val, cast(scv_desc30 as varchar(30)) cmpt_desc from glrscf_rec,glrscv_rec where scv_lgr_id=:ldgr and scv_cmpt_id=scf_cmpt_id and scv_actv=1 and scf_cmpt_seq=2";

						queryValidate = session.createSQLQuery(hql);

						queryValidate.setParameter("ldgr", info.getLgrId());

						listGL2 = queryValidate.list();
					}

					if (gl[2].toString().equals("1")) {
						hql = "select cast(scv_cmpt_val as varchar(30)) cmpt_val, cast(scv_desc30 as varchar(30)) cmpt_desc from glrscf_rec,glrscv_rec where scv_lgr_id=:ldgr and scv_cmpt_id=scf_cmpt_id and scv_actv=1 and scf_cmpt_seq=3";

						queryValidate = session.createSQLQuery(hql);

						queryValidate.setParameter("ldgr", info.getLgrId());

						listGL3 = queryValidate.list();
					}

					if (gl[3].toString().equals("1")) {
						hql = "select cast(scv_cmpt_val as varchar(30)) cmpt_val, cast(scv_desc30 as varchar(30)) cmpt_desc from glrscf_rec,glrscv_rec where scv_lgr_id=:ldgr and scv_cmpt_id=scf_cmpt_id and scv_actv=1 and scf_cmpt_seq=4";

						queryValidate = session.createSQLQuery(hql);

						queryValidate.setParameter("ldgr", info.getLgrId());

						listGL4 = queryValidate.list();
					}
				}
			}

			if (vldSubAccCombo == 0) {
				if (listGL1.size() > 0 && listGL2.size() > 0 && listGL3.size() > 0 && listGL4.size() > 0) {
					for (int i = 0; i < listGL1.size(); i++) {
						for (int j = 0; j < listGL2.size(); j++) {
							for (int k = 0; k < listGL3.size(); k++) {
								for (int l = 0; l < listGL4.size(); l++) {
									SubAcctInfo output = new SubAcctInfo();
									subAcctOutput = listGL1.get(i)[0].toString().trim() + "-"
											+ listGL2.get(j)[0].toString().trim() + "-"
											+ listGL3.get(k)[0].toString().trim() + "-"
											+ listGL4.get(l)[0].toString().trim();

									subAcctOutputDesc = listGL1.get(i)[0].toString().trim() + "("
											+ listGL1.get(i)[1].toString().trim() + ")" + "-"
											+ listGL2.get(j)[0].toString().trim() + "("
											+ listGL2.get(j)[1].toString().trim() + ")" + "-"
											+ listGL3.get(k)[0].toString().trim() + "("
											+ listGL3.get(k)[1].toString().trim() + ")" + "-"
											+ listGL4.get(l)[0].toString().trim() + "("
											+ listGL4.get(l)[1].toString().trim() + ")";

									output.setSubAcct(subAcctOutput);
									output.setSubAcctDesc(subAcctOutputDesc);

									starBrowseResponse.output.fldTblSubAcct.add(output);
								}
							}
						}
					}
				} else if (listGL1.size() > 0 && listGL2.size() > 0 && listGL3.size() > 0) {
					for (int i = 0; i < listGL1.size(); i++) {
						for (int j = 0; j < listGL2.size(); j++) {
							for (int k = 0; k < listGL3.size(); k++) {
								SubAcctInfo output = new SubAcctInfo();
								subAcctOutput = listGL1.get(i)[0].toString().trim() + "-"
										+ listGL2.get(j)[0].toString().trim() + "-"
										+ listGL3.get(k)[0].toString().trim() + "-"
										+ getZeros(subAcctComps.get(3).getCmptLgth());

								subAcctOutputDesc = listGL1.get(i)[0].toString().trim() + "("
										+ listGL1.get(i)[1].toString().trim() + ")" + "-"
										+ listGL2.get(j)[0].toString().trim() + "("
										+ listGL2.get(j)[1].toString().trim() + ")" + "-"
										+ listGL3.get(k)[0].toString().trim() + "("
										+ listGL3.get(k)[1].toString().trim() + ")" + "-"
										+ getZeros(subAcctComps.get(3).getCmptLgth());

								output.setSubAcct(subAcctOutput);
								output.setSubAcctDesc(subAcctOutputDesc);

								starBrowseResponse.output.fldTblSubAcct.add(output);
							}
						}
					}
				} else if (listGL1.size() > 0 && listGL2.size() > 0 && listGL4.size() > 0) {
					for (int i = 0; i < listGL1.size(); i++) {
						for (int j = 0; j < listGL2.size(); j++) {
							for (int k = 0; k < listGL4.size(); k++) {
								SubAcctInfo output = new SubAcctInfo();
								subAcctOutput = listGL1.get(i)[0].toString().trim() + "-"
										+ listGL2.get(j)[0].toString().trim() + "-"
										+ getZeros(subAcctComps.get(2).getCmptLgth()) + "-"
										+ listGL4.get(k)[0].toString().trim();

								subAcctOutputDesc = listGL1.get(i)[0].toString().trim() + "("
										+ listGL1.get(i)[1].toString().trim() + ")" + "-"
										+ listGL2.get(j)[0].toString().trim() + "("
										+ listGL2.get(j)[1].toString().trim() + ")" + "-"
										+ getZeros(subAcctComps.get(2).getCmptLgth()) + "-"
										+ listGL4.get(k)[0].toString().trim() + "("
										+ listGL4.get(k)[1].toString().trim() + ")";

								output.setSubAcct(subAcctOutput);
								output.setSubAcctDesc(subAcctOutputDesc);

								starBrowseResponse.output.fldTblSubAcct.add(output);
							}
						}
					}
				} else if (listGL1.size() > 0 && listGL3.size() > 0 && listGL4.size() > 0) {
					for (int i = 0; i < listGL1.size(); i++) {
						for (int j = 0; j < listGL3.size(); j++) {
							for (int k = 0; k < listGL4.size(); k++) {
								SubAcctInfo output = new SubAcctInfo();
								subAcctOutput = listGL1.get(i)[0].toString().trim() + "-"
										+ getZeros(subAcctComps.get(1).getCmptLgth()) + "-"
										+ listGL3.get(j)[0].toString().trim() + "-"
										+ listGL4.get(k)[0].toString().trim();

								subAcctOutputDesc = listGL1.get(i)[0].toString().trim() + "("
										+ listGL1.get(i)[1].toString().trim() + ")" + "-"
										+ getZeros(subAcctComps.get(1).getCmptLgth()) + "-"
										+ listGL3.get(j)[0].toString().trim() + "("
										+ listGL3.get(j)[1].toString().trim() + ")" + "-"
										+ listGL4.get(k)[0].toString().trim() + "("
										+ listGL4.get(k)[1].toString().trim() + ")";

								output.setSubAcct(subAcctOutput);
								output.setSubAcctDesc(subAcctOutputDesc);

								starBrowseResponse.output.fldTblSubAcct.add(output);
							}
						}
					}
				} else if (listGL2.size() > 0 && listGL3.size() > 0 && listGL4.size() > 0) {
					for (int i = 0; i < listGL2.size(); i++) {
						for (int j = 0; j < listGL3.size(); j++) {
							for (int k = 0; k < listGL4.size(); k++) {
								SubAcctInfo output = new SubAcctInfo();
								subAcctOutput = getZeros(subAcctComps.get(0).getCmptLgth()) + "-"
										+ listGL2.get(i)[0].toString().trim() + "-"
										+ listGL3.get(j)[0].toString().trim() + "-"
										+ listGL4.get(k)[0].toString().trim();

								subAcctOutputDesc = getZeros(subAcctComps.get(0).getCmptLgth()) + "-"
										+ listGL2.get(i)[0].toString().trim() + "("
										+ listGL2.get(i)[1].toString().trim() + ")" + "-"
										+ listGL3.get(j)[0].toString().trim() + "("
										+ listGL3.get(j)[1].toString().trim() + ")" + "-"
										+ listGL4.get(k)[0].toString().trim() + "("
										+ listGL4.get(k)[1].toString().trim() + ")";

								output.setSubAcct(subAcctOutput);
								output.setSubAcctDesc(subAcctOutputDesc);

								starBrowseResponse.output.fldTblSubAcct.add(output);
							}
						}
					}
				} else if (listGL1.size() > 0 && listGL2.size() > 0) {
					for (int i = 0; i < listGL1.size(); i++) {
						for (int j = 0; j < listGL2.size(); j++) {
							SubAcctInfo output = new SubAcctInfo();
							subAcctOutput = listGL1.get(i)[0].toString().trim() + "-"
									+ listGL2.get(j)[0].toString().trim() + "-"
									+ getZeros(subAcctComps.get(2).getCmptLgth()) + "-"
									+ getZeros(subAcctComps.get(3).getCmptLgth());
							subAcctOutputDesc = listGL1.get(i)[0].toString().trim() + "("
									+ listGL1.get(i)[1].toString().trim() + ")" + "-"
									+ listGL2.get(j)[0].toString().trim() + "(" + listGL2.get(j)[1].toString().trim()
									+ ")" + "-" + getZeros(subAcctComps.get(2).getCmptLgth()) + "-"
									+ getZeros(subAcctComps.get(3).getCmptLgth());

							output.setSubAcct(subAcctOutput);
							output.setSubAcctDesc(subAcctOutputDesc);
							starBrowseResponse.output.fldTblSubAcct.add(output);
						}
					}
				} else if (listGL2.size() > 0 && listGL3.size() > 0) {
					for (int j = 0; j < listGL2.size(); j++) {
						for (int k = 0; k < listGL3.size(); k++) {
							SubAcctInfo output = new SubAcctInfo();
							subAcctOutput = getZeros(subAcctComps.get(0).getCmptLgth()) + "-"
									+ listGL2.get(j)[0].toString().trim() + "-" + listGL3.get(k)[0].toString().trim()
									+ "-" + getZeros(subAcctComps.get(3).getCmptLgth());

							subAcctOutputDesc = getZeros(subAcctComps.get(0).getCmptLgth()) + "-"
									+ listGL2.get(j)[0].toString().trim() + "(" + listGL2.get(j)[1].toString().trim()
									+ ")" + "-" + listGL3.get(k)[0].toString().trim() + "("
									+ listGL3.get(k)[1].toString().trim() + ")" + "-"
									+ getZeros(subAcctComps.get(3).getCmptLgth());

							output.setSubAcct(subAcctOutput);
							output.setSubAcctDesc(subAcctOutputDesc);
							starBrowseResponse.output.fldTblSubAcct.add(output);
						}
					}
				} else if (listGL1.size() > 0 && listGL3.size() > 0) {

					for (int i = 0; i < listGL1.size(); i++) {
						for (int k = 0; k < listGL3.size(); k++) {
							SubAcctInfo output = new SubAcctInfo();
							subAcctOutput = listGL1.get(i)[0].toString().trim() + "-"
									+ getZeros(subAcctComps.get(1).getCmptLgth()) + "-"
									+ listGL3.get(k)[0].toString().trim() + "-"
									+ getZeros(subAcctComps.get(3).getCmptLgth());

							subAcctOutputDesc = listGL1.get(i)[0].toString().trim() + "("
									+ listGL1.get(i)[1].toString().trim() + ")" + "-"
									+ getZeros(subAcctComps.get(1).getCmptLgth()) + "-"
									+ listGL3.get(k)[0].toString().trim() + "(" + listGL3.get(k)[1].toString().trim()
									+ ")" + "-" + getZeros(subAcctComps.get(3).getCmptLgth());

							output.setSubAcct(subAcctOutput);
							output.setSubAcctDesc(subAcctOutputDesc);
							starBrowseResponse.output.fldTblSubAcct.add(output);
						}
					}
				} else if (listGL1.size() > 0 && listGL4.size() > 0) {

					for (int i = 0; i < listGL1.size(); i++) {
						for (int k = 0; k < listGL4.size(); k++) {
							SubAcctInfo output = new SubAcctInfo();
							subAcctOutput = listGL1.get(i)[0].toString().trim() + "-"
									+ getZeros(subAcctComps.get(1).getCmptLgth()) + "-"
									+ getZeros(subAcctComps.get(2).getCmptLgth()) + "-"
									+ listGL4.get(k)[0].toString().trim();

							subAcctOutputDesc = listGL1.get(i)[0].toString().trim() + "("
									+ listGL1.get(i)[1].toString().trim() + ")" + "-"
									+ getZeros(subAcctComps.get(1).getCmptLgth()) + "-"
									+ getZeros(subAcctComps.get(2).getCmptLgth()) + "-"
									+ listGL4.get(k)[0].toString().trim() + "(" + listGL4.get(k)[1].toString().trim()
									+ ")";

							output.setSubAcct(subAcctOutput);
							output.setSubAcctDesc(subAcctOutputDesc);
							starBrowseResponse.output.fldTblSubAcct.add(output);
						}
					}
				} else if (listGL2.size() > 0 && listGL4.size() > 0) {
					for (int j = 0; j < listGL2.size(); j++) {
						for (int k = 0; k < listGL4.size(); k++) {
							SubAcctInfo output = new SubAcctInfo();
							subAcctOutput = getZeros(subAcctComps.get(0).getCmptLgth()) + "-"
									+ listGL2.get(j)[0].toString().trim() + "-"
									+ getZeros(subAcctComps.get(2).getCmptLgth()) + "-"
									+ listGL4.get(k)[0].toString().trim();

							subAcctOutputDesc = getZeros(subAcctComps.get(0).getCmptLgth()) + "-"
									+ listGL2.get(j)[0].toString().trim() + "(" + listGL2.get(j)[1].toString().trim()
									+ ")" + "-" + getZeros(subAcctComps.get(2).getCmptLgth()) + "-"
									+ listGL4.get(k)[0].toString().trim() + "(" + listGL4.get(k)[1].toString().trim()
									+ ")";

							output.setSubAcct(subAcctOutput);
							output.setSubAcctDesc(subAcctOutputDesc);
							starBrowseResponse.output.fldTblSubAcct.add(output);
						}
					}
				} else if (listGL3.size() > 0 && listGL4.size() > 0) {
					for (int j = 0; j < listGL3.size(); j++) {
						for (int k = 0; k < listGL4.size(); k++) {
							SubAcctInfo output = new SubAcctInfo();
							subAcctOutput = getZeros(subAcctComps.get(0).getCmptLgth()) + "-"
									+ getZeros(subAcctComps.get(1).getCmptLgth()) + "-"
									+ listGL3.get(j)[0].toString().trim() + "-" + listGL4.get(k)[0].toString().trim();

							subAcctOutputDesc = getZeros(subAcctComps.get(0).getCmptLgth()) + "-"
									+ getZeros(subAcctComps.get(1).getCmptLgth()) + "-"
									+ listGL3.get(j)[0].toString().trim() + "(" + listGL3.get(j)[1].toString().trim()
									+ ")" + "-" + listGL4.get(k)[0].toString().trim() + "("
									+ listGL4.get(k)[1].toString().trim() + ")";

							output.setSubAcct(subAcctOutput);
							output.setSubAcctDesc(subAcctOutputDesc);
							starBrowseResponse.output.fldTblSubAcct.add(output);
						}
					}
				} else if (listGL1.size() > 0) {
					for (int i = 0; i < listGL1.size(); i++) {
						SubAcctInfo output = new SubAcctInfo();
						subAcctOutput = listGL1.get(i)[0].toString().trim() + "-"
								+ getZeros(subAcctComps.get(1).getCmptLgth()) + "-"
								+ getZeros(subAcctComps.get(2).getCmptLgth()) + "-"
								+ getZeros(subAcctComps.get(3).getCmptLgth());

						subAcctOutputDesc = listGL1.get(i)[0].toString().trim() + "("
								+ listGL1.get(i)[1].toString().trim() + ")"
								+ getZeros(subAcctComps.get(1).getCmptLgth()) + "-"
								+ getZeros(subAcctComps.get(2).getCmptLgth()) + "-"
								+ getZeros(subAcctComps.get(3).getCmptLgth());
						output.setSubAcct(subAcctOutput);
						output.setSubAcctDesc(subAcctOutputDesc);
						starBrowseResponse.output.fldTblSubAcct.add(output);
					}
				} else if (listGL2.size() > 0) {
					for (int i = 0; i < listGL2.size(); i++) {
						SubAcctInfo output = new SubAcctInfo();
						subAcctOutput = getZeros(subAcctComps.get(0).getCmptLgth()) + "-"
								+ listGL2.get(i)[0].toString().trim() + "-"
								+ getZeros(subAcctComps.get(2).getCmptLgth()) + "-"
								+ getZeros(subAcctComps.get(3).getCmptLgth());
						subAcctOutputDesc = getZeros(subAcctComps.get(1).getCmptLgth()) + "-"
								+ listGL2.get(i)[0].toString().trim() + "(" + listGL2.get(i)[1].toString().trim() + ")"
								+ "-" + getZeros(subAcctComps.get(2).getCmptLgth()) + "-"
								+ getZeros(subAcctComps.get(3).getCmptLgth());

						output.setSubAcct(subAcctOutput);
						output.setSubAcctDesc(subAcctOutputDesc);
						starBrowseResponse.output.fldTblSubAcct.add(output);
					}
				} else if (listGL3.size() > 0) {
					for (int i = 0; i < listGL3.size(); i++) {
						SubAcctInfo output = new SubAcctInfo();
						subAcctOutput = getZeros(subAcctComps.get(0).getCmptLgth()) + "-"
								+ getZeros(subAcctComps.get(1).getCmptLgth()) + "-"
								+ listGL3.get(i)[0].toString().trim() + "-"
								+ getZeros(subAcctComps.get(3).getCmptLgth());

						subAcctOutputDesc = getZeros(subAcctComps.get(0).getCmptLgth()) + "-"
								+ getZeros(subAcctComps.get(1).getCmptLgth()) + "-"
								+ listGL3.get(i)[0].toString().trim() + "(" + listGL3.get(i)[1].toString().trim() + ")"
								+ "-" + getZeros(subAcctComps.get(3).getCmptLgth());
						output.setSubAcct(subAcctOutput);
						output.setSubAcctDesc(subAcctOutputDesc);
						starBrowseResponse.output.fldTblSubAcct.add(output);
					}
				} else if (listGL4.size() > 0) {
					for (int i = 0; i < listGL4.size(); i++) {
						SubAcctInfo output = new SubAcctInfo();
						subAcctOutput = getZeros(subAcctComps.get(0).getCmptLgth()) + "-"
								+ getZeros(subAcctComps.get(1).getCmptLgth()) + "-"
								+ getZeros(subAcctComps.get(2).getCmptLgth()) + "-"
								+ listGL4.get(i)[0].toString().trim();

						subAcctOutputDesc = getZeros(subAcctComps.get(0).getCmptLgth()) + "-"
								+ getZeros(subAcctComps.get(1).getCmptLgth()) + "-"
								+ getZeros(subAcctComps.get(2).getCmptLgth()) + "-"
								+ listGL4.get(i)[0].toString().trim() + "(" + listGL4.get(i)[1].toString().trim() + ")";
						output.setSubAcct(subAcctOutput);
						output.setSubAcctDesc(subAcctOutputDesc);
						starBrowseResponse.output.fldTblSubAcct.add(output);
					}
				}
			}

			if (vldSubAccCombo == 1) {
				hql = "SELECT DISTINCT cast(sdx_sacct as varchar(30)) as sub_acct,1 FROM glrsdx_rec WHERE sdx_bsc_gl_acct=:gl_acct AND sdx_actv=1";

				queryValidate = session.createSQLQuery(hql);

				queryValidate.setParameter("gl_acct", glAcct);

				List<Object[]> listGLCombo = queryValidate.list();

				for (Object[] gl : listGLCombo) {

					SubAcctInfo output = new SubAcctInfo();

					output.setGlAcct(glAcct);

					subAcctOutput = "";
					subAcctOutputDesc = "";

					cmptStart = 0;

					for (int i = 0; i < subAcctComps.size(); i++) {
						String subAcct = gl[0].toString();

						subAcctOutput = subAcctOutput
								+ subAcct.substring(cmptStart, subAcctComps.get(i).getCmptLgth() + cmptStart) + "-";

						String subAcctVal = subAcct.substring(cmptStart, subAcctComps.get(i).getCmptLgth() + cmptStart);

						String compNm = getComponentNm(subAcctVal, i + 1);

						subAcctOutputDesc = subAcctOutputDesc + subAcctVal + "(" + compNm + ")" + "-";

						cmptStart = cmptStart + subAcctComps.get(i).getCmptLgth();

					}

					subAcctOutput = subAcctOutput.substring(0, subAcctOutput.length() - 1);
					subAcctOutputDesc = subAcctOutputDesc.substring(0, subAcctOutputDesc.length() - 1);

					output.setSubAcct(subAcctOutput);
					output.setSubAcctDesc(subAcctOutputDesc);

					starBrowseResponse.output.fldTblSubAcct.add(output);
				}
			}

		} catch (Exception e) {
			logger.debug("GL Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			session.close();
		}

	}

	public void readSubAcctListSky(BrowseResponse<SubAcctInfoBrowse> starBrowseResponse, String glAcct, String cmpyId) {

		List<SubAcctComp> subAcctComps = readSubAcctSeq();

		Session session = null;
		String hql = "";
		String subAcctOutput = "";
		String subAcctOutputDesc = "";
		int cmptStart = 0;
		int vldSubAccCombo = 0;

		List<Object[]> listGL1 = new ArrayList<Object[]>();
		List<Object[]> listGL2 = new ArrayList<Object[]>();
		List<Object[]> listGL3 = new ArrayList<Object[]>();
		List<Object[]> listGL4 = new ArrayList<Object[]>();

		LedgerInfo info = getLedgerId(cmpyId);

		try {
			session = SessionUtilGL.getSession();

			hql = "select bga_cmpt_aplc_1, bga_cmpt_aplc_2, bga_cmpt_aplc_3, bga_cmpt_aplc_4, bga_vld_sacct_comb from glrbga_rec where bga_bsc_gl_acct=:gl_acct";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("gl_acct", glAcct);

			List<Object[]> listGL = queryValidate.list();

			for (Object[] gl : listGL) {

				vldSubAccCombo = Integer.parseInt(gl[4].toString());

				if (vldSubAccCombo == 0) {
					if (gl[0].toString().equals("1")) {
						hql = "select cast(scv_cmpt_val as varchar(30)) cmpt_val, cast(scv_desc30 as varchar(30)) cmpt_desc from glrscf_rec,glrscv_rec where scv_lgr_id=:ldgr and scv_cmpt_id=scf_cmpt_id and scv_actv=1 and scf_cmpt_seq=1";

						queryValidate = session.createSQLQuery(hql);

						queryValidate.setParameter("ldgr", info.getLgrId());

						listGL1 = queryValidate.list();
					}

					if (gl[1].toString().equals("1")) {
						hql = "select cast(scv_cmpt_val as varchar(30)) cmpt_val, cast(scv_desc30 as varchar(30)) cmpt_desc from glrscf_rec,glrscv_rec where scv_lgr_id=:ldgr and scv_cmpt_id=scf_cmpt_id and scv_actv=1 and scf_cmpt_seq=2";

						queryValidate = session.createSQLQuery(hql);

						queryValidate.setParameter("ldgr", info.getLgrId());

						listGL2 = queryValidate.list();
					}

					if (gl[2].toString().equals("1")) {
						hql = "select cast(scv_cmpt_val as varchar(30)) cmpt_val, cast(scv_desc30 as varchar(30)) cmpt_desc from glrscf_rec,glrscv_rec where scv_lgr_id=:ldgr and scv_cmpt_id=scf_cmpt_id and scv_actv=1 and scf_cmpt_seq=3";

						queryValidate = session.createSQLQuery(hql);

						queryValidate.setParameter("ldgr", info.getLgrId());

						listGL3 = queryValidate.list();
					}

					if (gl[3].toString().equals("1")) {
						hql = "select cast(scv_cmpt_val as varchar(30)) cmpt_val, cast(scv_desc30 as varchar(30)) cmpt_desc from glrscf_rec,glrscv_rec where scv_lgr_id=:ldgr and scv_cmpt_id=scf_cmpt_id and scv_actv=1 and scf_cmpt_seq=4";

						queryValidate = session.createSQLQuery(hql);

						queryValidate.setParameter("ldgr", info.getLgrId());

						listGL4 = queryValidate.list();
					}
				}
			}

			if (vldSubAccCombo == 0) {
				if (listGL1.size() > 0 && listGL2.size() > 0 && listGL3.size() > 0 && listGL4.size() > 0) {
					for (int i = 0; i < listGL1.size(); i++) {
						for (int j = 0; j < listGL2.size(); j++) {
							for (int k = 0; k < listGL3.size(); k++) {
								for (int l = 0; l < listGL4.size(); l++) {
									SubAcctInfo output = new SubAcctInfo();
									subAcctOutput = listGL1.get(i)[0].toString().trim() + "-"
											+ listGL2.get(j)[0].toString().trim() + "-"
											+ listGL3.get(k)[0].toString().trim() + "-"
											+ listGL4.get(l)[0].toString().trim();

									subAcctOutputDesc = listGL1.get(i)[0].toString().trim() + "("
											+ listGL1.get(i)[1].toString().trim() + ")" + "-"
											+ listGL2.get(j)[0].toString().trim() + "("
											+ listGL2.get(j)[1].toString().trim() + ")" + "-"
											+ listGL3.get(k)[0].toString().trim() + "("
											+ listGL3.get(k)[1].toString().trim() + ")" + "-"
											+ listGL4.get(l)[0].toString().trim() + "("
											+ listGL4.get(l)[1].toString().trim() + ")";

									output.setSubAcct(subAcctOutput);
									output.setSubAcctDesc(subAcctOutputDesc);

									starBrowseResponse.output.fldTblSubAcct.add(output);
								}
							}
						}
					}
				} else if (listGL1.size() > 0 && listGL2.size() > 0 && listGL3.size() > 0) {
					for (int i = 0; i < listGL1.size(); i++) {
						for (int j = 0; j < listGL2.size(); j++) {
							for (int k = 0; k < listGL3.size(); k++) {
								SubAcctInfo output = new SubAcctInfo();
								subAcctOutput = listGL1.get(i)[0].toString().trim() + "-"
										+ listGL2.get(j)[0].toString().trim() + "-"
										+ listGL3.get(k)[0].toString().trim();

								subAcctOutputDesc = listGL1.get(i)[0].toString().trim() + "("
										+ listGL1.get(i)[1].toString().trim() + ")" + "-"
										+ listGL2.get(j)[0].toString().trim() + "("
										+ listGL2.get(j)[1].toString().trim() + ")" + "-"
										+ listGL3.get(k)[0].toString().trim() + "("
										+ listGL3.get(k)[1].toString().trim() + ")";

								output.setSubAcct(subAcctOutput);
								output.setSubAcctDesc(subAcctOutputDesc);

								starBrowseResponse.output.fldTblSubAcct.add(output);
							}
						}
					}
				} else if (listGL1.size() > 0 && listGL2.size() > 0 && listGL4.size() > 0) {
					for (int i = 0; i < listGL1.size(); i++) {
						for (int j = 0; j < listGL2.size(); j++) {
							for (int k = 0; k < listGL4.size(); k++) {
								SubAcctInfo output = new SubAcctInfo();
								subAcctOutput = listGL1.get(i)[0].toString().trim() + "-"
										+ listGL2.get(j)[0].toString().trim() + "-"
										+ getZeros(subAcctComps.get(2).getCmptLgth()) + "-"
										+ listGL4.get(k)[0].toString().trim();

								subAcctOutputDesc = listGL1.get(i)[0].toString().trim() + "("
										+ listGL1.get(i)[1].toString().trim() + ")" + "-"
										+ listGL2.get(j)[0].toString().trim() + "("
										+ listGL2.get(j)[1].toString().trim() + ")" + "-"
										+ getZeros(subAcctComps.get(2).getCmptLgth()) + "-"
										+ listGL4.get(k)[0].toString().trim() + "("
										+ listGL4.get(k)[1].toString().trim() + ")";

								output.setSubAcct(subAcctOutput);
								output.setSubAcctDesc(subAcctOutputDesc);

								starBrowseResponse.output.fldTblSubAcct.add(output);
							}
						}
					}
				} else if (listGL1.size() > 0 && listGL3.size() > 0 && listGL4.size() > 0) {
					for (int i = 0; i < listGL1.size(); i++) {
						for (int j = 0; j < listGL3.size(); j++) {
							for (int k = 0; k < listGL4.size(); k++) {
								SubAcctInfo output = new SubAcctInfo();
								subAcctOutput = listGL1.get(i)[0].toString().trim() + "-"
										+ getZeros(subAcctComps.get(1).getCmptLgth()) + "-"
										+ listGL3.get(j)[0].toString().trim() + "-"
										+ listGL4.get(k)[0].toString().trim();

								subAcctOutputDesc = listGL1.get(i)[0].toString().trim() + "("
										+ listGL1.get(i)[1].toString().trim() + ")" + "-"
										+ getZeros(subAcctComps.get(1).getCmptLgth()) + "-"
										+ listGL3.get(j)[0].toString().trim() + "("
										+ listGL3.get(j)[1].toString().trim() + ")" + "-"
										+ listGL4.get(k)[0].toString().trim() + "("
										+ listGL4.get(k)[1].toString().trim() + ")";

								output.setSubAcct(subAcctOutput);
								output.setSubAcctDesc(subAcctOutputDesc);

								starBrowseResponse.output.fldTblSubAcct.add(output);
							}
						}
					}
				} else if (listGL2.size() > 0 && listGL3.size() > 0 && listGL4.size() > 0) {
					for (int i = 0; i < listGL2.size(); i++) {
						for (int j = 0; j < listGL3.size(); j++) {
							for (int k = 0; k < listGL4.size(); k++) {
								SubAcctInfo output = new SubAcctInfo();
								subAcctOutput = getZeros(subAcctComps.get(0).getCmptLgth()) + "-"
										+ listGL2.get(i)[0].toString().trim() + "-"
										+ listGL3.get(j)[0].toString().trim() + "-"
										+ listGL4.get(k)[0].toString().trim();

								subAcctOutputDesc = getZeros(subAcctComps.get(0).getCmptLgth()) + "-"
										+ listGL2.get(i)[0].toString().trim() + "("
										+ listGL2.get(i)[1].toString().trim() + ")" + "-"
										+ listGL3.get(j)[0].toString().trim() + "("
										+ listGL3.get(j)[1].toString().trim() + ")" + "-"
										+ listGL4.get(k)[0].toString().trim() + "("
										+ listGL4.get(k)[1].toString().trim() + ")";

								output.setSubAcct(subAcctOutput);
								output.setSubAcctDesc(subAcctOutputDesc);

								starBrowseResponse.output.fldTblSubAcct.add(output);
							}
						}
					}
				} else if (listGL1.size() > 0 && listGL2.size() > 0) {
					for (int i = 0; i < listGL1.size(); i++) {
						for (int j = 0; j < listGL2.size(); j++) {
							SubAcctInfo output = new SubAcctInfo();
							subAcctOutput = listGL1.get(i)[0].toString().trim() + "-"
									+ listGL2.get(j)[0].toString().trim() + "-"
									+ getZeros(subAcctComps.get(2).getCmptLgth());
							subAcctOutputDesc = listGL1.get(i)[0].toString().trim() + "("
									+ listGL1.get(i)[1].toString().trim() + ")" + "-"
									+ listGL2.get(j)[0].toString().trim() + "(" + listGL2.get(j)[1].toString().trim()
									+ ")" + "-" + getZeros(subAcctComps.get(2).getCmptLgth());

							output.setSubAcct(subAcctOutput);
							output.setSubAcctDesc(subAcctOutputDesc);
							starBrowseResponse.output.fldTblSubAcct.add(output);
						}
					}
				} else if (listGL2.size() > 0 && listGL3.size() > 0) {
					for (int j = 0; j < listGL2.size(); j++) {
						for (int k = 0; k < listGL3.size(); k++) {
							SubAcctInfo output = new SubAcctInfo();
							subAcctOutput = getZeros(subAcctComps.get(0).getCmptLgth()) + "-"
									+ listGL2.get(j)[0].toString().trim() + "-" + listGL3.get(k)[0].toString().trim();

							subAcctOutputDesc = getZeros(subAcctComps.get(0).getCmptLgth()) + "-"
									+ listGL2.get(j)[0].toString().trim() + "(" + listGL2.get(j)[1].toString().trim()
									+ ")" + "-" + listGL3.get(k)[0].toString().trim() + "("
									+ listGL3.get(k)[1].toString().trim() + ")";

							output.setSubAcct(subAcctOutput);
							output.setSubAcctDesc(subAcctOutputDesc);
							starBrowseResponse.output.fldTblSubAcct.add(output);
						}
					}
				} else if (listGL1.size() > 0 && listGL3.size() > 0) {

					for (int i = 0; i < listGL1.size(); i++) {
						for (int k = 0; k < listGL3.size(); k++) {
							SubAcctInfo output = new SubAcctInfo();
							subAcctOutput = listGL1.get(i)[0].toString().trim() + "-"
									+ getZeros(subAcctComps.get(1).getCmptLgth()) + "-"
									+ listGL3.get(k)[0].toString().trim();

							subAcctOutputDesc = listGL1.get(i)[0].toString().trim() + "("
									+ listGL1.get(i)[1].toString().trim() + ")" + "-"
									+ getZeros(subAcctComps.get(1).getCmptLgth()) + "-"
									+ listGL3.get(k)[0].toString().trim() + "(" + listGL3.get(k)[1].toString().trim()
									+ ")";

							output.setSubAcct(subAcctOutput);
							output.setSubAcctDesc(subAcctOutputDesc);
							starBrowseResponse.output.fldTblSubAcct.add(output);
						}
					}
				} else if (listGL1.size() > 0 && listGL4.size() > 0) {

					for (int i = 0; i < listGL1.size(); i++) {
						for (int k = 0; k < listGL4.size(); k++) {
							SubAcctInfo output = new SubAcctInfo();
							subAcctOutput = listGL1.get(i)[0].toString().trim() + "-"
									+ getZeros(subAcctComps.get(1).getCmptLgth()) + "-"
									+ getZeros(subAcctComps.get(2).getCmptLgth()) + "-"
									+ listGL4.get(k)[0].toString().trim();

							subAcctOutputDesc = listGL1.get(i)[0].toString().trim() + "("
									+ listGL1.get(i)[1].toString().trim() + ")" + "-"
									+ getZeros(subAcctComps.get(1).getCmptLgth()) + "-"
									+ getZeros(subAcctComps.get(2).getCmptLgth()) + "-"
									+ listGL4.get(k)[0].toString().trim() + "(" + listGL4.get(k)[1].toString().trim()
									+ ")";

							output.setSubAcct(subAcctOutput);
							output.setSubAcctDesc(subAcctOutputDesc);
							starBrowseResponse.output.fldTblSubAcct.add(output);
						}
					}
				} else if (listGL2.size() > 0 && listGL4.size() > 0) {
					for (int j = 0; j < listGL2.size(); j++) {
						for (int k = 0; k < listGL4.size(); k++) {
							SubAcctInfo output = new SubAcctInfo();
							subAcctOutput = getZeros(subAcctComps.get(0).getCmptLgth()) + "-"
									+ listGL2.get(j)[0].toString().trim() + "-"
									+ getZeros(subAcctComps.get(2).getCmptLgth()) + "-"
									+ listGL4.get(k)[0].toString().trim();

							subAcctOutputDesc = getZeros(subAcctComps.get(0).getCmptLgth()) + "-"
									+ listGL2.get(j)[0].toString().trim() + "(" + listGL2.get(j)[1].toString().trim()
									+ ")" + "-" + getZeros(subAcctComps.get(2).getCmptLgth()) + "-"
									+ listGL4.get(k)[0].toString().trim() + "(" + listGL4.get(k)[1].toString().trim()
									+ ")";

							output.setSubAcct(subAcctOutput);
							output.setSubAcctDesc(subAcctOutputDesc);
							starBrowseResponse.output.fldTblSubAcct.add(output);
						}
					}
				} else if (listGL3.size() > 0 && listGL4.size() > 0) {
					for (int j = 0; j < listGL3.size(); j++) {
						for (int k = 0; k < listGL4.size(); k++) {
							SubAcctInfo output = new SubAcctInfo();
							subAcctOutput = getZeros(subAcctComps.get(0).getCmptLgth()) + "-"
									+ getZeros(subAcctComps.get(1).getCmptLgth()) + "-"
									+ listGL3.get(j)[0].toString().trim() + "-" + listGL4.get(k)[0].toString().trim();

							subAcctOutputDesc = getZeros(subAcctComps.get(0).getCmptLgth()) + "-"
									+ getZeros(subAcctComps.get(1).getCmptLgth()) + "-"
									+ listGL3.get(j)[0].toString().trim() + "(" + listGL3.get(j)[1].toString().trim()
									+ ")" + "-" + listGL4.get(k)[0].toString().trim() + "("
									+ listGL4.get(k)[1].toString().trim() + ")";

							output.setSubAcct(subAcctOutput);
							output.setSubAcctDesc(subAcctOutputDesc);
							starBrowseResponse.output.fldTblSubAcct.add(output);
						}
					}
				} else if (listGL1.size() > 0) {
					for (int i = 0; i < listGL1.size(); i++) {
						SubAcctInfo output = new SubAcctInfo();
						subAcctOutput = listGL1.get(i)[0].toString().trim() + "-"
								+ getZeros(subAcctComps.get(1).getCmptLgth()) + "-"
								+ getZeros(subAcctComps.get(2).getCmptLgth());

						subAcctOutputDesc = listGL1.get(i)[0].toString().trim() + "("
								+ listGL1.get(i)[1].toString().trim() + ")"
								+ getZeros(subAcctComps.get(1).getCmptLgth()) + "-"
								+ getZeros(subAcctComps.get(2).getCmptLgth());
						output.setSubAcct(subAcctOutput);
						output.setSubAcctDesc(subAcctOutputDesc);
						starBrowseResponse.output.fldTblSubAcct.add(output);
					}
				} else if (listGL2.size() > 0) {
					for (int i = 0; i < listGL2.size(); i++) {
						SubAcctInfo output = new SubAcctInfo();
						subAcctOutput = getZeros(subAcctComps.get(0).getCmptLgth()) + "-"
								+ listGL2.get(i)[0].toString().trim() + "-"
								+ getZeros(subAcctComps.get(2).getCmptLgth());
						subAcctOutputDesc = getZeros(subAcctComps.get(1).getCmptLgth()) + "-"
								+ listGL2.get(i)[0].toString().trim() + "(" + listGL2.get(i)[1].toString().trim() + ")"
								+ "-" + getZeros(subAcctComps.get(2).getCmptLgth());

						output.setSubAcct(subAcctOutput);
						output.setSubAcctDesc(subAcctOutputDesc);
						starBrowseResponse.output.fldTblSubAcct.add(output);
					}
				} else if (listGL3.size() > 0) {
					for (int i = 0; i < listGL3.size(); i++) {
						SubAcctInfo output = new SubAcctInfo();
						subAcctOutput = getZeros(subAcctComps.get(0).getCmptLgth()) + "-"
								+ getZeros(subAcctComps.get(1).getCmptLgth()) + "-"
								+ listGL3.get(i)[0].toString().trim();

						subAcctOutputDesc = getZeros(subAcctComps.get(0).getCmptLgth()) + "-"
								+ getZeros(subAcctComps.get(1).getCmptLgth()) + "-"
								+ listGL3.get(i)[0].toString().trim() + "(" + listGL3.get(i)[1].toString().trim() + ")";
						output.setSubAcct(subAcctOutput);
						output.setSubAcctDesc(subAcctOutputDesc);
						starBrowseResponse.output.fldTblSubAcct.add(output);
					}
				} else if (listGL4.size() > 0) {
					for (int i = 0; i < listGL4.size(); i++) {
						SubAcctInfo output = new SubAcctInfo();
						subAcctOutput = getZeros(subAcctComps.get(0).getCmptLgth()) + "-"
								+ getZeros(subAcctComps.get(1).getCmptLgth()) + "-"
								+ getZeros(subAcctComps.get(2).getCmptLgth()) + "-"
								+ listGL4.get(i)[0].toString().trim();

						subAcctOutputDesc = getZeros(subAcctComps.get(0).getCmptLgth()) + "-"
								+ getZeros(subAcctComps.get(1).getCmptLgth()) + "-"
								+ getZeros(subAcctComps.get(2).getCmptLgth()) + "-"
								+ listGL4.get(i)[0].toString().trim() + "(" + listGL4.get(i)[1].toString().trim() + ")";
						output.setSubAcct(subAcctOutput);
						output.setSubAcctDesc(subAcctOutputDesc);
						starBrowseResponse.output.fldTblSubAcct.add(output);
					}
				}
			}

			if (vldSubAccCombo == 1) {
				hql = "SELECT DISTINCT cast(sdx_sacct as varchar(30)) as sub_acct,1 FROM glrsdx_rec WHERE sdx_bsc_gl_acct=:gl_acct AND sdx_actv=1";

				queryValidate = session.createSQLQuery(hql);

				queryValidate.setParameter("gl_acct", glAcct);

				List<Object[]> listGLCombo = queryValidate.list();

				for (Object[] gl : listGLCombo) {

					SubAcctInfo output = new SubAcctInfo();

					output.setGlAcct(glAcct);

					subAcctOutput = "";
					subAcctOutputDesc = "";

					cmptStart = 0;

					for (int i = 0; i < subAcctComps.size(); i++) {
						String subAcct = gl[0].toString();

						subAcctOutput = subAcctOutput
								+ subAcct.substring(cmptStart, subAcctComps.get(i).getCmptLgth() + cmptStart) + "-";

						String subAcctVal = subAcct.substring(cmptStart, subAcctComps.get(i).getCmptLgth() + cmptStart);

						String compNm = getComponentNm(subAcctVal, i + 1);

						subAcctOutputDesc = subAcctOutputDesc + subAcctVal + "(" + compNm + ")" + "-";

						cmptStart = cmptStart + subAcctComps.get(i).getCmptLgth();

					}

					subAcctOutput = subAcctOutput.substring(0, subAcctOutput.length() - 1);
					subAcctOutputDesc = subAcctOutputDesc.substring(0, subAcctOutputDesc.length() - 1);

					output.setSubAcct(subAcctOutput);
					output.setSubAcctDesc(subAcctOutputDesc);

					starBrowseResponse.output.fldTblSubAcct.add(output);
				}
			}

		} catch (Exception e) {
			logger.debug("GL Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			session.close();
		}

	}

	public String getComponentNm(String cmpt, int seq) {

		Session session = null;
		String hql = "";
		String compNm = "";

		try {
			session = SessionUtilGL.getSession();

			hql = "select cast(scv_desc30 as varchar(30)) cmpt_nm, 1 from glrscv_rec where scv_cmpt_val=:cmpt and "
					+ " scv_cmpt_id = (select scf_cmpt_id from glrscf_rec where scf_cmpt_seq=:seq)";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpt", cmpt);
			queryValidate.setParameter("seq", seq);

			List<Object[]> listComp = queryValidate.list();

			for (Object[] comp : listComp) {

				compNm = comp[0].toString();
			}

		} catch (Exception e) {
			logger.debug("GL Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return compNm;
	}

	public String getFinCoGateway() {

		Session session = null;
		String hql = "";
		String finCoId = "";

		try {
			session = SessionUtil.getSession();

			hql = "select src_co_id, 1 from fin_co_dtls";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listCurCmpy = queryValidate.list();

			for (Object[] cmpy : listCurCmpy) {

				finCoId = cmpy[0].toString();
			}

		} catch (Exception e) {
			logger.debug("GL Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return finCoId;
	}

	public String getAccountingPeriod(String postDt) {

		Session session = null;
		String hql = "";
		String acctPeriod = "";

		try {
			session = SessionUtilGL.getSession();

			hql = "select fis_acctg_per,1 from glrfis_rec where date_part('year', fis_bgn_per_dt) = date_part('year', now()) ";
			
			if(postDt.length() > 0)
			{
				hql = hql + " and fis_desc30 like (select to_char(to_date('"+ postDt +"', 'MM/DD/YYYY'), 'Mon'))||'%'";
			}
			else
			{
				hql = hql + " and fis_desc30 like (select to_char(current_date, 'Mon'))||'%'";
			}
			
			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listAccPer = queryValidate.list();

			for (Object[] acctPer : listAccPer) {

				acctPeriod = acctPer[0].toString();
			}

		} catch (Exception e) {
			logger.debug("GL Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return acctPeriod;
	}

	public LedgerInfo getLedgerId(String cmpyId) {

		Session session = null;
		String hql = "";
		String ledgerId = "";

		LedgerInfo ledgerInfo = new LedgerInfo();

		try {
			session = SessionUtilGL.getSession();

			hql = "select cast(lgr_lgr_id as varchar(3)) lgr_id, cast(lgr_lgr_cry as varchar(3)) cry from glrlgr_rec where lgr_glc_id=:cmpy_id and lgr_lgr_typ='O' limit 1";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", cmpyId);

			List<Object[]> listCurCmpy = queryValidate.list();

			for (Object[] cmpy : listCurCmpy) {

				ledgerInfo.setCry(cmpy[1].toString());
				ledgerInfo.setLgrId(cmpy[0].toString());
			}

		} catch (Exception e) {
			logger.debug("GL Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return ledgerInfo;
	}

	public String getGLCompany(String cmpyId) {

		Session session = null;
		String hql = "";
		String glCmpyId = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = "select cast(gle_gl_cmpy_id as varchar(3)) gl_cmpy_id, 1 from mxrgle_rec where gle_key_cd='GLE' limit 1";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listCurCmpy = queryValidate.list();

			for (Object[] cmpy : listCurCmpy) {

				glCmpyId = cmpy[0].toString();
			}

		} catch (Exception e) {
			logger.debug("GL Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return glCmpyId;
	}

	public void addLedgerMain(int upldId, int hdrId, BankTrnDetails bankTrnDetails, String cmpyId, String usrId,
			String rmk, String postDt) {

		Session session = null;
		Transaction tx = null;
		String hql = "";

		DateFormat dfYM = new SimpleDateFormat("yyyyMM");

		DateFormat dfFull = new SimpleDateFormat("yyyy-MM-dd");

		DateFormat formatter;
		Date dateobj = null;
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			dateobj = formatter.parse(postDt);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		

		LedgerInfo ledgerInfo = getLedgerId(cmpyId);

		int ictlNo = getCtlNo();

		int iInsCtlNo = getInsCtlNo();

		/* GET FINANCIAL COMPANY */
		String finCoId = getFinCoGateway();
		
		SimpleDateFormat formatDt = new SimpleDateFormat("MM/dd/yyyy");  
		String postDtFormatted= formatDt.format(dateobj);  
		
		String acctPer = getAccountingPeriod(postDtFormatted);

		String trnId = "LE-" + getGLTransactionSeq();

		cmpyId = getGLCompany(cmpyId);

		try {
			session = SessionUtilGL.getSession();
			tx = session.beginTransaction();

			hql = "insert into gligje_rec (gje_src_co_id, gje_gat_ctl_no, gje_lgr_id, gje_jrnl_ent_typ, "
					+ "gje_acctg_per, gje_actvy_dt, gje_lgn_id, gje_src_jrnl, "
					+ "gje_nar_desc, gje_trs_src, gje_auto_rls_pstg, gje_orig_ref_pfx, "
					+ "gje_orig_ref_no, gje_orig_ref_itm, gje_orig_ref_sbitm, gje_orig_cry, "
					+ "gje_orig_exrt, gje_gl_extl_id1, gje_gl_extl_id2, gje_gl_extl1, gje_gl_extl2, "
					+ "gje_gl_extl_desc) values (:co_id, :ctl_no, :lgr_id, 'N', :actng_prd, :actvy_dt, "
					+ ":usr_id, 'OTH', '', 'E', 0, '', 0, 0, 0, :cry, 1, :extl_id, '', '', '', :ext_desc)";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("co_id", finCoId);
			queryValidate.setParameter("ctl_no", ictlNo);
			queryValidate.setParameter("lgr_id", ledgerInfo.getLgrId());
			queryValidate.setParameter("usr_id", usrId);
			queryValidate.setParameter("cry", ledgerInfo.getCry());
			queryValidate.setParameter("actng_prd", Integer.parseInt(acctPer));
			queryValidate.setParameter("actvy_dt", dateobj);
			queryValidate.setParameter("ext_desc", rmk);
			queryValidate.setParameter("extl_id", trnId);

			queryValidate.executeUpdate();

			addLedgerAmount(session, bankTrnDetails.getTypeCode(), bankTrnDetails.getTrnAMount(), ictlNo, finCoId);

			addSctitn(session, ictlNo, finCoId, cmpyId);

			addSytasy(session, ictlNo, iInsCtlNo, finCoId, cmpyId, usrId);

			tx.commit();
			// updateTrnSts(trnRef, Integer.toString(trnId), Integer.toString(hdrId),
			// Integer.toString(upldId));
			tx = session.beginTransaction();
			BankStmtDAO bankStmtDAO = new BankStmtDAO();
			
			Date d1 = new Date();
			
			while (true) {
				
				Date d2 = new Date();
				long seconds = (d2.getTime()-d1.getTime())/1000;
				
				if(seconds > CommonConstants.VLD_CHK_DUR)
				{
					delSytasy(session, ictlNo);
					delSctitn(session, finCoId, ictlNo, cmpyId);
					delLedgerItems(session, finCoId, ictlNo);
					delLedgerMain(session, finCoId, ictlNo);
					break;
				}
				
				String refVal = bankStmtDAO.getSTXRefInfo(trnId);

				String[] trnRefVal = refVal.split(",");
				
				
				
				if (trnRefVal.length > 0) {
					if (trnRefVal[1].substring(0, 1).equals("Y")) {
						if (trnRefVal[0].length() > 0) {
							bankStmtDAO.updateStmtTrn(refVal, String.valueOf(bankTrnDetails.getId()),
									String.valueOf(hdrId), String.valueOf(upldId), "S");
							break;
						}

					} else {
						if (trnRefVal[1].substring(0, 1).equals("E")) {
							bankStmtDAO.updateStmtTrn(refVal, String.valueOf(bankTrnDetails.getId()),
									String.valueOf(hdrId), String.valueOf(upldId), "S");
							break;
						}
					}
				}
			}

			bankStmtDAO.updateLedgerRef(trnId, bankTrnDetails.getId(), hdrId, upldId);

			tx.commit();

		} catch (Exception e) {
			logger.debug("GL Info : {}", e.getMessage(), e);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	/* USED TO AUTO UPDATE TRANSACTION WHILE DOING PAYMENTS */
	public void addLedgerEntry(MaintanenceResponse<PaymentManOutput> starManResponse, String cmpyId, String usrId,
			InvoiceInfo invoiceInfo) {

		Session session = null;
		Transaction tx = null;
		String hql = "";

		DateFormat dfYM = new SimpleDateFormat("yyyyMM");

		Date dateobj = new Date();
		System.out.println(dfYM.format(dateobj));

		LedgerInfo ledgerInfo = getLedgerId(cmpyId);

		int ictlNo = getCtlNo();

		int iInsCtlNo = getInsCtlNo();

		String acctPer = getAccountingPeriod("");

		/* GET FINANCIAL COMPANY */
		String finCoId = getFinCoGateway();

		String trnId = "LE-" + getGLTransactionSeq();

		cmpyId = getGLCompany(cmpyId);/* TEMPRORY RECORD FOR TESTING */

		try {
			session = SessionUtilGL.getSession();
			tx = session.beginTransaction();

			hql = "insert into gligje_rec (gje_src_co_id, gje_gat_ctl_no, gje_lgr_id, gje_jrnl_ent_typ, "
					+ "gje_acctg_per, gje_actvy_dt, gje_lgn_id, gje_src_jrnl, "
					+ "gje_nar_desc, gje_trs_src, gje_auto_rls_pstg, gje_orig_ref_pfx, "
					+ "gje_orig_ref_no, gje_orig_ref_itm, gje_orig_ref_sbitm, gje_orig_cry, "
					+ "gje_orig_exrt, gje_gl_extl_id1, gje_gl_extl_id2, gje_gl_extl1, gje_gl_extl2, "
					+ "gje_gl_extl_desc) values (:co_id, :ctl_no, :lgr_id, 'N', :actng_prd, :actvy_dt, "
					+ ":usr_id, 'OTH', '', 'E', 0, '', 0, 0, 0, :cry, 1, :extl_id, :extl_id1, :extl_id2, '', :ext_desc)";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("co_id", finCoId);
			queryValidate.setParameter("ctl_no", ictlNo);
			queryValidate.setParameter("lgr_id", ledgerInfo.getLgrId());
			queryValidate.setParameter("usr_id", usrId);
			queryValidate.setParameter("cry", ledgerInfo.getCry());
			queryValidate.setParameter("actng_prd", Integer.parseInt(acctPer));
			queryValidate.setParameter("actvy_dt", dateobj);
			queryValidate.setParameter("ext_desc", invoiceInfo.getNotesToPayee());
			queryValidate.setParameter("extl_id", trnId);
			queryValidate.setParameter("extl_id1", invoiceInfo.getVchrPfx() + "-" + invoiceInfo.getVchrNo());
			queryValidate.setParameter("extl_id2", invoiceInfo.getVchrInvNo());

			queryValidate.executeUpdate();

			addLedgerAmount(session, "475", String.valueOf(invoiceInfo.getVchrAmt()), ictlNo, finCoId);

			addSctitn(session, ictlNo, finCoId, cmpyId);

			addSytasy(session, ictlNo, iInsCtlNo, finCoId, cmpyId, usrId);

			tx.commit();

			tx = session.beginTransaction();
			BankStmtDAO bankStmtDAO = new BankStmtDAO();

			while (true) {

				String refVal = bankStmtDAO.getSTXRefInfo(trnId);

				String[] trnRefVal = refVal.split(",");

				if (trnRefVal.length > 0) {
					if (trnRefVal[1].substring(0, 1).equals("Y")) {
						if (trnRefVal[0].length() > 0) {
							break;
						}

					} else {
						if (trnRefVal[1].substring(0, 1).equals("E")) {
							starManResponse.output.rtnSts = 1;
							starManResponse.output.messages.add(CommonConstants.GL_PAY_ERR);
							break;
						}
					}
				}
			}

			tx.commit();

		} catch (Exception e) {
			logger.debug("GL Info : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(CommonConstants.GL_PAY_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void addLedgerAmount(Session session, String typeCode, String trnAmt, int ictlNo, String finCoId)
			throws Exception {
		String hql = "";

		String subAcct = "";

		List<EBSGLAccountInfo> glebsMapInputs = getGLAccount("1", typeCode);

		for (int i = 0; i < glebsMapInputs.size(); i++) {

			if (glebsMapInputs.get(i).getPosSeq().equals("P1")) {
				if (glebsMapInputs.get(i).getTrnTyp().equals("C")) {
					hql = "insert into gligjd_rec (gjd_src_co_id, gjd_gat_ctl_no, gjd_gat_seq_no, gjd_bsc_gl_acct, "
							+ "gjd_inbd_sacct, gjd_dr_amt, gjd_cr_amt, gjd_dist_rmk) values "
							+ "(:co_id,:ctl_no, :s_no, :gl_acct, :sub_acct, :dr_amt, :cr_amt, '')";
				} else if (glebsMapInputs.get(i).getTrnTyp().equals("D")) {
					hql = "insert into gligjd_rec (gjd_src_co_id, gjd_gat_ctl_no, gjd_gat_seq_no, gjd_bsc_gl_acct, "
							+ "gjd_inbd_sacct, gjd_dr_amt, gjd_cr_amt, gjd_dist_rmk) values "
							+ "(:co_id,:ctl_no, :s_no, :gl_acct, :sub_acct, :dr_amt, :cr_amt, '')";
				}

				Query queryValidate = session.createSQLQuery(hql);

				queryValidate.setParameter("co_id", finCoId);
				queryValidate.setParameter("ctl_no", ictlNo);
				queryValidate.setParameter("s_no", i + 1);
				queryValidate.setParameter("gl_acct", glebsMapInputs.get(i).getGlAcct());

				subAcct = glebsMapInputs.get(i).getGlSubAcct();

				subAcct = getSubAcctFmtd(subAcct);

				queryValidate.setParameter("sub_acct", subAcct);

				if (glebsMapInputs.get(i).getTrnTyp().equals("C")) {
					queryValidate.setParameter("cr_amt", Double.parseDouble(trnAmt));
					queryValidate.setParameter("dr_amt", Double.parseDouble("0"));
				} else if (glebsMapInputs.get(i).getTrnTyp().equals("D")) {
					queryValidate.setParameter("dr_amt", Double.parseDouble(trnAmt));
					queryValidate.setParameter("cr_amt", Double.parseDouble("0"));
				}

				queryValidate.executeUpdate();
			}
		}

	}

	public void addSctitn(Session session, int ictlNo, String finCoId, String cmpyId) throws Exception {
		String hql = "";

		hql = "insert into sctitn_rec (itn_src_co_id, itn_gat_ctl_no, itn_cmpy_id, itn_crtd_dtts, "
				+ "itn_sts_cd, itn_ssn_log_ctl_no, itn_trs_cat) "
				+ "values (:co_id, :ctl_no, :cmpy, now(), 'N', 0, 'OJE');";

		Query queryValidate = session.createSQLQuery(hql);

		queryValidate.setParameter("co_id", finCoId);
		queryValidate.setParameter("ctl_no", ictlNo);
		queryValidate.setParameter("cmpy", cmpyId);

		queryValidate.executeUpdate();
	}

	public void addSytasy(Session session, int ictlNo, int iInsCtlNo, String finCoId, String cmpyId, String usrId)
			throws Exception {
		String hql = "";
		String inpStr = "";

		hql = "insert into sytasy_rec (asy_async_ctl_no, asy_cmpy_id, asy_async_rqst_typ, asy_svc_pgm_nm, asy_svc_id, asy_inp_lgth, asy_out_lgth, asy_cltyp, asy_bld_clnt_info, asy_upd_lgn_id, asy_crtd_dtts, asy_crtd_dtms, asy_inp_lnkg_fld) values\r\n"
				+ "(:ctl_no, :cmpy_id, 'H', 'glzgje', 'JE Gateway', '00119','00007','S', 'Y', :usr_id, now(), 0, '')";

		inpStr = inpStr + "1" + finCoId;

		for (int i = 0; i < 10 - finCoId.length(); i++) {
			inpStr = inpStr + " ";
		}

		for (int i = 0; i < 10 - String.valueOf(ictlNo).length(); i++) {
			inpStr = inpStr + "0";
		}

		inpStr = inpStr + ictlNo;

		Query queryValidate = session.createSQLQuery(hql);

		queryValidate.setParameter("cmpy_id", cmpyId);
		queryValidate.setParameter("usr_id", usrId);
		/* queryValidate.setParameter("inp_param", inpStr); */
		queryValidate.setParameter("ctl_no", iInsCtlNo);

		queryValidate.executeUpdate();
	}

	/* GET MAX CTL NO */
	public int getCtlNo() {
		Session session = null;
		String hql = "";
		int ircrdCount = 0;

		try {
			session = SessionUtilGL.getSession();

			hql = "select COALESCE(max(gje_gat_ctl_no), 0) + 1 from gligje_rec";

			Query queryValidate = session.createSQLQuery(hql);

			ircrdCount = Integer.parseInt(String.valueOf(queryValidate.list().get(0)));

		} catch (Exception e) {
			logger.debug("GL Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return ircrdCount;

	}

	/* GET MAX CTL NO */
	public int getInsCtlNo() {
		Session session = null;
		String hql = "";
		int ircrdCount = 0;

		try {
			session = SessionUtilGL.getSession();

			hql = "select COALESCE(max(asy_async_ctl_no), 0) from sytasy_rec";

			Query queryValidate = session.createSQLQuery(hql);

			ircrdCount = Integer.parseInt(String.valueOf(queryValidate.list().get(0))) + 1;

		} catch (Exception e) {
			logger.debug("GL Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return ircrdCount;

	}

	public List<EBSGLAccountInfo> getGLAccount(String ebsId, String typCd) {
		Session session = null;
		String hql = "";
		List<EBSGLAccountInfo> glebsMapInputs = new ArrayList<EBSGLAccountInfo>();

		try {
			session = SessionUtil.getSession();

			hql = " SELECT info1.post_cr_acct_1 gl_acct1, info1.post_cr_sub_acct_1 gl_sub_acct1, info1.post_cr_acct_2 gl_acct2, "
					+ "info1.post_cr_sub_acct_2 gl_sub_acct2, info1.post_cr_acct_3 gl_acct3, info1.post_cr_sub_acct_3 gl_sub_acct3, "
					+ "'P1' as posting, 'C' as trn_type FROM gl_ebs_map, gl_posting_info info1 WHERE post_1 = post_id "
					+ "AND   ebs_id =:ebs_id AND   typ_cd =:typ_cd " + "UNION "
					+ "SELECT info1.post_dr_acct_1 gl_acct1, info1.post_dr_sub_acct_1 gl_sub_acct1, info1.post_dr_acct_2 gl_acct2, "
					+ "info1.post_dr_sub_acct_2 gl_sub_acct2, info1.post_dr_acct_3 gl_acct3, info1.post_dr_sub_acct_3 gl_sub_acct3, "
					+ "'P1', 'D' FROM gl_ebs_map, gl_posting_info info1 WHERE post_1 = post_id "
					+ "AND   ebs_id =:ebs_id AND   typ_cd =:typ_cd " + "UNION "
					+ "SELECT info1.post_cr_acct_1 gl_acct1, info1.post_cr_sub_acct_1 gl_sub_acct1, info1.post_cr_acct_2 gl_acct2, "
					+ "info1.post_cr_sub_acct_2 gl_sub_acct2, info1.post_cr_acct_3 gl_acct3, info1.post_cr_sub_acct_3 gl_sub_acct3, "
					+ "'P2', 'C' FROM gl_ebs_map, gl_posting_info info1 WHERE post_2 = post_id "
					+ "AND   ebs_id =:ebs_id AND   typ_cd =:typ_cd " + "UNION "
					+ "SELECT info1.post_dr_acct_1 gl_acct1, info1.post_dr_sub_acct_1 gl_sub_acct1, info1.post_dr_acct_2 gl_acct2, "
					+ "info1.post_dr_sub_acct_2 gl_sub_acct2, info1.post_dr_acct_3 gl_acct3, info1.post_dr_sub_acct_3 gl_sub_acct3, "
					+ "'P2', 'D' FROM gl_ebs_map, gl_posting_info info1 WHERE post_2 = post_id "
					+ "AND   ebs_id =:ebs_id AND   typ_cd =:typ_cd";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("ebs_id", Integer.parseInt(ebsId));
			queryValidate.setParameter("typ_cd", typCd);

			List<Object[]> listMapping = queryValidate.list();

			for (Object[] mapping : listMapping) {
				EBSGLAccountInfo output = new EBSGLAccountInfo();

				output.setGlAcct(mapping[0].toString());
				output.setGlSubAcct(mapping[1].toString());
				output.setTrnTyp(mapping[7].toString());
				output.setPosSeq(mapping[6].toString());

				glebsMapInputs.add(output);

			}

		} catch (Exception e) {
			logger.debug("GL Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return glebsMapInputs;
	}

	public List<SubAcctComp> readSubAcctSeq() {
		Session session = null;
		String hql = "";

		List<SubAcctComp> subAcctCompList = new ArrayList<SubAcctComp>();

		try {
			session = SessionUtilGL.getSession();

			hql = "select cast(scf_cmpt_id as varchar(3)) as cmpt_id, scf_cmpt_seq, scf_cmpt_lgth from glrscf_rec order by scf_cmpt_seq";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listGL = queryValidate.list();

			for (Object[] gl : listGL) {

				SubAcctComp output = new SubAcctComp();

				output.setCmptId(gl[0].toString());
				output.setCmptSeq(Integer.parseInt(gl[1].toString()));
				output.setCmptLgth(Integer.parseInt(gl[2].toString()));

				subAcctCompList.add(output);

			}

		} catch (Exception e) {
			logger.debug("GL Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return subAcctCompList;
	}

	public int getOccurnce(String input) {
		int iCount = 0;

		for (int i = 0; i < input.length(); i++) {
			if (input.charAt(i) == ',') {
				iCount = iCount + 1;
			}
		}

		return iCount;
	}

	public String getSubAcctFmtd(String subAcct) {
		String subAcctFinalVal = "";
		List<SubAcctComp> subAcctComps = readSubAcctSeq();

		if (subAcct.length() > 0) {
			String arr[] = subAcct.split("-");

			for (int k = 0; k < arr.length; k++) {
				if (!getSubAcctValue(subAcctComps.get(k).getCmptLgth()).equals(arr[k])) {
					subAcctFinalVal = subAcctFinalVal + arr[k] + ",";
				} else {
					subAcctFinalVal = subAcctFinalVal + "" + ",";
				}
			}

		}

		if (subAcctFinalVal.length() > 0) {
			subAcctFinalVal = subAcctFinalVal.substring(0, subAcctFinalVal.length() - 1);
		}

		return subAcctFinalVal;
	}

	public String getSubAcctClnt(String subAcct) {
		String subAcctFinalVal = "";
		List<SubAcctComp> subAcctComps = readSubAcctSeq();

		String subAcctVal = "";
		int strCompLgth = 0;
		int strCompLgthPrev = 0;

		for (int i = 0; i < subAcctComps.size(); i++) {
			String tmpAct = "";

			strCompLgth = strCompLgth + subAcctComps.get(i).getCmptLgth();

			if (i > 0) {
				strCompLgthPrev = strCompLgthPrev + subAcctComps.get(i - 1).getCmptLgth();
			}

			if (i == 0) {
				tmpAct = subAcct.substring(0, strCompLgth);
			} else {
				int k = i - 1;
				tmpAct = subAcct.substring(strCompLgthPrev, strCompLgth);
			}

			subAcctVal = subAcctVal + tmpAct + "-";
		}

		if (subAcctVal.length() > 0) {
			subAcctVal = subAcctVal.substring(0, subAcctVal.length() - 1);
		}

		return subAcctVal;
	}

	public String getSubAcctValue(int compLgth) {
		String rtnVal = "";

		for (int i = 0; i < compLgth; i++) {
			rtnVal = rtnVal + "0";
		}

		return rtnVal;
	}

	/* GET AVAILABLE GL POSTING COUNT */
	public String getPostingCount() {
		Session session = null;
		String hql = "";
		String seq = "";

		try {
			session = SessionUtil.getSession();

			//hql = "select LPAD(cast((count(*)+1) as text), 7, '0') as seq_val from gl_posting_info";
			
			hql = "select LPAD(cast((cast(SUBSTRING (max(post_id), 4, length(max(post_id))) as int)+1) as text), 7, '0') as seq_val from gl_posting_info";

			Query queryValidate = session.createSQLQuery(hql);

			seq = String.valueOf(queryValidate.list().get(0));

		} catch (Exception e) {
			logger.debug("GL Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return seq;
	}

	/* GET EXISTING MAPPING DETAILS */
	public int getGLPostingDetails(BrowseResponse<GLPostingDataBrowseOutput> starBrowseResponse) {
		Session session = null;
		String hql = "";
		int ircrdCount = 0;

		try {
			session = SessionUtil.getSession();

			hql = "select post_id, post_cr_acct_1, post_cr_sub_acct_1, post_cr_acct_2, post_cr_sub_acct_2, "
					+ "post_cr_acct_3, post_cr_sub_acct_3,post_dr_acct_1, post_dr_sub_acct_1, post_cr_acct_2, "
					+ "post_dr_sub_acct_2, post_dr_acct_3, post_dr_sub_acct_3, post_cr_desc_1, post_cr_desc_2, post_cr_desc_3,"
					+ " post_dr_desc_1, post_dr_desc_2, post_dr_desc_3  from gl_posting_info";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listGLMapping = queryValidate.list();

			for (Object[] lgMapping : listGLMapping) {

				GLPostingData glPostingInfo = new GLPostingData();

				glPostingInfo.setPostId(lgMapping[0].toString());

				if (lgMapping[1] != null) {
					glPostingInfo.setCrAcct1(lgMapping[1].toString());
				} else {
					glPostingInfo.setCrAcct1("");
				}

				if (lgMapping[2] != null) {
					glPostingInfo.setCrSubAcct1(lgMapping[2].toString());
				} else {
					glPostingInfo.setCrSubAcct1("");
				}

				if (lgMapping[3] != null) {
					glPostingInfo.setCrAcct2(lgMapping[3].toString());
				} else {
					glPostingInfo.setCrAcct2("");
				}

				if (lgMapping[4] != null) {
					glPostingInfo.setCrSubAcct2(lgMapping[4].toString());
				} else {
					glPostingInfo.setCrSubAcct2("");
				}

				if (lgMapping[5] != null) {
					glPostingInfo.setCrAcct3(lgMapping[5].toString());
				} else {
					glPostingInfo.setCrAcct3("");
				}

				if (lgMapping[6] != null) {
					glPostingInfo.setCrSubAcct3(lgMapping[6].toString());
				} else {
					glPostingInfo.setCrSubAcct3("");
				}

				if (lgMapping[7] != null) {
					glPostingInfo.setDrAcct1(lgMapping[7].toString());
				} else {
					glPostingInfo.setDrAcct1("");
				}

				if (lgMapping[8] != null) {
					glPostingInfo.setDrSubAcct1(lgMapping[8].toString());
				} else {
					glPostingInfo.setDrSubAcct1("");
				}

				if (lgMapping[9] != null) {
					glPostingInfo.setDrAcct2(lgMapping[9].toString());
				} else {
					glPostingInfo.setDrAcct2("");
				}

				if (lgMapping[10] != null) {
					glPostingInfo.setDrSubAcct2(lgMapping[10].toString());
				} else {
					glPostingInfo.setDrSubAcct2("");
				}

				if (lgMapping[11] != null) {
					glPostingInfo.setDrAcct3(lgMapping[11].toString());
				} else {
					glPostingInfo.setDrAcct3("");
				}

				if (lgMapping[12] != null) {
					glPostingInfo.setDrSubAcct3(lgMapping[12].toString());
				} else {
					glPostingInfo.setDrSubAcct3("");
				}

				if (lgMapping[13] != null) {
					glPostingInfo.setCrAcct1Desc(lgMapping[13].toString());
				} else {
					glPostingInfo.setCrAcct1Desc("");
				}

				if (lgMapping[14] != null) {
					glPostingInfo.setCrAcct2Desc(lgMapping[14].toString());
				} else {
					glPostingInfo.setCrAcct2Desc("");
				}

				if (lgMapping[15] != null) {
					glPostingInfo.setCrAcct3Desc(lgMapping[15].toString());
				} else {
					glPostingInfo.setCrAcct3Desc("");
				}

				if (lgMapping[16] != null) {
					glPostingInfo.setDrAcct1Desc(lgMapping[16].toString());
				} else {
					glPostingInfo.setDrAcct1Desc("");
				}

				if (lgMapping[17] != null) {
					glPostingInfo.setDrAcct2Desc(lgMapping[17].toString());
				} else {
					glPostingInfo.setDrAcct2Desc("");
				}

				if (lgMapping[18] != null) {
					glPostingInfo.setDrAcct3Desc(lgMapping[18].toString());
				} else {
					glPostingInfo.setDrAcct3Desc("");
				}

				starBrowseResponse.output.fldTblGLPosting.add(glPostingInfo);
			}

		} catch (Exception e) {
			logger.debug("GL Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return ircrdCount;
	}

	public void addBankDetails(MaintanenceResponse<GlPostingDataManOutput> starManResponse, GLPostingData glData) {
		Session session = null;
		Transaction tx = null;

		String postSeq = getPostingCount();

		String posId = "POS" + postSeq;

		try {

			session = SessionUtil.getSession();
			tx = session.beginTransaction();
			GlPostingInfo postingInfo = new GlPostingInfo();

			postingInfo.setPostId(posId);
			postingInfo.setCrAcct1(glData.getCrAcct1());
			postingInfo.setCrAcct1Desc(glData.getCrAcct1Desc());
			postingInfo.setCrSubAcct1(glData.getCrSubAcct1());
			postingInfo.setCrAcct2(glData.getCrAcct2());
			postingInfo.setCrAcct2Desc(glData.getCrAcct2Desc());
			postingInfo.setCrSubAcct2(glData.getCrSubAcct2());
			postingInfo.setCrAcct3(glData.getCrAcct3());
			postingInfo.setCrAcct3Desc(glData.getCrAcct3Desc());
			postingInfo.setCrSubAcct3(glData.getCrSubAcct3());
			postingInfo.setDrAcct1(glData.getDrAcct1());
			postingInfo.setDrAcct1Desc(glData.getDrAcct1Desc());
			postingInfo.setDrSubAcct1(glData.getDrSubAcct1());
			postingInfo.setDrAcct2(glData.getDrAcct2());
			postingInfo.setDrAcct2Desc(glData.getDrAcct2Desc());
			postingInfo.setDrSubAcct2(glData.getDrSubAcct2());
			postingInfo.setDrAcct3(glData.getDrAcct3());
			postingInfo.setDrAcct3Desc(glData.getDrAcct3Desc());
			postingInfo.setDrSubAcct3(glData.getDrSubAcct3());

			session.save(postingInfo);

			tx.commit();
		} catch (Exception e) {

			logger.debug("Bank Information : {}", e.getMessage(), e);

			starManResponse.output.messages.add(e.getMessage());
			starManResponse.output.rtnSts = 1;

			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public JournalInfo getJournalReference(String vchrNo) {
		Session session = null;
		String hql = "";
		String trnRef = "";
		String sts = "";
		String errMsg = "";

		JournalInfo journalInfo = null;

		try {
			session = SessionUtilGL.getSession();

			hql = "select oje_oje_pfx || '-' ||oje_oje_no, to_char(oje_actvy_dt, 'mm/dd/yyyy') actv_dt from gltoje_rec where oje_gl_extl_id2=:ext_id";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("ext_id", vchrNo);

			List<Object[]> listGLEntry = queryValidate.list();

			for (Object[] gl : listGLEntry) {

				journalInfo = new JournalInfo();

				journalInfo.setJeEntry(String.valueOf(gl[0]));
				journalInfo.setPostDt(String.valueOf(gl[1]));
			}

		} catch (Exception e) {
			logger.debug("GL Info DAO : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return journalInfo;

	}

	public String getGLTransactionSeq() {

		Session session = null;
		String docCtl_no = "";

		try {

			String hql = "";

			session = SessionUtil.getSession();

			hql = "select to_char(nextval('gl_ref_seq'),'0000000FM')";

			Query queryValidate = session.createSQLQuery(hql);

			docCtl_no = queryValidate.list().get(0).toString();

		} catch (Exception e) {
			logger.debug("Document : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return docCtl_no;

	}

	public String getZeros(int strLgth) {
		String zeroVal = "";

		for (int i = 0; i < strLgth; i++) {
			zeroVal = zeroVal + "0";
		}

		return zeroVal;
	}
	
	
	public void delSctitn(Session session, String finCoId, int ictlNo, String cmpyId) throws Exception
	{
		String hql = "";
		
		hql = "delete from sctitn_rec where itn_src_co_id=:co_id and itn_gat_ctl_no=:ctl_no and itn_cmpy_id=:cmpy";

		Query queryValidate = session.createSQLQuery(hql);

		queryValidate.setParameter("co_id", finCoId);
		queryValidate.setParameter("ctl_no", ictlNo);
		queryValidate.setParameter("cmpy", cmpyId);
		
		queryValidate.executeUpdate();
	}
	
	public void delSytasy(Session session, int ictlNo) throws Exception
	{
		String hql = "";
		
		hql = "delete from sytasy_rec where asy_async_ctl_no=:ctl_no";

		Query queryValidate = session.createSQLQuery(hql);
		
		queryValidate.setParameter("ctl_no", ictlNo);
		
		queryValidate.executeUpdate();
		
	}
	
	public void delLedgerItems(Session session, String finCoId, int ictlNo) throws Exception
	{
		String hql = "";
		
		hql = "delete from gligjd_rec where gjd_src_co_id=:co_id and gjd_gat_ctl_no=:ctl_no";

		Query queryValidate = session.createSQLQuery(hql);
		
		queryValidate.setParameter("co_id", finCoId);
		queryValidate.setParameter("ctl_no", ictlNo);
		
		queryValidate.executeUpdate();
		
	}
	
	public void delLedgerMain(Session session, String finCoId, int ictlNo) throws Exception
	{
		String hql = "";
			
		hql = "delete from gligje_rec where gje_src_co_id=:co_id and gje_gat_ctl_no=:ctl_no";

		Query queryValidate = session.createSQLQuery(hql);
		
		queryValidate.setParameter("co_id", finCoId);
		queryValidate.setParameter("ctl_no", ictlNo);
		
		queryValidate.executeUpdate();
	}

	public void delGlPosInfo(MaintanenceResponse<GlPostingDataManOutput> starManResponse, String postId) {
		Session session = null;
		Transaction tx = null;

		try {

			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			Object o = session.load(GlPostingInfo.class, postId);
			GlPostingInfo glinfo = (GlPostingInfo) o;

			session.delete(glinfo);
			starManResponse.output.messages.add("Record deleted sucessfully... " + postId);

			tx.commit();
		} catch (Exception e) {

			logger.debug("Bank Information : {}", e.getMessage(), e);

			starManResponse.output.messages.add(e.getMessage());
			starManResponse.output.rtnSts = 1;

			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
