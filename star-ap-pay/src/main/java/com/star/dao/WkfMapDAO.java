package com.star.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.star.common.BrowseResponse;
import com.star.common.CommonFunctions;
import com.star.common.MaintanenceResponse;
import com.star.common.SessionUtil;
import com.star.linkage.cstmdocinfo.VchrInfo;
import com.star.linkage.wkf.WkfHisBrowseOutput;
import com.star.linkage.wkf.WkfMapManOutput;
import com.star.linkage.wkf.WorkflowMappingInfo;
import com.star.modal.CstmWkfMap;
import com.star.modal.CstmWkfMapHis;
import com.star.modal.WorkflowStep;
import com.star.service.MailService;

public class WkfMapDAO {

	private static Logger logger = LoggerFactory.getLogger(WkfMapDAO.class);

	public List<WorkflowStep> getWorkflowDetails() {
		Session session = null;

		List<WorkflowStep> workflowSteps = new ArrayList<WorkflowStep>();

		try {

			String hql = "";

			session = SessionUtil.getSession();

			hql = "select step_id, wkf.wkf_id, step_name, amt_flg, brh_flg, "
					+ "ven_flg, day_flg, amt_frm, amt_to, brh, ven_id, days, day_bef, "
					+ "apvr, crcn_flg, (amt_flg + brh_flg + ven_flg + day_flg) AS tot_cond from cstm_ocr_wkf_step step, cstm_ocr_wkf wkf "
					+ " where step.wkf_id=wkf.wkf_id and wkf.deleted_dtts is null "
					+ "ORDER BY tot_cond ASC, day_bef DESC, day_flg DESC, ven_flg DESC, brh_flg DESC, amt_flg DESC, amt_frm desc";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listWkf = queryValidate.list();

			for (Object[] wkf : listWkf) {
				WorkflowStep workflow = new WorkflowStep();

				workflow.setStepId(Integer.parseInt(wkf[0].toString()));
				workflow.setWkfId(Integer.parseInt(wkf[1].toString()));
				workflow.setStepName(wkf[2].toString());
				workflow.setAmtFlg(Integer.parseInt(wkf[3].toString()));
				workflow.setBrhFlg(Integer.parseInt(wkf[4].toString()));
				workflow.setVenFlg(Integer.parseInt(wkf[5].toString()));
				workflow.setDayFlg(Integer.parseInt(wkf[6].toString()));
				workflow.setAmtFrm(Float.parseFloat(wkf[7].toString()));
				workflow.setAmtTo(Float.parseFloat(wkf[8].toString()));
				workflow.setBrh(wkf[9].toString());
				workflow.setVenId(wkf[10].toString());
				workflow.setDays(Integer.parseInt(wkf[11].toString()));

				if (wkf[12] != null) {
					workflow.setDayBef(Integer.parseInt(wkf[12].toString()));
				}

				workflow.setApvr(wkf[13].toString());
				workflow.setCrcnFlg(Integer.parseInt(wkf[14].toString()));

				workflowSteps.add(workflow);

			}

		} catch (Exception e) {

			logger.debug("Workflow Map Information : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return workflowSteps;
	}

	public void addWkfMap(int ctlNo, int wkfId, String usrId, String asgnTo, String rmk) {
		Session session = null;
		Transaction tx = null;
		Date date = new Date();
		try {

			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			CstmWkfMap cstmWkfMap = new CstmWkfMap();
			cstmWkfMap.setCtlNo(ctlNo);
			cstmWkfMap.setWkfId(wkfId);
			cstmWkfMap.setWkfSts("S");
			cstmWkfMap.setWkfAsgnOn(date);

			if (asgnTo.length() > 0) {
				cstmWkfMap.setWkfAsgnTo(asgnTo);
			} else {
				cstmWkfMap.setWkfAsgnTo(usrId);
			}

			if (asgnTo.length() > 0) {
				cstmWkfMap.setWkfRmk("Workflow Assigned");
			} else {
				cstmWkfMap.setWkfRmk("Workflow Added");
			}

			cstmWkfMap.setWkfRmkTyp("S");

			session.save(cstmWkfMap);

			if (asgnTo.length() > 0) {
				cstmWkfMap.setWkfAsgnTo(usrId);
				cstmWkfMap.setWkfRmk(rmk);
				cstmWkfMap.setWkfRmkTyp("U");

				addWkfMapHis(session, cstmWkfMap);

				cstmWkfMap.setWkfAsgnTo(asgnTo);
				cstmWkfMap.setWkfRmk("Workflow Assigned");
				cstmWkfMap.setWkfRmkTyp("S");

			}

			addWkfMapHis(session, cstmWkfMap);

			tx.commit();

			CstmDocInfoDAO cstmDocInfoDAO = new CstmDocInfoDAO();

			String upldUsr = cstmDocInfoDAO.getInvoiceUploadId(cstmWkfMap.getCtlNo());

			sendNotification(cstmWkfMap.getCtlNo(), cstmWkfMap.getWkfId(), upldUsr, cstmWkfMap.getWkfAsgnTo(), "",
					cstmWkfMap.getWkfRmk(), cstmWkfMap.getWkfSts(), "");

		} catch (Exception e) {

			logger.debug("Workflow Mapping Info : {}", e.getMessage(), e);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	/* UPDATE WORKFLOW MAPPING */
	public void updateWkfMap(MaintanenceResponse<WkfMapManOutput> starManResponse, CstmWkfMap cstmWkfMap) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		String mailWkfSts = cstmWkfMap.getWkfSts();
		String mailWkfRmk = cstmWkfMap.getWkfRmk();

		String nxtAprvr = "";
		String lstAprvr = "";
		String extFlg = "";

		CstmDocInfoDAO cstmDocInfoDAO = new CstmDocInfoDAO();
		String upldUsr = cstmDocInfoDAO.getInvoiceUploadId(cstmWkfMap.getCtlNo());

		Date date = new Date();
		try {

			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			nxtAprvr = getNextApprover(cstmWkfMap.getWkfAsgnTo(), cstmWkfMap.getWkfId(), cstmWkfMap.getCtlNo());

			extFlg = getExternalFlg(cstmWkfMap.getWkfAsgnTo(), cstmWkfMap.getWkfId(), cstmWkfMap.getCtlNo());

			lstAprvr = getLastApprover(cstmWkfMap.getWkfAsgnTo(), cstmWkfMap.getWkfId(), cstmWkfMap.getCtlNo(), extFlg);

			if (!extFlg.equals("E")) {
				if (cstmWkfMap.getWkfSts().equals("A") || cstmWkfMap.getWkfSts().equals("F")) {
					if (nxtAprvr.length() > 0) {
						hql = "update cstm_wkf_map set wkf_sts = :sts, wkf_asgn_to = :asgn_to , wkf_asgn_on=:asgn_on, wkf_rmk=:rmk, wkf_rmk_typ=:rmk_typ "
								+ " where ctl_no=:ctl and wkf_id=:wkf";
					} else {
						hql = "update cstm_wkf_map set wkf_sts = :sts , wkf_asgn_on=:asgn_on, wkf_rmk=:rmk, wkf_rmk_typ=:rmk_typ"
								+ " where ctl_no=:ctl and wkf_id=:wkf";
					}
				} else if (cstmWkfMap.getWkfSts().equals("B")) {
					hql = "update cstm_wkf_map set wkf_sts = :sts, wkf_asgn_to = :asgn_to , wkf_asgn_on=:asgn_on, wkf_rmk=:rmk, wkf_rmk_typ=:rmk_typ"
							+ " where ctl_no=:ctl and wkf_id=:wkf";
				} else if (cstmWkfMap.getWkfSts().equals("S")) {
					hql = "update cstm_wkf_map set wkf_sts = :sts, wkf_asgn_to = :asgn_to , wkf_asgn_on=:asgn_on, wkf_rmk=:rmk, wkf_rmk_typ=:rmk_typ"
							+ " where ctl_no=:ctl and wkf_id=:wkf";
				} else if (cstmWkfMap.getWkfSts().equals("R")) {
					hql = "update cstm_wkf_map set wkf_sts = :sts, wkf_asgn_to = :asgn_to , wkf_asgn_on=:asgn_on, wkf_rmk=:rmk, wkf_rmk_typ=:rmk_typ"
							+ " where ctl_no=:ctl and wkf_id=:wkf";
				}
				else if (cstmWkfMap.getWkfSts().equals("RV")) {
					hql = "update cstm_wkf_map set wkf_sts = :sts, wkf_asgn_to = :asgn_to , wkf_asgn_on=:asgn_on, wkf_rmk=:rmk, wkf_rmk_typ=:rmk_typ"
							+ " where ctl_no=:ctl and wkf_id=:wkf";
				}
			} else {
				hql = "update cstm_wkf_map set wkf_sts = :sts, wkf_asgn_to = :asgn_to , wkf_asgn_on=:asgn_on, wkf_rmk=:rmk, wkf_rmk_typ=:rmk_typ "
						+ " where ctl_no=:ctl and wkf_id=:wkf";
			}

			Query queryValidate = session.createSQLQuery(hql);

			if (!extFlg.equals("E")) {
				if ((cstmWkfMap.getWkfSts().equals("A") || cstmWkfMap.getWkfSts().equals("F"))
						&& nxtAprvr.length() > 0) {
					queryValidate.setParameter("sts", "S");
					queryValidate.setParameter("rmk_typ", "S");
				} else if (cstmWkfMap.getWkfSts().equals("B")) {
					queryValidate.setParameter("sts", "S");
					queryValidate.setParameter("rmk_typ", "S");
				} else if (cstmWkfMap.getWkfSts().equals("R")) {
					queryValidate.setParameter("sts", "R");
					queryValidate.setParameter("rmk_typ", "U");
				} else if (cstmWkfMap.getWkfSts().equals("RV")) {
					queryValidate.setParameter("sts", "S");
					queryValidate.setParameter("rmk_typ", "U");
				} else {
					queryValidate.setParameter("sts", cstmWkfMap.getWkfSts());
					queryValidate.setParameter("rmk_typ", "E");
				}
			} else {
				queryValidate.setParameter("sts", "S");
				queryValidate.setParameter("rmk_typ", "S");
			}

			if (!extFlg.equals("E")) {
				if ((cstmWkfMap.getWkfSts().equals("A") || cstmWkfMap.getWkfSts().equals("F"))
						&& nxtAprvr.length() > 0) {
					queryValidate.setParameter("asgn_to", nxtAprvr);
				} else if (cstmWkfMap.getWkfSts().equals("B")) {
					queryValidate.setParameter("asgn_to", lstAprvr);
				} else if (cstmWkfMap.getWkfSts().equals("R")) {
					queryValidate.setParameter("asgn_to", cstmWkfMap.getWkfAsgnTo());
				} else if (cstmWkfMap.getWkfSts().equals("S")) {
					queryValidate.setParameter("asgn_to", cstmWkfMap.getWkfAsgnTo());
				} else if (cstmWkfMap.getWkfSts().equals("RV")) {
					queryValidate.setParameter("asgn_to", cstmWkfMap.getWkfAsgnTo());
				}
			} else {

				queryValidate.setParameter("asgn_to", lstAprvr);
			}

			queryValidate.setParameter("asgn_on", date);
			queryValidate.setParameter("ctl", cstmWkfMap.getCtlNo());
			queryValidate.setParameter("wkf", cstmWkfMap.getWkfId());

			if (!extFlg.equals("E")) {
				if ((cstmWkfMap.getWkfSts().equals("A") || cstmWkfMap.getWkfSts().equals("F"))
						&& nxtAprvr.length() > 0) {
					if (cstmWkfMap.getWkfSts().equals("F")) {
						queryValidate.setParameter("rmk", "Auto Assigned Workflow Queue - Rejected (Sent Forward)");
					} else if (cstmWkfMap.getWkfSts().equals("A")) {
						queryValidate.setParameter("rmk", "Auto Assigned Workflow Queue - Approved");
					}
				} else if (cstmWkfMap.getWkfSts().equals("B")) {

					queryValidate.setParameter("rmk", "Auto Assigned Workflow Queue - Rejected (Sent Back)");
				} else if (cstmWkfMap.getWkfSts().equals("R")) {

					queryValidate.setParameter("rmk", cstmWkfMap.getWkfRmk());
				} else if (cstmWkfMap.getWkfSts().equals("RV")) {

					queryValidate.setParameter("rmk", cstmWkfMap.getWkfRmk());
				} else {

					queryValidate.setParameter("rmk", cstmWkfMap.getWkfRmk());
				}
			} else {

				if (cstmWkfMap.getWkfSts().equals("A")) {
					queryValidate.setParameter("rmk", "Auto Assigned Workflow Queue - Approved");
				} else {
					queryValidate.setParameter("rmk", "Auto Assigned Workflow Queue - Rejected");
				}
			}

			queryValidate.executeUpdate();

			cstmWkfMap.setWkfAsgnOn(date);

			if (!extFlg.equals("E")) {
				if (cstmWkfMap.getWkfSts().equals("S")) {

					cstmWkfMap.setWkfRmkTyp("E");
				} else {
					cstmWkfMap.setWkfRmkTyp("U");
				}
			} else {
				cstmWkfMap.setWkfRmkTyp("E");
			}
			
			if(cstmWkfMap.getWkfSts().equals("RV"))
			{
				cstmWkfMap.setWkfSts("S");
			}
			addWkfMapHis(session, cstmWkfMap);

			if (!extFlg.equals("E")) {
				if ((cstmWkfMap.getWkfSts().equals("A") || cstmWkfMap.getWkfSts().equals("F"))
						&& nxtAprvr.length() > 0) {
					if (cstmWkfMap.getWkfSts().equals("F")) {
						cstmWkfMap.setWkfRmk("Auto Assigned Workflow Queue - Rejected (Sent Forward)");
					} else if (cstmWkfMap.getWkfSts().equals("A")) {
						cstmWkfMap.setWkfRmk("Auto Assigned Workflow Queue - Approved");
					}
					cstmWkfMap.setWkfSts("S");
					cstmWkfMap.setWkfAsgnTo(nxtAprvr);
					cstmWkfMap.setWkfRmkTyp("S");

					addWkfMapHis(session, cstmWkfMap);
				} else if (cstmWkfMap.getWkfSts().equals("B")) {
					cstmWkfMap.setWkfRmk("Auto Assigned Workflow Queue - Rejected (Sent Back)");

					cstmWkfMap.setWkfSts("S");
					cstmWkfMap.setWkfAsgnTo(lstAprvr);
					cstmWkfMap.setWkfRmkTyp("S");

					addWkfMapHis(session, cstmWkfMap);

				}
			} else {

				if (cstmWkfMap.getWkfSts().equals("R")) {
					cstmWkfMap.setWkfRmk("Auto Assigned Workflow Queue - Rejected");
				} else if (cstmWkfMap.getWkfSts().equals("A")) {
					cstmWkfMap.setWkfRmk("Auto Assigned Workflow Queue - Approved");
				}
				cstmWkfMap.setWkfSts("S");
				cstmWkfMap.setWkfAsgnTo(lstAprvr);
				cstmWkfMap.setWkfRmkTyp("S");

				addWkfMapHis(session, cstmWkfMap);
			}

			tx.commit();

			if (!extFlg.equals("E")) {
				
				sendNotification(cstmWkfMap.getCtlNo(), cstmWkfMap.getWkfId(), upldUsr, nxtAprvr, lstAprvr, mailWkfRmk,
						mailWkfSts, cstmWkfMap.getWkfSts());
			} else {
				sendNotification(cstmWkfMap.getCtlNo(), cstmWkfMap.getWkfId(), upldUsr, lstAprvr, lstAprvr, mailWkfRmk,
						mailWkfSts, cstmWkfMap.getWkfSts());
			}

		} catch (

		Exception e) {

			logger.debug("Workflow Mapping Info : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(e.getMessage());
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void addWkfMapHis(Session session, CstmWkfMap cstmWkfMap) {

		CstmWkfMapHis cstmWkfMapHis = new CstmWkfMapHis();
		cstmWkfMapHis.setCtlNo(cstmWkfMap.getCtlNo());
		cstmWkfMapHis.setWkfId(cstmWkfMap.getWkfId());
		cstmWkfMapHis.setWkfSts(cstmWkfMap.getWkfSts());
		cstmWkfMapHis.setWkfAsgnOn(cstmWkfMap.getWkfAsgnOn());
		cstmWkfMapHis.setWkfAsgnTo(cstmWkfMap.getWkfAsgnTo());
		cstmWkfMapHis.setWkfRmk(cstmWkfMap.getWkfRmk());
		cstmWkfMapHis.setWkfRmkTyp(cstmWkfMap.getWkfRmkTyp());

		session.save(cstmWkfMapHis);
	}

	public void getWorkflowHistory(BrowseResponse<WkfHisBrowseOutput> starBrowseResponse, String wkfId, String ctlNo) {
		String hql = "";
		Session session = null;
		CommonFunctions objCom = new CommonFunctions();
		try {

			session = SessionUtil.getSession();

			hql = "select wkf_asgn_on, (select usr_nm from usr_dtls where usr_id=wkf_asgn_to) wkf_asgn_nm, wkf_sts, wkf_rmk, wkf_rmk_typ from cstm_wkf_map_his where 1=1";

			if (wkfId.length() > 0) {
				hql += " and wkf_id=:wkf";
			}

			if (ctlNo.length() > 0) {
				hql += " and ctl_no=:ctl_no";
			}

			hql += " order by wkf_asgn_on asc, wkf_his_id asc";

			Query queryValidate = session.createSQLQuery(hql);

			if (wkfId.length() > 0) {
				queryValidate.setParameter("wkf", Integer.parseInt(wkfId));
			}

			if (ctlNo.length() > 0) {
				queryValidate.setParameter("ctl_no", Integer.parseInt(ctlNo));
			}

			List<Object[]> listWkfHis = queryValidate.list();

			for (Object[] wkf : listWkfHis) {
				WorkflowMappingInfo mappingInfo = new WorkflowMappingInfo();

				if (wkf[0] != null) {
					mappingInfo.setAsgnOnDt((Date) wkf[0]);
					mappingInfo.setAsgnOnDtStr((objCom.formatDate((Date) wkf[0])));
				}

				mappingInfo.setAsgnToNm(wkf[1].toString());
				mappingInfo.setSts(wkf[2].toString());

				if (mappingInfo.getSts().equals("S")) {
					mappingInfo.setSts("In Review");
				} else if (mappingInfo.getSts().equals("R")) {
					mappingInfo.setSts("Rejected");
				} else if (mappingInfo.getSts().equals("A")) {
					mappingInfo.setSts("Approved");
				} else if (mappingInfo.getSts().equals("B")) {
					mappingInfo.setSts("Sent Back");
				} else if (mappingInfo.getSts().equals("F")) {
					mappingInfo.setSts("Forwarded");
				} else {
					mappingInfo.setSts("");
				}

				mappingInfo.setRmk(wkf[3].toString());

				mappingInfo.setRmkTyp(wkf[4].toString());

				WorkflowDAO workflowDAO = new WorkflowDAO();

				mappingInfo.setWkfAprvrList(workflowDAO.readWorkflowAprvr(session, wkfId));

				starBrowseResponse.output.fldTblWkHistory.add(mappingInfo);

			}

		} catch (Exception e) {

			logger.debug("Workflow Map Information : {}", e.getMessage(), e);
		} finally {
			session.close();
		}
	}

	/* GET WORK FLOW DETAILS */
	public void getWorkflowSpecific(String ctlNo, VchrInfo vchrInfo) {
		String hql = "";
		Session session = null;

		try {

			session = SessionUtil.getSession();

			hql = "select  map.wkf_id, wkf_name, wkf_sts,wkf_asgn_to, "
					+ "(select usr_nm from usr_dtls where usr_id=wkf_asgn_to) wkf_asgn_nm"
					+ " from cstm_wkf_map map left outer join cstm_ocr_wkf ocr on map.wkf_id=ocr.wkf_id where 1=1";

			if (ctlNo.length() > 0) {
				hql += " and ctl_no=:ctl_no";
			}

			Query queryValidate = session.createSQLQuery(hql);

			if (ctlNo.length() > 0) {
				queryValidate.setParameter("ctl_no", Integer.parseInt(ctlNo));
			}

			List<Object[]> listWkfHis = queryValidate.list();

			for (Object[] wkf : listWkfHis) {

				vchrInfo.setWrkFlwId(Integer.parseInt(wkf[0].toString()));
				if (wkf[1] != null) {
					vchrInfo.setWrkFlw(wkf[1].toString());
				} else {
					vchrInfo.setWrkFlw("");
				}
				vchrInfo.setWrkFlwSts(wkf[2].toString());
				vchrInfo.setWrkFlwAprvr(wkf[3].toString());
				vchrInfo.setWrkFlwAprvrNm(wkf[4].toString());

				setNextPrevApprover(vchrInfo.getWrkFlwAprvr(), vchrInfo.getWrkFlwId(), vchrInfo);
			}

			if (listWkfHis.size() == 0) {
				vchrInfo.setWrkFlwId(0);
				vchrInfo.setWrkFlw("");
				vchrInfo.setWrkFlwSts("");
				vchrInfo.setWrkFlwAprvr("");
			}

		} catch (Exception e) {

			logger.debug("Workflow Map Information : {}", e.getMessage(), e);
		} finally {
			session.close();
		}
	}

	/* GET WORK FLOW NEXT USER DETAILS */
	public String getNextApprover(String aprvr, int wkfId, int ctlNo) {
		String hql = "";
		Session session = null;
		String nxtAprvr = "";

		try {

			session = SessionUtil.getSession();

			hql = "select wkf_aprvr,1 from cstm_wkf_aprvr where wkf_id=:wkf and"
					+ " wkf_aprv_ordr > (select wkf_aprv_ordr from cstm_wkf_aprvr where wkf_id=:wkf and wkf_aprvr = :aprvr) "
					+ " order by wkf_aprv_ordr asc limit 1";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("wkf", wkfId);
			queryValidate.setParameter("aprvr", aprvr);

			List<Object[]> listWkfAprvr = queryValidate.list();

			for (Object[] wkfAprvr : listWkfAprvr) {

				nxtAprvr = wkfAprvr[0].toString();

			}

			/*
			 * if(listWkfAprvr.size() == 0) { hql =
			 * " select wkf_asgn_to,1 from cstm_wkf_map_his where wkf_id=:wkf and " +
			 * "wkf_asgn_to <> :aprvr and ctl_no=:ctl_no and wkf_his_id > " +
			 * "(select wkf_his_id from cstm_wkf_map_his where wkf_asgn_to = :aprvr " +
			 * "and ctl_no=:ctl_no order by wkf_his_id limit 1) order by wkf_his_id desc limit 1"
			 * ;
			 * 
			 * queryValidate = session.createSQLQuery(hql);
			 * 
			 * queryValidate.setParameter("wkf", wkfId);
			 * queryValidate.setParameter("ctl_no", ctlNo);
			 * queryValidate.setParameter("aprvr", aprvr);
			 * 
			 * listWkfAprvr = queryValidate.list();
			 * 
			 * for (Object[] wkfAprvr : listWkfAprvr) {
			 * 
			 * nxtAprvr = wkfAprvr[0].toString();
			 * 
			 * } }
			 */

		} catch (Exception e) {

			logger.debug("Workflow Map Information : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return nxtAprvr;
	}

	/* GET WORK FLOW LAST USER DETAILS */
	public String getLastApprover(String aprvr, int wkfId, int ctlNo, String extFlg) {
		String hql = "";
		Session session = null;
		String lstAprvr = "";

		try {

			session = SessionUtil.getSession();

			if (extFlg.equals("E")) {
				hql = "select wkf_asgn_to,1 from cstm_wkf_map_his where wkf_asgn_to <> :aprvr and "
						+ "ctl_no=:ctl_no and wkf_his_id < (select wkf_his_id from cstm_wkf_map_his where wkf_asgn_to = :aprvr and ctl_no=:ctl_no order by wkf_his_id desc limit 1) order by wkf_his_id desc limit 1";
			} else {
				hql = "select wkf_aprvr,1 from cstm_wkf_aprvr where wkf_id=:wkf and "
						+ "wkf_aprv_ordr < (select wkf_aprv_ordr from cstm_wkf_aprvr "
						+ "where wkf_id=:wkf and wkf_aprvr = :aprvr) order by wkf_aprv_ordr desc limit 1";
			}

			Query queryValidate = session.createSQLQuery(hql);

			if (extFlg.equals("E")) {
				queryValidate.setParameter("aprvr", aprvr);
				queryValidate.setParameter("ctl_no", ctlNo);
			} else {
				queryValidate.setParameter("aprvr", aprvr);
				queryValidate.setParameter("wkf", wkfId);
			}

			List<Object[]> listWkfAprvr = queryValidate.list();

			for (Object[] wkfAprvr : listWkfAprvr) {

				lstAprvr = wkfAprvr[0].toString();

			}

		} catch (Exception e) {

			logger.debug("Workflow Map Information : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return lstAprvr;
	}

	/* GET WORK FLOW NEXT USER DETAILS */
	public String setNextPrevApprover(String aprvr, int wkfId, VchrInfo vchrInfo) {
		String hql = "";
		Session session = null;
		String nxtAprvr = "";

		try {

			session = SessionUtil.getSession();

			hql = "select (select count(*) from cstm_wkf_aprvr where wkf_id=:wkf "
					+ "and wkf_aprv_ordr < (select wkf_aprv_ordr from cstm_wkf_aprvr "
					+ "where wkf_aprvr=:aprvr and wkf_id=:wkf)) cur_count, (select count(*) -1 "
					+ "from cstm_wkf_aprvr where wkf_id=:wkf) tot_count, "
					+ "(select count(*) from cstm_wkf_aprvr where wkf_id=:wkf and wkf_aprvr=:aprvr)";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("wkf", wkfId);
			queryValidate.setParameter("aprvr", aprvr);

			List<Object[]> listWkfAprvr = queryValidate.list();

			for (Object[] wkfAprvr : listWkfAprvr) {

				vchrInfo.setWkfBackSts(wkfAprvr[0].toString());
				vchrInfo.setWkfForwSts(wkfAprvr[1].toString());
				vchrInfo.setWkfExtSts(wkfAprvr[2].toString());
			}

		} catch (Exception e) {

			logger.debug("Workflow Map Information : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return nxtAprvr;
	}

	/* GET WORK FLOW NEXT USER DETAILS */
	public String getExternalFlg(String aprvr, int wkfId, int ctlNo) {
		String hql = "";
		Session session = null;
		String rmkTyp = "";

		try {

			session = SessionUtil.getSession();

			hql = "select wkf_rmk_typ,1 from cstm_wkf_map_his cwmh where "
					+ "ctl_no=:ctl_no order by wkf_his_id desc limit 1";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("ctl_no", ctlNo);
			/* queryValidate.setParameter("aprvr", aprvr); */

			List<Object[]> listWkfAprvr = queryValidate.list();

			for (Object[] wkfAprvr : listWkfAprvr) {

				rmkTyp = wkfAprvr[0].toString();
			}

		} catch (Exception e) {

			logger.debug("Workflow Map Information : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return rmkTyp;
	}

	public void delWkfMap(Session session, int ctlNo) throws Exception {
		String hql = "";

		hql = "delete from cstm_wkf_map where ctl_no=:ctl_no";

		Query queryValidate = session.createSQLQuery(hql);
		queryValidate.setParameter("ctl_no", ctlNo);
		queryValidate.executeUpdate();
	}

	public void delWkfMapHis(Session session, int ctlNo) throws Exception {
		String hql = "";

		hql = "delete from cstm_wkf_map_his where ctl_no=:ctl_no";

		Query queryValidate = session.createSQLQuery(hql);
		queryValidate.setParameter("ctl_no", ctlNo);
		queryValidate.executeUpdate();
	}

	public void sendNotification(int ctlNo, int wkfId, String usrId, String nxtAprvr, String lstAprvr, String rmk,
			String wkfSts, String newSts) {
		new Thread(() -> {
			MailService mailService = new MailService();

			JsonObject innerObject = new JsonObject();
			innerObject.addProperty("trnTyp", "wkf");
			innerObject.addProperty("reqId", wkfId);
			innerObject.addProperty("ctlNo", ctlNo);
			innerObject.addProperty("upldUsr", usrId);
			innerObject.addProperty("nxtAprvr", nxtAprvr);
			innerObject.addProperty("lstAprvr", lstAprvr);
			innerObject.addProperty("rmk", rmk);
			innerObject.addProperty("sts", wkfSts);
			innerObject.addProperty("nwSts", newSts);

			try {
				mailService.sendEmailProp(usrId, "", innerObject.toString());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}).start();
	}

}
