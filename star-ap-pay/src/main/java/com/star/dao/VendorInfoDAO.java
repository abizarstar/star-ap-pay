package com.star.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.MaintanenceResponse;
import com.star.common.SessionUtil;
import com.star.common.SessionUtilInformix;
import com.star.common.SessionUtilInformixESP;
import com.star.linkage.common.PaymentMethod;
import com.star.linkage.vendor.CompanyBrowseOutput;
import com.star.linkage.vendor.CompanyInfo;
import com.star.linkage.vendor.VendorBankBrowseOutput;
import com.star.linkage.vendor.VendorBrowseOutput;
import com.star.linkage.vendor.VendorDefaultGlSettingInput;
import com.star.linkage.vendor.VendorDefaultSettingBrowseOutput;
import com.star.linkage.vendor.VendorDefaultSettingInput;
import com.star.linkage.vendor.VendorEmailSetupBrowseOutput;
import com.star.linkage.vendor.VendorEmailSetupInput;
import com.star.linkage.vendor.VendorEmailSetupOutput;
import com.star.linkage.vendor.VendorInfo;
import com.star.linkage.vendor.VendorInput;
import com.star.linkage.vendor.VendorManOutput;
import com.star.linkage.vendor.VendorOutput;
import com.star.modal.CstmOcrVndrDfltSetg;
import com.star.modal.CstmOcrVndrGlSetg;
import com.star.modal.CstmVenAddInfo;
import com.star.modal.CstmVenBnkInfo;
import com.star.modal.CstmVenEmailSetup;
import com.star.modal.VenPayMthd;

public class VendorInfoDAO {

	private static Logger logger = LoggerFactory.getLogger(UserDAO.class);

	public void readVendorList(BrowseResponse<VendorBrowseOutput> starBrowseResponse, String cmpyId) {
		Session session = null;
		Transaction tx = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = " select cast(ven_cmpy_id as varchar(3)) cmpy_id, cast(ven_ven_id as varchar(8)) ven_id, "
					+ "cast(ven_ven_nm as varchar(15)) ven_nm, cast(ven_ven_long_nm as varchar(35)) ven_long_nm, "
					+ "cast(ven_cry as varchar(3))cry, cast(ven_pmt_typ as varchar(1)) pmt_typ, "
					+ "cast(ven_admin_brh as varchar(3)) admin_brh from aprven_rec where ven_cmpy_id=:cmpy and ven_actv=1";
			
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy", cmpyId);

			List<Object[]> listVendor = queryValidate.list();

			for (Object[] vendor : listVendor) {

				VendorInfo output = new VendorInfo();

				output.setCmpyId(vendor[0].toString());
				output.setVenId(vendor[1].toString());
				output.setVenNm(vendor[2].toString());
				output.setVenLongNm(vendor[3].toString());
				output.setVenCry(vendor[4].toString());
				output.setPmtTyp(vendor[5].toString());
				output.setVenAdminBrh(vendor[6].toString());

				starBrowseResponse.output.fldTblVendor.add(output);

			}

		} catch (Exception e) {
			logger.debug("Vendor Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			session.close();
		}

	}

	public void readVendorListESP(BrowseResponse<VendorBrowseOutput> starBrowseResponse, String cmpyId) {
		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformixESP.getSession();
			//session = SessionUtilInformix.getSession();
			/*hql = " select cast(ven_cmpy_id as varchar(3)) cmpy_id, cast(ven_ven_id as varchar(8)) ven_id, "
					+ "cast(ven_ven_nm as varchar(15)) ven_nm, cast(ven_ven_long_nm as varchar(35)) ven_long_nm, "
					+ "cast(ven_cry as varchar(3))cry, cast(ven_pmt_typ as varchar(1)) pmt_typ, "
					+ "cast(ven_admin_brh as varchar(3)) admin_brh from aprven_rec where ven_cmpy_id=:cmpy and ven_actv=1";
			*/
			hql = " select cast(ven_ven_no as varchar(8)) ven_id, cast(ven_ven_no as varchar(8)) ven_id, "
					+ "cast(ven_lkp_nm as varchar(15)) ven_nm, cast(ven_nm as varchar(35)) ven_long_nm, "
					+ "cast(ven_cry as varchar(3))cry, cast(ven_ven_cl as varchar(1)) pmt_typ, "
					+ "cast(ven_brh as varchar(3)) admin_brh from aprven_rec";

			
			Query queryValidate = session.createSQLQuery(hql);

			//queryValidate.setParameter("cmpy", cmpyId);

			List<Object[]> listVendor = queryValidate.list();

			for (Object[] vendor : listVendor) {

				VendorInfo output = new VendorInfo();

				output.setCmpyId(vendor[0].toString());
				output.setVenId(vendor[1].toString());
				output.setVenNm(vendor[2].toString());
				output.setVenLongNm(vendor[3].toString());
				output.setVenCry(vendor[4].toString());
				output.setPmtTyp(vendor[5].toString());
				output.setVenAdminBrh(vendor[6].toString());

				starBrowseResponse.output.fldTblVendor.add(output);

			}

		} catch (Exception e) {
			logger.debug("Vendor Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			session.close();
		}

	}
	
	public VendorInfo readVendorInfoESP(String cmpyId, String venId) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		VendorInfo output=null;

		try {
			session = SessionUtilInformixESP.getSession();
			//session = SessionUtilInformix.getSession();
			/*hql = " select cast(ven_cmpy_id as varchar(3)) cmpy_id, cast(ven_ven_id as varchar(8)) ven_id, "
					+ "cast(ven_ven_nm as varchar(15)) ven_nm, cast(ven_ven_long_nm as varchar(35)) ven_long_nm, "
					+ "cast(ven_cry as varchar(3))cry, cast(ven_pmt_typ as varchar(1)) pmt_typ, "
					+ "cast(ven_admin_brh as varchar(3)) admin_brh from aprven_rec where ven_cmpy_id=:cmpy and ven_actv=1";
			*/
			hql = " select cast(ven_ven_no as varchar(15)) ven_id, cast(ven_ven_no as varchar(15)) ven_id, "
					+ "cast(ven_pt_nm as varchar(60)) ven_nm, cast(ven_nm as varchar(60)) ven_long_nm, "
					+ "cast(ven_cry as varchar(3))cry, cast(ven_ven_cl as varchar(1)) pmt_typ, "
					+ "cast(ven_brh as varchar(3)) admin_brh, "
					+ "cast(ven_addr1 as varchar(55)) venadd1, cast(ven_addr2 as varchar(55)) venadd2, cast(ven_addr3 as varchar(55)) venadd3, "
					+ "cast(ven_city as varchar(30)) venCity, cast(ven_pcd as varchar(15)) venPCD, cast(ven_sls_cntc as varchar(35)) venContName, "
					+ "cast(ven_fax_no as varchar(15)) venFaxNo, cast(ven_lng_cd as varchar(2)) venLang  "
					+ "from aprven_rec where cast(ven_ven_no as varchar(15))=:vnid";
			
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("vnid", venId);

			List<Object[]> listVendor = queryValidate.list();
			
			for (Object[] vendor : listVendor) {

				output = new VendorInfo();

				output.setCmpyId(vendor[0].toString());
				output.setVenId(vendor[1].toString());
				output.setVenNm(vendor[2].toString());
				output.setVenLongNm(vendor[3].toString());
				output.setVenCry(vendor[4].toString());
				output.setPmtTyp(vendor[5].toString());
				output.setVenAdminBrh(vendor[6].toString());
				output.setVenAdd1(vendor[7].toString());
				output.setVenAdd2(vendor[8].toString());
				output.setVenAdd3(vendor[9].toString());
				output.setVenCity(vendor[10].toString());
				output.setVenPostCode(vendor[11].toString());
				output.setVenContName(vendor[12].toString());
				output.setVenFaxNo(vendor[13].toString());
				output.setVenLang(vendor[14].toString());
			}

		} catch (Exception e) {
			logger.debug("Vendor Info : {}", e.getMessage(), e);
			
		} finally {
			session.close();
		}
		return output;

	}

	public void readCompanyList(BrowseResponse<CompanyBrowseOutput> starBrowseResponse) {
		Session session = null;
		Transaction tx = null;
		String hql = "";

		try {
			
			session = SessionUtilInformixESP.getSession();

			//hql = " select cast(csc_cmpy_id as varchar(3)) cmpy_id, cast(csc_co_nm as varchar(35)) co_nm from scrcsc_rec";
			hql = " select cast(csc_key_cd as varchar(3)) cmpy_id, cast(csc_nm as varchar(35)) co_nm from scrcsc_rec";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listCompany = queryValidate.list();

			for (Object[] company : listCompany) {

				CompanyInfo output = new CompanyInfo();

				output.setCmpyId(company[0].toString());
				output.setCmpyNm(company[1].toString());
				starBrowseResponse.output.fldTblCompany.add(output);

			}

		} catch (Exception e) {
			logger.debug("Vendor Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			session.close();
		}

	}

	public void addVendorInfo(MaintanenceResponse<VendorManOutput> maintanenceResponse, VendorEmailSetupInput vendorEmailSetupInput) {
		Session session = null;
		Transaction tx = null;
		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			CstmVenEmailSetup emailSetup = new CstmVenEmailSetup();

			emailSetup.setVenId(vendorEmailSetupInput.getVenId());
			emailSetup.setSubject(vendorEmailSetupInput.getSubject());
			emailSetup.setBody(vendorEmailSetupInput.getBody());

			session.save(emailSetup);

			tx.commit();
		} catch (Exception e) {
			logger.debug("Vendor Email Setup Info : {}", e.getMessage(), e);
			maintanenceResponse.output.messages.add(e.getMessage());
			maintanenceResponse.output.rtnSts = 1;

			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void addVendorInfo(MaintanenceResponse<VendorManOutput> maintanenceResponse, VendorInput vendorInput) {
		Session session = null;
		Transaction tx = null;
		try {
			Date date = new Date();

			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			CstmVenAddInfo addInfo = new CstmVenAddInfo();

			addInfo.setCmpyId(vendorInput.getCmpyId());
			addInfo.setVenId(vendorInput.getVenId());
			addInfo.setPayMthd(vendorInput.getPymntMtd());
			addInfo.setVenNm(vendorInput.getVenNm());
			addInfo.setVenLongNm(vendorInput.getVenLongNm());
			addInfo.setCry(vendorInput.getCry());
			addInfo.setCty(vendorInput.getCty());
			addInfo.setCrtdBy(vendorInput.getCrtdBy());
			addInfo.setCrtdDtts(date);

			session.save(addInfo);

			for (int i = 0; i < vendorInput.getBankList().size(); i++) {
				CstmVenBnkInfo bnkInfo = new CstmVenBnkInfo();

				bnkInfo.setCmpyId(vendorInput.getCmpyId());
				bnkInfo.setVenId(vendorInput.getVenId());
				bnkInfo.setBnkNm(vendorInput.getBankList().get(i).getAcctNm());
				bnkInfo.setBnkAccNo(vendorInput.getBankList().get(i).getAcctNo());
				bnkInfo.setBnkKey(vendorInput.getBankList().get(i).getBnkKey());
				bnkInfo.setExtRef(vendorInput.getBankList().get(i).getExtRef());

				session.save(bnkInfo);
			}

			for (int i = 0; i < vendorInput.getPymntMtdList().size(); i++) {
				VenPayMthd venPayMthd = new VenPayMthd();

				venPayMthd.setCmpyId(vendorInput.getCmpyId());
				venPayMthd.setVenId(vendorInput.getVenId());
				venPayMthd.setPmtMthdId(Integer.parseInt(vendorInput.getPymntMtdList().get(i).getMthdId()));
				venPayMthd.setPmtDflt(vendorInput.getPymntMtdList().get(i).getMthdDflt());

				session.save(venPayMthd);
			}
			tx.commit();
		} catch (Exception e) {
			logger.debug("Vendor Info : {}", e.getMessage(), e);
			maintanenceResponse.output.messages.add(e.getMessage());
			maintanenceResponse.output.rtnSts = 1;

			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}	
	
	public void updateVendorInfo(MaintanenceResponse<VendorManOutput> starManResponse, VendorInput vendorInput) {

		deleteVendor(starManResponse, vendorInput);

		if (starManResponse.output.rtnSts == 0) {
			addVendorInfo(starManResponse, vendorInput);
		}

	}

	public void updateVendorInfo(MaintanenceResponse<VendorManOutput> starManResponse, VendorEmailSetupInput vendorEmailSetupInput) {

		deleteVendor(starManResponse, vendorEmailSetupInput);

		if (starManResponse.output.rtnSts == 0) {
			addVendorInfo(starManResponse, vendorEmailSetupInput);
		}

	}
	
	
	public void deleteVendor(MaintanenceResponse<VendorManOutput> starManResponse, VendorInput vendorInput) {
		Session session = null;
		Transaction tx = null;
		String hql = "";

		try {

			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			hql = "delete from cstm_ven_add_info where cmpy_id=:cmpy and ven_id=:ven";
			Query queryValidate = session.createSQLQuery(hql);
			queryValidate.setParameter("cmpy", vendorInput.getCmpyId());
			queryValidate.setParameter("ven", vendorInput.getVenId());
			queryValidate.executeUpdate();

			hql = " delete from cstm_ven_bnk_info where cmpy_id=:cmpy and ven_id=:ven";
			queryValidate = session.createSQLQuery(hql);
			queryValidate.setParameter("cmpy", vendorInput.getCmpyId());
			queryValidate.setParameter("ven", vendorInput.getVenId());
			queryValidate.executeUpdate();

			hql = " delete from ven_pay_mthd where cmpy_id=:cmpy and ven_id=:ven";
			queryValidate = session.createSQLQuery(hql);
			queryValidate.setParameter("cmpy", vendorInput.getCmpyId());
			queryValidate.setParameter("ven", vendorInput.getVenId());
			queryValidate.executeUpdate();

			tx.commit();

		} catch (Exception e) {
			logger.debug("Vendor Info : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(CommonConstants.SERVER_ERR);

			tx.rollback();
		} finally {

			session.close();
		}
	}

	public void deleteVendor(MaintanenceResponse<VendorManOutput> starManResponse, VendorEmailSetupInput vendorEmailSetupInput) {
		Session session = null;
		Transaction tx = null;
		String hql = "";

		try {

			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			hql = "delete from vndr_eml_confg where ven_id=:ven";
			Query queryValidate = session.createSQLQuery(hql);
			//queryValidate.setParameter("cmpy", vendorInput.getCmpyId());
			queryValidate.setParameter("ven", vendorEmailSetupInput.getVenId());
			queryValidate.executeUpdate();		

			tx.commit();

		} catch (Exception e) {
			logger.debug("Vendor Info : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(CommonConstants.SERVER_ERR);

			tx.rollback();
		} finally {

			session.close();
		}
	}	
	
	public int validateVendor(VendorInput vendorInput) {

		Session session = null;
		String hql = "";
		int ircrdCount = 0;

		try {
			session = SessionUtil.getSession();

			hql = "select count(*) from cstm_ven_add_info where cmpy_id=:cmpy and ven_id=:ven";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy", vendorInput.getCmpyId());
			queryValidate.setParameter("ven", vendorInput.getVenId());

			ircrdCount = Integer.parseInt(String.valueOf(queryValidate.list().get(0)));

		} catch (Exception e) {
			logger.debug("Vendor Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}

		return ircrdCount;
	}
	
	public int validateVendor(VendorEmailSetupInput vendorEmailSetupInput) {

		Session session = null;
		String hql = "";
		int ircrdCount = 0;

		try {
			session = SessionUtil.getSession();

			hql = "select count(*) from vndr_eml_confg where ven_id=:ven";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("ven", vendorEmailSetupInput.getVenId());

			ircrdCount = Integer.parseInt(String.valueOf(queryValidate.list().get(0)));

		} catch (Exception e) {
			logger.debug("Vendor Email Setup Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}

		return ircrdCount;
	}
	

	public int validateVendorAccount(String cmpyId, String venId, String accNo) {

		Session session = null;
		String hql = "";
		int ircrdCount = 0;

		try {
			session = SessionUtil.getSession();

			hql = "select count(*) from cstm_ven_bnk_info where cmpy_id=:cmpy and ven_id=:ven and bnk_acc_no=:acc_no";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy", cmpyId);
			queryValidate.setParameter("ven", venId);
			queryValidate.setParameter("acc_no", accNo);

			ircrdCount = Integer.parseInt(String.valueOf(queryValidate.list().get(0)));

		} catch (Exception e) {
			logger.debug("Vendor Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return ircrdCount;
	}

	public void readVendorInfo(BrowseResponse<VendorBankBrowseOutput> starBrowseResponse, String cmpyId, String venId) {
		Session session = null;
		String hql = "";

		GetCommonDAO commonDAO = new GetCommonDAO();

		try {
			session = SessionUtil.getSession();

			hql = " select * from cstm_ven_add_info where 1=1";

			if (cmpyId.length() > 0) {
				hql = hql + " and cmpy_id = :cmpy ";
			}

			if (venId.length() > 0) {
				hql = hql + " and ven_id=:ven";
			}

			Query queryValidate = session.createSQLQuery(hql);

			if (cmpyId.length() > 0) {
				queryValidate.setParameter("cmpy", cmpyId);
			}

			if (venId.length() > 0) {
				queryValidate.setParameter("ven", venId);
			}

			List<Object[]> listVendorAdd = queryValidate.list();

			for (Object[] vendorAdd : listVendorAdd) {

				VendorOutput output = new VendorOutput();

				output.setCmpyId(vendorAdd[0].toString());
				output.setVenId(vendorAdd[1].toString());
				output.setVenNm(vendorAdd[2].toString());
				output.setVenLongNm(vendorAdd[3].toString());
				output.setPayMthd(vendorAdd[4].toString());
				output.setCry(vendorAdd[5].toString());
				output.setCty(vendorAdd[6].toString());
				output.setCrtdBy(vendorAdd[7].toString());
				output.setCrtdOn((Date) vendorAdd[8]);

				readVendorSpecific(output, output.getCmpyId(), output.getVenId(), session);

				readVendorPaymentMethod(output, output.getCmpyId(), output.getVenId(), session);

				starBrowseResponse.output.fldTblVendor.add(output);

			}

		} catch (Exception e) {
			logger.debug("Vendor Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
			e.printStackTrace();
		} finally {
			session.close();
		}

	}

	public void readVendorEmailSetup(BrowseResponse<VendorEmailSetupBrowseOutput> starBrowseResponse, String cmpyId, String venId) {
		Session session = null;
		String hql = "";
		try {
			session = SessionUtil.getSession();

			hql = " select * from vndr_eml_confg where 1=1";

			//if (cmpyId.length() > 0) {
			//	hql = hql + " and cmpy_id = :cmpy ";
			//}

			if (venId.length() > 0) {
				hql = hql + " and ven_id=:ven";
			}

			Query queryValidate = session.createSQLQuery(hql);

			//if (cmpyId.length() > 0) {
			//	queryValidate.setParameter("cmpy", cmpyId);
			//}

			if (venId.length() > 0) {
				queryValidate.setParameter("ven", venId);
			}

			List<Object[]> listVendorES = queryValidate.list();

			for (Object[] vendorES : listVendorES) {

				VendorEmailSetupOutput output = new VendorEmailSetupOutput();

				//output.setCmpyId(vendorAdd[0].toString());
				output.setVenId(vendorES[0].toString());
				output.setSubject(vendorES[1].toString());
				output.setBody(vendorES[2].toString());

				starBrowseResponse.output.fldTblVendorES.add(output);

			}

		} catch (Exception e) {
			logger.debug("Vendor Email Setup Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
			e.printStackTrace();
		} finally {
			session.close();
		}

	}
	
	
	public void readVendorSpecific(VendorOutput vendorOuput, String cmpyId, String venId, Session session) {
		String hql = "";

		try {

			hql = " select * from cstm_ven_bnk_info where 1=1";

			if (cmpyId.length() > 0 && venId.length() > 0) {
				hql = hql + " and cmpy_id = :cmpy and ven_id=:ven";
			}

			Query queryValidate = session.createSQLQuery(hql);

			if (cmpyId.length() > 0 && venId.length() > 0) {
				queryValidate.setParameter("cmpy", cmpyId);
				queryValidate.setParameter("ven", venId);
			}

			List<Object[]> listVendor = queryValidate.list();

			List<CstmVenBnkInfo> bnkAcctInfos = new ArrayList<CstmVenBnkInfo>();

			for (Object[] vendor : listVendor) {

				CstmVenBnkInfo output = new CstmVenBnkInfo();

				output.setCmpyId(vendor[0].toString());
				output.setVenId(vendor[1].toString());
				output.setBnkNm(vendor[2].toString());
				output.setBnkAccNo(vendor[3].toString());
				output.setBnkKey(vendor[4].toString());
				output.setExtRef(vendor[5].toString());

				bnkAcctInfos.add(output);

			}

			vendorOuput.setBnkList(bnkAcctInfos);

		} catch (Exception e) {
			logger.debug("Vendor Info : {}", e.getMessage(), e);

		}

	}

	public void readVendorPaymentMethod(VendorOutput vendorOutput, String cmpyId, String venId, Session session) {
		String hql = "";

		try {

			hql = "SELECT id, mthd_cd, mthd_nm, pmt_dflt FROM ven_pay_mthd, pay_mthd WHERE pmt_mthd_id = id ";

			if (cmpyId.length() > 0 && venId.length() > 0) {
				hql = hql + " AND cmpy_id = :cmpy AND ven_id=:ven";
			}

			Query queryValidate = session.createSQLQuery(hql);

			if (cmpyId.length() > 0 && venId.length() > 0) {
				queryValidate.setParameter("cmpy", cmpyId);
				queryValidate.setParameter("ven", venId);
			}

			List<Object[]> listMethods = queryValidate.list();

			List<PaymentMethod> vndrPymntMthds = new ArrayList<PaymentMethod>();

			for (Object[] method : listMethods) {

				PaymentMethod output = new PaymentMethod();

				output.setMthdId(method[0].toString());
				output.setMthdCd(method[1].toString());
				output.setMthdNm(method[2].toString());
				output.setMthdDflt(Boolean.parseBoolean(method[3].toString()));

				vndrPymntMthds.add(output);

			}

			vendorOutput.setPayMthdList(vndrPymntMthds);

		} catch (Exception e) {
			logger.debug("Vendor Info : {}", e.getMessage(), e);

		}

	}

	public void saveVendorDefaultSetting(MaintanenceResponse<VendorManOutput> starManResponse,
			VendorDefaultSettingInput vndrDfltInput) {

		addVendorDefaultSetting(starManResponse, vndrDfltInput);

	}

	public void deleteVendorDefaultSetting(Session session, VendorDefaultSettingInput vndrDfltInput) throws Exception {
		String hql = "";

		hql = "DELETE FROM cstm_ocr_vndr_dflt_setg WHERE cmpy_id=:cmpy AND ven_id=:ven";
		Query queryValidate = session.createSQLQuery(hql);
		queryValidate.setParameter("cmpy", vndrDfltInput.getCmpyId());
		queryValidate.setParameter("ven", vndrDfltInput.getVenId());
		queryValidate.executeUpdate();

		hql = "DELETE FROM cstm_ocr_vndr_gl_setg WHERE cmpy_id=:cmpy AND ven_id=:ven";
		queryValidate = session.createSQLQuery(hql);
		queryValidate.setParameter("cmpy", vndrDfltInput.getCmpyId());
		queryValidate.setParameter("ven", vndrDfltInput.getVenId());
		queryValidate.executeUpdate();
	}

	public void addVendorDefaultSetting(MaintanenceResponse<VendorManOutput> maintanenceResponse,
			VendorDefaultSettingInput vndrDfltInput) {
		Session session = null;
		Transaction tx = null;
		try {
			Date date = new Date();

			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			deleteVendorDefaultSetting(session, vndrDfltInput);

			CstmOcrVndrDfltSetg addInfo = new CstmOcrVndrDfltSetg();

			addInfo.setCmpyId(vndrDfltInput.getCmpyId());
			addInfo.setVenId(vndrDfltInput.getVenId());
			addInfo.setVchrBrh(vndrDfltInput.getVchrBrh());
			addInfo.setVchrCat(vndrDfltInput.getVchrCat());
			addInfo.setTrsStsActn(vndrDfltInput.getTrsStsActn());
			addInfo.setTrsSts(vndrDfltInput.getTrsSts());
			addInfo.setTrsRsn(vndrDfltInput.getTrsRsn());
			addInfo.setNtfUsr(vndrDfltInput.getNtfUsr());
			addInfo.setIsActive(Integer.parseInt(vndrDfltInput.getIsActive()));
			addInfo.setCrtdDtts(date);
			addInfo.setCrtdBy(vndrDfltInput.getCrtdBy());

			session.save(addInfo);

			for (int i = 0; i < vndrDfltInput.getGlAcctList().size(); i++) {
				CstmOcrVndrGlSetg glInfo = new CstmOcrVndrGlSetg();

				glInfo.setCmpyId(vndrDfltInput.getCmpyId());
				glInfo.setVenId(vndrDfltInput.getVenId());
				glInfo.setBscGlAcct(vndrDfltInput.getGlAcctList().get(i).getBscGlAcct());
				glInfo.setSacct(vndrDfltInput.getGlAcctList().get(i).getSacct());
				glInfo.setSacctTxt(vndrDfltInput.getGlAcctList().get(i).getSacctTxt());
				glInfo.setDistRmk(vndrDfltInput.getGlAcctList().get(i).getDistRmk());

				session.save(glInfo);
			}

			tx.commit();
		} catch (Exception e) {
			logger.debug("Vendor Info : {}", e.getMessage(), e);
			maintanenceResponse.output.messages.add(e.getMessage());
			maintanenceResponse.output.rtnSts = 1;

			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void readVendorDefaultSetting(BrowseResponse<VendorDefaultSettingBrowseOutput> starBrowseResponse,
			String cmpyId, String venId, String userId) {
		Session session = null;
		String hql = "";

		VendorDefaultSettingInput output = new VendorDefaultSettingInput();

		GetCommonDAO commonDAO = new GetCommonDAO();

		try {
			session = SessionUtil.getSession();

			hql = " SELECT * FROM cstm_ocr_vndr_dflt_setg WHERE 1=1 ";

			if (cmpyId.length() > 0) {
				hql = hql + " AND cmpy_id = :cmpy ";
			}

			if (venId.length() > 0) {
				hql = hql + " and ven_id=:ven";
			}

			Query queryValidate = session.createSQLQuery(hql);

			if (cmpyId.length() > 0) {
				queryValidate.setParameter("cmpy", cmpyId);
			}

			if (venId.length() > 0) {
				queryValidate.setParameter("ven", venId);
			}

			List<Object[]> listVndrDfltStng = queryValidate.list();

			for (Object[] vndrDfltStng : listVndrDfltStng) {

				if (venId.length() == 0) {
					output = new VendorDefaultSettingInput();
				}

				output.setCmpyId(vndrDfltStng[0].toString());
				output.setVenId(vndrDfltStng[1].toString());
				output.setVchrBrh(vndrDfltStng[2].toString());
				output.setVchrCat(vndrDfltStng[3].toString());
				output.setTrsStsActn(vndrDfltStng[4].toString());
				output.setTrsSts(vndrDfltStng[5].toString());
				output.setTrsRsn(vndrDfltStng[6].toString());
				output.setNtfUsr(vndrDfltStng[7].toString());
				output.setIsActive(vndrDfltStng[8].toString());
				// output.setCrtdOn((Date)vndrDfltStng[9]);
				output.setCrtdBy(vndrDfltStng[10].toString());

				readVendorGlData(output, output.getCmpyId(), output.getVenId(), session);

				if (venId.length() == 0) {
					starBrowseResponse.output.fldTblVndrDfltStng.add(output);
				}
			}

			if (venId.length() > 0) {
				readStxVendorDftSetting(output, cmpyId, venId, userId);

				starBrowseResponse.output.fldTblVndrDfltStng.add(output);
			}

		} catch (Exception e) {
			logger.debug("Vendor Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
			e.printStackTrace();
		} finally {
			session.close();
		}

	}

	public void readStxVendorDftSetting(VendorDefaultSettingInput output, String cmpyId, String venId, String userId) {
		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = " select cast(pyt_desc30 as varchar(30)) pyt_trm, "
					+ "cast((select ven_cry from aprven_rec where ven_cmpy_id=:cmpy and ven_ven_id=:ven) as varchar(3)) ven_cry,  "
					+ " cast((select usr_usr_brh from mxrusr_rec where usr_lgn_id=:usr) as varchar(3)) usr_brh,"
					+ " cast((select ven_admin_brh from aprven_rec where ven_cmpy_id=:cmpy and ven_ven_id=:ven) as varchar(3)) usr_brh_adm"
					+ " from aprshf_rec, scrpyt_rec where shf_pttrm = pyt_pttrm and shf_shp_fm=0 ";

			if (cmpyId.length() > 0) {
				hql = hql + " AND shf_cmpy_id = :cmpy ";
			}

			if (venId.length() > 0) {
				hql = hql + " and shf_ven_id=:ven";
			}

			Query queryValidate = session.createSQLQuery(hql);

			if (cmpyId.length() > 0) {
				queryValidate.setParameter("cmpy", cmpyId);
			}

			if (venId.length() > 0) {
				queryValidate.setParameter("ven", venId);
			}
			
			queryValidate.setParameter("usr", userId);

			List<Object[]> listVndrDfltStng = queryValidate.list();

			for (Object[] vndrDfltStng : listVndrDfltStng) {

				if(vndrDfltStng[0] != null)
				{
					output.setPayTerm(vndrDfltStng[0].toString());
				}
				else
				{
					output.setPayTerm("");
				}
				
				if(vndrDfltStng[1] != null)
				{
					output.setCry(vndrDfltStng[1].toString());
				}
				else
				{
					output.setCry("");
				}
				
				if(vndrDfltStng[2] != null)
				{
					output.setVchrBrh(vndrDfltStng[2].toString());
				}
				else
				{
					output.setVchrBrh("");
				}
				
				if(vndrDfltStng[3] != null)
				{
					if(output.getVchrBrh().length() == 0)
					{
						output.setVchrBrh(vndrDfltStng[3].toString());
					}
				}

			}

		} catch (Exception e) {
			logger.debug("Vendor Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}

	public void readVendorGlData(VendorDefaultSettingInput vendorOuput, String cmpyId, String venId, Session session) {
		String hql = "";

		try {

			hql = " SELECT * FROM cstm_ocr_vndr_gl_setg WHERE 1=1";

			if (cmpyId.length() > 0 && venId.length() > 0) {
				hql = hql + " AND cmpy_id = :cmpy AND ven_id=:ven";
			}

			Query queryValidate = session.createSQLQuery(hql);

			if (cmpyId.length() > 0 && venId.length() > 0) {
				queryValidate.setParameter("cmpy", cmpyId);
				queryValidate.setParameter("ven", venId);
			}

			List<Object[]> listGlData = queryValidate.list();

			List<VendorDefaultGlSettingInput> vndrGlData = new ArrayList<VendorDefaultGlSettingInput>();

			for (Object[] glData : listGlData) {

				VendorDefaultGlSettingInput output = new VendorDefaultGlSettingInput();

				output.setCmpyId(glData[0].toString());
				output.setVenId(glData[1].toString());
				output.setBscGlAcct(glData[2].toString());
				output.setSacct(glData[3].toString());
				output.setSacctTxt(glData[4].toString());
				output.setDistRmk(glData[5].toString());

				vndrGlData.add(output);

			}

			vendorOuput.setGlAcctList(vndrGlData);

		} catch (Exception e) {
			logger.debug("Vendor Info : {}", e.getMessage(), e);

		}

	}

}
