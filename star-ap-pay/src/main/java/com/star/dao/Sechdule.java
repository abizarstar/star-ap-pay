package com.star.dao;

import java.util.Date;
import java.util.List;
import java.util.function.Consumer;

import org.quartz.CronScheduleBuilder;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.quartz.impl.StdSchedulerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.linkage.job.Job15Sec;
import com.star.linkage.job.JobBrowseOutput;
import com.star.linkage.job.JobBrowseResponse;

public class Sechdule {
	TriggerKey tgrl;
	private static Logger logger = LoggerFactory.getLogger(Sechdule.class);
	String exp = "";

	public void job(BrowseResponse<JobBrowseOutput> starBrowseResponse, String cron) {

		// exp = time + " * * * * ?";
		exp = cron;
		//exp= "0/2 * * * * ? *";
		String s = new Date().toInstant().toString();
		String nm = s.substring(s.length() - 4)+"* * * * ?";
		System.out.println(">>>>>>>>>>>> "+cron+"  The file name in date format " + s.substring(s.length() - 4));
		String tgrnm = "tgrnm" + nm;
		String tgrgrp = "tgrgrp" + nm;
		try {
			Scheduler scheduler = new StdSchedulerFactory("quartz.properties").getScheduler();
			JobKey jobkey = new JobKey("getStmt", "JOB_GET_STMT");
			JobDetail jd = JobBuilder.newJob(Job15Sec.class).withIdentity(jobkey).build();
			Trigger tgr = TriggerBuilder.newTrigger().withIdentity(tgrnm, tgrgrp)
					.withSchedule(CronScheduleBuilder.cronSchedule(exp)).build();

			if (scheduler.isStarted()) {

				TriggerKey oldtgr = new TriggerKey("tgr1", "tgrgrp1");
				List<? extends Trigger> tgrt = scheduler.getTriggersOfJob(jobkey);

				tgrt.forEach(new Consumer<Trigger>() {
					@Override
					public void accept(Trigger tgy) {
						tgrl = tgy.getKey();
					}
				});
				scheduler.rescheduleJob(tgrl, tgr);
			} else {
				scheduler.start();
				scheduler.scheduleJob(jd, tgr);
			}
		} catch (SchedulerException e) {
			logger.error("Error getting scheduler status", e);
		}
	}

}