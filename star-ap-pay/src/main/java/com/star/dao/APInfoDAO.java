package com.star.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.CommonFunctions;
import com.star.common.SessionUtil;
import com.star.common.SessionUtilInformix;
import com.star.linkage.ap.APDesCheckInfoBrowseOutput;
import com.star.linkage.ap.APDesbCheckInfo;
import com.star.linkage.ap.APDisbursement;
import com.star.linkage.ap.APDisbursementBrowseOutput;
import com.star.linkage.ap.APEnquiryBrowseOutput;
import com.star.linkage.ap.APEnquiryInfo;
import com.star.linkage.ap.CheckViewBrowseOutput;
import com.star.linkage.ap.CheckViewInfo;
import com.star.linkage.ap.OpenItemBrowseOutput;
import com.star.linkage.ap.OpenItemInfo;

public class APInfoDAO {

	private static Logger logger = LoggerFactory.getLogger(APInfoDAO.class);

	CommonFunctions cmn = new CommonFunctions();

	public void readAPSession(BrowseResponse<APEnquiryBrowseOutput> starBrowseResponse, String cmpyId) {

		logger.info("AP Info : {}", new Object() {
		}.getClass().getEnclosingMethod().getName());

		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();
			/*
			 * hql =
			 * "select CAST(dss_cmpy_id  AS VARCHAR(3)) cmpy_id, dss_ssn_id AS ssn_id, CAST(dss_dsb_brh  AS VARCHAR(3)) dsb_brh, "
			 * +
			 * "dss_jrnl_dt  AS jrnl_dt, CAST(dss_lgn_id    AS VARCHAR(8)) lgn_id, CAST(dss_bnk AS VARCHAR(3)) bnk,"
			 * +
			 * " CAST(dss_dsb_cry  AS VARCHAR(3)) dsb_cry, dss_dsb_exrt  AS dsb_exrt, CAST(dss_ap_cry1  AS VARCHAR(3))ap_cry1,"
			 * +
			 * " dss_ap_xexrt1  AS dsb_xexrt1, CAST(dss_ap_cry2  AS VARCHAR(3))ap_cry2,dss_ap_xexrt2  AS dsb_xexrt2,"
			 * +
			 * "  CAST(dss_ap_cry3  AS VARCHAR(3))ap_cry3,dss_ap_xexrt3  AS dsb_xexrt3 from aptdss_rec"
			 * ;
			 */

			hql = "SELECT CAST(dss_cmpy_id AS VARCHAR(3)) cmpy_id, dss_ssn_id AS ssn_id, "
					+ " CAST(dss_dsb_brh AS VARCHAR(3)) dsb_brh, dss_jrnl_dt AS jrnl_dt, "
					+ " CAST(dss_lgn_id AS VARCHAR(8)) lgn_id,        CAST(dss_bnk AS VARCHAR(3)) bnk,        "
					+ " CAST(dss_dsb_cry AS VARCHAR(3)) dsb_cry, dss_dsb_exrt, CAST(dss_ap_cry1 AS VARCHAR(3)) ap_cry, dss_ap_xexrt1, "
					+ " count(*) rcd_cnt, sum(dsh_ven_chk_amt) amt"
					+ " FROM aptdss_rec right outer join aptdsh_rec on dss_cmpy_id=dsh_cmpy_id and "
					+ " dss_ssn_id=dsh_ssn_id WHERE dss_cmpy_id=:cmpy_id group by 1,2,3,4,5,6,7,8,9,10 order by dss_ssn_id";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", cmpyId);

			List<Object[]> listEnquiry = queryValidate.list();

			for (Object[] enquiry : listEnquiry) {

				APEnquiryInfo output = new APEnquiryInfo();
				output.setCmpyId(enquiry[0].toString());
				output.setSsnId(enquiry[1].toString());
				output.setDsbBrh(enquiry[2].toString());
				if (enquiry[3] != null) {
					SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
					String formatDate = formatter.format((Date) enquiry[3]);

					output.setJrnlDt(formatDate);
				} else {
					output.setJrnlDt("");
				}
				output.setLgnId(enquiry[4].toString());
				output.setDssBnk(enquiry[5].toString());
				output.setDsbCry(enquiry[6].toString());
				output.setDsbExrt(cmn.formatAmount(Double.parseDouble(enquiry[7].toString())));
				output.setApCry1(enquiry[8].toString());
				output.setApXexrt1(cmn.formatAmount(Double.parseDouble(enquiry[9].toString())));
				output.setDisbNo(Integer.parseInt(enquiry[10].toString()));
				output.setDisbAmt(cmn.formatAmount(Double.parseDouble(enquiry[11].toString())));

				starBrowseResponse.output.fldTblAPEnqry.add(output);
			}

		} catch (Exception e) {
			// TODO: handle exception
			logger.debug("AP Info : {}", e.getMessage(), e);

			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
			e.printStackTrace();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void readAPDisbursement(BrowseResponse<APDisbursementBrowseOutput> starBrowseResponse, String cmpyId,
			String ssnId) {

		logger.info("AP Info : {}", new Object() {
		}.getClass().getEnclosingMethod().getName());

		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();
			hql = "select CAST(dsh_cmpy_id AS VARCHAR(3)) cmpy_id, CAST(dsh_dsb_pfx AS VARCHAR(2)) dsb_pfx, dsh_dsb_no AS dsb_no, dsh_ssn_id AS ssn_id, "
					+ "CAST(dsh_ven_id AS VARCHAR(8)) ven_id, CAST(ven_ven_nm AS VARCHAR(15)) ven_nm,"
					+ "CAST(dsh_pmt_typ AS VARCHAR(1)) pmt_type,dsh_ven_chk_no AS ven_chk_no,"
					+ "dsh_ven_chk_dt AS chk_dt, dsh_ven_chk_amt AS ven_chk,"
					+ "CAST(dsh_bnk_pmt_ref AS VARCHAR(22)) pmt_ref, (select tsa_sts_actn from tcttsa_rec where "
					+ "tsa_ref_itm= 0 and tsa_ref_sbitm=0 and tsa_sts_typ='T' and "
					+ "tsa_ref_pfx=dsh_dsb_pfx and tsa_ref_no=dsh_dsb_no and tsa_cmpy_id=dsh_cmpy_id) disb_sts "
					+ "from aptdsh_rec, aprven_rec where dsh_cmpy_id=ven_cmpy_id and dsh_ven_id=ven_ven_id";

			if (cmpyId.trim().length() > 0) {
				hql += " AND dsh_cmpy_id=:cmpy_id";
			}

			if (ssnId.trim().length() > 0) {
				hql += " AND dsh_ssn_id=:ssn_id";
			}

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", cmpyId);

			if (ssnId.trim().length() > 0) {
				queryValidate.setParameter("ssn_id", Integer.parseInt(ssnId));
			}

			List<Object[]> listRecords = queryValidate.list();

			for (Object[] record : listRecords) {

				APDisbursement output = new APDisbursement();

				output.setCmpyId(record[0].toString());
				output.setDsbPfx(record[1].toString());
				output.setDsbNo(record[2].toString());
				output.setSsnId(record[3].toString());
				output.setVenId(record[4].toString());
				output.setVenNm(record[5].toString());
				output.setPmtType(record[6].toString());
				output.setVenChkNo(record[7].toString());
				if (record[8] != null) {
					SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
					String formatDate = formatter.format((Date) record[8]);

					output.setVenChkDt(formatDate);
				} else {
					output.setVenChkDt("");
				}
				output.setVenChkAmt(cmn.formatAmount(Double.parseDouble(record[9].toString())));
				output.setBnkPmtRef(record[10].toString());
				output.setDisbSts(record[11].toString());

				starBrowseResponse.output.fldTblAPDisbursement.add(output);
			}
		} catch (Exception e) {
			// TODO: handle exception
			logger.debug("AP Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void readOpenItems(BrowseResponse<OpenItemBrowseOutput> starBrowseRes, String cmpyId, String venId,
			String brh, String cry) throws Exception {

		logger.info("AP Info : {}", new Object() {
		}.getClass().getEnclosingMethod().getName());

		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = "SELECT pyh_due_dt, pyh_ven_inv_dt, CAST(pyh_opa_pfx AS VARCHAR(2)), pyh_opa_no, CAST(pyh_ven_inv_no AS VARCHAR(22)), "
					+ "CAST(pyh_pmt_typ AS VARCHAR(1)), CAST(pyh_ap_brh AS VARCHAR(3)), pyh_disc_dt, CAST(pyh_cry AS VARCHAR(3)), pyh_disc_amt, pyh_balamt, pyh_ip_amt, "
					+ "pyh_balamt-pyh_ip_amt AS in_prs_bal FROM aptpyh_rec WHERE pyh_arch_trs=0";

			if (cmpyId.length() > 0) {
				hql = hql + " AND pyh_cmpy_id = :cmpy_id";
			}

			if (venId.length() > 0) {
				hql = hql + " AND pyh_ven_id = :ven_id";
			}

			if (brh.length() > 0) {
				hql = hql + " AND pyh_ap_brh = :brh";
			}

			if (cry.length() > 0) {
				hql = hql + " AND pyh_cry = :cry";
			}

			hql += " ORDER BY pyh_due_dt, pyh_opa_pfx, pyh_opa_no";
			;

			// TODO trn_sts is not NULL
			Query queryValidate = session.createSQLQuery(hql);

			if (cmpyId.length() > 0) {
				queryValidate.setParameter("cmpy_id", cmpyId);
			}
			if (venId.length() > 0) {
				queryValidate.setParameter("ven_id", venId);
			}
			if (brh.length() > 0) {
				queryValidate.setParameter("brh", brh);
			}
			if (cry.length() > 0) {
				queryValidate.setParameter("cry", cry);
			}

			List<Object[]> rows = queryValidate.list();
			CommonFunctions objCom = new CommonFunctions();
			for (Object[] row : rows) {
				OpenItemInfo openItem = new OpenItemInfo();

				if (row[0] != null) {
					SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
					String formatDate = formatter.format((Date) row[0]);

					openItem.setDueDt(formatDate);
				} else {
					openItem.setDueDt("");
				}
				if (row[1] != null) {
					SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
					String formatDate = formatter.format((Date) row[1]);

					openItem.setInvDt(formatDate);
				} else {
					openItem.setInvDt("");
				}
				openItem.setRefPfx(String.valueOf(row[2]));
				openItem.setRefNo(String.valueOf(row[3]));
				openItem.setInvNo(String.valueOf(row[4]));
				openItem.setPmtTyp(String.valueOf(row[5]));
				openItem.setBrh(String.valueOf(row[6]));
				if (row[7] != null) {
					SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
					String formatDate = formatter.format((Date) row[7]);

					openItem.setDiscDt(String.valueOf(formatDate));
				} else {
					openItem.setDiscDt("");
				}
				openItem.setCry(String.valueOf(row[8]));
				openItem.setDiscAmt(objCom.formatAmount(Double.parseDouble(row[9].toString())));
				openItem.setBalAmt(objCom.formatAmount(Double.parseDouble(row[10].toString())));
				openItem.setIpAmt(objCom.formatAmount(Double.parseDouble(row[11].toString())));
				openItem.setInPrsBal(objCom.formatAmount(Double.parseDouble(row[12].toString())));

				starBrowseRes.output.fldTblOpnItm.add(openItem);
			}

		} catch (Exception e) {

			logger.debug("AP Info : {}", e.getMessage(), e);
			starBrowseRes.output.rtnSts = 1;
			starBrowseRes.output.messages.add(CommonConstants.SERVER_ERR);

			session.close();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void readCheckView(BrowseResponse<CheckViewBrowseOutput> starBrowseResponse, String cmpyId, String dsbPfx,
			String dsbNo) throws Exception {

		logger.info("AP Info : {}", new Object() {
		}.getClass().getEnclosingMethod().getName());

		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = "SELECT CAST(pyh_opa_pfx AS VARCHAR(2)), pyh_opa_no, CAST(pyh_ven_inv_no AS VARCHAR(22)), dsd_dsamt, "
					+ "CAST(pyh_cry AS VARCHAR(3)), dsd_dsb_amt, pyh_exrt FROM aptdsd_rec, aptpyh_rec WHERE dsd_cmpy_id = pyh_cmpy_id "
					+ "AND dsd_ap_pfx = pyh_ap_pfx AND dsd_ap_no=pyh_ap_no ";

			if (cmpyId.length() > 0) {
				hql = hql + " AND pyh_cmpy_id = :cmpy_id";
			}

			if (dsbPfx.length() > 0) {
				hql = hql + " AND dsd_dsb_pfx = :dsb_pfx";
			}

			if (dsbNo.length() > 0) {
				hql = hql + " AND dsd_dsb_no = :dsb_no";
			}

			hql += " ORDER BY pyh_opa_pfx, pyh_opa_no";

			// TODO trn_sts is not NULL
			Query queryValidate = session.createSQLQuery(hql);

			if (cmpyId.length() > 0) {
				queryValidate.setParameter("cmpy_id", cmpyId);
			}
			if (dsbPfx.length() > 0) {
				queryValidate.setParameter("dsb_pfx", dsbPfx);
			}
			if (dsbNo.length() > 0) {
				queryValidate.setParameter("dsb_no", Integer.parseInt(dsbNo));
			}

			List<Object[]> rows = queryValidate.list();
			CommonFunctions objCom = new CommonFunctions();
			for (Object[] row : rows) {
				CheckViewInfo checkView = new CheckViewInfo();

				checkView.setRefPfx(String.valueOf(row[0]));
				checkView.setRefNo(String.valueOf(row[1]));
				checkView.setInvNo(String.valueOf(row[2]));
				checkView.setPmtAmt(objCom.formatAmount(Double.parseDouble(row[3].toString())));
				checkView.setCry(String.valueOf(row[4]));
				checkView.setDsbAmt(objCom.formatAmount(Double.parseDouble(row[5].toString())));
				checkView.setExrt(objCom.formatAmount(Double.parseDouble(row[6].toString())));

				starBrowseResponse.output.fldTblChkVw.add(checkView);
			}

		} catch (Exception e) {

			logger.debug("AP Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(CommonConstants.SERVER_ERR);

			session.close();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public List<APDesbCheckInfo> readDsbCheck(BrowseResponse<APDesCheckInfoBrowseOutput> starBrowseResponse, String cmpyId,
			String invcNo, String venId, String brh, String frmInvcDt, String toInvcDt) throws Exception {

		logger.info("AP Info : {}", new Object() {
		}.getClass().getEnclosingMethod().getName());
		CommonFunctions commonFunctions = new CommonFunctions();
		Session session = null;
		String hql = "";
		List<APDesbCheckInfo> apChkDsbExports = new ArrayList<APDesbCheckInfo>();
		
		try {
			session = SessionUtilInformix.getSession();

			hql = "select CAST(v.pyh_ven_id  AS VARCHAR(8)) vnId, CAST(v.pyh_ven_inv_no  AS VARCHAR(8)) invcNo,v.pyh_ven_inv_dt, v.pyh_due_dt,v.pyh_po_no, "
					+ "v.pyh_opa_no, CAST(v.pyh_ap_brh  AS VARCHAR(3)) brh, v.pyh_orig_amt , v.pyh_disc_amt,v.pyh_ent_dt, "
					+ "CAST(ven.ven_ven_nm  AS VARCHAR(15)) venNm, cast(v.pyh_opa_pfx as varchar(2)) vchr_pfx from aptpyh_rec v, aprven_rec ven where 1=1 "
					+ " and ven.ven_cmpy_id = v.pyh_cmpy_id and ven.ven_ven_id = v.pyh_ven_id";

			if (cmpyId.length() > 0) {
				hql = hql + " AND pyh_cmpy_id = :cmpy_id";
			}

			if (invcNo.length() > 0) {
				hql = hql + " AND pyh_ven_inv_no = :invno";
			}

			if (venId.length() > 0) {
				hql = hql + " AND pyh_ven_id = :ven_id";
			}

			if (brh.length() > 0) {
				hql = hql + " AND pyh_ap_brh = :brh";
			}

			if (frmInvcDt.length() > 0) {
				hql = hql + " and pyh_ven_inv_dt >= to_date(:invc_dt, 'mm/dd/yyyy')";
			}

			if (toInvcDt.length() > 0) {
				hql = hql + " and pyh_ven_inv_dt <= to_date(:invc_dt_to, 'mm/dd/yyyy')";
			}

			Query queryValidate = session.createSQLQuery(hql);

			if (cmpyId.length() > 0) {
				queryValidate.setParameter("cmpy_id", cmpyId);
			}
			if (invcNo.length() > 0) {
				queryValidate.setParameter("invno", invcNo);
			}

			if (venId.length() > 0) {
				queryValidate.setParameter("ven_id", venId);
			}

			if (brh.length() > 0) {
				queryValidate.setParameter("brh", brh);
			}

			if (frmInvcDt.length() > 0) {
				queryValidate.setParameter("invc_dt", frmInvcDt);
			}

			if (toInvcDt.length() > 0) {
				queryValidate.setParameter("invc_dt_to", toInvcDt);
			}

			List<Object[]> rows = queryValidate.list();
			CommonFunctions objCom = new CommonFunctions();
			for (Object[] row : rows) {
				APDesbCheckInfo output = new APDesbCheckInfo();

				if (row[0].equals(null)) {
				} else
					output.setVndrId(row[0].toString());
				if (row[1].equals(null)) {
				} else
					output.setInvcNo(row[1].toString());
				if (row[2].equals(null)) {
				} else {
					String invDt = new SimpleDateFormat("MM-dd-yyyy")
							.format(new SimpleDateFormat("yyyy-MM-dd").parse(row[2].toString()));
					output.setInvcDt(invDt);

					output.setInvcDt((commonFunctions.formatDateWithoutTime((Date) row[2])));
				}
				if (row[3].equals(null)) {
				} else {
					String dueDt = new SimpleDateFormat("MM-dd-yyyy")
							.format(new SimpleDateFormat("yyyy-MM-dd").parse(row[3].toString()));
					output.setDueDt(dueDt);

					output.setDueDt((commonFunctions.formatDateWithoutTime((Date) row[3])));
				}
				if (row[4].equals(null)) {
				} else
				{
					if(row[4].toString().equals("0"))
					{
						output.setPo("");
					}
					else
					{
						output.setPo("PO-" + row[4].toString());
					}
				}
				if (row[5].equals(null)) {
				} else
					output.setVchrNo(row[5].toString());
				if (row[6].equals(null)) {
				} else
					output.setBrh(row[6].toString());
				if (row[7].equals(null)) {

					output.setAmt("");
					output.setAmtStr("");

				} else {
					output.setAmt(row[7].toString());
					output.setAmtStr(commonFunctions.formatAmount(Double.parseDouble(row[7].toString())));
				}

				if (row[8].equals(null)) {

					output.setDisAmt("");
					output.setDisAmtStr("");

				} else {
					output.setDisAmt(row[8].toString());
					output.setDisAmtStr(commonFunctions.formatAmount(Double.parseDouble(row[8].toString())));
				}
				if (row[9] != null) {
					output.setEntrDt((commonFunctions.formatDateWithoutTime((Date) row[9])));

				} else {

					output.setEntrDt("");
				}

				if (row[10] != null) {
					output.setVndrNm(row[10].toString());
				} else {
					output.setVndrNm("");
				}

				if (row[11] != null) {
					output.setVchrPfx(row[11].toString());
				} else {
					output.setVchrPfx("");
				}

				String chkno = readChkNo(starBrowseResponse, output.getVchrPfx() + "-" + output.getVchrNo());

				if (!chkno.equals("")) {
					output.setCheckNo(chkno);
					starBrowseResponse.output.fldTblAPDsbChk.add(output);
					apChkDsbExports.add(output);
				}

			}

		} catch (Exception e) {

			logger.debug("AP Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(CommonConstants.SERVER_ERR);

			session.close();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		
		return apChkDsbExports;
	}

	public String readChkNo(BrowseResponse<APDesCheckInfoBrowseOutput> starBrowseResponse, String invcNo)
			throws Exception {

		logger.info("AP Info : {}", new Object() {
		}.getClass().getEnclosingMethod().getName());

		Session session = null;
		String hql = "";
		String chkNo = "";
		try {
			session = SessionUtil.getSession();

			hql = "select chk_no, 1 from cstm_param_inv where 1=1 and vchr_pay_mthd = 5";

			if (invcNo.length() > 0) {
				hql += " AND inv_no = :invcno";
			}
			Query queryValidate = session.createSQLQuery(hql);
			if (invcNo.length() > 0) {
				queryValidate.setParameter("invcno", invcNo);
			}

			List<Object[]> rows = queryValidate.list();
			for (Object[] row : rows) {
				if (row[0] != null) {
					chkNo = row[0].toString();
				} else {
					chkNo = "";
				}
			}

		} catch (Exception e) {

			logger.debug("AP Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(CommonConstants.SERVER_ERR);

			session.close();
		}
		return chkNo;
	}

}
