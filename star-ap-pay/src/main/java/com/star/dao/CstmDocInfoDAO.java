package com.star.dao;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.informix.asf.Connection;
import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.CommonFunctions;
import com.star.common.MaintanenceResponse;
import com.star.common.SessionUtil;
import com.star.common.SessionUtilInformix;
import com.star.common.SessionUtilInformixESP;
import com.star.common.ValidateValue;
import com.star.linkage.cstmdocinfo.CstmDocAddBrowseOutput;
import com.star.linkage.cstmdocinfo.CstmDocAddInfoInput;
import com.star.linkage.cstmdocinfo.CstmDocAddOutput;
import com.star.linkage.cstmdocinfo.CstmDocInfoBrowseOutput;
import com.star.linkage.cstmdocinfo.CstmDocInfoInput;
import com.star.linkage.cstmdocinfo.CstmDocInfoManOutput;
import com.star.linkage.cstmdocinfo.GlInfo;
import com.star.linkage.cstmdocinfo.ReconInfo;
import com.star.linkage.cstmdocinfo.VchrInfo;
import com.star.linkage.cstmvchrinfo.VoucherReconInfo;
import com.star.linkage.cstmvchrinfo.VoucherReconInfoBrowseOutput;
import com.star.linkage.payment.RemitInvoiceInfo;
import com.star.modal.CstmDocAddInfo;
import com.star.modal.CstmDocInfo;
import com.star.modal.CstmParamInv;
import com.star.modal.CstmVchrGlInfo;
import com.star.modal.CstmVchrReconInfo;

public class CstmDocInfoDAO {

	private static Logger logger = LoggerFactory.getLogger(ConditionDAO.class);

	public void addCstmDocument(MaintanenceResponse<CstmDocInfoManOutput> maintanenceResponse,
			CstmDocInfoInput cstmDocInfoInp) {
		Session session = null;
		Transaction tx = null;
		try {
			Date date = new Date();

			session = SessionUtil.getSession();
			tx = session.beginTransaction();
			CstmDocInfo cstmDocInfo = new CstmDocInfo();

			cstmDocInfo.setCtlNo(cstmDocInfoInp.getCtlNo());
			cstmDocInfo.setGatCtlNo(cstmDocInfoInp.getGatCtlNo());
			cstmDocInfo.setFlPdf(cstmDocInfoInp.getFlPdf());
			cstmDocInfo.setFlPdfName(cstmDocInfoInp.getFlPdfName());
			cstmDocInfo.setFlTxt(cstmDocInfoInp.getFlTxt());
			cstmDocInfo.setCrtDtts(date);
			cstmDocInfo.setPrsDtts(date);
			cstmDocInfo.setWrkFlw(cstmDocInfoInp.getWrkFlw());
			cstmDocInfo.setSrcFlg(cstmDocInfoInp.getSrcFlg());
			cstmDocInfo.setIsPrs(cstmDocInfoInp.getIsPrs());
			cstmDocInfo.setApproverTo(cstmDocInfoInp.getApproverTo());
			cstmDocInfo.setTrnSts(cstmDocInfoInp.getTrnSts());
			cstmDocInfo.setVchrNo(cstmDocInfoInp.getVchrNo());
			cstmDocInfo.setUpldBy(cstmDocInfoInp.getUpldBy());

			session.save(cstmDocInfo);

			tx.commit();
		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
			maintanenceResponse.output.messages.add(e.getMessage());
			maintanenceResponse.output.rtnSts = 1;

			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	/* GET ALL THE DEFINE FIELDS LIST */
	public void readFields(BrowseResponse<CstmDocInfoBrowseOutput> starBrowseRes, String trnSts, String userId,
			String usrGrp, String year) throws Exception {
		Session session = null;
		String hql = "";

		WkfMapDAO wkfMapDAO = new WkfMapDAO();

		try {
			session = SessionUtil.getSession();

			hql = " select ctl_no,gat_ctl_no,fl_pdf,fl_pdf_name,fl_txt,crt_dtts,prs_dtts, "
					+ " vchr_ctl_no,vchr_inv_no,vchr_ven_id,vchr_ven_nm,vchr_brh,vchr_amt,vchr_ext_ref,"
					+ " vchr_inv_dt,vchr_due_dt,vchr_pay_term, vchr_cry, upld_by,usr_nm,trn_rmk,src_flg, vchr_cat "
					+ " from cstm_doc_info doc_info , cstm_vchr_info vchr_info, usr_dtls where"
					+ " usr_dtls.usr_id = doc_info.upld_by and"
					+ " doc_info.ctl_no = vchr_info.vchr_ctl_no and (vchr_stx_ref is null or vchr_stx_ref='')";
			if (trnSts.length() == 0) {
				hql = hql + " and trn_sts is NULL";
			} else if (trnSts.length() > 0) {
				if (!trnSts.equals("R")) {
					hql = hql + " and trn_sts=:trn_sts ";
				}
			}

			if (!usrGrp.equals("1")) {
				hql = hql
						+ " and ((upld_by=:usr) OR doc_info.ctl_no in (select ctl_no from cstm_wkf_map where wkf_asgn_to=:usr and wkf_sts in ('S','R')))  ";
			}

			if (trnSts.equals("R")) {
				hql = hql + " and (doc_info.ctl_no in (select ctl_no from cstm_wkf_map where wkf_sts = 'R'))";
			} else {
				hql = hql
						+ " and (doc_info.ctl_no in (select ctl_no from cstm_wkf_map where wkf_sts <> 'R') OR doc_info.ctl_no not in (select ctl_no from cstm_wkf_map))";
			}

			if (year.length() > 0) {
				hql = hql + " and date_part('year', doc_info.crt_dtts)=:year";
			}

			hql = hql + " order by crt_dtts desc";

			// TODO trn_sts is not NULL
			Query queryValidate = session.createSQLQuery(hql);

			if (trnSts.length() > 0 && !trnSts.equals("R")) {
				queryValidate.setParameter("trn_sts", trnSts);
			}

			if (!usrGrp.equals("1")) {
				queryValidate.setParameter("usr", userId);
			}

			if (year.length() > 0) {
				queryValidate.setParameter("year", Double.parseDouble(year));
			}

			List<Object[]> listCstmDocVal = queryValidate.list();
			CommonFunctions objCom = new CommonFunctions();
			for (Object[] cstmDocVal : listCstmDocVal) {
				VchrInfo cstmdocInfo = new VchrInfo();

				cstmdocInfo.setCtlNo((Integer) (cstmDocVal[0]));
				cstmdocInfo.setGatCtlNo((Integer) (cstmDocVal[1]));
				cstmdocInfo.setFlPdf(String.valueOf(cstmDocVal[2]));
				cstmdocInfo.setFlPdfName(String.valueOf(cstmDocVal[3]));
				cstmdocInfo.setFlTxt(String.valueOf(cstmDocVal[4]));
				if (cstmDocVal[5] != null) {
					cstmdocInfo.setCrtDttsStr((objCom.formatDateWithoutTime((Date) cstmDocVal[5])));
					cstmdocInfo.setCrtDtts((Date) (cstmDocVal[5]));
				}
				if (cstmDocVal[6] != null) {
					cstmdocInfo.setPrsDttsStr((objCom.formatDateWithoutTime((Date) cstmDocVal[6])));
					cstmdocInfo.setPrsDtts((Date) (cstmDocVal[6]));
				}
				cstmdocInfo.setVchrCtlNo((Integer) (cstmDocVal[7]));
				cstmdocInfo.setVchrInvNo(String.valueOf(cstmDocVal[8]));
				cstmdocInfo.setVchrVenId(String.valueOf(cstmDocVal[9]));
				cstmdocInfo.setVchrVenNm(String.valueOf(cstmDocVal[10]));
				cstmdocInfo.setVchrBrh(String.valueOf(cstmDocVal[11]));

				cstmdocInfo.setVchrAmt(Double.parseDouble(String.valueOf(cstmDocVal[12])));

				if (Double.parseDouble(String.valueOf(cstmDocVal[12])) > 0) {
					cstmdocInfo.setVchrAmtStr(objCom.formatAmount(Double.parseDouble(String.valueOf(cstmDocVal[12]))));
				} else {
					cstmdocInfo.setVchrAmtStr("");
				}
				cstmdocInfo.setVchrExtRef(String.valueOf(cstmDocVal[13]));
				if (cstmDocVal[14] != null) {
					cstmdocInfo.setVchrInvDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[14])));
					cstmdocInfo.setVchrInvDt((Date) (cstmDocVal[14]));
				} else {
					cstmdocInfo.setVchrInvDtStr("");
				}
				if (cstmDocVal[15] != null) {
					cstmdocInfo.setVchrDueDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[15])));
					cstmdocInfo.setVchrDueDt((Date) (cstmDocVal[15]));
				} else {
					cstmdocInfo.setVchrDueDtStr("");
				}
				cstmdocInfo.setVchrPayTerm(String.valueOf(cstmDocVal[16]));
				cstmdocInfo.setVchrCry(String.valueOf(cstmDocVal[17]));
				cstmdocInfo.setUpldBy(String.valueOf(cstmDocVal[18]));
				cstmdocInfo.setUsrNm(String.valueOf(cstmDocVal[19]));
				if (cstmDocVal[20] != null) {
					cstmdocInfo.setTrnRmk(String.valueOf(cstmDocVal[20]));
				} else {
					cstmdocInfo.setTrnRmk("");
				}
				cstmdocInfo.setSrcFlg(String.valueOf(cstmDocVal[21]));

				if (cstmDocVal[22] != null) {
					cstmdocInfo.setVchrCat(cstmDocVal[22].toString());
				} else {
					cstmdocInfo.setVchrCat("");
				}

				wkfMapDAO.getWorkflowSpecific(String.valueOf(cstmdocInfo.getCtlNo()), cstmdocInfo);

				starBrowseRes.output.fldTblDoc.add(cstmdocInfo);
			}

		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
			starBrowseRes.output.rtnSts = 1;
			starBrowseRes.output.messages.add(CommonConstants.SERVER_ERR);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	/* GET ALL THE DEFINE FIELDS LIST */
	public void readStxFields(BrowseResponse<CstmDocInfoBrowseOutput> starBrowseRes, String trnSts, int pageNo,
			String cmpyId, String year) throws Exception {
		Session session = null;
		String hql = "";
		String voucherList = "";

		WkfMapDAO wkfMapDAO = new WkfMapDAO();

		List<CstmParamInv> invsListPending = new ArrayList<CstmParamInv>();

		invsListPending = getPendingVoucherList(trnSts, year);

		voucherList = getVoucherList(invsListPending);

		try {
			session = SessionUtilInformixESP.getSession();

			/*
			 * hql =
			 * "SELECT cast(vch_ven_inv_no as varchar(22)) vch_ven_inv_no, cast(vch_ven_id as varchar(8)) vch_ven_id,"
			 * +
			 * " cast(aprven_rec.ven_ven_long_nm as varchar(35)) ven_ven_long_nm, cast(vch_vchr_brh as varchar(3)) vch_vchr_brh,"
			 * +
			 * " vch_vchr_amt, cast(vch_cry as varchar(3)) vch_cry, vch_ven_inv_dt, vch_due_dt"
			 * + " FROM aptvch_rec, aprven_rec" +
			 * " WHERE aptvch_rec.vch_cmpy_id = aprven_rec.ven_cmpy_id AND aptvch_rec.vch_ven_id = aprven_rec.ven_ven_id"
			 * ;
			 */
			if (CommonConstants.DB_TYP.equals("INF")) {

				if (trnSts.equals("C") || trnSts.equals("A") ) {
					hql = "SELECT pyh_ven_no as ven_id, cast(trim(ven_nm) as varchar(35)) as ven_long_nm, cast(trim(pyh_desc_20) as varchar(22)) as ven_inv_no, "
							+ " cast('' as varchar(35)) as lgn_id, "
							+ " TO_DATE(cast(pyh_ref_dt as varchar(8)),'%Y%m%d') ref_dt, "
							+ " TO_DATE(cast(pyh_due_dt as varchar(8)),'%Y%m%d') due_dt,"
							+ " cast('PO' as varchar(2)) as po_pfx, 0 po_no, 0 po_itm, 		" + "cast('' as varchar(3)) as cry, "
							+ " cast(trim(pyh_ref_cd) as varchar(2)) as pyhr_pfx, pyh_ref_no, "
							+ " cast(trim(pyh_brh) as varchar(3)) as pyhr_brh,"
							+ " TO_DATE(cast(pyh_ref_dt as varchar(8)),'%Y%m%d') ref_dt, pyh_amt_ap, "
							+ " cast('' as varchar(3)) as pyh_cat, cast('' as varchar(1)) sts_trn, cast('' as varchar(1)) sts_pay"
							+ " FROM aptpyh_rec,      aprven_rec "
							+ " WHERE ven_ven_no = pyh_ven_no  and substr(cast(pyh_ref_dt as varchar(8)),0,4)=:year";
					
					if(trnSts.equals("C"))
					{
						hql = hql + " and pyh_balance = 0";
					}
					else
					{
						hql = hql + " and pyh_balance <> 0";
					}
					
				} else {
					hql = "SELECT vch_ven_no as ven_id, cast(trim(ven_nm) as varchar(35)) as ven_long_nm, cast(trim(vch_ext_ref) as varchar(22)) as ven_inv_no, "
							+ "cast((select trim(usr_usr) from mxrusr_rec where usr_usr=vch_lgn_id) as varchar(35)) as lgn_id, "
							+ "TO_DATE(cast(vch_ref_dt as varchar(8)),'%Y%m%d')vch_ref_dt, "
							+ "TO_DATE(cast(vch_due_dt as varchar(8)),'%Y%m%d') vch_due_dt, "
							+ "cast('PO' as varchar(2)) as po_pfx, vch_po_no, vch_po_itm, 					 "
							+ "cast('' as varchar(3)) as cry, "
							+ "cast(trim(vch_ref_pfx) as varchar(2)) as vchr_pfx, vch_vch_no, "
							+ "cast(trim(vch_brh) as varchar(3)) as vchr_brh,"
							+ "TO_DATE(cast(vch_ref_dt as varchar(8)),'%Y%m%d')vch_ref_dt, vch_vch_amt, "
							+ "cast('' as varchar(3)) as vchr_cat, cast('' as varchar(1)) sts_trn, cast('' as varchar(1)) sts_pay"
							+ " FROM aptvch_rec,      aprven_rec "
							+ "WHERE ven_ven_no = vch_ven_no and substr(cast(vch_ref_dt as varchar(8)),0,4)=:year";
				}
			} else if (CommonConstants.DB_TYP.equals("POS")) {
				hql = "SELECT vch_ven_no as ven_id, trim(ven_nm) as ven_long_nm, trim(vch_ext_ref) as ven_inv_no,"
						+ " (select trim(usr_usr) from mxrusr_rec where usr_usr=vch_lgn_id) as lgn_id, TO_DATE(cast(vch_ref_dt as varchar(8)),'YYYYMMDD')vch_ref_dt, "
						+ " TO_DATE(cast(vch_due_dt as varchar(8)),'YYYYMMDD') vch_due_dt, "
						+ "cast('PO' as varchar(2)) as po_pfx, vch_po_no, vch_po_itm,"
						+ " cast('' as varchar(3)) as cry, trim(vch_ref_pfx) as vchr_pfx, vch_vch_no, trim(vch_brh) as vchr_brh,"
						+ " TO_DATE(cast(vch_ref_dt as varchar(8)),'YYYYMMDD')vch_ref_dt, "
						+ " vch_vch_amt, cast('' as varchar(3)) as vchr_cat, cast('' as varchar(1)), cast('' as varchar(1)) "
						+ " FROM aptvch_rec, aprven_rec  WHERE 1=1 and ven_ven_no = vch_ven_no and"
						+ " substring(cast(vch_ref_dt as varchar(8)),0,5)=:year";
			}

			/* SENT FOR APPROVAL AND APPROVED RECORD */
			if (trnSts.equals("S")) {
				hql = hql + " and vch_ref_pfx || '-' || vch_vch_no in (:vchrList)";
			}  
			
			
			else if(!trnSts.equals("C") && !trnSts.equals("A") ){
				hql = hql + " and vch_ref_pfx || '-' || vch_vch_no not in (:vchrList)";
			}

			int skipVal = pageNo * 50;

			if (CommonConstants.DB_TYP.equals("POS")) {
				hql = hql + " order by vch_ssn_dt desc, vch_vch_no desc offset " + skipVal + " limit "
						+ CommonConstants.DB_OUT_REC;
			} else if (CommonConstants.DB_TYP.equals("INF")) {
				
				if (trnSts.equals("C") || trnSts.equals("A") ) {
					hql = hql + " order by pyh_ref_no desc skip " + skipVal + " limit " + CommonConstants.DB_OUT_REC;
				}
				else
				{
					hql = hql + " order by vch_vch_no desc skip " + skipVal + " limit " + CommonConstants.DB_OUT_REC;
				}
				
				
			}

			/* hql = hql + " order by vch_ent_dt desc, vch_vchr_no desc"; */

			// TODO trn_sts is not NULL
			Query queryValidate = session.createSQLQuery(hql);

			if (!trnSts.equals("C") && !trnSts.equals("A")) {
				List<String> items = Arrays.asList(voucherList.split("\\s*,\\s*"));

				queryValidate.setParameterList("vchrList", items);
			}

			/*	*/
			queryValidate.setParameter("year", year);

			/*
			 * queryValidate.setParameter("rec_val", pageNo * 100);
			 * queryValidate.setParameter("rcrd_out", CommonConstants.DB_OUT_REC);
			 */

			List<Object[]> listCstmDocVal = queryValidate.list();
			CommonFunctions objCom = new CommonFunctions();
			for (Object[] cstmDocVal : listCstmDocVal) {
				VchrInfo cstmdocInfo = new VchrInfo();

				cstmdocInfo.setVchrVenId(String.valueOf(cstmDocVal[0]));
				cstmdocInfo.setVchrVenNm(String.valueOf(cstmDocVal[1]));
				cstmdocInfo.setVchrInvNo(String.valueOf(cstmDocVal[2]));
				cstmdocInfo.setUpldBy(String.valueOf(cstmDocVal[3]));
				if (cstmDocVal[4] != null) {
					cstmdocInfo.setVchrInvDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[4])));
					cstmdocInfo.setVchrInvDt((Date) (cstmDocVal[4]));
				}
				if (cstmDocVal[5] != null) {
					cstmdocInfo.setVchrDueDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[5])));
					cstmdocInfo.setVchrDueDt((Date) (cstmDocVal[5]));
				}
				cstmdocInfo.setPoPfx(String.valueOf(cstmDocVal[6]));
				cstmdocInfo.setPoNo(String.valueOf(cstmDocVal[7]));
				cstmdocInfo.setPoItm(String.valueOf(cstmDocVal[8]));
				cstmdocInfo.setVchrCry(String.valueOf(cstmDocVal[9]));
				cstmdocInfo.setVchrPfx(String.valueOf(cstmDocVal[10]));
				cstmdocInfo.setVchrNo(String.valueOf(cstmDocVal[11]));
				cstmdocInfo.setVchrBrh(String.valueOf(cstmDocVal[12]));
				if (cstmDocVal[13] != null) {
					cstmdocInfo.setCrtDttsStr((objCom.formatDateWithoutTime((Date) cstmDocVal[13])));
					cstmdocInfo.setCrtDtts((Date) (cstmDocVal[13]));
				}
				cstmdocInfo.setVchrAmt(Double.parseDouble(String.valueOf(cstmDocVal[14])));
				cstmdocInfo.setVchrAmtStr(objCom.formatAmount(Double.parseDouble(String.valueOf(cstmDocVal[14]))));
				cstmdocInfo.setVchrCat(String.valueOf(cstmDocVal[15]));
				cstmdocInfo.setTransSts(String.valueOf(cstmDocVal[16]));
				cstmdocInfo.setPymntSts(String.valueOf(cstmDocVal[17]));

				if (String.valueOf(cstmDocVal[10]).length() > 0 && String.valueOf(cstmDocVal[11]).length() > 0) {
					getFileNameByVchrNo(cstmdocInfo, String.valueOf(cstmDocVal[10]), String.valueOf(cstmDocVal[11]));
					wkfMapDAO.getWorkflowSpecific(String.valueOf(cstmdocInfo.getCtlNo()), cstmdocInfo);
				} else {
					cstmdocInfo.setWrkFlw("");
					cstmdocInfo.setWrkFlwId(0);
					cstmdocInfo.setWrkFlwSts("");
					cstmdocInfo.setWrkFlwAprvr("");
				}

				for (int i = 0; i < invsListPending.size(); i++) {
					if ((cstmdocInfo.getVchrPfx() + "-" + cstmdocInfo.getVchrNo())
							.equals(invsListPending.get(i).getInvNo())) {
						cstmdocInfo.setChkNo(invsListPending.get(i).getChkNo());
						cstmdocInfo.setReqId(invsListPending.get(i).getReqId());
						break;
					}
				}

				starBrowseRes.output.fldTblDoc.add(cstmdocInfo);
			}

		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
			starBrowseRes.output.rtnSts = 1;
			starBrowseRes.output.messages.add(CommonConstants.SERVER_ERR);

		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	/* GET RECORD COUNT ON READ SERVICE */
	public void readCounts(BrowseResponse<CstmDocInfoBrowseOutput> starBrowseRes, String userId, String cmpyId,
			String year) throws Exception {
		Session session = null;
		String hql = "";
		String voucherListPending = "";
		String voucherListSent = "";
		String voucherListApproved = "";
		List<CstmParamInv> invsListPending = new ArrayList<CstmParamInv>();
		List<CstmParamInv> invsListSent = new ArrayList<CstmParamInv>();
		List<CstmParamInv> invsListApproved = new ArrayList<CstmParamInv>();

		invsListPending = getPendingVoucherList("", year);
		invsListSent = getPendingVoucherList("S", year);
		invsListApproved = getPendingVoucherList("A", year);

		voucherListPending = getVoucherList(invsListPending);
		voucherListSent = getVoucherList(invsListSent);
		voucherListApproved = getVoucherList(invsListApproved);

		try {
			session = SessionUtilInformixESP.getSession();

			if (CommonConstants.DB_TYP.equals("POS")) {
				hql = "select (SELECT count(*) to_review FROM aptvch_rec, aprven_rec  "
						+ "WHERE 1=1 and ven_ven_no = vch_ven_no " + " and vch_ref_pfx || '-' || vch_vch_no not "
						+ "in (:vchrListReview) and substring(cast(vch_ref_dt as varchar(8)),0,5)=:year) to_review, "
						+ "(SELECT count(*) confirmed FROM aptvch_rec, aprven_rec "
						+ " WHERE 1=1 and ven_ven_no = vch_ven_no "
						+ " and vch_ref_pfx || '-' || vch_vch_no in (:vchrListSent) "
						+ " and substring(cast(vch_ref_dt as varchar(8)),0,5)=:year) confirmed,"
						+ " (SELECT count(*) pay_approved FROM aptvch_rec, aprven_rec  WHERE 1=1 and "
						+ " ven_ven_no = vch_ven_no and vch_ref_pfx || '-' || vch_vch_no in (:vchrListApproved) "
						+ " and substring(cast(vch_ref_dt as varchar(8)),0,5)=:year) pay_approved, "
						+ "(SELECT count(*) to_review FROM aptvch_rec,aptpyh_rec where vch_ref_pfx = pyh_ref_cd"
						+ " and vch_vch_no = pyh_ref_no and pyh_balance=0 and 	:year) completed";
			} else if (CommonConstants.DB_TYP.equals("INF")) {

				hql = "select (SELECT count(*) to_review FROM aptvch_rec  WHERE "
						+ "vch_ref_pfx || '-' || vch_vch_no not in (:vchrListReview) "
						+ "and substr(cast(vch_ref_dt as varchar(8)),0,4)=:year) to_review, " + "0 confirmed,"
						+ " (SELECT COUNT(*) to_review FROM aptpyh_rec WHERE pyh_balance <> 0 and substr(cast(pyh_ref_dt as varchar(8)),0,4)=:year) pay_approved, "
						+ "(SELECT COUNT(*) to_review FROM aptpyh_rec WHERE pyh_balance = 0 and substr(cast(pyh_ref_dt as varchar(8)),0,4)=:year) completed "
						+ "from scrcsc_rec limit 1";
			}

			// TODO trn_sts is not NULL
			Query queryValidate = session.createSQLQuery(hql);

			List<String> itemsReview = Arrays.asList(voucherListPending.split("\\s*,\\s*"));

			queryValidate.setParameterList("vchrListReview", itemsReview);

			List<String> itemsSent = Arrays.asList(voucherListSent.split("\\s*,\\s*"));

			List<String> itemsApproved = Arrays.asList(voucherListApproved.split("\\s*,\\s*"));

			if (CommonConstants.DB_TYP.equals("POS")) {
				queryValidate.setParameterList("vchrListSent", itemsSent);

				queryValidate.setParameterList("vchrListApproved", itemsApproved);
			}

			/* queryValidate.setParameter("cmpy", cmpyId); */

			queryValidate.setParameter("year", year);

			List<Object[]> listCstmDocVal = queryValidate.list();

			for (Object[] cstmDocVal : listCstmDocVal) {

				starBrowseRes.output.toReviewCount = Integer.parseInt(cstmDocVal[0].toString());
				starBrowseRes.output.confirmedCount = invsListSent.size();
				starBrowseRes.output.payApprovedCount = Integer.parseInt(cstmDocVal[2].toString());
				starBrowseRes.output.completeCount = Integer.parseInt(cstmDocVal[3].toString());

			}

		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void readCountDocument(BrowseResponse<CstmDocInfoBrowseOutput> starBrowseRes, String userId, String usrGrp,
			String year) throws Exception {
		Session session = null;
		String hql = "";
		Transaction tx = null;
		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();
			/*
			 * hql =
			 * " select count(*), 1  from cstm_doc_info doc_info , cstm_vchr_info vchr_info, usr_dtls where"
			 * + " usr_dtls.usr_id = doc_info.upld_by and" +
			 * " doc_info.ctl_no = vchr_info.vchr_ctl_no and (vchr_stx_ref is null or vchr_stx_ref='')"
			 * ;
			 * 
			 * hql = hql + " and trn_sts is NULL";
			 */

			hql = "select (select count(*) from cstm_doc_info doc_info , cstm_vchr_info vchr_info, "
					+ "usr_dtls where 	usr_dtls.usr_id = doc_info.upld_by and 	"
					+ "doc_info.ctl_no = vchr_info.vchr_ctl_no and (vchr_stx_ref is null or vchr_stx_ref='') and date_part('year', doc_info.crt_dtts)=:year "
					+ "and trn_sts is NULL and (doc_info.ctl_no in (select ctl_no from cstm_wkf_map where wkf_sts <> 'R') OR doc_info.ctl_no not in (select ctl_no from cstm_wkf_map))";

			if (!usrGrp.equals("1"))
				hql = hql
						+ " and (doc_info.upld_by =:usr OR doc_info.ctl_no in (select ctl_no from cstm_wkf_map where wkf_asgn_to=:usr and wkf_sts='S'))";

			hql = hql + ") rvw_inv," + "(select count(*) from cstm_doc_info doc_info , cstm_vchr_info vchr_info, "
					+ "usr_dtls where 	usr_dtls.usr_id = doc_info.upld_by and 	"
					+ "doc_info.ctl_no = vchr_info.vchr_ctl_no and (vchr_stx_ref is null or vchr_stx_ref='') "
					+ "and trn_sts='H' and date_part('year', doc_info.crt_dtts)=:year";

			if (!usrGrp.equals("1"))
				hql = hql
						+ " and (doc_info.upld_by =:usr OR doc_info.ctl_no in (select ctl_no from cstm_wkf_map where wkf_asgn_to=:usr and wkf_sts='S'))";

			hql = hql + ") hld_inv,  ";

			hql = hql + "(select count(*) from cstm_doc_info doc_info , cstm_vchr_info vchr_info, "
					+ "usr_dtls where 	usr_dtls.usr_id = doc_info.upld_by and 	"
					+ "doc_info.ctl_no = vchr_info.vchr_ctl_no and date_part('year', doc_info.crt_dtts)=:year and (vchr_stx_ref is null or vchr_stx_ref='') "
					+ "and trn_sts is NULL ";

			if (!usrGrp.equals("1")) {
				hql = hql
						+ " and (doc_info.upld_by =:usr OR doc_info.ctl_no in (select ctl_no from cstm_wkf_map where wkf_asgn_to=:usr and wkf_sts='R'))) reject_inv";
			} else {
				hql = hql + " and (doc_info.ctl_no in (select ctl_no from cstm_wkf_map where wkf_sts='R'))) reject_inv";
			}

			// TODO trn_sts is not NULL
			Query queryValidate = session.createSQLQuery(hql);

			if (!usrGrp.equals("1"))
				queryValidate.setParameter("usr", userId);

			queryValidate.setParameter("year", Double.parseDouble(year));

			List<Object[]> listCstmDocVal = queryValidate.list();

			for (Object[] cstmDocVal : listCstmDocVal) {

				starBrowseRes.output.toReviewDocCount = Integer.parseInt(cstmDocVal[0].toString());
				starBrowseRes.output.toHoldDocCount = Integer.parseInt(cstmDocVal[1].toString());
				starBrowseRes.output.rejectDocCount = Integer.parseInt(cstmDocVal[2].toString());
			}

		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
			starBrowseRes.output.rtnSts = 1;
			starBrowseRes.output.messages.add(CommonConstants.SERVER_ERR);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void getFileNameByVchrNo(VchrInfo cstmdocInfo, String vchrPfx, String vchrNo) {
		Session session = null;
		String hql = "";
		String vchrNoStr = vchrPfx + "-" + vchrNo;

		try {
			session = SessionUtil.getSession();

			hql = "select ctl_no, fl_pdf_name, fl_pdf from cstm_doc_info, cstm_vchr_info where ctl_no=vchr_ctl_no and vchr_stx_ref=:stx_ref";

			// TODO trn_sts is not NULL
			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("stx_ref", vchrNoStr);

			List<Object[]> rows = queryValidate.list();

			for (Object[] row : rows) {
				cstmdocInfo.setCtlNo((Integer) (row[0]));
				cstmdocInfo.setFlPdfName(String.valueOf(row[1]));
				cstmdocInfo.setFlPdf(String.valueOf(row[2]));
			}

			hql = "select count(*) from cstm_doc_info doc, cstm_doc_add_info cdai where doc_ctl_no=doc.ctl_no and "
					+ "(doc.ctl_no = (select vchr.vchr_ctl_no from cstm_vchr_info vchr where vchr_stx_ref =:vchr_no) or cdai.vchr_no=:vchr_no)";

			queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("vchr_no", vchrNoStr);

			//cstmdocInfo.setAddDocCount(Integer.parseInt(String.valueOf(queryValidate.list().get(0))));

		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	/* GET ALL THE DEFINE FIELDS LIST */
	public List<CstmParamInv> getPendingVoucherList(String sts, String year) {
		Session session = null;
		String hql = "";
		String voucherList = "";

		List<CstmParamInv> cstmParamInvsList = new ArrayList<CstmParamInv>();

		try {
			session = SessionUtil.getSession();

			hql = "select inv_no, chk_no, inv.req_id from cstm_pay_params param,cstm_param_inv inv where inv.req_id=param.req_id and inv.inv_sts=true";

			if (sts.length() > 0) {
				hql += " and pay_sts=:sts";
			}

			if (year.length() > 0) {
				hql += " and date_part('year', crtd_on)=:year";
			}

			// TODO trn_sts is not NULL
			Query queryValidate = session.createSQLQuery(hql);

			if (sts.length() > 0) {
				queryValidate.setParameter("sts", sts);
			}

			if (year.length() > 0) {
				queryValidate.setParameter("year", Double.parseDouble(year));
			}

			List<Object[]> listVouchers = queryValidate.list();

			for (Object[] vouchers : listVouchers) {

				CstmParamInv cstmParamInv = new CstmParamInv();

				if (vouchers[0] != null) {
					cstmParamInv.setInvNo(vouchers[0].toString());
				} else {
					cstmParamInv.setInvNo("");
				}

				if (vouchers[1] != null) {
					cstmParamInv.setChkNo(vouchers[1].toString());
				} else {
					cstmParamInv.setChkNo("");
				}

				if (vouchers[2] != null) {
					cstmParamInv.setReqId(vouchers[2].toString());
				} else {
					cstmParamInv.setReqId("");
				}

				cstmParamInvsList.add(cstmParamInv);

				voucherList = voucherList + vouchers[0].toString() + ",";
			}

			if (voucherList.length() > 0) {
				voucherList = voucherList.substring(0, voucherList.length() - 1);
			}
		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return cstmParamInvsList;
	}

	/* GET ALL THE DEFINE FIELDS LIST */
	public List<CstmParamInv> getPendingVouchersSearch(String sts, String year, String usrId) {
		Session session = null;
		String hql = "";
		String voucherList = "";

		List<CstmParamInv> cstmParamInvsList = new ArrayList<CstmParamInv>();

		try {
			session = SessionUtil.getSession();

			hql = "select inv_no, chk_no, inv.req_id, param.crtd_by from cstm_pay_params param,cstm_param_inv inv where "
					+ "inv.req_id=param.req_id and inv.inv_sts=true and case when crtd_by = :usr_id then pay_sts <> 'P' "
					+ " else 1=1 end";

			if (sts.length() > 0) {
				hql += " and pay_sts=:sts";
			}

			if (year.length() > 0) {
				hql += " and date_part('year', crtd_on)=:year";
			}

			// TODO trn_sts is not NULL
			Query queryValidate = session.createSQLQuery(hql);

			if (sts.length() > 0) {
				queryValidate.setParameter("sts", sts);
			}

			if (year.length() > 0) {
				queryValidate.setParameter("year", Double.parseDouble(year));
			}

			queryValidate.setParameter("usr_id", usrId);

			List<Object[]> listVouchers = queryValidate.list();

			for (Object[] vouchers : listVouchers) {

				CstmParamInv cstmParamInv = new CstmParamInv();

				if (vouchers[0] != null) {
					cstmParamInv.setInvNo(vouchers[0].toString());
				} else {
					cstmParamInv.setInvNo("");
				}

				if (vouchers[1] != null) {
					cstmParamInv.setChkNo(vouchers[1].toString());
				} else {
					cstmParamInv.setChkNo("");
				}

				if (vouchers[2] != null) {
					cstmParamInv.setReqId(vouchers[2].toString());
				} else {
					cstmParamInv.setReqId("");
				}

				if (vouchers[3].toString().equals(usrId)) {

				}

				cstmParamInvsList.add(cstmParamInv);

				voucherList = voucherList + vouchers[0].toString() + ",";
			}

			if (voucherList.length() > 0) {
				voucherList = voucherList.substring(0, voucherList.length() - 1);
			}
		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return cstmParamInvsList;
	}

	/* GET ALL THE DEFINE FIELDS LIST */
	public List<CstmParamInv> getPendingVouchersSearchESP(String sts, String year, String usrId) {
		Session session = null;
		String hql = "";
		String voucherList = "";

		List<CstmParamInv> cstmParamInvsList = new ArrayList<CstmParamInv>();

		try {
			session = SessionUtil.getSession();

			hql = "select inv_no, chk_no, inv.req_id, param.crtd_by from cstm_pay_params param,cstm_param_inv inv where "
					+ "inv.req_id=param.req_id and inv.inv_sts=true and case when crtd_by = :usr_id then pay_sts <> 'P' "
					+ " else 1=1 end";

			if (sts.length() > 0) {
				hql += " and pay_sts=:sts";
			}

			if (year.length() > 0) {
				hql += " and date_part('year', crtd_on)=:year";
			}

			// TODO trn_sts is not NULL
			Query queryValidate = session.createSQLQuery(hql);

			if (sts.length() > 0) {
				queryValidate.setParameter("sts", sts);
			}

			if (year.length() > 0) {
				queryValidate.setParameter("year", Double.parseDouble(year));
			}

			queryValidate.setParameter("usr_id", usrId);

			List<Object[]> listVouchers = queryValidate.list();

			for (Object[] vouchers : listVouchers) {

				CstmParamInv cstmParamInv = new CstmParamInv();

				if (vouchers[0] != null) {
					cstmParamInv.setInvNo(vouchers[0].toString());
				} else {
					cstmParamInv.setInvNo("");
				}

				if (vouchers[1] != null) {
					cstmParamInv.setChkNo(vouchers[1].toString());
				} else {
					cstmParamInv.setChkNo("");
				}

				if (vouchers[2] != null) {
					cstmParamInv.setReqId(vouchers[2].toString());
				} else {
					cstmParamInv.setReqId("");
				}

				if (vouchers[3].toString().equals(usrId)) {

				}

				cstmParamInvsList.add(cstmParamInv);

				voucherList = voucherList + vouchers[0].toString() + ",";
			}

			if (voucherList.length() > 0) {
				voucherList = voucherList.substring(0, voucherList.length() - 1);
			}
		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return cstmParamInvsList;
	}

	public int getCtlNo() {
		Session session = null;
		String hql = "";
		int ctlNo = 0;

		try {
			session = SessionUtil.getSession();
			session.beginTransaction();

			hql = " select COALESCE(max(ctl_no),0) ctl_no from cstm_doc_info";

			Query queryValidate = session.createSQLQuery(hql);

			ctlNo = Integer.parseInt(queryValidate.list().get(0).toString()) + 1;

		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return ctlNo;
	}

	public String getFileSeqNo() {

		Session session = null;
		String docCtl_no = "";

		try {

			String hql = "";

			session = SessionUtil.getSession();

			hql = "select to_char(nextval('ctl_no'),'0000000000FM')";

			Query queryValidate = session.createSQLQuery(hql);

			docCtl_no = queryValidate.list().get(0).toString();

		} catch (Exception e) {
			logger.debug("Document : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return docCtl_no;

	}

	public String getInvoiceUploadId(int ctlNo) {

		Session session = null;
		String upldUsr = "";

		try {

			String hql = "";

			session = SessionUtil.getSession();

			hql = "select upld_by from cstm_doc_info cdi where ctl_no = :ctl_no";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("ctl_no", ctlNo);

			if (queryValidate.list().size() > 0) {
				upldUsr = queryValidate.list().get(0).toString();
			}

		} catch (Exception e) {
			logger.debug("Document User : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return upldUsr;

	}

	public String getPdf(int ctlNo) {
		Session session = null;
		String hql = "";
		String pdfLocation = "";

		try {
			session = SessionUtil.getSession();
			session.beginTransaction();

			hql = " select  cast(fl_pdf as varchar(100)) fl_pdf,cast(fl_pdf as varchar(100)) fl_pdf_name  from cstm_doc_info where ctl_no =:ctlNo";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("ctlNo", ctlNo);

			Object obj = queryValidate.list();

			if (obj != null) {
				pdfLocation = queryValidate.list().get(0).toString();
			}

		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return pdfLocation;
	}

	/* GET Pdf name and it's Original Name */
	public List<Object[]> getPdfName(int ctlNo) throws Exception {
		Session session = null;
		String hql = "";
		List<Object[]> listCstmDocVal = null;
		try {
			session = SessionUtil.getSession();

			hql = " select  cast(fl_pdf as varchar(100)) fl_pdf, cast(fl_pdf_name as varchar(100)) fl_pdf_name from cstm_doc_info where ctl_no =:ctlNo";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("ctlNo", ctlNo);

			listCstmDocVal = queryValidate.list();

		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
			session.close();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return listCstmDocVal;
	}

	public void updDocDtls(MaintanenceResponse<CstmDocInfoManOutput> maintanenceResponse, VchrInfo fieldsInput,
			JsonArray reconList, JsonArray glList) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			hql = "update cstm_vchr_info set vchr_inv_no = :vchr_inv_no , vchr_ven_id=:vchr_ven_id, vchr_ven_nm=:vchr_ven_nm, "
					+ " vchr_brh = :vchr_brh , vchr_amt=:vchr_amt, vchr_ext_ref=:vchr_ext_ref, "
					+ " vchr_inv_dt = :vchr_inv_dt,";

			if (fieldsInput.getVchrDueDt() != null) {
				hql = hql + "vchr_due_dt=:vchr_due_dt, ";
			} else {
				hql = hql + "vchr_due_dt=null, ";
			}

			hql = hql + "vchr_pay_term=:vchr_pay_term, vchr_cry=:vchr_cry, vchr_po_no=:po_no, vchr_po_itm=:po_itm " + "where " + "vchr_ctl_no=:vchr_ctl_no ";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("vchr_ctl_no", fieldsInput.getVchrCtlNo());
			queryValidate.setParameter("vchr_inv_no", String.valueOf(fieldsInput.getVchrInvNo()));
			queryValidate.setParameter("vchr_ven_id", String.valueOf(fieldsInput.getVchrVenId()));
			queryValidate.setParameter("vchr_ven_nm", String.valueOf(fieldsInput.getVchrVenNm()));
			queryValidate.setParameter("vchr_brh", String.valueOf(fieldsInput.getVchrBrh()));
			queryValidate.setParameter("vchr_amt", Double.parseDouble(String.valueOf(fieldsInput.getVchrAmt())));
			queryValidate.setParameter("vchr_ext_ref", String.valueOf(fieldsInput.getVchrExtRef()));
			queryValidate.setParameter("vchr_inv_dt", ((Date) fieldsInput.getVchrInvDt()));

			if (fieldsInput.getVchrDueDt() != null) {
				queryValidate.setParameter("vchr_due_dt", ((Date) fieldsInput.getVchrDueDt()));
			}
			queryValidate.setParameter("vchr_pay_term", String.valueOf(fieldsInput.getVchrPayTerm()));
			queryValidate.setParameter("vchr_cry", String.valueOf(fieldsInput.getVchrCry()));
			queryValidate.setParameter("po_no", String.valueOf(fieldsInput.getPoNo()));
			queryValidate.setParameter("po_itm", String.valueOf(fieldsInput.getPoItm()));

			/* Update Recon Information */
			deleteReconInformation(session, reconList, fieldsInput.getVchrCtlNo());

			addReconInformation(session, reconList, fieldsInput.getVchrCtlNo());

			/* Update GL Information */
			deleteGLInformation(session, glList, fieldsInput.getVchrCtlNo());

			addGLInformation(session, glList, fieldsInput.getVchrCtlNo());

			queryValidate.executeUpdate();

			tx.commit();
		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
			maintanenceResponse.output.rtnSts = 1;
			maintanenceResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	/* UPDATE RECON INFORMATION */
	public void deleteReconInformation(Session session, JsonArray reconList, int ctlNo) throws Exception {
		String hql = "";

		hql = "delete from cstm_vchr_recon_info where vchr_ctl_no=:vchr_ctl_no";

		Query queryValidate = session.createSQLQuery(hql);
		queryValidate.setParameter("vchr_ctl_no", ctlNo);
		queryValidate.executeUpdate();
	}

	public void addReconInformation(Session session, JsonArray reconList, int ctlNo) throws Exception {
		for (int i = 0; i < reconList.size(); i++) {
			JsonObject jsonObject = reconList.get(i).getAsJsonObject();

			CstmVchrReconInfo reconInfo = new CstmVchrReconInfo();

			reconInfo.setVchrCtlNo(ctlNo);
			reconInfo.setVchrCrcnNo(Integer.parseInt(jsonObject.get("crcnNo").getAsString()));
			reconInfo.setVchrTrsNo(jsonObject.get("trsNo").getAsString());
			reconInfo.setVchrPoNo(jsonObject.get("poNo").getAsString());
			reconInfo.setVchrCstNo(Integer.parseInt(jsonObject.get("costNo").getAsString()));
			reconInfo.setVchrBalAmt(Double.parseDouble(jsonObject.get("balAmt").getAsString()));
			reconInfo.setVchrVenId(jsonObject.get("reconVenId").getAsString());
			session.save(reconInfo);
		}
	}

	/* UPDATE GL INFORMATION */
	public void deleteGLInformation(Session session, JsonArray reconList, int ctlNo) throws Exception {
		String hql = "";

		hql = "delete from cstm_vchr_gl_info where vchr_ctl_no=:vchr_ctl_no";

		Query queryValidate = session.createSQLQuery(hql);
		queryValidate.setParameter("vchr_ctl_no", ctlNo);
		queryValidate.executeUpdate();
	}

	public void addGLInformation(Session session, JsonArray glList, int ctlNo) throws Exception {
		for (int i = 0; i < glList.size(); i++) {
			JsonObject jsonObject = glList.get(i).getAsJsonObject();

			CstmVchrGlInfo glInfo = new CstmVchrGlInfo();

			glInfo.setVchrCtlNo(ctlNo);
			glInfo.setVchrAcct(jsonObject.get("glAcc").getAsString());
			glInfo.setVchrSubAcct(jsonObject.get("glSubAcc").getAsString());
			glInfo.setVchrDrAmt(Double.parseDouble(jsonObject.get("drAmt").getAsString()));
			glInfo.setVchrCrAmt(Double.parseDouble(jsonObject.get("crAmt").getAsString()));
			glInfo.setVchrRmk(jsonObject.get("remark").getAsString());
			session.save(glInfo);
		}
	}

	public void updStxReference(MaintanenceResponse<CstmDocInfoManOutput> maintanenceResponse, VchrInfo fieldsInput) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			hql = "update cstm_vchr_info set vchr_stx_ref = :vchr_no where vchr_ctl_no=:vchr_ctl_no ";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("vchr_ctl_no", fieldsInput.getVchrCtlNo());
			queryValidate.setParameter("vchr_no", String.valueOf(fieldsInput.getVchrNo()));

			queryValidate.executeUpdate();

			tx.commit();
		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
			maintanenceResponse.output.rtnSts = 1;
			maintanenceResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	/* GET specific THE DEFINE FIELDS LIST */
	public void viewRecord(BrowseResponse<CstmDocInfoBrowseOutput> starBrowseRes, int ctlNo, String cmpyId) {
		Session session = null;
		String hql = "";

		try {
			session = SessionUtil.getSession();

			hql = " select ctl_no,gat_ctl_no,fl_pdf,fl_pdf_name,fl_txt,crt_dtts,prs_dtts, "
					+ " vchr_ctl_no,vchr_inv_no,vchr_ven_id,vchr_ven_nm,vchr_brh,vchr_amt,"
					+ " vchr_ext_ref,vchr_inv_dt,vchr_due_dt,vchr_pay_term, vchr_cry , upld_by, usr_nm,trn_rmk, vchr_cat, vchr_po_no, vchr_po_itm "
					+ " from cstm_doc_info doc_info , cstm_vchr_info vchr_info, usr_dtls where" + " ctl_no=:ctl_no and "
					+ " usr_dtls.usr_id = doc_info.upld_by and" + " doc_info.ctl_no = vchr_info.vchr_ctl_no";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("ctl_no", ctlNo);
			CommonFunctions objCom = new CommonFunctions();
			List<Object[]> listCstmDocVal = queryValidate.list();
			GetCommonDAO objComDAO = new GetCommonDAO();

			for (Object[] cstmDocVal : listCstmDocVal) {
				VchrInfo cstmdocInfo = new VchrInfo();

				cstmdocInfo.setCtlNo((Integer) (cstmDocVal[0]));
				cstmdocInfo.setGatCtlNo((Integer) (cstmDocVal[1]));
				cstmdocInfo.setFlPdf(String.valueOf(cstmDocVal[2]));
				cstmdocInfo.setFlPdfName(String.valueOf(cstmDocVal[3]));
				cstmdocInfo.setFlTxt(String.valueOf(cstmDocVal[4]));

				if (cstmDocVal[5] != null) {
					cstmdocInfo.setCrtDttsStr((objCom.formatDateWithoutTime((Date) cstmDocVal[5])));
					cstmdocInfo.setCrtDtts((Date) (cstmDocVal[5]));
				}

				if (cstmDocVal[6] != null) {
					cstmdocInfo.setPrsDttsStr((objCom.formatDateWithoutTime((Date) cstmDocVal[6])));
					cstmdocInfo.setPrsDtts((Date) (cstmDocVal[6]));
				}
				cstmdocInfo.setVchrCtlNo((Integer) (cstmDocVal[7]));
				cstmdocInfo.setVchrInvNo(String.valueOf(cstmDocVal[8]));
				cstmdocInfo.setVchrVenId(String.valueOf(cstmDocVal[9]));
				cstmdocInfo.setVchrVenNm(String.valueOf(cstmDocVal[10]));
				cstmdocInfo.setVchrBrh(String.valueOf(cstmDocVal[11]));
				cstmdocInfo.setVchrAmt(Double.parseDouble(String.valueOf(cstmDocVal[12])));

				if (Double.parseDouble(String.valueOf(cstmDocVal[12])) > 0) {
					cstmdocInfo.setVchrAmtStr(objCom.formatAmount(Double.parseDouble(String.valueOf(cstmDocVal[12]))));
				} else {
					cstmdocInfo.setVchrAmtStr("");
				}

				cstmdocInfo.setVchrExtRef(String.valueOf(cstmDocVal[13]));

				if (cstmDocVal[14] != null) {
					cstmdocInfo.setVchrInvDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[14])));
					cstmdocInfo.setVchrInvDt((Date) (cstmDocVal[14]));
				}

				if (cstmDocVal[15] != null) {
					cstmdocInfo.setVchrDueDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[15])));
					cstmdocInfo.setVchrDueDt((Date) (cstmDocVal[15]));
				}

				cstmdocInfo.setVchrPayTerm(String.valueOf(cstmDocVal[16]));
				cstmdocInfo.setVchrCry(String.valueOf(cstmDocVal[17]));
				cstmdocInfo.setUpldBy(String.valueOf(cstmDocVal[18]));
				cstmdocInfo.setUsrNm(String.valueOf(cstmDocVal[19]));
				cstmdocInfo.setTrnRmk(String.valueOf(cstmDocVal[20]));

				if (cstmDocVal[21] != null) {
					cstmdocInfo.setVchrCat(String.valueOf(cstmDocVal[21]));
				} else {
					cstmdocInfo.setVchrCat("");
				}
				
				if (cstmDocVal[22] != null) {
					cstmdocInfo.setPoNo(String.valueOf(cstmDocVal[22]));
				} else {
					cstmdocInfo.setPoNo("");
				}
				
				if (cstmDocVal[23] != null) {
					cstmdocInfo.setPoItm(String.valueOf(cstmDocVal[23]));
				} else {
					cstmdocInfo.setPoItm("");
				}

				// GET VENDOR LIST
				// cstmdocInfo.setVendorList(objComDAO.getVendorList(String.valueOf(cstmDocVal[10]),
				// cmpyId));

				String vchrNo = validateInvoice(cstmdocInfo.getVchrVenId(), cstmdocInfo.getVchrInvNo());

				getReconDetails(cmpyId, cstmdocInfo.getVchrVenId(), cstmdocInfo.getPoNo(), cstmdocInfo.getVchrCry(),
						cstmdocInfo.getVchrExtRef(), cstmdocInfo);

				List<ReconInfo> reconInfos = getReconDetails(session, ctlNo, cmpyId);

				List<GlInfo> glInfos = getGLDetails(session, ctlNo);

				cstmdocInfo.setCostRecon(reconInfos);
				cstmdocInfo.setGlData(glInfos);

				if (vchrNo.length() > 0) {
					cstmdocInfo.setReferenceVoucherNo(vchrNo);
				} else {
					cstmdocInfo.setReferenceVoucherNo("");
				}

				starBrowseRes.output.fldTblDoc.add(cstmdocInfo);
			}

		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
			starBrowseRes.output.rtnSts = 1;
			starBrowseRes.output.messages.add(CommonConstants.SERVER_ERR);

			session.close();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public List<ReconInfo> getReconDetails(Session session, int ctlNo, String cmpyId) {
		String hql = "";
		List<ReconInfo> reconInfoList = new ArrayList<ReconInfo>();

		hql = " select * from cstm_vchr_recon_info where vchr_ctl_no=:ctl_no";

		Query queryValidate = session.createSQLQuery(hql);

		queryValidate.setParameter("ctl_no", ctlNo);
		CommonFunctions objCom = new CommonFunctions();
		List<Object[]> listRecon = queryValidate.list();

		for (Object[] reconInfo : listRecon) {

			ReconInfo recon = new ReconInfo();

			recon.setCrcnNo(Integer.parseInt(reconInfo[1].toString()));
			recon.setTrsNo(reconInfo[2].toString());
			recon.setPoNo(reconInfo[3].toString());
			recon.setCostNo(Integer.parseInt(reconInfo[4].toString()));
			recon.setBalAmt(Double.parseDouble(reconInfo[5].toString()));
			recon.setReconVenId(reconInfo[6].toString());

			int iSts = validateRecon(recon.getCrcnNo(), cmpyId);

			if (iSts == 0) {
				reconInfoList.add(recon);
			}
		}

		return reconInfoList;

	}

	public int validateRecon(int crcnNo, String cmpyId) {
		Session session = null;
		String hql = "";
		int iRcdCount = 0;

		try {
			session = SessionUtilInformix.getSession();

			hql = " select count(*) from irtcri_rec where cri_crcn_pfx = 'IR'";

			hql = hql + " and cri_cmpy_id=:cmpy_id";

			hql = hql + " and cri_crcn_no = :crcn_no";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", cmpyId);

			queryValidate.setParameter("crcn_no", crcnNo);

			iRcdCount = Integer.parseInt(String.valueOf(queryValidate.list().get(0)));
		} catch (Exception e) {
			logger.debug("Voucher Information : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return iRcdCount;

	}

	public List<GlInfo> getGLDetails(Session session, int ctlNo) {
		String hql = "";
		List<GlInfo> glInfos = new ArrayList<GlInfo>();

		hql = " select * from cstm_vchr_gl_info where vchr_ctl_no=:ctl_no";

		Query queryValidate = session.createSQLQuery(hql);

		queryValidate.setParameter("ctl_no", ctlNo);
		CommonFunctions objCom = new CommonFunctions();
		List<Object[]> listGL = queryValidate.list();

		for (Object[] gl : listGL) {

			GlInfo glInfo = new GlInfo();

			glInfo.setGlAcc(gl[1].toString());
			glInfo.setGlSubAcc(gl[2].toString());
			glInfo.setDrAmt(Double.parseDouble(gl[3].toString()));
			glInfo.setCrAmt(Double.parseDouble(gl[4].toString()));
			glInfo.setRemark(gl[5].toString());

			glInfos.add(glInfo);
		}

		return glInfos;
	}

	/* GET FIELDS FOR IN-PROCESS/PAID */
	public void isPrsOrPaidFields(BrowseResponse<CstmDocInfoBrowseOutput> starBrowseRes, Boolean isPrs)
			throws Exception {
		Session session = null;
		String hql = "";

		try {
			session = SessionUtil.getSession();

			hql = " select vchr_ven_nm,vchr_amt,vchr_inv_dt, is_prs,vchr_ven_id"
					+ " from cstm_doc_info doc_info , cstm_vchr_info vchr_info where" + " doc_info.is_prs=:is_prs and "
					+ " doc_info.ctl_no = vchr_info.vchr_ctl_no";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("is_prs", isPrs);
			CommonFunctions objCom = new CommonFunctions();
			List<Object[]> listCstmDocVal = queryValidate.list();

			for (Object[] cstmDocVal : listCstmDocVal) {
				VchrInfo cstmdocInfo = new VchrInfo();

				cstmdocInfo.setVchrVenNm(String.valueOf(cstmDocVal[0]));
				cstmdocInfo.setVchrAmt(Double.parseDouble(String.valueOf(cstmDocVal[1])));
				if (cstmDocVal[2] != null) {
					cstmdocInfo.setVchrInvDtStr((objCom.formatDateWithoutTime((Date) cstmDocVal[2])));
					cstmdocInfo.setVchrInvDt((Date) (cstmDocVal[2]));
				}
				cstmdocInfo.setIsPrs(Boolean.parseBoolean(String.valueOf(cstmDocVal[3])));
				cstmdocInfo.setVchrVenId(String.valueOf(cstmDocVal[4]));

				starBrowseRes.output.fldTblDoc.add(cstmdocInfo);
			}

		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
			starBrowseRes.output.rtnSts = 1;
			starBrowseRes.output.messages.add(CommonConstants.SERVER_ERR);

			session.close();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void delVchrInfo(int ctlNo, Session session) throws Exception {
		String hql = "";

		hql = "delete from cstm_vchr_info where vchr_ctl_no=:vchr_ctl_no";

		Query queryValidate = session.createSQLQuery(hql);
		queryValidate.setParameter("vchr_ctl_no", ctlNo);
		queryValidate.executeUpdate();
	}

	public void deleteDoc(MaintanenceResponse<CstmDocInfoManOutput> starManResponse, int ctlNo) {
		Session session = null;
		Transaction tx = null;
		String hql = "";

		WkfMapDAO wkfMapDAO = new WkfMapDAO();

		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			delVchrInfo(ctlNo, session);

			hql = "delete from cstm_doc_info where ctl_no=:ctl_no ";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("ctl_no", ctlNo);

			queryValidate.executeUpdate();

			wkfMapDAO.delWkfMap(session, ctlNo);
			wkfMapDAO.delWkfMapHis(session, ctlNo);

			tx.commit();

		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void rejectDoc(MaintanenceResponse<CstmDocInfoManOutput> starManResponse, int ctlNo, String trnRmk) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			hql = "update cstm_doc_info set trn_sts =:trn_sts, trn_rmk =:trn_rmk where ctl_no=:ctl_no ";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("ctl_no", ctlNo);
			queryValidate.setParameter("trn_sts", "R");
			queryValidate.setParameter("trn_rmk", trnRmk);

			queryValidate.executeUpdate();

			tx.commit();

		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public String validateInvoice(String venId, String invNo) {
		String vchrNo = "";

		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = " select cast(vch_vchr_pfx as varchar(2)) vchr_pfx, vch_vchr_no from aptvch_rec where 1=1"
					+ " AND vch_ven_id=:ven_id and vch_ven_inv_no=:inv_no";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("ven_id", venId);
			queryValidate.setParameter("inv_no", invNo);

			List<Object[]> listVoucher = queryValidate.list();

			for (Object[] voucher : listVoucher) {

				vchrNo = voucher[0].toString() + "-" + voucher[1].toString();

			}
		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return vchrNo;
	}

	public void getReconDetails(String cmpyId, String venId, String poNo, String cry, String extRef,
			VchrInfo vchrInfo) {
		Session session = null;
		String hql = "";
		CommonFunctions objCom = new CommonFunctions();
		session = SessionUtilInformix.getSession();

		hql = " select count(*) rcrd_cnt, coalesce(sum(crh_balamt),0) bal_amt from irtcrh_rec where crh_ven_id=:ven_id "
				+ "and crh_cry=:cry and crh_cmpy_id=:cmpy  and crh_balamt<>0 and crh_balamt <>crh_ip_amt";

		if (extRef.length() > 0) {
			hql += " and crh_extl_ref=:extRef";
		}

		Query queryValidate = session.createSQLQuery(hql);

		queryValidate.setParameter("ven_id", venId);
		queryValidate.setParameter("cry", cry);
		queryValidate.setParameter("cmpy", cmpyId);

		if (extRef.length() > 0) {
			queryValidate.setParameter("extRef", extRef);
		}

		List<Object[]> listReconInfo = queryValidate.list();

		for (Object[] voucher : listReconInfo) {

			vchrInfo.setReconCount(Integer.parseInt(voucher[0].toString()));
			vchrInfo.setReconAmt(Double.parseDouble(voucher[1].toString()));
			vchrInfo.setReconAmtStr(objCom.formatAmount(Double.parseDouble(String.valueOf(voucher[1]))));

		}

	}

	public String validatePO(String poNo, String poItm) {
		String vendorNo = "";

		Session session = null;
		String hql = "";

		session = SessionUtilInformix.getSession();

		if (poNo.length() > 0 && poItm.length() == 0) {
			hql = " select cast(poh_ven_id as varchar(8)),1 from potpoh_rec where poh_po_pfx='PO' and poh_po_no=:po_no";
		}

		if (poNo.length() > 0 && poItm.length() > 0) {
			hql = " select cast(poh_ven_id as varchar(8)),1 from potpoh_rec, potpoi_Rec where poh_po_pfx = poi_po_pfx "
					+ "and poh_po_no = poi_po_no and poh_po_pfx='PO' and poh_po_no=:po_no and poi_po_itm=:po_itm";
		}

		Query queryValidate = session.createSQLQuery(hql);

		if (poNo.length() > 0 && poItm.length() == 0) {
			queryValidate.setParameter("po_no", Integer.parseInt(poNo));
		}

		if (poNo.length() > 0 && poItm.length() > 0) {
			queryValidate.setParameter("po_no", Integer.parseInt(poNo));
			queryValidate.setParameter("po_itm", Integer.parseInt(poItm));
		}

		List<Object[]> listVoucher = queryValidate.list();

		for (Object[] voucher : listVoucher) {

			vendorNo = voucher[0].toString();

		}

		return vendorNo;
	}

	public String getVoucherList(List<CstmParamInv> cstmParamInvs) {

		String voucherList = "";

		for (int i = 0; i < cstmParamInvs.size(); i++) {

			voucherList = voucherList + cstmParamInvs.get(i).getInvNo() + ",";
		}

		if (voucherList.length() > 0) {
			voucherList = voucherList.substring(0, voucherList.length() - 1);
		}

		return voucherList;

	}

	public void updateDocStatus(MaintanenceResponse<CstmDocInfoManOutput> starManResponse, int ctlNo, String trnSts,
			String rmk) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			hql = "update cstm_doc_info set ";

			if (trnSts.length() == 0) {
				hql += " trn_sts = NULL, ";
			} else {
				hql += " trn_sts =:trn_sts, ";
			}

			hql += " trn_rmk =:trn_rmk where ctl_no=:ctl_no ";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("ctl_no", ctlNo);
			if (trnSts.length() > 0) {
				queryValidate.setParameter("trn_sts", trnSts);
			}
			queryValidate.setParameter("trn_rmk", rmk);

			queryValidate.executeUpdate();

			tx.commit();

		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	/* ADD ADDITIONAL DOCUMENT CHANGES */

	public int getAdditionalDocCount(String ctlNo) {
		Session session = null;
		String hql = "";
		int iRcdCount = 0;

		try {
			session = SessionUtil.getSession();

			hql = " select count(*) from cstm_doc_add_info where doc_ctl_no = :ctl_no";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("ctl_no", Integer.parseInt(ctlNo));

			iRcdCount = Integer.parseInt(String.valueOf(queryValidate.list().get(0)));

		} catch (Exception e) {
			logger.debug("Document Add Information : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return iRcdCount;

	}

	public void addCstmAddDocument(MaintanenceResponse<CstmDocInfoManOutput> maintanenceResponse,
			CstmDocAddInfoInput cstmDocInfoInp) {
		Session session = null;
		Transaction tx = null;
		try {
			Date date = new Date();

			session = SessionUtil.getSession();
			tx = session.beginTransaction();
			CstmDocAddInfo cstmDocAddInfo = new CstmDocAddInfo();

			cstmDocAddInfo.setDocCtlNo(cstmDocInfoInp.getDocCtlNo());
			cstmDocAddInfo.setFlNm(cstmDocInfoInp.getFlNm());
			cstmDocAddInfo.setFlOrgName(cstmDocInfoInp.getFlOrgName());
			cstmDocAddInfo.setCrtDtts(date);
			cstmDocAddInfo.setUpldBy(cstmDocInfoInp.getUpldBy());
			cstmDocAddInfo.setVchrNo(cstmDocInfoInp.getVchrNo());

			session.save(cstmDocAddInfo);

			tx.commit();
		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
			maintanenceResponse.output.messages.add(e.getMessage());
			maintanenceResponse.output.rtnSts = 1;

			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void getAdditionalDocList(BrowseResponse<CstmDocAddBrowseOutput> starBrowseRes, String ctlNo,
			String vchrNo) {
		Session session = null;
		String hql = "";
		CommonFunctions objCom = new CommonFunctions();
		ValidateValue validateValue = new ValidateValue();
		try {
			session = SessionUtil.getSession();

			hql = " select ctl_no, doc_ctl_no, fl_org_name, fl_nm, crt_dtts, "
					+ "(Select usr_nm from usr_dtls where usr_id=upld_by) usr_nm "
					+ "from cstm_doc_add_info where 1<>1 ";

			if (Integer.parseInt(ctlNo) > 0) {
				hql = hql + "or doc_ctl_no = :ctl_no";
			}

			if (vchrNo.length() > 0) {
				hql = hql + " or vchr_no = :vchr";
			}

			Query queryValidate = session.createSQLQuery(hql);

			if (Integer.parseInt(ctlNo) > 0) {
				queryValidate.setParameter("ctl_no", Integer.parseInt(ctlNo));
			}

			if (vchrNo.length() > 0) {
				queryValidate.setParameter("vchr", vchrNo);
			}

			List<Object[]> listAddDocument = queryValidate.list();

			for (Object[] document : listAddDocument) {

				CstmDocAddOutput addInfo = new CstmDocAddOutput();

				addInfo.setCtlNo(Integer.parseInt(document[0].toString()));
				addInfo.setDocCtlNo(Integer.parseInt(document[1].toString()));
				addInfo.setFlOrgName(document[2].toString());
				addInfo.setFlNm(document[3].toString());

				String fileExtn = validateValue.getFileExtension(addInfo.getFlNm());

				if (document[4] != null) {
					addInfo.setCrtDttsStr((objCom.formatDate((Date) document[4])));
					addInfo.setCrtDtts((Date) (document[4]));
				}

				addInfo.setDocExtn(fileExtn.toUpperCase());

				addInfo.setUpldBy(document[5].toString());

				starBrowseRes.output.fldTblDocAdd.add(addInfo);

			}

		} catch (Exception e) {
			logger.debug("Document Add Information : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void getAdditionalSTXDocList(BrowseResponse<CstmDocAddBrowseOutput> starBrowseRes, String poNo,
			String vchrNo, String cmpyId, String ctlNo) {
		Session session = null;
		String hql = "";
		String temp = "";
		CommonFunctions objCom = new CommonFunctions();
		ValidateValue validateValue = new ValidateValue();
		try {
			session = SessionUtilInformix.getSession();

			if (vchrNo.length() > 0) {
				
				
				/* GET RECEIPT FROM PO NO */
				hql = "select vch_po_no,1 from aptvch_rec where vch_cmpy_id=:cmpy_id and vch_vchr_pfx || '-' || vch_vchr_no = :vchr_no";
				
				Query queryValidate = session.createSQLQuery(hql);

				queryValidate.setParameter("cmpy_id", cmpyId);
				queryValidate.setParameter("vchr_no", vchrNo);
				
				List<Object[]> listPO = queryValidate.list();

				for (Object[] poVal : listPO) {

					temp = poVal[0].toString();
				}
				
				hql = " select arh_doc_pfx, arh_arch_ctl_no, arh_arch_ver_no, "
						+ "arh_arch_ctl_itm,cast(arh_prim_ref as varchar(30)) as prim_ref, arh_gen_dtts, "
						+ "(select cast(usr_nm as varchar(35)) from mxrusr_rec where usr_lgn_id=arh_lgn_id) usr_nm "
						+ "from mxtarh_rec where arh_cmpy_id=:cmpy_id and arh_bus_doc_typ='PO' and ";
						
				if (CommonConstants.DB_TYP.equals("POS")) {
					hql = hql + " arh_prim_ref like '%" + createOrdStr(temp) + "%'";
				}
				else
				{
					hql = hql + " arh_prim_ref like '%" + createOrdStr(temp) + "%'";
				}
				
				hql = hql + "order by  arh_arch_ver_no desc limit 1";

				queryValidate = session.createSQLQuery(hql);
				
				queryValidate.setParameter("cmpy_id", cmpyId);

				List<Object[]> listAddDocument = queryValidate.list();

				for (Object[] document : listAddDocument) {

					CstmDocAddOutput addInfo = new CstmDocAddOutput();

					addInfo.setDocPfx(document[0].toString());
					addInfo.setDocArcCtlNo(document[1].toString());
					addInfo.setDocArcVerNo(document[2].toString());
					addInfo.setDocArcCtlItm(document[3].toString());
					addInfo.setFlOrgName(document[4].toString() + ".pdf");

					if (document[5] != null) {
						addInfo.setCrtDttsStr((objCom.formatDate((Date) document[5])));
						addInfo.setCrtDtts((Date) (document[5]));
					}

					addInfo.setDocExtn("PDF");

					addInfo.setUpldBy(document[6].toString());

					starBrowseRes.output.fldTblDocAddStx.add(addInfo);

				}
				
				/* GET RECEIPT FROM PO NO */
				hql = "select distinct rci_rcpt_no,1 from trtrci_rec,aptvch_rec where rci_cmpy_id = vch_cmpy_id and "
						+ "rci_prnt_pfx = vch_po_pfx and rci_prnt_no = vch_po_no "
						+ " and vch_cmpy_id = :cmpy_id and vch_vchr_pfx || '-' || vch_vchr_no = :vchr_no";
				
				queryValidate = session.createSQLQuery(hql);

				queryValidate.setParameter("cmpy_id", cmpyId);
				queryValidate.setParameter("vchr_no", vchrNo);
				
				List<Object[]> listRcpt = queryValidate.list();

				for (Object[] rcptVal : listRcpt) {

					temp = rcptVal[0].toString();
				}
				
				hql = " select arh_doc_pfx, arh_arch_ctl_no, arh_arch_ver_no, "
						+ " arh_arch_ctl_itm,cast(arh_prim_ref as varchar(30)) as prim_ref, arh_gen_dtts, "
						+ " (select cast(usr_nm as varchar(35)) from mxrusr_rec where usr_lgn_id=arh_lgn_id) usr_nm from mxtarh_rec where"
						+ " arh_cmpy_id=:cmpy_id and "
						+ " arh_bus_doc_typ='RCV' and ";
						
				if (CommonConstants.DB_TYP.equals("POS")) {
					hql = hql + " arh_prim_ref like '%" + createOrdStr(temp) + "%'";
				}
				else
				{
					hql = hql + " arh_prim_ref like '%" + createOrdStr(temp) + "%'";
				}
				
				hql = hql + " order by  arh_arch_ver_no desc limit 1 ";

				queryValidate = session.createSQLQuery(hql);

				queryValidate.setParameter("cmpy_id", cmpyId);
				
				listAddDocument = queryValidate.list();

				for (Object[] document : listAddDocument) {

					CstmDocAddOutput addInfo = new CstmDocAddOutput();

					addInfo.setDocPfx(document[0].toString());
					addInfo.setDocArcCtlNo(document[1].toString());
					addInfo.setDocArcVerNo(document[2].toString());
					addInfo.setDocArcCtlItm(document[3].toString());
					addInfo.setFlOrgName(document[4].toString() + ".pdf");

					if (document[5] != null) {
						addInfo.setCrtDttsStr((objCom.formatDate((Date) document[5])));
						addInfo.setCrtDtts((Date) (document[5]));
					}

					addInfo.setDocExtn("PDF");

					addInfo.setUpldBy(document[6].toString());

					starBrowseRes.output.fldTblDocAddStx.add(addInfo);
				}	
				
				/* GET DATA FROM VOUCHER RECONCILIATION RECORDS */
				hql = "select cast(crh_trs_pfx as varchar(2)) trs_pfx,crh_trs_no from irtcri_rec ri, irtcrh_rec rh where crh_cmpy_id = cri_cmpy_id " + 
						" and crh_crcn_pfx = cri_crcn_pfx and crh_crcn_no = cri_crcn_no" + 
						" and cri_ref_pfx || '-' || cri_ref_no = :vchr_no and cri_cmpy_id=:cmpy_id " +
						" union select jci_trs_pfx,jci_trs_no from apjjci_rec where "
						+ "jci_cmpy_id=:cmpy_id and jci_vchr_pfx ||'-'|| jci_vchr_no= :vchr_no";
		
				queryValidate = session.createSQLQuery(hql);

				queryValidate.setParameter("cmpy_id", cmpyId);
				queryValidate.setParameter("vchr_no", vchrNo);
				
				List<Object[]> listDoc = queryValidate.list();

				for (Object[] docTrnVal : listDoc) {

					String refPfx  = docTrnVal[0].toString();
					String refNo  = docTrnVal[1].toString();
					
					hql = " select arh_doc_pfx, arh_arch_ctl_no, arh_arch_ver_no, "
							+ " arh_arch_ctl_itm,cast(arh_prim_ref as varchar(30)) as prim_ref, arh_gen_dtts, "
							+ " (select cast(usr_nm as varchar(35)) from mxrusr_rec where usr_lgn_id=arh_lgn_id) usr_nm from mxtarh_rec where"
							+ " arh_cmpy_id=:cmpy_id and ";
							
						hql = hql + " arh_prim_ref like '" + refPfx + createOrdStr(refNo) + "%'";
					
					hql = hql + " order by  arh_arch_ver_no desc limit 1 ";

					queryValidate = session.createSQLQuery(hql);

					queryValidate.setParameter("cmpy_id", cmpyId);
					
					listAddDocument = queryValidate.list();

					for (Object[] document : listAddDocument) {

						CstmDocAddOutput addInfo = new CstmDocAddOutput();

						addInfo.setDocPfx(document[0].toString());
						addInfo.setDocArcCtlNo(document[1].toString());
						addInfo.setDocArcVerNo(document[2].toString());
						addInfo.setDocArcCtlItm(document[3].toString());
						addInfo.setFlOrgName(document[4].toString() + ".pdf");

						if (document[5] != null) {
							addInfo.setCrtDttsStr((objCom.formatDate((Date) document[5])));
							addInfo.setCrtDtts((Date) (document[5]));
						}

						addInfo.setDocExtn("PDF");

						addInfo.setUpldBy(document[6].toString());

						starBrowseRes.output.fldTblDocAddStx.add(addInfo);
					}	
					
				}
				
				
			} else 
			{
				if (poNo.length() > 0) {
				hql = " select arh_doc_pfx, arh_arch_ctl_no, arh_arch_ver_no, "
						+ " arh_arch_ctl_itm,cast(arh_prim_ref as varchar(30)) as prim_ref, arh_gen_dtts, "
						+ " (select cast(usr_nm as varchar(35)) from mxrusr_rec where usr_lgn_id=arh_lgn_id) usr_nm from mxtarh_rec "
						+ "where arh_cmpy_id=:cmpy_id and arh_bus_doc_typ='PO' and ";
						
				if (CommonConstants.DB_TYP.equals("POS")) {
					hql = hql + " arh_prim_ref like '%" + createOrdStr(poNo) + "%'";
				}
				else
				{
					hql = hql + " arh_prim_ref like '%" + createOrdStr(poNo) +"%'";
				}
				
				hql = hql + " order by  arh_arch_ver_no desc limit 1 ";
								
				Query queryValidate = session.createSQLQuery(hql);
				
				queryValidate.setParameter("cmpy_id", cmpyId);
				
				List<Object[]> listAddDocument = queryValidate.list();

				for (Object[] document : listAddDocument) {

					CstmDocAddOutput addInfo = new CstmDocAddOutput();

					addInfo.setDocPfx(document[0].toString());
					addInfo.setDocArcCtlNo(document[1].toString());
					addInfo.setDocArcVerNo(document[2].toString());
					addInfo.setDocArcCtlItm(document[3].toString());
					addInfo.setFlOrgName(document[4].toString() + ".pdf");

					if (document[5] != null) {
						addInfo.setCrtDttsStr((objCom.formatDate((Date) document[5])));
						addInfo.setCrtDtts((Date) (document[5]));
					}

					addInfo.setDocExtn("PDF");

					addInfo.setUpldBy(document[6].toString());

					starBrowseRes.output.fldTblDocAddStx.add(addInfo);
				}
				
				/* GET RECEIPT FROM PO NO */
				hql = "select rci_rcpt_no,1 from trtrci_rec where rci_cmpy_id=:cmpy_id and rci_rcpt_pfx='RC' and rci_prnt_pfx='PO' and rci_prnt_no=:po_no";
				
				queryValidate = session.createSQLQuery(hql);

				queryValidate.setParameter("po_no", Integer.parseInt(poNo));
				queryValidate.setParameter("cmpy_id", cmpyId);
				
				List<Object[]> listPoNo = queryValidate.list();

				for (Object[] poNoVal : listPoNo) {

					temp = poNoVal[0].toString();
				}
				
				
				hql = " select arh_doc_pfx, arh_arch_ctl_no, arh_arch_ver_no, "
						+ " arh_arch_ctl_itm,cast(arh_prim_ref as varchar(30)) as prim_ref, arh_gen_dtts, "
						+ " (select cast(usr_nm as varchar(35)) from mxrusr_rec where usr_lgn_id=arh_lgn_id) usr_nm from mxtarh_rec "
						+ "where arh_cmpy_id=:cmpy_id and arh_bus_doc_typ='RCV' and ";
						
				if (CommonConstants.DB_TYP.equals("POS")) {
					hql = hql + " arh_prim_ref like '%" + createOrdStr(temp) +"%'";
				}
				else
				{
					hql = hql + " arh_prim_ref like '%" + createOrdStr(temp) +"%'";
				}
				
				hql = hql + " order by  arh_arch_ver_no desc limit 1 ";

				queryValidate = session.createSQLQuery(hql);
				
				queryValidate.setParameter("cmpy_id", cmpyId);

				listAddDocument = queryValidate.list();

				for (Object[] document : listAddDocument) {

					CstmDocAddOutput addInfo = new CstmDocAddOutput();

					addInfo.setDocPfx(document[0].toString());
					addInfo.setDocArcCtlNo(document[1].toString());
					addInfo.setDocArcVerNo(document[2].toString());
					addInfo.setDocArcCtlItm(document[3].toString());
					addInfo.setFlOrgName(document[4].toString() + ".pdf");

					if (document[5] != null) {
						addInfo.setCrtDttsStr((objCom.formatDate((Date) document[5])));
						addInfo.setCrtDtts((Date) (document[5]));
					}

					addInfo.setDocExtn("PDF");

					addInfo.setUpldBy(document[6].toString());

					starBrowseRes.output.fldTblDocAddStx.add(addInfo);
				}
				
			}
				
				/* GET DATA FROM VOUCHER RECONCILIATION RECORDS */
				hql = "select distinct cast(split_part(vchr_trs_no,'-',1) as varchar(2)) ref_pfx, split_part(vchr_trs_no,'-',2) from "
						+ "	cstm_vchr_recon_info cvri where vchr_ctl_no=:ctl_no";
				
				session = SessionUtil.getSession();
				
				Query queryValidate = session.createSQLQuery(hql);

				queryValidate.setParameter("ctl_no", Integer.parseInt(ctlNo));
				
				List<Object[]> listDoc = queryValidate.list();

				for (Object[] docTrnVal : listDoc) {
					
					String refPfx  = docTrnVal[0].toString();
					String refNo  = docTrnVal[1].toString();
					
					hql = " select arh_doc_pfx, arh_arch_ctl_no, arh_arch_ver_no, "
							+ " arh_arch_ctl_itm,cast(arh_prim_ref as varchar(30)) as prim_ref, arh_gen_dtts, "
							+ " (select cast(usr_nm as varchar(35)) from mxrusr_rec where usr_lgn_id=arh_lgn_id) usr_nm from mxtarh_rec where"
							+ " arh_cmpy_id=:cmpy_id and ";
							
						hql = hql + " arh_prim_ref like '" + refPfx + createOrdStr(refNo) + "%'";
					
					hql = hql + " order by  arh_arch_ver_no desc limit 1 ";
					
					session = SessionUtilInformix.getSession();
					
					queryValidate = session.createSQLQuery(hql);

					queryValidate.setParameter("cmpy_id", cmpyId);
					
					List<Object[]> listAddDocument = queryValidate.list();

					for (Object[] document : listAddDocument) {

						CstmDocAddOutput addInfo = new CstmDocAddOutput();

						addInfo.setDocPfx(document[0].toString());
						addInfo.setDocArcCtlNo(document[1].toString());
						addInfo.setDocArcVerNo(document[2].toString());
						addInfo.setDocArcCtlItm(document[3].toString());
						addInfo.setFlOrgName(document[4].toString() + ".pdf");

						if (document[5] != null) {
							addInfo.setCrtDttsStr((objCom.formatDate((Date) document[5])));
							addInfo.setCrtDtts((Date) (document[5]));
						}

						addInfo.setDocExtn("PDF");

						addInfo.setUpldBy(document[6].toString());

						starBrowseRes.output.fldTblDocAddStx.add(addInfo);
					}	
					
				}
			
			
		}

		} catch (Exception e) {
			logger.debug("Document Add Information : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	/* DELETE ADDITIONAL DOCUMENT */
	public void deleteAdditionDoc(MaintanenceResponse<CstmDocInfoManOutput> maintanenceResponse, String ctlNo)
			throws Exception {
		String hql = "";
		Session session = null;
		Transaction tx = null;

		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();
			hql = "delete from cstm_doc_add_info where ctl_no=:ctl_no";

			Query queryValidate = session.createSQLQuery(hql);
			queryValidate.setParameter("ctl_no", Integer.parseInt(ctlNo));
			queryValidate.executeUpdate();

			tx.commit();
		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
			maintanenceResponse.output.rtnSts = 1;
			maintanenceResponse.output.messages.add(CommonConstants.SERVER_ERR);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	/* GET Pdf name and it's Original Name */
	public List<Object[]> getDocAddtn(int ctlNo) throws Exception {
		Session session = null;
		String hql = "";
		List<Object[]> listCstmDocVal = null;
		try {
			session = SessionUtil.getSession();

			hql = "select fl_org_name, fl_nm from cstm_doc_add_info where ctl_no = :ctlNo";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("ctlNo", ctlNo);

			listCstmDocVal = queryValidate.list();

		} catch (Exception e) {

			logger.debug("Document : {}", e.getMessage(), e);
			session.close();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return listCstmDocVal;
	}
	
	public String createOrdStr(String ordNo)
	{
		String ordStr = "";
		
		for(int i = ordNo.length(); i < 8; i++)
		{
			ordStr = ordStr + "0";
		}
		
		ordStr = ordStr + ordNo;
		
		return ordStr;
	}
	
}
