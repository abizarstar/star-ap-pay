package com.star.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.MaintanenceResponse;
import com.star.common.SessionUtil;
import com.star.linkage.ebstypecode.EBSBrowseOutput;
import com.star.linkage.ebstypecode.EBSGLAccountInfo;
import com.star.linkage.ebstypecode.EBSInfo;
import com.star.linkage.ebstypecode.GLEBSInfoBrowseOutput;
import com.star.linkage.ebstypecode.GLEBSMapBrowseOutput;
import com.star.linkage.ebstypecode.GLEBSMapInput;
import com.star.linkage.ebstypecode.GLEBSMapMntOutput;
import com.star.linkage.ebstypecode.TypeCodeBrowseOutput;
import com.star.linkage.ebstypecode.TypeCodeInfo;
import com.star.modal.GlEbsMap;

public class EBSTypeCodeDAO {

	private static Logger logger = LoggerFactory.getLogger(EBSTypeCodeDAO.class);
	
	public void readEBSList(BrowseResponse<EBSBrowseOutput> starBrowseResponse)
	{
		Session session = null;
		String hql = "";

		try {
			session = SessionUtil.getSession();

			hql = " select id, ebs_nm from ebs_type order by ebs_nm";

			Query queryValidate = session.createSQLQuery(hql);
						
			List<Object[]> listEBS = queryValidate.list();

			for (Object[] ebs : listEBS) {

				EBSInfo output = new EBSInfo();

				output.setEbsId(ebs[0].toString());
				output.setEbsNm(ebs[1].toString());
				
				starBrowseResponse.output.fldTblEbs.add(output);

			}

		} catch (Exception e) {
			
			logger.debug("EBS Type Code Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			session.close();
		}

	}

	public void readTypeCodeList(BrowseResponse<TypeCodeBrowseOutput> starBrowseResponse, String ebsId)
	{
		Session session = null;
		String hql = "";

		try {
			session = SessionUtil.getSession();

			hql = " select typ_cd, typ_nm from ebs_type_codes where ebs_id=:ebs_id order by typ_nm";

			Query queryValidate = session.createSQLQuery(hql);
			
			queryValidate.setParameter("ebs_id", Integer.parseInt(ebsId));
						
			List<Object[]> listType = queryValidate.list();

			for (Object[] type : listType) {

				TypeCodeInfo output = new TypeCodeInfo();

				output.setTypCd(type[0].toString());
				output.setTypNm(type[1].toString());
				
				starBrowseResponse.output.fldTblTypeCode.add(output);

			}

		} catch (Exception e) {
			// TODO: handle exception
			logger.debug("EBS Type Code Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			session.close();
		}

	}
	
	
	public String readTypeCodeName(String typCd)
	{
		Session session = null;
		String hql = "";
		String typeCodeNm = "";

		try {
			session = SessionUtil.getSession();

			hql = "select typ_cd, typ_nm from ebs_type_codes where typ_cd=:typ_cd";

			Query queryValidate = session.createSQLQuery(hql);
			
			queryValidate.setParameter("typ_cd", typCd);
			
			List<Object[]> listTypeCode = queryValidate.list();

			for (Object[] typeCode : listTypeCode) {

				typeCodeNm = typeCode[1].toString();
			}

		} catch (Exception e) {
			logger.debug("EBS Type Code Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return typeCodeNm;

	}
	
	public void addGlEbsMapping(MaintanenceResponse<GLEBSMapMntOutput> starManResponse, List<GLEBSMapInput> glEbsMapInput) {
		
		deleteGLEBSMapping(starManResponse, glEbsMapInput);
		
		if(starManResponse.output.rtnSts == 0)
		{
			addGLEBSMapping(starManResponse, glEbsMapInput);
		}
		
	}
	
	public void deleteGLEBSMapping(MaintanenceResponse<GLEBSMapMntOutput> starManResponse, List<GLEBSMapInput> glEbsMapInput) {
		Session session = null;
		Transaction tx = null;
		String hql = "";

		try {
			
			session = SessionUtil.getSession();
			tx = session.beginTransaction();
			
			for(int i = 0; i < glEbsMapInput.size(); i++)
			{
				hql = "delete from gl_ebs_map where ebs_id=:ebs_id and typ_cd=:typ_cd";
				Query queryValidate = session.createSQLQuery(hql);
				queryValidate.setParameter("ebs_id", Integer.parseInt(glEbsMapInput.get(i).getEbsId()));
				queryValidate.setParameter("typ_cd", glEbsMapInput.get(i).getTypCd());
				queryValidate.executeUpdate();
			}
			tx.commit();

		} catch (Exception e) {
			// TODO: handle exception
			logger.debug("EBS Type Code Info : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(CommonConstants.SERVER_ERR);

		} finally {

			session.close();
		}
	}
	
	public void addGLEBSMapping(MaintanenceResponse<GLEBSMapMntOutput> maintanenceResponse, List<GLEBSMapInput> glEbsMapInput) {
		Session session = null;
		Transaction tx = null;
		try {

			session = SessionUtil.getSession();
			tx = session.beginTransaction();
			
			for(int i = 0; i < glEbsMapInput.size(); i++)
			{
				GlEbsMap addMapping = new GlEbsMap();
				
				addMapping.setEbsId( Integer.parseInt(glEbsMapInput.get(i).getEbsId()) );
				addMapping.setTypCd(glEbsMapInput.get(i).getTypCd());
				addMapping.setPost1(glEbsMapInput.get(i).getPost1());
				addMapping.setPost2(glEbsMapInput.get(i).getPost2());
				
				session.save(addMapping);
			}
			
			
			
			tx.commit();
		} catch (Exception e) {
			logger.debug("EBS Type Code Info : {}", e.getMessage(), e);
			maintanenceResponse.output.messages.add(e.getMessage());
			maintanenceResponse.output.rtnSts = 1;

			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
	
	public void checkGlEBSMappping(BrowseResponse<GLEBSMapBrowseOutput> starBrowseResponse, String ebsId, String typCd)
	{
		Session session = null;
		String hql = "";
		String glAcct = "";

		try {
			session = SessionUtil.getSession();
			hql = " select gl_acct from gl_ebs_map where ebs_id=:ebs_id AND typ_cd=:typ_cd";

			Query queryValidate = session.createSQLQuery(hql);
			
			queryValidate.setParameter("ebs_id", Integer.parseInt(ebsId));
			queryValidate.setParameter("typ_cd", typCd);
			
			if(queryValidate.list().size() > 0)
			{
				glAcct = queryValidate.list().get(0).toString();
			}
						
			GLEBSMapInput output = new GLEBSMapInput();

			/*output.setGlAcct(glAcct);*/
			
			starBrowseResponse.output.fldTblGlEbsMap.add(output);

		} catch (Exception e) {
			// TODO: handle exception
			logger.debug("EBS Type Code Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			session.close();
		}

	}
	
	public void readGlEBSMapping(BrowseResponse<GLEBSMapBrowseOutput> starBrowseResponse, String ebsId)
	{
		Session session = null;
		String hql = "";

		try {
			session = SessionUtil.getSession();

			hql = 	"SELECT C.ebs_id, T.ebs_nm, C.typ_cd, C.typ_nm, "
					+ "(select post_1 from gl_ebs_map where ebs_id=C.ebs_id and typ_cd=C.typ_cd) post_1, "
					+ "(select post_2 from gl_ebs_map where ebs_id=C.ebs_id and typ_cd=C.typ_cd) post_2 "
					+ "FROM ebs_type T, ebs_type_codes C where  C.ebs_id = T.id and C.ebs_id=:ebs_id";

			Query queryValidate = session.createSQLQuery(hql);
			
			queryValidate.setParameter("ebs_id", Integer.parseInt(ebsId));
						
			List<Object[]> listMapping = queryValidate.list();

			for (Object[] mapping : listMapping) {

				GLEBSMapInput output = new GLEBSMapInput();

				output.setEbsId(mapping[0].toString());
				output.setEbsNm(mapping[1].toString());
				output.setTypCd(mapping[2].toString());
				output.setTypNm(mapping[3].toString());
				
				if(mapping[4] != null)
				{
					output.setPost1(mapping[4].toString());
				}
				else
				{
					output.setPost1("");
				}
				
				if(mapping[5] != null)
				{
					output.setPost2(mapping[5].toString());
				}
				else
				{
					output.setPost2("");
				}
				
				starBrowseResponse.output.fldTblGlEbsMap.add(output);

			}

		} catch (Exception e) {
			// TODO: handle exception
			logger.debug("EBS Type Code Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			session.close();
		}

	}
	
	public int validateGlEBSMappping(String ebsId, String typCd)
	{
		Session session = null;
		String hql = "";
		int ircrdCount = 0;

		try {
			session = SessionUtil.getSession();

			hql = " select count(*) from gl_ebs_map where ebs_id=:ebs_id AND typ_cd=:typ_cd and gl_trn_typ in ('C', 'D')";

			Query queryValidate = session.createSQLQuery(hql);
			
			queryValidate.setParameter("ebs_id", Integer.parseInt(ebsId));
			queryValidate.setParameter("typ_cd", typCd);
			
			ircrdCount = Integer.parseInt(String.valueOf(queryValidate.list().get(0)));
						

		} catch (Exception e) {
			// TODO: handle exception
			logger.debug("EBS Type Code Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}
		
		return ircrdCount;
		
	}
	
	public int validateAccountComb(String ebsId, String typCd, String acct, String subAcct, String trnTyp)
	{
		Session session = null;
		String hql = "";
		int ircrdCount = 0;

		try {
			session = SessionUtil.getSession();

			hql = " select count(*) from gl_ebs_map where ebs_id=:ebs_id AND "
					+ "typ_cd=:typ_cd and gl_trn_typ=:trn and gl_acct=:acct and gl_sub_acct=:sub_acct";

			Query queryValidate = session.createSQLQuery(hql);
			
			queryValidate.setParameter("ebs_id", Integer.parseInt(ebsId));
			queryValidate.setParameter("typ_cd", typCd);
			queryValidate.setParameter("trn", trnTyp);
			queryValidate.setParameter("acct", acct);
			queryValidate.setParameter("sub_acct", subAcct);
			
			ircrdCount = Integer.parseInt(String.valueOf(queryValidate.list().get(0)));
						

		} catch (Exception e) {
			// TODO: handle exception
			logger.debug("EBS Type Code Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}
		
		return ircrdCount;
		
	}
	
	public void getGLAccount(BrowseResponse<GLEBSInfoBrowseOutput> starBrowseResponse, String ebsId, String typCd) {
		Session session = null;
		String hql = "";

		try {
			session = SessionUtil.getSession();

			hql = " SELECT info1.post_cr_acct_1 gl_acct1, info1.post_cr_sub_acct_1 gl_sub_acct1, info1.post_cr_acct_2 gl_acct2, "
					+ "info1.post_cr_sub_acct_2 gl_sub_acct2, info1.post_cr_acct_3 gl_acct3, info1.post_cr_sub_acct_3 gl_sub_acct3, "
					+ "'P1' as posting, 'C' as trn_type FROM gl_ebs_map, gl_posting_info info1 WHERE post_1 = post_id "
					+ "AND   ebs_id =:ebs_id AND   typ_cd =:typ_cd " + "UNION "
					+ "SELECT info1.post_dr_acct_1 gl_acct1, info1.post_dr_sub_acct_1 gl_sub_acct1, info1.post_dr_acct_2 gl_acct2, "
					+ "info1.post_dr_sub_acct_2 gl_sub_acct2, info1.post_dr_acct_3 gl_acct3, info1.post_dr_sub_acct_3 gl_sub_acct3, "
					+ "'P1', 'D' FROM gl_ebs_map, gl_posting_info info1 WHERE post_1 = post_id "
					+ "AND   ebs_id =:ebs_id AND   typ_cd =:typ_cd " + "UNION "
					+ "SELECT info1.post_cr_acct_1 gl_acct1, info1.post_cr_sub_acct_1 gl_sub_acct1, info1.post_cr_acct_2 gl_acct2, "
					+ "info1.post_cr_sub_acct_2 gl_sub_acct2, info1.post_cr_acct_3 gl_acct3, info1.post_cr_sub_acct_3 gl_sub_acct3, "
					+ "'P2', 'C' FROM gl_ebs_map, gl_posting_info info1 WHERE post_2 = post_id "
					+ "AND   ebs_id =:ebs_id AND   typ_cd =:typ_cd " + "UNION "
					+ "SELECT info1.post_dr_acct_1 gl_acct1, info1.post_dr_sub_acct_1 gl_sub_acct1, info1.post_dr_acct_2 gl_acct2, "
					+ "info1.post_dr_sub_acct_2 gl_sub_acct2, info1.post_dr_acct_3 gl_acct3, info1.post_dr_sub_acct_3 gl_sub_acct3, "
					+ "'P2', 'D' FROM gl_ebs_map, gl_posting_info info1 WHERE post_2 = post_id "
					+ "AND   ebs_id =:ebs_id AND   typ_cd =:typ_cd";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("ebs_id", Integer.parseInt(ebsId));
			queryValidate.setParameter("typ_cd", typCd);

			List<Object[]> listMapping = queryValidate.list();

			for (Object[] mapping : listMapping) {
				EBSGLAccountInfo output = new EBSGLAccountInfo();

				output.setGlAcct(mapping[0].toString());
				output.setGlSubAcct(mapping[1].toString());
				
				if(mapping[2] != null)
				{
					output.setGlAcct1(mapping[2].toString());
				}
				else
				{
					output.setGlAcct1("");
				}

				if(mapping[3] != null)
				{
					output.setGlSubAcct1(mapping[3].toString());
				}
				else
				{
					output.setGlSubAcct1("");
				}
				
				if(mapping[4] != null)
				{
					output.setGlAcct2(mapping[4].toString());
				}
				else
				{
					output.setGlAcct2("");
				}

				if(mapping[5] != null)
				{
					output.setGlSubAcct2(mapping[5].toString());
				}
				else
				{
					output.setGlSubAcct2("");
				}
				
				output.setPosSeq(mapping[6].toString());
				output.setTrnTyp(mapping[7].toString());
				
				starBrowseResponse.output.fldTblGlEbsMap.add(output);

			}

		} catch (Exception e) {
			logger.debug("GL Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}
	
}
