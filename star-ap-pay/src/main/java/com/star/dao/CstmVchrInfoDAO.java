package com.star.dao;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.CommonFunctions;
import com.star.common.MaintanenceResponse;
import com.star.common.SessionUtil;
import com.star.common.SessionUtilInformix;
import com.star.linkage.cstmdocinfo.ReconInfo;
import com.star.linkage.cstmvchrinfo.CstmVchrInfoBrowseOutput;
import com.star.linkage.cstmvchrinfo.CstmVchrInfoInput;
import com.star.linkage.cstmvchrinfo.CstmVchrInfoManOutput;
import com.star.linkage.cstmvchrinfo.VoucherReconInfo;
import com.star.linkage.cstmvchrinfo.VoucherReconInfoBrowseOutput;
import com.star.linkage.execcnfg.ExecCnfgBrowseOutput;
import com.star.linkage.export.CostReconExport;
import com.star.modal.CstmVchrInfo;

public class CstmVchrInfoDAO {

	private static Logger logger = LoggerFactory.getLogger(CstmVchrInfoDAO.class);

	public void addCstmVoucher(MaintanenceResponse<CstmVchrInfoManOutput> maintanenceResponse,
			CstmVchrInfoInput cstmVchrInfoInp) {
		Session session = null;
		Transaction tx = null;
		try {
			Date date = new Date();

			session = SessionUtil.getSession();
			tx = session.beginTransaction();
			CstmVchrInfo cstmVchrInfo = new CstmVchrInfo();

			cstmVchrInfo.setVchrCtlNo(cstmVchrInfoInp.getVchrCtlNo());
			cstmVchrInfo.setVchrInvNo(cstmVchrInfoInp.getVchrInvNo());
			cstmVchrInfo.setVchrVenId(cstmVchrInfoInp.getVchrVenId());
			cstmVchrInfo.setVchrVenNm(cstmVchrInfoInp.getVchrVenNm());
			cstmVchrInfo.setVchrBrh(cstmVchrInfoInp.getVchrBrh());
			cstmVchrInfo.setVchrInvDt(cstmVchrInfoInp.getVchrInvDt());
			cstmVchrInfo.setVchrDueDt(cstmVchrInfoInp.getVchrDueDt());
			cstmVchrInfo.setVchrExtRef(cstmVchrInfoInp.getVchrExtRef());
			cstmVchrInfo.setVchrPayTerm(cstmVchrInfoInp.getVchrPayTerm());
			cstmVchrInfo.setVchrAmt(cstmVchrInfoInp.getVchrAmt());
			cstmVchrInfo.setVchrCry(cstmVchrInfoInp.getVchrCry());
			cstmVchrInfo.setVchrCat(cstmVchrInfoInp.getVchrCat());

			session.save(cstmVchrInfo);

			tx.commit();
		} catch (Exception e) {
			logger.debug("Voucher Information : {}", e.getMessage(), e);
			maintanenceResponse.output.messages.add(e.getMessage());
			maintanenceResponse.output.rtnSts = 1;

			e.printStackTrace();
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	/* GET ALL THE DEFINE FIELDS LIST */
	public void readFields(BrowseResponse<CstmVchrInfoBrowseOutput> starBrowseRes) throws Exception {
		Session session = null;
		String hql = "";

		try {
			session = SessionUtil.getSession();

			hql = " select vchr_ctl_no,vchr_inv_no,vchr_ven_id,vchr_ven_nm,vchr_brh,vchr_amt,vchr_ext_ref,vchr_inv_dt,vchr_due_dt,vchr_pay_term,vchr_cry from cstm_vchr_info";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listCstmVchrVal = queryValidate.list();

			for (Object[] cstmVchrVal : listCstmVchrVal) {
				CstmVchrInfo cstmVchrInfo = new CstmVchrInfo();

				cstmVchrInfo.setVchrCtlNo((Integer) (cstmVchrVal[0]));
				cstmVchrInfo.setVchrInvNo(String.valueOf(cstmVchrVal[1]));
				cstmVchrInfo.setVchrVenId(String.valueOf(cstmVchrVal[2]));
				cstmVchrInfo.setVchrVenNm(String.valueOf(cstmVchrVal[3]));
				cstmVchrInfo.setVchrBrh(String.valueOf(cstmVchrVal[4]));
				// cstmVchrInfo.setVchrAmt((double) (cstmVchrVal[5]));
				cstmVchrInfo.setVchrExtRef(String.valueOf(cstmVchrVal[6]));
				cstmVchrInfo.setVchrInvDt((Date) (cstmVchrVal[7]));
				cstmVchrInfo.setVchrDueDt((Date) (cstmVchrVal[8]));
				cstmVchrInfo.setVchrPayTerm(String.valueOf(cstmVchrVal[9]));
				cstmVchrInfo.setVchrCry(String.valueOf(cstmVchrVal[10]));

				starBrowseRes.output.fldTblDoc.add(cstmVchrInfo);
			}

		} catch (Exception e) {

			logger.debug("Voucher Information : {}", e.getMessage(), e);
			starBrowseRes.output.rtnSts = 1;
			starBrowseRes.output.messages.add(CommonConstants.SERVER_ERR);

			session.close();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	/* GET ALL THE DEFINE FIELDS LIST */
	public void readVoucherRecon(BrowseResponse<VoucherReconInfoBrowseOutput> starBrowseRes, String cmpyId,
			String brhId, String venId, String cry, String ctlNo) throws Exception {
		Session session = null;
		String hql = "";
		CommonFunctions objCom = new CommonFunctions();

		try {
			session = SessionUtilInformix.getSession();

			if (CommonConstants.DB_TYP.equals("POS")) {
				hql = " SELECT crh_po_no, crh_po_itm, crh_crcn_no, cast(crh_crcn_brh as varchar(3)) as brh, cast(crh_trs_pfx as varchar(2)) trs_pfx, crh_trs_no, crh_trs_itm, "
						+ "crh_trs_sbitm, crh_bal_qty, crh_orig_amt, crh_balamt, to_char(crh_actvy_dt, 'MM/dd/yyyy') actvy_dt, crh_cc_no, cast(ccr_desc20 as varchar(20)) cost_desc, "
						+ "(select string_agg(cri_ref_pfx||'-'||cri_ref_no,',') "
						+ "from irtcri_rec where cri_cmpy_id = crh_cmpy_id and cri_crcn_pfx=crh_crcn_pfx and cri_crcn_no = crh_crcn_no) as ref_vou, "
						+ "cast(crh_whs as varchar(3)) whs, cast(crh_cry as varchar(3)) cry, crh_ip_amt,(select string_agg(tsa_sts_actn, ',') from irtcri_rec,tcttsa_Rec"
						+ "	where cri_cmpy_id = tsa_cmpy_id and tsa_ref_pfx = cri_ref_pfx and tsa_ref_no=cri_ref_no "
						+ "	and tsa_ref_itm=0 and tsa_ref_sbitm=0 and tsa_sts_typ='T' and cri_crcn_pfx=crh_crcn_pfx and cri_crcn_no=crh_crcn_no) crcn_sts"
						+ " FROM irtcrh_rec, ctrccr_rec WHERE ccr_cc_no = crh_cc_no and crh_balamt<> 0 ";
			} else if (CommonConstants.DB_TYP.equals("INF")) {
				hql = " SELECT crh_po_no, crh_po_itm, crh_crcn_no, cast(crh_crcn_brh as varchar(3)) as brh, cast(crh_trs_pfx as varchar(2)) trs_pfx, crh_trs_no, crh_trs_itm, "
						+ "crh_trs_sbitm, crh_bal_qty, crh_orig_amt, crh_balamt, to_char(crh_actvy_dt, '%m/%d/%Y') actvy_dt, crh_cc_no, cast(ccr_desc20 as varchar(20)) cost_desc, "
						+ " '' AS ref_vou, "
						+ "cast(crh_whs as varchar(3)) whs, cast(crh_cry as varchar(3)) cry, crh_ip_amt, "
						+ "(select case when count(*) = 0 then 'C' else 'A' end from irtcri_rec,tcttsa_Rec  "
						+ "	where cri_cmpy_id = tsa_cmpy_id and tsa_ref_pfx = cri_ref_pfx and tsa_ref_no=cri_ref_no"
						+ "	and tsa_ref_itm=0 and tsa_ref_sbitm=0 and tsa_sts_typ='T' and cri_crcn_pfx=crh_crcn_pfx and "
						+ "	cri_crcn_no = crh_crcn_no and cri_cmpy_id = crh_cmpy_id and tsa_sts_actn='A') as crcn_sts"
						+ " FROM irtcrh_rec, ctrccr_rec WHERE ccr_cc_no = crh_cc_no and crh_balamt<> 0 ";
			}

			hql = hql + " and crh_cmpy_id=:cmpy_id";

			/* hql = hql + " and crh_crcn_brh=:brh_id"; */

			hql = hql + " and crh_ven_id=:ven_id";

			hql = hql + " and crh_cry=:cry";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", cmpyId);
			/* queryValidate.setParameter("brh_id", brhId); */
			queryValidate.setParameter("ven_id", venId);
			queryValidate.setParameter("cry", cry);

			List<Object[]> listVchrRecon = queryValidate.list();

			for (Object[] vchrRecon : listVchrRecon) {
				VoucherReconInfo reconInfo = new VoucherReconInfo();

				reconInfo.setPoNo(Integer.parseInt(vchrRecon[0].toString()));
				reconInfo.setPoItm(Integer.parseInt(vchrRecon[1].toString()));
				reconInfo.setCrcnNo(Integer.parseInt(vchrRecon[2].toString()));
				reconInfo.setBrh(vchrRecon[3].toString());
				reconInfo.setTrsPfx(vchrRecon[4].toString());
				reconInfo.setTrsNo(Integer.parseInt(vchrRecon[5].toString()));
				reconInfo.setTrsItm(Integer.parseInt(vchrRecon[6].toString()));
				reconInfo.setTrnsbItm(Integer.parseInt(vchrRecon[7].toString()));
				reconInfo.setBalQty(Double.parseDouble(vchrRecon[8].toString()));
				reconInfo.setOrigAmt(Double.parseDouble(vchrRecon[9].toString()));
				reconInfo.setBalAmt(Double.parseDouble(vchrRecon[10].toString()));
				reconInfo.setActvyDtStr(vchrRecon[11].toString());
				reconInfo.setCostNo(Integer.parseInt(vchrRecon[12].toString()));
				reconInfo.setCostDesc(vchrRecon[13].toString());
				reconInfo.setOrigAmtStr(objCom.formatAmount(Double.parseDouble(vchrRecon[9].toString())));
				reconInfo.setBalAmtStr(objCom.formatAmount(Double.parseDouble(vchrRecon[10].toString())));

				if (vchrRecon[14] != null) {
					reconInfo.setRefVoucher(vchrRecon[14].toString());
				} else {
					reconInfo.setRefVoucher("");
				}

				if (vchrRecon[15] != null) {
					reconInfo.setWhs(vchrRecon[15].toString());
				} else {
					reconInfo.setWhs("");
				}

				reconInfo.setCry(vchrRecon[16].toString());

				reconInfo.setIpAmt(Double.parseDouble(vchrRecon[17].toString()));
				reconInfo.setIpAmtStr(objCom.formatAmount(Double.parseDouble(vchrRecon[17].toString())));

				if (vchrRecon[18] != null) {
					List<String> statusList = Arrays.asList(vchrRecon[18].toString());
					int reconFlg = 0;
					if (reconInfo.getBalAmt() > 0) {
						for (String element : statusList) {
							if (element.contains("A") || element.contains("H")) {
								reconInfo.setFlg("A");
								reconFlg = 1;
								break;
							}
						}

						if (reconFlg == 0) {
							reconInfo.setFlg("C");
						}

					} else {
						reconInfo.setFlg("A");
					}
				} else {
					reconInfo.setFlg("");
				}

				starBrowseRes.output.fldTblRecon.add(reconInfo);

			}

			readExistRecon(starBrowseRes, ctlNo, cmpyId);

		} catch (Exception e) {

			logger.debug("Voucher Information : {}", e.getMessage(), e);
			starBrowseRes.output.rtnSts = 1;
			starBrowseRes.output.messages.add(CommonConstants.SERVER_ERR);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	/* READ SELECTED RECON DATA */
	public void readExistRecon(BrowseResponse<VoucherReconInfoBrowseOutput> starBrowseRes, String ctlNo,
			String cmpyId) {
		Session session = null;
		String hql = "";
		CommonFunctions objCom = new CommonFunctions();
		List<Integer> reconCrcnList = new ArrayList<Integer>();

		try {

			session = SessionUtil.getSession();

			CstmDocInfoDAO cstmDocInfoDAO = new CstmDocInfoDAO();
			String crcnStr = "";

			List<ReconInfo> reconList = cstmDocInfoDAO.getReconDetails(session, Integer.parseInt(ctlNo), cmpyId);

			for (int i = 0; i < reconList.size(); i++) {
				reconCrcnList.add(reconList.get(i).getCrcnNo());
			}

			if (reconList.size() == 0) {
				reconCrcnList.add(0);
			}

			if (reconList.size() > 0) {
				session = SessionUtilInformix.getSession();

				if (CommonConstants.DB_TYP.equals("POS")) {
					hql = " SELECT crh_po_no, crh_po_itm, crh_crcn_no, cast(crh_crcn_brh as varchar(3)) as brh, cast(crh_trs_pfx as varchar(2)) trs_pfx, crh_trs_no, crh_trs_itm, "
							+ "crh_trs_sbitm, crh_bal_qty, crh_orig_amt, crh_balamt, to_char(crh_actvy_dt, 'MM/dd/yyyy') actvy_dt, crh_cc_no, cast(ccr_desc20 as varchar(20)) cost_desc, "
							+ "(select string_agg(cri_ref_pfx||'-'||cri_ref_no,',') "
							+ "from irtcri_rec where cri_cmpy_id = crh_cmpy_id and cri_crcn_pfx=crh_crcn_pfx and cri_crcn_no = crh_crcn_no) as ref_vou, "
							+ "cast(crh_whs as varchar(3)) whs, cast(crh_cry as varchar(3)) cry, cast(crh_ven_id as varchar(8)) ven_id, cast(ven_ven_nm as varchar(15)) ven_nm, crh_ip_amt,(select string_agg(tsa_sts_actn, ',') from irtcri_rec,tcttsa_Rec "
							+ " where cri_cmpy_id = tsa_cmpy_id and tsa_ref_pfx = cri_ref_pfx and tsa_ref_no=cri_ref_no "
							+ " and tsa_ref_itm=0 and tsa_ref_sbitm=0 and tsa_sts_typ='T' and cri_crcn_pfx=crh_crcn_pfx and cri_crcn_no=crh_crcn_no) crcn_sts "
							+ "FROM irtcrh_rec, ctrccr_rec, aprven_rec WHERE crh_cmpy_id = ven_cmpy_id and crh_ven_id = ven_ven_id and ccr_cc_no = crh_cc_no and crh_balamt<> 0 and crh_crcn_no in (:crcn_no)";
				} else if (CommonConstants.DB_TYP.equals("INF")) {
					hql = " SELECT crh_po_no, crh_po_itm, crh_crcn_no, cast(crh_crcn_brh as varchar(3)) as brh, cast(crh_trs_pfx as varchar(2)) trs_pfx, crh_trs_no, crh_trs_itm, "
							+ "crh_trs_sbitm, crh_bal_qty, crh_orig_amt, crh_balamt, to_char(crh_actvy_dt, '%m/%d/%Y') actvy_dt, crh_cc_no, cast(ccr_desc20 as varchar(20)) cost_desc, "
							+ " '' AS ref_vou, "
							+ "cast(crh_whs as varchar(3)) whs, cast(crh_cry as varchar(3)) cry, cast(crh_ven_id as varchar(8)) ven_id, cast(ven_ven_nm as varchar(15)) ven_nm, crh_ip_amt, "
							+ "(select case when count(*) = 0 then 'C' else 'A' end from irtcri_rec,tcttsa_Rec  "
							+ " where cri_cmpy_id = tsa_cmpy_id and tsa_ref_pfx = cri_ref_pfx and tsa_ref_no=cri_ref_no"
							+ " and tsa_ref_itm=0 and tsa_ref_sbitm=0 and tsa_sts_typ='T' and cri_crcn_pfx=crh_crcn_pfx and "
							+ " cri_crcn_no = crh_crcn_no and cri_cmpy_id = crh_cmpy_id and tsa_sts_actn='A') as crcn_sts "
							+ "FROM irtcrh_rec, ctrccr_rec, aprven_rec WHERE crh_cmpy_id = ven_cmpy_id and crh_ven_id = ven_ven_id and ccr_cc_no = crh_cc_no and crh_balamt<> 0 and crh_crcn_no in (:crcn_no)";
				}

				hql = hql + " and crh_cmpy_id=:cmpy_id";

				Query queryValidate = session.createSQLQuery(hql);

				queryValidate.setParameter("cmpy_id", cmpyId);
				queryValidate.setParameterList("crcn_no", reconCrcnList);

				List<Object[]> listVchrRecon = queryValidate.list();

				for (Object[] vchrRecon : listVchrRecon) {
					VoucherReconInfo reconInfo = new VoucherReconInfo();

					reconInfo.setPoNo(Integer.parseInt(vchrRecon[0].toString()));
					reconInfo.setPoItm(Integer.parseInt(vchrRecon[1].toString()));
					reconInfo.setCrcnNo(Integer.parseInt(vchrRecon[2].toString()));
					reconInfo.setBrh(vchrRecon[3].toString());
					reconInfo.setTrsPfx(vchrRecon[4].toString());
					reconInfo.setTrsNo(Integer.parseInt(vchrRecon[5].toString()));
					reconInfo.setTrsItm(Integer.parseInt(vchrRecon[6].toString()));
					reconInfo.setTrnsbItm(Integer.parseInt(vchrRecon[7].toString()));
					reconInfo.setBalQty(Double.parseDouble(vchrRecon[8].toString()));
					reconInfo.setOrigAmt(Double.parseDouble(vchrRecon[9].toString()));
					reconInfo.setBalAmt(Double.parseDouble(vchrRecon[10].toString()));
					reconInfo.setActvyDtStr(vchrRecon[11].toString());
					reconInfo.setCostNo(Integer.parseInt(vchrRecon[12].toString()));
					reconInfo.setCostDesc(vchrRecon[13].toString());
					reconInfo.setOrigAmtStr(objCom.formatAmount(Double.parseDouble(vchrRecon[9].toString())));
					reconInfo.setBalAmtStr(objCom.formatAmount(Double.parseDouble(vchrRecon[10].toString())));

					if (vchrRecon[14] != null) {
						reconInfo.setRefVoucher(vchrRecon[14].toString());
					} else {
						reconInfo.setRefVoucher("");
					}

					if (vchrRecon[15] != null) {
						reconInfo.setWhs(vchrRecon[15].toString());
					} else {
						reconInfo.setWhs("");
					}

					reconInfo.setCry(vchrRecon[16].toString());
					reconInfo.setVenId(vchrRecon[17].toString());
					reconInfo.setVenNm(vchrRecon[18].toString());

					for (int i = 0; i < reconList.size(); i++) {
						if (reconList.get(i).getCrcnNo() == reconInfo.getCrcnNo()) {
							reconInfo.setIpAmt(reconList.get(i).getBalAmt());
							reconInfo.setIpAmtStr(objCom.formatAmount(reconList.get(i).getBalAmt()));
							break;
						}
					}

					if (vchrRecon[20] != null) {
						List<String> statusList = Arrays.asList(vchrRecon[20].toString());
						int reconFlg = 0;
						if (reconInfo.getBalAmt() > 0) {
							for (String element : statusList) {
								if (element.contains("A") || element.contains("H")) {
									reconInfo.setFlg("A");
									reconFlg = 1;
									break;
								}
							}

							if (reconFlg == 0) {
								starBrowseRes.output.fldTblReconSel.add(reconInfo);
							}

						}
					} else {
						starBrowseRes.output.fldTblReconSel.add(reconInfo);
					}

				}
			}
		} catch (Exception e) {

			logger.debug("Voucher Information : {}", e.getMessage(), e);
			starBrowseRes.output.rtnSts = 1;
			starBrowseRes.output.messages.add(CommonConstants.SERVER_ERR);
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public List<CostReconExport> readVoucherReconExport(String cmpyId, String brhId, String venId, String cry) {
		Session session = null;
		String hql = "";
		CommonFunctions objCom = new CommonFunctions();

		List<CostReconExport> costReconExports = new ArrayList<CostReconExport>();

		try {
			session = SessionUtilInformix.getSession();

			if (CommonConstants.DB_TYP.equals("POS")) {
				hql = " SELECT crh_po_no, crh_po_itm, crh_crcn_no, cast(crh_crcn_brh as varchar(3)) as brh, cast(crh_trs_pfx as varchar(2)) trs_pfx, crh_trs_no, crh_trs_itm, "
						+ "crh_trs_sbitm, crh_bal_qty, crh_orig_amt, crh_balamt, to_char(crh_actvy_dt, 'MM/dd/yyyy') actvy_dt, crh_cc_no, cast(ccr_desc20 as varchar(20)) cost_desc, "
						+ "(select string_agg(cri_ref_pfx||'-'||cri_ref_no,',') "
						+ "from irtcri_rec where cri_cmpy_id = crh_cmpy_id and cri_crcn_pfx=crh_crcn_pfx and cri_crcn_no = crh_crcn_no) as ref_vou, "
						+ "cast(crh_whs as varchar(3)) whs, cast(crh_cry as varchar(3)) cry "
						+ "FROM irtcrh_rec, ctrccr_rec WHERE ccr_cc_no = crh_cc_no and crh_balamt<> 0 ";

				hql = hql + " and crh_cmpy_id=:cmpy_id";
			} else if (CommonConstants.DB_TYP.equals("INF")) {
				hql = " SELECT crh_po_no, crh_po_itm, crh_crcn_no, cast(crh_crcn_brh as varchar(3)) as brh, cast(crh_trs_pfx as varchar(2)) trs_pfx, crh_trs_no, crh_trs_itm, "
						+ "crh_trs_sbitm, crh_bal_qty, crh_orig_amt, crh_balamt, to_char(crh_actvy_dt, '%m/%d/%Y') actvy_dt, crh_cc_no, cast(ccr_desc20 as varchar(20)) cost_desc, "
						+ " cast(string_agg(crh_cmpy_id, crh_crcn_pfx, crh_crcn_no) as varchar(254)) AS ref_vou, "
						+ " cast(crh_whs as varchar(3)) whs, cast(crh_cry as varchar(3)) cry "
						+ "FROM irtcrh_rec, ctrccr_rec WHERE ccr_cc_no = crh_cc_no and crh_balamt<> 0 ";

				hql = hql + " and crh_cmpy_id=:cmpy_id";
			}

			/* hql = hql + " and crh_crcn_brh=:brh_id"; */

			hql = hql + " and crh_ven_id=:ven_id";

			hql = hql + " and crh_cry=:cry";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", cmpyId);
			/* queryValidate.setParameter("brh_id", brhId); */
			queryValidate.setParameter("ven_id", venId);
			queryValidate.setParameter("cry", cry);

			List<Object[]> listVchrRecon = queryValidate.list();

			for (Object[] vchrRecon : listVchrRecon) {

				CostReconExport reconInfo = new CostReconExport();

				if (!vchrRecon[0].toString().equals("0")) {
					reconInfo.setPoNo("PO-" + vchrRecon[0].toString() + "-" + vchrRecon[1].toString());
				} else {
					reconInfo.setPoNo("");
				}
				reconInfo.setCrcnNo(vchrRecon[2].toString());
				reconInfo.setBrh(vchrRecon[3].toString());
				reconInfo.setTrsNo(vchrRecon[4].toString() + "-" + vchrRecon[5].toString() + "-"
						+ vchrRecon[6].toString() + "-" + vchrRecon[7].toString());
				reconInfo.setBalQty(Double.parseDouble(vchrRecon[8].toString()));
				reconInfo.setOrigAmt(Double.parseDouble(vchrRecon[9].toString()));
				reconInfo.setBalAmt(Double.parseDouble(vchrRecon[10].toString()));
				if (vchrRecon[12] != null) {
					reconInfo.setCost(vchrRecon[12].toString() + "-" + vchrRecon[13].toString());
				} else {
					reconInfo.setCost("");
				}

				if (vchrRecon[14] != null) {
					reconInfo.setRefNo(vchrRecon[14].toString());
				} else {
					reconInfo.setRefNo("");
				}

				if (vchrRecon[15] != null) {
					reconInfo.setWhs(vchrRecon[15].toString());
				} else {
					reconInfo.setWhs("");
				}

				// CostReconExport costReconExport = new
				// CostReconExport(vchrRecon[0].toString(), vchrRecon[2].toString(),
				// vchrRecon[3].toString(), "", 0, 0, "", 0, "", "", "");

				/* reconInfo.setCry(vchrRecon[16].toString()); */

				costReconExports.add(reconInfo);

			}

		} catch (Exception e) {

			logger.debug("Voucher Information : {}", e.getMessage(), e);
			session.close();
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return costReconExports;

	}

	public BigDecimal getBigDecimal(Object value) {
		BigDecimal ret = null;
		if (value != null) {
			if (value instanceof BigDecimal) {
				ret = (BigDecimal) value;
			} else if (value instanceof String) {
				ret = new BigDecimal((String) value);
			} else if (value instanceof BigInteger) {
				ret = new BigDecimal((BigInteger) value);
			} else if (value instanceof Number) {
				ret = new BigDecimal(((Number) value).doubleValue());
			} else {
				throw new ClassCastException("Not possible to coerce [" + value + "] from class " + value.getClass()
						+ " into a BigDecimal.");
			}
		}
		return ret;
	}

	public int getVchrCtlNo() {
		Session session = null;
		String hql = "";
		int ctlNo = 0;

		try {
			session = SessionUtil.getSession();

			hql = " select COALESCE(max(vchr_ctl_no),0) vchr_ctl_no from cstm_vchr_info";

			Query queryValidate = session.createSQLQuery(hql);

			ctlNo = Integer.parseInt(queryValidate.list().get(0).toString()) + 1;

		} catch (Exception e) {

			logger.debug("Voucher Information : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return ctlNo;
	}

	public List<String> getVenderID(String venName) {
		Session session = null;
		String hql = "";
		String venId = "";

		List<String> venDtls = new ArrayList<String>();

		String venNmStx = getVenderOcrMap(venName);

		try {
			session = SessionUtilInformix.getSession();

			hql = " select  cast(ven_ven_id as varchar(8)) ven_ven_id, "
					+ "cast(ven_ven_long_nm as varchar(35)) ven_long_nm "
					+ " from aprven_rec where lower(ven_ven_nm) = lower(:venName) "
					+ "or lower(ven_ven_long_nm)=lower(:venName) or "
					+ "lower(ven_ven_nm) =lower(:venNameStx) or lower(ven_ven_id) =lower(:venName)";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("venName", venName);
			queryValidate.setParameter("venNameStx", venNmStx);

			List<String> venList = queryValidate.list();

			List<Object[]> listRequest = queryValidate.list();

			for (Object[] request : listRequest) {

				venDtls.add(request[0].toString());
				venDtls.add(request[1].toString());

				venId = request[0].toString();

			}

			if (listRequest.size() == 0) {

				venDtls.add("");
				venDtls.add("");
			}

		} catch (Exception e) {

			logger.debug("Voucher Information : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return venDtls;
	}

	public String getVenderOcrMap(String venName) {
		Session session = null;
		String hql = "";
		String venNmStx = "";

		try {
			session = SessionUtil.getSession();

			hql = " select  ven_nm_stx  from cstm_ocr_ven_map where ven_nm =:venName";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("venName", venName);

			List<String> venList = queryValidate.list();

			if (venList.size() > 0) {
				venNmStx = venList.get(0).toString();
			} else {
				venNmStx = "";
			}

		} catch (Exception e) {

			logger.debug("Voucher Information : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return venNmStx;
	}

	public void getErrorMsg(String userId, MaintanenceResponse<CstmVchrInfoManOutput> starManResponse) {
		Session session = null;
		String hql = "";
		String venId = "";
		int usrCount = 0;
		String ssnCtlNo;

		try {
			session = SessionUtilInformix.getSession();

			hql = " select count(*) from mxrusr_rec where usr_lgn_id=:usr_id";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("usr_id", userId);

			usrCount = Integer.parseInt(String.valueOf(queryValidate.list().get(0)));

			if (usrCount == 0) {
				BrowseResponse<ExecCnfgBrowseOutput> starBrowseResponse = new BrowseResponse<ExecCnfgBrowseOutput>();
				starBrowseResponse.setOutput(new ExecCnfgBrowseOutput());

				ExecCnfgDAO cnfgDAO = new ExecCnfgDAO();
				cnfgDAO.getExecInfo(starBrowseResponse);

				if (starBrowseResponse.output.fldTblExec.size() > 0) {
					userId = starBrowseResponse.output.fldTblExec.get(0).getUsrId();
					usrCount = 1;
				}
			}

			if (usrCount > 0) {

				hql = " select i00_ssn_log_ctl_no from xcti00_rec where i00_evnt='VCW' and i00_sts_cd='E' and i00_usr_id=:usr_id order by i00_crtd_dtts desc limit 1";

				queryValidate = session.createSQLQuery(hql);

				queryValidate.setParameter("usr_id", userId);

				ssnCtlNo = String.valueOf(queryValidate.list().get(0));

				if (CommonConstants.DB_TYP.equals("POS")) {
					hql = "select distinct msg_err_msg_typ, msg_msg_var from sctmsg_rec, sctslg_rec, "
							+ "xcti00_rec where 	msg_cmpy_id = slg_cmpy_id 	and slg_cmpy_id = i00_cmpy_id 	"
							+ "and i00_ssn_log_ctl_no = slg_ssn_log_ctl_no 	and slg_ssn_log_seq_no = 1 	and "
							+ "slg_ssn_log_ctl_no =:log_ctl 	and msg_clnt_dtts between slg_clnt_dtts - interval '2 seconds' and slg_clnt_dtts + interval '2 seconds' "
							+ "	and msg_clnt_host_nm = slg_clnt_host_nm 	and msg_clnt_tty_nm = slg_clnt_tty_nm 	and msg_clnt_pid = slg_clnt_pid";
				} else {
					hql = "select distinct msg_err_msg_typ, msg_msg_var from sctmsg_rec, sctslg_rec, "
							+ "xcti00_rec where 	msg_cmpy_id = slg_cmpy_id 	and slg_cmpy_id = i00_cmpy_id 	"
							+ "and i00_ssn_log_ctl_no = slg_ssn_log_ctl_no 	and slg_ssn_log_seq_no = 1 	and "
							+ "slg_ssn_log_ctl_no =:log_ctl 	and msg_clnt_dtts between slg_clnt_dtts - 2 UNITS SECOND and slg_clnt_dtts + 2 UNITS SECOND "
							+ "	and msg_clnt_host_nm = slg_clnt_host_nm 	and msg_clnt_tty_nm = slg_clnt_tty_nm 	and msg_clnt_pid = slg_clnt_pid";
				}

				queryValidate = session.createSQLQuery(hql);

				queryValidate.setParameter("log_ctl", Integer.parseInt(ssnCtlNo));

				List<Object[]> listErrMsg = queryValidate.list();

				for (Object[] errMsg : listErrMsg) {

					starManResponse.output.messages.add(errMsg[1].toString());

				}

			}
		} catch (Exception e) {

			logger.debug("Voucher Information : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	/* DELETE RECON DATA IF AVAILABE IN GATEWAY TABLE */
	public void deleteReconData(JsonArray crcnNoArray) {
		Session session = null;
		Transaction tx = null;
		String hql = "";

		try {

			session = SessionUtilInformix.getSession();
			tx = session.beginTransaction();

			for (int i = 0; i < crcnNoArray.size(); i++) {
				JsonObject jsonObject = crcnNoArray.get(i).getAsJsonObject();

				int crcnNo = jsonObject.get("crcnNo").getAsInt();

				hql = "delete from XCWWV1_rec where wv1_crcn_pfx='IR' and wv1_crcn_no=:crcn_no";
				Query queryValidate = session.createSQLQuery(hql);
				queryValidate.setParameter("crcn_no", crcnNo);
				queryValidate.executeUpdate();
			}

			tx.commit();

		} catch (Exception e) {
			logger.debug("Voucher Information : {}", e.getMessage(), e);
			tx.rollback();
		} finally {

			session.close();
		}
	}

}
