package com.star.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.CommonFunctions;
import com.star.common.SessionUtilInformix;
import com.star.common.SessionUtilInformixESP;
import com.star.linkage.dashboard.APOverviewBrowseOuput;

public class DashboardDAO {

	private static Logger logger = LoggerFactory.getLogger(DashboardDAO.class);
	
	public void getOverviewTotals(BrowseResponse<APOverviewBrowseOuput> starBrowseResponse, String cmpyId, String year)
	{
		Session session = null;
		Transaction tx = null;
		String hql = "";
		CommonFunctions objCom = new CommonFunctions();

		try {
			session = SessionUtilInformixESP.getSession();
			
			
			if (CommonConstants.DB_TYP.equals("POS")) {
				
			hql = "select (select COALESCE (sum(vch_vch_amt), 0) from aptvch_rec where vch_ref_pfx||vch_vch_no in (select pyh_ref_cd||pyh_ref_no from aptpyh_rec where pyh_balance<>0) and substring(cast(vch_ref_dt as varchar(8)),0,5)=:year) payable,"
					+ " (select COALESCE (sum(vch_vch_amt),0) from aptvch_rec where vch_due_dt > cast(to_char(CURRENT_DATE, 'YYYYMMDD') as integer) and vch_ref_pfx||vch_vch_no in (select pyh_ref_cd||pyh_ref_no from aptpyh_rec where pyh_balance<>0) and substring(cast(vch_ref_dt as varchar(8)),0,5)=:year) balance, "
					+ "(select COALESCE (sum(vch_vch_amt),0) from aptvch_rec where vch_due_dt < cast(to_char(CURRENT_DATE, 'YYYYMMDD') as integer) and vch_ref_pfx||vch_vch_no in (select pyh_ref_cd||pyh_ref_no from aptpyh_rec where pyh_balance<>0) and substring(cast(vch_ref_dt as varchar(8)),0,5)=:year) overdue_balance, "
					+ "0 overdue_per";
			}
			else if (CommonConstants.DB_TYP.equals("INF")) {
			
				hql = "SELECT (select nvl(sum(pyh_amt_ap),0) from aptpyh_rec where pyh_balance <> 0 and substr(cast(pyh_ref_dt as varchar(8)),0,4)=:year) payable,"
						+ " (select nvl(sum(pyh_amt_ap),0) from aptpyh_rec where pyh_balance <> 0 and substr(cast(pyh_ref_dt as varchar(8)),0,4)=:year and TO_DATE(cast(pyh_due_dt as varchar(8)),'%Y%m%d')  > CURRENT) balance, "
						+ " (select nvl(sum(pyh_amt_ap),0) from aptpyh_rec where pyh_balance <> 0 and substr(cast(pyh_ref_dt as varchar(8)),0,4)=:year and TO_DATE(cast(pyh_due_dt as varchar(8)),'%Y%m%d')  < CURRENT) overdue_balance, 0 FROM systables limit 1";
			}
			
			
			
			Query queryValidate = session.createSQLQuery(hql);
			
			if (CommonConstants.DB_TYP.equals("INF")) {
				/*queryValidate.setParameter("cmpy", cmpyId);*/
			}
			
			queryValidate.setParameter("year", year);
						
			List<Object[]> listOverview = queryValidate.list();

			for (Object[] totals : listOverview) {

				starBrowseResponse.output.totPayable = objCom.formatAmount(Double.parseDouble(totals[0].toString()));
				starBrowseResponse.output.balance = objCom.formatAmount(Double.parseDouble(totals[1].toString()));
				starBrowseResponse.output.overDueBalance = objCom.formatAmount(Double.parseDouble(totals[2].toString()));
				
				double overDuePer  = 0;
				
				if(Double.parseDouble(totals[0].toString()) != 0)
				{
					overDuePer = (Double.parseDouble(totals[2].toString())/Double.parseDouble(totals[0].toString()))*100;
				}
				
				starBrowseResponse.output.overDuePer = objCom.formatAmount(overDuePer);
		
			}
			
			if (CommonConstants.DB_TYP.equals("POS")) {
			hql = "select (select count(*) from aptvch_rec where vch_due_dt > cast(to_char(CURRENT_DATE, 'YYYYMMDD') as integer) and vch_due_dt > cast(to_char(CURRENT_DATE, 'YYYYMMDD') as integer) and substring(cast(vch_ref_dt as varchar(8)),0,5)=:year) as less_30_day, "
					+ "(select count(*) from aptvch_rec where vch_due_dt < cast(to_char(CURRENT_DATE, 'YYYYMMDD') as integer) and vch_due_dt > cast(to_char(CURRENT_DATE, 'YYYYMMDD') as integer) and substring(cast(vch_ref_dt as varchar(8)),0,5)=:year) as bet_30_60_day, "
					+ "(select count(*) from aptvch_rec where vch_due_dt < cast(to_char(CURRENT_DATE, 'YYYYMMDD') as integer) and vch_due_dt > cast(to_char(CURRENT_DATE, 'YYYYMMDD') as integer) and substring(cast(vch_ref_dt as varchar(8)),0,5)=:year) as bet_60_90_day, "
					+ "(select count(*) from aptvch_rec where vch_due_dt < cast(to_char(CURRENT_DATE, 'YYYYMMDD') as integer) and substring(cast(vch_ref_dt as varchar(8)),0,5)=:year) as above_90_day";
			}
			else if (CommonConstants.DB_TYP.equals("INF")) {
				hql = "select (select count(*) from aptvch_rec where TO_DATE(cast(vch_due_dt as varchar(8)),'%Y%m%d') > TODAY and TO_DATE(cast(vch_due_dt as varchar(8)),'%Y%m%d') > TODAY - 30 and substr(cast(vch_ref_dt as varchar(8)),0,4)=:year) as less_30_day, 					(select count(*) from aptvch_rec where TO_DATE(cast(vch_due_dt as varchar(8)),'%Y%m%d') < TODAY - 30 and TO_DATE(cast(vch_due_dt as varchar(8)),'%Y%m%d') > TODAY - 60 and substr(cast(vch_ref_dt as varchar(8)),0,4)=:year)  as bet_30_60_day, 					(select count(*) from aptvch_rec where TO_DATE(cast(vch_due_dt as varchar(8)),'%Y%m%d') < TODAY - 60 and TO_DATE(cast(vch_due_dt as varchar(8)),'%Y%m%d') > TODAY - 90 and substr(cast(vch_ref_dt as varchar(8)),0,4)=:year) as bet_60_90_day, 					(select count(*) from aptvch_rec where TO_DATE(cast(vch_due_dt as varchar(8)),'%Y%m%d') < TODAY - 90 and substr(cast(vch_ref_dt as varchar(8)),0,4)=:year) as above_90_day from systables limit 1";
				
			}
			
		
			queryValidate = session.createSQLQuery(hql);
			
			queryValidate.setParameter("year", year);
						
			List<Object[]> listInvoiceTotals = queryValidate.list();

			for (Object[] invoice : listInvoiceTotals) {
				
				starBrowseResponse.output.dueInvoice30Days = invoice[0].toString();
				starBrowseResponse.output.dueInvoice60Days = invoice[1].toString();
				starBrowseResponse.output.dueInvoice90Days = invoice[2].toString();
				starBrowseResponse.output.dueInvoice90PlusDays = invoice[3].toString();
				
			}

		} catch (Exception e) {
			
			logger.debug("Voucher Information : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	
}
