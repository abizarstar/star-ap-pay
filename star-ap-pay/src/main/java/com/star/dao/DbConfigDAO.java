package com.star.dao;

import java.sql.Connection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.MaintanenceResponse;
import com.star.common.SessionUtil;
import com.star.linkage.TstCon;
import com.star.linkage.dbconfig.DbConfigBrowseOutput;
import com.star.linkage.dbconfig.DbInfo;
import com.star.linkage.dbconfig.DbManOutput;
import com.star.modal.Db;

public class DbConfigDAO {

	private static Logger logger = LoggerFactory.getLogger(DbConfigDAO.class);

	public void getconfigDb(BrowseResponse<DbConfigBrowseOutput> starBrowseResponse, String conType) {
		String hql = "";
		Session session = null;

		try {

			session = SessionUtil.getSession();

			hql = "select * from con_cnfg con where 1=1";
			if (conType.length() > 0) {
				hql += " AND con.con_typ=:conType";
			}
			Query queryValidate = session.createSQLQuery(hql);
			if (conType.length() > 0) {
				queryValidate.setParameter("conType", conType);
			}
			List<Object[]> listdb = queryValidate.list();

			for (Object[] db : listdb) {

				DbInfo info = new DbInfo();

				if (db[0] != null) {
					info.setDriver(db[0].toString());
				} else {
					info.setDriver("");
				}
				if (db[1] != null) {
					String[] str = db[1].toString().split("/");

					info.setUrl(str[0].toString());
					String[] str1 = str[2].split(":");
					info.setHost(str1[0]);
					info.setPort(str1[1]);
				} else {
					info.setUrl("");
				}
				if (db[2] != null) {
					info.setUser(db[2].toString());
				} else {
					info.setUser("");
				}
				if (db[3] != null) {
					info.setPassword(db[3].toString());
				} else {
					info.setPassword("");
				}
				if (db[4] != null) {
					info.setConType(db[4].toString());
				} else {
					info.setConType("");
				}
				starBrowseResponse.output.fldTbldbcnfg.add(info);
			}

		} catch (Exception e) {

			logger.debug("Db Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;

		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void setconfigDb(MaintanenceResponse<DbManOutput> starManResponse, String driver, String url, String host,
			String port, String user, String password, String conType) {

		Session session = null;
		Transaction tx = null;
		Date date = new Date();

		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();
			String urls = "";
			Db dbs = new Db();
			Object ssn = session.get(Db.class, conType);
			Optional<Object> opt = Optional.ofNullable(ssn);
			if (opt.isPresent()) {
				dbs = (Db) ssn;
			} else {
				dbs = new Db();
			}

			if (driver.length() > 0) {
				dbs.setDriver(driver);
			}
			if (url.length() > 0 && host.length() > 0 && port.length() > 0 && user.length() > 0) {
				urls = url + "://" + host + ":" + port + "/" + user;
				dbs.setUrl(urls);
			}
			if (user.length() > 0) {
				dbs.setUsr_nm(user);
			}
			if (password.length() > 0) {
				dbs.setPass(password);
			}
			if (conType.length() > 0) {
				dbs.setCon_typ(conType);
			}
			Connection contst = TstCon.getCon(driver, urls, user, password);
			// Optional<Connection> opt = Optional.ofNullable(contst);
			System.out.println("  ---- - connection detial ---  " + contst);

			Optional<Object> opt1 = Optional.ofNullable(contst);
			if (opt1.isPresent()) {
				contst.close();
				session.saveOrUpdate(dbs);
				System.out.println("  ---- -db saved --- ");
				tx.commit();
			} else {
				starManResponse.output.messages.add("invalid connection detail.... please enter correct detail");
				tx.rollback();
			}
		} catch (Exception e) {

			logger.debug("Db Information : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(e.getMessage());
			tx.rollback();

		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void delDb(MaintanenceResponse<DbManOutput> starManResponse, String driver) {

		Session session = null;
		Transaction tx = null;

		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			Db dbs = new Db();
			if (driver.length() > 0) {
				dbs = (Db) session.get(Db.class, driver);
				session.delete(dbs);
				tx.commit();
			}
		} catch (Exception e) {

			logger.debug("Db Information : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(e.getMessage());
			tx.rollback();

		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
