package com.star.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.CommonFunctions;
import com.star.common.MaintanenceResponse;
import com.star.common.SessionUtil;
import com.star.linkage.check.CheckBrowseOutput;
import com.star.linkage.check.CheckLotInfo;
import com.star.linkage.check.CheckManOutput;
import com.star.linkage.check.VoidCheckBrowse;
import com.star.linkage.check.VoidCheckDetails;
import com.star.linkage.check.VoidCheckInfo;
import com.star.linkage.cstmdocinfo.CstmDocInfoBrowseOutput;
import com.star.linkage.cstmdocinfo.VchrInfo;
import com.star.linkage.payment.PaymentInfoOutput;
import com.star.linkage.payment.PaymentManOutput;
import com.star.modal.BnkChkLotInfo;
import com.star.modal.ChkLotUsage;
import com.star.modal.ChkVoidInfo;

public class CheckDAO {

	private static Logger logger = LoggerFactory.getLogger(CheckDAO.class);

	public void addCheckDetails(MaintanenceResponse<CheckManOutput> starManResponse, String bnkCode, String acctId,
			List<CheckLotInfo> lotArray) {
		Session session = null;
		Transaction tx = null;
		int inUseFlg = 0;
		String hql = "";

		try {

			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			// deleteCheckLotInfo(session, bnkCode, acctId);

			for (int i = 0; i < lotArray.size(); i++) {

				CheckLotInfo checkLotInfo = getCheckLotInfoIndividual(bnkCode, acctId, lotArray.get(i).getLotNo());

				if (checkLotInfo.isChkLotUse() == true) {
					inUseFlg = inUseFlg + 1;
				}

				if (checkLotInfo.getLotNo() == 0) {
					BnkChkLotInfo chkInfo = new BnkChkLotInfo();

					chkInfo.setBnkAcctNo(acctId);
					chkInfo.setBnkCode(bnkCode);
					chkInfo.setChkLotNo(lotArray.get(i).getLotNo());
					chkInfo.setChkLotInfo(lotArray.get(i).getLotInfo());
					chkInfo.setChkLotStrt(lotArray.get(i).getChkFrm());
					chkInfo.setChkLotEnd(lotArray.get(i).getChkTo());

					if (i == 0) {
						chkInfo.setChkLotUse(true);
					} else {
						chkInfo.setChkLotUse(false);
					}

					chkInfo.setChkLotOpn(true);

					session.save(chkInfo);
				} else {
					hql = "update bnk_chk_lot_info set chk_lot_info = :info,"
							+ " chk_lot_strt=:strt, chk_lot_end=:end  where bnk_code=:code and bnk_acct_no=:acct_no and chk_lot_no=:lot_no";

					Query queryValidate = session.createSQLQuery(hql);

					queryValidate.setParameter("info", lotArray.get(i).getLotInfo());
					queryValidate.setParameter("strt", lotArray.get(i).getChkFrm());
					queryValidate.setParameter("end", lotArray.get(i).getChkTo());
					queryValidate.setParameter("code", bnkCode);
					queryValidate.setParameter("acct_no", acctId);
					queryValidate.setParameter("lot_no", lotArray.get(i).getLotNo());

					queryValidate.executeUpdate();
				}
			}

			if (inUseFlg == 0) {
				addCurCheckDetails(session, bnkCode, acctId, lotArray.get(0).getLotNo(), lotArray.get(0).getChkFrm());
			}

			tx.commit();
		} catch (Exception e) {

			logger.debug("Check Info : {}", e.getMessage(), e);
			starManResponse.output.messages.add(e.getMessage());
			starManResponse.output.rtnSts = 1;

			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void deleteOpenUnusedCheckLot(String bnkCode, String acctId) throws Exception {
		Session session = SessionUtil.getSession();
		String hql = "";
		
		hql = "DELETE FROM bnk_chk_lot_info WHERE bnk_code=:bnk_code AND bnk_acct_no=:acct_no AND chk_lot_use=false AND chk_lot_opn=true";

		Query queryValidate = session.createSQLQuery(hql);
		queryValidate.setParameter("bnk_code", bnkCode);
		queryValidate.setParameter("acct_no", acctId);
		queryValidate.executeUpdate();
	}
	
	public void addCurCheckDetails(Session session, String bnkCode, String acctId, int lotNo, String chkNo)
			throws Exception {

		ChkLotUsage chkLotUsage = new ChkLotUsage();

		chkLotUsage.setBnkAcctNo(acctId);
		chkLotUsage.setBnkCode(bnkCode);
		chkLotUsage.setCurLot(lotNo);
		chkLotUsage.setCurChk(chkNo);

		session.save(chkLotUsage);
	}

	public void deleteCheckLotInfo(Session session, String bnkCode, String acctId) throws Exception {
		String hql = "";

		hql = "delete from bnk_chk_lot_info where bnk_code=:bnk_code and bnk_acct_no=:acct_no";

		Query queryValidate = session.createSQLQuery(hql);
		queryValidate.setParameter("bnk_code", bnkCode);
		queryValidate.setParameter("acct_no", acctId);
		queryValidate.executeUpdate();
	}

	public void getCheckLotDetails(BrowseResponse<CheckBrowseOutput> starBrowseResponse, String bnkCode,
			String acctId) {
		Session session = null;

		try {

			String hql = "";

			session = SessionUtil.getSession();

			hql = "select * from bnk_chk_lot_info where 1=1";

			if (bnkCode.length() > 0) {
				hql = hql + " and bnk_code=:bnkcode";
			}

			if (acctId.length() > 0) {
				hql = hql + " and bnk_acct_no=:acct_id";
			}

			hql = hql + " ORDER BY chk_lot_no ASC";

			Query queryValidate = session.createSQLQuery(hql);

			if (bnkCode.length() > 0) {
				queryValidate.setParameter("bnkcode", bnkCode);
			}

			if (acctId.length() > 0) {
				queryValidate.setParameter("acct_id", acctId);
			}

			List<Object[]> listChkLot = queryValidate.list();

			for (Object[] lot : listChkLot) {
				CheckLotInfo checkLotInfo = new CheckLotInfo();

				checkLotInfo.setBnkCode(lot[0].toString());
				checkLotInfo.setAcctNo(lot[1].toString());
				checkLotInfo.setLotNo(Integer.parseInt(lot[2].toString()));
				checkLotInfo.setLotInfo(lot[3].toString());
				checkLotInfo.setChkFrm(lot[4].toString());
				checkLotInfo.setChkTo(lot[5].toString());
				checkLotInfo.setChkLotUse(Boolean.parseBoolean(lot[6].toString()));
				checkLotInfo.setChkLotOpn(Boolean.parseBoolean(lot[7].toString()));

				starBrowseResponse.output.fldTblCheck.add(checkLotInfo);
				
				
			}
			
			ChkLotUsage chkLotUsage = getCurrentCheckNo(bnkCode, acctId);
			
			starBrowseResponse.output.curChkNo = chkLotUsage.getCurChk();
			starBrowseResponse.output.curLotNo = chkLotUsage.getCurLot();
			
		} catch (Exception e) {
			logger.debug("Check Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}
	}

	public CheckLotInfo getCheckLotInfoIndividual(String bnkCode, String acctId, int lotNo) {
		Session session = null;
		CheckLotInfo checkLotInfo = new CheckLotInfo();
		try {

			String hql = "";

			session = SessionUtil.getSession();

			hql = "select * from bnk_chk_lot_info where 1=1";

			if (bnkCode.length() > 0) {
				hql = hql + " and bnk_code=:bnkcode";
			}

			if (acctId.length() > 0) {
				hql = hql + " and bnk_acct_no=:acct_id";
			}

			hql = hql + " and chk_lot_no=:lot_no";

			Query queryValidate = session.createSQLQuery(hql);

			if (bnkCode.length() > 0) {
				queryValidate.setParameter("bnkcode", bnkCode);
			}

			if (acctId.length() > 0) {
				queryValidate.setParameter("acct_id", acctId);
			}

			queryValidate.setParameter("lot_no", lotNo);

			List<Object[]> listChkLot = queryValidate.list();

			for (Object[] lot : listChkLot) {

				checkLotInfo.setBnkCode(lot[0].toString());
				checkLotInfo.setAcctNo(lot[1].toString());
				checkLotInfo.setLotNo(Integer.parseInt(lot[2].toString()));
				checkLotInfo.setLotInfo(lot[3].toString());
				checkLotInfo.setChkFrm(lot[4].toString());
				checkLotInfo.setChkTo(lot[5].toString());
				checkLotInfo.setChkLotUse(Boolean.parseBoolean(lot[6].toString()));
				checkLotInfo.setChkLotOpn(Boolean.parseBoolean(lot[7].toString()));
			}

		} catch (Exception e) {
			logger.debug("Check Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}

		return checkLotInfo;
	}

	public void getVoidCheckDetails(BrowseResponse<VoidCheckBrowse> starBrowseResponse) {
		Session session = null;
		CommonFunctions commonFunctions = new CommonFunctions();

		try {

			String hql = "";

			session = SessionUtil.getSession();

			hql = "select bnk.bnk_code, bnk.bnk_acct_no,chk_no,chk_vd_on,"
					+ "(select usr_nm from usr_dtls where usr_id=chk_vd_by), chk_vd_rmk "
					+ "from chk_void_info chk, cstm_bnk_acct_info bnk where "
					+ "chk.bnk_code = bnk.bnk_code and chk.bnk_acct_no=bnk.bnk_acct_no";

			hql = hql + " ORDER BY chk_vd_on DESC";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listChkVoid = queryValidate.list();

			for (Object[] voidChk : listChkVoid) {
				VoidCheckDetails voidCheckDetails = new VoidCheckDetails();

				voidCheckDetails.setBnk(voidChk[0].toString());
				voidCheckDetails.setAcctNo(voidChk[1].toString());
				voidCheckDetails.setChkNo(voidChk[2].toString());

				if (voidChk[3] != null) {
					voidCheckDetails.setChkVdOnStr((commonFunctions.formatDateWithoutTime((Date) voidChk[3])));
					voidCheckDetails.setChkVdOn((Date) voidChk[3]);
				}

				voidCheckDetails.setChkVdBy(voidChk[4].toString());
				voidCheckDetails.setChkVdRmk(voidChk[5].toString());

				starBrowseResponse.output.fldTblCheckDetails.add(voidCheckDetails);
			}

		} catch (Exception e) {
			logger.debug("Check Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}
	}

	/* METHOD TO GET CHECK LOT USAGE DETAILS BASED ON BANK ACCOUTN INFORMATION */
	public ChkLotUsage getCurrentCheckNo(String bnkCode, String acctNo) {

		Session session = null;
		String hql = "";

		ChkLotUsage chkLotUsage = new ChkLotUsage();

		try {
			session = SessionUtil.getSession();

			hql = "select * from chk_lot_usage where bnk_code=:code and bnk_acct_no=:acct_no limit 1 ";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("code", bnkCode);

			queryValidate.setParameter("acct_no", acctNo);

			List<Object[]> listCurLot = queryValidate.list();

			for (Object[] lot : listCurLot) {

				chkLotUsage.setBnkCode(lot[0].toString());
				chkLotUsage.setBnkAcctNo(lot[1].toString());
				chkLotUsage.setCurLot(Integer.parseInt(lot[2].toString()));
				chkLotUsage.setCurChk(lot[3].toString());
			}
			
			while (1 == 1) {
				int iFlg = validateVoidCheck(bnkCode, acctNo,chkLotUsage.getCurChk());

				if (iFlg == 0) {
					break;
				} else {
					
					int getNextCheck = Integer.parseInt(chkLotUsage.getCurChk()) + 1;
					
					chkLotUsage.setCurChk(String.valueOf(getNextCheck));
				}
			}
			
		} catch (Exception e) {
			logger.debug("Check Info : {}", e.getMessage(), e);
			// TODO: handle exception
		} finally {
			session.close();
		}

		return chkLotUsage;
	}

	public void updCheckNo(ChkLotUsage chkLotUsage) throws Exception {
	Session session = null;
		Transaction tx = null;
		String hql = "";
		CheckLotInfo checkLotInfo = new CheckLotInfo();

		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			String endChkNo = getEndLotCheck(chkLotUsage);

			if (Integer.parseInt(endChkNo) < Integer.parseInt(chkLotUsage.getCurChk())) {
				List<CheckLotInfo> checkInfoLst = getChkLotList(chkLotUsage);

				if (checkInfoLst.size() > 0) {
					checkLotInfo = checkInfoLst.get(0);
				}

				updateChkLot(session, chkLotUsage, checkLotInfo, "O");

				updateChkLot(session, chkLotUsage, checkLotInfo, "N");

				hql = "update chk_lot_usage set cur_lot=:lot, cur_chk = :chk_no where bnk_code=:code and bnk_acct_no=:acct_no";
			} else {
				hql = "update chk_lot_usage set cur_chk = :chk_no where bnk_code=:code and bnk_acct_no=:acct_no";
			}

			Query queryValidate = session.createSQLQuery(hql);

			if (Integer.parseInt(endChkNo) < Integer.parseInt(chkLotUsage.getCurChk())) {
				queryValidate.setParameter("chk_no", checkLotInfo.getChkFrm());
				queryValidate.setParameter("lot", checkLotInfo.getLotNo());
				queryValidate.setParameter("code", chkLotUsage.getBnkCode());
				queryValidate.setParameter("acct_no", chkLotUsage.getBnkAcctNo());
			} else {
				queryValidate.setParameter("chk_no", chkLotUsage.getCurChk());
				queryValidate.setParameter("code", chkLotUsage.getBnkCode());
				queryValidate.setParameter("acct_no", chkLotUsage.getBnkAcctNo());
			}

			queryValidate.executeUpdate();

			tx.commit();
		} catch (Exception e) {
			logger.debug("Check Info : {}", e.getMessage(), e);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void updateChkLot(Session session, ChkLotUsage chkLotUsage, CheckLotInfo checkLotInfo, String flg)
			throws Exception {
		String hql = "";

		if (flg.equals("O")) {
			hql = "update bnk_chk_lot_info set chk_lot_use = false,"
					+ " chk_lot_opn=false where bnk_code=:code and bnk_acct_no=:acct_no and chk_lot_no=:lot_no";
		} else {
			hql = "update bnk_chk_lot_info set chk_lot_use = true,"
					+ " chk_lot_opn=true where bnk_code=:code and bnk_acct_no=:acct_no and chk_lot_no=:lot_no";
		}

		Query queryValidate = session.createSQLQuery(hql);

		if (flg.equals("O")) {
			queryValidate.setParameter("code", chkLotUsage.getBnkCode());
			queryValidate.setParameter("acct_no", chkLotUsage.getBnkAcctNo());
			queryValidate.setParameter("lot_no", chkLotUsage.getCurLot());
		} else {
			queryValidate.setParameter("code", chkLotUsage.getBnkCode());
			queryValidate.setParameter("acct_no", chkLotUsage.getBnkAcctNo());
			queryValidate.setParameter("lot_no", checkLotInfo.getLotNo());
		}

		queryValidate.executeUpdate();

	}

	public String getEndLotCheck(ChkLotUsage chkLotUsage) {

		Session session = null;
		String hql = "";
		String lotLstChkNo = "";

		try {
			session = SessionUtil.getSession();

			hql = "select chk_lot_end, 1 from bnk_chk_lot_info where bnk_code=:code and bnk_acct_no=:acct_no and chk_lot_no=:lot_no ";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("code", chkLotUsage.getBnkCode());
			queryValidate.setParameter("acct_no", chkLotUsage.getBnkAcctNo());
			queryValidate.setParameter("lot_no", chkLotUsage.getCurLot());

			List<Object[]> listCurLot = queryValidate.list();

			for (Object[] lot : listCurLot) {

				lotLstChkNo = lot[0].toString();
			}

		} catch (Exception e) {
			logger.debug("Check Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return lotLstChkNo;
	}

	public List<CheckLotInfo> getChkLotList(ChkLotUsage chkLotUsage) {

		Session session = null;
		String hql = "";
		List<CheckLotInfo> checkLotInfos = new ArrayList<CheckLotInfo>();

		try {
			session = SessionUtil.getSession();

			hql = "select chk_lot_no, chk_lot_strt, chk_lot_end from bnk_chk_lot_info where bnk_code=:code and bnk_acct_no=:acct_no and chk_lot_no > :lot_no ";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("code", chkLotUsage.getBnkCode());
			queryValidate.setParameter("acct_no", chkLotUsage.getBnkAcctNo());
			queryValidate.setParameter("lot_no", chkLotUsage.getCurLot());

			List<Object[]> listCurLot = queryValidate.list();

			for (Object[] lot : listCurLot) {

				CheckLotInfo lotInfo = new CheckLotInfo();

				lotInfo.setLotNo(Integer.parseInt(lot[0].toString()));
				lotInfo.setChkFrm(lot[1].toString());
				lotInfo.setChkTo(lot[2].toString());

				checkLotInfos.add(lotInfo);
			}

		} catch (Exception e) {
			logger.debug("Check Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return checkLotInfos;
	}

	public void updCheckForInvoice(Session session, String invNo, String checkNo) {
		/*Session session = null;
		Transaction tx = null;*/
		String hql = "";

		/*try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();*/

			hql = "update cstm_param_inv set chk_no = :chk where inv_no=:inv";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("chk", checkNo);
			queryValidate.setParameter("inv", invNo);

			queryValidate.executeUpdate();

		/*	tx.commit();
		} catch (Exception e) {
			logger.debug("Check Info : {}", e.getMessage(), e);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}*/

	}

	public boolean getVenListForChecks(MaintanenceResponse<PaymentManOutput> manResponse, String reqId, int iTotChks) {
		Session session = null;
		boolean rtnMsg = false;

		try {
			session = SessionUtil.getSession();
			PaymentInfoOutput params = new PaymentInfoOutput();
			PaymentDAO dao = new PaymentDAO();

			dao.readInvoice(params, reqId, session);

			List<String> venList = new ArrayList<String>();

			for (int i = 0; i < params.getParamInvList().size(); i++) {
				venList.add(params.getParamInvList().get(i).getVchrVenId());
			}

			Set<String> uniqueVendor = new HashSet<String>(venList);

			if (iTotChks >= uniqueVendor.size()) {
				rtnMsg = true;
			}
		} catch (Exception e) {
			logger.debug("Check Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		if (rtnMsg == false) {
			manResponse.output.rtnSts = 1;
			manResponse.output.messages.add(CommonConstants.CHK_LOT_ERR);
		}

		return rtnMsg;

	}

	public void getCheckPaymentDetails(BrowseResponse<CstmDocInfoBrowseOutput> starBrowseResponse, String bnkCode,
			String acctId,String cmpyId) {
		CommonFunctions commonFunctions = new CommonFunctions();
		Session session = null;
		PaymentDAO paymentDAO = new PaymentDAO();

		try {

			String hql = "";

			session = SessionUtil.getSession();

			hql = "select param.req_id, crtd_on, req_actn_on, (select usr_nm from usr_dtls where usr_id=req_actn_by) usr_nm, inv_no, vchr_notes, chk_no, inv.inv_sts from "
					+ "cstm_pay_params param, cstm_param_inv inv, pay_mthd pay where "
					+ "inv.vchr_pay_mthd = pay.id and param.req_id = inv.req_id and mthd_cd='CHK' and param.cmpy_id=:cmpy_id";

			if (bnkCode.length() > 0) {
				hql = hql + " and pay_bnk_code=:bnkcode";
			}

			if (acctId.length() > 0) {
				hql = hql + " and pay_acct_no=:acct_id";
			}

			hql = hql + " ORDER BY req_actn_by desc";

			Query queryValidate = session.createSQLQuery(hql);

			if (bnkCode.length() > 0) {
				queryValidate.setParameter("bnkcode", bnkCode);
			}

			if (acctId.length() > 0) {
				queryValidate.setParameter("acct_id", acctId);
			}
			
			queryValidate.setParameter("cmpy_id", cmpyId);

			List<Object[]> listInvoices = queryValidate.list();

			for (Object[] chkInvoice : listInvoices) {

				VchrInfo vchrInfo = new VchrInfo();

				vchrInfo.setReqId(chkInvoice[0].toString());

				if (chkInvoice[1] != null) {
					vchrInfo.setCrtDttsStr((commonFunctions.formatDateWithoutTime((Date) chkInvoice[1])));
					vchrInfo.setCrtDtts((Date) chkInvoice[1]);
				}

				if (chkInvoice[2] != null) {
					vchrInfo.setPrsDttsStr((commonFunctions.formatDateWithoutTime((Date) chkInvoice[2])));
					vchrInfo.setPrsDtts((Date) chkInvoice[2]);
				}

				vchrInfo.setPrsBy(chkInvoice[3].toString());

				vchrInfo.setVchrNo(chkInvoice[4].toString());
				vchrInfo.setNote(chkInvoice[5].toString());
				vchrInfo.setChkNo(chkInvoice[6].toString());
				vchrInfo.setIsPrs((Boolean) chkInvoice[7]);

				paymentDAO.readSpecificVoucher(chkInvoice[4].toString(), vchrInfo);

				starBrowseResponse.output.fldTblDoc.add(vchrInfo);
			}

		} catch (Exception e) {
			logger.debug("Check Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}
	}

	public void updVoidCheck(List<VoidCheckInfo> checkInfos, String remark, String userId) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		Date date = new Date();

		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			for (int i = 0; i < checkInfos.size(); i++) {
				hql = "update cstm_param_inv set inv_sts='false', chk_vd_on=:vd_on, chk_vd_by=:usr, chk_vd_rmk=:rmk where req_id =:req and inv_no=:inv and chk_no=:chk";

				Query queryValidate = session.createSQLQuery(hql);

				queryValidate.setParameter("chk", checkInfos.get(i).getChkNo());
				queryValidate.setParameter("inv", checkInfos.get(i).getVchrNo());
				queryValidate.setParameter("req", checkInfos.get(i).getReqId());
				queryValidate.setParameter("usr", userId);
				queryValidate.setParameter("vd_on", date);
				queryValidate.setParameter("rmk", remark);

				queryValidate.executeUpdate();
			}

			tx.commit();
		} catch (Exception e) {
			logger.debug("Check Info : {}", e.getMessage(), e);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public int validateVoidCheck(String bnkCode, String acctId, String checkNo) {
		int iRcrdCount = 0;
		Session session = null;

		try {

			String hql = "";

			session = SessionUtil.getSession();

			hql = "select count(*) from chk_void_info where bnk_code=:bnkcode and bnk_acct_no=:acct_id and chk_no=:chk";

			if (bnkCode.length() > 0) {
				hql = hql + " and bnk_code=:bnkcode";
			}

			if (acctId.length() > 0) {
				hql = hql + " and bnk_acct_no=:acct_id";
			}

			if (checkNo.length() > 0) {
				hql = hql + " and chk_no=:chk";
			}

			Query queryValidate = session.createSQLQuery(hql);

			if (bnkCode.length() > 0) {
				queryValidate.setParameter("bnkcode", bnkCode);
			}

			if (acctId.length() > 0) {
				queryValidate.setParameter("acct_id", acctId);
			}

			if (checkNo.length() > 0) {
				queryValidate.setParameter("chk", checkNo);
			}

			iRcrdCount = Integer.parseInt(String.valueOf(queryValidate.list().get(0)));

		} catch (Exception e) {
			logger.debug("Check Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}

		return iRcrdCount;

	}
	
	public int validateVoidCheckNo(String bnkCode, String acctId, String checkNo) {
		int iRcrdCount = 0;
		Session session = null;

		try {

			String hql = "";

			session = SessionUtil.getSession();
			
			hql = "SELECT COUNT(*) FROM bnk_chk_lot_info WHERE 1=1 ";

			if (bnkCode.length() > 0) {
				hql = hql + " AND bnk_code=:bnkcode";
			}

			if (acctId.length() > 0) {
				hql = hql + " AND bnk_acct_no=:acct_id";
			}

			if (checkNo.length() > 0) {
				hql = hql + " AND CAST (chk_lot_strt AS INTEGER) <= :chk AND CAST (chk_lot_end AS INTEGER) >= :chk";
			}

			Query queryValidate = session.createSQLQuery(hql);

			if (bnkCode.length() > 0) {
				queryValidate.setParameter("bnkcode", bnkCode);
			}

			if (acctId.length() > 0) {
				queryValidate.setParameter("acct_id", acctId);
			}

			if (checkNo.length() > 0) {
				queryValidate.setParameter("chk", Integer.parseInt(checkNo));
			}

			iRcrdCount = Integer.parseInt(String.valueOf(queryValidate.list().get(0)));

		} catch (Exception e) {
			logger.debug("Check Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}

		return iRcrdCount;

	}

	public void addVoidCheck(MaintanenceResponse<CheckManOutput> starManResponse, String bnkCode, String acctNo,
			String chkNo, String remark, String userId) {
		Session session = null;
		Transaction tx = null;
		Date date = new Date();

		try {

			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			ChkVoidInfo chkInfo = new ChkVoidInfo();

			chkInfo.setBnkAcctNo(acctNo);
			chkInfo.setBnkCode(bnkCode);
			chkInfo.setChkNo(chkNo);
			chkInfo.setChkVdRmk(remark);
			chkInfo.setChkVdBy(userId);
			chkInfo.setChkVdOn(date);

			session.save(chkInfo);

			tx.commit();
		} catch (Exception e) {

			logger.debug("Check Info : {}", e.getMessage(), e);
			starManResponse.output.messages.add(e.getMessage());
			starManResponse.output.rtnSts = 1;

			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
