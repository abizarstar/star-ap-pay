package com.star.dao;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.CommonFunctions;
import com.star.common.SessionUtilInformix;
import com.star.linkage.ar.AREnquiryBrowseOutput;
import com.star.linkage.ar.AREnquiryInfo;
import com.star.linkage.ar.ARPaymentBrowseOutput;
import com.star.linkage.ar.ARPaymentInfo;
import com.star.linkage.ar.AdvancePaymentBrowseOutput;
import com.star.linkage.ar.AdvancePaymentInfo;
import com.star.linkage.ar.CashReceiptBrowseOutput;
import com.star.linkage.ar.CashReceiptInfo;
import com.star.linkage.ar.GLDistributionBrowseOutput;
import com.star.linkage.ar.GLDistributionInfo;
import com.star.linkage.ar.SessionBrowseOutput;
import com.star.linkage.ar.SessionInfo;
import com.star.linkage.ar.UnappliedDebitBrowseOutput;
import com.star.linkage.ar.UnappliedDebitInfo;

public class CstmVchrNoteDAO {

	private static Logger logger = LoggerFactory.getLogger(CstmVchrNoteDAO.class);
	
	public void readSessions(BrowseResponse<SessionBrowseOutput> starBrowseResponse, String cmpyId, String ssnId, String crcpNo, String cusId)
	{
		logger.info("AR Info : {}", new Object() {}.getClass().getEnclosingMethod().getName());
		
		Session session = null;
		String hql = "";
		CommonFunctions objCom = new CommonFunctions();
		
		try {
			session = SessionUtilInformix.getSession();

			hql = "SELECT CAST(crs_cmpy_id AS VARCHAR(3)), crs_ssn_id, CAST(crs_crcp_brh AS VARCHAR(3)), crs_jrnl_dt, CAST(usr_nm AS VARCHAR(35)), "
					+ "CAST(crs_bnk AS VARCHAR(3)), crs_tot_nbr_chk, crs_tot_dep_amt, (select count(tsa_sts_actn)  FROM artcsh_rec, tcttsa_rec "
					+ "WHERE csh_ssn_id=crs_ssn_id and tsa_ref_pfx=csh_crcp_pfx and tsa_ref_no=csh_crcp_no and tsa_ref_itm=0 and tsa_ref_sbitm=0 " + 
					" and tsa_sts_typ='T' and tsa_sts_actn='A') AS sts_count, CAST(crs_dep_cry AS VARCHAR(3)), CAST(crs_dep_exrt AS VARCHAR(14)) AS dep_exrt, crs_dep_dt, "
					+ "CAST(crs_ar_cry1 AS VARCHAR(3)), crs_ar_xexrt1, CAST(crs_ar_cry2 AS VARCHAR(3)), CAST(crs_ar_xexrt2 AS VARCHAR(14)) AS ar_xexrt2, CAST(crs_ar_cry3 AS VARCHAR(3)), "
					+ "CAST(crs_ar_xexrt3 AS VARCHAR(14)) AS ar_xexrt3 FROM artcrs_rec S, mxrusr_rec U where S.crs_lgn_id = U.usr_lgn_id AND crs_cmpy_id=:cmpy_id";
			
			if( ssnId.trim().length() > 0 || crcpNo.trim().length() > 0 || cusId.trim().length() > 0 ) {
				hql += " AND crs_ssn_id in (SELECT csh_ssn_id FROM artcsh_rec WHERE 1=1 ";
				
				if( ssnId.trim().length() > 0 ) {
					hql += " AND csh_ssn_id=:ssn_id";
				}
				if( crcpNo.trim().length() > 0 ) {
					hql += " AND csh_crcp_no=:crcp_no";
				}
				if( cusId.trim().length() > 0 ) {
					hql += " AND csh_cr_ctl_cus_id=:cus_id";
				}
				
				hql += ")";
			}
			
			hql += " order by crs_jrnl_dt desc";
			
			Query queryValidate = session.createSQLQuery(hql);
			
			queryValidate.setParameter("cmpy_id", cmpyId);
			
			if( ssnId.trim().length() > 0 ) {
				queryValidate.setParameter("ssn_id", Integer.parseInt(ssnId));
			}
			if( crcpNo.trim().length() > 0 ) {
				queryValidate.setParameter("crcp_no", Integer.parseInt(crcpNo));
			}
			if( cusId.trim().length() > 0 ) {
				queryValidate.setParameter("cus_id", cusId);
			}
						
			List<Object[]> listSession = queryValidate.list();

			for (Object[] ssn : listSession) {

				SessionInfo output = new SessionInfo();

				output.setCmpyId(ssn[0].toString());
				output.setSsnId(ssn[1].toString());
				output.setBrh(ssn[2].toString());
				if (ssn[3] != null) {
					//output.setJrnlDtStr((commonFunctions.formatDateWithoutTime((Date) request[2])));
					
					SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
					String formatDate = formatter.format((Date) ssn[3]);
					
				 	output.setJrnlDt(formatDate);
				}
				output.setLgnNm(ssn[4].toString());
				output.setBnk(ssn[5].toString());
				output.setTotNbrChk(ssn[6].toString());
				output.setTotDepAmt(ssn[7].toString());
				
				output.setTotDepAmtStr(objCom.formatAmount(Double.parseDouble(ssn[7].toString())));
				
				if( Integer.parseInt(ssn[8].toString()) == 0 ) {
					output.setSts("C");
				}else {
					output.setSts("A");
				}
				if (ssn[9] != null) {
					output.setDepCry(ssn[9].toString());
				}
				if (ssn[10] != null) {
					output.setDepExrt(ssn[10].toString());
				}
				if (ssn[11] != null) {
					//output.setJrnlDtStr((commonFunctions.formatDateWithoutTime((Date) request[2])));
					
					SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
					String formatDate = formatter.format((Date) ssn[11]);
					
				 	output.setDepDt(formatDate);
				}
				if (ssn[12] != null) {
					output.setArCry1(ssn[12].toString());
				}
				if (ssn[13] != null) {
					output.setArXexrt1(ssn[13].toString());
				}
				if (ssn[14] != null) {
					output.setArCry2(ssn[14].toString());
				}
				if (ssn[15] != null) {
					output.setArXexrt2(ssn[15].toString());
				}
				if (ssn[16] != null) {
					output.setArCry3(ssn[16].toString());
				}
				if (ssn[17] != null) {
					output.setArXexrt3(ssn[17].toString());
				}
				
				starBrowseResponse.output.fldTblSsn.add(output);

			}

		} catch (Exception e) {
			// TODO: handle exception
			logger.debug("AR Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	
	public void readCashReceipt(BrowseResponse<CashReceiptBrowseOutput> starBrowseResponse, String ssnId)
	{
		logger.info("AR Info : {}", new Object() {}.getClass().getEnclosingMethod().getName());
		
		Session session = null;
		String hql = "";
		CommonFunctions objCom = new CommonFunctions();

		try {
			session = SessionUtilInformix.getSession();
			
			hql = "SELECT CAST(csh_cmpy_id AS VARCHAR(3)), CAST(csh_crcp_pfx AS VARCHAR(2)), csh_crcp_no, CAST(csh_cr_ctl_cus_id AS VARCHAR(8)), "+
					" csh_cus_chk_amt, CAST(csh_cus_chk_no AS VARCHAR(10)), CAST(csh_desc30 AS VARCHAR(30)),( select tsa_sts_actn from tcttsa_rec "+
					" where tsa_ref_pfx=csh_crcp_pfx and tsa_ref_no=csh_crcp_no and tsa_ref_itm=0 and tsa_ref_sbitm=0 and tsa_sts_typ='T') rcpt_sts "+
					"FROM artcsh_rec WHERE csh_ssn_id=:ssn_id";
			
			Query queryValidate = session.createSQLQuery(hql);
			
			queryValidate.setParameter("ssn_id", Integer.parseInt(ssnId));
						
			List<Object[]> listSession = queryValidate.list();

			for (Object[] ssn : listSession) {

				CashReceiptInfo output = new CashReceiptInfo();

				output.setCmpyId(ssn[0].toString());
				output.setCrPfx(ssn[1].toString());
				output.setCrNo(ssn[2].toString());
				output.setCusId(ssn[3].toString());
				output.setChkAmt(ssn[4].toString());
				output.setChkAmtStr(objCom.formatAmount(Double.parseDouble(ssn[4].toString())));
				output.setChkNo(ssn[5].toString());
				output.setDesc(ssn[6].toString());
				output.setSts(ssn[7].toString());
				output.setChkProof( calcCashRcptCheckProof(ssn[0].toString(), ssn[1].toString(), ssn[2].toString()) );
				
				starBrowseResponse.output.fldTblCashRcpt.add(output);

			}

		} catch (Exception e) {
			// TODO: handle exception
			logger.debug("AR Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	
	public void readARPayments(BrowseResponse<ARPaymentBrowseOutput> starBrowseResponse, String refPfx, String refNo)
	{
		logger.info("AR Info : {}", new Object() {}.getClass().getEnclosingMethod().getName());
		
		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();
			
			hql = "select CAST(csd_cmpy_id AS VARCHAR(3)), CAST(csd_ref_pfx AS VARCHAR(2)), csd_ref_no, csd_ref_itm, CAST(csd_ar_pfx AS VARCHAR(2)), "+
					"csd_ar_no, csd_dsamt, csd_rcvd_amt, rvh_upd_ref from artcsd_rec, artrvh_rec where csd_cmpy_id = rvh_cmpy_id "+
					"and csd_ar_pfx=rvh_ar_pfx and csd_ar_no=rvh_ar_no and csd_ref_pfx=:ref_pfx and csd_ref_no=:ref_no";
			
			Query queryValidate = session.createSQLQuery(hql);
			
			queryValidate.setParameter("ref_pfx", refPfx);
			queryValidate.setParameter("ref_no", Integer.parseInt(refNo));
						
			List<Object[]> listSession = queryValidate.list();

			for (Object[] ssn : listSession) {

				ARPaymentInfo output = new ARPaymentInfo();

				output.setCmpyId(ssn[0].toString());
				output.setRefPfx(ssn[1].toString());
				output.setRefNo(ssn[2].toString());
				output.setRefItm(ssn[3].toString());
				output.setArPfx(ssn[4].toString());
				output.setArNo(ssn[5].toString());
				output.setDsAmt(ssn[6].toString());
				output.setRcvdAmt(ssn[7].toString());
				output.setUpdRef(ssn[8].toString());
				
				starBrowseResponse.output.fldTblARPymnt.add(output);

			}

		} catch (Exception e) {
			// TODO: handle exception
			logger.debug("AR Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void readAREnquiry(BrowseResponse<AREnquiryBrowseOutput> starBrowseResponse, String cusId, String brh, String dueDt, String invNo, String cmpyId, String refPfx, String refNo)
	{
		logger.info("AR Info : {}", new Object() {}.getClass().getEnclosingMethod().getName());
		
		Session session = null;
		String hql = "";
		CommonFunctions objCom = new CommonFunctions();

		try {
			session = SessionUtilInformix.getSession();

			hql = "SELECT CAST(rvh_cmpy_id AS VARCHAR(3)), CAST(rvh_ar_pfx AS VARCHAR(2)), rvh_ar_no, CAST(rvh_sld_cus_id AS VARCHAR(8)), "+
					"CAST(rvh_desc30 AS VARCHAR(30)), CAST(rvh_cry AS VARCHAR(3)), rvh_due_dt, CAST(rvh_ar_brh AS VARCHAR(3)), "+
					"CAST(rvh_upd_ref AS VARCHAR(22)), rvh_inv_dt, rvh_orig_amt, rvh_ip_amt, rvh_balamt FROM artrvh_rec WHERE rvh_balamt <> 0 ";
			
			if( !cusId.trim().equalsIgnoreCase("") ) {
				hql += " AND rvh_sld_cus_id=:cus_id";
			}
			if( !brh.trim().equalsIgnoreCase("") ) {
				hql += " AND rvh_ar_brh=:brh";
			}
			if( !dueDt.trim().equalsIgnoreCase("") ) {
				hql += " AND to_char(rvh_due_dt, 'mm/dd/yyyy')=:due_dt";
			}
			if( !invNo.trim().equalsIgnoreCase("") ) {
				hql += " AND trim(rvh_upd_ref)=trim(:upd_ref)";
			}
			
			Query queryValidate = session.createSQLQuery(hql);
			
			if( !cusId.trim().equalsIgnoreCase("") ) {
				queryValidate.setParameter("cus_id", cusId);
			}
			if( !brh.trim().equalsIgnoreCase("") ) {
				queryValidate.setParameter("brh", brh);
			}
			if( !dueDt.trim().equalsIgnoreCase("") ) {
				queryValidate.setParameter("due_dt", dueDt);
			}
			if( !invNo.trim().equalsIgnoreCase("") ) {
				queryValidate.setParameter("upd_ref", invNo);
			}
						
			List<Object[]> listEnquiry = queryValidate.list();

			for (Object[] enquiry : listEnquiry) {

				AREnquiryInfo output = new AREnquiryInfo();

				output.setCmpyId(enquiry[0].toString());
				output.setArPfx(enquiry[1].toString());
				output.setArNo(enquiry[2].toString());
				output.setCusId(enquiry[3].toString());
				output.setDesc(enquiry[4].toString());
				output.setCry(enquiry[5].toString());
				if (enquiry[6] != null) {					
					SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
					String formatDate = formatter.format((Date) enquiry[6]);

					output.setDueDt(formatDate);
				}
				output.setBrh(enquiry[7].toString());
				output.setUpdRef(enquiry[8].toString());
				if (enquiry[9] != null) {					
					SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
					String formatDate = formatter.format((Date) enquiry[9]);

					output.setInvDt(formatDate);
				}
				output.setOrigAmt(enquiry[10].toString());
				output.setOrigAmtStr(objCom.formatAmount(Double.parseDouble(enquiry[10].toString())));
				output.setIpAmt(enquiry[11].toString());
				output.setIpAmtStr(objCom.formatAmount(Double.parseDouble(enquiry[11].toString())));
				output.setBalamt(enquiry[12].toString());
				output.setBalamtStr(objCom.formatAmount(Double.parseDouble(enquiry[12].toString())));
				
				starBrowseResponse.output.fldTblAREnqry.add(output);

			}
			
			starBrowseResponse.output.chkProof = calcCashRcptCheckProof(cmpyId, refPfx, refNo);

		} catch (Exception e) {
			// TODO: handle exception
			logger.debug("AR Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void readAdvancePayment(BrowseResponse<AdvancePaymentBrowseOutput> starBrowseResponse, String cmpyId, String refPfx, String refNo)
	{
		logger.info("AR Info : {}", new Object() {}.getClass().getEnclosingMethod().getName());
		
		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = "select CAST(adp_desc30 AS VARCHAR(30)), adp_adv_amt, CAST(crs_dep_cry AS VARCHAR(3)) from artadp_rec, artcrs_rec, artcsh_rec " + 
					"where crs_cmpy_id = csh_cmpy_id and crs_ssn_id = csh_ssn_id and csh_cmpy_id = adp_cmpy_id and "+
					"csh_crcp_pfx = adp_ref_pfx and csh_crcp_no = adp_ref_no and adp_adv_ref_pfx='CJ' ";
			
			if( !cmpyId.trim().equalsIgnoreCase("") ) {
				hql += " and adp_cmpy_id=:cmpy_id";
			}
			if( !refPfx.trim().equalsIgnoreCase("") ) {
				hql += " and adp_ref_pfx=:ref_pfx";
			}
			if( !refNo.trim().equalsIgnoreCase("") ) {
				hql += " and adp_ref_no=:ref_no";
			}
			
			Query queryValidate = session.createSQLQuery(hql);
			
			if( !cmpyId.trim().equalsIgnoreCase("") ) {
				queryValidate.setParameter("cmpy_id", cmpyId);
			}
			if( !refPfx.trim().equalsIgnoreCase("") ) {
				queryValidate.setParameter("ref_pfx", refPfx);
			}
			if( !refNo.trim().equalsIgnoreCase("") ) {
				queryValidate.setParameter("ref_no", Integer.parseInt(refNo));
			}
						
			List<Object[]> listRecords = queryValidate.list();

			for (Object[] record : listRecords) {

				AdvancePaymentInfo output = new AdvancePaymentInfo();

				output.setDesc(record[0].toString());
				output.setAdvAmt(record[1].toString());
				output.setCry(record[2].toString());
				
				starBrowseResponse.output.fldTblAdvPmt.add(output);

			}
			
			starBrowseResponse.output.chkProof = calcCashRcptCheckProof(cmpyId, refPfx, refNo);

		} catch (Exception e) {
			// TODO: handle exception
			logger.debug("AR Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}
	
	public void readUnappliedDebit(BrowseResponse<UnappliedDebitBrowseOutput> starBrowseResponse, String cmpyId, String refPfx, String refNo)
	{
		logger.info("AR Info : {}", new Object() {}.getClass().getEnclosingMethod().getName());
		
		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();
			
			hql = "select CAST(adp_desc30 AS VARCHAR(30)), adp_adv_amt, CAST(crs_dep_cry AS VARCHAR(3)) from artadp_rec, artcrs_rec, artcsh_rec " + 
					"where crs_cmpy_id = csh_cmpy_id and crs_ssn_id = csh_ssn_id and csh_cmpy_id = adp_cmpy_id and "+
					"csh_crcp_pfx = adp_ref_pfx and csh_crcp_no = adp_ref_no and adp_adv_ref_pfx='DA' ";
			
			if( !cmpyId.trim().equalsIgnoreCase("") ) {
				hql += " and adp_cmpy_id=:cmpy_id";
			}
			if( !refPfx.trim().equalsIgnoreCase("") ) {
				hql += " and adp_ref_pfx=:ref_pfx";
			}
			if( !refNo.trim().equalsIgnoreCase("") ) {
				hql += " and adp_ref_no=:ref_no";
			}
			
			Query queryValidate = session.createSQLQuery(hql);
			
			if( !cmpyId.trim().equalsIgnoreCase("") ) {
				queryValidate.setParameter("cmpy_id", cmpyId);
			}
			if( !refPfx.trim().equalsIgnoreCase("") ) {
				queryValidate.setParameter("ref_pfx", refPfx);
			}
			if( !refNo.trim().equalsIgnoreCase("") ) {
				queryValidate.setParameter("ref_no", Integer.parseInt(refNo));
			}
						
			List<Object[]> listRecords = queryValidate.list();

			for (Object[] record : listRecords) {

				UnappliedDebitInfo output = new UnappliedDebitInfo();

				output.setDesc(record[0].toString());
				output.setDrAmt(record[1].toString());
				output.setCry(record[2].toString());
				
				starBrowseResponse.output.fldTblUnappliedDr.add(output);

			}
			
			starBrowseResponse.output.chkProof = calcCashRcptCheckProof(cmpyId, refPfx, refNo);

		} catch (Exception e) {
			// TODO: handle exception
			logger.debug("AR Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
	
	public void readGLDistribution(BrowseResponse<GLDistributionBrowseOutput> starBrowseResponse, String cmpyId, String refPfx, String refNo)
	{
		logger.info("AR Info : {}", new Object() {}.getClass().getEnclosingMethod().getName());
		
		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();
			
			hql = "select CAST(gds_bsc_gl_acct AS VARCHAR(8)), CAST(bga_desc30 AS VARCHAR(30)), CAST(gds_sacct AS VARCHAR(30)), gds_dr_amt, "
					+ " gds_cr_amt from tctgds_rec, glrbga_rec where bga_bsc_gl_acct = gds_bsc_gl_acct ";

			if( !cmpyId.trim().equalsIgnoreCase("") ) {
				hql += " and gds_cmpy_id=:cmpy_id";
			}
			if( !refPfx.trim().equalsIgnoreCase("") ) {
				hql += " and gds_ref_pfx=:ref_pfx";
			}
			if( !refNo.trim().equalsIgnoreCase("") ) {
				hql += " and gds_ref_no=:ref_no";
			}
			
			Query queryValidate = session.createSQLQuery(hql);
			
			if( !cmpyId.trim().equalsIgnoreCase("") ) {
				queryValidate.setParameter("cmpy_id", cmpyId);
			}
			if( !refPfx.trim().equalsIgnoreCase("") ) {
				queryValidate.setParameter("ref_pfx", refPfx);
			}
			if( !refNo.trim().equalsIgnoreCase("") ) {
				queryValidate.setParameter("ref_no", Integer.parseInt(refNo));
			}
						
			List<Object[]> listRecords = queryValidate.list();

			for (Object[] record : listRecords) {

				GLDistributionInfo output = new GLDistributionInfo();

				output.setGlAcct(record[0].toString());
				output.setDesc(record[1].toString());
				output.setSacct(record[2].toString());
				output.setDrAmt(record[3].toString());
				output.setCrAmt(record[4].toString());
				
				starBrowseResponse.output.fldTblGLDist.add(output);

			}
			
			starBrowseResponse.output.chkProof = calcCashRcptCheckProof(cmpyId, refPfx, refNo);

		} catch (Exception e) {
			// TODO: handle exception
			logger.debug("AR Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
	
	public BigDecimal calcCashRcptCheckProof(String cmpyId, String refPfx, String refNo)
	{
		logger.info("AR Info : {}", new Object() {}.getClass().getEnclosingMethod().getName());
		
		Session session = null;
		String hql = "";
		BigDecimal chkProof = new BigDecimal(0);
		BigDecimal chkAmt = new BigDecimal(0);
		BigDecimal advPymnt = new BigDecimal(0);
		BigDecimal unappliedDebit = new BigDecimal(0);
		BigDecimal glDebit = new BigDecimal(0);
		BigDecimal glCredit = new BigDecimal(0);

		try {
			session = SessionUtilInformix.getSession();
			
			//Get Check Amount
			hql = "SELECT csh_cus_chk_amt, 1 FROM artcsh_rec WHERE 1=1 ";

			if( !cmpyId.trim().equalsIgnoreCase("") ) {
				hql += " AND csh_cmpy_id=:cmpy_id";
			}
			if( !refPfx.trim().equalsIgnoreCase("") ) {
				hql += " AND csh_crcp_pfx=:ref_pfx";
			}
			if( !refNo.trim().equalsIgnoreCase("") ) {
				hql += " AND csh_crcp_no=:ref_no";
			}
			
			Query queryValidate = session.createSQLQuery(hql);
			
			if( !cmpyId.trim().equalsIgnoreCase("") ) {
				queryValidate.setParameter("cmpy_id", cmpyId);
			}
			if( !refPfx.trim().equalsIgnoreCase("") ) {
				queryValidate.setParameter("ref_pfx", refPfx);
			}
			if( !refNo.trim().equalsIgnoreCase("") ) {
				queryValidate.setParameter("ref_no", Integer.parseInt(refNo));
			}
						
			List<Object[]> listRecords = queryValidate.list();

			for (Object[] record : listRecords) {
				if (record[0] != null) {
					chkAmt = new BigDecimal(record[0].toString());
				}
			}
			System.out.println("chkAmt--->" + chkAmt);
			
			//Get Advance Payment
			hql = "select SUM(adp_adv_amt) AS tot_adv_amt, 1 from artadp_rec, artcrs_rec, artcsh_rec where crs_cmpy_id = csh_cmpy_id and "
					+ "crs_ssn_id = csh_ssn_id and csh_cmpy_id = adp_cmpy_id and csh_crcp_pfx = adp_ref_pfx and csh_crcp_no = adp_ref_no "
					+ "and adp_adv_ref_pfx='CJ' ";

			if( !cmpyId.trim().equalsIgnoreCase("") ) {
				hql += " AND adp_cmpy_id=:cmpy_id";
			}
			if( !refPfx.trim().equalsIgnoreCase("") ) {
				hql += " AND adp_ref_pfx=:ref_pfx";
			}
			if( !refNo.trim().equalsIgnoreCase("") ) {
				hql += " AND adp_ref_no=:ref_no";
			}
			
			queryValidate = session.createSQLQuery(hql);
			
			if( !cmpyId.trim().equalsIgnoreCase("") ) {
				queryValidate.setParameter("cmpy_id", cmpyId);
			}
			if( !refPfx.trim().equalsIgnoreCase("") ) {
				queryValidate.setParameter("ref_pfx", refPfx);
			}
			if( !refNo.trim().equalsIgnoreCase("") ) {
				queryValidate.setParameter("ref_no", Integer.parseInt(refNo));
			}
						
			listRecords = queryValidate.list();

			for (Object[] record : listRecords) {
				if (record[0] != null) {
					advPymnt = new BigDecimal(record[0].toString());
				}
			}
			System.out.println("advPymnt--->" + advPymnt);
			
			
			//Get Unapplied Debit
			hql = "select SUM(adp_adv_amt) AS tot_adv_amt, 1 from artadp_rec, artcrs_rec, artcsh_rec where crs_cmpy_id = csh_cmpy_id and "
					+ "crs_ssn_id = csh_ssn_id and csh_cmpy_id = adp_cmpy_id and csh_crcp_pfx = adp_ref_pfx and csh_crcp_no = adp_ref_no "
					+ "and adp_adv_ref_pfx='DA' ";

			if( !cmpyId.trim().equalsIgnoreCase("") ) {
				hql += " AND adp_cmpy_id=:cmpy_id";
			}
			if( !refPfx.trim().equalsIgnoreCase("") ) {
				hql += " AND adp_ref_pfx=:ref_pfx";
			}
			if( !refNo.trim().equalsIgnoreCase("") ) {
				hql += " AND adp_ref_no=:ref_no";
			}
			
			queryValidate = session.createSQLQuery(hql);
			
			if( !cmpyId.trim().equalsIgnoreCase("") ) {
				queryValidate.setParameter("cmpy_id", cmpyId);
			}
			if( !refPfx.trim().equalsIgnoreCase("") ) {
				queryValidate.setParameter("ref_pfx", refPfx);
			}
			if( !refNo.trim().equalsIgnoreCase("") ) {
				queryValidate.setParameter("ref_no", Integer.parseInt(refNo));
			}
						
			listRecords = queryValidate.list();

			for (Object[] record : listRecords) {
				if (record[0] != null) {
					unappliedDebit = new BigDecimal(record[0].toString());
				}
			}
			System.out.println("unappliedDebit--->" + unappliedDebit);

			
			//Get GL Distribution
			hql = "select SUM(gds_dr_amt) AS tot_dr_amt, SUM(gds_cr_amt) AS tot_cr_amt from tctgds_rec, glrbga_rec where "
					+ "bga_bsc_gl_acct = gds_bsc_gl_acct ";

			if( !cmpyId.trim().equalsIgnoreCase("") ) {
				hql += " AND gds_cmpy_id=:cmpy_id";
			}
			if( !refPfx.trim().equalsIgnoreCase("") ) {
				hql += " AND gds_ref_pfx=:ref_pfx";
			}
			if( !refNo.trim().equalsIgnoreCase("") ) {
				hql += " AND gds_ref_no=:ref_no";
			}
			
			queryValidate = session.createSQLQuery(hql);
			
			if( !cmpyId.trim().equalsIgnoreCase("") ) {
				queryValidate.setParameter("cmpy_id", cmpyId);
			}
			if( !refPfx.trim().equalsIgnoreCase("") ) {
				queryValidate.setParameter("ref_pfx", refPfx);
			}
			if( !refNo.trim().equalsIgnoreCase("") ) {
				queryValidate.setParameter("ref_no", Integer.parseInt(refNo));
			}
						
			listRecords = queryValidate.list();

			for (Object[] record : listRecords) {
				if (record[0] != null) {
					glDebit = new BigDecimal(record[0].toString());
				}
				if (record[1] != null) {
					glCredit = new BigDecimal(record[1].toString());
				}
			}
			System.out.println("glDebit--->" + glDebit);
			System.out.println("glCredit--->" + glCredit);
			System.out.println("chkProof--->" + chkProof);
			chkProof = chkAmt;
			chkProof = chkProof.subtract(advPymnt);
			chkProof = chkProof.add(unappliedDebit);
			chkProof = chkProof.add(glDebit);
			chkProof = chkProof.subtract(glCredit);
			System.out.println("chkProof--->" + chkProof);

		} catch (Exception e) {
			
			logger.debug("AR Info : {}", e.getMessage(), e);
			
		} finally {
			if (session != null) {
				session.close();
			}
		}
		
		return chkProof;
	}
}
