package com.star.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.CommonFunctions;
import com.star.common.MaintanenceResponse;
import com.star.common.SessionUtil;
import com.star.linkage.payment.PaymentInfo;
import com.star.linkage.wkf.WkfChk;
import com.star.linkage.wkf.WkfChkBrowseOutput;
import com.star.linkage.wkf.WkfCondtnBrowseOutput;
import com.star.linkage.wkf.WorkflowInput;
import com.star.linkage.wkf.WorkflowManOutput;
import com.star.linkage.wkf.WorkflowStepInput;
import com.star.modal.CstmWkfAprvr;
import com.star.modal.Workflow;
import com.star.modal.WorkflowStep;

public class WkfInfoDAO {

	private static Logger logger = LoggerFactory.getLogger(WkfInfoDAO.class);
	CommonFunctions cmn = new CommonFunctions();

	public void wkfChk(BrowseResponse<WkfChkBrowseOutput> starBrowseResponse, String wfName) {
		Session session = null;
		Transaction tx = null;
		String hql = "";
		int ircrdCount = 0;

		try {
			session = SessionUtil.getSession();

			hql = "select count(*) from cstm_ocr_wkf where LOWER(TRIM(wkf_name))=LOWER(TRIM(:wkf_name))";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("wkf_name", wfName);
			String wkf = String.valueOf(queryValidate.list().get(0));

			starBrowseResponse.output.nbrRecRtn = Integer.parseInt(wkf);

		} catch (Exception e) {

			logger.debug("Workflow Information : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(e.getMessage());

		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void createWkfStep(Session session, WorkflowStepInput stepInput, long wkfId) throws Exception {

		WorkflowStep step = new WorkflowStep();

		step.setWkfId(wkfId);
		step.setStepName(stepInput.getStepName());
		step.setAmtFlg(stepInput.getAmtFlg());
		step.setBrhFlg(stepInput.getBrhFlg());
		step.setVenFlg(stepInput.getVenFlg());
		step.setDayFlg(stepInput.getDayFlg());
		step.setAmtFrm(stepInput.getAmtFrm());
		step.setAmtTo(stepInput.getAmtTo());
		step.setBrh(stepInput.getBrh());
		step.setVenId(stepInput.getVenId());
		step.setDays(stepInput.getDays());
		step.setDayBef(stepInput.getDayBef());
		step.setVchrCat(stepInput.getVchrCat());
		step.setVchrCatFlg(stepInput.getVchrCatFlg());

		step.setCrcnFlg(stepInput.getCrcnFlg());

		for (int i = 0; i < stepInput.getWkfAprvrList().size(); i++) {
			if (stepInput.getWkfAprvrList().get(i).getOrder() == 1) {
				step.setApvr(stepInput.getWkfAprvrList().get(i).getUsrId());
			}
		}

		session.save(step);

		/* SET APPROVER RECORD */
		createWkfAprvr(session, stepInput, wkfId);

	}

	public void delWkfAprvr(long wkfId, Session session) throws Exception {
		String hql = "";

		hql = "delete from cstm_wkf_aprvr where wkf_id=:wkf";

		Query queryValidate = session.createSQLQuery(hql);

		queryValidate.setParameter("wkf", wkfId);

		queryValidate.executeUpdate();

	}

	public void createWkfAprvr(Session session, WorkflowStepInput stepInput, long wkfId) throws Exception {

		for (int i = 0; i < stepInput.getWkfAprvrList().size(); i++) {
			CstmWkfAprvr aprvr = new CstmWkfAprvr();

			aprvr.setStepId(0);
			aprvr.setWkfId(wkfId);
			aprvr.setWkfAprvOrd(stepInput.getWkfAprvrList().get(i).getOrder());
			aprvr.setWkfAprvr(stepInput.getWkfAprvrList().get(i).getUsrId());

			session.save(aprvr);
		}
	}

	public void wkfSave(MaintanenceResponse<WorkflowManOutput> starManResponse, WorkflowInput input,
			WorkflowStepInput stepInput, String usrId) {
		Session session = null;
		Transaction tx = null;
		Date date = new Date();

		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			Workflow wrkflow = new Workflow();
			wrkflow.setWkfName(input.getWkfNm());
			wrkflow.setCrtdBy(usrId);
			wrkflow.setCrtdDate(date);

			session.save(wrkflow);

			createWkfStep(session, stepInput, wrkflow.getWkfId());

			tx.commit();

		} catch (Exception e) {

			logger.debug("Workflow Information : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(e.getMessage());
			tx.rollback();

		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void chkSaveCondition(BrowseResponse<WkfChkBrowseOutput> starBrowseRes, WorkflowStepInput stp) {
		// TODO Auto-generated method stub
		Session session = null;
		Date date = new Date();
		String hql = null;
		String hql1 = null;
		int amtFlg = stp.getAmtFlg();
		int brhFlg = stp.getBrhFlg();
		int venFlg = stp.getVenFlg();
		int dayFlg = stp.getDayFlg();

		WkfCondtnBrowseOutput starBrowseres;

		hql = "SELECT wkf.wkf_id,wkf.wkf_name, stp.step_id,stp.wkf_id,stp.amt_flg, stp.brh_flg,"
				+ "stp.ven_flg, stp.day_flg, stp.amt_frm, stp.amt_to, stp.brh, stp.ven_id, stp.days, stp.day_bef, stp.apvr, stp.crcn_flg "
				+ "FROM cstm_ocr_wkf_step AS stp, cstm_ocr_wkf As wkf WHERE stp.wkf_id=wkf.wkf_id";

		if (amtFlg != 0) {
			hql += " AND amt_flg=:amt_flg";
		}
		if (brhFlg != 0) {
			hql += " AND brh_flg=:brh_flg";
		}
		if (venFlg != 0) {
			hql += " AND ven_flg=:ven_flg";
		}
		if (dayFlg != 0) {
			hql += " AND day_flg=:day_flg";
		}
		session = SessionUtil.getSession();

		Query queryValidate = session.createSQLQuery(hql);
		if (amtFlg != 0) {
			queryValidate.setParameter("amt_flg", amtFlg);
		}
		if (brhFlg != 0) {
			queryValidate.setParameter("brh_flg", brhFlg);
		}
		if (venFlg != 0) {
			queryValidate.setParameter("ven_flg", venFlg);
		}
		if (dayFlg != 0) {
			queryValidate.setParameter("day_flg", dayFlg);
		}
		List<Object[]> listRecords = queryValidate.list();
		List<WkfChk> list = new ArrayList<WkfChk>();
		int flg = 0;
		for (Object[] record : listRecords) {
			WkfChk output = new WkfChk();
			output.setWfName(record[1].toString());
			if (record[4] != null) {
				output.setAmtFlg(record[4].toString());
			} else {
				output.setAmtFlg("");

			}
			if (record[5] != null) {
				output.setBrhFlg(record[5].toString());
			} else
				output.setBrhFlg("");
			if (record[6] != null) {
				output.setVenFlg(record[6].toString());
			} else
				output.setVenFlg("");
			if (record[7] != null) {
				output.setDayFlg(record[7].toString());
			} else
				output.setDayFlg("");
			if (amtFlg != 0) {
				float frm = Float.parseFloat(record[8].toString());
				float to = Float.parseFloat(record[9].toString());
				if ((frm > 0 && frm <= stp.getAmtFrm()) || (0 > to && to >= stp.getAmtTo())) {
					output.setAmtFrom(record[8].toString());
					output.setAmtTo(record[9].toString());
					starBrowseRes.output.rtnSts = 1;
					if (flg == 0)
						flg = 1;
				}
			}
			if (brhFlg != 0) {
				String brh = record[10].toString();
				if (brh.contains(stp.getBrh())) {
					output.setBrh(brh);
					System.out.println(brh + "===" + stp.getBrh());

					starBrowseRes.output.rtnSts = 1;
					if (flg == 0)
						flg = 1;
				}
			}
			if (venFlg != 0) {
				String ven = record[11].toString();
				if (ven.contains(stp.getVenId())) {
					output.setVenId(ven);
					System.out.println(ven + "===" + stp.getVenId());
					starBrowseRes.output.rtnSts = 1;
					if (flg == 0)
						flg = 1;
				}
			}
			if (dayFlg != 0) {
				if (record[12] != null) {
					output.setDays(record[12].toString());
					starBrowseRes.output.rtnSts = 1;
					if (record[13] != null) {
						output.setDayBef(record[13].toString());
						starBrowseRes.output.rtnSts = 1;
						if (flg == 0)
							flg = 1;
					}
				}
			}
			if (record[6] != null) {
				output.setVenFlg(record[6].toString());
			} else
				output.setVenFlg("");
			list.add(output);

		}
		if (flg != 1) {
			System.out.println("checked new wk can save now" + flg);
		} else {
			System.out.println(flg);
			for (WkfChk s : list) {
				starBrowseRes.output.fldTblWkfChk.add(s);
			}
			System.out.println("List of workflow conflicct --" + list);
			starBrowseRes.output.rtnSts = 1;
		}

	}

	public void updWkf(String userId, String cmpyId, BrowseResponse<WkfChkBrowseOutput> starBrowseResponse,
			WorkflowStepInput workflowStepInput) {
		// TODO Auto-generated method stub
	}

	public void validateStep(BrowseResponse<WkfChkBrowseOutput> starBrowseRes, WorkflowStepInput stp) {
		// TODO Auto-generated method stub
		Session session = null;
		String hql = null;
		int amtFlg = stp.getAmtFlg();
		int brhFlg = stp.getBrhFlg();
		int venFlg = stp.getVenFlg();
		int dayFlg = stp.getDayFlg();
		int catFlg = stp.getVchrCatFlg();

		int iAmtChk = 1;
		int iBrhChk = 1;
		int iVenChk = 1;
		int iDayChk = 1;
		int iCatChk = 1;
		

		hql = "SELECT wkf_name, step_id, cstm_ocr_wkf_step.wkf_id, step_name, amt_flg, brh_flg, ven_flg, day_flg, amt_frm, amt_to, brh,"
				+ " ven_id, days, day_bef, apvr, crcn_flg, vchr_cat, cat_flg FROM cstm_ocr_wkf_step, cstm_ocr_wkf WHERE"
				+ " cstm_ocr_wkf.wkf_id=cstm_ocr_wkf_step.wkf_id AND deleted_by IS NULL AND amt_flg=:amt_flg AND brh_flg=:brh_flg"
				+ " AND ven_flg=:ven_flg AND day_flg=:day_flg AND cat_flg=:cat_flg";

		if (stp.getDayFlg() == 1) {
			if (stp.getDayBef() == 1) {
				hql += " AND day_bef=1";
			} else {
				hql += " AND day_bef=0";
			}
		}

		if (stp.getWkfId().length() > 0) {
			hql += " AND cstm_ocr_wkf_step.wkf_id <> :wkf_id";
		}

		session = SessionUtil.getSession();

		Query queryValidate = session.createSQLQuery(hql);

		queryValidate.setParameter("amt_flg", amtFlg);
		queryValidate.setParameter("brh_flg", brhFlg);
		queryValidate.setParameter("ven_flg", venFlg);
		queryValidate.setParameter("day_flg", dayFlg);
		queryValidate.setParameter("cat_flg", catFlg);

		if (stp.getWkfId().length() > 0) {
			queryValidate.setParameter("wkf_id", Integer.parseInt(stp.getWkfId()));
		}

		List<Object[]> listRecords = queryValidate.list();
		List<WkfChk> list = new ArrayList<WkfChk>();

		for (Object[] record : listRecords) {

			iAmtChk = 1;
			iBrhChk = 1;
			iVenChk = 1;
			iDayChk = 1;
			iCatChk = 1;

			if (amtFlg == 1) {
				iAmtChk = 0;

				float frm = Float.parseFloat(record[8].toString());
				float to = Float.parseFloat(record[9].toString());

				if ((frm <= stp.getAmtFrm() && to >= stp.getAmtFrm()) || (frm <= stp.getAmtTo() && to >= stp.getAmtTo())
						|| (stp.getAmtFrm() <= frm && stp.getAmtTo() >= to)) {
					iAmtChk = 1;
				}
			}

			if (brhFlg == 1) {
				iBrhChk = 0;

				String brh = record[10].toString();
				String[] inptBrhArr = stp.getBrh().split(",");
				for (int iCnt = 0; iCnt < inptBrhArr.length; iCnt++) {
					if (brh.contains(inptBrhArr[iCnt])) {
						iBrhChk = 1;
					}
				}
			}
			
			if (catFlg == 1) {
				iCatChk = 0;

				String cat = record[16].toString();
				String[] inptCatArr = stp.getVchrCat().split(",");
				for (int iCnt = 0; iCnt < inptCatArr.length; iCnt++) {
					if (cat.contains(inptCatArr[iCnt])) {
						iCatChk = 1;
					}
				}
			}

			if (venFlg == 1) {
				iVenChk = 0;

				String ven = record[11].toString();
				String[] inptVenArr = stp.getVenId().split(",");
				for (int iCnt = 0; iCnt < inptVenArr.length; iCnt++) {
					if (ven.contains(inptVenArr[iCnt])) {
						iVenChk = 1;
					}
				}
			}

			if (dayFlg == 1) {
				iDayChk = 0;

				if (stp.getDayBef() == 1) {
					iDayChk = 1;
				} else {
					iDayChk = 1;
				}
			}

			if (iAmtChk == 1 && iBrhChk == 1 && iVenChk == 1 && iDayChk == 1 && iCatChk == 1) {
				WkfChk output = new WkfChk();

				if (record[0] != null) {
					output.setWfName(record[0].toString());
				} else {
					output.setWfName("");
				}
				output.setWfId(record[2].toString());
				output.setAmtFlg(record[4].toString());
				output.setBrhFlg(record[5].toString());
				output.setVenFlg(record[6].toString());
				output.setDayFlg(record[7].toString());
				output.setAmtFrom(record[8].toString());
				output.setAmtTo(record[9].toString());
				if (record[10] != null) {
					output.setBrh(record[10].toString());
				} else {
					output.setBrh("");
				}
				if (record[11] != null) {
					output.setVenId(record[11].toString());
				} else {
					output.setVenId("");
				}
				output.setDays(record[12].toString());
				output.setDayBef(record[13].toString());
				output.setCostRecon(record[15].toString());
				output.setVchrCat(record[16].toString());
				output.setVchrCatFlg(record[17].toString());

				starBrowseRes.output.fldTblWkfChk.add(output);
			}
		}
	}

	public void updateWorkflow(MaintanenceResponse<WorkflowManOutput> starManResponse, WorkflowInput input,
			WorkflowStepInput stepInput, String usrId) {
		Session session = null;
		Transaction tx = null;
		String hql = "";

		try {

			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			hql = "UPDATE cstm_ocr_wkf_step SET step_name=:step_name, amt_flg=:amt_flg, brh_flg=:brh_flg, ven_flg=:ven_flg, "
					+ " day_flg=:day_flg, amt_frm=:amt_frm, amt_to=:amt_to, brh=:brh, ven_id=:ven_id, days=:days, day_bef=:day_bef, "
					+ " apvr=:apvr, crcn_flg=:crcn_flg, vchr_cat=:cat WHERE wkf_id=:wkf_id";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("step_name", stepInput.getStepName());
			queryValidate.setParameter("amt_flg", stepInput.getAmtFlg());
			queryValidate.setParameter("brh_flg", stepInput.getBrhFlg());
			queryValidate.setParameter("ven_flg", stepInput.getVenFlg());
			queryValidate.setParameter("day_flg", stepInput.getDayFlg());
			queryValidate.setParameter("amt_frm", stepInput.getAmtFrm());
			queryValidate.setParameter("amt_to", stepInput.getAmtTo());
			queryValidate.setParameter("brh", stepInput.getBrh());
			queryValidate.setParameter("ven_id", stepInput.getVenId());
			queryValidate.setParameter("days", stepInput.getDays());
			queryValidate.setParameter("day_bef", stepInput.getDayBef());

			queryValidate.setParameter("crcn_flg", stepInput.getCrcnFlg());
			queryValidate.setParameter("cat", stepInput.getVchrCat());
			queryValidate.setParameter("wkf_id", Integer.parseInt(stepInput.getWkfId()));

			if(stepInput.getWkfAprvrList().size() > 0) {
				for (int i = 0; i < stepInput.getWkfAprvrList().size(); i++) {
					if (stepInput.getWkfAprvrList().get(i).getOrder() == 1) {
	
						queryValidate.setParameter("apvr", stepInput.getWkfAprvrList().get(i).getUsrId());
					}
				}
			}else {
				queryValidate.setParameter("apvr", "");
			}

			queryValidate.executeUpdate();

			/* DELETE EXISTING APPROVER LIST */
			delWkfAprvr(Long.parseLong(stepInput.getWkfId()), session);

			createWkfAprvr(session, stepInput, Long.parseLong(stepInput.getWkfId()));

			tx.commit();

		} catch (Exception e) {
			logger.debug("Workflow Info : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(e.getMessage());

			tx.rollback();
		} finally {

			session.close();
		}
	}
	
	
	public void delWorkflow(MaintanenceResponse<WorkflowManOutput> starManResponse, String wkfNm, String wkfId) throws Exception {
		Session session = null;
		Transaction tx = null;
		String hql = "";

		try {

			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			hql = "delete from cstm_ocr_wkf where wkf_name=:wkf and wkf_id=:id";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("wkf", wkfNm);
			queryValidate.setParameter("id", Integer.parseInt(wkfId));
		
			queryValidate.executeUpdate();
			
			
			hql = "delete from cstm_ocr_wkf_step where step_name=:wkf and wkf_id=:id";

			queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("wkf", wkfNm);
			queryValidate.setParameter("id", Integer.parseInt(wkfId));
		
			queryValidate.executeUpdate();

			/* DELETE EXISTING APPROVER LIST */
			delWkfAprvr(Integer.parseInt(wkfId), session);

			tx.commit();

		} catch (Exception e) {
			logger.debug("Workflow Info : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(e.getMessage());

			tx.rollback();
		} finally {

			session.close();
		}
	}

}
