package com.star.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.CommonConstants;
import com.star.common.CommonFunctions;
import com.star.common.SessionUtil;
import com.star.common.SessionUtilInformix;
import com.star.common.SessionUtilInformixESP;
import com.star.linkage.ach.CompanyAddress;
import com.star.linkage.ach.CompanyBankInfo;
import com.star.linkage.ach.InvoiceInfo;
import com.star.linkage.ach.VendorAddress;
import com.star.linkage.ach.VendorBankInfo;
import com.star.linkage.cstmdocinfo.VchrInfo;
import com.star.modal.ElecPaySeq;

public class GetACHDetailsDAO {

	private static Logger logger = LoggerFactory.getLogger(GetACHDetailsDAO.class);

	/* GET ROUTING NUMBER AND ACCOUNT NO TO PREPARE ACH FILE */
	public CompanyBankInfo getBankInfoByCompany(String cmpyId, String bnkCode, String acctNo) {
		Session session = null;
		CompanyBankInfo bankInfo = new CompanyBankInfo();
		//cmpyId="SQ1";
		try {

			String hql = "";

			session = SessionUtil.getSession();

			hql = "select bnk_nm, bnk_rout_no, bnk_acct_id, bnk_acct_desc, bnk_acct_no, "
					+ "cmpy_bnk.bnk_code, bnk_acct.bnk_ach_id, bnk_acct.bnk_mmb_id from cstm_cmpy_bnk_info cmpy_bnk, "
					+ "cstm_bnk_acct_info bnk_acct, cstm_bnk_info bnk_info  where "
					+ "cmpy_bnk.bnk_code = bnk_acct.bnk_code and bnk_info.bnk_code = cmpy_bnk.bnk_code and "
					+ "cmpy_id=:cmpy";

			hql = hql + " and bnk_acct.bnk_code = :bnk_code";

			hql = hql + " and bnk_acct_no = :bnk_acct";

			Query queryValidate = session.createSQLQuery(hql);

			if (cmpyId.length() > 0) {
				queryValidate.setParameter("cmpy", cmpyId);
			}

			if (bnkCode.length() > 0) {
				queryValidate.setParameter("bnk_code", bnkCode);
			}

			if (acctNo.length() > 0) {
				queryValidate.setParameter("bnk_acct", acctNo);
			}

			List<Object[]> listCompanyBank = queryValidate.list();

			for (Object[] bnkCompany : listCompanyBank) {
				bankInfo.setBnkName(bnkCompany[0].toString());
				bankInfo.setRoutingNo(bnkCompany[1].toString());
				bankInfo.setAcctNm(bnkCompany[3].toString());
				bankInfo.setAcctNo(bnkCompany[4].toString());
				bankInfo.setBnkCode(bnkCompany[5].toString());
				if (bnkCompany[6] != null) {
					bankInfo.setBnkAchId(bnkCompany[6].toString());
				} else {
					bankInfo.setBnkAchId("");
				}

				if (bnkCompany[7] != null) {
					bankInfo.setBnkMmbId(bnkCompany[7].toString());
				} else {
					bankInfo.setBnkMmbId("");
				}
			}

		} catch (Exception e) {
			logger.debug("ACH Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}

		return bankInfo;

	}

	public VendorBankInfo getBankInfoByVendor(String cmpyId, String venId) {
		Session session = null;
		VendorBankInfo bankInfo = new VendorBankInfo();
		GetCommonDAO commonDAO = new GetCommonDAO();
		CommonFunctions commonFunctions = new CommonFunctions();
		try {

			String hql = "";

			session = SessionUtil.getSession();

			hql = "select bnk_info.ven_id, pay_mthd, bnk_key, bnk_nm, bnk_acc_no from cstm_ven_add_info add_info, "
					+ "cstm_ven_bnk_info bnk_info "
					+ "where add_info.cmpy_id = bnk_info.cmpy_id and add_info.ven_id = bnk_info.ven_id "
					+ "and add_info.cmpy_id=:cmpy and add_info.ven_id=:venid";

			Query queryValidate = session.createSQLQuery(hql);

			if (cmpyId.length() > 0) {
				queryValidate.setParameter("cmpy", cmpyId);
				queryValidate.setParameter("venid", venId);
			}

			List<Object[]> listCompanyVendor = queryValidate.list();

			for (Object[] bnkVendor : listCompanyVendor) {
				String temp = commonFunctions.whiteEscapeCharacter(bnkVendor[0].toString().trim());
				bankInfo.setVenId(temp);
				
				temp = commonFunctions.whiteEscapeCharacter(bnkVendor[1].toString().trim());
				bankInfo.setPayMthd(temp);
				
				temp = commonFunctions.whiteEscapeCharacter(bnkVendor[2].toString().trim());
				bankInfo.setBnkRoutingNo(temp);
				
				temp = commonFunctions.whiteEscapeCharacter(bnkVendor[3].toString().trim());
				bankInfo.setBnkNm(temp);
				
				temp = commonFunctions.whiteEscapeCharacter(bnkVendor[4].toString().trim());
				bankInfo.setBnkAcctNo(temp);
				
				String venNm = commonDAO.getVendorName(cmpyId, bankInfo.getVenId());
				temp = commonFunctions.whiteEscapeCharacter(venNm);
				bankInfo.setVenNm(temp);
			}

		} catch (Exception e) {
			logger.debug("ACH Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}

		return bankInfo;

	}

	public InvoiceInfo getInvoiceDetails(String invCtlNo) {
		Session session = null;
		InvoiceInfo invoiceInfo = new InvoiceInfo();
		try {

			String hql = "";

			session = SessionUtil.getSession();

			hql = "select vchr_ctl_no, vchr_inv_no, vchr_ven_id, vchr_amt from cstm_vchr_info where vchr_ctl_no=:ctlno";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("ctlno", Integer.parseInt(invCtlNo));

			List<Object[]> listInvoices = queryValidate.list();

			for (Object[] invoices : listInvoices) {
				invoiceInfo.setVchrCtlNo(invoices[0].toString());
				invoiceInfo.setVchrInvNo(invoices[1].toString());
				invoiceInfo.setVchrVenId(invoices[2].toString());
				invoiceInfo.setVchrAmt(Double.parseDouble(invoices[3].toString()));
			}

		} catch (Exception e) {
			logger.debug("ACH Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}

		return invoiceInfo;

	}

	public InvoiceInfo getInvoiceDetailsStx(InvoiceInfo invoiceInfo, String vchrNo) {
		Session session = null;

		try {

			String hql = "";

			session = SessionUtilInformix.getSession();

			if (CommonConstants.DB_TYP.equals("POS")) {
				hql = "	select cast(pyh_cmpy_id as varchar(3)) cmpy_id, cast(pyh_opa_pfx as varchar(2)) pfx, "
						+ "pyh_opa_no, cast(pyh_ven_id as varchar(8)) ven_id, cast(pyh_ven_inv_no as varchar(22)) ven_inv_no, "
						+ "pyh_orig_amt ,pyh_disc_amt,cast(pyh_cry as varchar(3)) cry, to_char(pyh_ven_inv_dt, 'yyyy-mm-dd') inv_dt,"
						+ " (case when (pyh_disc_dt - date(now())) is null or (pyh_disc_dt - date(now()) < 0) then 'N' else 'Y' end) disc_sts"
						+ " from aptpyh_rec where 1 = 1";

				if (vchrNo.length() > 0) {
					hql = hql + " and pyh_opa_pfx || '-' || pyh_opa_no =:vchr_no";
				}
			} else if (CommonConstants.DB_TYP.equals("INF")) {

				hql = "	select cast(pyh_cmpy_id as varchar(3)) cmpy_id, cast(pyh_opa_pfx as varchar(2)) pfx, "
						+ "pyh_opa_no, cast(pyh_ven_id as varchar(8)) ven_id, cast(pyh_ven_inv_no as varchar(22)) ven_inv_no, "
						+ "pyh_orig_amt ,pyh_disc_amt,cast(pyh_cry as varchar(3)) cry, to_char(pyh_ven_inv_dt, '%Y-%m-%d') inv_dt, "
						+ "(case when (pyh_disc_dt - date(now())) is null or (pyh_disc_dt - date(now()) < 0) then 'N' else 'Y' end) disc_sts"
						+ "from aptpyh_rec where 1 = 1";

				if (vchrNo.length() > 0) {
					hql = hql + " and pyh_opa_pfx || '-' || pyh_opa_no =:vchr_no";
				}

			}

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("vchr_no", vchrNo);

			List<Object[]> listVouchers = queryValidate.list();

			for (Object[] voucher : listVouchers) {
				invoiceInfo.setCmpyId(voucher[0].toString());
				invoiceInfo.setVchrPfx(voucher[1].toString());
				invoiceInfo.setVchrNo(voucher[2].toString());
				invoiceInfo.setVchrVenId(voucher[3].toString());
				invoiceInfo.setVchrInvNo(voucher[4].toString().trim());
				invoiceInfo.setVchrAmt(Double.parseDouble(voucher[5].toString()));
				invoiceInfo.setVchrDiscAmt(Double.parseDouble(voucher[6].toString()));
				invoiceInfo.setVchrCry(voucher[7].toString());
				invoiceInfo.setInvDt(voucher[8].toString());
				invoiceInfo.setVchrAmtStr(voucher[6].toString());

				if (voucher[9].toString().equals("N")) {
					invoiceInfo.setVchrDiscAmt(0);
				}
			}

		} catch (Exception e) {
			logger.debug("ACH Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}

		return invoiceInfo;

	}

	public InvoiceInfo getInvoiceDetailsESP(InvoiceInfo invoiceInfo, String vchrNo) {
		Session session = null;

		try {

			String hql = "";

			session = SessionUtilInformixESP.getSession();

			hql = "select trim('SQ1') cmpy_id, "+
					"cast(pyh_ref_cd as varchar(2)) pfx, "+
					"pyh_ref_no, "+
					"cast(pyh_ven_no as varchar(8)) ven_id, "+
					"cast(pyh_ven_inv_no as varchar(22)) ven_inv_no, "+
					"pyh_amt_ap ,"+
					"pyh_disc_amt_ap,"+
					"trim('') cry,"+
					"to_char(cast(concat(left(cast (pyh_ref_dt as varchar(8)),4),'-',substring(cast (pyh_ref_dt as varchar(8)),5,2) ,'-',right(cast (pyh_ref_dt as varchar(8)),2))  as date), 'yyyy-mm-dd') inv_dt,"+
					"(case when (cast(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-',substring(cast (pyh_disc_dt as varchar(8)),5,2) ,'-',right(cast (pyh_disc_dt as varchar(8)),2))  as date) - date(now())) is null or (cast(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-',substring(cast (pyh_disc_dt as varchar(8)),5,2) ,'-',right(cast (pyh_disc_dt as varchar(8)),2))  as date) - date(now()) < 0) then 'N' else 'Y' end) disc_sts "+
					" from aptpyh_rec where 1 = 1 and pyh_ref_dt>0 and  pyh_disc_dt>0 " ;
			/*ESP query*/
			hql = 	"select trim('CSC') cmpy_id,"+
					"cast(pyh_ref_cd as varchar(2)) pfx,"+
					"pyh_ref_no,"+
					"cast(pyh_ven_no as varchar(8)) ven_id,"+
					"cast(pyh_ven_inv_no as varchar(22)) ven_inv_no,"+
					"pyh_amt_ap ,"+
					"pyh_disc_amt_ap,"+
					"trim(ven_cry) cry,"+
					"concat(concat(concat(left(cast (pyh_ref_dt as varchar(8)),4),'-'),concat(substr(cast(pyh_ref_dt as varchar(8)),5,2),'-')),right(cast (pyh_ref_dt as varchar(8)),2)) inv_dt,"+
					"(case when (cast(concat(concat(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-'), concat(substr(cast (pyh_disc_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_disc_dt as varchar(8)),2))as date) - date(sysdate)) is null "+ 
					"or "+
					"(cast(concat(concat(concat(left(cast (pyh_disc_dt as varchar(8)),4),'-'),  concat(substr(cast (pyh_disc_dt as varchar(8)),5,2) ,'-')),right(cast (pyh_disc_dt as varchar(8)),2))as date) - date(sysdate) < 0) "+ 
					"then 'N' else 'Y' end) disc_sts, "+
					"(select djh_chq_no from apjdjh_rec where apjdjh_rec.djh_brh=apjdjo_rec.djo_ssn_brh and apjdjh_rec.djh_djh_no= apjdjo_rec.djo_djh_no and aptpyh_rec.pyh_ref_no=apjdjo_rec.djo_ref_no) chkno, "+
					//"from aptpyh_rec "+
					//"inner join apjdjo_rec on aptpyh_rec.pyh_ref_no=apjdjo_rec.djo_ref_no "+
					//"from apjdjo_rec "+
					//"inner join aptpyh_rec on aptpyh_rec.pyh_ref_no=apjdjo_rec.djo_ref_no  "+
					//"where 1 = 1 and pyh_ref_dt>0 and  pyh_disc_dt>0 ";
					//"where 1 = 1  ";
					"djh_issue_dt, djh_chq_net_amt "+
					"from apjdjo_rec  "+
					"inner join aptpyh_rec on aptpyh_rec.pyh_ref_no=apjdjo_rec.djo_ref_no "+  
					"inner join aprven_rec on aprven_rec.ven_ven_no=aptpyh_rec.pyh_ven_no "+
					"inner join apjdjh_rec on djo_ssn_brh=djh_brh and djo_djh_no=djh_djh_no and djh_trs_type='V' "+  
					"where  pyh_disc_dt is not null AND pyh_ref_dt is not null AND pyh_due_dt is not null ";
			
			
			if (vchrNo.length() > 0) {
				hql = hql + " and pyh_ref_cd || '-' || pyh_ref_no =:vchr_no";
			}

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("vchr_no", vchrNo);

			List<Object[]> listVouchers = queryValidate.list();

			for (Object[] voucher : listVouchers) {
				invoiceInfo.setCmpyId(voucher[0].toString());
				invoiceInfo.setVchrPfx(voucher[1].toString());
				invoiceInfo.setVchrNo(voucher[2].toString());
				invoiceInfo.setVchrVenId(voucher[3].toString());
				invoiceInfo.setVchrInvNo(voucher[4].toString().trim());
				invoiceInfo.setVchrAmt(Double.parseDouble(voucher[5].toString()));
				invoiceInfo.setVchrDiscAmt(Double.parseDouble(voucher[6].toString()));
				invoiceInfo.setVchrCry(voucher[7].toString());
				invoiceInfo.setInvDt(voucher[8].toString());
				invoiceInfo.setVchrAmtStr(voucher[6].toString());

				if (voucher[9].toString().equals("N")) {
					invoiceInfo.setVchrDiscAmt(0);
				}
				invoiceInfo.setChkNo(voucher[10].toString());
				invoiceInfo.setPaymentDt(voucher[11].toString());
				invoiceInfo.setChkNetAmt(Double.parseDouble(voucher[12].toString()));
			}

		} catch (Exception e) {
			logger.debug("ACH Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}

		return invoiceInfo;

	}
	
	/* GET CURRENT SEQUENCE TO BE USED IN ACH FILE */
	public ElecPaySeq getPaySequence() {
		Session session = null;
		ElecPaySeq elecPaySeq = new ElecPaySeq();

		try {

			String hql = "";

			session = SessionUtil.getSession();

			hql = "select * from elec_pay_seq";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listSeq = queryValidate.list();

			for (Object[] seq : listSeq) {
				elecPaySeq.setId(Integer.parseInt(seq[0].toString()));
				elecPaySeq.setMsgIdSeq(Integer.parseInt(seq[1].toString()));
				elecPaySeq.setPmtIdAch(Integer.parseInt(seq[2].toString()));
				elecPaySeq.setPmtIdWir(Integer.parseInt(seq[3].toString()));
				elecPaySeq.setPmtIdChk(Integer.parseInt(seq[4].toString()));
				elecPaySeq.setInstrIdACH(Integer.parseInt(seq[5].toString()));
				elecPaySeq.setInstrIdChk(Integer.parseInt(seq[6].toString()));
				elecPaySeq.setInstrIdWire(Integer.parseInt(seq[7].toString()));
			}

		} catch (Exception e) {
			logger.debug("ACH Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}

		return elecPaySeq;

	}

	public void updateMsgSeq(Session session, int msgId) {
		/* Session session = null; */
		Transaction tx = null;
		String hql = "";

		msgId = msgId + 1;

		/*
		 * try { session = SessionUtil.getSession(); tx = session.beginTransaction();
		 */

		hql = "update elec_pay_seq set msg_id_seq=:msg_id where id=1";

		Query queryValidate = session.createSQLQuery(hql);

		queryValidate.setParameter("msg_id", msgId);

		queryValidate.executeUpdate();

		/*
		 * tx.commit(); } catch (Exception e) { logger.debug("ACH Info : {}",
		 * e.getMessage(), e); tx.rollback(); } finally { if (session != null) {
		 * session.close(); } }
		 */

	}

	public void updateChkSeq(Session session, int chkId) {
		/*
		 * Session session = null; Transaction tx = null;
		 */
		String hql = "";

		chkId = chkId + 1;

		/*
		 * try { session = SessionUtil.getSession(); tx = session.beginTransaction();
		 */

		hql = "update elec_pay_seq set pmt_id_chk=:chk_id where id=1";

		Query queryValidate = session.createSQLQuery(hql);

		queryValidate.setParameter("chk_id", chkId);

		queryValidate.executeUpdate();

		/*
		 * tx.commit(); } catch (Exception e) { logger.debug("ACH Info : {}",
		 * e.getMessage(), e); tx.rollback(); } finally { if (session != null) {
		 * session.close(); } }
		 */

	}

	public void updateWireSeq(Session session, int wirId) {
		/* Session session = null; */
		Transaction tx = null;
		String hql = "";

		wirId = wirId + 1;

		/*
		 * try { session = SessionUtil.getSession(); tx = session.beginTransaction();
		 */

		hql = "update elec_pay_seq set pmt_id_wir=:wir_id where id=1";

		Query queryValidate = session.createSQLQuery(hql);

		queryValidate.setParameter("wir_id", wirId);

		queryValidate.executeUpdate();

		/*
		 * tx.commit(); } catch (Exception e) { logger.debug("ACH Info : {}",
		 * e.getMessage(), e); tx.rollback(); } finally { if (session != null) {
		 * session.close(); } }
		 */

	}

	public void updateACHSeq(Session session, int chkId) {
		/*
		 * Session session = null; Transaction tx = null;
		 */
		String hql = "";

		chkId = chkId + 1;

		/*
		 * try { session = SessionUtil.getSession(); tx = session.beginTransaction();
		 */

		hql = "update elec_pay_seq set pmt_id_ach=:ach_id where id=1";

		Query queryValidate = session.createSQLQuery(hql);

		queryValidate.setParameter("ach_id", chkId);

		queryValidate.executeUpdate();

		/*
		 * tx.commit(); } catch (Exception e) { logger.debug("ACH Info : {}",
		 * e.getMessage(), e); tx.rollback(); } finally { if (session != null) {
		 * session.close(); } }
		 */

	}

	public void updateInstrChkSeq(Session session, int chkId) {
		/*
		 * Session session = null; Transaction tx = null;
		 */
		String hql = "";

		chkId = chkId + 1;

		/*
		 * try { session = SessionUtil.getSession(); tx = session.beginTransaction();
		 */

		hql = "update elec_pay_seq set instr_id_chk=:chk_id where id=1";

		Query queryValidate = session.createSQLQuery(hql);

		queryValidate.setParameter("chk_id", chkId);

		queryValidate.executeUpdate();

		/*
		 * tx.commit(); } catch (Exception e) { logger.debug("ACH Info : {}",
		 * e.getMessage(), e); tx.rollback(); } finally { if (session != null) {
		 * session.close(); } }
		 */

	}

	public void updateInstrWireSeq(Session session, int wirId) {
		/*Session session = null;
		Transaction tx = null;*/
		String hql = "";

		wirId = wirId + 1;

		/*try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();*/

			hql = "update elec_pay_seq set instr_id_wir=:wir_id where id=1";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("wir_id", wirId);

			queryValidate.executeUpdate();

		/*	tx.commit();
		} catch (Exception e) {
			logger.debug("ACH Info : {}", e.getMessage(), e);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}*/

	}

	public void updateInstrACHSeq(int achId) {
		Session session = null;
		Transaction tx = null;
		String hql = "";

		achId = achId + 1;

		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			hql = "update elec_pay_seq set instr_id_ach=:ach_id where id=1";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("ach_id", achId);

			queryValidate.executeUpdate();

			tx.commit();
		} catch (Exception e) {
			logger.debug("ACH Info : {}", e.getMessage(), e);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public VendorAddress getVendorAddress(String cmpyId, String venId) {
		Session session = null;
		VendorAddress addrInfo = new VendorAddress();
		CommonFunctions commonFunctions = new CommonFunctions();
		try {

			String hql = "";

			session = SessionUtilInformix.getSession();

			hql = "select cast(cva_nm1 as varchar(35)) as ven_nm, cast(cva_pcd as varchar(10)) as pcd,"
					+ "cast(cva_city as varchar(35)) as city,cast(cva_st_prov as varchar(2)) as st_prov,"
					+ "cast(cva_cty as varchar(3)) cty, cast(cva_addr1 as varchar(35)) addr1 from scrcva_rec, "
					+ "aprven_rec where ven_cmpy_id = cva_cmpy_id and ven_ven_id = cva_cus_ven_id and "
					+ "cva_ref_pfx='VN' and cva_cus_ven_typ='V' and cva_addr_typ='P' and cva_addr_no=0 "
					+ "and ven_cmpy_id=:cmpy and ven_ven_id=:venid";

			Query queryValidate = session.createSQLQuery(hql);

			if (cmpyId.length() > 0) {
				queryValidate.setParameter("cmpy", cmpyId);
				queryValidate.setParameter("venid", venId);
			}

			List<Object[]> listVendorAddrP = queryValidate.list();

			for (Object[] vendorAddr : listVendorAddrP) {
				String temp = commonFunctions.whiteEscapeCharacter(vendorAddr[0].toString().trim());
				addrInfo.setVendorNm(temp);
				
				if (vendorAddr[1] != null) {
					 temp = commonFunctions.whiteEscapeCharacter(vendorAddr[1].toString().trim());
					addrInfo.setPostalCode(temp);
				} else {
					addrInfo.setPostalCode("");
				}

				if (vendorAddr[2] != null) {
					temp = commonFunctions.whiteEscapeCharacter(vendorAddr[2].toString().trim());
					addrInfo.setTownNm(temp);
				} else {
					addrInfo.setTownNm("");
				}

				if (vendorAddr[3] != null) {
					temp = commonFunctions.whiteEscapeCharacter(vendorAddr[3].toString().trim());
					addrInfo.setCtrySubDvsn(temp);
				} else {
					addrInfo.setCtrySubDvsn("");
				}

				if (vendorAddr[4] != null) {
					temp = commonFunctions.whiteEscapeCharacter(vendorAddr[4].toString().trim());
					addrInfo.setCty(temp);
				} else {
					addrInfo.setCty("");
				}

				if (vendorAddr[5] != null) {
					temp = commonFunctions.whiteEscapeCharacter(vendorAddr[5].toString().trim());
					addrInfo.setAddr(temp);
				} else {
					addrInfo.setAddr("");
				}
			}

			if (addrInfo.getVendorNm() == null || addrInfo.getVendorNm().length() == 0) {
				hql = "select cast(cva_nm1 as varchar(35)) as ven_nm, cast(cva_pcd as varchar(10)) as pcd,"
						+ "cast(cva_city as varchar(35)) as city,cast(cva_st_prov as varchar(2)) as st_prov,"
						+ "cast(cva_cty as varchar(3)) cty, cast(cva_addr1 as varchar(35)) addr1 from scrcva_rec, "
						+ "aprven_rec where ven_cmpy_id = cva_cmpy_id and ven_ven_id = cva_cus_ven_id and "
						+ "cva_ref_pfx='VN' and cva_cus_ven_typ='V' and cva_addr_typ='L' and cva_addr_no=0 "
						+ "and ven_cmpy_id=:cmpy and ven_ven_id=:venid";

				queryValidate = session.createSQLQuery(hql);

				if (cmpyId.length() > 0) {
					queryValidate.setParameter("cmpy", cmpyId);
					queryValidate.setParameter("venid", venId);
				}

				List<Object[]> listVendorAddrL = queryValidate.list();

				for (Object[] vendorAddr : listVendorAddrL) {
					String temp = commonFunctions.whiteEscapeCharacter(vendorAddr[0].toString().trim());
					addrInfo.setVendorNm(temp);
					if (vendorAddr[1] != null) {
						temp = commonFunctions.whiteEscapeCharacter(vendorAddr[1].toString().trim());
						addrInfo.setPostalCode(temp);
					} else {
						addrInfo.setPostalCode("");
					}

					if (vendorAddr[2] != null) {
						temp = commonFunctions.whiteEscapeCharacter(vendorAddr[2].toString().trim());
						addrInfo.setTownNm(temp);
					} else {
						addrInfo.setTownNm("");
					}

					if (vendorAddr[3] != null) {
						temp = commonFunctions.whiteEscapeCharacter(vendorAddr[3].toString().trim());
						addrInfo.setCtrySubDvsn(temp);
					} else {
						addrInfo.setCtrySubDvsn("");
					}

					if (vendorAddr[4] != null) {
						temp = commonFunctions.whiteEscapeCharacter(vendorAddr[4].toString().trim());
						addrInfo.setCty(temp);
					} else {
						addrInfo.setCty("");
					}

					if (vendorAddr[5] != null) {
						temp = commonFunctions.whiteEscapeCharacter(vendorAddr[5].toString().trim());
						addrInfo.setAddr(temp);
					} else {
						addrInfo.setAddr("");
					}
				}
			}

		} catch (Exception e) {
			logger.debug("ACH Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}

		return addrInfo;

	}

	public CompanyAddress getCompanyAddress(String cmpyId) {
		Session session = null;
		CompanyAddress addrInfo = new CompanyAddress();
		cmpyId="CSC";
		try {

			String hql = "";

			session = SessionUtilInformix.getSession();
			session = SessionUtilInformixESP.getSession();
			/*
			hql = "select cast(csc_co_nm as varchar(35)) as co_nm, cast(csc_nm1 as varchar(35)) as csc_nm1, "
					+ "cast(csc_pcd as varchar(10)) as pcd, cast(csc_city as varchar(35)) as city, "
					+ "cast(csc_st_prov as varchar(2)) as st_prov, cast(csc_cty as varchar(3)) cty, "
					+ "cast(csc_addr1 as varchar(35)) addr1, cast(csc_addr2 as varchar(35)) addr2 from scrcsc_rec "
					+ "where csc_cmpy_id=:cmpy ";
			*/
			hql = "select cast(csc_nm as varchar(35)) as co_nm, cast(csc_nm as varchar(35)) as csc_nm1, "
					+ "cast(csc_pcd as varchar(10)) as pcd, cast(csc_city as varchar(35)) as city, "
					+ "cast(csc_city as varchar(2)) as st_prov, cast(csc_cty as varchar(3)) cty, "
					+ "cast(csc_addr1 as varchar(35)) addr1, cast(csc_addr2 as varchar(35)) addr2 from scrcsc_rec "
					+ "where csc_key_cd=:cmpy ";
			

			Query queryValidate = session.createSQLQuery(hql);

			if (cmpyId.length() > 0) {
				queryValidate.setParameter("cmpy", cmpyId);
			}

			List<Object[]> listCmpyAddr = queryValidate.list();

			for (Object[] cmpyAddr : listCmpyAddr) {
				if (cmpyAddr[0] != null) {
					addrInfo.setCmpyCoNm(cmpyAddr[0].toString());
				}

				if (cmpyAddr[1] != null) {
					addrInfo.setCmpyNm(cmpyAddr[1].toString());
				}

				if (cmpyAddr[2] != null) {
					addrInfo.setPostalCode(cmpyAddr[2].toString());
				}

				if (cmpyAddr[3] != null) {
					addrInfo.setTownNm(cmpyAddr[3].toString());
				}

				if (cmpyAddr[4] != null) {
					addrInfo.setCtrySubDvsn(cmpyAddr[4].toString());
				}

				if (cmpyAddr[5] != null) {
					addrInfo.setCty(cmpyAddr[5].toString());
				}

				if (cmpyAddr[6] != null && cmpyAddr[7] != null) {
					addrInfo.setAddr(cmpyAddr[6].toString() + " " + cmpyAddr[7].toString());
				} else if (cmpyAddr[6] != null && cmpyAddr[7] == null) {
					addrInfo.setAddr(cmpyAddr[6].toString());
				} else if (cmpyAddr[6] == null && cmpyAddr[7] != null) {
					addrInfo.setAddr(cmpyAddr[7].toString());
				}

			}

		} catch (Exception e) {
			logger.debug("ACH Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}

		return addrInfo;

	}

	public List<InvoiceInfo> getPropInvoices(String proposalId) {
		Session session = null;
		String payMthd = "";

		List<InvoiceInfo> invoiceList = new ArrayList<InvoiceInfo>();

		try {

			String hql = "";

			session = SessionUtil.getSession();

			hql = "select mthd_cd,inv_no, vchr_notes from cstm_param_inv ,pay_mthd mthd where inv_sts = true and vchr_pay_mthd = mthd.id and req_id=:prop_id order by mthd.id, inv_no";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("prop_id", proposalId);

			List<Object[]> listVoucher = queryValidate.list();

			for (Object[] vou : listVoucher) {
				if (vou[0] != null) {
					InvoiceInfo info = new InvoiceInfo();

					payMthd = vou[0].toString();

					if (vou[2] != null) {
						info.setNotesToPayee(vou[2].toString());
					} else {
						info.setNotesToPayee("");
					}

					info.setPayMthd(payMthd);

					//getInvoiceDetailsStx(info, vou[1].toString());
					getInvoiceDetailsESP(info, vou[1].toString());

					invoiceList.add(info);
				}
			}

		} catch (Exception e) {
			logger.debug("ACH Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}

		return invoiceList;

	}

}
