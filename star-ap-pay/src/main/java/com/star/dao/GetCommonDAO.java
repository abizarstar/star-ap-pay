package com.star.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.MaintanenceResponse;
import com.star.common.SessionUtil;
import com.star.common.SessionUtilGL;
import com.star.common.SessionUtilInformix;
import com.star.common.SessionUtilInformixESP;
import com.star.linkage.ach.CompanyAddress;
import com.star.linkage.ar.SessionBrowseOutput;
import com.star.linkage.ar.SessionInfo;
import com.star.linkage.common.BankStx;
import com.star.linkage.common.BankStxBrowseOutput;
import com.star.linkage.common.Branch;
import com.star.linkage.common.BranchBrowseOutput;
import com.star.linkage.common.Country;
import com.star.linkage.common.CountryBrowseOutput;
import com.star.linkage.common.Currency;
import com.star.linkage.common.CurrencyBrowseOutput;
import com.star.linkage.common.Customer;
import com.star.linkage.common.CustomerBrowseOutput;
import com.star.linkage.common.ExchangeRtBrowseOutput;
import com.star.linkage.common.PaymentMethod;
import com.star.linkage.common.PaymentMethodBrowseOutput;
import com.star.linkage.common.TransactionStatusAction;
import com.star.linkage.common.TransactionStatusActionBrowseOutput;
import com.star.linkage.common.TransactionStatusReason;
import com.star.linkage.common.TransactionStatusReasonBrowseOutput;
import com.star.linkage.common.Vendor;
import com.star.linkage.common.VendorBrowseOutput;
import com.star.linkage.common.VoucherCategory;
import com.star.linkage.common.VoucherCategoryBrowseOutput;
import com.star.linkage.company.Address;
import com.star.linkage.connection.ConnectionManOutput;

public class GetCommonDAO {

	private static Logger logger = LoggerFactory.getLogger(GetCommonDAO.class);

	public void getCustomerList(BrowseResponse<CustomerBrowseOutput> starBrowseResponse, String cmpyId) {

		Session session = null;
		Transaction tx = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = " select cast(trim(cus_cus_id) as varchar(8)) cus_id, "
					+ "cast(trim(cus_cus_long_nm) as varchar(35)) cus_nm from arrcus_rec where 1=1 ";

			if (cmpyId.length() > 0) {
				hql += " and cus_cmpy_id=:cmpy_id";
			}

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", cmpyId);

			List<Object[]> listCustomer = queryValidate.list();

			for (Object[] customer : listCustomer) {

				Customer output = new Customer();

				output.setCusId(customer[0].toString());
				output.setCusNm(customer[1].toString());

				starBrowseResponse.output.fldTblCustomer.add(output);

			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}

	public void getVendorList(BrowseResponse<VendorBrowseOutput> starBrowseResponse, String cmpyId) {

		Session session = null;
		Transaction tx = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = " select cast(trim(ven_ven_id) as varchar(8)) ven_id, "
					+ "cast(trim(ven_ven_long_nm) as varchar(35)) ven_nm from aprven_rec where 1=1";

			if (cmpyId.length() > 0) {
				hql += " and ven_cmpy_id=:cmpy_id";
			}

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listVendor = queryValidate.list();

			queryValidate.setParameter("cmpy_id", cmpyId);

			for (Object[] vendor : listVendor) {

				Vendor output = new Vendor();

				output.setVenId(vendor[0].toString());
				output.setVenNm(vendor[1].toString());

				starBrowseResponse.output.fldTblVendor.add(output);

			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}
	
	public void getVendorEmailList(BrowseResponse<VendorBrowseOutput> starBrowseResponse, String cmpyId, String venId) {

		Session session = null;
		Transaction tx = null;
		String hql = "";
		
		try {
			session = SessionUtilInformix.getSession();
			//session = SessionUtilGL.getSession();

			hql = " select cast(trim(cva_email) as varchar(50)) ven_email,1 from scrcva_rec where cva_addr_typ='L' and cva_ref_pfx='VN' and cva_cus_ven_typ='V' and cva_addr_no='0' and cva_cus_ven_id =:ven_Id";
			//hql = "select cast(trim(cva_email) as varchar(35)) ven_email,cast(trim(cva_email) as varchar(35)) ven_email2,cast(trim(cva_email) as varchar(35)) ven_email3 from scrcva_rec where cva_cus_ven_id=:ven_Id order by ven_email";
						
			Query queryValidate = session.createSQLQuery(hql);
			
			queryValidate.setParameter("ven_Id", venId);

			List<Object[]> listVendor = queryValidate.list();
			
			for (Object[] vendor : listVendor) {

				Vendor output = new Vendor();
				output.setVenEmail(vendor[0].toString());
				output.setVenId(vendor[0].toString());
				output.setVenNm(vendor[0].toString());
				starBrowseResponse.output.fldTblVendor.add(output);

			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}

	public String getVendorName(String cmpyId, String venId) {

		Session session = null;
		Transaction tx = null;
		String hql = "";
		String venNm = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = " select cast(trim(ven_ven_id) as varchar(8)) ven_id, cast(trim(ven_ven_long_nm) as varchar(35)) ven_nm from aprven_rec ";

			hql = hql + " where ven_cmpy_id =:cmpy and ven_ven_id=:ven";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy", cmpyId);
			queryValidate.setParameter("ven", venId);

			List<Object[]> listVendor = queryValidate.list();

			for (Object[] vendor : listVendor) {

				Vendor output = new Vendor();

				output.setVenId(vendor[0].toString());
				output.setVenNm(vendor[1].toString());

				venNm = vendor[1].toString();
			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return venNm;
	}

	public void getCountryList(BrowseResponse<CountryBrowseOutput> starBrowseResponse) {

		Session session = null;
		Transaction tx = null;
		String hql = "";

		try {
			session = SessionUtilInformixESP.getSession();

			//hql = " select cast(trim(cty_cty) as varchar(3)) cty, cast(trim(cty_desc30) as varchar(30)) cty_desc from scrcty_Rec";
			hql = " select cast((cty_cty) as varchar(3)) cty, cast(trim(cty_desc) as varchar(30)) cty_desc from scrcty_Rec";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listCountry = queryValidate.list();

			for (Object[] country : listCountry) {

				Country output = new Country();

				output.setCty(country[0].toString());
				output.setCtyDesc(country[1].toString());

				starBrowseResponse.output.fldTblCountry.add(output);
			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}

	public void getBranchList(BrowseResponse<BranchBrowseOutput> starBrowseResponse, String cmpyId) {

		Session session = null;
		Transaction tx = null;
		String hql = "";

		try {
			session = SessionUtilInformixESP.getSession();

			//hql = " select cast(trim(brh_brh) as varchar(3)) brh_brh,"
			//		+ "cast(trim(brh_brh_nm) as varchar(15)) brh_brh_nm from scrbrh_rec where 1=1 ";
			
			hql = " select cast(trim(brh_brh) as varchar(3)) brh_brh,"
					+ "cast(trim(brh_nm) as varchar(15)) brh_brh_nm from scrbrh_rec where 1=1 ";


			//if (cmpyId.length() > 0) {
			//	hql += " and brh_cmpy_id=:cmpy_id";
			//}

			Query queryValidate = session.createSQLQuery(hql);

			//queryValidate.setParameter("cmpy_id", cmpyId);

			List<Object[]> listBranch = queryValidate.list();

			for (Object[] branch : listBranch) {

				Branch output = new Branch();

				output.setBrhBrh(branch[0].toString());
				output.setBrhBrhNm(branch[1].toString());

				starBrowseResponse.output.fldTblBranch.add(output);

			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}

	public void getBankList(BrowseResponse<BankStxBrowseOutput> starBrowseResponse, String cmpyId) {
		Session session = null;
		Transaction tx = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = " select cast(trim(bnk_bnk) as varchar(3)) bnk_bnk,cast(trim(bnk_desc30) as varchar(30)) bnk_nm, "
					+ "cast(trim(bnk_cry) as varchar(3)), bnk_actv, (select crx_xexrt from scrcrx_rec, scrcsc_rec "
					+ "where  crx_cmpy_id = csc_cmpy_id and crx_orig_cry=bnk_cry and crx_eqv_cry=csc_bas_cry "
					+ "and crx_cmpy_id =:cmpy) exc_rt from scrbnk_rec";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy", cmpyId);

			List<Object[]> listBank = queryValidate.list();

			for (Object[] bank : listBank) {

				BankStx output = new BankStx();

				output.setBnk(bank[0].toString());
				output.setBnkDesc(bank[1].toString());
				output.setBnkCry(bank[2].toString());
				output.setBnkActv(Integer.parseInt(bank[3].toString()));

				if (bank[4] != null) {
					output.setExcRt(Double.parseDouble(bank[4].toString()));
				} else {
					output.setExcRt(0);
				}

				starBrowseResponse.output.fldTblBank.add(output);

			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}

	public void getExchangeRate(BrowseResponse<ExchangeRtBrowseOutput> starBrowseResponse, String cmpyId,
			String origCry, String eqvCry) {
		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = " select crx_xexrt from scrcrx_rec where crx_cmpy_id =:cmpy and crx_orig_cry=:origCry and crx_eqv_cry =:eqvCry";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy", cmpyId);
			queryValidate.setParameter("origCry", origCry);
			queryValidate.setParameter("eqvCry", eqvCry);

			if (queryValidate.list().size() > 0) {
				starBrowseResponse.output.excRt = String.valueOf(queryValidate.list().get(0));
			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}

	public void getCurrencyList(BrowseResponse<CurrencyBrowseOutput> starBrowseResponse) {

		Session session = null;
		Transaction tx = null;
		String hql = "";

		try {
			session = SessionUtilInformixESP.getSession();

			//hql = " select cast(trim(cry_cry) as varchar(3)) cry,cast(trim(cry_desc15) as varchar(15)) cry_nm from scrcry_rec";
			hql = " select cast(trim(cry_sym) as varchar(3)) cry,cast(trim(cry_desc) as varchar(15)) cry_nm from scrcry_rec";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listCry = queryValidate.list();

			for (Object[] currency : listCry) {

				Currency output = new Currency();

				output.setCry(currency[0].toString());
				output.setCryDesc(currency[1].toString());

				starBrowseResponse.output.fldTblCry.add(output);

			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}

	public void validateCon(MaintanenceResponse<ConnectionManOutput> starManResponse) {

		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = " select first 1 8 from systables";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listCheck = queryValidate.list();

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(CommonConstants.ERP_CON_FAILED);
		} finally {
			session.close();
		}

	}

	public List<Vendor> getVendorList(String VenNm, String cmpyId) {

		Session session = null;
		Transaction tx = null;
		String hql = "";
		List<Vendor> vendorList = new ArrayList<Vendor>();
		try {
			session = SessionUtilInformix.getSession();

			hql = " select cast(trim(ven_ven_id) as varchar(8)) ven_id, cast(trim(ven_ven_long_nm) as varchar(35)) ven_nm "
					+ "from aprven_rec ";

			hql = hql + " where ven_cmpy_id=:cmpy";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy", cmpyId);

			/* queryValidate.setParameter("ven_nm", VenNm + '%'); */// + '%' '%' +

			List<Object[]> listVendor = queryValidate.list();

			for (Object[] vendor : listVendor) {

				Vendor output = new Vendor();

				output.setVenId(vendor[0].toString());
				output.setVenNm(vendor[1].toString());

				vendorList.add(output);

			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return vendorList;
	}

	public void getPaymentMethodList(BrowseResponse<PaymentMethodBrowseOutput> starBrowseResponse) {

		Session session = null;
		Transaction tx = null;
		String hql = "";

		try {
			session = SessionUtil.getSession();

			hql = " SELECT id, mthd_cd, mthd_nm FROM pay_mthd ORDER BY mthd_nm";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listPymtMthd = queryValidate.list();

			for (Object[] pymtMthd : listPymtMthd) {

				PaymentMethod output = new PaymentMethod();

				output.setMthdId(pymtMthd[0].toString());
				output.setMthdCd(pymtMthd[1].toString());
				output.setMthdNm(pymtMthd[2].toString());

				starBrowseResponse.output.fldTblPymtMthd.add(output);
			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}

	public void getVouCatList(BrowseResponse<VoucherCategoryBrowseOutput> starBrowseResponse) {

		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = " select cast(vct_vchr_cat as varchar(4)) vchr_cat, cast(vct_desc as varchar(30)) as desc from aprvct_rec WHERE vct_actv=1 ORDER BY vct_vchr_cat";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listVouCat = queryValidate.list();

			for (Object[] category : listVouCat) {

				VoucherCategory output = new VoucherCategory();

				output.setVchrCat(category[0].toString());
				output.setDesc(category[1].toString());

				starBrowseResponse.output.fldTblVouCategory.add(output);

			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}

	public void getTrnStsActnList(BrowseResponse<TransactionStatusActionBrowseOutput> starBrowseResponse) {

		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = "select cast(stc_sts_actn as varchar(1)) as sts, cast(stc_desc30 as varchar(30)) as sts_desc from tcrstc_rec where stc_sts_typ='T' and stc_aplc_lvl='T'";

			// "SELECT cast(sta_sts as varchar(4)) as sts, cast(sta_desc15 as varchar(16))
			// as desc FROM scrsta_rec WHERE sta_sts_typ='T' AND sta_actv=1 ORDER BY
			// sta_sts";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listTrnStsActn = queryValidate.list();

			for (Object[] trnStsActn : listTrnStsActn) {

				TransactionStatusAction output = new TransactionStatusAction();

				output.setSts(trnStsActn[0].toString());
				output.setDesc(trnStsActn[1].toString());

				starBrowseResponse.output.fldTblTrnStsActn.add(output);

			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}
	}

	public void getTrnStsList(BrowseResponse<TransactionStatusActionBrowseOutput> starBrowseResponse) {

		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = "SELECT cast(sta_sts as varchar(4)) as sts, cast(sta_desc15 as varchar(16)) as desc FROM scrsta_rec WHERE sta_sts_typ='T' AND sta_actv=1 ORDER BY sta_sts";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listTrnStsActn = queryValidate.list();

			for (Object[] trnStsActn : listTrnStsActn) {

				TransactionStatusAction output = new TransactionStatusAction();

				output.setSts(trnStsActn[0].toString());
				output.setDesc(trnStsActn[1].toString());

				starBrowseResponse.output.fldTblTrnStsActn.add(output);

			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}
	}

	public void getTrnStsRsnList(BrowseResponse<TransactionStatusReasonBrowseOutput> starBrowseResponse,
			String stsActn) {

		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = "SELECT cast(rsn_rsn as varchar(4)) as rsn, cast(rsn_desc30 as varchar(31)) as desc "
					+ " FROM scrrsn_rec WHERE rsn_rsn_typ=:typ AND rsn_actv=1 ORDER BY rsn_rsn";

			Query queryValidate = session.createSQLQuery(hql);

			if (stsActn.equals("C")) {
				queryValidate.setParameter("typ", "TRC");
			} else if (stsActn.equals("H")) {
				queryValidate.setParameter("typ", "TRH");
			}

			List<Object[]> listTrnStsRsn = queryValidate.list();

			for (Object[] trnStsRsn : listTrnStsRsn) {

				TransactionStatusReason output = new TransactionStatusReason();

				output.setRsn(trnStsRsn[0].toString());
				output.setDesc(trnStsRsn[1].toString());

				starBrowseResponse.output.fldTblTrnStsRsn.add(output);

			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}
	}

	public void getSessionList(BrowseResponse<SessionBrowseOutput> starBrowseResponse, String cmpyId) {

		Session session = null;
		String hql = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = "SELECT crs_ssn_id, cast(crs_cmpy_id as varchar(4)) as cmpy_id FROM artcrs_rec WHERE crs_cmpy_id=:cmpy_id ORDER BY crs_ssn_id";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy_id", cmpyId);

			List<Object[]> listRows = queryValidate.list();

			for (Object[] row : listRows) {

				SessionInfo output = new SessionInfo();

				output.setSsnId(row[0].toString());

				starBrowseResponse.output.fldTblSsn.add(output);

			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}
	}

	public Address getCompanyAddress(String cmpyId) {

		Session session = null;
		Transaction tx = null;
		String hql = "";

		Address output = new Address();
		try {
			session = SessionUtilInformix.getSession();

			hql = " select cast(csc_addr1 as varchar(35)), cast(csc_addr2 as varchar(35)), "
					+ "cast(csc_addr3 as varchar(35)),cast(csc_city as varchar(35)), "
					+ "cast(csc_pcd as varchar(10)), cast(csc_cty as varchar(3)), "
					+ "cast(csc_st_prov as varchar(2)) from scrcsc_rec where csc_cmpy_id=:cmpy";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy", cmpyId);

			List<Object[]> listCmpy = queryValidate.list();

			for (Object[] cmpy : listCmpy) {

				if (cmpy[0] != null) {
					output.setAddr1(cmpy[0].toString());
				} else {
					output.setAddr1("");
				}

				if (cmpy[1] != null) {
					output.setAddr2(cmpy[1].toString());
				} else {
					output.setAddr2("");
				}
				if (cmpy[2] != null) {
					output.setAddr3(cmpy[2].toString());
				} else {
					output.setAddr3("");
				}
				if (cmpy[3] != null) {
					output.setCity(cmpy[3].toString());
				} else {
					output.setCity("");
				}
				if (cmpy[4] != null) {
					output.setPcd(cmpy[4].toString());
				} else {
					output.setPcd("");
				}
				if (cmpy[5] != null) {
					output.setCty(cmpy[5].toString());
				} else {
					output.setCty("");
				}
				
				if (cmpy[6] != null) {
					output.setStProv(cmpy[6].toString());
				} else {
					output.setStProv("");
				}
			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return output;

	}

	public Address getVendorAddress(String cmpyId, String venId) {

		Session session = null;
		Transaction tx = null;
		String hql = "";

		Address output = new Address();

		try {
			session = SessionUtilInformix.getSession();

			hql = " select cast(cva_addr1 as varchar(35)), cast(cva_addr2 as varchar(35)), "
					+ "cast(cva_addr3 as varchar(35)),cast(cva_city as varchar(35)), cast(cva_pcd as varchar(10)), "
					+ "cast(cva_cty as varchar(3)), cast(cva_st_prov as varchar(2)) from scrcva_rec where "
					+ "cva_cmpy_id=:cmpy and cva_ref_pfx='VN' and cva_cus_ven_typ='V' and cva_cus_ven_id=:ven "
					+ "and cva_addr_no=0 and cva_addr_typ='L'";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("cmpy", cmpyId);
			queryValidate.setParameter("ven", venId);

			List<Object[]> listCmpy = queryValidate.list();

			for (Object[] cmpy : listCmpy) {

				if (cmpy[0] != null) {
					output.setAddr1(cmpy[0].toString());
				} else {
					output.setAddr1("");
				}

				if (cmpy[1] != null) {
					output.setAddr2(cmpy[1].toString());
				} else {
					output.setAddr2("");
				}
				if (cmpy[2] != null) {
					output.setAddr3(cmpy[2].toString());
				} else {
					output.setAddr3("");
				}
				if (cmpy[3] != null) {
					output.setCity(cmpy[3].toString());
				} else {
					output.setCity("");
				}
				if (cmpy[4] != null) {
					output.setPcd(cmpy[4].toString());
				} else {
					output.setPcd("");
				}
				if (cmpy[5] != null) {
					output.setCty(cmpy[5].toString());
				} else {
					output.setCty("");
				}
			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return output;
	}

}
