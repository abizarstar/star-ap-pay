package com.star.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.MaintanenceResponse;
import com.star.common.SessionUtil;
import com.star.linkage.menu.Menu;
import com.star.linkage.menu.MenuBrowseOutput;
import com.star.linkage.menu.MenuInfo;
import com.star.linkage.menu.MenuInput;
import com.star.linkage.menu.MenuListInput;
import com.star.linkage.menu.MenuManOutput;
import com.star.modal.UsrGrpMenu;
import com.star.modal.UsrGrpSubMenu;

public class SuperUserDAO {

	private static Logger logger = LoggerFactory.getLogger(SuperUserDAO.class);

	public void getMenubyGroup(BrowseResponse<MenuBrowseOutput> browseResponse, String grpId) {
		String hql = "";
		Session session = null;

		try {

			session = SessionUtil.getSession();

			hql = " select menu.menu_id, menu_nm, menu_html_pg, menu_icon,is_vw, grp.grp_id, "
					+ "(select grp_nm from usr_group where grp_id=grp.grp_id) grp_nm from usr_grp_menu grp , "
					+ "usr_menu menu where grp.menu_id = menu.menu_id ";

			if (grpId.length() > 0) {
				hql = hql + "and grp_id=:grpid";
			}

			hql = hql + " order by grp.grp_id, menu.menu_id";

			Query queryValidate = session.createSQLQuery(hql);

			if (grpId.length() > 0) {
				queryValidate.setParameter("grpid", Integer.parseInt(grpId));
			}

			List<Object[]> listMenu = queryValidate.list();

			for (Object[] menu : listMenu) {

				MenuInfo info = new MenuInfo();

				if (menu[0] != null) {
					info.setMenuId(menu[0].toString());
				} else {
					info.setMenuId("");
				}

				if (menu[1] != null) {
					info.setMenuNm(menu[1].toString());
				} else {
					info.setMenuNm("");
				}

				if (menu[2] != null) {
					info.setMenuHtml(menu[2].toString());
				} else {
					info.setMenuHtml("");
				}

				if (menu[3] != null) {
					info.setMenuIcon(menu[3].toString());
				} else {
					info.setMenuIcon("");
				}
				if (menu[4] != null) {
					info.setIsVw(menu[4].toString());
				} else {
					info.setIsVw("");
				}

				info.setGrpId(menu[5].toString());
				info.setGrpNm(menu[6].toString());

				List<Menu> subMenuList = getSubMenu(session, menu[5].toString(), info.getMenuId());

				info.setSubMenuList(subMenuList);

				browseResponse.output.fldTblMenu.add(info);
			}

		} catch (Exception e) {

			logger.debug("Menu Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public List<Menu> getSubMenu(Session session, String grpId, String menuId) throws Exception {
		String hql = "";

		List<Menu> subMenuList = new ArrayList<Menu>();

		hql = " select menu.sub_menu_id, menu.sub_menu_nm, sub_menu_html_pg, sub_menu_icon, is_vw from "
				+ "usr_grp_sub_menu sub_user, usr_sub_menu menu where sub_user.sub_menu_id = menu.sub_menu_id "
				+ "and sub_user.menu_id = menu.menu_id " + " and grp_id=:grpid and menu.menu_id=:menuid";

		Query queryValidate = session.createSQLQuery(hql);

		queryValidate.setParameter("grpid", Integer.parseInt(grpId));
		queryValidate.setParameter("menuid", Integer.parseInt(menuId));

		List<Object[]> listMenu = queryValidate.list();

		for (Object[] menu : listMenu) {

			Menu info = new Menu();

			if (menu[0] != null) {
				info.setMenuId(menu[0].toString());
			} else {
				info.setMenuId("");
			}

			if (menu[1] != null) {
				info.setMenuNm(menu[1].toString());
			} else {
				info.setMenuNm("");
			}

			if (menu[2] != null) {
				info.setMenuHtml(menu[2].toString());
			} else {
				info.setMenuHtml("");
			}

			if (menu[3] != null) {
				info.setMenuIcon(menu[3].toString());
			} else {
				info.setMenuIcon("");
			}
			if (menu[4] != null) {
				info.setIsVw(menu[4].toString());
			} else {
				info.setIsVw("");
			}

			subMenuList.add(info);
		}

		return subMenuList;
	}

	public void getMenuAll(BrowseResponse<MenuBrowseOutput> browseResponse, String userId) {
		String hql = "";
		Session session = null;

		try {

			session = SessionUtil.getSession();
			int x = getUsrGrp(session, userId);
			if (x == 1) {

				hql = " select menu_id, menu_nm,is_vw from usr_menu order by menu_seq";

				Query queryValidate = session.createSQLQuery(hql);

				List<Object[]> listMenu = queryValidate.list();

				for (Object[] menu : listMenu) {

					MenuInfo info = new MenuInfo();

					if (menu[0] != null) {
						info.setMenuId(menu[0].toString());
					} else {
						info.setMenuId("");
					}

					if (menu[1] != null) {
						info.setMenuNm(menu[1].toString());
					} else {
						info.setMenuNm("");
					}
					if (menu[2] != null) {
						info.setIsVw(menu[2].toString());
					} else {
						info.setIsVw("");
					}

					List<Menu> subMenuList = getSubMenuAll(session, info.getMenuId());

					info.setSubMenuList(subMenuList);

					browseResponse.output.fldTblMenu.add(info);

				}
			}
		} catch (Exception e) {

			logger.debug("Menu Info : {}", e.getMessage(), e);

		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public List<Menu> getSubMenuAll(Session session, String menuId) throws Exception {
		String hql = "";

		List<Menu> subMenuList = new ArrayList<Menu>();

		hql = " select sub_menu_id, sub_menu_nm, is_vw from usr_sub_menu where menu_id=:menuid";

		Query queryValidate = session.createSQLQuery(hql);

		queryValidate.setParameter("menuid", Integer.parseInt(menuId));

		List<Object[]> listMenu = queryValidate.list();

		for (Object[] menu : listMenu) {

			Menu info = new Menu();

			if (menu[0] != null) {
				info.setMenuId(menu[0].toString());
			} else {
				info.setMenuId("");
			}

			if (menu[1] != null) {
				info.setMenuNm(menu[1].toString());
			} else {
				info.setMenuNm("");
			}
			if (menu[2] != null) {
				info.setIsVw(menu[2].toString());
			} else {
				info.setIsVw("");
			}

			subMenuList.add(info);
		}

		return subMenuList;
	}

	public void addMenuInformation(MaintanenceResponse<MenuManOutput> starManResponse, MenuListInput menuInput,
			String userId) {
		Session session = null;
		Transaction tx = null;
		try {

			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			Integer x = getUsrGrp(session, userId);
			//boolean isVw = true;
			if (x >= 1) {
				/* DELETE INFORMATION IF AVAILABLE */
				// delMenuInformation(session, menuInput.getGrpId(), userId);
	//			delMenuInformation(session, x.toString(), userId);
				String mnid = "";
				String sbmnid = "";
				String mvw="";
				String svw="";
				for (int i = 0; i < menuInput.getMenuId().size(); i++) {	
					mnid=menuInput.getMenuId().get(i).getMenuId();
					mvw=menuInput.getMenuId().get(i).getIsVw();
					setMenuVw(session, mnid, mvw);
					//UsrGrpMenu grpMenu = new UsrGrpMenu();	grpMenu.setGrpId(x); grpMenu.setMenuId(Integer.parseInt(menuInput.getMenuId().get(i).getMenuId()));		session.save(grpMenu);
				}

				for (int i = 0; i < menuInput.getSubMenList().size(); i++) {
					mnid=menuInput.getSubMenList().get(i).getMenuId();
					sbmnid=menuInput.getSubMenList().get(i).getSubMenuId();
					svw=menuInput.getSubMenList().get(i).getIsVw();

					setsbMenuVw(session, mnid,sbmnid,svw);
					//UsrGrpSubMenu grpSubMenu = new UsrGrpSubMenu();	grpSubMenu.setGrpId(x);	grpSubMenu.setMenuId(Integer.parseInt(menuInput.getSubMenList().get(i).getMenuId()));	grpSubMenu.setSubMenuId(Integer.parseInt(menuInput.getSubMenList().get(i).getSubMenuId()));		session.save(grpSubMenu);
				}	
				//	setMenuVw(session, mnid, menuInput.getMenuId().get(i).getIsVw());
					//setsbMenuVw(session, menuInput.getSubMenList().get(i).getMenuId(),menuInput.getSubMenList().get(i).getSubMenuId());

				tx.commit();
			}
		} catch (Exception e) {

			logger.debug("Menu Info : {}", e.getMessage(), e);

			starManResponse.output.messages.add(e.getMessage());
			starManResponse.output.rtnSts = 1;

			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	private void setsbMenuVw(Session session, String menuId, String subMenuId, String vw) {
		// TODO Auto-generated method stub
		String hql = "";
		Boolean isvw = false;
		try {
			int mn = Integer.parseInt(menuId);
			int sbmn = Integer.parseInt(subMenuId);
			if(vw.equals("true")) {isvw=true;}

			hql = " update usr_sub_menu set is_vw=:isvw where 1=1 ";

			if (mn > 0) {
				hql = hql + " AND menu_id =:menuid ";
			}
			if (sbmn > 0) {
				hql = hql + " AND sub_menu_id =:sbmenuid ";
			}

			Query queryValidate = session.createSQLQuery(hql);

			if (mn > 0) {
				queryValidate.setParameter("menuid", mn);
			}
			if (sbmn > 0) {
				queryValidate.setParameter("sbmenuid", sbmn);
			}
			queryValidate.setParameter("isvw", isvw);

			int x = queryValidate.executeUpdate();
		} catch (Exception e) {
			System.out.println("--------- exception ----"+e.getMessage()+"------------");
			}
	}

	private void setMenuVw(Session session, String menuId, String vw) {
		// TODO Auto-generated method stub
		String hql = "";
		Boolean isvw = false;
		int mn = Integer.parseInt(menuId);

		try {
			if (vw.length() > 0) {
				if(vw.equals("true")) {isvw=true;}

			}
			hql = " update usr_menu set is_vw=:isvw where 1=1 ";

			if (mn > 0) {
				hql = hql + " AND menu_id =:menuid ";
			}

			Query queryValidate = session.createSQLQuery(hql);
			if (menuId.length() > 0) {
				queryValidate.setParameter("menuid", Integer.parseInt(menuId));
			}
			if (vw.length() > 0) {
				queryValidate.setParameter("isvw", isvw);
			}
			int x = queryValidate.executeUpdate();
			System.out.println("--------- query ----"+x+"------------");
		} catch (Exception e) {
			System.out.println("--------- exception ----"+e.getMessage()+"------------");
			}
	}

	public void delMenuInformation(Session session, String grpId, String userId) throws Exception {
		String hql = "";

		hql = "delete from usr_grp_menu where grp_id=:grpid";

		Query queryValidate = session.createSQLQuery(hql);
		queryValidate.setParameter("grpid", Integer.parseInt(grpId));

		queryValidate.executeUpdate();

		hql = "delete from usr_grp_sub_menu where grp_id=:grpid";

		queryValidate = session.createSQLQuery(hql);

		queryValidate.setParameter("grpid", Integer.parseInt(grpId));

		queryValidate.executeUpdate();

	}

	public int getUsrGrp(Session session, String userId) {
		// TODO Auto-generated method stub
		String hql = "";
		int grpId = 0;
		try {
			hql = "select usr_grp,1=1 from usr_dtls usr where usr_id=:usrid";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("usrid", userId);

			List<Object[]> usrgrp = queryValidate.list();

			for (Object[] grpid : usrgrp) {

				if (grpid[0] != null) {
					grpId = Integer.parseInt(grpid[0].toString());
				}
			}
		} catch (Exception e) {
			logger.error(e.toString());
		}

		System.out.println("------ " + grpId + " is group id of " + userId + " -------");
		return grpId;

	}

}
