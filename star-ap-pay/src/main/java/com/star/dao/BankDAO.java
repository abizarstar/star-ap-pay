package com.star.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.MaintanenceResponse;
import com.star.common.SessionUtil;
import com.star.linkage.bank.BankAccntInfo;
import com.star.linkage.bank.BankBrowseOutput;
import com.star.linkage.bank.BankInfo;
import com.star.linkage.bank.BankManOutput;
import com.star.linkage.company.CompanyInfo;
import com.star.modal.CstmBnkAcctInfo;
import com.star.modal.CstmBnkInfo;
import com.star.modal.CstmFtpInfo;

public class BankDAO {

	private static Logger logger = LoggerFactory.getLogger(BankDAO.class);
	
	public void addBankDetails(MaintanenceResponse<BankManOutput> starManResponse, BankInfo bankInput) {
		Session session = null;
		Transaction tx = null;

		try {

			session = SessionUtil.getSession();
			tx = session.beginTransaction();
			CstmBnkInfo bnkInfo = new CstmBnkInfo();

			bnkInfo.setBnkBrh(bankInput.getBnkBrh());
			bnkInfo.setBnkCity(bankInput.getBnkCity());
			bnkInfo.setBnkCode(bankInput.getBnkCode());
			bnkInfo.setBnkCry(bankInput.getBnkCry());
			bnkInfo.setBnkCty(bankInput.getBnkCty());
			bnkInfo.setBnkNm(bankInput.getBnkNm());
			bnkInfo.setBnkRoutNo(bankInput.getBnkRoutNo());
			bnkInfo.setBnkSwiftCode(bankInput.getBnkSwiftCode());
			
			session.save(bnkInfo);
			
			delFTPInfo(bankInput, session);

			addBankFTPDetails(starManResponse, bankInput, session);
			
			delAcctInfo(bankInput, session);
			
			addBankAccountDetails(bankInput, session);
			
			tx.commit();
		} catch (Exception e) {

			logger.debug("Bank Information : {}", e.getMessage(), e);
			
			starManResponse.output.messages.add(e.getMessage());
			starManResponse.output.rtnSts = 1;

			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
	
	public void updBankDetails(MaintanenceResponse<BankManOutput> starManResponse, BankInfo bankInput) {
		Session session = null;
		Transaction tx = null;

		try {

			session = SessionUtil.getSession();
			tx = session.beginTransaction();
			CstmBnkInfo bnkInfo = new CstmBnkInfo();

			bnkInfo.setBnkBrh(bankInput.getBnkBrh());
			bnkInfo.setBnkCity(bankInput.getBnkCity());
			bnkInfo.setBnkCode(bankInput.getBnkCode());
			bnkInfo.setBnkCry(bankInput.getBnkCry());
			bnkInfo.setBnkCty(bankInput.getBnkCty());
			bnkInfo.setBnkNm(bankInput.getBnkNm());
			bnkInfo.setBnkRoutNo(bankInput.getBnkRoutNo());
			bnkInfo.setBnkSwiftCode(bankInput.getBnkSwiftCode());
			
			session.update(bnkInfo);
			
			delFTPInfo(bankInput, session);

			addBankFTPDetails(starManResponse, bankInput, session);
			
			delAcctInfo(bankInput, session);
			
			addBankAccountDetails(bankInput, session);
			
			tx.commit();
		} catch (Exception e) {

			logger.debug("Bank Information : {}", e.getMessage(), e);
			starManResponse.output.messages.add(e.getMessage());
			starManResponse.output.rtnSts = 1;

			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void addBankFTPDetails(MaintanenceResponse<BankManOutput> starManResponse, BankInfo bankInput,
			Session session) throws Exception{

		if(bankInput.getBnkHost().length() > 0)
		{
			CstmFtpInfo ftpInfo = new CstmFtpInfo();

			ftpInfo.setBnkCode(bankInput.getBnkCode());
			ftpInfo.setBnkFtpPass(bankInput.getBnkFtpPass());
			ftpInfo.setBnkFtpUser(bankInput.getBnkFtpUser());
			ftpInfo.setBnkHost(bankInput.getBnkHost());
			ftpInfo.setBnkPort(bankInput.getBnkPort());
			ftpInfo.setBnkKeyPath(bankInput.getBnkKeyPath());
			ftpInfo.setBnkFtpLoc(bankInput.getBnkSftpFilePath());
			ftpInfo.setBnkFtpTyp(bankInput.getBnkSftpTyp());

			session.save(ftpInfo);
		}
	}
	
	public void addBankAccountDetails(BankInfo bankInput,
			Session session) throws Exception{

			if(bankInput.getBankAcctList() != null)
			{
				for(int i = 0 ; i < bankInput.getBankAcctList().size() ; i++)
				{
					CstmBnkAcctInfo bnkAcctInfo = new CstmBnkAcctInfo();
					
					bnkAcctInfo.setBnkAcctId(bankInput.getBankAcctList().get(i).getBnkAcct());
					bnkAcctInfo.setBnkAcctDesc(bankInput.getBankAcctList().get(i).getBnkAcctDesc());
					bnkAcctInfo.setBnkAcctNo(bankInput.getBankAcctList().get(i).getBnkAcctNo());
					bnkAcctInfo.setBnkCode(bankInput.getBnkCode());
					bnkAcctInfo.setDflValue(bankInput.getBankAcctList().get(i).isDefault());
					bnkAcctInfo.setBnkAchId(bankInput.getBankAcctList().get(i).getBnkAchId());
					bnkAcctInfo.setBnkMmbId(bankInput.getBankAcctList().get(i).getBnkMmbId());
					
					session.save(bnkAcctInfo);
				}
			}
		}
	
	public void delFTPInfo(BankInfo bankInput, Session session) throws Exception
	{
		String hql = "";
		
		hql = "delete from cstm_ftp_info where bnk_code=:bnkcode";

		Query queryValidate = session.createSQLQuery(hql);
		queryValidate.setParameter("bnkcode", bankInput.getBnkCode());
		queryValidate.executeUpdate();
	}
	
	public void delAcctInfo(BankInfo bankInput, Session session) throws Exception
	{
		String hql = "";
		
		hql = "delete from cstm_bnk_acct_info where bnk_code=:bnkcode";

		Query queryValidate = session.createSQLQuery(hql);
		queryValidate.setParameter("bnkcode", bankInput.getBnkCode());
		queryValidate.executeUpdate();
	}
	
	public int validateBnkCode(String bnkCode) {

		Session session = null;
		String hql = "";
		int ircrdCount = 0;

		try {
			session = SessionUtil.getSession();

			hql = "select count(*) from cstm_bnk_info where bnk_code=:bnkcode";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("bnkcode", bnkCode);

			ircrdCount = Integer.parseInt(String.valueOf(queryValidate.list().get(0)));

		} catch (Exception e) {
			// TODO: handle exception
			logger.debug("Bank Information : {}", e.getMessage(), e);
			
		} finally {
			session.close();
		}

		return ircrdCount;
	}
	
	public void getBankDetails(BrowseResponse<BankBrowseOutput> starManResponse, String bnkCode)
	{
		Session session= null;
		
    	try{
    		    		
    		String hql = "";
    		
    		session = SessionUtil.getSession();
    		
			hql = "select * from cstm_bnk_info where 1=1";
	    	
			if(bnkCode.length() > 0)
			{
				hql = hql + " and bnk_code=:bnkcode";
			}
			
		    Query queryValidate= session.createSQLQuery(hql);
		    
		    if(bnkCode.length() > 0)
			{
		    	queryValidate.setParameter("bnkcode", bnkCode);
			}
		    
			 List<Object[]> listBnk =  queryValidate.list();
			 
			 for (Object[] bnk : listBnk)
		     {
				 BankInfo bankInfo = new BankInfo();
				 
				 bankInfo.setBnkCode(bnk[0].toString());
				 bankInfo.setBnkCty(bnk[1].toString());
				 bankInfo.setBnkRoutNo(bnk[2].toString());
				 bankInfo.setBnkNm(bnk[3].toString());
				 bankInfo.setBnkCity(bnk[4].toString());
				 bankInfo.setBnkBrh(bnk[5].toString());
				 bankInfo.setBnkSwiftCode(bnk[6].toString());
				 bankInfo.setBnkCry(bnk[7].toString());
				 
				 getBankFTPDetails(bankInfo, bankInfo.getBnkCode(), session);
				 
				 CompanyInfo output = new CompanyInfo();
				 
				 output.setBankCode(bankInfo.getBnkCode());
				 
				 getBankAccountDetails(bankInfo, output, session);
				 
				 starManResponse.output.fldTblBank.add(bankInfo);
		     }
		    	   
    	}
    	catch (Exception e) {
    		
    		logger.debug("Bank Information : {}", e.getMessage(), e);
		}
    	finally {
			session.close();
		}
	}
	
	public void getBankList(BrowseResponse<BankBrowseOutput> starManResponse)
	{
		Session session= null;
		
    	try{
    		    		
    		String hql = "";
    		
    		session = SessionUtil.getSession();
    		
			hql = "select * from cstm_bnk_info where 1=1";
	    	
		    Query queryValidate= session.createSQLQuery(hql);
		    		    
			 List<Object[]> listBnk =  queryValidate.list();
			 
			 for (Object[] bnk : listBnk)
		     {
				 BankInfo bankInfo = new BankInfo();
				 
				 bankInfo.setBnkCode(bnk[0].toString());
				 bankInfo.setBnkNm(bnk[3].toString());
				 
				 starManResponse.output.fldTblBank.add(bankInfo);
		     }
		    	   
    	}
    	catch (Exception e) {
    		logger.debug("Bank Information : {}", e.getMessage(), e);
    		
		}
    	finally {
			session.close();
		}
	}
	
	public void getBankFTPDetails(BankInfo bankInfo, String bnkCode, Session session)
	{
		String hql = "";
		
		hql = "select * from cstm_ftp_info where 1=1";
    	
		if(bnkCode.length() > 0)
		{
			hql = hql + " and bnk_code=:bnkcode";
		}
		
	    Query queryValidate= session.createSQLQuery(hql);
	    
	    if(bnkCode.length() > 0)
		{
	    	queryValidate.setParameter("bnkcode", bnkCode);
		}
	    
		 List<Object[]> listBnkFtp =  queryValidate.list();
		 
		 for (Object[] bnkFtp : listBnkFtp)
	     {
			 bankInfo.setBnkHost(bnkFtp[1].toString());
			 bankInfo.setBnkFtpUser(bnkFtp[2].toString());
			 bankInfo.setBnkFtpPass(bnkFtp[3].toString());
			 bankInfo.setBnkKeyPath(bnkFtp[4].toString());
			 bankInfo.setBnkPort(bnkFtp[5].toString());
			 
			 if(bnkFtp[6] != null)
			 {
				 bankInfo.setBnkSftpFilePath(bnkFtp[6].toString());
			 }
			 else
			 {
				 bankInfo.setBnkSftpFilePath("");
			 }
			 
			 if(bnkFtp[7] != null)
			 {
				 bankInfo.setBnkSftpTyp(bnkFtp[7].toString());
			 }
			 else
			 {
				 bankInfo.setBnkSftpTyp("");
			 }
	     }
	}
	
	public void getBankAccountDetails(BankInfo bankInfo, CompanyInfo output, Session session)
	{
		String hql = "";
		
		hql = "select bnk.bnk_code, bnk_acct_id, bnk_acct_desc, bnk_acct_no, is_dflt, bnk_rout_no, bnk_ach_id, bnk_mmb_id from cstm_bnk_acct_info acct, cstm_bnk_info bnk where bnk.bnk_code=acct.bnk_code ";
    	
		if(output.getBankCode().length() > 0)
		{
			hql = hql + " and bnk.bnk_code=:bnkcode";
		}
		
	    Query queryValidate= session.createSQLQuery(hql);
	    
	    if(output.getBankCode().length() > 0)
		{
	    	queryValidate.setParameter("bnkcode", output.getBankCode());
		}
	    
		 List<Object[]> listBnkAcct =  queryValidate.list();
		 
		 List<BankAccntInfo> bankAcctList = new ArrayList<BankAccntInfo>();
		 
		 for (Object[] bnkAcct : listBnkAcct)
	     {
			 BankAccntInfo bankAccntInfo = new BankAccntInfo();
			 
			 bankAccntInfo.setBnkAcct(bnkAcct[1].toString());
			 bankAccntInfo.setBnkAcctDesc(bnkAcct[2].toString());
			 bankAccntInfo.setBnkAcctNo(bnkAcct[3].toString());
			 bankAccntInfo.setDefault((Boolean)bnkAcct[4]);
			 bankAccntInfo.setBnkRoutNo(bnkAcct[5].toString());
			 
			 if(bnkAcct[6] != null)
			 {
				 bankAccntInfo.setBnkAchId(bnkAcct[6].toString());
			 }
			 else
			 {
				 bankAccntInfo.setBnkAchId("");
			 }
			 
			 if(bnkAcct[7] != null)
			 {
				 bankAccntInfo.setBnkMmbId(bnkAcct[7].toString());
			 }
			 else
			 {
				 bankAccntInfo.setBnkMmbId("");
			 }
			 
			 
			 bankAcctList.add(bankAccntInfo);
	     }
		 
		 bankInfo.setBankAcctList(bankAcctList);
	}
	
}
