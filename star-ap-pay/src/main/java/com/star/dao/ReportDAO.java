package com.star.dao;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.CommonFunctions;
import com.star.common.SessionUtil;
import com.star.common.SessionUtilGL;
import com.star.common.SessionUtilInformix;
import com.star.linkage.ap.APDesbCheckInfo;
import com.star.linkage.ar.AREnquiryInfo;
import com.star.linkage.report.LockBoxInfo;
import com.star.linkage.report.LockBoxInforBrowseOutput;

public class ReportDAO {

	private static Logger logger = LoggerFactory.getLogger(ReportDAO.class);

	public List<LockBoxInfo> readLockBoxTrn(BrowseResponse<LockBoxInforBrowseOutput> starBrowseResponse, String frmDpstDt,
			String toDpstDt, String cusId, String sts) throws Exception {

		logger.info("Report Info : {}", new Object() {
		}.getClass().getEnclosingMethod().getName());

		CommonFunctions commonFunctions = new CommonFunctions();
		Session session = null;
		String hql = "";
		String tmpSts = sts;
		List<LockBoxInfo> dataList = new ArrayList<LockBoxInfo>();
		
		if(sts.equals("O") ) {
			tmpSts = "";
		}

		try {
			session = SessionUtil.getSession();

			hql = "select ltd.batch_id, hdr.rcrd_cnt, hdr.batch_tot, chk_no, inv_no, inv_amt, dpst_dt, "
					+ "substring(erp_ref, 0, position('-' in erp_ref)) stx_ssn, "
					+ "substring(erp_ref, position('-' in erp_ref)+1, length(erp_ref)) stx_rcpt "
					+ "from lck_trn_dtls ltd, lck_hdr_dtls hdr where hdr.ebs_upld_id = ltd.ebs_upld_id and "
					+ "ltd.batch_id = hdr.batch_id ";

			if (frmDpstDt.length() > 0) {
				hql = hql + " and dpst_dt >= to_date(:dpst_dt, 'mm/dd/yyyy')";
			}

			if (toDpstDt.length() > 0) {
				hql = hql + " and dpst_dt <= to_date(:dpst_dt_to, 'mm/dd/yyyy')";
			}

			hql = hql + " order by (dpst_dt, ltd.batch_id) asc";

			Query queryValidate = session.createSQLQuery(hql);

			if (frmDpstDt.length() > 0) {
				queryValidate.setParameter("dpst_dt", frmDpstDt);
			}

			if (toDpstDt.length() > 0) {
				queryValidate.setParameter("dpst_dt_to", toDpstDt);
			}

			List<Object[]> rows = queryValidate.list();
			CommonFunctions objCom = new CommonFunctions();
			for (Object[] row : rows) {
				LockBoxInfo output = new LockBoxInfo();

				if(row[3] != null)
				{
					output.setChkNo(row[3].toString());
				}
				else
				{
					output.setChkNo("");
				}
				
				if(row[4] != null)
				{
					output.setInvNo(row[4].toString());
				}
				else
				{
					output.setInvNo("");
				}

				
				if(row[5] != null)
				{
					output.setAmountStr(commonFunctions.formatAmount(Double.parseDouble(row[5].toString())));
				}
				else
				{
					output.setAmountStr("");
				}
				
				if (row[6] != null) {
					output.setDpstDt((objCom.formatDateWithoutTime((Date) row[6])));
				}
				else
				{
					output.setDpstDt("");
				}

				
				if(row[7] != null)
				{
					output.setSsnId(row[7].toString());
				}
				else
				{
					output.setSsnId("");
				}
				
				if(row[8] != null)
				{
					output.setCashRcpt(row[8].toString());
				}
				else
				{
					output.setCashRcpt("");
				}
				
				
				AREnquiryInfo arInfo = null;
				
				if(output.getCashRcpt().length() > 0)
				{
					arInfo = getReceiptInfo(output, output.getCashRcpt());
					
					if(output.getCusId() == null)
					{
						output.setCusId("");
						output.setCusNm("");
						output.setTrnSts("");
					}
					
					getLedgerInfo(output);
				}
				else
				{
					String invNo = output.getInvNo().replaceFirst("^0+(?!$)", "");
					
					arInfo  = getInvoiceInfo(invNo);
					
					if(arInfo.getCusId() != null)
					{
						output.setCusId(arInfo.getCusId());
						output.setCusNm(arInfo.getCusNm());
						output.setTrnSts("");
					}
					else
					{
						output.setCusId("");
						output.setCusNm("");
						output.setTrnSts("");
					}
					
					output.setJeRef("");
					output.setActvyDt("");
					output.setLgnUsr("");
					output.setFrmAcctngPr("");
					output.setToAcctngPr("");
					output.setExtDesc("");
					output.setExtDesc1("");
				}
				
				if( cusId.length() > 0 && sts.length() > 0 ) 
				{					
					if(cusId.equals(output.getCusId()) && tmpSts.equals(output.getTrnSts()))
					{
						starBrowseResponse.output.fldTblLockBox.add(output);
						dataList.add(output);
					}
				}
				else if(cusId.length() > 0)
				{
					if(cusId.equals(output.getCusId()))
					{
						starBrowseResponse.output.fldTblLockBox.add(output);
						dataList.add(output);
					}
				}	
				else if(sts.length() > 0)
				{
					if(tmpSts.equals(output.getTrnSts()))
					{
						starBrowseResponse.output.fldTblLockBox.add(output);
						dataList.add(output);
					}
				}				
				else
				{
					starBrowseResponse.output.fldTblLockBox.add(output);
					dataList.add(output);
				}
				
				
			}

		} catch (Exception e) {

			logger.debug("Report Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(CommonConstants.SERVER_ERR);

			session.close();
		} finally {
			if (session != null) {
				session.close();
			}
		}
		
		return dataList;
	}
	
	public void readCashReceiptInfo(BrowseResponse<LockBoxInforBrowseOutput> starBrowseResponse, String frmDpstDt,
			String toDpstDt) throws Exception {

		logger.info("Report Info : {}", new Object() {
		}.getClass().getEnclosingMethod().getName());

		BankStmtDAO bankStmtDAO = new BankStmtDAO();
		CommonFunctions commonFunctions = new CommonFunctions();
		Session session = null;
		String hql = "";

		try {
			session = SessionUtil.getSession();

			hql = "select ltd.batch_id, hdr.rcrd_cnt, hdr.batch_tot, chk_no, inv_no, inv_amt, dpst_dt, "
					+ "substring(erp_ref, 0, position('-' in erp_ref)) stx_ssn, "
					+ "substring(erp_ref, position('-' in erp_ref)+1, length(erp_ref)) stx_rcpt "
					+ "from lck_trn_dtls ltd, lck_hdr_dtls hdr where hdr.ebs_upld_id = ltd.ebs_upld_id and "
					+ "ltd.batch_id = hdr.batch_id ";

			if (frmDpstDt.length() > 0) {
				hql = hql + " and dpst_dt >= to_date(:dpst_dt, 'mm/dd/yyyy')";
			}

			if (toDpstDt.length() > 0) {
				hql = hql + " and dpst_dt <= to_date(:dpst_dt_to, 'mm/dd/yyyy')";
			}

			hql = hql + " order by (dpst_dt, ltd.batch_id) asc";

			Query queryValidate = session.createSQLQuery(hql);

			if (frmDpstDt.length() > 0) {
				queryValidate.setParameter("dpst_dt", frmDpstDt);
			}

			if (toDpstDt.length() > 0) {
				queryValidate.setParameter("dpst_dt_to", toDpstDt);
			}

			List<Object[]> rows = queryValidate.list();
			CommonFunctions objCom = new CommonFunctions();
			for (Object[] row : rows) {
				LockBoxInfo output = new LockBoxInfo();

				if(row[3] != null)
				{
					output.setChkNo(row[3].toString());
				}
				else
				{
					output.setChkNo("");
				}
				
				if(row[4] != null)
				{
					output.setInvNo(row[4].toString());
				}
				else
				{
					output.setInvNo("");
				}

				
				if(row[5] != null)
				{
					output.setAmountStr(commonFunctions.formatAmount(Double.parseDouble(row[5].toString())));
				}
				else
				{
					output.setAmountStr("");
				}
				
				if (row[6] != null) {
					output.setDpstDt((objCom.formatDateWithoutTime((Date) row[6])));
				}
				else
				{
					output.setDpstDt("");
				}

				
				if(row[7] != null)
				{
					output.setSsnId(row[7].toString());
				}
				else
				{
					output.setSsnId("");
				}
				
				if(row[8] != null)
				{
					output.setCashRcpt(row[8].toString());
				}
				else
				{
					output.setCashRcpt("");
				}
				
				String invNo = output.getInvNo().replaceFirst("^0+(?!$)", "");
				
				AREnquiryInfo arInfo  = getInvoiceInfo(invNo);
				
				output.setCusId(arInfo.getCusId());
				output.setCusNm(arInfo.getCusNm());
				
				starBrowseResponse.output.fldTblLockBox.add(output);
			}

		} catch (Exception e) {

			logger.debug("Report Info : {}", e.getMessage(), e);
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.messages.add(CommonConstants.SERVER_ERR);

			session.close();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
	
	public AREnquiryInfo getInvoiceInfo(String invNo) {
		Session session = null;
		String hql = "";
		CommonFunctions objCom = new CommonFunctions();
		AREnquiryInfo output = new AREnquiryInfo();
		
		invNo = invNo.replaceFirst("^0+(?!$)", "");
		
		try {
			session = SessionUtilInformix.getSession();

			hql = "SELECT CAST(rvh_cmpy_id AS VARCHAR(3)) rvh_cmpy_id, CAST(rvh_ar_pfx AS VARCHAR(2)) rvh_ar_pfx,"
					+ " rvh_ar_no, CAST(rvh_sld_cus_id AS VARCHAR(8)) rvh_sld_cus_id, "
					+ "CAST(rvh_desc30 AS VARCHAR(30)) rvh_desc30, "
					+ "CAST(rvh_cry AS VARCHAR(3)) rvh_cry, rvh_due_dt, "
					+ "CAST(rvh_ar_brh AS VARCHAR(3)) rvh_ar_brh, " + "CAST(rvh_upd_ref AS VARCHAR(22)) rvh_upd_ref, "
					+ "rvh_inv_dt, rvh_orig_amt, rvh_ip_amt, rvh_balamt, rvh_disc_amt, cast(cus_cus_nm as varchar(15)) cus_nm FROM artrvh_rec, arrcus_rec WHERE "
					+ "rvh_cmpy_id = cus_cmpy_id and rvh_sld_cus_id = cus_cus_id and rvh_balamt <> 0 ";

			hql = hql + " and (rvh_rsn_ref=:inv_no OR rvh_upd_ref=:inv_no)";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("inv_no", invNo);

			List<Object[]> listEnquiry = queryValidate.list();

			for (Object[] enquiry : listEnquiry) {
				output = new AREnquiryInfo();

				output.setCmpyId(enquiry[0].toString());
				output.setArPfx(enquiry[1].toString());
				output.setArNo(enquiry[2].toString());
				output.setCusId(enquiry[3].toString());
				output.setDesc(enquiry[4].toString());
				output.setCry(enquiry[5].toString());
				if (enquiry[6] != null) {
					SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
					String formatDate = formatter.format((Date) enquiry[6]);

					output.setDueDt(formatDate);
				}
				output.setBrh(enquiry[7].toString());
				output.setUpdRef(enquiry[8].toString());
				if (enquiry[9] != null) {
					SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
					String formatDate = formatter.format((Date) enquiry[9]);

					output.setInvDt(formatDate);
				}
				output.setOrigAmt(enquiry[10].toString());
				output.setOrigAmtStr(objCom.formatAmount(Double.parseDouble(enquiry[10].toString())));
				output.setIpAmt(enquiry[11].toString());
				output.setIpAmtStr(objCom.formatAmount(Double.parseDouble(enquiry[11].toString())));
				output.setBalamt(enquiry[12].toString());
				output.setBalamtStr(objCom.formatAmount(Double.parseDouble(enquiry[12].toString())));
				output.setDiscAmt(enquiry[13].toString());
				output.setDiscAmtStr(objCom.formatAmount(Double.parseDouble(enquiry[13].toString())));
				output.setCusNm(enquiry[14].toString());

				if (Double.parseDouble(output.getBalamt()) == Double.parseDouble(output.getIpAmt())) {
					output.setProcessFlg(true);
				} else {
					output.setProcessFlg(false);
				}
			}

		} catch (Exception e) {
			logger.debug("Bank Statement Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return output;

	}
	
	public AREnquiryInfo getReceiptInfo(LockBoxInfo lckBoxInfo, String rcptNo) {
		Session session = null;
		String hql = "";
		CommonFunctions objCom = new CommonFunctions();
		AREnquiryInfo output = new AREnquiryInfo();
		
		try {
			session = SessionUtilInformix.getSession();

			hql = "select CAST(cus_cus_id as varchar(8)) cus_id, CAST(cus_cus_nm as varchar(15)) cus_nm, "
					+ " (select tsa_sts_actn from tcttsa_rec where tsa_ref_pfx=csh_crcp_pfx and tsa_ref_no=csh_crcp_no " + 
					" and tsa_ref_itm=0 and tsa_ref_sbitm=0 and tsa_sts_typ='T') sts"
					+ " from artcsh_rec, arrcus_rec where cus_cus_id=csh_cr_ctl_cus_id";

			hql = hql + " and csh_crcp_pfx||'-'||csh_crcp_no = :rcpt_no";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("rcpt_no", rcptNo);

			List<Object[]> listEnquiry = queryValidate.list();

			for (Object[] enquiry : listEnquiry) {
				output = new AREnquiryInfo();

				lckBoxInfo.setCusId(enquiry[0].toString());
				lckBoxInfo.setCusNm(enquiry[1].toString());
				lckBoxInfo.setTrnSts(enquiry[2].toString());
			}

		} catch (Exception e) {
			logger.debug("Bank Statement Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return output;

	}
	
	public void getLedgerInfo(LockBoxInfo lockBoxInfo) {
		Session session = null;
		String hql = "";
		
		int iRecord = 0;
		try {
			session = SessionUtilGL.getSession();

			hql = "select cast(oje_oje_pfx as varchar(2)), oje_oje_no,oje_actvy_dt, "
					+ "cast((select usr_nm from mxrusr_rec where usr_lgn_id=oje_lgn_id) as varchar(35)) usr_nm, "
					+ "oje_fm_acctg_per, oje_to_acctg_per,cast(oje_gl_extl2 as varchar(22)), cast(oje_gl_extl_desc as varchar(35))"
					+ " from gltoje_rec where oje_orig_ref_pfx||'-'||oje_orig_ref_no=:rcpt_no" ;

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("rcpt_no", lockBoxInfo.getCashRcpt());

			List<Object[]> listEnquiry = queryValidate.list();

			for (Object[] jeRefrence : listEnquiry) {
				
				
				lockBoxInfo.setJeRef(jeRefrence[0].toString() + "-" + jeRefrence[1].toString());
				lockBoxInfo.setActvyDt(jeRefrence[2].toString());
				
				lockBoxInfo.setLgnUsr(jeRefrence[3].toString());
				lockBoxInfo.setFrmAcctngPr(jeRefrence[4].toString());
				lockBoxInfo.setToAcctngPr(jeRefrence[5].toString());
				lockBoxInfo.setExtDesc(jeRefrence[6].toString());
				lockBoxInfo.setExtDesc1(jeRefrence[7].toString());
				iRecord = iRecord + 1;
			}
			
			if(iRecord == 0)
			{				
				lockBoxInfo.setJeRef("");
				lockBoxInfo.setActvyDt("");
				lockBoxInfo.setLgnUsr("");
				lockBoxInfo.setFrmAcctngPr("");
				lockBoxInfo.setToAcctngPr("");
				lockBoxInfo.setExtDesc("");
				lockBoxInfo.setExtDesc1("");
			}

		} catch (Exception e) {
			logger.debug("Bank Statement Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}

}
