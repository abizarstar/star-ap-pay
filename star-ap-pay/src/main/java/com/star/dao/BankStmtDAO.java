package com.star.dao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.CommonFunctions;
import com.star.common.MaintanenceResponse;
import com.star.common.SessionUtil;
import com.star.common.SessionUtilGL;
import com.star.common.SessionUtilInformix;
import com.star.linkage.ach.InvoiceInfo;
import com.star.linkage.ap.GLDistributionInfo;
import com.star.linkage.ar.AREnquiryInfo;
import com.star.linkage.cstmdocinfo.VchrInfo;
import com.star.linkage.ebs.APPostingBrowseInfo;
import com.star.linkage.ebs.BankHdrDtls;
import com.star.linkage.ebs.BankStmtBrowseOutput;
import com.star.linkage.ebs.BankStmtDetails;
import com.star.linkage.ebs.BankStmtHdrOutput;
import com.star.linkage.ebs.BankStmtManOutput;
import com.star.linkage.ebs.BankStmtTrnOutput;
import com.star.linkage.ebs.BankTrnDetails;
import com.star.linkage.ebs.InvoicePmntInfo;
import com.star.linkage.ebs.LockBoxDetails;
import com.star.linkage.ebs.LockBoxHdrInfo;
import com.star.linkage.ebs.LockBoxStmtBrowseOutput;
import com.star.linkage.ebs.LockBoxTrnInfo;
import com.star.linkage.gl.GLEntryDataBrwose;
import com.star.linkage.gl.GlEntryData;
import com.star.linkage.gl.LedgerInfo;
import com.star.linkage.rcpt.RcptInfo;
import com.star.linkage.stmt.BnkStmtInput;
import com.star.linkage.stmt.BnkStmtManOutput;
import com.star.modal.CstmParamInv;
import com.star.modal.EbsHdrDtls;
import com.star.modal.EbsTrnDtls;
import com.star.modal.EbsTrnInvDtls;
import com.star.modal.EbsUpldDtls;
import com.star.modal.LckHdrDtls;
import com.star.modal.LckTrnDtls;

public class BankStmtDAO {

	private static Logger logger = LoggerFactory.getLogger(BankStmtDAO.class);

	public int getBnkStmtCount() {
		Session session = null;
		String hql = "";
		int iCount = 0;

		try {
			session = SessionUtil.getSession();

			hql = " select id, 1 from ebs_upld_dtls order by id desc limit 1";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listBnkStmt = queryValidate.list();

			for (Object[] ebs : listBnkStmt) {

				iCount = Integer.parseInt(ebs[0].toString());
			}

		} catch (Exception e) {

			logger.debug("Bank Statement Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}

		return iCount;
	}

	public void addBnkStmtDtls(MaintanenceResponse<BnkStmtManOutput> starManResponse, BnkStmtInput bnkStmtInp,
			BankStmtDetails bankStmtDetails, String usrId, String cmpyId) {
		Session session = null;
		Transaction tx = null;
		Date date = new Date();
		try {

			session = SessionUtil.getSession();
			tx = session.beginTransaction();
			EbsUpldDtls ebsDtls = new EbsUpldDtls();

			ebsDtls.setId(bnkStmtInp.getId());
			ebsDtls.setFlExt(bnkStmtInp.getFlExt());
			ebsDtls.setFlNm(bnkStmtInp.getFlNm());
			ebsDtls.setFlOrgNm(bnkStmtInp.getFlOrgNm());
			ebsDtls.setUpldBy(usrId);
			ebsDtls.setUpldOn(date);
			/*
			 * ebsDtls.setOpnBal(Double.parseDouble(bankStmtDetails.getOpnBal()));
			 * ebsDtls.setClsBal(Double.parseDouble(bankStmtDetails.getCloBal()));
			 * ebsDtls.setAvlblClsBal(Double.parseDouble(bankStmtDetails.getAvlblClsBal()));
			 * ebsDtls.setTotCrdTrn(Double.parseDouble(bankStmtDetails.getTotCreditTrn()));
			 * ebsDtls.setCrdTrnCnt(Integer.parseInt(bankStmtDetails.getCrdTrnCount()));
			 * ebsDtls.setTotDebTrn(Double.parseDouble(bankStmtDetails.getTotDebitTrn()));
			 * ebsDtls.setDebTrnCnt(Integer.parseInt(bankStmtDetails.getDebTrnCount()));
			 */

			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");

			if (bankStmtDetails.getCrtdDt().length() > 0) {
				Date pDate = dateFormat.parse(bankStmtDetails.getCrtdDt() + " " + bankStmtDetails.getCrtdTime());
				ebsDtls.setCrtdDtts(pDate);
			}

			if (bankStmtDetails.getStmtUpldDt().length() > 0) {
				Date pDate = dateFormat
						.parse(bankStmtDetails.getStmtUpldDt() + " " + bankStmtDetails.getStmtUpldTime());
				ebsDtls.setPrsOn(pDate);
			}

			session.save(ebsDtls);

			bankStmtDetails.setUpldId(ebsDtls.getId());

			addHdrDtls(session, bankStmtDetails, ebsDtls.getId(), bankStmtDetails.getCrtdDt(), usrId, cmpyId);

			tx.commit();
		} catch (Exception e) {

			logger.debug("Bank Statement Info : {}", e.getMessage(), e);
			starManResponse.output.messages.add(e.getMessage());
			starManResponse.output.rtnSts = 1;

			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void addHdrDtls(Session session, BankStmtDetails bnkStmtInp, int stmtId, String stmtDt, String usrId, String cmpyId)
			throws Exception {
		int iHdrCount = 0;

		for (int i = 0; i < bnkStmtInp.getBnkHdrList().size(); i++) {
			if (i == 0) {
				iHdrCount = getHdrCount(stmtId);
			}

			iHdrCount = iHdrCount + 1;

			BankHdrDtls bankHdrDtls = bnkStmtInp.getBnkHdrList().get(i);

			EbsHdrDtls ebsHdrDtls = new EbsHdrDtls();
			ebsHdrDtls.setId(iHdrCount);
			ebsHdrDtls.setEbsUpldId(stmtId);
			ebsHdrDtls.setEbsRcvrId(bankHdrDtls.getRcvrId());
			ebsHdrDtls.setOpnBal(Double.parseDouble(bankHdrDtls.getOpnBal()));
			ebsHdrDtls.setClsBal(Double.parseDouble(bankHdrDtls.getCloBal()));
			ebsHdrDtls.setAvlblClsBal(Double.parseDouble(bankHdrDtls.getAvlblClsBal()));
			ebsHdrDtls.setTotCrdTrn(Double.parseDouble(bankHdrDtls.getTotCreditTrn()));
			ebsHdrDtls.setCrdTrnCnt(Integer.parseInt(bankHdrDtls.getCrdTrnCount()));
			ebsHdrDtls.setTotDebTrn(Double.parseDouble(bankHdrDtls.getTotDebitTrn()));
			ebsHdrDtls.setDebTrnCnt(Integer.parseInt(bankHdrDtls.getDebTrnCount()));

			session.save(ebsHdrDtls);

			bankHdrDtls.setHdrId(ebsHdrDtls.getId());

			/* ADD TRANSACTION DETAILS */
			addTrnDtls(session, bankHdrDtls, stmtId, stmtDt, iHdrCount, usrId, cmpyId);

		}
	}

	public void addTrnDtls(Session session, BankHdrDtls bankHdrDtls, int stmtId, String stmtDt, int iHdrId, String usrId,
			String cmpyId) throws Exception {
		int iTrnCount = 0;

		ValidateGLDAO gldao = new ValidateGLDAO();

		for (int i = 0; i < bankHdrDtls.getListBnkTrn().size(); i++) {
			if (i == 0) {
				iTrnCount = getTrnCount(stmtId);
			}
			
			iTrnCount = iTrnCount + 1;

			BankTrnDetails bankTrnDetails = bankHdrDtls.getListBnkTrn().get(i);
			bankTrnDetails.setId(iTrnCount);

			EbsTrnDtls ebsTrnDtls = new EbsTrnDtls();
			ebsTrnDtls.setId(iTrnCount);
			ebsTrnDtls.setEbsHdrId(iHdrId);
			ebsTrnDtls.setEbsUpldId(stmtId);
			ebsTrnDtls.setTrnAmount(Double.parseDouble(bankTrnDetails.getTrnAMount()));
			ebsTrnDtls.setTypCode(bankTrnDetails.getTypeCode());
			ebsTrnDtls.setBnkRef(bankTrnDetails.getBnkRef());
			ebsTrnDtls.setCustRef(bankTrnDetails.getCustRef());
			ebsTrnDtls.setAdtnRef(bankTrnDetails.getAdtnRef());

			if (ebsTrnDtls.getTypCode().equals(CommonConstants.ZBA_CREDIT)
					|| ebsTrnDtls.getTypCode().equals(CommonConstants.LOCKBOX_DEPOSIT)
					|| ebsTrnDtls.getTypCode().equals(CommonConstants.PRE_AUTH_ACH_CREDIT)
					|| ebsTrnDtls.getTypCode().equals(CommonConstants.INCOMING_MONEY_TRFR)) {

				ebsTrnDtls.setTrnTyp("C");
			} else if (ebsTrnDtls.getTypCode().equals(CommonConstants.CHECK_PAID)
					|| ebsTrnDtls.getTypCode().equals(CommonConstants.PRE_AUTH_ACH_DEBIT)
					|| ebsTrnDtls.getTypCode().equals(CommonConstants.OUTGOING_MONEY_TRFR)
					|| ebsTrnDtls.getTypCode().equals(CommonConstants.ZBA_DEBIT)
					|| ebsTrnDtls.getTypCode().equals(CommonConstants.DISBURSING_DEBIT)
					|| ebsTrnDtls.getTypCode().equals(CommonConstants.ACCOUNT_FEE)) {
				ebsTrnDtls.setTrnTyp("D");
			} else {
				ebsTrnDtls.setTrnTyp("");
			}

			ebsTrnDtls.setIsPrs(false);

			session.save(ebsTrnDtls);

			/* CHANGE TO POST TRANSACTION INITIAL ON LOAD*/
			String trnType = gldao.getTrnType(ebsTrnDtls.getTypCode());

			if (trnType.equals("C") || (trnType.equals("D"))) {
				gldao.processTrnOnLoad(ebsTrnDtls.getTypCode(), ebsTrnDtls.getTrnAmount(), ebsTrnDtls.getId(),
						ebsTrnDtls.getEbsHdrId(), ebsTrnDtls.getEbsUpldId(), stmtDt, cmpyId, usrId, "1");
			}

		}

	}

	public int getTrnCount(int stmtId) {
		Session session = null;
		String hql = "";
		int iCount = 0;

		try {
			session = SessionUtil.getSession();

			hql = " select count(*) from ebs_trn_dtls where ebs_upld_id=:id";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("id", stmtId);

			List<Object[]> listBnkStmt = queryValidate.list();

			iCount = Integer.parseInt(String.valueOf(queryValidate.list().get(0)));

		} catch (Exception e) {

			logger.debug("Bank Statement Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}

		return iCount;
	}

	public int getHdrCount(int stmtId) {
		Session session = null;
		String hql = "";
		int iCount = 0;

		try {
			session = SessionUtil.getSession();

			hql = " select count(*) from ebs_hdr_dtls where ebs_upld_id=:id";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("id", stmtId);

			List<Object[]> listBnkStmt = queryValidate.list();

			iCount = Integer.parseInt(String.valueOf(queryValidate.list().get(0)));

		} catch (Exception e) {

			logger.debug("Bank Statement Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}

		return iCount;
	}

	public void updateStmtTrn(String erpRef, String trnId, String hdrId, String upldId, String trnTyp)
			throws Exception {
		Session session = null;
		Transaction tx = null;
		String hql = "";

		String[] refDtls = erpRef.split(",");

		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			if (trnTyp.equals("L")) {
				hql = "update lck_trn_dtls set erp_ref = :erp_ref, err_msg=:err where id=:trn_id and ebs_upld_id=:upld_id";
			} else {
				hql = "update ebs_trn_dtls set erp_ref = :erp_ref, err_msg=:err where id=:trn_id and ebs_hdr_id=:hdr_id and ebs_upld_id=:upld_id";
			}

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("erp_ref", refDtls[0]);
			queryValidate.setParameter("trn_id", Integer.parseInt(trnId));
			if (!trnTyp.equals("L")) {
				queryValidate.setParameter("hdr_id", Integer.parseInt(hdrId));
			}

			queryValidate.setParameter("upld_id", Integer.parseInt(upldId));

			if (refDtls.length >= 2) {
				if (refDtls[1].substring(0, 1).equals("E")) {
					queryValidate.setParameter("err", refDtls[1]);
				} else {
					queryValidate.setParameter("err", "");
				}
			} else {
				queryValidate.setParameter("err", "");
			}

			queryValidate.executeUpdate();

			tx.commit();
		} catch (Exception e) {
			logger.debug("Bank Statement Info : {}", e.getMessage(), e);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updateLedgerRef(String appRef, int trnId, int hdrId, int upldId) throws Exception {
		Session session = null;
		Transaction tx = null;
		String hql = "";

		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			hql = "update ebs_trn_dtls set app_ref = :app_ref where id=:trn_id and ebs_hdr_id=:hdr_id and ebs_upld_id=:upld_id";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("app_ref", appRef);
			queryValidate.setParameter("trn_id", trnId);
			queryValidate.setParameter("hdr_id", hdrId);
			queryValidate.setParameter("upld_id", upldId);

			queryValidate.executeUpdate();

			tx.commit();
		} catch (Exception e) {
			logger.debug("Bank Statement Info : {}", e.getMessage(), e);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void readStmtTrn(BrowseResponse<BankStmtBrowseOutput> starBrowseResponse, String stmtId) {
		Session session = null;
		int iFlg = 0;
		CommonFunctions commonFunctions = new CommonFunctions();
		BankStmtHdrOutput bankStmtHdrOutput = new BankStmtHdrOutput();
		try {

			String hql = "";

			session = SessionUtil.getSession();

			hql = "select typ_cd, typ_nm, trn_amount, trn.trn_typ, is_prs, upld.upld_on, upld.crtd_dtts, hdr.tot_crd_trn, hdr.crd_trn_cnt, hdr.tot_deb_trn, "
					+ "hdr.deb_trn_cnt from ebs_trn_dtls trn, ebs_type_codes code, ebs_hdr_dtls hdr, ebs_upld_dtls upld "
					+ "where trn.typ_code = code.typ_cd and hdr.id = trn.ebs_hdr_id and upld.id = trn.ebs_upld_id";

			if (stmtId.length() > 0) {
				hql = hql + " and trn.ebs_upld_id=:id";
			}

			Query queryValidate = session.createSQLQuery(hql);

			if (stmtId.length() > 0) {
				queryValidate.setParameter("id", Integer.parseInt(stmtId));
			}

			List<Object[]> listRequest = queryValidate.list();

			List<BankStmtTrnOutput> trnOutputsList = new ArrayList<BankStmtTrnOutput>();

			for (Object[] request : listRequest) {

				if (request[5] != null) {
					bankStmtHdrOutput.setUpldOnStr(commonFunctions.formatDate((Date) request[5]));
					bankStmtHdrOutput.setUpldOn((Date) request[5]);
				}

				if (request[6] != null) {
					bankStmtHdrOutput.setCrtdDttsStr(commonFunctions.formatDate((Date) request[6]));
					bankStmtHdrOutput.setCrtdDtts((Date) request[6]);
				}

				bankStmtHdrOutput.setCrdTrnCount(request[7].toString());
				bankStmtHdrOutput.setTotCrdTrn(request[8].toString());
				bankStmtHdrOutput.setTotDebTrn(request[9].toString());
				bankStmtHdrOutput.setDebTrnCount(request[10].toString());

				BankStmtTrnOutput bankStmtTrnOutput = new BankStmtTrnOutput();

				bankStmtTrnOutput.setTypCode(request[0].toString());
				bankStmtTrnOutput.setTypNm(request[1].toString());
				bankStmtTrnOutput.setTrnAmount(request[2].toString());
				bankStmtTrnOutput.setTrnType(request[3].toString());
				bankStmtTrnOutput.setIsPrs(Boolean.parseBoolean(request[4].toString()));

				trnOutputsList.add(bankStmtTrnOutput);

				bankStmtHdrOutput.setListTrn(trnOutputsList);

				iFlg = iFlg + 1;

			}

			starBrowseResponse.output.fldTblBnkTrn.add(bankStmtHdrOutput);

		} catch (Exception e) {
			logger.debug("Bank Statement Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}

	}

	public void readStmtHdr(BrowseResponse<BankStmtBrowseOutput> starBrowseResponse, String stmtId, String upldOnFrm,
			String upldOnTo, String stmtDtFrm, String stmtDtTo, String status, int pageNo) {
		Session session = null;
		int iFlg = 0;
		CommonFunctions commonFunctions = new CommonFunctions();
		BankStmtHdrOutput bankStmtHdrOutput = new BankStmtHdrOutput();
		try {

			String hql = "";

			session = SessionUtil.getSession();

			hql = "select upld.upld_on, upld.crtd_dtts, hdr.tot_crd_trn, hdr.crd_trn_cnt, hdr.tot_deb_trn, "
					+ "hdr.deb_trn_cnt, hdr.id, COALESCE((select bnk_code || ' - ' || bnk_acct_desc from cstm_bnk_acct_info where bnk_acct_no=hdr.ebs_rcvr_id), hdr.ebs_rcvr_id)  rcvr_id, hdr.ebs_upld_id from ebs_hdr_dtls hdr,  "
					+ "ebs_upld_dtls upld where upld.id = hdr.ebs_upld_id";

			if (stmtId.length() > 0) {
				hql = hql + " and hdr.ebs_upld_id=:id";
			}

			if (upldOnFrm.length() > 0) {
				hql = hql + " and to_char(upld.upld_on,'mm/dd/yyyy') >=:upld_frm";
			}

			if (upldOnTo.length() > 0) {
				hql = hql + " and to_char(upld.upld_on,'mm/dd/yyyy') <=:upld_to";
			}

			if (stmtDtFrm.length() > 0) {
				hql = hql + " and to_char(upld.crtd_dtts,'mm/dd/yyyy') >=:stmt_frm";
			}

			if (stmtDtTo.length() > 0) {
				hql = hql + " and to_char(upld.crtd_dtts,'mm/dd/yyyy') <=:stmt_to";
			}

			int skipVal = pageNo * 50;

			hql = hql + " order by upld.crtd_dtts desc offset " + skipVal + " limit " + CommonConstants.DB_OUT_REC;

			Query queryValidate = session.createSQLQuery(hql);

			if (stmtId.length() > 0) {
				queryValidate.setParameter("id", Integer.parseInt(stmtId));
			}

			if (upldOnFrm.length() > 0) {
				queryValidate.setParameter("upld_frm", upldOnFrm);
			}

			if (upldOnTo.length() > 0) {
				queryValidate.setParameter("upld_to", upldOnTo);
			}

			if (stmtDtFrm.length() > 0) {
				queryValidate.setParameter("stmt_frm", stmtDtFrm);
			}

			if (stmtDtTo.length() > 0) {
				queryValidate.setParameter("stmt_to", stmtDtTo);
			}

			List<Object[]> listRequest = queryValidate.list();

			for (Object[] request : listRequest) {

				bankStmtHdrOutput = new BankStmtHdrOutput();

				if (request[5] != null) {
					bankStmtHdrOutput.setUpldOnStr(commonFunctions.formatDate((Date) request[0]));
					bankStmtHdrOutput.setUpldOn((Date) request[0]);
				}

				if (request[6] != null) {
					bankStmtHdrOutput.setCrtdDttsStr(commonFunctions.formatDate((Date) request[1]));
					bankStmtHdrOutput.setCrtdDtts((Date) request[1]);
				}

				bankStmtHdrOutput.setTotCrdTrn(commonFunctions.formatAmount(Double.parseDouble(request[2].toString())));
				bankStmtHdrOutput.setCrdTrnCount(request[3].toString());

				bankStmtHdrOutput.setTotDebTrn(commonFunctions.formatAmount(Double.parseDouble(request[4].toString())));
				bankStmtHdrOutput.setDebTrnCount(request[5].toString());
				bankStmtHdrOutput.setHdrId(Integer.parseInt(request[6].toString()));
				bankStmtHdrOutput.setRcvrId(request[7].toString());
				bankStmtHdrOutput.setUpldId(Integer.parseInt(request[8].toString()));

				List<BankStmtTrnOutput> bnkStmtTRn = readStmtHdrTrn(request[8].toString(), request[6].toString(),
						status);

				bankStmtHdrOutput.setListTrn(bnkStmtTRn);

				iFlg = iFlg + 1;

				starBrowseResponse.output.fldTblBnkTrn.add(bankStmtHdrOutput);
			}

		} catch (Exception e) {

			logger.debug("Bank Statement Info : {}", e.getMessage(), e);
			e.printStackTrace();

		} finally {
			session.close();
		}

	}

	public List<BankStmtTrnOutput> readStmtHdrTrn(String stmtId, String rcvrId, String status) {
		Session session = null;
		int iFlg = 0;

		List<BankStmtTrnOutput> trnOutputsList = new ArrayList<BankStmtTrnOutput>();
		CommonFunctions commonFunctions = new CommonFunctions();

		try {

			String hql = "";

			session = SessionUtil.getSession();

			hql = "select typ_code, typ_nm, trn_amount, trn.trn_typ, bnk_ref, "
					+ "cust_ref, adtn_ref, erp_ref, is_prs, err_msg, trn.id, trn.app_ref, "
					+ "code.trn_typ type_cd_trn_typ, bnk_map.bnk_id, (select gl_ref from ebs_trn_gl_dtls gl "
					+ "where gl.id = trn.id and gl.ebs_hdr_id = trn.ebs_hdr_id 	and gl.ebs_upld_id = trn.ebs_upld_id "
					+ " and gl.typ_code = bnk_map.typ_cd and gl_ref != '') post_1 from "
					+ " ebs_trn_dtls trn,ebs_type_codes code, type_cd_bnk_map bnk_map "
					+ " where trn.typ_code = code.typ_cd and bnk_map.typ_cd = code.typ_cd and trn.del_sts=true";

			if (stmtId.length() > 0) {
				hql = hql + " and trn.ebs_upld_id=:id";
			}

			if (rcvrId.length() > 0) {
				hql = hql + " and trn.ebs_hdr_id=:rcvr_id";
			}

			if (status.equals("PO")) {
				hql = hql + " and erp_ref is not null";
			} else if (status.equals("PE")) {
				hql = hql + " and erp_ref is null";
			}

			hql = hql + " order by erp_ref asc, typ_code asc";

			Query queryValidate = session.createSQLQuery(hql);

			if (stmtId.length() > 0) {
				queryValidate.setParameter("id", Integer.parseInt(stmtId));
			}

			if (rcvrId.length() > 0) {
				queryValidate.setParameter("rcvr_id", Integer.parseInt(rcvrId));
			}

			List<Object[]> listRequest = queryValidate.list();

			for (Object[] request : listRequest) {

				BankStmtTrnOutput bankStmtTrnOutput = new BankStmtTrnOutput();
				String errMsg = "";
				if (request[9] != null) {

					errMsg = request[9].toString();
				}

				/*
				 * if (request[11] != null && request[11].toString().length() > 0) {
				 * 
				 * String erpTrn = getSTXRefInfo(request[11].toString());
				 * 
				 * String[] trnRefVal = erpTrn.split(",");
				 * 
				 * if (request[7] == null) {
				 * 
				 * if (trnRefVal.length > 1) { if (trnRefVal[1].length() >= 1) {
				 * bankStmtTrnOutput.setErpRef(trnRefVal[0]);
				 * bankStmtTrnOutput.setTrnSts(trnRefVal[1].substring(0, 1)); } else {
				 * bankStmtTrnOutput.setTrnSts(""); bankStmtTrnOutput.setErpRef(""); }
				 * 
				 * if (trnRefVal[1].length() > 1) {
				 * bankStmtTrnOutput.setErrMsg(trnRefVal[1].substring(1,
				 * trnRefVal[1].length())); } } else { bankStmtTrnOutput.setTrnSts("");
				 * bankStmtTrnOutput.setErpRef(""); }
				 * 
				 * } else if (request[7].toString().length() == 0) {
				 * 
				 * if (trnRefVal.length > 1 && errMsg.length() == 0) { updateStmtTrn(erpTrn,
				 * request[10].toString(), rcvrId, stmtId, ""); }
				 * 
				 * if (trnRefVal.length > 1) { if (trnRefVal[1].length() >= 1) {
				 * bankStmtTrnOutput.setErpRef(trnRefVal[0]);
				 * bankStmtTrnOutput.setTrnSts(trnRefVal[1].substring(0, 1)); } else {
				 * bankStmtTrnOutput.setTrnSts(""); bankStmtTrnOutput.setErpRef(""); }
				 * 
				 * if (trnRefVal[1].length() > 1) {
				 * bankStmtTrnOutput.setErrMsg(trnRefVal[1].substring(1,
				 * trnRefVal[1].length())); } } else { bankStmtTrnOutput.setTrnSts("");
				 * bankStmtTrnOutput.setErpRef(""); }
				 * 
				 * } else if (request[7] != null) {
				 * 
				 * if (trnRefVal.length > 1) { if (trnRefVal[1].length() >= 1) {
				 * bankStmtTrnOutput.setErpRef(trnRefVal[0]);
				 * bankStmtTrnOutput.setTrnSts(trnRefVal[1].substring(0, 1)); } else {
				 * bankStmtTrnOutput.setErpRef(""); bankStmtTrnOutput.setTrnSts(""); }
				 * 
				 * if (trnRefVal[1].length() > 1) {
				 * bankStmtTrnOutput.setErrMsg(trnRefVal[1].substring(1,
				 * trnRefVal[1].length())); } } else { bankStmtTrnOutput.setTrnSts("");
				 * bankStmtTrnOutput.setErpRef(""); }
				 * 
				 * }
				 * 
				 * bankStmtTrnOutput.setAppRef(request[11].toString()); } else {
				 * bankStmtTrnOutput.setAppRef(""); }
				 */

				bankStmtTrnOutput.setTypCode(request[0].toString());
				bankStmtTrnOutput.setTypNm(request[1].toString());
				bankStmtTrnOutput.setTrnAmount(commonFunctions.formatAmount(Double.parseDouble(request[2].toString())));
				bankStmtTrnOutput.setTrnType(request[3].toString());

				if (request[4] != null) {
					bankStmtTrnOutput.setBnkRef(request[4].toString());
				} else {
					bankStmtTrnOutput.setBnkRef("");
				}

				if (request[5] != null) {
					bankStmtTrnOutput.setCustRef(request[5].toString());
				} else {
					bankStmtTrnOutput.setCustRef("");
				}

				if (request[6] != null) {
					bankStmtTrnOutput.setAdtnRef(request[6].toString());
				} else {
					bankStmtTrnOutput.setAdtnRef("");
				}

				if (request[7] != null) {
					bankStmtTrnOutput.setErpRef(request[7].toString());
					bankStmtTrnOutput.setTrnSts("Y");
				} else {
					bankStmtTrnOutput.setErpRef("");
					bankStmtTrnOutput.setTrnSts("");
				}

				bankStmtTrnOutput.setIsPrs(Boolean.parseBoolean(request[8].toString()));

				if (request[9] != null && request[9].toString().length() > 0) {

					errMsg = request[9].toString();

					if (errMsg.length() >= 1) {
						bankStmtTrnOutput.setTrnSts(errMsg.substring(0, 1));
						bankStmtTrnOutput.setErrMsg(errMsg.substring(1, errMsg.length()));
					} else {
						bankStmtTrnOutput.setErrMsg(errMsg);
					}
				} else {
					bankStmtTrnOutput.setErrMsg("");
				}

				if (request[11] != null) {
					bankStmtTrnOutput.setAppRef(request[11].toString());
				} else {
					bankStmtTrnOutput.setAppRef("");
				}

				bankStmtTrnOutput.setId(request[10].toString());

				if (request[12] != null) {
					bankStmtTrnOutput.setTypCdTrnTyp(request[12].toString());
				} else {
					bankStmtTrnOutput.setTypCdTrnTyp("");
				}

				if (request[13] != null) {
					bankStmtTrnOutput.setBnkCode(request[13].toString());
				} else {
					bankStmtTrnOutput.setBnkCode("");
				}
				
				if (request[14] != null) {
					bankStmtTrnOutput.setPost1Ref(request[14].toString());
				} else {
					bankStmtTrnOutput.setPost1Ref("");
				}

				trnOutputsList.add(bankStmtTrnOutput);

				iFlg = iFlg + 1;

			}

		} catch (Exception e) {
			logger.debug("Bank Statement Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}

		return trnOutputsList;

	}

	public void addLockBoxDtls(MaintanenceResponse<BnkStmtManOutput> starManResponse, BnkStmtInput bnkStmtInp,
			LockBoxDetails lockBoxDtls, String usrId, String cmpyId) {
		Session session = null;
		Transaction tx = null;
		Date date = new Date();
		try {

			session = SessionUtil.getSession();
			tx = session.beginTransaction();
			EbsUpldDtls ebsDtls = new EbsUpldDtls();

			ebsDtls.setId(bnkStmtInp.getId());
			ebsDtls.setFlExt(bnkStmtInp.getFlExt());
			ebsDtls.setFlNm(bnkStmtInp.getFlNm());
			ebsDtls.setFlOrgNm(bnkStmtInp.getFlOrgNm());
			ebsDtls.setUpldBy(usrId);
			ebsDtls.setUpldOn(date);
			/*
			 * ebsDtls.setOpnBal(Double.parseDouble(bankStmtDetails.getOpnBal()));
			 * ebsDtls.setClsBal(Double.parseDouble(bankStmtDetails.getCloBal()));
			 * ebsDtls.setAvlblClsBal(Double.parseDouble(bankStmtDetails.getAvlblClsBal()));
			 * ebsDtls.setTotCrdTrn(Double.parseDouble(bankStmtDetails.getTotCreditTrn()));
			 * ebsDtls.setCrdTrnCnt(Integer.parseInt(bankStmtDetails.getCrdTrnCount()));
			 * ebsDtls.setTotDebTrn(Double.parseDouble(bankStmtDetails.getTotDebitTrn()));
			 * ebsDtls.setDebTrnCnt(Integer.parseInt(bankStmtDetails.getDebTrnCount()));
			 */

			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");

			/*
			 * if (bankStmtDetails.getCrtdDt().length() > 0) { Date pDate =
			 * dateFormat.parse(bankStmtDetails.getCrtdDt() + " " +
			 * bankStmtDetails.getCrtdTime()); ebsDtls.setCrtdDtts(pDate); }
			 */

			session.save(ebsDtls);

			addLckBatchDtls(session, lockBoxDtls, ebsDtls.getId(), usrId, cmpyId);

			tx.commit();
		} catch (Exception e) {

			logger.debug("Bank Statement Info : {}", e.getMessage(), e);
			starManResponse.output.messages.add(e.getMessage());
			starManResponse.output.rtnSts = 1;

			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public int getBatchHdrCount(int stmtId) {
		Session session = null;
		String hql = "";
		int iCount = 0;

		try {
			session = SessionUtil.getSession();

			hql = " select count(*) from lck_hdr_dtls where ebs_upld_id=:id";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("id", stmtId);

			List<Object[]> listBnkStmt = queryValidate.list();

			iCount = Integer.parseInt(String.valueOf(queryValidate.list().get(0)));

		} catch (Exception e) {
			logger.debug("Bank Statement Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}

		return iCount;
	}

	public int getBatchTrnCount(int stmtId) {
		Session session = null;
		String hql = "";
		int iCount = 0;

		try {
			session = SessionUtil.getSession();

			hql = " select count(*) from lck_trn_dtls where ebs_upld_id=:id";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("id", stmtId);

			List<Object[]> listBnkStmt = queryValidate.list();

			iCount = Integer.parseInt(String.valueOf(queryValidate.list().get(0)));

		} catch (Exception e) {
			logger.debug("Bank Statement Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return iCount;
	}

	public void addLckBatchDtls(Session session, LockBoxDetails lockBoxDtls, int stmtId, String usrId, String cmpyId) throws Exception {
		int iHdrCount = 0;
		
		String dpstDt = "";
		for (int i = 0; i < lockBoxDtls.getHdrInfoLst().size(); i++) {
			if (i == 0) {
				iHdrCount = getBatchHdrCount(stmtId);
			}

			iHdrCount = iHdrCount + 1;

			LockBoxHdrInfo lockBoxHdrInfo = lockBoxDtls.getHdrInfoLst().get(i);

			lockBoxHdrInfo.setEbsUpldId(stmtId);
			lockBoxHdrInfo.setId(iHdrCount);

			LckHdrDtls lckBoxHdrDtls = new LckHdrDtls();

			lckBoxHdrDtls.setId(iHdrCount);
			lckBoxHdrDtls.setEbsUpldId(stmtId);
			lckBoxHdrDtls.setBatchId(lockBoxHdrInfo.getBatchId());
			lckBoxHdrDtls.setBatchTot(lockBoxHdrInfo.getBatchTot());
			lckBoxHdrDtls.setLckboxNo(lockBoxHdrInfo.getLockBoxNo());
			lckBoxHdrDtls.setRcrdCnt(lockBoxHdrInfo.getRcrdCount());

			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm");

			if (lockBoxHdrInfo.getDpstDt().length() > 0) {
				Date pDate = dateFormat.parse(lockBoxHdrInfo.getDpstDt() + " 00:00");
				lckBoxHdrDtls.setDpstDt(pDate);
			}
			
			dpstDt = lockBoxHdrInfo.getDpstDt();

			session.save(lckBoxHdrDtls);

			/* ADD TRANSACTION DETAILS */
		}

		addBatchTrnDtls(session, lockBoxDtls, stmtId, iHdrCount, dpstDt, usrId, cmpyId);

	}

	public void addBatchTrnDtls(Session session, LockBoxDetails lockBoxDtls, int stmtId, int iHdrId, String stmtDt, String usrId, String cmpyId) throws Exception {
		int iTrnCount = 0;
		ValidateGLDAO gldao = new ValidateGLDAO();
		
		for (int i = 0; i < lockBoxDtls.getTrnInfoLst().size(); i++) {
			if (i == 0) {
				iTrnCount = getBatchTrnCount(stmtId);
			}

			iTrnCount = iTrnCount + 1;

			LockBoxTrnInfo lckBoxTrnDetails = lockBoxDtls.getTrnInfoLst().get(i);

			lckBoxTrnDetails.setId(iTrnCount);
			lckBoxTrnDetails.setEbsUpldId(stmtId);

			LckTrnDtls lckTrnDtls = new LckTrnDtls();

			lckTrnDtls.setId(iTrnCount);
			lckTrnDtls.setEbsUpldId(stmtId);
			lckTrnDtls.setBatchId(lckBoxTrnDetails.getBatchId());
			lckTrnDtls.setTrnId(lckBoxTrnDetails.getSeqNo());
			lckTrnDtls.setChkNo(lckBoxTrnDetails.getChkNo());
			lckTrnDtls.setInvAmt(lckBoxTrnDetails.getAmount());
			lckTrnDtls.setInvNo(lckBoxTrnDetails.getInvNo());

			lckTrnDtls.setIsPrs(false);

			session.save(lckTrnDtls);
			
			/* CHANGE TO POST TRANSACTION INITIAL ON LOAD*/
			String trnType = gldao.getTrnType("LBX"); /* Lockbox Code*/

			if (trnType.equals("C")) {
				gldao.processTrnOnLoad("LBX", Double.parseDouble(lckTrnDtls.getInvAmt()), lckTrnDtls.getId(),
						Integer.parseInt(lckTrnDtls.getBatchId()), lckTrnDtls.getEbsUpldId(), stmtDt, cmpyId, usrId, "2");
			}
			
		}

	}

	public void readStmtLockboxHdr(BrowseResponse<LockBoxStmtBrowseOutput> starBrowseResponse, String stmtId,
			String upldOnFrm, String upldOnTo) {
		Session session = null;
		CommonFunctions commonFunctions = new CommonFunctions();
		LockBoxHdrInfo lckBxHdrInfo = new LockBoxHdrInfo();

		int iCount = 0;

		try {

			String hql = "";

			session = SessionUtil.getSession();

			hql = "select upld.upld_on, upld.crtd_dtts, hdr.id, hdr.ebs_upld_id, hdr.batch_id, "
					+ "hdr.dpst_dt, hdr.rcrd_cnt, hdr.batch_tot from lck_hdr_dtls hdr,"
					+ " ebs_upld_dtls upld where upld.id = hdr.ebs_upld_id";

			if (stmtId.length() > 0) {
				hql = hql + " and hdr.ebs_upld_id=:id";
			}

			if (upldOnFrm.length() > 0) {
				hql = hql + " and to_char(upld.upld_on,'mm/dd/yyyy') >=:upld_frm";
			}

			if (upldOnTo.length() > 0) {
				hql = hql + " and to_char(upld.upld_on,'mm/dd/yyyy') <=:upld_to";
			}

			hql = hql + " order by hdr.dpst_dt desc";

			Query queryValidate = session.createSQLQuery(hql);

			if (stmtId.length() > 0) {
				queryValidate.setParameter("id", Integer.parseInt(stmtId));
			}

			if (upldOnFrm.length() > 0) {
				queryValidate.setParameter("upld_frm", upldOnFrm);
			}

			if (upldOnTo.length() > 0) {
				queryValidate.setParameter("upld_to", upldOnTo);
			}

			List<Object[]> listRequest = queryValidate.list();

			for (Object[] request : listRequest) {

				lckBxHdrInfo = new LockBoxHdrInfo();

				if (request[0] != null) {
					lckBxHdrInfo.setUpldOnStr(commonFunctions.formatDate((Date) request[0]));
				}

				if (request[1] != null) {
					lckBxHdrInfo.setCrtdDttsStr(commonFunctions.formatDate((Date) request[1]));
				}

				lckBxHdrInfo.setId(Integer.parseInt(request[2].toString()));
				lckBxHdrInfo.setEbsUpldId(Integer.parseInt(request[3].toString()));
				lckBxHdrInfo.setBatchId(request[4].toString());

				if (request[5] != null) {
					lckBxHdrInfo.setDpstDt(commonFunctions.formatDate((Date) request[5]));
				}

				lckBxHdrInfo.setRcrdCount(Integer.parseInt(request[6].toString()));
				lckBxHdrInfo.setBatchTot(Double.parseDouble(request[7].toString()));
				lckBxHdrInfo.setBatchTotStr(commonFunctions.formatAmount(Double.parseDouble(request[7].toString())));
				List<LockBoxTrnInfo> lockBoxTrnList = readStmtLockboxTrn(String.valueOf(lckBxHdrInfo.getEbsUpldId()),
						lckBxHdrInfo.getBatchId());

				lckBxHdrInfo.setListTrn(lockBoxTrnList);

				starBrowseResponse.output.fldTblLckBxHdr.add(lckBxHdrInfo);

				iCount = iCount + 1;
			}

		} catch (Exception e) {
			logger.debug("Bank Statement Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

	}

	public List<LockBoxTrnInfo> readStmtLockboxTrn(String stmtId, String batchId) {
		Session session = null;
		List<LockBoxTrnInfo> lckBxTrnInfoList = new ArrayList<LockBoxTrnInfo>();
		CommonFunctions commonFunctions = new CommonFunctions();
		try {

			String hql = "";

			session = SessionUtil.getSession();

			hql = "select id, batch_id, trn_id, lck_acct_no, chk_no, inv_no, "
					+ "inv_amt, erp_ref, is_prs, err_msg, (select bnk_id from type_cd_bnk_map where typ_cd='LCK') bnk_nm,"
					+ "( select gl_ref from ebs_trn_gl_dtls gl where gl.id = trn.id and gl.ebs_hdr_id = cast(trn.batch_id as int) "
					+ "and gl.ebs_upld_id = trn.ebs_upld_id and gl.typ_code = 'LBX' and gl_ref != '') post_1 from "
					+ "lck_trn_dtls trn where 1=1 and del_sts=true";

			if (stmtId.length() > 0) {
				hql = hql + " and trn.ebs_upld_id=:id";
			}

			if (batchId.length() > 0) {
				hql = hql + " and trn.batch_id=:batch_id";
			}

			hql = hql + " order by erp_ref, trn_id";

			Query queryValidate = session.createSQLQuery(hql);

			if (stmtId.length() > 0) {
				queryValidate.setParameter("id", Integer.parseInt(stmtId));
			}

			if (batchId.length() > 0) {
				queryValidate.setParameter("batch_id", batchId);
			}

			List<Object[]> listRequest = queryValidate.list();

			for (Object[] request : listRequest) {

				LockBoxTrnInfo lckBxTrnInfo = new LockBoxTrnInfo();

				lckBxTrnInfo.setId(Integer.parseInt(request[0].toString()));
				lckBxTrnInfo.setBatchId(request[1].toString());
				lckBxTrnInfo.setSeqNo(request[2].toString());

				if (request[3] != null) {
					lckBxTrnInfo.setAcctNo(request[3].toString().trim());
				} else {
					lckBxTrnInfo.setAcctNo("");
				}

				if (request[4] != null) {
					lckBxTrnInfo.setChkNo(request[4].toString().trim());
				} else {
					lckBxTrnInfo.setChkNo("");
				}

				if (request[5] != null) {
					lckBxTrnInfo.setInvNo(request[5].toString().trim());
				} else {
					lckBxTrnInfo.setInvNo("");
				}

				if (request[6] != null) {
					lckBxTrnInfo.setAmount(request[6].toString().trim());
					lckBxTrnInfo.setAmountStr(commonFunctions.formatAmount(Double.parseDouble(request[6].toString())));

				} else {
					lckBxTrnInfo.setAmount("");
				}

				if (request[7] != null) {
					lckBxTrnInfo.setErpRef(request[7].toString().trim());
				} else {
					lckBxTrnInfo.setErpRef("");
				}

				lckBxTrnInfo.setPrs(Boolean.parseBoolean(request[8].toString()));

				if (request[9] != null) {
					lckBxTrnInfo.setErrMsg(request[9].toString());
				} else {
					lckBxTrnInfo.setErrMsg("");
				}

				if (request[10] != null) {
					lckBxTrnInfo.setBnkCode(request[10].toString());
				} else {
					lckBxTrnInfo.setBnkCode("");
				}
				
				if (request[11] != null) {
					lckBxTrnInfo.setPost1Ref(request[11].toString());
				} else {
					lckBxTrnInfo.setPost1Ref("");
				}

				if (lckBxTrnInfo.getInvNo().length() > 0) {
					AREnquiryInfo info = getInvoiceInfo(lckBxTrnInfo.getInvNo(), lckBxTrnInfo.getErpRef());

					if (info.getCusId() != null) {
						lckBxTrnInfo.setCustomer(info.getCusId() + " (" + info.getCusNm() + ")");
					} else {
						lckBxTrnInfo.setCustomer("");
					}
				} else {
					lckBxTrnInfo.setCustomer("");
				}

				lckBxTrnInfo.setTrnSts("");

				lckBxTrnInfoList.add(lckBxTrnInfo);
			}

		} catch (Exception e) {
			logger.debug("Bank Statement Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}

		return lckBxTrnInfoList;

	}

	public AREnquiryInfo getInvoiceInfo(String invNo, String rcptNo) {
		Session session = null;
		String hql = "";
		CommonFunctions objCom = new CommonFunctions();
		AREnquiryInfo output = new AREnquiryInfo();

		invNo = invNo.replaceFirst("^0+(?!$)", "");

		try {
			session = SessionUtilInformix.getSession();

			hql = "SELECT CAST(rvh_cmpy_id AS VARCHAR(3)) rvh_cmpy_id, CAST(rvh_ar_pfx AS VARCHAR(2)) rvh_ar_pfx,"
					+ " rvh_ar_no, CAST(rvh_sld_cus_id AS VARCHAR(8)) rvh_sld_cus_id, "
					+ "CAST(rvh_desc30 AS VARCHAR(30)) rvh_desc30, "
					+ "CAST(rvh_cry AS VARCHAR(3)) rvh_cry, rvh_due_dt, "
					+ "CAST(rvh_ar_brh AS VARCHAR(3)) rvh_ar_brh, " + "CAST(rvh_upd_ref AS VARCHAR(22)) rvh_upd_ref, "
					+ "rvh_inv_dt, rvh_orig_amt, rvh_ip_amt, rvh_balamt, rvh_disc_amt, cast(cus_cus_nm as varchar(15)) cus_nm FROM artrvh_rec, arrcus_rec WHERE "
					+ "rvh_cmpy_id = cus_cmpy_id and rvh_sld_cus_id = cus_cus_id and rvh_balamt <> 0 ";

			hql = hql + " and (rvh_rsn_ref=:inv_no OR rvh_upd_ref=:inv_no)";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("inv_no", invNo);

			List<Object[]> listEnquiry = queryValidate.list();

			for (Object[] enquiry : listEnquiry) {
				output = new AREnquiryInfo();

				output.setCmpyId(enquiry[0].toString());
				output.setArPfx(enquiry[1].toString());
				output.setArNo(enquiry[2].toString());
				output.setCusId(enquiry[3].toString());
				output.setDesc(enquiry[4].toString());
				output.setCry(enquiry[5].toString());
				if (enquiry[6] != null) {
					SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
					String formatDate = formatter.format((Date) enquiry[6]);

					output.setDueDt(formatDate);
				}
				output.setBrh(enquiry[7].toString());
				output.setUpdRef(enquiry[8].toString());
				if (enquiry[9] != null) {
					SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yy");
					String formatDate = formatter.format((Date) enquiry[9]);

					output.setInvDt(formatDate);
				}
				output.setOrigAmt(enquiry[10].toString());
				output.setOrigAmtStr(objCom.formatAmount(Double.parseDouble(enquiry[10].toString())));
				output.setIpAmt(enquiry[11].toString());
				output.setIpAmtStr(objCom.formatAmount(Double.parseDouble(enquiry[11].toString())));
				output.setBalamt(enquiry[12].toString());
				output.setBalamtStr(objCom.formatAmount(Double.parseDouble(enquiry[12].toString())));
				output.setDiscAmt(enquiry[13].toString());
				output.setDiscAmtStr(objCom.formatAmount(Double.parseDouble(enquiry[13].toString())));
				output.setCusNm(enquiry[14].toString());

				if (Double.parseDouble(output.getBalamt()) == Double.parseDouble(output.getIpAmt())) {
					output.setProcessFlg(true);
				} else {
					output.setProcessFlg(false);
				}
			}

			if (listEnquiry.size() == 0 && rcptNo.length() > 0) {
				String rcptArr[] = rcptNo.split("-");

				if (rcptArr.length > 2) {
					String receiptPfx = rcptArr[1];
					String receiptNo = rcptArr[2];

					hql = "select cast(csh_cmpy_id as varchar(3)) cmpy_id, cast(csh_cr_ctl_cus_id as varchar(8)) cus_id, cast(cus_cus_nm as varchar(15)) cus_nm"
							+ " from artcsh_rec, arrcus_rec "
							+ "where csh_cmpy_id = cus_cmpy_id and csh_cr_ctl_cus_id = cus_cus_id and "
							+ "csh_crcp_pfx=:rcpt_pfx and csh_crcp_no =:rcpt_no ";

					queryValidate = session.createSQLQuery(hql);

					queryValidate.setParameter("rcpt_pfx", receiptPfx);
					queryValidate.setParameter("rcpt_no", Integer.parseInt(receiptNo));

					List<Object[]> listRcptInfo = queryValidate.list();

					for (Object[] rcptInfo : listRcptInfo) {

						output = new AREnquiryInfo();

						output.setCmpyId(rcptInfo[0].toString());
						output.setCusId(rcptInfo[1].toString());
						output.setCusNm(rcptInfo[2].toString());
						output.setArPfx("");
						output.setArNo("");
						output.setDesc("");
						output.setCry("");
						output.setBrh("");
						output.setUpdRef("");
						output.setInvDt("");
						output.setOrigAmtStr("");
						output.setIpAmtStr("");
						output.setBalamtStr("");
						output.setDiscAmtStr("");

						output.setProcessFlg(false);
					}
				}
			}

		} catch (Exception e) {
			logger.debug("Bank Statement Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return output;

	}

	public String getSTXRefInfo(String appRef) {
		Session session = null;
		String hql = "";
		String trnRef = "";
		String sts = "";
		String errMsg = "";

		try {
			session = SessionUtilGL.getSession();

			hql = "select oje_oje_pfx || '-' ||oje_oje_no from gltoje_rec where oje_gl_extl_id1=:ext_id";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("ext_id", appRef);

			if (queryValidate.list().size() > 0) {
				trnRef = String.valueOf(queryValidate.list().get(0));
			} else {
				trnRef = "";
			}

			hql = "select itn_sts_cd from sctitn_rec where itn_gat_ctl_no= (select gje_gat_ctl_no from gligje_rec where gje_gl_extl_id1=:ext_id) and itn_trs_cat='OJE'";

			queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("ext_id", appRef);

			if (queryValidate.list().size() > 0) {
				sts = String.valueOf(queryValidate.list().get(0));
			} else {
				sts = "";
			}

			if (sts.equals("E")) {
				hql = "select msg_msg_var from sctmsg_rec, sctslg_rec where "
						+ "slg_ssn_log_ctl_no = (select itn_ssn_log_ctl_no from sctitn_rec "
						+ "where itn_gat_ctl_no= (select gje_gat_ctl_no from gligje_rec "
						+ " where gje_gl_extl_id1=:ext_id) and itn_trs_cat='OJE') "
						+ "and msg_pgm_nm='glzgje' and slg_ssn_log_dtts = msg_msg_dtts " + "and slg_extl_ctl_no <>0";

				queryValidate = session.createSQLQuery(hql);

				queryValidate.setParameter("ext_id", appRef);

				if (queryValidate.list().size() > 0) {
					errMsg = String.valueOf(queryValidate.list().get(0));
				} else {
					errMsg = "";
				}
			}
		} catch (Exception e) {
			logger.debug("Bank Statement Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		trnRef = trnRef + "," + sts + errMsg;

		return trnRef;

	}

	/* ADD LEDGER INFORMATION AGAINST A TRANSACTION */
	public void addLedgerDetails(MaintanenceResponse<BankStmtManOutput> starManResponse, int upldId, int hdrId,
			int trnId, List<GLDistributionInfo> glDistInfoList, String cmpyId, String usrId, String rmk, String typCd,
			String narrDesc, String postDt) {

		GLInfoDAO dao = new GLInfoDAO();

		Session session = null;
		Transaction tx = null;
		String hql = "";

		DateFormat formatter;
		Date dateobj = null;
		formatter = new SimpleDateFormat("MM/dd/yyyy");
		try {
			dateobj = formatter.parse(postDt);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		/*
		 * Date dateobj = new Date(); System.out.println(dfYM.format(dateobj));
		 */

		LedgerInfo ledgerInfo = dao.getLedgerId(cmpyId);

		int ictlNo = dao.getCtlNo();

		int iInsCtlNo = dao.getInsCtlNo();

		String acctPer = dao.getAccountingPeriod(postDt);

		/* GET FINANCIAL COMPANY */
		String finCoId = dao.getFinCoGateway();

		String trnRef = "LE-" + dao.getGLTransactionSeq();

		cmpyId = dao.getGLCompany(cmpyId);/* TEMPRORY RECORD FOR TESTING */

		String typCodeNm = "";
		if (typCd.length() > 0) {
			EBSTypeCodeDAO codeDAO = new EBSTypeCodeDAO();
			typCodeNm = codeDAO.readTypeCodeName(typCd);
		}

		try {
			session = SessionUtilGL.getSession();
			tx = session.beginTransaction();

			hql = "insert into gligje_rec (gje_src_co_id, gje_gat_ctl_no, gje_lgr_id, gje_jrnl_ent_typ, "
					+ "gje_acctg_per, gje_actvy_dt, gje_lgn_id, gje_src_jrnl, "
					+ "gje_nar_desc, gje_trs_src, gje_auto_rls_pstg, gje_orig_ref_pfx, "
					+ "gje_orig_ref_no, gje_orig_ref_itm, gje_orig_ref_sbitm, gje_orig_cry, "
					+ "gje_orig_exrt, gje_gl_extl_id1, gje_gl_extl_id2, gje_gl_extl1, gje_gl_extl2, "
					+ "gje_gl_extl_desc) values (:co_id, :ctl_no, :lgr_id, 'N', :actng_prd, :actvy_dt, "
					+ ":usr_id, 'OTH', :narr_desc, 'E', 0, '', 0, 0, 0, :cry, 1, :extl_id, '', '', '', :ext_desc)";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("co_id", finCoId);
			queryValidate.setParameter("ctl_no", ictlNo);
			queryValidate.setParameter("lgr_id", ledgerInfo.getLgrId());
			queryValidate.setParameter("usr_id", usrId);
			queryValidate.setParameter("cry", ledgerInfo.getCry());
			queryValidate.setParameter("actng_prd", Integer.parseInt(acctPer));
			queryValidate.setParameter("actvy_dt", dateobj);
			queryValidate.setParameter("ext_desc", typCd + "-" + typCodeNm);
			queryValidate.setParameter("extl_id", trnRef);
			queryValidate.setParameter("narr_desc", narrDesc);

			queryValidate.executeUpdate();

			addLedgerAmount(session, glDistInfoList, ictlNo, finCoId);

			dao.addSctitn(session, ictlNo, finCoId, cmpyId);

			dao.addSytasy(session, ictlNo, iInsCtlNo, finCoId, cmpyId, usrId);

			tx.commit();
			// updateTrnSts(trnRef, Integer.toString(trnId), Integer.toString(hdrId),
			// Integer.toString(upldId));

			boolean flgErr = false;
			String errMsg = "";
			String erpGLRef = "";

			Date d1 = new Date();

			while (true) {

				Date d2 = new Date();

				long seconds = (d2.getTime() - d1.getTime()) / 1000;

				if (seconds > CommonConstants.VLD_CHK_DUR) {
					flgErr = true;
					errMsg = "Unable to process the records via Gateway <br> Please try after some time.";
					tx = session.beginTransaction();
					GLInfoDAO glInfoDAO = new GLInfoDAO();
					glInfoDAO.delSytasy(session, ictlNo);
					glInfoDAO.delSctitn(session, finCoId, ictlNo, cmpyId);
					glInfoDAO.delLedgerItems(session, finCoId, ictlNo);
					glInfoDAO.delLedgerMain(session, finCoId, ictlNo);
					tx.commit();
					break;
				}

				String refVal = getSTXRefInfo(trnRef);

				String[] trnRefVal = refVal.split(",");

				if (trnRefVal.length > 0) {
					if (trnRefVal[1].substring(0, 1).equals("Y")) {

						if (trnRefVal[0].length() > 0) {
							updateStmtTrn(trnRefVal[0], String.valueOf(trnId), String.valueOf(hdrId),
									String.valueOf(upldId), "S");
							erpGLRef = trnRefVal[0];
							break;
						}

					} else {
						if (trnRefVal[1].substring(0, 1).equals("E")) {
							errMsg = trnRefVal[1];
							flgErr = true;
							break;
						}
					}
				}
			}

			if (flgErr == false) {

				tx = session.beginTransaction();

				BankStmtDAO bankStmtDAO = new BankStmtDAO();

				if (typCd.equalsIgnoreCase("LCK")) {
					bankStmtDAO.updateStmtTrn(erpGLRef, Integer.toString(trnId), Integer.toString(hdrId),
							Integer.toString(upldId), "L");
				} else {
					bankStmtDAO.updateLedgerRef(trnRef, trnId, hdrId, upldId);
				}

				starManResponse.output.glEntry = erpGLRef;

				tx.commit();
			} else {

				starManResponse.output.messages.add(errMsg);

				starManResponse.output.glEntry = "";

				starManResponse.output.rtnSts = 1;

			}

		} catch (Exception e) {
			logger.debug("GL Info : {}", e.getMessage(), e);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void addLedgerAmount(Session session, List<GLDistributionInfo> glDistInfoList, int ictlNo, String finCoId)
			throws Exception {

		GLInfoDAO glInfoDAO = new GLInfoDAO();

		String hql = "";

		String subAcct = "";

		for (int i = 0; i < glDistInfoList.size(); i++) {

			/*
			 * if (Double.parseDouble(glDistInfoList.get(i).getCrAmt()) > 0) { hql =
			 * "insert into gligjd_rec (gjd_src_co_id, gjd_gat_ctl_no, gjd_gat_seq_no, gjd_bsc_gl_acct, "
			 * + "gjd_inbd_sacct, gjd_dr_amt, gjd_cr_amt, gjd_dist_rmk) values " +
			 * "(:co_id,:ctl_no, :s_no, :gl_acct, :sub_acct, :dr_amt, :cr_amt, '')"; } else
			 * if (Double.parseDouble(glDistInfoList.get(i).getDrAmt()) > 0) { hql =
			 * "insert into gligjd_rec (gjd_src_co_id, gjd_gat_ctl_no, gjd_gat_seq_no, gjd_bsc_gl_acct, "
			 * + "gjd_inbd_sacct, gjd_dr_amt, gjd_cr_amt, gjd_dist_rmk) values " +
			 * "(:co_id,:ctl_no, :s_no, :gl_acct, :sub_acct, :dr_amt, :cr_amt, '')"; }
			 */

			hql = "insert into gligjd_rec (gjd_src_co_id, gjd_gat_ctl_no, gjd_gat_seq_no, gjd_bsc_gl_acct, "
					+ "gjd_inbd_sacct, gjd_dr_amt, gjd_cr_amt, gjd_dist_rmk) values "
					+ "(:co_id,:ctl_no, :s_no, :gl_acct, :sub_acct, :dr_amt, :cr_amt, :rmk)";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("co_id", finCoId);
			queryValidate.setParameter("ctl_no", ictlNo);
			queryValidate.setParameter("s_no", i + 1);
			queryValidate.setParameter("gl_acct", glDistInfoList.get(i).getGlAcct());

			subAcct = glDistInfoList.get(i).getSacct();

			subAcct = glInfoDAO.getSubAcctFmtd(subAcct);

			queryValidate.setParameter("sub_acct", subAcct);

			/*
			 * if(glDistInfoList.get(i).getCrAmt().length() > 0) {
			 * queryValidate.setParameter("cr_amt",
			 * Double.parseDouble(glDistInfoList.get(i).getCrAmt())); } else {
			 * queryValidate.setParameter("cr_amt", 0); }
			 * 
			 * if(glDistInfoList.get(i).getDrAmt().length() > 0) {
			 * queryValidate.setParameter("dr_amt",
			 * Double.parseDouble(glDistInfoList.get(i).getDrAmt())); } else {
			 * queryValidate.setParameter("dr_amt", 0); }
			 */

			queryValidate.setParameter("cr_amt", Double.parseDouble(glDistInfoList.get(i).getCrAmt()));
			queryValidate.setParameter("dr_amt", Double.parseDouble(glDistInfoList.get(i).getDrAmt()));
			queryValidate.setParameter("rmk", glDistInfoList.get(i).getRemark());

			queryValidate.executeUpdate();
		}

	}

	public void addInvoiceTrn(RcptInfo rcptInfo, String erpRef) {
		Session session = null;
		Transaction tx = null;

		try {

			session = SessionUtil.getSession();
			tx = session.beginTransaction();
			EbsTrnInvDtls ebsDtls = new EbsTrnInvDtls();

			ebsDtls.setErpRef(erpRef);
			ebsDtls.setInvNo(rcptInfo.getInvNo());
			ebsDtls.setBrh(rcptInfo.getBrh());
			ebsDtls.setCusId(rcptInfo.getCusNo());
			ebsDtls.setDiscAmt(Double.parseDouble(rcptInfo.getDiscAmt()));
			ebsDtls.setAmt(Double.parseDouble(rcptInfo.getAmount()));
			ebsDtls.setDueDt(rcptInfo.getDueDt());
			ebsDtls.setUpdRef(rcptInfo.getUpdRef());

			session.save(ebsDtls);
			tx.commit();
		} catch (Exception e) {

			logger.debug("Bank Statement Info : {}", e.getMessage(), e);

			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void addVoucherTrn(VchrInfo vchrInfo, String erpRef) {
		Session session = null;
		Transaction tx = null;

		try {

			session = SessionUtil.getSession();
			tx = session.beginTransaction();
			EbsTrnInvDtls ebsDtls = new EbsTrnInvDtls();

			ebsDtls.setErpRef(erpRef);
			ebsDtls.setInvNo(vchrInfo.getVchrPfx() + "-" + vchrInfo.getVchrNo());
			ebsDtls.setBrh(vchrInfo.getVchrBrh());
			ebsDtls.setCusId(vchrInfo.getVchrVenId());
			ebsDtls.setDiscAmt(vchrInfo.getDiscAmt());
			ebsDtls.setAmt(vchrInfo.getVchrAmt());
			ebsDtls.setDueDt(vchrInfo.getVchrDueDtStr());
			ebsDtls.setUpdRef(vchrInfo.getVchrInvNo());

			session.save(ebsDtls);
			tx.commit();
		} catch (Exception e) {

			logger.debug("Bank Statement Info : {}", e.getMessage(), e);

			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void updateTrnSts(String trnRef, String trnId, String hdrId, String upldId) {
		new Thread(() -> {
			try {

				int iCount = 0;

				while (true) {
					iCount = iCount + 1;
					String erpTrn = getSTXRefInfo(trnRef);

					if (erpTrn.length() > 0) {
						updateStmtTrn(erpTrn, trnId, hdrId, upldId, "");
						break;
					}

					if (iCount > 50) {
						break;
					}
				}

			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}).start();
	}

	public int getBankTrnExist(String stmtDt, String trnType) {
		Session session = null;
		String hql = "";
		int iCount = 0;

		try {
			session = SessionUtil.getSession();

			if (trnType.equals("LCK")) {
				hql = " select count(*) from ebs_upld_dtls where fl_nm like 'Lockbox%' and to_char(prs_on, 'MM/dd/yyyy') = :stmt_dt";
			} else {
				hql = " select count(*) from ebs_upld_dtls where fl_nm like 'BAI%' and to_char(prs_on, 'MM/dd/yyyy') = :stmt_dt";
			}

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("stmt_dt", stmtDt);

			List<Object[]> listBnkStmt = queryValidate.list();

			iCount = Integer.parseInt(String.valueOf(queryValidate.list().get(0)));

		} catch (Exception e) {

			logger.debug("Bank Statement Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}

		return iCount;
	}

	public int getBankTrnExistStmtDate(String stmtDt, String trnType) {
		Session session = null;
		String hql = "";
		int iCount = 0;

		try {
			session = SessionUtil.getSession();

			if (trnType.equals("LCK")) {
				hql = " select count(*) from lck_hdr_dtls where to_char(dpst_dt, 'yyyy-MM-dd') = :stmt_dt";
			} else {
				hql = " select count(*) from ebs_upld_dtls where fl_nm like 'BAI%' and to_char(crtd_dtts, 'yyyy-MM-dd') = :stmt_dt";
			}

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("stmt_dt", stmtDt);

			List<Object[]> listBnkStmt = queryValidate.list();

			iCount = Integer.parseInt(String.valueOf(queryValidate.list().get(0)));

		} catch (Exception e) {

			logger.debug("Bank Statement Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}

		return iCount;
	}

	public void getGLTransaction(BrowseResponse<GLEntryDataBrwose> starBrowseResponse, String trnRef) {
		Session session = null;
		int iFlg = 0;
		GLInfoDAO glInfoDAO = new GLInfoDAO();
		try {

			String hql = "";

			session = SessionUtilGL.getSession();

			hql = "select cast(ojd_bsc_gl_acct as varchar(8)) ojd_bsc_gl_acct, cast(ojd_lgr_id as varchar(3)) ojd_lgr_id , "
					+ "cast(ojd_sacct as varchar(30)) sub_acct, ojd_dr_amt,"
					+ " ojd_cr_amt, cast(ojd_dist_rmk as varchar(50)) dist_rmk, cast(bga_desc30 as varchar(30)) acct_desc, "
					+ " to_char(oje_actvy_dt, 'MM/dd/yyyy') actvy_dt, cast(oje_nar_desc as varchar(50)) as nar_desc from gltoje_rec, gltojd_Rec, glrbga_rec "
					+ "where ojd_bsc_gl_acct = bga_bsc_gl_acct and ojd_oje_pfx = oje_oje_pfx and ojd_oje_no = oje_oje_no "
					+ "and ojd_oje_pfx || '-' || ojd_oje_no = :int_ref order by ojd_dist_ln_no asc";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("int_ref", trnRef);

			List<Object[]> listRequest = queryValidate.list();

			for (Object[] request : listRequest) {

				GlEntryData data = new GlEntryData();

				data.setBscGLAcct(request[0].toString());
				data.setLdgrId(request[1].toString());
				if (request[2] != null) {
					if (request[2].toString().matches("^[0]+$")) {
						data.setBscGLSubAcct("");
					} else {

						String subAcctVal = glInfoDAO.getSubAcctClnt(request[2].toString());
						data.setBscGLSubAcct(subAcctVal);
					}

				} else {
					data.setBscGLSubAcct("");
				}

				data.setDrAmt(Double.parseDouble(request[3].toString()));
				data.setCrAmt(Double.parseDouble(request[4].toString()));

				if (request[5] != null) {
					data.setRmk(request[5].toString());
				} else {
					data.setRmk("");
				}

				data.setBscGlAcctDesc(request[6].toString());

				starBrowseResponse.output.entryDt = request[7].toString();
				if (request[8] != null) {
					starBrowseResponse.output.narrDesc = request[8].toString();
				} else {
					starBrowseResponse.output.narrDesc = "";
				}

				starBrowseResponse.output.fldTblGLEntry.add(data);
			}

		} catch (Exception e) {
			logger.debug("Bank Statement Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}

	}

	/* GET ALREADY POSTED TRANSACTION DETAILS */
	public int getJournalEntryTrs(BrowseResponse<APPostingBrowseInfo> starBrowseResponse, String refNo) {
		Session session = null;
		String hql = "";
		int iCount = 0;

		EBSTransactionDAO dao = new EBSTransactionDAO();

		List<InvoicePmntInfo> invsListApproved = new ArrayList<InvoicePmntInfo>();
		invsListApproved = dao.getApprovedVoucher("A", "", "");

		try {
			session = SessionUtil.getSession();

			hql = " select inv_no as vchr_no, brh, cus_id, amount, disc_amt, upd_ref as inv_no, due_dt from ebs_trn_inv_dtls where 1=1";

			if (refNo.length() > 0) {
				hql = hql + " and erp_ref=:ref";
			}

			Query queryValidate = session.createSQLQuery(hql);

			if (refNo.length() > 0) {
				queryValidate.setParameter("ref", refNo);
			}

			List<Object[]> listTrn = queryValidate.list();

			for (Object[] prsTrn : listTrn) {

				VchrInfo info = new VchrInfo();

				info.setVchrNo(prsTrn[0].toString());
				info.setVchrBrh(prsTrn[1].toString());
				info.setVchrVenId(prsTrn[2].toString());
				info.setVchrAmtStr(prsTrn[3].toString());
				info.setDiscAmtStr(prsTrn[4].toString());
				info.setVchrInvNo(prsTrn[5].toString());
				info.setVchrDueDtStr(prsTrn[6].toString());

				int iFlg = 0;
				for (int i = 0; i < invsListApproved.size(); i++) {
					if (invsListApproved.get(i).getInvNo().equals(info.getVchrNo())) {
						info.setChkNo(invsListApproved.get(i).getChkNo());
						iFlg = 1;
					}
				}

				if (iFlg == 0) {
					info.setChkNo("");
				}

				starBrowseResponse.output.fldTblVchrInfo.add(info);

			}

		} catch (Exception e) {

			logger.debug("Bank Statement Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}

		return iCount;
	}

	/* GET ALREADY POSTED TRANSACTION DETAILS */
	public int getJournalEntryTrsAccount(BrowseResponse<APPostingBrowseInfo> starBrowseResponse, String refNo) {
		Session session = null;
		String hql = "";
		int iCount = 0;
		GLInfoDAO dao = new GLInfoDAO();

		try {
			session = SessionUtilGL.getSession();

			hql = " select cast(ojd_bsc_gl_acct as varchar(8)) as gl_acct, cast(ojd_sacct as varchar(30)) sacct, "
					+ "ojd_dr_amt, ojd_cr_amt, cast(ojd_dist_rmk as varchar(50)) dist_rmk, "
					+ "cast(bga_desc30 as varchar(30)) acct_desc, to_char(oje_actvy_dt, 'MM/dd/yyyy') actvy_dt, "
					+ "cast(oje_nar_desc as varchar(50)) narr_desc from "
					+ "gltoje_rec, gltojd_rec, glrbga_rec where ojd_oje_pfx = oje_oje_pfx and ojd_oje_no = oje_oje_no "
					+ "and ojd_bsc_gl_acct = bga_bsc_gl_acct and ojd_oje_pfx||'-'|| ojd_oje_no = :ref";

			Query queryValidate = session.createSQLQuery(hql);

			if (refNo.length() > 0) {
				queryValidate.setParameter("ref", refNo);
			}

			List<Object[]> listTrnJL = queryValidate.list();

			for (Object[] jlEntry : listTrnJL) {

				GLDistributionInfo distributionInfo = new GLDistributionInfo();

				distributionInfo.setGlAcct(jlEntry[0].toString());
				distributionInfo.setSacct(jlEntry[1].toString());
				distributionInfo.setDesc(jlEntry[5].toString());
				distributionInfo.setDrAmt(jlEntry[2].toString());
				distributionInfo.setCrAmt(jlEntry[3].toString());

				if (jlEntry[4] != null) {
					distributionInfo.setRemark(jlEntry[4].toString());
				} else {
					distributionInfo.setRemark("");
				}

				if (jlEntry[1].toString().matches("^[0]+$")) {
					distributionInfo.setSacct("");
				} else {

					String subAcct = dao.getSubAcctClnt(jlEntry[1].toString());

					distributionInfo.setSacct(subAcct);
				}

				starBrowseResponse.output.entryDt = jlEntry[6].toString();

				if (jlEntry[7] != null) {
					starBrowseResponse.output.narrDesc = jlEntry[7].toString();
				} else {
					starBrowseResponse.output.narrDesc = "";
				}
				starBrowseResponse.output.fldTblGlDist.add(distributionInfo);

			}

		} catch (Exception e) {

			logger.debug("Bank Statement Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}

		return iCount;
	}

	public String getTypeCodeBank(String typCd) {
		Session session = null;
		String hql = "";
		String bnkCode = "";

		try {
			session = SessionUtil.getSession();

			hql = " select bnk_id from type_cd_bnk_map where typ_cd=:typ";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("typ", typCd);

			if (queryValidate.list().size() > 0) {
				bnkCode = String.valueOf(queryValidate.list().get(0));
			}

		} catch (Exception e) {

			logger.debug("Bank Statement Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}

		return bnkCode;
	}

	public void updateTrnStatus(String trnTyp, String trnId, String hdrId, String upldId) {
		Session session = null;
		Transaction tx = null;
		String hql = "";

		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			if (trnTyp.equals("L")) {
				hql = "update lck_trn_dtls set del_sts = false where id=:trn_id and ebs_upld_id=:upld_id";
			} else {
				hql = "update ebs_trn_dtls set del_sts = false where id=:trn_id and ebs_hdr_id=:hdr_id and ebs_upld_id=:upld_id";
			}

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("trn_id", Integer.parseInt(trnId));
			if (!trnTyp.equals("L")) {
				queryValidate.setParameter("hdr_id", Integer.parseInt(hdrId));
			}

			queryValidate.setParameter("upld_id", Integer.parseInt(upldId));
			queryValidate.executeUpdate();

			tx.commit();
		} catch (Exception e) {
			logger.debug("Bank Statement Info : {}", e.getMessage(), e);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
