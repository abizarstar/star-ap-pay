package com.star.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.SessionUtil;
import com.star.linkage.bank.BankInfo;

public class GetStmtSchDAO {
	
	private static Logger logger = LoggerFactory.getLogger(GetStmtSchDAO.class);

	public List<BankInfo> getFTPLocation(String locTyp) {
		Session session = null;
		String hql = "";
		List<BankInfo> bankInfos = new ArrayList<BankInfo>();

		try {
			session = SessionUtil.getSession();

			hql = "select * from cstm_ftp_info where 1=1 and bnk_ftp_typ = :typ and is_active=true";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("typ", locTyp);

			List<Object[]> listBnkFtp = queryValidate.list();

			for (Object[] bnkFtp : listBnkFtp) {
				BankInfo info = new BankInfo();

				info.setBnkHost(bnkFtp[1].toString());
				info.setBnkFtpUser(bnkFtp[2].toString());
				info.setBnkFtpPass(bnkFtp[3].toString());
				if (bnkFtp[4] != null) {
					info.setBnkKeyPath(bnkFtp[4].toString());
				} else {
					info.setBnkKeyPath("");
				}
				info.setBnkPort(bnkFtp[5].toString());

				if (bnkFtp[6] != null) {
					info.setBnkSftpFilePath(bnkFtp[6].toString());
				} else {
					info.setBnkSftpFilePath("");
				}

				if (bnkFtp[7] != null) {
					info.setBnkSftpTyp(bnkFtp[7].toString());
				} else {
					info.setBnkSftpTyp("");
				}

				bankInfos.add(info);

			}
		} catch (Exception e) {

			logger.debug("Payment Info : {}", e.getMessage(), e);
		} finally {
			if (session != null) {
				session.close();
			}
		}

		return bankInfos;
	}
}
