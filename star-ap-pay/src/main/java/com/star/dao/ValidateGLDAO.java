package com.star.dao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.CommonFunctions;
import com.star.common.MaintanenceResponse;
import com.star.common.SessionUtil;
import com.star.common.SessionUtilGL;
import com.star.common.SessionUtilInformix;
import com.star.linkage.ach.InvoiceInfo;
import com.star.linkage.ach.InvoiceInfoBrowse;
import com.star.linkage.ap.GLDistributionInfo;
import com.star.linkage.ebs.BankStmtHdrOutput;
import com.star.linkage.ebs.BankStmtManOutput;
import com.star.linkage.ebs.BankStmtTrnBrowse;
import com.star.linkage.ebs.BankStmtTrnOutput;
import com.star.linkage.ebstypecode.EBSGLAccountInfo;
import com.star.linkage.gl.JournalInfo;
import com.star.linkage.gl.LedgerInfo;
import com.star.linkage.invoice.InvoiceInfoBrowseOutput;
import com.star.linkage.payment.PaymentInfo;
import com.star.linkage.payment.PaymentManOutput;
import com.star.modal.CstmPayParams;
import com.star.modal.EbsTrnGlDtls;

public class ValidateGLDAO {

	private static Logger logger = LoggerFactory.getLogger(ValidateGLDAO.class);

	public void readStmtTrn(BrowseResponse<BankStmtTrnBrowse> starBrowseResponse, String stmtDt, String trnType,String stmtType) {
		Session session = null;
		int iFlg = 0;
		CommonFunctions commonFunctions = new CommonFunctions();
		BankStmtHdrOutput bankStmtHdrOutput = new BankStmtHdrOutput();
		try {

			String hql = "";

			session = SessionUtil.getSession();
			
			if(stmtType.equals("EBS"))
			{
				hql = "select 	trn.id, trn.ebs_hdr_id , trn.ebs_upld_id, typ_code, typ_nm, trn_amount, bnk_ref, "
					+ "cust_ref, adtn_ref, erp_ref, is_prs, err_msg, trn.app_ref, code.trn_typ type_cd_trn_typ, "
					+ "bnk_map.bnk_id, 	upld.crtd_dtts,( 	select 		gl_ref 	from 		ebs_trn_gl_dtls gl 	where 	"
					+ "gl.id = trn.id 	and gl.ebs_hdr_id = trn.ebs_hdr_id 	and gl.ebs_upld_id = trn.ebs_upld_id 	"
					+ "and gl.typ_code = bnk_map.typ_cd and gl_ref != '') post_1, "
					+ "( select gl_ref_upd 	from ebs_trn_gl_dtls gl where 	gl.id = trn.id 	and gl.ebs_hdr_id = trn.ebs_hdr_id 	and gl.ebs_upld_id = trn.ebs_upld_id 	"
					+ "and gl.typ_code = bnk_map.typ_cd and gl_ref_upd != '') post_2 from ebs_trn_dtls trn, ebs_type_codes code, "
					+ "type_cd_bnk_map bnk_map, 	ebs_upld_dtls upld where 	upld.id = trn.ebs_upld_id "
					+ "and trn.typ_code = code.typ_cd 	and bnk_map.typ_cd = code.typ_cd and trn.del_sts = true ";
			}
			else if(stmtType.equals("LCK"))
			{
				hql = "select trn.id, trn.batch_id, trn.ebs_upld_id, 'LBX' typ_code, 'Lockbox Transaction' typ_nm, "
						+ "trn.inv_amt, trn.chk_no bnk_ref,trn.inv_no cust_ref,'' adtn_ref, erp_ref, is_prs, err_msg, '' app_ref,'' "
						+ "type_cd_trn_typ,'' bnk_id, hdr.dpst_dt crtd_dtts, "
						+ "( select gl_ref from ebs_trn_gl_dtls gl where gl.id = trn.id and gl.ebs_hdr_id = cast(trn.batch_id as int) and gl.ebs_upld_id = trn.ebs_upld_id and gl.typ_code = 'LBX' and gl_ref != '') post_1, "
						+ "( select gl_ref_upd from ebs_trn_gl_dtls gl where gl.id = trn.id and gl.ebs_hdr_id = cast(trn.batch_id as int) and gl.ebs_upld_id = trn.ebs_upld_id and gl.typ_code = 'LBX' and gl_ref_upd != '') post_2 "
						+ "from lck_trn_dtls trn, lck_hdr_dtls hdr where hdr.ebs_upld_id = trn.ebs_upld_id and "
						+ "hdr.batch_id = trn.batch_id and trn.del_sts = true"; 
			}

			if(stmtType.equals("EBS"))
			{
				if (trnType.length() > 0) {
					hql = hql + " and code.trn_typ=:trn_typ";
				}
				
				if (stmtDt.length() > 0) {
					hql = hql + " and to_char(upld.crtd_dtts,'mm/dd/yyyy') =:stmt_dt";
				}
				
				hql = hql + " order by upld.crtd_dtts asc";
			}
			else
			{
				if (stmtDt.length() > 0) {
					hql = hql + " and to_char(hdr.dpst_dt,'mm/dd/yyyy') =:stmt_dt";
				}
				
				hql = hql + " order by hdr.dpst_dt asc";
			}

			
			Query queryValidate = session.createSQLQuery(hql);

			if (stmtDt.length() > 0) {
				queryValidate.setParameter("stmt_dt", stmtDt);
			}
			
			if(stmtType.equals("EBS"))
			{
				if (trnType.length() > 0) {
					queryValidate.setParameter("trn_typ", trnType);
				}
			}

			List<Object[]> listRequest = queryValidate.list();

			for (Object[] request : listRequest) {

				BankStmtTrnOutput bankStmtTrnOutput = new BankStmtTrnOutput();
				String errMsg = "";
				bankStmtTrnOutput.setId(request[0].toString());
				bankStmtTrnOutput.setHdrId(Integer.parseInt(request[1].toString()));
				bankStmtTrnOutput.setUpldId(Integer.parseInt(request[2].toString()));

				bankStmtTrnOutput.setTypCode(request[3].toString());
				bankStmtTrnOutput.setTypNm(request[4].toString());
				bankStmtTrnOutput.setTrnAmount(commonFunctions.formatAmount(Double.parseDouble(request[5].toString())));

				if (request[6] != null) {
					bankStmtTrnOutput.setBnkRef(request[6].toString());
				} else {
					bankStmtTrnOutput.setBnkRef("");
				}

				if (request[7] != null) {
					bankStmtTrnOutput.setCustRef(request[7].toString());
				} else {
					bankStmtTrnOutput.setCustRef("");
				}

				if (request[8] != null) {
					bankStmtTrnOutput.setAdtnRef(request[8].toString());
				} else {
					bankStmtTrnOutput.setAdtnRef("");
				}

				if (request[9] != null) {
					bankStmtTrnOutput.setErpRef(request[9].toString());
					bankStmtTrnOutput.setTrnSts("Y");
				} else {
					bankStmtTrnOutput.setErpRef("");
					bankStmtTrnOutput.setTrnSts("");
				}

				bankStmtTrnOutput.setIsPrs(Boolean.parseBoolean(request[10].toString()));

				if (request[11] != null && request[11].toString().length() > 0) {

					errMsg = request[9].toString();

					if (errMsg.length() >= 1) {
						bankStmtTrnOutput.setTrnSts(errMsg.substring(0, 1));
						bankStmtTrnOutput.setErrMsg(errMsg.substring(1, errMsg.length()));
					} else {
						bankStmtTrnOutput.setErrMsg(errMsg);
					}
				} else {
					bankStmtTrnOutput.setErrMsg("");
				}

				if (request[12] != null) {
					bankStmtTrnOutput.setAppRef(request[12].toString());
				} else {
					bankStmtTrnOutput.setAppRef("");
				}

				if (request[13] != null) {
					bankStmtTrnOutput.setTypCdTrnTyp(request[13].toString());
				} else {
					bankStmtTrnOutput.setTypCdTrnTyp("");
				}

				if (request[14] != null) {
					bankStmtTrnOutput.setBnkCode(request[14].toString());
				} else {
					bankStmtTrnOutput.setBnkCode("");
				}

				if (request[15] != null) {
					bankStmtTrnOutput.setCrtdDttsStr(commonFunctions.formatDate((Date) request[15]));
					bankStmtTrnOutput.setCrtdDtts((Date) request[15]);
				}

				if (request[16] != null) {
					bankStmtTrnOutput.setPost1Ref(request[16].toString());
				} else {
					bankStmtTrnOutput.setPost1Ref("");
				}

				if (request[17] != null) {
					bankStmtTrnOutput.setPost2Ref(request[17].toString());
				} else {
					bankStmtTrnOutput.setPost2Ref("");
				}

				iFlg = iFlg + 1;

				starBrowseResponse.output.fldTblBnkTrn.add(bankStmtTrnOutput);
			}

		} catch (Exception e) {

			logger.debug("Bank Statement Info : {}", e.getMessage(), e);
			e.printStackTrace();

		} finally {
			session.close();
		}

	}

	public void processTrn(MaintanenceResponse<BankStmtManOutput> starManResponse, List<BankStmtTrnOutput> baiInfos,
			String usrId, String cmpyId, String post1, String post2) {
		Session session = null;

		double trnAmount = 0.0;
		String postDt = "";
		String erpRef = "";

		for (int i = 0; i < baiInfos.size(); i++) {
			String hql = "";

			session = SessionUtil.getSession();
			
			if(!baiInfos.get(i).getTypCode().equals("LBX"))
			{
				hql = "select  trn_amount,to_char(upld.crtd_dtts,'mm/dd/yyyy') stmt_dt, erp_ref from ebs_trn_dtls trn, ebs_upld_dtls upld where "
					+ " upld.id = trn.ebs_upld_id and trn.del_sts = true and trn.id=:id "
					+ " and trn.ebs_hdr_id=:hdr_id and trn.ebs_upld_id=:upld_id and trn.typ_code =:typ_cd  ";
			}
			else
			{
				hql = "select  inv_amt,to_char(hdr.dpst_dt,'mm/dd/yyyy') stmt_dt, erp_ref from  lck_trn_dtls trn, lck_hdr_dtls hdr where "
						+ " hdr.ebs_upld_id = trn.ebs_upld_id and hdr.batch_id = trn.batch_id and trn.del_sts = true and trn.id=:id "
						+ "and cast(trn.batch_id as int)=:hdr_id and trn.ebs_upld_id=:upld_id";
			}

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("id", Integer.parseInt(baiInfos.get(i).getId()));
			queryValidate.setParameter("hdr_id", baiInfos.get(i).getHdrId());
			queryValidate.setParameter("upld_id", baiInfos.get(i).getUpldId());
			
			if(!baiInfos.get(i).getTypCode().equals("LBX"))
			{
				queryValidate.setParameter("typ_cd", baiInfos.get(i).getTypCode());
			}

			List<Object[]> listRequest = queryValidate.list();

			for (Object[] request : listRequest) {

				trnAmount = Double.parseDouble(request[0].toString());
				postDt = request[1].toString();
				
				if(request[2] != null)
				{
					erpRef = request[2].toString();
				}
				else
				{
					erpRef = "";
				}

			}

			GLInfoDAO glInfoDAO = new GLInfoDAO();
			
			
			List<EBSGLAccountInfo> glCombination = null;
			
			if(!baiInfos.get(i).getTypCode().equals("LBX"))
			{
				glCombination = glInfoDAO.getGLAccount("1", baiInfos.get(i).getTypCode());
			}
			else
			{
				glCombination = glInfoDAO.getGLAccount("2", "LBX");
			}

			List<GLDistributionInfo> glebsMapInputs = new ArrayList<GLDistributionInfo>();

			if (post1.equals("Y") && glCombination.size() > 0) {

				glebsMapInputs = new ArrayList<GLDistributionInfo>();

				for (int j = 0; j < glCombination.size(); j++) {
					EBSGLAccountInfo accountInfo = glCombination.get(j);

					if (accountInfo.getPosSeq().equals("P1")) {

						GLDistributionInfo distInfo = new GLDistributionInfo();
						distInfo.setGlAcct(accountInfo.getGlAcct());
						distInfo.setSacct(accountInfo.getGlSubAcct());

						if (accountInfo.getTrnTyp().equals("D")) {
							distInfo.setDrAmt(String.valueOf(trnAmount));
							distInfo.setCrAmt(String.valueOf("0.0"));
						} else if (accountInfo.getTrnTyp().equals("C")) {
							distInfo.setCrAmt(String.valueOf(trnAmount));
							distInfo.setDrAmt(String.valueOf("0.0"));
						}
						distInfo.setRemark("");

						glebsMapInputs.add(distInfo);

					}
				}

				/* VALIDATE AMOUNT */
				double drAmt = 0;
				double crAmt = 0;
				for (int k = 0; k < glebsMapInputs.size(); k++) {
					drAmt = drAmt + Double.parseDouble(glebsMapInputs.get(k).getDrAmt());
					crAmt = crAmt + Double.parseDouble(glebsMapInputs.get(k).getCrAmt());
				}

				if (crAmt == drAmt && glebsMapInputs.size() > 0) {

					addLedgerDetails(starManResponse, baiInfos.get(i).getUpldId(), baiInfos.get(i).getHdrId(),
							Integer.parseInt(baiInfos.get(i).getId()), glebsMapInputs, cmpyId, usrId, "",
							baiInfos.get(i).getTypCode(), "", postDt, "1");
				}
			}

			if (post2.equals("Y") && glCombination.size() > 0) {

				glebsMapInputs = new ArrayList<GLDistributionInfo>();

				for (int j = 0; j < glCombination.size(); j++) {
					EBSGLAccountInfo accountInfo = glCombination.get(j);

					if (accountInfo.getPosSeq().equals("P2")) {
						
						String[] arrOfStr = erpRef.split("-");
						String subAccount = "";
						
						GLDistributionInfo distInfo = new GLDistributionInfo();
					
						if(arrOfStr.length > 2)
						{
							subAccount = getSubAccountCode(arrOfStr[2]);
						}
						
						if (accountInfo.getTrnTyp().equals("C"))
						{
							if(subAccount.length() > 0)
							{
								distInfo.setSacct(subAccount);
								distInfo.setGlAcct("11000260");
							}
							else
							{
								distInfo.setSacct(accountInfo.getGlSubAcct());
								distInfo.setGlAcct(accountInfo.getGlAcct());
							}
						}
						else
						{
							distInfo.setSacct(accountInfo.getGlSubAcct());
							distInfo.setGlAcct(accountInfo.getGlAcct());
						}
						
						if (accountInfo.getTrnTyp().equals("D")) {
							distInfo.setDrAmt(String.valueOf(trnAmount));
							distInfo.setCrAmt(String.valueOf("0.0"));
						} else if (accountInfo.getTrnTyp().equals("C")) {
							distInfo.setCrAmt(String.valueOf(trnAmount));
							distInfo.setDrAmt(String.valueOf("0.0"));
						}
						distInfo.setRemark("");
						glebsMapInputs.add(distInfo);

					}
				}

				double drAmt = 0;
				double crAmt = 0;
				for (int k = 0; k < glebsMapInputs.size(); k++) {
					drAmt = drAmt + Double.parseDouble(glebsMapInputs.get(k).getDrAmt());
					crAmt = crAmt + Double.parseDouble(glebsMapInputs.get(k).getCrAmt());
				}

				if (crAmt == drAmt && glebsMapInputs.size() > 0) {

					addLedgerDetails(starManResponse, baiInfos.get(i).getUpldId(), baiInfos.get(i).getHdrId(),
							Integer.parseInt(baiInfos.get(i).getId()), glebsMapInputs, cmpyId, usrId, "",
							baiInfos.get(i).getTypCode(), "", postDt, "2");
				}
			}

			if (glebsMapInputs.size() == 0) {
				starManResponse.output.messages.add("GL Mapping not found");

				starManResponse.output.rtnSts = 1;
			}
		}

	}

	/* ADD LEDGER INFORMATION AGAINST A TRANSACTION */
	public void addLedgerDetails(MaintanenceResponse<BankStmtManOutput> starManResponse, int upldId, int hdrId,
			int trnId, List<GLDistributionInfo> glDistInfoList, String cmpyId, String usrId, String rmk, String typCd,
			String narrDesc, String postDt, String pos) {

		GLInfoDAO dao = new GLInfoDAO();
		BankStmtDAO bankStmtDAO = new BankStmtDAO();

		Session session = null;
		Transaction tx = null;
		String hql = "";

		DateFormat formatter;
		Date dateobj = null;
		formatter = new SimpleDateFormat("MM/dd/yyyy");
		try {
			dateobj = formatter.parse(postDt);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		/*
		 * Date dateobj = new Date(); System.out.println(dfYM.format(dateobj));
		 */

		LedgerInfo ledgerInfo = dao.getLedgerId(cmpyId);

		int ictlNo = dao.getCtlNo();

		int iInsCtlNo = dao.getInsCtlNo();

		String acctPer = dao.getAccountingPeriod(postDt);

		/* GET FINANCIAL COMPANY */
		String finCoId = dao.getFinCoGateway();

		String trnRef = "LE-" + dao.getGLTransactionSeq();

		cmpyId = dao.getGLCompany(cmpyId);/* TEMPRORY RECORD FOR TESTING */

		String typCodeNm = "";
		if (typCd.length() > 0) {
			EBSTypeCodeDAO codeDAO = new EBSTypeCodeDAO();
			typCodeNm = codeDAO.readTypeCodeName(typCd);
		}

		try {
			session = SessionUtilGL.getSession();
			tx = session.beginTransaction();

			hql = "insert into gligje_rec (gje_src_co_id, gje_gat_ctl_no, gje_lgr_id, gje_jrnl_ent_typ, "
					+ "gje_acctg_per, gje_actvy_dt, gje_lgn_id, gje_src_jrnl, "
					+ "gje_nar_desc, gje_trs_src, gje_auto_rls_pstg, gje_orig_ref_pfx, "
					+ "gje_orig_ref_no, gje_orig_ref_itm, gje_orig_ref_sbitm, gje_orig_cry, "
					+ "gje_orig_exrt, gje_gl_extl_id1, gje_gl_extl_id2, gje_gl_extl1, gje_gl_extl2, "
					+ "gje_gl_extl_desc) values (:co_id, :ctl_no, :lgr_id, 'N', :actng_prd, :actvy_dt, "
					+ ":usr_id, 'OTH', :narr_desc, 'E', 0, '', 0, 0, 0, :cry, 1, :extl_id, '', '', '', :ext_desc)";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("co_id", finCoId);
			queryValidate.setParameter("ctl_no", ictlNo);
			queryValidate.setParameter("lgr_id", ledgerInfo.getLgrId());
			queryValidate.setParameter("usr_id", usrId);
			queryValidate.setParameter("cry", ledgerInfo.getCry());
			queryValidate.setParameter("actng_prd", Integer.parseInt(acctPer));
			queryValidate.setParameter("actvy_dt", dateobj);
			queryValidate.setParameter("ext_desc", typCd + "-" + typCodeNm);
			queryValidate.setParameter("extl_id", trnRef);
			queryValidate.setParameter("narr_desc", narrDesc);

			queryValidate.executeUpdate();

			addLedgerAmount(session, glDistInfoList, ictlNo, finCoId);

			dao.addSctitn(session, ictlNo, finCoId, cmpyId);

			dao.addSytasy(session, ictlNo, iInsCtlNo, finCoId, cmpyId, usrId);

			tx.commit();
			// updateTrnSts(trnRef, Integer.toString(trnId), Integer.toString(hdrId),
			// Integer.toString(upldId));

			boolean flgErr = false;
			String errMsg = "";
			String erpGLRef = "";

			Date d1 = new Date();

			while (true) {

				Date d2 = new Date();

				long seconds = (d2.getTime() - d1.getTime()) / 1000;

				if (seconds > CommonConstants.VLD_CHK_DUR) {
					flgErr = true;
					errMsg = "Unable to process the records via Gateway <br> Please try after some time.";
					tx = session.beginTransaction();
					GLInfoDAO glInfoDAO = new GLInfoDAO();
					glInfoDAO.delSytasy(session, ictlNo);
					glInfoDAO.delSctitn(session, finCoId, ictlNo, cmpyId);
					glInfoDAO.delLedgerItems(session, finCoId, ictlNo);
					glInfoDAO.delLedgerMain(session, finCoId, ictlNo);
					tx.commit();
					break;
				}

				String refVal = bankStmtDAO.getSTXRefInfo(trnRef);

				String[] trnRefVal = refVal.split(",");

				if (trnRefVal.length > 0) {
					if (trnRefVal[1].substring(0, 1).equals("Y")) {

						if (trnRefVal[0].length() > 0) {
							erpGLRef = trnRefVal[0];
							break;
						}

					} else {
						if (trnRefVal[1].substring(0, 1).equals("E")) {
							errMsg = trnRefVal[1];
							flgErr = true;
							break;
						}
					}
				}
			}

			if (flgErr == false) {

				if (pos.equals("1")) {
					addGLRef(trnId, hdrId, upldId, typCd, erpGLRef, "");
				} else {
					addGLRef(trnId, hdrId, upldId, typCd, "", erpGLRef);
				}

			} else {

				starManResponse.output.messages.add(trnId + "-" + hdrId + "-" + upldId + "-" + typCd + "->" + errMsg);

				starManResponse.output.rtnSts = 1;

			}

		} catch (Exception e) {
			logger.debug("GL Info : {}", e.getMessage(), e);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public void addLedgerAmount(Session session, List<GLDistributionInfo> glDistInfoList, int ictlNo, String finCoId)
			throws Exception {

		GLInfoDAO glInfoDAO = new GLInfoDAO();

		String hql = "";

		String subAcct = "";

		for (int i = 0; i < glDistInfoList.size(); i++) {

			/*
			 * if (Double.parseDouble(glDistInfoList.get(i).getCrAmt()) > 0) { hql =
			 * "insert into gligjd_rec (gjd_src_co_id, gjd_gat_ctl_no, gjd_gat_seq_no, gjd_bsc_gl_acct, "
			 * + "gjd_inbd_sacct, gjd_dr_amt, gjd_cr_amt, gjd_dist_rmk) values " +
			 * "(:co_id,:ctl_no, :s_no, :gl_acct, :sub_acct, :dr_amt, :cr_amt, '')"; } else
			 * if (Double.parseDouble(glDistInfoList.get(i).getDrAmt()) > 0) { hql =
			 * "insert into gligjd_rec (gjd_src_co_id, gjd_gat_ctl_no, gjd_gat_seq_no, gjd_bsc_gl_acct, "
			 * + "gjd_inbd_sacct, gjd_dr_amt, gjd_cr_amt, gjd_dist_rmk) values " +
			 * "(:co_id,:ctl_no, :s_no, :gl_acct, :sub_acct, :dr_amt, :cr_amt, '')"; }
			 */

			hql = "insert into gligjd_rec (gjd_src_co_id, gjd_gat_ctl_no, gjd_gat_seq_no, gjd_bsc_gl_acct, "
					+ "gjd_inbd_sacct, gjd_dr_amt, gjd_cr_amt, gjd_dist_rmk) values "
					+ "(:co_id,:ctl_no, :s_no, :gl_acct, :sub_acct, :dr_amt, :cr_amt, :rmk)";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("co_id", finCoId);
			queryValidate.setParameter("ctl_no", ictlNo);
			queryValidate.setParameter("s_no", i + 1);
			queryValidate.setParameter("gl_acct", glDistInfoList.get(i).getGlAcct());

			subAcct = glDistInfoList.get(i).getSacct();
			
			if(!subAcct.contains(","))
			{
				subAcct = glInfoDAO.getSubAcctFmtd(subAcct);
			}

			queryValidate.setParameter("sub_acct", subAcct);

			/*
			 * if(glDistInfoList.get(i).getCrAmt().length() > 0) {
			 * queryValidate.setParameter("cr_amt",
			 * Double.parseDouble(glDistInfoList.get(i).getCrAmt())); } else {
			 * queryValidate.setParameter("cr_amt", 0); }
			 * 
			 * if(glDistInfoList.get(i).getDrAmt().length() > 0) {
			 * queryValidate.setParameter("dr_amt",
			 * Double.parseDouble(glDistInfoList.get(i).getDrAmt())); } else {
			 * queryValidate.setParameter("dr_amt", 0); }
			 */

			queryValidate.setParameter("cr_amt", Double.parseDouble(glDistInfoList.get(i).getCrAmt()));
			queryValidate.setParameter("dr_amt", Double.parseDouble(glDistInfoList.get(i).getDrAmt()));
			queryValidate.setParameter("rmk", glDistInfoList.get(i).getRemark());

			queryValidate.executeUpdate();
		}

	}
	
	public void addGLRef(int trnId, int hdrId, int upldId, String typeCode, String glRef, String glRefUpd) {
		Session session = null;
		Transaction tx = null;

		try {

			session = SessionUtil.getSession();
			tx = session.beginTransaction();
			EbsTrnGlDtls params = new EbsTrnGlDtls();
			params.setId(trnId);
			params.setEbsHdrId(hdrId);
			params.setEbsUpldId(upldId);
			params.setTypCode(typeCode);
			params.setGlRef(glRef);
			params.setGlRefUpd(glRefUpd);

			session.save(params);

			tx.commit();
		} catch (Exception e) {

			logger.debug("Payment Info : {}", e.getMessage(), e);
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

	public String getTrnType(String typCode) {
		Session session = null;
		String hql = "";
		String trnType = "";

		try {
			session = SessionUtil.getSession();

			hql = "select trn_typ,1 from ebs_type_codes etc where typ_cd=:typ_cd ";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("typ_cd", typCode);

			List<Object[]> listTypeCodes = queryValidate.list();

			for (Object[] typeCd : listTypeCodes) {

				trnType = typeCd[0].toString();
			}

		} catch (Exception e) {
			logger.debug("Check Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return trnType;
	}
	
	
	public String getSubAccountCode(String rcptNo) {
		Session session = null;
		String hql = "";
		String subAccount = "";

		try {
			session = SessionUtilInformix.getSession();

			hql = "	select cast(xjd_inbd_sacct as varchar(40)) sub_acct,1 from fmtxje_rec,fmtxjd_rec where xje_gat_ctl_no = xjd_gat_ctl_no and " + 
					"	xje_orig_ref_pfx='CH' and xje_orig_ref_no=:rcpt_no and xjd_bsc_gl_acct='11000260' ";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("rcpt_no", Integer.parseInt(rcptNo));

			List<Object[]> listSubAccount = queryValidate.list();

			for (Object[] typeCd : listSubAccount) {

				subAccount = typeCd[0].toString();
			}

		} catch (Exception e) {
			logger.debug("Check Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return subAccount;
	}

	public void processTrnOnLoad(String typeCode, double trnAmount, int trnId, int hdrId, int upldId, String postDt,
			String cmpyId, String usrId, String stmtType) {
		GLInfoDAO glInfoDAO = new GLInfoDAO();

		List<EBSGLAccountInfo> glCombination = glInfoDAO.getGLAccount(stmtType, typeCode);

		List<GLDistributionInfo> glebsMapInputs = new ArrayList<GLDistributionInfo>();

		if (glCombination.size() > 0) {

			glebsMapInputs = new ArrayList<GLDistributionInfo>();

			for (int j = 0; j < glCombination.size(); j++) {
				EBSGLAccountInfo accountInfo = glCombination.get(j);

				if (accountInfo.getPosSeq().equals("P1")) {

					GLDistributionInfo distInfo = new GLDistributionInfo();
					distInfo.setGlAcct(accountInfo.getGlAcct());
					distInfo.setSacct(accountInfo.getGlSubAcct());

					if (accountInfo.getTrnTyp().equals("D")) {
						distInfo.setDrAmt(String.valueOf(trnAmount));
						distInfo.setCrAmt(String.valueOf("0.0"));
					} else if (accountInfo.getTrnTyp().equals("C")) {
						distInfo.setCrAmt(String.valueOf(trnAmount));
						distInfo.setDrAmt(String.valueOf("0.0"));
					}
					distInfo.setRemark("");

					glebsMapInputs.add(distInfo);

				}
			}

			/* VALIDATE AMOUNT */
			double drAmt = 0;
			double crAmt = 0;
			for (int k = 0; k < glebsMapInputs.size(); k++) {
				drAmt = drAmt + Double.parseDouble(glebsMapInputs.get(k).getDrAmt());
				crAmt = crAmt + Double.parseDouble(glebsMapInputs.get(k).getCrAmt());
			}

			if (crAmt == drAmt && glebsMapInputs.size() > 0) {

				addLedgerDetailsOnLoad(upldId, hdrId, trnId, glebsMapInputs, cmpyId, usrId, "", typeCode, "", postDt,
						"1");
			}
		}
	}

	/* ADD LEDGER INFORMATION AGAINST A TRANSACTION */
	public int addLedgerDetailsOnLoad(int upldId, int hdrId, int trnId, List<GLDistributionInfo> glDistInfoList,
			String cmpyId, String usrId, String rmk, String typCd, String narrDesc, String postDt, String pos) {

		GLInfoDAO dao = new GLInfoDAO();
		BankStmtDAO bankStmtDAO = new BankStmtDAO();
		int rtnSts = 0;
		Session session = null;
		Transaction tx = null;
		String hql = "";

		DateFormat formatter;
		Date dateobj = null;
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			dateobj = formatter.parse(postDt);
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		LedgerInfo ledgerInfo = dao.getLedgerId(cmpyId);

		int ictlNo = dao.getCtlNo();

		int iInsCtlNo = dao.getInsCtlNo();

		SimpleDateFormat formatDt = new SimpleDateFormat("MM/dd/yyyy");  
		String postDtFormatted= formatDt.format(dateobj);  
		
		String acctPer = dao.getAccountingPeriod(postDtFormatted);

		/* GET FINANCIAL COMPANY */
		String finCoId = dao.getFinCoGateway();

		String trnRef = "LE-" + dao.getGLTransactionSeq();

		cmpyId = dao.getGLCompany(cmpyId);/* TEMPRORY RECORD FOR TESTING */

		String typCodeNm = "";
		if (typCd.length() > 0) {
			EBSTypeCodeDAO codeDAO = new EBSTypeCodeDAO();
			typCodeNm = codeDAO.readTypeCodeName(typCd);
		}

		try {
			session = SessionUtilGL.getSession();
			tx = session.beginTransaction();

			hql = "insert into gligje_rec (gje_src_co_id, gje_gat_ctl_no, gje_lgr_id, gje_jrnl_ent_typ, "
					+ "gje_acctg_per, gje_actvy_dt, gje_lgn_id, gje_src_jrnl, "
					+ "gje_nar_desc, gje_trs_src, gje_auto_rls_pstg, gje_orig_ref_pfx, "
					+ "gje_orig_ref_no, gje_orig_ref_itm, gje_orig_ref_sbitm, gje_orig_cry, "
					+ "gje_orig_exrt, gje_gl_extl_id1, gje_gl_extl_id2, gje_gl_extl1, gje_gl_extl2, "
					+ "gje_gl_extl_desc) values (:co_id, :ctl_no, :lgr_id, 'N', :actng_prd, :actvy_dt, "
					+ ":usr_id, 'OTH', :narr_desc, 'E', 0, '', 0, 0, 0, :cry, 1, :extl_id, '', '', '', :ext_desc)";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("co_id", finCoId);
			queryValidate.setParameter("ctl_no", ictlNo);
			queryValidate.setParameter("lgr_id", ledgerInfo.getLgrId());
			queryValidate.setParameter("usr_id", usrId);
			queryValidate.setParameter("cry", ledgerInfo.getCry());
			queryValidate.setParameter("actng_prd", Integer.parseInt(acctPer));
			queryValidate.setParameter("actvy_dt", dateobj);
			queryValidate.setParameter("ext_desc", typCd + "-" + typCodeNm);
			queryValidate.setParameter("extl_id", trnRef);
			queryValidate.setParameter("narr_desc", narrDesc);

			queryValidate.executeUpdate();

			bankStmtDAO.addLedgerAmount(session, glDistInfoList, ictlNo, finCoId);

			dao.addSctitn(session, ictlNo, finCoId, cmpyId);

			dao.addSytasy(session, ictlNo, iInsCtlNo, finCoId, cmpyId, usrId);

			tx.commit();
			// updateTrnSts(trnRef, Integer.toString(trnId), Integer.toString(hdrId),
			// Integer.toString(upldId));

			boolean flgErr = false;
			String errMsg = "";
			String erpGLRef = "";

			Date d1 = new Date();

			while (true) {

				Date d2 = new Date();

				long seconds = (d2.getTime() - d1.getTime()) / 1000;

				if (seconds > CommonConstants.VLD_CHK_DUR) {
					flgErr = true;
					errMsg = "Unable to process the records via Gateway <br> Please try after some time.";
					tx = session.beginTransaction();
					GLInfoDAO glInfoDAO = new GLInfoDAO();
					glInfoDAO.delSytasy(session, ictlNo);
					glInfoDAO.delSctitn(session, finCoId, ictlNo, cmpyId);
					glInfoDAO.delLedgerItems(session, finCoId, ictlNo);
					glInfoDAO.delLedgerMain(session, finCoId, ictlNo);
					tx.commit();
					break;
				}

				String refVal = bankStmtDAO.getSTXRefInfo(trnRef);

				String[] trnRefVal = refVal.split(",");

				if (trnRefVal.length > 0) {
					if (trnRefVal[1].substring(0, 1).equals("Y")) {

						if (trnRefVal[0].length() > 0) {
							erpGLRef = trnRefVal[0];
							break;
						}

					} else {
						if (trnRefVal[1].substring(0, 1).equals("E")) {
							errMsg = trnRefVal[1];
							flgErr = true;
							break;
						}
					}
				}
			}

			if (flgErr == false) {

				if (pos.equals("1")) {
					addGLRef(trnId, hdrId, upldId, typCd, erpGLRef, "");
				} else {
					addGLRef(trnId, hdrId, upldId, typCd, "", erpGLRef);
				}

			} else {

				rtnSts = 1;

			}

		} catch (Exception e) {
			logger.debug("GL Info : {}", e.getMessage(), e);
			tx.rollback();
			rtnSts = 1;
		} finally {
			if (session != null) {
				session.close();
			}
		}
		return rtnSts;
	}
	
	public List<String> getDistinctVendors(List<InvoiceInfo> info) {
		List<String> venList = new ArrayList<String>();

		for (int i = 0; i < info.size(); i++) {
			if (!venList.contains(info.get(i).getVchrVenId())) {
				venList.add(info.get(i).getVchrVenId());
			}
		}

		return venList;
	}
	
	public List<InvoiceInfo> getDistinctVendorInfo(List<InvoiceInfo> info)
	{
		double totalAmnt = 0.0;
		String vchrInvNo = "";
		String vchrRef = "";
		String vchrPayMthd = "";
		String vchrCry = "";
		String cmpyId = "";
		String pmntDt = "";
		String vchrNotes = "";
		List<InvoiceInfo> invTotals = new ArrayList<InvoiceInfo>();
		CommonFunctions functions = new CommonFunctions();
		
		List<String> distVenList = getDistinctVendors(info);
		
		for(int i =0; i < distVenList.size(); i++)
		{
			InvoiceInfo invoiceInfo = new InvoiceInfo();
			totalAmnt = 0.0;
			vchrInvNo = "";
			vchrRef = "";
			pmntDt = "";
			for(int j =0; j < info.size(); j++)
			{
				if(info.get(j).getVchrVenId().equals(distVenList.get(i)))
				{
					totalAmnt = totalAmnt + info.get(j).getVchrAmt() - info.get(j).getVchrDiscAmt();
					vchrInvNo = vchrInvNo + info.get(j).getVchrInvNo() + " ";
					vchrRef = vchrRef + info.get(j).getVchrPfx() + info.get(j).getVchrNo() ;
					
					if(info.get(j).getVchrInvNo() != null)
					{
						if(info.get(j).getVchrInvNo().trim().length() > 0)
						{
							vchrRef = vchrRef + " " + info.get(j).getVchrInvNo().trim();
						}
					}
					
					if(info.get(j).getNotesToPayee() != null)
					{
						if(info.get(j).getNotesToPayee().trim().length() > 0)
						{
							vchrRef = vchrRef + " " + info.get(j).getNotesToPayee().trim();
						}
					}
					
					vchrRef = vchrRef + "/";
					
					vchrPayMthd = info.get(j).getPayMthd();
					vchrCry = info.get(j).getVchrCry();
					cmpyId = info.get(j).getCmpyId();
					pmntDt = info.get(j).getPaymentDt();
				}
			}
						
			invoiceInfo.setCmpyId(cmpyId);
			invoiceInfo.setVchrVenId(distVenList.get(i));
			invoiceInfo.setVchrAmt(totalAmnt);
			
			String temp = functions.whiteEscapeCharacter(vchrInvNo);
			if(temp.length() > 140)
			{
				temp = temp.substring(0,139);
				temp = temp + "/";
			}
			
			invoiceInfo.setVchrInvNo(temp);
			
			temp = functions.whiteEscapeCharacter(vchrRef);
			
			if(temp.length() > 140)
			{
				temp = temp.substring(0,139);
				temp = temp + "/";
			}
			
			invoiceInfo.setVchrNo(temp);
			invoiceInfo.setPayMthd(vchrPayMthd);
			invoiceInfo.setVchrCry(vchrCry);
			invoiceInfo.setPaymentDt(pmntDt);
			
			
			invTotals.add(invoiceInfo);
			
		}
		
		return invTotals;
	}
	
	
	/* USED TO AUTO UPDATE TRANSACTION WHILE DOING PAYMENTS */
	public void addLedgerEntryOnLoad(MaintanenceResponse<PaymentManOutput> starManResponse, String cmpyId, String usrId,
			InvoiceInfo invoiceInfo, String reqId, String payDt) {
		
		GLInfoDAO dao = new GLInfoDAO();
		BankStmtDAO bankStmtDAO = new BankStmtDAO();
		
		Session session = null;
		Transaction tx = null;
		String hql = "";

		DateFormat formatter;
		Date dateobj = null;		
		
		LedgerInfo ledgerInfo = dao.getLedgerId(cmpyId);

		int ictlNo = dao.getCtlNo();

		int iInsCtlNo = dao.getInsCtlNo();
		
		String formatDate = "";
		formatter = new SimpleDateFormat("MM/dd/yyyy");
		if(payDt.length() > 0)
		{
			formatDate = payDt;
			
			try {
				dateobj = formatter.parse(formatDate);
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		else
		{
			formatDate = formatter.format(new Date());
			dateobj = new Date();
		}
		
		String acctPer = dao.getAccountingPeriod(formatDate);

		/* GET FINANCIAL COMPANY */
		String finCoId = dao.getFinCoGateway();

		String trnId = "LE-" + dao.getGLTransactionSeq();

		cmpyId = dao.getGLCompany(cmpyId);/* TEMPRORY RECORD FOR TESTING */

		try {
			session = SessionUtilGL.getSession();
			tx = session.beginTransaction();

			hql = "insert into gligje_rec (gje_src_co_id, gje_gat_ctl_no, gje_lgr_id, gje_jrnl_ent_typ, "
					+ "gje_acctg_per, gje_actvy_dt, gje_lgn_id, gje_src_jrnl, "
					+ "gje_nar_desc, gje_trs_src, gje_auto_rls_pstg, gje_orig_ref_pfx, "
					+ "gje_orig_ref_no, gje_orig_ref_itm, gje_orig_ref_sbitm, gje_orig_cry, "
					+ "gje_orig_exrt, gje_gl_extl_id1, gje_gl_extl_id2, gje_gl_extl1, gje_gl_extl2, "
					+ "gje_gl_extl_desc) values (:co_id, :ctl_no, :lgr_id, 'N', :actng_prd, :actvy_dt, "
					+ ":usr_id, 'OTH', '', 'E', 0, '', 0, 0, 0, :cry, 1, :extl_id, :extl_id1, :extl_id2, '', :ext_desc)";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("co_id", finCoId);
			queryValidate.setParameter("ctl_no", ictlNo);
			queryValidate.setParameter("lgr_id", ledgerInfo.getLgrId());
			queryValidate.setParameter("usr_id", usrId);
			queryValidate.setParameter("cry", ledgerInfo.getCry());
			queryValidate.setParameter("actng_prd", Integer.parseInt(acctPer));
			queryValidate.setParameter("actvy_dt", dateobj);
			queryValidate.setParameter("ext_desc", "");
			queryValidate.setParameter("extl_id", trnId);
			queryValidate.setParameter("extl_id1", reqId);
			queryValidate.setParameter("extl_id2", invoiceInfo.getVchrVenId());

			queryValidate.executeUpdate();

			dao.addLedgerAmount(session, "475", String.valueOf(invoiceInfo.getVchrAmt()), ictlNo, finCoId);

			dao.addSctitn(session, ictlNo, finCoId, cmpyId);

			dao.addSytasy(session, ictlNo, iInsCtlNo, finCoId, cmpyId, usrId);

			tx.commit();

			tx = session.beginTransaction();
			
			Date d1 = new Date();
			
			while (true) {
				
				Date d2 = new Date();

				long seconds = (d2.getTime() - d1.getTime()) / 1000;

				if (seconds > CommonConstants.VLD_CHK_DUR) {
					tx = session.beginTransaction();
					GLInfoDAO glInfoDAO = new GLInfoDAO();
					glInfoDAO.delSytasy(session, ictlNo);
					glInfoDAO.delSctitn(session, finCoId, ictlNo, cmpyId);
					glInfoDAO.delLedgerItems(session, finCoId, ictlNo);
					glInfoDAO.delLedgerMain(session, finCoId, ictlNo);
					tx.commit();
					break;
				}
				
				String refVal = bankStmtDAO.getSTXRefInfo(trnId);

				String[] trnRefVal = refVal.split(",");

				if (trnRefVal.length > 0) {
					if (trnRefVal[1].substring(0, 1).equals("Y")) {
						if (trnRefVal[0].length() > 0) {
							break;
						}

					} else {
						if (trnRefVal[1].substring(0, 1).equals("E")) {
							/*starManResponse.output.rtnSts = 1;*/
							starManResponse.output.messages.add(CommonConstants.GL_PAY_ERR);
							break;
						}
					}
				}
			}

			tx.commit();

		} catch (Exception e) {
			logger.debug("GL Info : {}", e.getMessage(), e);
			/*starManResponse.output.rtnSts = 1;*/
			/*starManResponse.output.messages.add(CommonConstants.GL_PAY_ERR);*/
			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}
	}
	
	
	
	
	public List<InvoiceInfo> getPropInvoices(BrowseResponse<InvoiceInfoBrowse> starBrowseResponse, String proposalId) {
		Session session = null;
		String payMthd = "";
		CommonFunctions commonFunctions = new CommonFunctions();
		List<InvoiceInfo> invoiceList = new ArrayList<InvoiceInfo>();

		try {

			String hql = "";

			session = SessionUtil.getSession();

			hql = "select mthd_cd,inv_no, vchr_notes, param.req_id,param.pay_crtd_on from cstm_param_inv inv,"
					+ "pay_mthd mthd, cstm_pay_params param where "
					+ "inv.req_id = param.req_id and inv_sts = true and vchr_pay_mthd = mthd.id ";
			
			if(proposalId.length() > 0)
			{
				hql = hql + " and param.req_id=:prop_id";
			}
			
			hql = hql + " order by param.req_id, mthd.id, inv_no";
			
			Query queryValidate = session.createSQLQuery(hql);

			if(proposalId.length() > 0)
			{
				queryValidate.setParameter("prop_id", proposalId);
			}

			List<Object[]> listVoucher = queryValidate.list();

			for (Object[] vou : listVoucher) {
				if (vou[0] != null) {
					InvoiceInfo info = new InvoiceInfo();
					
					info.setReqId(vou[3].toString());
					
					payMthd = vou[0].toString();

					if (vou[2] != null) {
						info.setNotesToPayee(vou[2].toString());
					} else {
						info.setNotesToPayee("");
					}
					
					if (vou[4] != null) {
						info.setPaymentDt((commonFunctions.formatDateWithoutTime((Date) vou[4])));
					} else {
						info.setPaymentDt("");
					}

					info.setPayMthd(payMthd);

					getInvoiceDetailsStx(info, vou[1].toString(), info.getReqId());

					invoiceList.add(info);
					
					starBrowseResponse.output.fldTblInvoice.add(info);
				}
			}

		} catch (Exception e) {
			logger.debug("ACH Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}

		return invoiceList;

	}
	
	public InvoiceInfo getInvoiceDetailsStx(InvoiceInfo invoiceInfo, String vchrNo, String reqId) {
		Session session = null;

		try {

			String hql = "";

			session = SessionUtilInformix.getSession();

			if (CommonConstants.DB_TYP.equals("POS")) {
				hql = "	select cast(pyh_cmpy_id as varchar(3)) cmpy_id, cast(pyh_opa_pfx as varchar(2)) pfx, "
						+ "pyh_opa_no, cast(pyh_ven_id as varchar(8)) ven_id, cast(pyh_ven_inv_no as varchar(22)) ven_inv_no, "
						+ "pyh_orig_amt ,pyh_disc_amt,cast(pyh_cry as varchar(3)) cry, to_char(pyh_ven_inv_dt, 'yyyy-mm-dd') inv_dt,"
						+ " (case when (pyh_disc_dt - date(now())) is null or (pyh_disc_dt - date(now()) < 0) then 'N' else 'Y' end) disc_sts"
						+ " from aptpyh_rec where 1 = 1";

				if (vchrNo.length() > 0) {
					hql = hql + " and pyh_opa_pfx || '-' || pyh_opa_no =:vchr_no";
				}
			} else if (CommonConstants.DB_TYP.equals("INF")) {

				hql = "	select cast(pyh_cmpy_id as varchar(3)) cmpy_id, cast(pyh_opa_pfx as varchar(2)) pfx, "
						+ "pyh_opa_no, cast(pyh_ven_id as varchar(8)) ven_id, cast(pyh_ven_inv_no as varchar(22)) ven_inv_no, "
						+ "pyh_orig_amt ,pyh_disc_amt,cast(pyh_cry as varchar(3)) cry, to_char(pyh_ven_inv_dt, '%Y-%m-%d') inv_dt, "
						+ "(case when (pyh_disc_dt - date(now())) is null or (pyh_disc_dt - date(now()) < 0) then 'N' else 'Y' end) disc_sts"
						+ "from aptpyh_rec where 1 = 1";

				if (vchrNo.length() > 0) {
					hql = hql + " and pyh_opa_pfx || '-' || pyh_opa_no =:vchr_no";
				}

			}

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("vchr_no", vchrNo);

			List<Object[]> listVouchers = queryValidate.list();

			for (Object[] voucher : listVouchers) {
				invoiceInfo.setCmpyId(voucher[0].toString());
				invoiceInfo.setVchrPfx(voucher[1].toString());
				invoiceInfo.setVchrNo(voucher[2].toString());
				invoiceInfo.setVchrVenId(voucher[3].toString());
				invoiceInfo.setVchrInvNo(voucher[4].toString().trim());
				invoiceInfo.setVchrAmt(Double.parseDouble(voucher[5].toString()));
				invoiceInfo.setVchrDiscAmt(Double.parseDouble(voucher[6].toString()));
				invoiceInfo.setVchrCry(voucher[7].toString());
				invoiceInfo.setInvDt(voucher[8].toString());
				invoiceInfo.setVchrAmtStr(voucher[6].toString());

				if (voucher[9].toString().equals("N")) {
					invoiceInfo.setVchrDiscAmt(0);
				}
				
				JournalInfo glRef = getJournalReference(reqId, invoiceInfo.getVchrVenId());

				if (glRef != null) {
					invoiceInfo.setGlEntry(glRef.getJeEntry());
					invoiceInfo.setGlEntryDt(glRef.getPostDt());
				} else {
					invoiceInfo.setGlEntry("");
					invoiceInfo.setGlEntryDt("");
				}
				
				double totalAmnt = invoiceInfo.getVchrAmt() - invoiceInfo.getVchrDiscAmt();
				
				invoiceInfo.setVchrVendTotal(totalAmnt);
			}

		} catch (Exception e) {
			logger.debug("ACH Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}

		return invoiceInfo;

	}
	
	public JournalInfo getJournalReference(String reqId, String venId) {
		Session session = null;
		String hql = "";
		String trnRef = "";
		String sts = "";
		String errMsg = "";

		JournalInfo journalInfo = null;

		try {
			session = SessionUtilGL.getSession();

			hql = "select oje_oje_pfx || '-' ||oje_oje_no, to_char(oje_actvy_dt, 'mm/dd/yyyy') actv_dt from gltoje_rec "
					+ "where oje_gl_extl_id2=:req_id and oje_gl_extl1=:ven_id";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("req_id", reqId);
			queryValidate.setParameter("ven_id", venId);

			List<Object[]> listGLEntry = queryValidate.list();

			for (Object[] gl : listGLEntry) {

				journalInfo = new JournalInfo();

				journalInfo.setJeEntry(String.valueOf(gl[0]));
				journalInfo.setPostDt(String.valueOf(gl[1]));
			}

		} catch (Exception e) {
			logger.debug("GL Info DAO : {}", e.getMessage(), e);
		} finally {
			session.close();
		}

		return journalInfo;

	}

}


