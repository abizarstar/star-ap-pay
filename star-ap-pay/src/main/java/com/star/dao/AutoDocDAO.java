package com.star.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.SessionUtil;
import com.star.linkage.automate.PullDirecInfo;

public class AutoDocDAO {

	private static Logger logger = LoggerFactory.getLogger(AutoDocDAO.class);
	
	public List<PullDirecInfo> getDirectoryList() {

		Session session = null;
		String hql = "";
		
		List<PullDirecInfo> direcInfos = new ArrayList<PullDirecInfo>();

		try {
			session = SessionUtil.getSession();

			hql = " select root_pth,dir_typ from doc_pull_dir_config";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listDir = queryValidate.list();

			for (Object[] dir : listDir) {

				PullDirecInfo pullDirecInfo = new PullDirecInfo();

				pullDirecInfo.setRootPath(dir[0].toString());
				pullDirecInfo.setDirType(dir[1].toString());
				
				direcInfos.add(pullDirecInfo);

			}

		} catch (Exception e) {
			logger.debug("Common Info : {}", e.getMessage(), e);
		} finally {
			session.close();
		}
		
		return direcInfos;
	}
	
}
