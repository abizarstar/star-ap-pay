package com.star.dao;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.star.common.BrowseResponse;
import com.star.common.CommonFunctions;
import com.star.common.SessionUtil;
import com.star.linkage.eml.EmailConfigBrowse;
import com.star.linkage.eml.EmailConfigDetails;
import com.star.modal.UsrEmlConfigDtls;

public class UsrEmlConfigDAO {

	private static Logger logger = LoggerFactory.getLogger(UsrEmlConfigDAO.class);

	public void addEmlInfo(String eml, String accToken, String refToken, String chkSum, String lgnTyp, String usrId) {

		Session session = null;
		Transaction tx = null;
		Date date = new Date();

		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		long time = cal.getTimeInMillis();

		try {

			session = SessionUtil.getSession();
			tx = session.beginTransaction();
			UsrEmlConfigDtls params = new UsrEmlConfigDtls();
			params.setUsrEml(eml);
			params.setAccessToken(accToken);
			params.setRefreshToekn(refToken);
			params.setChkSum(chkSum);
			params.setLgnTyp(lgnTyp);
			params.setLstRfshTm(time);
			params.setCrtdBy(usrId);
			params.setCrtdOn(date);
			params.setSts(true);

			session.save(params);

			tx.commit();
		} catch (Exception e) {

			logger.debug("Email Config Info : {}", e.getMessage(), e);

			tx.rollback();
		} finally {
			if (session != null) {
				session.close();
			}
		}

	}

	public void updEmlInfo(String eml, String accToken, String refToken) {
		Session session = null;
		String hql = "";
		Transaction tx = null;

		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			hql = "UPDATE usr_eml_config_dtls SET acc_token=:acc, rfsh_token=:rfsh WHERE usr_eml=:eml";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("acc", accToken);
			queryValidate.setParameter("rfsh", refToken);
			queryValidate.setParameter("eml", eml);

			queryValidate.executeUpdate();

			tx.commit();

		} catch (Exception e) {
			logger.debug("Payment Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}
	}

	public void updLstRefreshTime(String eml, long rfshTm) {
		Session session = null;
		String hql = "";
		Transaction tx = null;

		Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		long time = cal.getTimeInMillis();

		try {
			session = SessionUtil.getSession();
			tx = session.beginTransaction();

			hql = "UPDATE usr_eml_config_dtls SET lst_rfsh_tm=:rfsh_tm WHERE usr_eml=:eml";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("rfsh_tm", rfshTm);
			queryValidate.setParameter("eml", eml);

			queryValidate.executeUpdate();

			tx.commit();

		} catch (Exception e) {
			logger.debug("Payment Info : {}", e.getMessage(), e);

		} finally {
			session.close();
		}
	}

	public int validateEmail(String emlId) {

		Session session = null;
		int emlCount = 0;

		try {

			String hql = "";

			session = SessionUtil.getSession();

			hql = "select count(*) from usr_eml_config_dtls where usr_eml=:eml";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("eml", emlId);

			emlCount = Integer.parseInt(queryValidate.list().get(0).toString());

		} catch (Exception e) {
			logger.debug("Default Fields Info : {}", e.getMessage(), e);

		} finally {
			if (session != null) {
				session.close();
			}
		}

		return emlCount;
	}

	public UsrEmlConfigDtls getRefreshToken(String emlId) {

		Session session = null;
		String refreshToken = "";

		UsrEmlConfigDtls configDtls = new UsrEmlConfigDtls();

		try {

			String hql = "";

			session = SessionUtil.getSession();

			hql = "select rfsh_token,lst_rfsh_tm from usr_eml_config_dtls where usr_eml=:eml";

			Query queryValidate = session.createSQLQuery(hql);

			queryValidate.setParameter("eml", emlId);

			List<Object[]> listRequest = queryValidate.list();

			for (Object[] request : listRequest) {

				if (request[0] != null) {
					configDtls.setRefreshToekn(request[0].toString());
				} else {
					configDtls.setRefreshToekn("");
				}
				configDtls.setLstRfshTm(Long.parseLong(request[1].toString()));
			}

		} catch (Exception e) {
			logger.debug("Default Fields Info : {}", e.getMessage(), e);

		} finally {
			if (session != null) {
				session.close();
			}
		}

		return configDtls;
	}

	public void getEmlDetails(BrowseResponse<EmailConfigBrowse> browseResponse) {

		Session session = null;
		String refreshToken = "";

		CommonFunctions commonFunctions = new CommonFunctions();

		try {

			String hql = "";

			session = SessionUtil.getSession();

			hql = "select usr_eml, crtd_on, (select usr_nm from usr_dtls where usr_id= crtd_by) user_NM, sts, lgn_typ, chk_sum from usr_eml_config_dtls where sts=true";

			Query queryValidate = session.createSQLQuery(hql);

			List<Object[]> listRequest = queryValidate.list();

			for (Object[] request : listRequest) {

				EmailConfigDetails configDtls = new EmailConfigDetails();

				configDtls.setUsrEml(request[0].toString());

				if (request[0] != null) {
					configDtls.setCrtdOnStr((commonFunctions.formatDate((Date) request[1])));
					configDtls.setCrtdOn((Date) request[1]);
				}

				if (request[2] != null) {
					configDtls.setCrtdBy(request[2].toString());
				} else {
					configDtls.setCrtdBy("");
				}
				configDtls.setSts((Boolean) request[3]);

				if (request[4] != null) {
					configDtls.setLgnTyp(request[4].toString());
				} else {
					configDtls.setLgnTyp("");
				}

				if (request[5] != null) {
					configDtls.setChkSum(request[5].toString());
				} else {
					configDtls.setChkSum("");
				}

				browseResponse.output.fldTblEmlConfig.add(configDtls);
			}

		} catch (Exception e) {
			logger.debug("Default Fields Info : {}", e.getMessage(), e);

		} finally {
			if (session != null) {
				session.close();
			}
		}
	}

}
