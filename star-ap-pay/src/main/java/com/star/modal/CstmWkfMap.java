package com.star.modal;

import java.io.Serializable;
import java.util.Date;

public class CstmWkfMap implements Serializable{

	private int ctlNo;
	private int wkfId;
	private Date wkfAsgnOn;
	private String wkfAsgnTo;
	private String wkfSts;
	private String wkfRmk;
	private String wkfRmkTyp;
	
	public int getCtlNo() {
		return ctlNo;
	}
	public void setCtlNo(int ctlNo) {
		this.ctlNo = ctlNo;
	}
	public int getWkfId() {
		return wkfId;
	}
	public void setWkfId(int wkfId) {
		this.wkfId = wkfId;
	}
	public Date getWkfAsgnOn() {
		return wkfAsgnOn;
	}
	public void setWkfAsgnOn(Date wkfAsgnOn) {
		this.wkfAsgnOn = wkfAsgnOn;
	}
	public String getWkfAsgnTo() {
		return wkfAsgnTo;
	}
	public void setWkfAsgnTo(String wkfAsgnTo) {
		this.wkfAsgnTo = wkfAsgnTo;
	}
	public String getWkfSts() {
		return wkfSts;
	}
	public void setWkfSts(String wkfSts) {
		this.wkfSts = wkfSts;
	}
	public String getWkfRmk() {
		return wkfRmk;
	}
	public void setWkfRmk(String wkfRmk) {
		this.wkfRmk = wkfRmk;
	}
	public String getWkfRmkTyp() {
		return wkfRmkTyp;
	}
	public void setWkfRmkTyp(String wkfRmkTyp) {
		this.wkfRmkTyp = wkfRmkTyp;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ctlNo;
		result = prime * result + wkfId;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CstmWkfMap other = (CstmWkfMap) obj;
		if (ctlNo != other.ctlNo)
			return false;
		if (wkfId != other.wkfId)
			return false;
		return true;
	}
	
	
	
}
