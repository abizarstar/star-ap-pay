package com.star.modal;

import java.io.Serializable;
import java.util.Date;

public class Workflow implements Serializable{

	
	private long wkfId;
	private String wkfName;
	private Date crtdDate;
	private String crtdBy;
	private Date dltDate;
	private String dltBy;
	
	public long getWkfId() {
		return wkfId;
	}
	public String getWkfName() {
		return wkfName;
	}
	public void setWkfName(String wkfName) {
		this.wkfName = wkfName;
	}
	public void setWkfId(long wkfId) {
		this.wkfId = wkfId;
	}
	public Date getCrtdDate() {
		return crtdDate;
	}
	public void setCrtdDate(Date crtdDate) {
		this.crtdDate = crtdDate;
	}
	public String getCrtdBy() {
		return crtdBy;
	}
	public void setCrtdBy(String crtdBy) {
		this.crtdBy = crtdBy;
	}
	public Date getDltDate() {
		return dltDate;
	}
	public void setDltDate(Date dltDate) {
		this.dltDate = dltDate;
	}
	public String getDltBy() {
		return dltBy;
	}
	public void setDltBy(String dltBy) {
		this.dltBy = dltBy;
	}
	
}
