package com.star.modal;

import java.io.Serializable;

public class LckTrnDtls implements Serializable{

	private int id;
	private int ebsUpldId;
	private String batchId;
	private String trnId;
	private String lckRoutNo;
	private String lckAcctNo;
	private String chkNo;
	private String invNo;
	private String invAmt;
	private String erpRef;
	private boolean isPrs;
	private String errMsg;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getEbsUpldId() {
		return ebsUpldId;
	}
	public void setEbsUpldId(int ebsUpldId) {
		this.ebsUpldId = ebsUpldId;
	}
	public String getBatchId() {
		return batchId;
	}
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}
	public String getTrnId() {
		return trnId;
	}
	public void setTrnId(String trnId) {
		this.trnId = trnId;
	}
	public String getLckRoutNo() {
		return lckRoutNo;
	}
	public void setLckRoutNo(String lckRoutNo) {
		this.lckRoutNo = lckRoutNo;
	}
	public String getLckAcctNo() {
		return lckAcctNo;
	}
	public void setLckAcctNo(String lckAcctNo) {
		this.lckAcctNo = lckAcctNo;
	}
	public String getChkNo() {
		return chkNo;
	}
	public void setChkNo(String chkNo) {
		this.chkNo = chkNo;
	}
	public String getInvNo() {
		return invNo;
	}
	public void setInvNo(String invNo) {
		this.invNo = invNo;
	}
	public String getInvAmt() {
		return invAmt;
	}
	public void setInvAmt(String invAmt) {
		this.invAmt = invAmt;
	}
	public String getErpRef() {
		return erpRef;
	}
	public void setErpRef(String erpRef) {
		this.erpRef = erpRef;
	}
	public boolean getIsPrs() {
		return isPrs;
	}
	public void setIsPrs(boolean isPrs) {
		this.isPrs = isPrs;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((batchId == null) ? 0 : batchId.hashCode());
		result = prime * result + ebsUpldId;
		result = prime * result + id;
		result = prime * result + ((trnId == null) ? 0 : trnId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LckTrnDtls other = (LckTrnDtls) obj;
		if (batchId == null) {
			if (other.batchId != null)
				return false;
		} else if (!batchId.equals(other.batchId))
			return false;
		if (ebsUpldId != other.ebsUpldId)
			return false;
		if (id != other.id)
			return false;
		if (trnId == null) {
			if (other.trnId != null)
				return false;
		} else if (!trnId.equals(other.trnId))
			return false;
		return true;
	}
	
	
	
}
