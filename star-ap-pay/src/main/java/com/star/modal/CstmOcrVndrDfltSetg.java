package com.star.modal;

import java.io.Serializable;
import java.util.Date;

public class CstmOcrVndrDfltSetg implements Serializable{

	private String cmpyId;
	private String venId;
	private String vchrBrh;
	private String vchrCat;
	private String trsStsActn;
	private String trsSts;
	private String trsRsn;
	private String ntfUsr;
	private Integer isActive;
	private Date crtdDtts;
	private String crtdBy;
	
	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}
	
	public String getVenId() {
		return venId;
	}
	public void setVenId(String venId) {
		this.venId = venId;
	}
	
	public String getVchrBrh() {
		return vchrBrh;
	}
	public void setVchrBrh(String vchrBrh) {
		this.vchrBrh = vchrBrh;
	}
	
	public String getVchrCat() {
		return vchrCat;
	}
	public void setVchrCat(String vchrCat) {
		this.vchrCat = vchrCat;
	}
	
	public String getTrsStsActn() {
		return trsStsActn;
	}
	public void setTrsStsActn(String trsStsActn) {
		this.trsStsActn = trsStsActn;
	}
	
	public String getTrsSts() {
		return trsSts;
	}
	public void setTrsSts(String trsSts) {
		this.trsSts = trsSts;
	}
	
	public String getTrsRsn() {
		return trsRsn;
	}
	public void setTrsRsn(String trsRsn) {
		this.trsRsn = trsRsn;
	}
	
	public String getNtfUsr() {
		return ntfUsr;
	}
	public void setNtfUsr(String ntfUsr) {
		this.ntfUsr = ntfUsr;
	}
	
	public Integer getIsActive() {
		return isActive;
	}
	public void setIsActive(Integer isActive) {
		this.isActive = isActive;
	}
	
	public Date getCrtdDtts() {
		return crtdDtts;
	}
	public void setCrtdDtts(Date crtdDtts) {
		this.crtdDtts = crtdDtts;
	}
	
	public String getCrtdBy() {
		return crtdBy;
	}
	public void setCrtdBy(String crtdBy) {
		this.crtdBy = crtdBy;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cmpyId == null) ? 0 : cmpyId.hashCode());
		result = prime * result + ((venId == null) ? 0 : venId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CstmOcrVndrDfltSetg other = (CstmOcrVndrDfltSetg) obj;
		if (cmpyId == null) {
			if (other.cmpyId != null)
				return false;
		} else if (!cmpyId.equals(other.cmpyId))
			return false;
		if (venId == null) {
			if (other.venId != null)
				return false;
		} else if (!venId.equals(other.venId))
			return false;
		return true;
	}
	

}
