package com.star.modal;

import java.io.Serializable;
import java.util.Date;

public class CstmVenBnkInfo implements Serializable{

	private String cmpyId;
	private String venId;
	private String bnkNm;
	private String bnkAccNo;
	private String bnkKey;
	private String extRef;
	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}
	public String getVenId() {
		return venId;
	}
	public void setVenId(String venId) {
		this.venId = venId;
	}
	public String getBnkNm() {
		return bnkNm;
	}
	public void setBnkNm(String bnkNm) {
		this.bnkNm = bnkNm;
	}
	public String getBnkAccNo() {
		return bnkAccNo;
	}
	public void setBnkAccNo(String bnkAccNo) {
		this.bnkAccNo = bnkAccNo;
	}
	public String getBnkKey() {
		return bnkKey;
	}
	public void setBnkKey(String bnkKey) {
		this.bnkKey = bnkKey;
	}
	public String getExtRef() {
		return extRef;
	}
	public void setExtRef(String extRef) {
		this.extRef = extRef;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bnkAccNo == null) ? 0 : bnkAccNo.hashCode());
		result = prime * result + ((cmpyId == null) ? 0 : cmpyId.hashCode());
		result = prime * result + ((venId == null) ? 0 : venId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CstmVenBnkInfo other = (CstmVenBnkInfo) obj;
		if (bnkAccNo == null) {
			if (other.bnkAccNo != null)
				return false;
		} else if (!bnkAccNo.equals(other.bnkAccNo))
			return false;
		if (cmpyId == null) {
			if (other.cmpyId != null)
				return false;
		} else if (!cmpyId.equals(other.cmpyId))
			return false;
		if (venId == null) {
			if (other.venId != null)
				return false;
		} else if (!venId.equals(other.venId))
			return false;
		return true;
	}
	
	
}
