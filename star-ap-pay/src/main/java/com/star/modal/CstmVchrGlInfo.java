package com.star.modal;

import java.io.Serializable;

public class CstmVchrGlInfo implements Serializable{

	private int  vchrCtlNo ;
	private String vchrAcct ;
	private String vchrSubAcct;
	private double vchrDrAmt;
	private double vchrCrAmt;
	private String vchrRmk;
    
    public int getVchrCtlNo() {
		return vchrCtlNo;
	}

	public String getVchrAcct() {
		return vchrAcct;
	}

	public void setVchrAcct(String vchrAcct) {
		this.vchrAcct = vchrAcct;
	}

	public String getVchrSubAcct() {
		return vchrSubAcct;
	}

	public void setVchrSubAcct(String vchrSubAcct) {
		this.vchrSubAcct = vchrSubAcct;
	}

	public double getVchrDrAmt() {
		return vchrDrAmt;
	}

	public void setVchrDrAmt(double vchrDrAmt) {
		this.vchrDrAmt = vchrDrAmt;
	}

	public double getVchrCrAmt() {
		return vchrCrAmt;
	}

	public void setVchrCrAmt(double vchrCrAmt) {
		this.vchrCrAmt = vchrCrAmt;
	}

	public String getVchrRmk() {
		return vchrRmk;
	}

	public void setVchrRmk(String vchrRmk) {
		this.vchrRmk = vchrRmk;
	}

	public void setVchrCtlNo(int vchrCtlNo) {
		this.vchrCtlNo = vchrCtlNo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((vchrAcct == null) ? 0 : vchrAcct.hashCode());
		result = prime * result + vchrCtlNo;
		result = prime * result + ((vchrSubAcct == null) ? 0 : vchrSubAcct.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CstmVchrGlInfo other = (CstmVchrGlInfo) obj;
		if (vchrAcct == null) {
			if (other.vchrAcct != null)
				return false;
		} else if (!vchrAcct.equals(other.vchrAcct))
			return false;
		if (vchrCtlNo != other.vchrCtlNo)
			return false;
		if (vchrSubAcct == null) {
			if (other.vchrSubAcct != null)
				return false;
		} else if (!vchrSubAcct.equals(other.vchrSubAcct))
			return false;
		return true;
	}
	
	
	
}
