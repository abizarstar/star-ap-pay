package com.star.modal;

public class ElecPaySeq {

	private int id;
	private int msgIdSeq;
	private int pmtIdAch;
	private int pmtIdWir;
	private int pmtIdChk;
	private int instrIdChk;
	private int instrIdWire;
	private int instrIdACH;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getMsgIdSeq() {
		return msgIdSeq;
	}
	public void setMsgIdSeq(int msgIdSeq) {
		this.msgIdSeq = msgIdSeq;
	}
	public int getPmtIdAch() {
		return pmtIdAch;
	}
	public void setPmtIdAch(int pmtIdAch) {
		this.pmtIdAch = pmtIdAch;
	}
	public int getPmtIdWir() {
		return pmtIdWir;
	}
	public void setPmtIdWir(int pmtIdWir) {
		this.pmtIdWir = pmtIdWir;
	}
	public int getPmtIdChk() {
		return pmtIdChk;
	}
	public void setPmtIdChk(int pmtIdChk) {
		this.pmtIdChk = pmtIdChk;
	}
	public int getInstrIdChk() {
		return instrIdChk;
	}
	public void setInstrIdChk(int instrIdChk) {
		this.instrIdChk = instrIdChk;
	}
	public int getInstrIdWire() {
		return instrIdWire;
	}
	public void setInstrIdWire(int instrIdWire) {
		this.instrIdWire = instrIdWire;
	}
	public int getInstrIdACH() {
		return instrIdACH;
	}
	public void setInstrIdACH(int instrIdACH) {
		this.instrIdACH = instrIdACH;
	}
	
	
	
}
