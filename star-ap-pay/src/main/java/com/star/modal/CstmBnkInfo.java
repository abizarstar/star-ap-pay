package com.star.modal;

public class CstmBnkInfo {
	
	private String bnkCode;
	private String bnkCty;
	private String bnkRoutNo;
	private String bnkNm;
	private String bnkCity;
	private String bnkBrh;
	private String bnkCry;
	private String bnkSwiftCode;
	public String getBnkCode() {
		return bnkCode;
	}
	public void setBnkCode(String bnkCode) {
		this.bnkCode = bnkCode;
	}
	public String getBnkCty() {
		return bnkCty;
	}
	public void setBnkCty(String bnkCty) {
		this.bnkCty = bnkCty;
	}
	public String getBnkRoutNo() {
		return bnkRoutNo;
	}
	public void setBnkRoutNo(String bnkRoutNo) {
		this.bnkRoutNo = bnkRoutNo;
	}
	public String getBnkNm() {
		return bnkNm;
	}
	public void setBnkNm(String bnkNm) {
		this.bnkNm = bnkNm;
	}
	public String getBnkCity() {
		return bnkCity;
	}
	public void setBnkCity(String bnkCity) {
		this.bnkCity = bnkCity;
	}
	public String getBnkBrh() {
		return bnkBrh;
	}
	public void setBnkBrh(String bnkBrh) {
		this.bnkBrh = bnkBrh;
	}
	public String getBnkSwiftCode() {
		return bnkSwiftCode;
	}
	public void setBnkSwiftCode(String bnkSwiftCode) {
		this.bnkSwiftCode = bnkSwiftCode;
	}
	public String getBnkCry() {
		return bnkCry;
	}
	public void setBnkCry(String bnkCry) {
		this.bnkCry = bnkCry;
	}	
	
}

