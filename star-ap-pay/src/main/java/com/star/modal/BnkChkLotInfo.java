package com.star.modal;

import java.io.Serializable;

public class BnkChkLotInfo implements Serializable{

	private String bnkCode;
	private String bnkAcctNo;
	private int chkLotNo;
	private String chkLotInfo;
	private String chkLotStrt;
	private String chkLotEnd;
	private Boolean chkLotUse;
	private Boolean chkLotOpn;
	
	public String getBnkCode() {
		return bnkCode;
	}
	public void setBnkCode(String bnkCode) {
		this.bnkCode = bnkCode;
	}
	public String getBnkAcctNo() {
		return bnkAcctNo;
	}
	public void setBnkAcctNo(String bnkAcctNo) {
		this.bnkAcctNo = bnkAcctNo;
	}
	public int getChkLotNo() {
		return chkLotNo;
	}
	public void setChkLotNo(int chkLotNo) {
		this.chkLotNo = chkLotNo;
	}
	public String getChkLotInfo() {
		return chkLotInfo;
	}
	public void setChkLotInfo(String chkLotInfo) {
		this.chkLotInfo = chkLotInfo;
	}
	public String getChkLotStrt() {
		return chkLotStrt;
	}
	public void setChkLotStrt(String chkLotStrt) {
		this.chkLotStrt = chkLotStrt;
	}
	public String getChkLotEnd() {
		return chkLotEnd;
	}
	public void setChkLotEnd(String chkLotEnd) {
		this.chkLotEnd = chkLotEnd;
	}
	public Boolean getChkLotUse() {
		return chkLotUse;
	}
	public void setChkLotUse(Boolean chkLotUse) {
		this.chkLotUse = chkLotUse;
	}
	public Boolean getChkLotOpn() {
		return chkLotOpn;
	}
	public void setChkLotOpn(Boolean chkLotOpn) {
		this.chkLotOpn = chkLotOpn;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bnkAcctNo == null) ? 0 : bnkAcctNo.hashCode());
		result = prime * result + ((bnkCode == null) ? 0 : bnkCode.hashCode());
		result = prime * result + chkLotNo;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BnkChkLotInfo other = (BnkChkLotInfo) obj;
		if (bnkAcctNo == null) {
			if (other.bnkAcctNo != null)
				return false;
		} else if (!bnkAcctNo.equals(other.bnkAcctNo))
			return false;
		if (bnkCode == null) {
			if (other.bnkCode != null)
				return false;
		} else if (!bnkCode.equals(other.bnkCode))
			return false;
		if (chkLotNo != other.chkLotNo)
			return false;
		return true;
	}
	
	
	
}
