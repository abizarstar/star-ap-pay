package com.star.modal;

import java.io.Serializable;

public class EbsTrnGlDtls implements Serializable{

	private int trnId;
	private int id;
	private int ebsHdrId;
	private int ebsUpldId;
	private String typCode;
	private String glRef;
	private String glRefUpd;

	public int getTrnId() {
		return trnId;
	}
	public void setTrnId(int trnId) {
		this.trnId = trnId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getEbsUpldId() {
		return ebsUpldId;
	}
	public void setEbsUpldId(int ebsUpldId) {
		this.ebsUpldId = ebsUpldId;
	}
	public String getTypCode() {
		return typCode;
	}
	public void setTypCode(String typCode) {
		this.typCode = typCode;
	}
	public int getEbsHdrId() {
		return ebsHdrId;
	}
	public void setEbsHdrId(int ebsHdrId) {
		this.ebsHdrId = ebsHdrId;
	}
	public String getGlRef() {
		return glRef;
	}
	public void setGlRef(String glRef) {
		this.glRef = glRef;
	}
	public String getGlRefUpd() {
		return glRefUpd;
	}
	public void setGlRefUpd(String glRefUpd) {
		this.glRefUpd = glRefUpd;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + trnId;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EbsTrnGlDtls other = (EbsTrnGlDtls) obj;
		if (trnId != other.trnId)
			return false;
		return true;
	}
	
}
