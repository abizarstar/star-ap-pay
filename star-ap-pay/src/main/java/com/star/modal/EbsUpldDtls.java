package com.star.modal;

import java.util.Date;

public class EbsUpldDtls {

	private int id;
	private String flNm;
	private String flOrgNm;
	private String flExt;
	private String upldBy;
	private Date upldOn;
	private Date prsOn;
	private Date crtdDtts;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFlNm() {
		return flNm;
	}
	public void setFlNm(String flNm) {
		this.flNm = flNm;
	}
	public String getFlOrgNm() {
		return flOrgNm;
	}
	public void setFlOrgNm(String flOrgNm) {
		this.flOrgNm = flOrgNm;
	}
	public String getFlExt() {
		return flExt;
	}
	public void setFlExt(String flExt) {
		this.flExt = flExt;
	}
	
	public Date getPrsOn() {
		return prsOn;
	}
	public void setPrsOn(Date prsOn) {
		this.prsOn = prsOn;
	}
	public String getUpldBy() {
		return upldBy;
	}
	public void setUpldBy(String upldBy) {
		this.upldBy = upldBy;
	}
	public Date getUpldOn() {
		return upldOn;
	}
	public void setUpldOn(Date upldOn) {
		this.upldOn = upldOn;
	}
	public Date getCrtdDtts() {
		return crtdDtts;
	}
	public void setCrtdDtts(Date crtdDtts) {
		this.crtdDtts = crtdDtts;
	}
	
}
