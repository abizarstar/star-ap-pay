package com.star.modal;

import java.io.Serializable;
import java.util.Date;

public class CstmVenAddInfo implements Serializable{

	private String cmpyId;
	private String venId;
	private String venNm;
	private String venLongNm;
	private String cty;
	private String cry;
	private String payMthd;
	public String getCrtdBy() {
		return crtdBy;
	}
	public void setCrtdBy(String crtdBy) {
		this.crtdBy = crtdBy;
	}
	public Date getCrtdDtts() {
		return crtdDtts;
	}
	public void setCrtdDtts(Date crtdDtts) {
		this.crtdDtts = crtdDtts;
	}
	private String crtdBy;
	private Date crtdDtts;
	
	
	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}
	public String getVenId() {
		return venId;
	}
	public void setVenId(String venId) {
		this.venId = venId;
	}
	public String getPayMthd() {
		return payMthd;
	}
	public void setPayMthd(String payMthd) {
		this.payMthd = payMthd;
	}
	public String getVenNm() {
		return venNm;
	}
	public void setVenNm(String venNm) {
		this.venNm = venNm;
	}
	public String getVenLongNm() {
		return venLongNm;
	}
	public void setVenLongNm(String venLongNm) {
		this.venLongNm = venLongNm;
	}
	public String getCty() {
		return cty;
	}
	public void setCty(String cty) {
		this.cty = cty;
	}
	public String getCry() {
		return cry;
	}
	public void setCry(String cry) {
		this.cry = cry;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cmpyId == null) ? 0 : cmpyId.hashCode());
		result = prime * result + ((venId == null) ? 0 : venId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CstmVenAddInfo other = (CstmVenAddInfo) obj;
		if (cmpyId == null) {
			if (other.cmpyId != null)
				return false;
		} else if (!cmpyId.equals(other.cmpyId))
			return false;
		if (venId == null) {
			if (other.venId != null)
				return false;
		} else if (!venId.equals(other.venId))
			return false;
		return true;
	}
	
	
}
