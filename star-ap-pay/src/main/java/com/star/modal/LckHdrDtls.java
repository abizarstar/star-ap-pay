package com.star.modal;

import java.io.Serializable;
import java.util.Date;

public class LckHdrDtls implements Serializable {

	private int id;
	private int ebsUpldId;
	private String batchId;
	private String lckboxNo;
	private Date dpstDt;
	private int rcrdCnt;
	private double batchTot;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getEbsUpldId() {
		return ebsUpldId;
	}
	public void setEbsUpldId(int ebsUpldId) {
		this.ebsUpldId = ebsUpldId;
	}
	public String getBatchId() {
		return batchId;
	}
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}
	public String getLckboxNo() {
		return lckboxNo;
	}
	public void setLckboxNo(String lckboxNo) {
		this.lckboxNo = lckboxNo;
	}
	public Date getDpstDt() {
		return dpstDt;
	}
	public void setDpstDt(Date dpstDt) {
		this.dpstDt = dpstDt;
	}
	public int getRcrdCnt() {
		return rcrdCnt;
	}
	public void setRcrdCnt(int rcrdCnt) {
		this.rcrdCnt = rcrdCnt;
	}
	public double getBatchTot() {
		return batchTot;
	}
	public void setBatchTot(double batchTot) {
		this.batchTot = batchTot;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((batchId == null) ? 0 : batchId.hashCode());
		result = prime * result + ebsUpldId;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LckHdrDtls other = (LckHdrDtls) obj;
		if (batchId == null) {
			if (other.batchId != null)
				return false;
		} else if (!batchId.equals(other.batchId))
			return false;
		if (ebsUpldId != other.ebsUpldId)
			return false;
		if (id != other.id)
			return false;
		return true;
	}
	
	
}
