package com.star.modal;

import java.io.Serializable;

public class ChkLotUsage implements Serializable{

	private String bnkCode;
	private String bnkAcctNo;
	private long curLot;
	private String curChk;
	public String getBnkCode() {
		return bnkCode;
	}
	public void setBnkCode(String bnkCode) {
		this.bnkCode = bnkCode;
	}
	public String getBnkAcctNo() {
		return bnkAcctNo;
	}
	public void setBnkAcctNo(String bnkAcctNo) {
		this.bnkAcctNo = bnkAcctNo;
	}
	public long getCurLot() {
		return curLot;
	}
	public void setCurLot(long curLot) {
		this.curLot = curLot;
	}
	public String getCurChk() {
		return curChk;
	}
	public void setCurChk(String curChk) {
		this.curChk = curChk;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bnkAcctNo == null) ? 0 : bnkAcctNo.hashCode());
		result = prime * result + ((bnkCode == null) ? 0 : bnkCode.hashCode());
		result = prime * result + ((curChk == null) ? 0 : curChk.hashCode());
		result = prime * result + (int) (curLot ^ (curLot >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChkLotUsage other = (ChkLotUsage) obj;
		if (bnkAcctNo == null) {
			if (other.bnkAcctNo != null)
				return false;
		} else if (!bnkAcctNo.equals(other.bnkAcctNo))
			return false;
		if (bnkCode == null) {
			if (other.bnkCode != null)
				return false;
		} else if (!bnkCode.equals(other.bnkCode))
			return false;
		if (curChk == null) {
			if (other.curChk != null)
				return false;
		} else if (!curChk.equals(other.curChk))
			return false;
		if (curLot != other.curLot)
			return false;
		return true;
	}
	
	
	
}
