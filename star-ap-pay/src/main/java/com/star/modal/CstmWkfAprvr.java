package com.star.modal;

import java.io.Serializable;

public class CstmWkfAprvr implements Serializable{

	private long stepId;
	private long wkfId;
	private int wkfAprvOrd;
	private String wkfAprvr;
	public long getStepId() {
		return stepId;
	}
	public void setStepId(long stepId) {
		this.stepId = stepId;
	}
	public long getWkfId() {
		return wkfId;
	}
	public void setWkfId(long wkfId) {
		this.wkfId = wkfId;
	}
	public int getWkfAprvOrd() {
		return wkfAprvOrd;
	}
	public void setWkfAprvOrd(int wkfAprvOrd) {
		this.wkfAprvOrd = wkfAprvOrd;
	}
	public String getWkfAprvr() {
		return wkfAprvr;
	}
	public void setWkfAprvr(String wkfAprvr) {
		this.wkfAprvr = wkfAprvr;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (stepId ^ (stepId >>> 32));
		result = prime * result + wkfAprvOrd;
		result = prime * result + ((wkfAprvr == null) ? 0 : wkfAprvr.hashCode());
		result = prime * result + (int) (wkfId ^ (wkfId >>> 32));
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CstmWkfAprvr other = (CstmWkfAprvr) obj;
		if (stepId != other.stepId)
			return false;
		if (wkfAprvOrd != other.wkfAprvOrd)
			return false;
		if (wkfAprvr == null) {
			if (other.wkfAprvr != null)
				return false;
		} else if (!wkfAprvr.equals(other.wkfAprvr))
			return false;
		if (wkfId != other.wkfId)
			return false;
		return true;
	}
	
	
	
}
