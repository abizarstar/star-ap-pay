package com.star.modal;

import java.io.Serializable;
import java.util.Date;

public class CstmPayParams implements Serializable{

	private String reqId;
	private String cmpyId;
	private Date reqDt;
	private String crtdBy;
	private Date crtdOn;
	private String reqUserBy;
	private Date reqUserOn;
	private String reqActnBy;
	private Date reqActnOn;
	private String paySts;
	private String reqActnRmk;
	private String issueDt;
	

	public String getIssueDt() {
		return issueDt;
	}
	public void setIssueDt(String issueDt) {
		this.issueDt = issueDt;
	}
	public String getReqId() {
		return reqId;
	}
	public void setReqId(String reqId) {
		this.reqId = reqId;
	}
	
	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}
	public Date getReqDt() {
		return reqDt;
	}
	public void setReqDt(Date reqDt) {
		this.reqDt = reqDt;
	}
	public String getCrtdBy() {
		return crtdBy;
	}
	public void setCrtdBy(String crtdBy) {
		this.crtdBy = crtdBy;
	}
	public Date getCrtdOn() {
		return crtdOn;
	}
	public void setCrtdOn(Date crtdOn) {
		this.crtdOn = crtdOn;
	}
	public String getReqUserBy() {
		return reqUserBy;
	}
	public void setReqUserBy(String reqUserBy) {
		this.reqUserBy = reqUserBy;
	}
	public Date getReqUserOn() {
		return reqUserOn;
	}
	public void setReqUserOn(Date reqUserOn) {
		this.reqUserOn = reqUserOn;
	}
	public String getReqActnBy() {
		return reqActnBy;
	}
	public void setReqActnBy(String reqActnBy) {
		this.reqActnBy = reqActnBy;
	}
	public Date getReqActnOn() {
		return reqActnOn;
	}
	public void setReqActnOn(Date reqActnOn) {
		this.reqActnOn = reqActnOn;
	}
	public String getPaySts() {
		return paySts;
	}
	public void setPaySts(String paySts) {
		this.paySts = paySts;
	}
	public String getReqActnRmk() {
		return reqActnRmk;
	}
	public void setReqActnRmk(String reqActnRmk) {
		this.reqActnRmk = reqActnRmk;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cmpyId == null) ? 0 : cmpyId.hashCode());
		result = prime * result + ((reqId == null) ? 0 : reqId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CstmPayParams other = (CstmPayParams) obj;
		if (cmpyId == null) {
			if (other.cmpyId != null)
				return false;
		} else if (!cmpyId.equals(other.cmpyId))
			return false;
		if (reqId == null) {
			if (other.reqId != null)
				return false;
		} else if (!reqId.equals(other.reqId))
			return false;
		return true;
	}
	
	
}
