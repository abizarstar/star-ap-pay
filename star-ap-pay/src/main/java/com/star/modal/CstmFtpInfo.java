package com.star.modal;

import java.io.Serializable;

public class CstmFtpInfo  implements Serializable{

	private String bnkCode;
	private String bnkHost;
	private String bnkPort;
	private String bnkFtpUser;
	private String bnkFtpPass;
	private String bnkKeyPath;
	private String bnkFtpLoc;
	private String bnkFtpTyp;
	private boolean active;
	
	public String getBnkCode() {
		return bnkCode;
	}
	public void setBnkCode(String bnkCode) {
		this.bnkCode = bnkCode;
	}
	public String getBnkHost() {
		return bnkHost;
	}
	public void setBnkHost(String bnkHost) {
		this.bnkHost = bnkHost;
	}
	public String getBnkPort() {
		return bnkPort;
	}
	public void setBnkPort(String bnkPort) {
		this.bnkPort = bnkPort;
	}
	public String getBnkFtpUser() {
		return bnkFtpUser;
	}
	public void setBnkFtpUser(String bnkFtpUser) {
		this.bnkFtpUser = bnkFtpUser;
	}
	public String getBnkFtpPass() {
		return bnkFtpPass;
	}
	public void setBnkFtpPass(String bnkFtpPass) {
		this.bnkFtpPass = bnkFtpPass;
	}
	public String getBnkKeyPath() {
		return bnkKeyPath;
	}
	public void setBnkKeyPath(String bnkKeyPath) {
		this.bnkKeyPath = bnkKeyPath;
	}
	public String getBnkFtpLoc() {
		return bnkFtpLoc;
	}
	public void setBnkFtpLoc(String bnkFtpLoc) {
		this.bnkFtpLoc = bnkFtpLoc;
	}
	public String getBnkFtpTyp() {
		return bnkFtpTyp;
	}
	public void setBnkFtpTyp(String bnkFtpTyp) {
		this.bnkFtpTyp = bnkFtpTyp;
	}
	public boolean getActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bnkCode == null) ? 0 : bnkCode.hashCode());
		result = prime * result + ((bnkFtpTyp == null) ? 0 : bnkFtpTyp.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CstmFtpInfo other = (CstmFtpInfo) obj;
		if (bnkCode == null) {
			if (other.bnkCode != null)
				return false;
		} else if (!bnkCode.equals(other.bnkCode))
			return false;
		if (bnkFtpTyp == null) {
			if (other.bnkFtpTyp != null)
				return false;
		} else if (!bnkFtpTyp.equals(other.bnkFtpTyp))
			return false;
		return true;
	}
	
	
	
}
