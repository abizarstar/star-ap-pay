package com.star.modal;

import java.io.Serializable;

public class UsrAprConAmt implements Serializable {

	private String usrId;
	private double frmAmt;
	private double toAmt;
	public String getUsrId() {
		return usrId;
	}
	public void setUsrId(String usrId) {
		this.usrId = usrId;
	}
	public double getFrmAmt() {
		return frmAmt;
	}
	public void setFrmAmt(double frmAmt) {
		this.frmAmt = frmAmt;
	}
	public double getToAmt() {
		return toAmt;
	}
	public void setToAmt(double toAmt) {
		this.toAmt = toAmt;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(frmAmt);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(toAmt);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((usrId == null) ? 0 : usrId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UsrAprConAmt other = (UsrAprConAmt) obj;
		if (Double.doubleToLongBits(frmAmt) != Double.doubleToLongBits(other.frmAmt))
			return false;
		if (Double.doubleToLongBits(toAmt) != Double.doubleToLongBits(other.toAmt))
			return false;
		if (usrId == null) {
			if (other.usrId != null)
				return false;
		} else if (!usrId.equals(other.usrId))
			return false;
		return true;
	}
	
	
	
}
