package com.star.modal;

import java.io.Serializable;

public class CstmVchrReconInfo implements Serializable{

	private int  vchrCtlNo ;
	private int vchrCrcnNo ;
	private String vchrTrsNo;
	private String vchrPoNo;
	private int vchrCstNo;
	private double vchrBalAmt;
	private String vchrVenId;
    
    public int getVchrCtlNo() {
		return vchrCtlNo;
	}

	public int getVchrCrcnNo() {
		return vchrCrcnNo;
	}

	public void setVchrCrcnNo(int vchrCrcnNo) {
		this.vchrCrcnNo = vchrCrcnNo;
	}

	public String getVchrTrsNo() {
		return vchrTrsNo;
	}

	public void setVchrTrsNo(String vchrTrsNo) {
		this.vchrTrsNo = vchrTrsNo;
	}

	public String getVchrPoNo() {
		return vchrPoNo;
	}

	public void setVchrPoNo(String vchrPoNo) {
		this.vchrPoNo = vchrPoNo;
	}

	public int getVchrCstNo() {
		return vchrCstNo;
	}

	public void setVchrCstNo(int vchrCstNo) {
		this.vchrCstNo = vchrCstNo;
	}

	public double getVchrBalAmt() {
		return vchrBalAmt;
	}

	public void setVchrBalAmt(double vchrBalAmt) {
		this.vchrBalAmt = vchrBalAmt;
	}

	public void setVchrCtlNo(int vchrCtlNo) {
		this.vchrCtlNo = vchrCtlNo;
	}

	public String getVchrVenId() {
		return vchrVenId;
	}

	public void setVchrVenId(String vchrVenId) {
		this.vchrVenId = vchrVenId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + vchrCrcnNo;
		result = prime * result + vchrCtlNo;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CstmVchrReconInfo other = (CstmVchrReconInfo) obj;
		if (vchrCrcnNo != other.vchrCrcnNo)
			return false;
		if (vchrCtlNo != other.vchrCtlNo)
			return false;
		return true;
	}

	
	
}
