package com.star.modal;

import java.io.Serializable;

public class CstmOcrVndrGlSetg implements Serializable{

	private String cmpyId;
	private String venId;
	private String bscGlAcct;
	private String sacct;
	private String sacctTxt;
	private String distRmk;
	
	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}
	
	public String getVenId() {
		return venId;
	}
	public void setVenId(String venId) {
		this.venId = venId;
	}
	
	public String getBscGlAcct() {
		return bscGlAcct;
	}
	public void setBscGlAcct(String bscGlAcct) {
		this.bscGlAcct = bscGlAcct;
	}
	
	public String getSacct() {
		return sacct;
	}
	public void setSacct(String sacct) {
		this.sacct = sacct;
	}
	
	public String getSacctTxt() {
		return sacctTxt;
	}
	public void setSacctTxt(String sacctTxt) {
		this.sacctTxt = sacctTxt;
	}
	
	public String getDistRmk() {
		return distRmk;
	}
	public void setDistRmk(String distRmk) {
		this.distRmk = distRmk;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bscGlAcct == null) ? 0 : bscGlAcct.hashCode());
		result = prime * result + ((cmpyId == null) ? 0 : cmpyId.hashCode());
		result = prime * result + ((sacct == null) ? 0 : sacct.hashCode());
		result = prime * result + ((venId == null) ? 0 : venId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CstmOcrVndrGlSetg other = (CstmOcrVndrGlSetg) obj;
		if (bscGlAcct == null) {
			if (other.bscGlAcct != null)
				return false;
		} else if (!bscGlAcct.equals(other.bscGlAcct))
			return false;
		if (cmpyId == null) {
			if (other.cmpyId != null)
				return false;
		} else if (!cmpyId.equals(other.cmpyId))
			return false;
		if (sacct == null) {
			if (other.sacct != null)
				return false;
		} else if (!sacct.equals(other.sacct))
			return false;
		if (venId == null) {
			if (other.venId != null)
				return false;
		} else if (!venId.equals(other.venId))
			return false;
		return true;
	}
	
	
}
