package com.star.modal;

public class CstmOcrVenMap {

	private String venNm;
	private String venNmStx;
	private String venId;
	private String venInvNoFld;
	private String venInvDtFld;
	private String venAmtFld;
	private String venCryFld;
	private String venDueDtFld;
	private int invPos;
	
	public String getVenNm() {
		return venNm;
	}
	public void setVenNm(String venNm) {
		this.venNm = venNm;
	}
	public String getVenNmStx() {
		return venNmStx;
	}
	public void setVenNmStx(String venNmStx) {
		this.venNmStx = venNmStx;
	}
	public String getVenId() {
		return venId;
	}
	public void setVenId(String venId) {
		this.venId = venId;
	}
	public String getVenInvNoFld() {
		return venInvNoFld;
	}
	public void setVenInvNoFld(String venInvNoFld) {
		this.venInvNoFld = venInvNoFld;
	}
	public String getVenInvDtFld() {
		return venInvDtFld;
	}
	public void setVenInvDtFld(String venInvDtFld) {
		this.venInvDtFld = venInvDtFld;
	}
	public String getVenAmtFld() {
		return venAmtFld;
	}
	public void setVenAmtFld(String venAmtFld) {
		this.venAmtFld = venAmtFld;
	}
	public String getVenCryFld() {
		return venCryFld;
	}
	public void setVenCryFld(String venCryFld) {
		this.venCryFld = venCryFld;
	}
	public String getVenDueDtFld() {
		return venDueDtFld;
	}
	public void setVenDueDtFld(String venDueDtFld) {
		this.venDueDtFld = venDueDtFld;
	}
	public int getInvPos() {
		return invPos;
	}
	public void setInvPos(int invPos) {
		this.invPos = invPos;
	}
	
	
	
}
