package com.star.modal;

import java.io.Serializable;
import java.util.Date;

public class AppLogInfo implements Serializable {

	private int logId;
	private String logUriPath;
	private String logReq;
	private String logUsr;
	private Date logDtts;
	public int getLogId() {
		return logId;
	}
	public void setLogId(int logId) {
		this.logId = logId;
	}
	public String getLogUriPath() {
		return logUriPath;
	}
	public void setLogUriPath(String logUriPath) {
		this.logUriPath = logUriPath;
	}
	public String getLogReq() {
		return logReq;
	}
	public void setLogReq(String logReq) {
		this.logReq = logReq;
	}
	public String getLogUsr() {
		return logUsr;
	}
	public void setLogUsr(String logUsr) {
		this.logUsr = logUsr;
	}
	public Date getLogDtts() {
		return logDtts;
	}
	public void setLogDtts(Date logDtts) {
		this.logDtts = logDtts;
	} 
	
	
	
}
