package com.star.modal;

import java.io.Serializable;
import java.util.Date;

public class CstmParamInv implements Serializable{

	private String reqId;
	private String invNo;
	private boolean invSts;
	private int vchrPayMthd;
    private String notes;
    private String chkNo;
    private Date chkVdOn;
    private String chkVdBy;
    private String chkVdRmk;
    private String glEntrySts;

	public String getReqId() {
		return reqId;
	}
	public void setReqId(String reqId) {
		this.reqId = reqId;
	}
	
	
	public String getInvNo() {
		return invNo;
	}
	public void setInvNo(String invNo) {
		this.invNo = invNo;
	}
	public boolean isInvSts() {
		return invSts;
	}
	public void setInvSts(boolean invSts) {
		this.invSts = invSts;
	}
	
	public int getVchrPayMthd() {
		return vchrPayMthd;
	}
	public void setVchrPayMthd(int vchrPayMthd) {
		this.vchrPayMthd = vchrPayMthd;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getChkNo() {
		return chkNo;
	}
	public void setChkNo(String chkNo) {
		this.chkNo = chkNo;
	}
	public Date getChkVdOn() {
		return chkVdOn;
	}
	public void setChkVdOn(Date chkVdOn) {
		this.chkVdOn = chkVdOn;
	}
	public String getChkVdBy() {
		return chkVdBy;
	}
	public void setChkVdBy(String chkVdBy) {
		this.chkVdBy = chkVdBy;
	}
	public String getChkVdRmk() {
		return chkVdRmk;
	}
	public void setChkVdRmk(String chkVdRmk) {
		this.chkVdRmk = chkVdRmk;
	}
	public String getGlEntrySts() {
		return glEntrySts;
	}
	public void setGlEntrySts(String glEntrySts) {
		this.glEntrySts = glEntrySts;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((invNo == null) ? 0 : invNo.hashCode());
		result = prime * result + ((reqId == null) ? 0 : reqId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CstmParamInv other = (CstmParamInv) obj;
		if (invNo == null) {
			if (other.invNo != null)
				return false;
		} else if (!invNo.equals(other.invNo))
			return false;
		if (reqId == null) {
			if (other.reqId != null)
				return false;
		} else if (!reqId.equals(other.reqId))
			return false;
		return true;
	}
	
	
}
