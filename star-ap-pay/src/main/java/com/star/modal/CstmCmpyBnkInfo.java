package com.star.modal;

import java.io.Serializable;

public class CstmCmpyBnkInfo implements Serializable{

	private String cmpyId;
	private String bnkCode;
	
	public String getBnkCode() {
		return bnkCode;
	}
	public void setBnkCode(String bnkCode) {
		this.bnkCode = bnkCode;
	}
	
	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bnkCode == null) ? 0 : bnkCode.hashCode());
		result = prime * result + ((cmpyId == null) ? 0 : cmpyId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CstmCmpyBnkInfo other = (CstmCmpyBnkInfo) obj;
		if (bnkCode == null) {
			if (other.bnkCode != null)
				return false;
		} else if (!bnkCode.equals(other.bnkCode))
			return false;
		if (cmpyId == null) {
			if (other.cmpyId != null)
				return false;
		} else if (!cmpyId.equals(other.cmpyId))
			return false;
		return true;
	}
	
	
	
}
