package com.star.modal;

import java.io.Serializable;

public class EbsHdrDtls implements Serializable {

	private int id;
	private int ebsUpldId;
	private String ebsRcvrId;
	private double opnBal;
	private double clsBal;
	private double avlblClsBal;
	private double totCrdTrn;
	private int crdTrnCnt;
	private double totDebTrn;
	private int debTrnCnt;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getEbsUpldId() {
		return ebsUpldId;
	}
	public void setEbsUpldId(int ebsUpldId) {
		this.ebsUpldId = ebsUpldId;
	}
	public String getEbsRcvrId() {
		return ebsRcvrId;
	}
	public void setEbsRcvrId(String ebsRcvrId) {
		this.ebsRcvrId = ebsRcvrId;
	}
	public double getOpnBal() {
		return opnBal;
	}
	public void setOpnBal(double opnBal) {
		this.opnBal = opnBal;
	}
	public double getClsBal() {
		return clsBal;
	}
	public void setClsBal(double clsBal) {
		this.clsBal = clsBal;
	}
	public double getAvlblClsBal() {
		return avlblClsBal;
	}
	public void setAvlblClsBal(double avlblClsBal) {
		this.avlblClsBal = avlblClsBal;
	}
	public double getTotCrdTrn() {
		return totCrdTrn;
	}
	public void setTotCrdTrn(double totCrdTrn) {
		this.totCrdTrn = totCrdTrn;
	}
	public int getCrdTrnCnt() {
		return crdTrnCnt;
	}
	public void setCrdTrnCnt(int crdTrnCnt) {
		this.crdTrnCnt = crdTrnCnt;
	}
	public double getTotDebTrn() {
		return totDebTrn;
	}
	public void setTotDebTrn(double totDebTrn) {
		this.totDebTrn = totDebTrn;
	}
	public int getDebTrnCnt() {
		return debTrnCnt;
	}
	public void setDebTrnCnt(int debTrnCnt) {
		this.debTrnCnt = debTrnCnt;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ebsRcvrId == null) ? 0 : ebsRcvrId.hashCode());
		result = prime * result + ebsUpldId;
		result = prime * result + id;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EbsHdrDtls other = (EbsHdrDtls) obj;
		if (ebsRcvrId == null) {
			if (other.ebsRcvrId != null)
				return false;
		} else if (!ebsRcvrId.equals(other.ebsRcvrId))
			return false;
		if (ebsUpldId != other.ebsUpldId)
			return false;
		if (id != other.id)
			return false;
		return true;
	}
	
	
	
	
}
