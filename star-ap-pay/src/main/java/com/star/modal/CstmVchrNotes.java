package com.star.modal;

import java.util.Date;

public class CstmVchrNotes {
	
	private int notesId;
	private int ctlNo;
	private String crtdBy;
	private Date crtdOn;
	private String note;
	public int getNotesId() {
		return notesId;
	}
	public void setNotesId(int notesId) {
		this.notesId = notesId;
	}
	public int getCtlNo() {
		return ctlNo;
	}
	public void setCtlNo(int ctlNo) {
		this.ctlNo = ctlNo;
	}
	public String getCrtdBy() {
		return crtdBy;
	}
	public void setCrtdBy(String crtdBy) {
		this.crtdBy = crtdBy;
	}
	public Date getCrtdOn() {
		return crtdOn;
	}
	public void setCrtdOn(Date crtdOn) {
		this.crtdOn = crtdOn;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
	
	
	

	
}
