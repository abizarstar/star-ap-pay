package com.star.modal;

import java.io.Serializable;

public class VenPayMthd implements Serializable{

	private String cmpyId;
	private String venId;
	private Integer pmtMthdId;
	private Boolean pmtDflt;
	
	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}
	
	public String getVenId() {
		return venId;
	}
	public void setVenId(String venId) {
		this.venId = venId;
	}
	
	public Integer getPmtMthdId() {
		return pmtMthdId;
	}
	public void setPmtMthdId(Integer pmtMthdId) {
		this.pmtMthdId = pmtMthdId;
	}
	
	public Boolean getPmtDflt() {
		return pmtDflt;
	}
	public void setPmtDflt(Boolean pmtDflt) {
		this.pmtDflt = pmtDflt;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cmpyId == null) ? 0 : cmpyId.hashCode());
		result = prime * result + ((pmtMthdId == null) ? 0 : pmtMthdId.hashCode());
		result = prime * result + ((venId == null) ? 0 : venId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		VenPayMthd other = (VenPayMthd) obj;
		if (cmpyId == null) {
			if (other.cmpyId != null)
				return false;
		} else if (!cmpyId.equals(other.cmpyId))
			return false;
		if (pmtMthdId == null) {
			if (other.pmtMthdId != null)
				return false;
		} else if (!pmtMthdId.equals(other.pmtMthdId))
			return false;
		if (venId == null) {
			if (other.venId != null)
				return false;
		} else if (!venId.equals(other.venId))
			return false;
		return true;
	}
	
	
	
}
