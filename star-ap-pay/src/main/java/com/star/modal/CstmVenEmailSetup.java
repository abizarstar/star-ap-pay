package com.star.modal;

import java.io.Serializable;
import java.util.Date;

public class CstmVenEmailSetup implements Serializable{

	
	private String venId;
	private String subject;
	private String body;
	
	public void setVenId(String venId) {
		this.venId = venId;
	}
	public String getVenId() {
		return venId;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((venId == null) ? 0 : venId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CstmVenEmailSetup other = (CstmVenEmailSetup) obj;
		
		if (venId == null) {
			if (other.venId != null)
				return false;
		} else if (!venId.equals(other.venId))
			return false;
		return true;
	}
	
	
	
}
