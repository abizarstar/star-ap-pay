package com.star.modal;

import java.io.Serializable;

public class CstmBnkAcctInfo implements Serializable{

	private String bnkCode;
	private String bnkAcctId;
	private String bnkAcctDesc;
	private String bnkAcctNo;
	private String bnkAchId;
	private String bnkMmbId;
	private boolean dflValue;
	
	public String getBnkCode() {
		return bnkCode;
	}
	public void setBnkCode(String bnkCode) {
		this.bnkCode = bnkCode;
	}
	
	public String getBnkAcctId() {
		return bnkAcctId;
	}
	public void setBnkAcctId(String bnkAcctId) {
		this.bnkAcctId = bnkAcctId;
	}
	public String getBnkAcctDesc() {
		return bnkAcctDesc;
	}
	public void setBnkAcctDesc(String bnkAcctDesc) {
		this.bnkAcctDesc = bnkAcctDesc;
	}
	public String getBnkAcctNo() {
		return bnkAcctNo;
	}
	public void setBnkAcctNo(String bnkAcctNo) {
		this.bnkAcctNo = bnkAcctNo;
	}
	public boolean isDflValue() {
		return dflValue;
	}
	public void setDflValue(boolean dflValue) {
		this.dflValue = dflValue;
	}
	public String getBnkAchId() {
		return bnkAchId;
	}
	public void setBnkAchId(String bnkAchId) {
		this.bnkAchId = bnkAchId;
	}
	public String getBnkMmbId() {
		return bnkMmbId;
	}
	public void setBnkMmbId(String bnkMmbId) {
		this.bnkMmbId = bnkMmbId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bnkAcctNo == null) ? 0 : bnkAcctNo.hashCode());
		result = prime * result + ((bnkCode == null) ? 0 : bnkCode.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CstmBnkAcctInfo other = (CstmBnkAcctInfo) obj;
		if (bnkAcctNo == null) {
			if (other.bnkAcctNo != null)
				return false;
		} else if (!bnkAcctNo.equals(other.bnkAcctNo))
			return false;
		if (bnkCode == null) {
			if (other.bnkCode != null)
				return false;
		} else if (!bnkCode.equals(other.bnkCode))
			return false;
		return true;
	}
	
	
	
}
