package com.star.modal;

import java.io.Serializable;

import javax.persistence.Entity;

@Entity
public class Db implements Serializable {
/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
String driver;
String url;
String usr_nm;
String pass;
String  con_typ;
public String getUrl() {
	return url;
}
public String getDriver() {
	return driver;
}
public void setDriver(String driver) {
	this.driver = driver;
}
public void setUrl(String url) {
	this.url = url;
}
public String getUsr_nm() {
	return usr_nm;
}
public void setUsr_nm(String usr_nm) {
	this.usr_nm = usr_nm;
}
public String getPass() {
	return pass;
}
public void setPass(String pass) {
	this.pass = pass;
}
public String getCon_typ() {
	return con_typ;
}
public void setCon_typ(String con_typ) {
	this.con_typ = con_typ;
}

}

