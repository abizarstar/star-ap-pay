package com.star.modal;

import java.util.Date;

public class UsrEmlConfigDtls {

	private String usrEml;
	private String accessToken;
	private String refreshToekn;
	private String crtdBy;
	private Date crtdOn;
	private boolean sts;
	private long lstRfshTm;
	private String chkSum;
	private String lgnTyp;
	
	
	
	
	public String getChkSum() {
		return chkSum;
	}
	public void setChkSum(String chkSum) {
		this.chkSum = chkSum;
	}
	public String getLgnTyp() {
		return lgnTyp;
	}
	public void setLgnTyp(String lgnTyp) {
		this.lgnTyp = lgnTyp;
	}
	public String getUsrEml() {
		return usrEml;
	}
	public void setUsrEml(String usrEml) {
		this.usrEml = usrEml;
	}
	public String getAccessToken() {
		return accessToken;
	}
	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}
	public String getRefreshToekn() {
		return refreshToekn;
	}
	public void setRefreshToekn(String refreshToekn) {
		this.refreshToekn = refreshToekn;
	}
	public String getCrtdBy() {
		return crtdBy;
	}
	public void setCrtdBy(String crtdBy) {
		this.crtdBy = crtdBy;
	}
	public Date getCrtdOn() {
		return crtdOn;
	}
	public void setCrtdOn(Date crtdOn) {
		this.crtdOn = crtdOn;
	}
	public boolean isSts() {
		return sts;
	}
	public void setSts(boolean sts) {
		this.sts = sts;
	}
	public long getLstRfshTm() {
		return lstRfshTm;
	}
	public void setLstRfshTm(long lstRfshTm) {
		this.lstRfshTm = lstRfshTm;
	}
	
	
}