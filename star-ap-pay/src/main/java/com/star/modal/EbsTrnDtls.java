package com.star.modal;

import java.io.Serializable;

public class EbsTrnDtls implements Serializable{

	private int id;
	private int ebsHdrId;
	private int ebsUpldId;
	private String typCode;
	private double trnAmount;
	private String trnTyp;	
	private String bnkRef;
	private String custRef;
	private String adtnRef;
	private String erpRef;
	private Boolean isPrs; 
	private String errMsg;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getEbsUpldId() {
		return ebsUpldId;
	}
	public void setEbsUpldId(int ebsUpldId) {
		this.ebsUpldId = ebsUpldId;
	}
	public String getTypCode() {
		return typCode;
	}
	public void setTypCode(String typCode) {
		this.typCode = typCode;
	}
	public double getTrnAmount() {
		return trnAmount;
	}
	public void setTrnAmount(double trnAmount) {
		this.trnAmount = trnAmount;
	}
	
	
	
	public String getBnkRef() {
		return bnkRef;
	}
	public void setBnkRef(String bnkRef) {
		this.bnkRef = bnkRef;
	}
	public String getCustRef() {
		return custRef;
	}
	public void setCustRef(String custRef) {
		this.custRef = custRef;
	}
	public String getAdtnRef() {
		return adtnRef;
	}
	public void setAdtnRef(String adtnRef) {
		this.adtnRef = adtnRef;
	}
	public int getEbsHdrId() {
		return ebsHdrId;
	}
	public void setEbsHdrId(int ebsHdrId) {
		this.ebsHdrId = ebsHdrId;
	}
	public String getTrnTyp() {
		return trnTyp;
	}
	public void setTrnTyp(String trnTyp) {
		this.trnTyp = trnTyp;
	}
	public String getErpRef() {
		return erpRef;
	}
	public void setErpRef(String erpRef) {
		this.erpRef = erpRef;
	}
	public Boolean getIsPrs() {
		return isPrs;
	}
	public void setIsPrs(Boolean isPrs) {
		this.isPrs = isPrs;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ebsHdrId;
		result = prime * result + ebsUpldId;
		result = prime * result + id;
		result = prime * result + ((typCode == null) ? 0 : typCode.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EbsTrnDtls other = (EbsTrnDtls) obj;
		if (ebsHdrId != other.ebsHdrId)
			return false;
		if (ebsUpldId != other.ebsUpldId)
			return false;
		if (id != other.id)
			return false;
		if (typCode == null) {
			if (other.typCode != null)
				return false;
		} else if (!typCode.equals(other.typCode))
			return false;
		return true;
	}
	
	
	
}
