package com.star.modal;

import java.io.Serializable;
import java.util.Date;

public class ChkVoidInfo implements Serializable{

	private String bnkCode;
	private String bnkAcctNo;
	private String chkNo;
	private Date chkVdOn;
	private String chkVdBy;
	private String chkVdRmk;
	
	public String getBnkCode() {
		return bnkCode;
	}
	public void setBnkCode(String bnkCode) {
		this.bnkCode = bnkCode;
	}
	public String getBnkAcctNo() {
		return bnkAcctNo;
	}
	public void setBnkAcctNo(String bnkAcctNo) {
		this.bnkAcctNo = bnkAcctNo;
	}
	public String getChkNo() {
		return chkNo;
	}
	public void setChkNo(String chkNo) {
		this.chkNo = chkNo;
	}
	
	public Date getChkVdOn() {
		return chkVdOn;
	}
	public void setChkVdOn(Date chkVdOn) {
		this.chkVdOn = chkVdOn;
	}
	public String getChkVdBy() {
		return chkVdBy;
	}
	public void setChkVdBy(String chkVdBy) {
		this.chkVdBy = chkVdBy;
	}
	public String getChkVdRmk() {
		return chkVdRmk;
	}
	public void setChkVdRmk(String chkVdRmk) {
		this.chkVdRmk = chkVdRmk;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bnkAcctNo == null) ? 0 : bnkAcctNo.hashCode());
		result = prime * result + ((bnkCode == null) ? 0 : bnkCode.hashCode());
		result = prime * result + ((chkNo == null) ? 0 : chkNo.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ChkVoidInfo other = (ChkVoidInfo) obj;
		if (bnkAcctNo == null) {
			if (other.bnkAcctNo != null)
				return false;
		} else if (!bnkAcctNo.equals(other.bnkAcctNo))
			return false;
		if (bnkCode == null) {
			if (other.bnkCode != null)
				return false;
		} else if (!bnkCode.equals(other.bnkCode))
			return false;
		if (chkNo == null) {
			if (other.chkNo != null)
				return false;
		} else if (!chkNo.equals(other.chkNo))
			return false;
		return true;
	}
	
	
	
}
