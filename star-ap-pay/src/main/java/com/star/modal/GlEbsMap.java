package com.star.modal;

import java.io.Serializable;

public class GlEbsMap implements Serializable{

	private Integer ebsId;
	private String typCd;
	private String post1;
	private String post2;
	
	public Integer getEbsId() {
		return ebsId;
	}
	public void setEbsId(Integer ebsId) {
		this.ebsId = ebsId;
	}
	
	public String getTypCd() {
		return typCd;
	}
	public void setTypCd(String typCd) {
		this.typCd = typCd;
	}
	public String getPost1() {
		return post1;
	}
	public void setPost1(String post1) {
		this.post1 = post1;
	}
	public String getPost2() {
		return post2;
	}
	public void setPost2(String post2) {
		this.post2 = post2;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((ebsId == null) ? 0 : ebsId.hashCode());
		result = prime * result + ((typCd == null) ? 0 : typCd.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		GlEbsMap other = (GlEbsMap) obj;
		if (ebsId == null) {
			if (other.ebsId != null)
				return false;
		} else if (!ebsId.equals(other.ebsId))
			return false;
		if (typCd == null) {
			if (other.typCd != null)
				return false;
		} else if (!typCd.equals(other.typCd))
			return false;
		return true;
	}
	
	
}
