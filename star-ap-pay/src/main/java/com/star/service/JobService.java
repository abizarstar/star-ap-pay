package com.star.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.dao.Sechdule;
import com.star.dao.WorkflowDAO;
import com.star.linkage.job.JobBrowseOutput;

@Path("/job")
public class JobService {

	@POST
	@Secured
	@Path("/new")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response createJob(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data)
			throws Exception {
		BrowseResponse<JobBrowseOutput> starBrowseResponse = new BrowseResponse<JobBrowseOutput>();
		starBrowseResponse.setOutput(new JobBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String dt = jsonObject.get("sdlrdt").getAsString();
		Sechdule sd = new Sechdule();
		String [] dts=dt.split("/");
		String m= dts[0];
		String d=dts[1];
		String y=dts[2];
		String cron = "0/2 * * "+d+" "+m+" ? "+y;
		sd.job(starBrowseResponse, cron);
		System.out.println("----------- input  -- " + cron + "------------");
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTbljb.size();
		return starResponseBuilder.getSuccessResponse(getBrowseOutputWorkflow(starBrowseResponse));
	}

	private GenericEntity<?> getBrowseOutputWorkflow(BrowseResponse<JobBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<JobBrowseOutput>>(starBrowseResponse) {
		};
	}
}
