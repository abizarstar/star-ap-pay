package com.star.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import org.jxls.common.Context;
import org.jxls.util.JxlsHelper;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.MaintanenceResponse;
import com.star.common.ManageDirectory;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.dao.ARInfoDAO;
import com.star.linkage.ar.ARDesboardBrowseOutput;
import com.star.linkage.ar.AREnquiryBrowseOutput;
import com.star.linkage.ar.ARPaymentBrowseOutput;
import com.star.linkage.ar.ARPmtRecvDataBrowseOutput;
import com.star.linkage.ar.AdvancePaymentBrowseOutput;
import com.star.linkage.ar.CashReceiptBrowseOutput;
import com.star.linkage.ar.GLDistributionBrowseOutput;
import com.star.linkage.ar.SessionBrowseOutput;
import com.star.linkage.ar.UnappliedDebitBrowseOutput;
import com.star.linkage.ar.ValidateDiscountDateManOutput;

@Path("/ar")
public class ARService {

	@POST
	@Secured
	@Path("/read-ssn")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getSessionRecords(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BrowseResponse<SessionBrowseOutput> starBrowseResponse = new BrowseResponse<SessionBrowseOutput>();
		starBrowseResponse.setOutput(new SessionBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String ssnId = jsonObject.get("ssnId").getAsString();
		String crcpNo = jsonObject.get("crcpNo").getAsString();
		String cusId = jsonObject.get("cusId").getAsString();

		ARInfoDAO arInfo = new ARInfoDAO();

		arInfo.readSessions(starBrowseResponse, cmpyId, ssnId, crcpNo, cusId);
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblSsn.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));
	}

	private GenericEntity<?> getGenericBrowseOutput(BrowseResponse<SessionBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<SessionBrowseOutput>>(starBrowseResponse) {
		};
	}

	@POST
	@Secured
	@Path("/read-cashRcpt")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getCashReceipt(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<CashReceiptBrowseOutput> starBrowseResponse = new BrowseResponse<CashReceiptBrowseOutput>();
		starBrowseResponse.setOutput(new CashReceiptBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String ssnId = jsonObject.get("ssnId").getAsString();

		ARInfoDAO arInfo = new ARInfoDAO();

		arInfo.readCashReceipt(starBrowseResponse, ssnId);
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblCashRcpt.size();

		return starResponseBuilder.getSuccessResponse(getCashReceiptBrowseOutput(starBrowseResponse));
	}

	private GenericEntity<?> getCashReceiptBrowseOutput(BrowseResponse<CashReceiptBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<CashReceiptBrowseOutput>>(starBrowseResponse) {
		};
	}

	@POST
	@Secured
	@Path("/read-arPymnt")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getARPayments(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BrowseResponse<ARPaymentBrowseOutput> starBrowseResponse = new BrowseResponse<ARPaymentBrowseOutput>();
		starBrowseResponse.setOutput(new ARPaymentBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String refPfx = jsonObject.get("refPfx").getAsString();
		String refNo = jsonObject.get("refNo").getAsString();

		ARInfoDAO arInfo = new ARInfoDAO();

		arInfo.readARPayments(starBrowseResponse, cmpyId, refPfx, refNo);
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblARPymnt.size();

		return starResponseBuilder.getSuccessResponse(getARPaymentBrowseOutput(starBrowseResponse));
	}

	private GenericEntity<?> getARPaymentBrowseOutput(BrowseResponse<ARPaymentBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<ARPaymentBrowseOutput>>(starBrowseResponse) {
		};
	}

	@POST
	@Secured
	@Path("/read-arEnqry")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getAREnquiry(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BrowseResponse<AREnquiryBrowseOutput> starBrowseResponse = new BrowseResponse<AREnquiryBrowseOutput>();
		starBrowseResponse.setOutput(new AREnquiryBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String cusId = jsonObject.get("cusId").getAsString();
		String brh = jsonObject.get("brh").getAsString();
		String dueDt = jsonObject.get("dueDt").getAsString();
		String invNo = jsonObject.get("invNo").getAsString();
		String refPfx = jsonObject.get("refPfx").getAsString();
		String refNo = jsonObject.get("refNo").getAsString();
		String cry = "";
		if (jsonObject.has("cry")) {
			cry = jsonObject.get("cry").getAsString();
		}

		ARInfoDAO arInfo = new ARInfoDAO();

		arInfo.readAREnquiry(starBrowseResponse, cusId, brh, dueDt, invNo, cmpyId, refPfx, refNo, cry);
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblAREnqry.size();

		return starResponseBuilder.getSuccessResponse(getAREnquiryBrowseOutput(starBrowseResponse));
	}

	private GenericEntity<?> getAREnquiryBrowseOutput(BrowseResponse<AREnquiryBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<AREnquiryBrowseOutput>>(starBrowseResponse) {
		};
	}

	@POST
	@Secured
	@Path("/read-advPmt")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getAdvancePayment(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BrowseResponse<AdvancePaymentBrowseOutput> starBrowseResponse = new BrowseResponse<AdvancePaymentBrowseOutput>();
		starBrowseResponse.setOutput(new AdvancePaymentBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String refPfx = jsonObject.get("refPfx").getAsString();
		String refNo = jsonObject.get("refNo").getAsString();

		ARInfoDAO arInfo = new ARInfoDAO();

		arInfo.readAdvancePayment(starBrowseResponse, cmpyId, refPfx, refNo);
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblAdvPmt.size();

		return starResponseBuilder.getSuccessResponse(getAdvancePaymentBrowseOutput(starBrowseResponse));
	}

	private GenericEntity<?> getAdvancePaymentBrowseOutput(
			BrowseResponse<AdvancePaymentBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<AdvancePaymentBrowseOutput>>(starBrowseResponse) {
		};
	}

	@POST
	@Secured
	@Path("/read-uaDebit")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getUnappliedDebit(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BrowseResponse<UnappliedDebitBrowseOutput> starBrowseResponse = new BrowseResponse<UnappliedDebitBrowseOutput>();
		starBrowseResponse.setOutput(new UnappliedDebitBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String refPfx = jsonObject.get("refPfx").getAsString();
		String refNo = jsonObject.get("refNo").getAsString();

		ARInfoDAO arInfo = new ARInfoDAO();

		arInfo.readUnappliedDebit(starBrowseResponse, cmpyId, refPfx, refNo);
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblUnappliedDr.size();

		return starResponseBuilder.getSuccessResponse(getUnappliedDebitBrowseOutput(starBrowseResponse));
	}

	private GenericEntity<?> getUnappliedDebitBrowseOutput(
			BrowseResponse<UnappliedDebitBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<UnappliedDebitBrowseOutput>>(starBrowseResponse) {
		};
	}

	@POST
	@Secured
	@Path("/read-glDist")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getGLDistribution(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BrowseResponse<GLDistributionBrowseOutput> starBrowseResponse = new BrowseResponse<GLDistributionBrowseOutput>();
		starBrowseResponse.setOutput(new GLDistributionBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String refPfx = jsonObject.get("refPfx").getAsString();
		String refNo = jsonObject.get("refNo").getAsString();

		ARInfoDAO arInfo = new ARInfoDAO();

		arInfo.readGLDistribution(starBrowseResponse, cmpyId, refPfx, refNo);
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblGLDist.size();

		return starResponseBuilder.getSuccessResponse(getGLDistributionBrowseOutput(starBrowseResponse));
	}

	private GenericEntity<?> getGLDistributionBrowseOutput(
			BrowseResponse<GLDistributionBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<GLDistributionBrowseOutput>>(starBrowseResponse) {
		};
	}

	@POST
	@Secured
	@Path("/read-aragng")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getArDashboard(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BrowseResponse<ARDesboardBrowseOutput> starBrowseResponse = new BrowseResponse<ARDesboardBrowseOutput>();
		starBrowseResponse.setOutput(new ARDesboardBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		String cusId = "";
		String agng = "";
		String brh = "";
		String invcDt = "";
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		if (jsonObject.get("cusId") == null) {
		} else
			cusId = jsonObject.get("cusId").getAsString();
		if (jsonObject.get("agng") == null) {
		} else
			agng = jsonObject.get("agng").getAsString();
		if (jsonObject.get("brh") == null) {
		} else
			brh = jsonObject.get("brh").getAsString();
		if (jsonObject.get("invcDt") == null) {
		} else
			invcDt = jsonObject.get("invcDt").getAsString();

		ARInfoDAO arInfo = new ARInfoDAO();
		arInfo.readDesboard(starBrowseResponse, cusId, agng, brh, invcDt);
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblARdsbrd.size();

		return starResponseBuilder.getSuccessResponse(getArDeshboardBrowseOutput(starBrowseResponse));
	}

	private GenericEntity<?> getArDeshboardBrowseOutput(BrowseResponse<ARDesboardBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<ARDesboardBrowseOutput>>(starBrowseResponse) {
		};
	}

	@POST
	@Secured
	@Path("/read-ar-rcv")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getArPmtRecvData(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BrowseResponse<ARPmtRecvDataBrowseOutput> starBrowseResponse = new BrowseResponse<ARPmtRecvDataBrowseOutput>();
		starBrowseResponse.setOutput(new ARPmtRecvDataBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		String cusId = "";
		String agng = "";
		String brh = "";
		String stDt = "";
		String enDt = "";
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		if (jsonObject.get("cusId") == null) {
		} else
			cusId = jsonObject.get("cusId").getAsString();
		if (jsonObject.get("agng") == null) {
		} else
			agng = jsonObject.get("agng").getAsString();
		if (jsonObject.get("brh") == null) {
		} else
			brh = jsonObject.get("brh").getAsString();
		if (jsonObject.get("strtdt") == null) {
		} else
			stDt = jsonObject.get("strtdt").getAsString();
		if (jsonObject.get("enddt") == null) {
		} else
			enDt = jsonObject.get("enddt").getAsString();

		ARInfoDAO arInfo = new ARInfoDAO();
		arInfo.readPmtRecvData(starBrowseResponse, cusId, agng, brh, stDt, enDt);

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblARPmtRecvdata.size();

		return starResponseBuilder.getSuccessResponse(getArPmtRecvDataBrowseOutput(starBrowseResponse));
	}

	private GenericEntity<?> getArPmtRecvDataBrowseOutput(
			BrowseResponse<ARPmtRecvDataBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<ARPmtRecvDataBrowseOutput>>(starBrowseResponse) {
		};
	}

	@POST
	@Secured
	@Path("/read-ar-rcv_export")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getReconData(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BrowseResponse<ARPmtRecvDataBrowseOutput> starBrowseResponse = new BrowseResponse<ARPmtRecvDataBrowseOutput>();
		starBrowseResponse.setOutput(new ARPmtRecvDataBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		String cusId = "";
		String agng = "";
		String brh = "";
		String stDt = "";
		String enDt = "";
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		if (jsonObject.get("cusId") == null) {
		} else
			cusId = jsonObject.get("cusId").getAsString();
		if (jsonObject.get("agng") == null) {
		} else
			agng = jsonObject.get("agng").getAsString();
		if (jsonObject.get("brh") == null) {
		} else
			brh = jsonObject.get("brh").getAsString();
		if (jsonObject.get("strtdt") == null) {
		} else
			stDt = jsonObject.get("strtdt").getAsString();
		if (jsonObject.get("enddt") == null) {
		} else
			enDt = jsonObject.get("enddt").getAsString();
		ARInfoDAO arInfo = new ARInfoDAO();
		List arRcvExports = arInfo.readPmtRecvData(starBrowseResponse, cusId, agng, brh, stDt, enDt);
		ManageDirectory directory = new ManageDirectory();

		InputStream fileInputStream;
		javax.ws.rs.core.Response.ResponseBuilder responseBuilder = null;
		String flNm = "ar_rcv" + new Date().getTime() + ".xlsx";
		String flNmPath = "C:\\temp\\" + flNm;

		File initialFile = new File("xls/ar-rcv-tmpl.xlsx");

		try (InputStream is = getClass().getClassLoader().getResourceAsStream("xls/ar-rcv-tmpl.xlsx")) {
			try (OutputStream os = new FileOutputStream(flNmPath)) {
				Context context = new Context();
				context.putVar("arRcvExports", arRcvExports);
				JxlsHelper.getInstance().processTemplate(is, os, context);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		fileInputStream = directory.downloadFile(flNmPath);

		responseBuilder = javax.ws.rs.core.Response.ok();
		return responseBuilder.build();

	}
	
	@POST
	@Secured
	@Path("/vldt-disc")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response vldtDiscDt(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {
		
		MaintanenceResponse<ValidateDiscountDateManOutput> starManResponse = new MaintanenceResponse<ValidateDiscountDateManOutput>();
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		starManResponse.setOutput(new ValidateDiscountDateManOutput());

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String postDt = jsonObject.get("postDt").getAsString();
		String discDt = jsonObject.get("discDt").getAsString();

		ARInfoDAO arInfo = new ARInfoDAO();
		
		starManResponse.output.allowFlg = arInfo.vldtDiscDt(starManResponse, postDt, discDt);

		return starResponseBuilder.getSuccessResponse(getVldtDiscDtMaintanenceOutput(starManResponse));
	}

	private GenericEntity<?> getVldtDiscDtMaintanenceOutput(MaintanenceResponse<ValidateDiscountDateManOutput> starManResponse) {
		return new GenericEntity<MaintanenceResponse<ValidateDiscountDateManOutput>>(starManResponse) {
		};
	}

}
