package com.star.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.MaintanenceResponse;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.dao.CompanyDAO;
import com.star.linkage.bank.BankManOutput;
import com.star.linkage.company.CompanyBrowseOutput;
import com.star.linkage.company.CompanyManOutput;


@Path("/company")
public class CompanyService {

	@POST
	@Secured
	@Path("/read")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getCompany(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<CompanyBrowseOutput> starBrowseResponse = new BrowseResponse<CompanyBrowseOutput>();
		starBrowseResponse.setOutput(new CompanyBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String cmpyId = jsonObject.get("cmpyId").getAsString();
		
		CompanyDAO companyDAO = new CompanyDAO();
		companyDAO.getBankDetails(starBrowseResponse, cmpyId);
		
		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));
	}
	
	private GenericEntity<?> getGenericBrowseOutput(BrowseResponse<CompanyBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<CompanyBrowseOutput>>(starBrowseResponse) {
		};
	}
	
	@POST
	@Secured
	@Path("/add")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response addBankCompany(@HeaderParam("user-id") String userId, String data) throws Exception {

		MaintanenceResponse<CompanyManOutput> starManResponse = new MaintanenceResponse<CompanyManOutput>();
		starManResponse.setOutput(new CompanyManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String cmpyId = jsonObject.get("cmpyId").getAsString();
		String bnkCode = jsonObject.get("bnkCode").getAsString();
		
		
		CompanyDAO companyDAO = new CompanyDAO();
		companyDAO.addCompanyBank(starManResponse, cmpyId, bnkCode);
		
		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starManResponse));
	}
	
	private GenericEntity<?> getGenericMaintenanceOutput(
			MaintanenceResponse<CompanyManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<CompanyManOutput>>(starMaintenanceResponse) {
		};
	}
	
}
