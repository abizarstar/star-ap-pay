package com.star.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import org.jxls.common.Context;
import org.jxls.util.JxlsHelper;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.ManageDirectory;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.dao.APInfoDAO;
import com.star.dao.ARInfoDAO;
import com.star.linkage.ap.APDesCheckInfoBrowseOutput;
import com.star.linkage.ap.APDisbursementBrowseOutput;
import com.star.linkage.ap.APEnquiryBrowseOutput;
import com.star.linkage.ap.CheckViewBrowseOutput;
import com.star.linkage.ap.OpenItemBrowseOutput;
import com.star.linkage.ar.ARPmtRecvDataBrowseOutput;

@Path("/ap")
public class APService {

	@POST
	@Secured
	@Path("/read-ap-disb")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getAPDisbursment(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {
		BrowseResponse<APDisbursementBrowseOutput> starBrowseResponse = new BrowseResponse<APDisbursementBrowseOutput>();
		starBrowseResponse.setOutput(new APDisbursementBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String ssnId = jsonObject.get("ssnId").getAsString();

		APInfoDAO apInfo = new APInfoDAO();
		apInfo.readAPDisbursement(starBrowseResponse, cmpyId, ssnId);
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblAPDisbursement.size();
		return starResponseBuilder.getSuccessResponse(getAPDisbursementBrowseOutput(starBrowseResponse));
	}

	private GenericEntity<?> getAPDisbursementBrowseOutput(
			BrowseResponse<APDisbursementBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<APDisbursementBrowseOutput>>(starBrowseResponse) {
		};
	}

	@POST
	@Secured
	@Path("/read-ap-ssn")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getAPSession(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {
		BrowseResponse<APEnquiryBrowseOutput> starBrowseResponse = new BrowseResponse<APEnquiryBrowseOutput>();
		starBrowseResponse.setOutput(new APEnquiryBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		APInfoDAO apInfo = new APInfoDAO();
		apInfo.readAPSession(starBrowseResponse, cmpyId);
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblAPEnqry.size();

		return starResponseBuilder.getSuccessResponse(getAPEnquiryBrowseOutput(starBrowseResponse));
	}

	private GenericEntity<?> getAPEnquiryBrowseOutput(BrowseResponse<APEnquiryBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<APEnquiryBrowseOutput>>(starBrowseResponse) {
		};
	}

	@POST
	@Secured
	@Path("/opn-itm")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getOpenItems(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BrowseResponse<OpenItemBrowseOutput> starBrowseResponse = new BrowseResponse<OpenItemBrowseOutput>();
		starBrowseResponse.setOutput(new OpenItemBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String venId = jsonObject.get("venId").getAsString();
		String brh = jsonObject.get("brh").getAsString();
		String cry = jsonObject.get("cry").getAsString();

		APInfoDAO apInfoDAO = new APInfoDAO();

		apInfoDAO.readOpenItems(starBrowseResponse, cmpyId, venId, brh, cry);
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblOpnItm.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));
	}

	private GenericEntity<?> getGenericBrowseOutput(BrowseResponse<OpenItemBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<OpenItemBrowseOutput>>(starBrowseResponse) {
		};
	}

	@POST
	@Secured
	@Path("/read-check-view")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getCheckView(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BrowseResponse<CheckViewBrowseOutput> starBrowseResponse = new BrowseResponse<CheckViewBrowseOutput>();
		starBrowseResponse.setOutput(new CheckViewBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String dsbPfx = jsonObject.get("dsbPfx").getAsString();
		String dsbNo = jsonObject.get("dsbNo").getAsString();

		APInfoDAO apInfo = new APInfoDAO();
		apInfo.readCheckView(starBrowseResponse, cmpyId, dsbPfx, dsbNo);
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblChkVw.size();
		return starResponseBuilder.getSuccessResponse(getCheckViewBrowseOutput(starBrowseResponse));
	}

	private GenericEntity<?> getCheckViewBrowseOutput(BrowseResponse<CheckViewBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<CheckViewBrowseOutput>>(starBrowseResponse) {
		};
	}

	@POST
	@Secured
	@Path("/read-check-dsb")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getCheckdsbView(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BrowseResponse<APDesCheckInfoBrowseOutput> starBrowseResponse = new BrowseResponse<APDesCheckInfoBrowseOutput>();
		starBrowseResponse.setOutput(new APDesCheckInfoBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String invcNo = jsonObject.get("invNo").getAsString();
		String venId = jsonObject.get("venId").getAsString();
		String brh = jsonObject.get("brh").getAsString();
		String frmInvDt = jsonObject.get("frmInvDt").getAsString();
		String toInvcDt = jsonObject.get("toInvcDt").getAsString();
		
		APInfoDAO apInfo = new APInfoDAO();
		apInfo.readDsbCheck(starBrowseResponse, cmpyId, invcNo, venId, brh, frmInvDt, toInvcDt);
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblAPDsbChk.size();
		return starResponseBuilder.getSuccessResponse(getCheckdsbViewBrowseOutput(starBrowseResponse));
	}

	private GenericEntity<?> getCheckdsbViewBrowseOutput(BrowseResponse<APDesCheckInfoBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<APDesCheckInfoBrowseOutput>>(starBrowseResponse) {
		};
	}
}
