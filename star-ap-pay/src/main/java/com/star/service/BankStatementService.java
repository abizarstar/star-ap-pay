package com.star.service;

import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;

import javax.annotation.security.RolesAllowed;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.BodyPart;
import org.glassfish.jersey.media.multipart.ContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;
import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.MaintanenceResponse;
import com.star.common.ManageDirectory;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.common.ValidateValue;
import com.star.common.bai.ProcessBAIFile;
import com.star.common.bai.ProcessLockbox;
import com.star.dao.BankStmtDAO;
import com.star.dao.GLInfoDAO;
import com.star.dao.InvoiceDAO;
import com.star.linkage.ap.GLDistributionInfo;
import com.star.linkage.ar.AREnquiryInfo;
import com.star.linkage.cstmdocinfo.VchrInfo;
import com.star.linkage.ebs.APPostingBrowseInfo;
import com.star.linkage.ebs.BankHdrDtls;
import com.star.linkage.ebs.BankStmtBrowseOutput;
import com.star.linkage.ebs.BankStmtDetails;
import com.star.linkage.ebs.BankStmtList;
import com.star.linkage.ebs.BankStmtListBrowse;
import com.star.linkage.ebs.BankStmtManOutput;
import com.star.linkage.ebs.BankTrnDetails;
import com.star.linkage.ebs.LockBoxDetails;
import com.star.linkage.ebs.LockBoxStmtBrowseOutput;
import com.star.linkage.ebstypecode.EBSGLAccountInfo;
import com.star.linkage.gl.GLEntryDataBrwose;
import com.star.linkage.invoice.InvoiceInfoBrowseOutput;
import com.star.linkage.stmt.BnkStmtInput;
import com.star.linkage.stmt.BnkStmtManOutput;
import com.ws.samples.handler.MsgSOAPHandler;
import com.ws.samples.handler.SecSOAPHandler;

@Path("/bai")
public class BankStatementService {

	@POST
	@Path("/inv")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getInvoiceDetails(@Context HttpServletResponse response, String data,
			@HeaderParam("user-id") String userId, @HeaderParam("auth-token") String authToken,
			@HeaderParam("app-host") String appHost, @HeaderParam("app-port") String appPort,
			@HeaderParam("cmpy-id") String cmpyId) throws Exception {

		BrowseResponse<InvoiceInfoBrowseOutput> starBrowseResponse = new BrowseResponse<InvoiceInfoBrowseOutput>();
		starBrowseResponse.setOutput(new InvoiceInfoBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String invNo = jsonObject.get("invNo").getAsString();
		String checkNo = jsonObject.get("checkNo").getAsString();
		String checkDesc = jsonObject.get("checkDesc").getAsString();
		String checkAmt = jsonObject.get("checkAmt").getAsString();
		String bnk = jsonObject.get("bnk").getAsString();

		InvoiceDAO invoiceDAO = new InvoiceDAO();
		invoiceDAO.getInvoiceDetails(starBrowseResponse, invNo);

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblInvoice.size();

		if (starBrowseResponse.output.nbrRecRtn > 0) {

			JsonObject object = new JsonObject();

			object.addProperty("cusId", starBrowseResponse.output.fldTblInvoice.get(0).getCusId());
			object.addProperty("ssnId", starBrowseResponse.output.fldTblInvoice.get(0).getSsnId());
			object.addProperty("brh", starBrowseResponse.output.fldTblInvoice.get(0).getArBrh());
			object.addProperty("checkNo", checkNo);
			object.addProperty("checkDesc", checkDesc);
			object.addProperty("checkAmt", checkAmt);
			object.addProperty("stxBnk", bnk);
			object.addProperty("invNo", invNo);

			CreateCashRecipt cashReciptService = new CreateCashRecipt();

			SecSOAPHandler securityHandler = cashReciptService.createSecurityHandlers(userId, authToken, appHost,
					appPort, cmpyId);
			MsgSOAPHandler messagesHandler = cashReciptService.createMessageHandlers(userId, authToken, appHost,
					appPort, cmpyId);

			if (starBrowseResponse.output.fldTblInvoice.get(0).getSsnId().length() > 0) {
				String cashRcpt = cashReciptService.createRcpt(securityHandler, messagesHandler, object.toString());

				object.addProperty("rcptNo", String.valueOf(cashRcpt));

				cashReciptService.createARDistribution(securityHandler, messagesHandler, object.toString());

			} else {

				int ssnId = cashReciptService.createSession(securityHandler, messagesHandler, object.toString());

				object.addProperty("ssnId", String.valueOf(ssnId));

				String cashRcpt = cashReciptService.createRcpt(securityHandler, messagesHandler, object.toString());

				object.addProperty("rcptNo", String.valueOf(cashRcpt));

				cashReciptService.createARDistribution(securityHandler, messagesHandler, object.toString());

				System.out.println(cashRcpt);
			}
		}

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));

	}

	@POST
	@Secured
	@RolesAllowed("ADMIN")
	@Path("/pdf")
	@Consumes({ MediaType.MULTIPART_FORM_DATA })
	public Response uploadPdfFile(@FormDataParam("stmtTyp") String stmtTyp,@FormDataParam("postParam") String postParam, @HeaderParam("user-id") String userId,
			@HeaderParam("cmpy-id") String cmpyId, @HeaderParam("auth-token") String authToken,
			@HeaderParam("app-host") String appHost, @HeaderParam("app-port") String appPort,
			@FormDataParam("file") FormDataBodyPart body) throws Exception {

		ManageDirectory directory = new ManageDirectory();
		ValidateValue validateValue = new ValidateValue();
		BankStmtDAO bankStmtDAO = new BankStmtDAO();
		BankStmtDetails bankStmtDetails = new BankStmtDetails();
		LockBoxDetails lockBoxDetails = new LockBoxDetails();

		MaintanenceResponse<BnkStmtManOutput> starMaintenanceResponse = new MaintanenceResponse<BnkStmtManOutput>();
		starMaintenanceResponse.setOutput(new BnkStmtManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		String rtnMsg = "";

		List<BnkStmtInput> documnetNmList = new ArrayList<BnkStmtInput>();

		for (BodyPart part : body.getParent().getBodyParts()) {

			System.out.println(part.getContentDisposition().getParameters().size());

			InputStream is = part.getEntityAs(InputStream.class);
			ContentDisposition meta = part.getContentDisposition();

			if (meta.getFileName() != null) {

				BnkStmtInput bnkStmtInp = new BnkStmtInput();

				System.out.println(meta.getFileName());

				String flExtn = validateValue.getFileExtension(meta.getFileName());

				int lstBnkId = bankStmtDAO.getBnkStmtCount();

				lstBnkId = lstBnkId + 1;

				String sysFlNm = "";

				if (stmtTyp.equals("1")) {
					sysFlNm = "BAI_" + lstBnkId + "_" + formattedDate();
				} else if (stmtTyp.equals("2")) {
					sysFlNm = "Lockbox_" + lstBnkId + "_" + formattedDate();
				}

				bnkStmtInp.setFlExt(flExtn);
				bnkStmtInp.setFlNm(sysFlNm + ".txt");
				bnkStmtInp.setFlOrgNm(meta.getFileName());
				bnkStmtInp.setId(lstBnkId);

				rtnMsg = directory.createFile(CommonConstants.BAI_DIR, sysFlNm, flExtn, is);

				documnetNmList.add(bnkStmtInp);

				if (rtnMsg.length() > 0) {
					break;
				}

				if (stmtTyp.equals("1")) {
					ProcessBAIFile baiFile = new ProcessBAIFile();

					baiFile.readBAIFile(
							CommonConstants.ROOT_DIR + CommonConstants.BAI_DIR + "/" + sysFlNm + "." + flExtn,
							bankStmtDetails);
				} else if (stmtTyp.equals("2")) {
					ProcessLockbox lockbox = new ProcessLockbox();

					lockbox.getLockboxDetails(
							CommonConstants.ROOT_DIR + CommonConstants.BAI_DIR + "/" + sysFlNm + "." + flExtn,
							lockBoxDetails);
				}
			}
		}
		
		int flg =0;
		if(stmtTyp.equals("1"))
		{
			flg = bankStmtDAO.getBankTrnExistStmtDate(bankStmtDetails.getCrtdDt(), "BAI");
		}
		else if(stmtTyp.equals("2"))
		{
			if(lockBoxDetails.getHdrInfoLst().size() > 0)
			{
				flg = bankStmtDAO.getBankTrnExistStmtDate(lockBoxDetails.getHdrInfoLst().get(0).getDpstDt(), "LCK");
			}
		}

		if (documnetNmList.size() > 0 && rtnMsg.length() == 0 && flg == 0) {

			for (int i = 0; i < documnetNmList.size(); i++) {

				if (stmtTyp.equals("1")) {
					bankStmtDAO.addBnkStmtDtls(starMaintenanceResponse, documnetNmList.get(i), bankStmtDetails, userId, cmpyId);
				} else if (stmtTyp.equals("2")) {
					bankStmtDAO.addLockBoxDtls(starMaintenanceResponse, documnetNmList.get(i), lockBoxDetails, userId, cmpyId);
				}

				
				
				if (starMaintenanceResponse.output.rtnSts == 0 && postParam.equals("PI")) {

					for (int j = 0; j < bankStmtDetails.getBnkHdrList().size(); j++) {
						BankHdrDtls bankHdrDtls = bankStmtDetails.getBnkHdrList().get(j);

						for (int k = 0; k < bankHdrDtls.getListBnkTrn().size(); k++) {
							BankTrnDetails bankTrnDetails = bankHdrDtls.getListBnkTrn().get(k);

							bankTrnDetails.getTypeCode();

							/* AUTO LEDGER ENTRY FOR ZBA CREDIT AND ZBA DEBIT TRANSACIONS */
							if (bankTrnDetails.getTypeCode().equals("275")) {
								GLInfoDAO dao = new GLInfoDAO();

								dao.addLedgerMain(bankStmtDetails.getUpldId(), bankHdrDtls.getHdrId(), bankTrnDetails,
										cmpyId, userId, "ZBA CREDIT 275", bankStmtDetails.getCrtdDt());

							} else if (bankTrnDetails.getTypeCode().equals("575")) {
								GLInfoDAO dao = new GLInfoDAO();
								dao.addLedgerMain(bankStmtDetails.getUpldId(), bankHdrDtls.getHdrId(), bankTrnDetails,
										cmpyId, userId, "ZBA DEBIT 575", bankStmtDetails.getCrtdDt());
							}

						}

					}

					for (int j = 0; j < lockBoxDetails.getTrnInfoLst().size(); j++) {

						BankStmtDAO dao = new BankStmtDAO();
						String invNo = lockBoxDetails.getTrnInfoLst().get(j).getInvNo();
						double stmtAmt = Double.parseDouble(lockBoxDetails.getTrnInfoLst().get(j).getAmount());
						System.out.println(invNo);

						AREnquiryInfo info = dao.getInvoiceInfo(invNo, "");

						if (info.getArNo() != null) {
							if (Double.parseDouble(info.getOrigAmt()) == stmtAmt) {
								
								Date date = new Date();  
							    SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");  
							    String strDate= formatter.format(date);  
								
								JsonObject obj = new JsonObject();

								String bnk = dao.getTypeCodeBank("LCK");

								obj.addProperty("ssnId", "");
								obj.addProperty("rcpt", "");
								obj.addProperty("payAmt", info.getIpAmt());
								obj.addProperty("bnk", bnk);
								obj.addProperty("post", strDate);
								obj.addProperty("jrnl", strDate);

								JsonArray array = new JsonArray();

								JsonObject objInv = new JsonObject();

								objInv.addProperty("arNo", info.getArPfx() + "-" + info.getArNo());
								objInv.addProperty("brh", info.getBrh());
								objInv.addProperty("cus", info.getCusId());
								objInv.addProperty("amt", info.getOrigAmt());
								objInv.addProperty("ref", info.getUpdRef());
								objInv.addProperty("discAmt", info.getDiscAmt());
								objInv.addProperty("due", info.getDueDt());

								array.add(objInv);

								obj.add("invTrn", array);
								
								obj.addProperty("trn", lockBoxDetails.getTrnInfoLst().get(j).getId());
								obj.addProperty("upld", lockBoxDetails.getTrnInfoLst().get(j).getEbsUpldId());
								obj.addProperty("trnTypRef", "L");
								
								System.out.println(obj.toString());

								CashReciptService cashReciptService = new CashReciptService();

								cashReciptService.createRcptMultiple(obj.toString(), userId, authToken, appHost,
										appPort, cmpyId);

							}
						}
					}
				}
			}
		} else {
			starMaintenanceResponse.output.rtnSts = 1;
		
			if(flg >0)
			{
				starMaintenanceResponse.output.messages.add(CommonConstants.STMT_ERR);
			}
			else
			{
				starMaintenanceResponse.output.messages.add(rtnMsg);
			}
		}

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starMaintenanceResponse));
	}

	@POST
	@Secured
	@Path("/read")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getStatement(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<BankStmtBrowseOutput> starBrowseResponse = new BrowseResponse<BankStmtBrowseOutput>();
		starBrowseResponse.setOutput(new BankStmtBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String upldOnFrm = jsonObject.get("upldOnFrm").getAsString();
		String upldOnTo = jsonObject.get("upldOnTo").getAsString();
		String stmntDtFrm = jsonObject.get("stmntDtFrm").getAsString();
		String stmntDtTo = jsonObject.get("stmntDtTo").getAsString();
		String status = jsonObject.get("status").getAsString();
		int pageNo = jsonObject.get("pageNo").getAsInt();

		BankStmtDAO bankStmtDAO = new BankStmtDAO();

		bankStmtDAO.readStmtHdr(starBrowseResponse, "", upldOnFrm, upldOnTo, stmntDtFrm, stmntDtTo, status, pageNo);

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblBnkTrn.size();
		
		if (starBrowseResponse.output.fldTblBnkTrn.size() < CommonConstants.DB_OUT_REC) {
			starBrowseResponse.output.eof = 1;
		} else {
			starBrowseResponse.output.eof = 0;
		}

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputStmt(starBrowseResponse));
	}

	@POST
	@Secured
	@Path("/read-lckbx")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getStatementLockbox(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<LockBoxStmtBrowseOutput> starBrowseResponse = new BrowseResponse<LockBoxStmtBrowseOutput>();
		starBrowseResponse.setOutput(new LockBoxStmtBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String upldOnFrm = jsonObject.get("upldOnFrm").getAsString();
		String upldOnTo = jsonObject.get("upldOnTo").getAsString();

		BankStmtDAO bankStmtDAO = new BankStmtDAO();

		bankStmtDAO.readStmtLockboxHdr(starBrowseResponse, "", upldOnFrm, upldOnTo);

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblLckBxHdr.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputStmtLckBx(starBrowseResponse));

	}

	@POST
	@Secured
	@Path("/upd-gl-data")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response updateGLTransaction(@HeaderParam("user-id") String usrId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		MaintanenceResponse<BankStmtManOutput> starManResponse = new MaintanenceResponse<BankStmtManOutput>();
		starManResponse.setOutput(new BankStmtManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String hdrId = jsonObject.get("hdrId").getAsString();
		String upldId = jsonObject.get("upldId").getAsString();
		String trnId = jsonObject.get("trnId").getAsString();
		String typCd = jsonObject.get("typCd").getAsString();
		String rmk = jsonObject.get("rmk").getAsString();
		String narrDesc = jsonObject.get("rmk").getAsString();
		String postDt = jsonObject.get("postDt").getAsString();

		JsonArray glMapArray = jsonObject.get("glMap").getAsJsonArray();

		List<GLDistributionInfo> glebsMapInputs = new ArrayList<GLDistributionInfo>();

		for (int i = 0; i < glMapArray.size(); i++) {
			JsonObject jsonGlObject = glMapArray.get(i).getAsJsonObject();

			GLDistributionInfo distInfo = new GLDistributionInfo();

			distInfo.setGlAcct(jsonGlObject.get("acct").getAsString());
			distInfo.setSacct(jsonGlObject.get("subAcct").getAsString());

			/*if (jsonGlObject.get("trnTyp").getAsString().equals("C")) {
				distInfo.setCrAmt(jsonGlObject.get("crAmt").getAsString());
				distInfo.setDrAmt(jsonGlObject.get("drAmt").getAsString());
			}

			if (jsonGlObject.get("trnTyp").getAsString().equals("D")) {
				distInfo.setDrAmt(jsonGlObject.get("amt").getAsString());
				distInfo.setCrAmt("0");
			}*/
			
			distInfo.setCrAmt(jsonGlObject.get("crAmt").getAsString());
			distInfo.setDrAmt(jsonGlObject.get("drAmt").getAsString());
			distInfo.setRemark(jsonGlObject.get("remark").getAsString());

			glebsMapInputs.add(distInfo);
		}

		BankStmtDAO dao = new BankStmtDAO();

		dao.addLedgerDetails(starManResponse, Integer.parseInt(upldId), Integer.parseInt(hdrId),
				Integer.parseInt(trnId), glebsMapInputs, cmpyId, usrId, rmk, typCd, narrDesc, postDt);

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputStmt(starManResponse));
	}

	@POST
	@Secured
	@Path("/upd-gl-data-ap")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response updateGLTransactionAP(@HeaderParam("user-id") String usrId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		MaintanenceResponse<BankStmtManOutput> starManResponse = new MaintanenceResponse<BankStmtManOutput>();
		starManResponse.setOutput(new BankStmtManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String hdrId = jsonObject.get("hdrId").getAsString();
		String upldId = jsonObject.get("upldId").getAsString();
		String trnId = jsonObject.get("trnId").getAsString();
		String typCd = jsonObject.get("typCd").getAsString();
		String rmk = jsonObject.get("rmk").getAsString();
		String narrDesc = jsonObject.get("narr").getAsString();
		String postDt = jsonObject.get("postDt").getAsString();

		JsonArray glMapArray = jsonObject.get("glMap").getAsJsonArray();

		List<GLDistributionInfo> glebsMapInputs = new ArrayList<GLDistributionInfo>();

		for (int i = 0; i < glMapArray.size(); i++) {
			JsonObject jsonGlObject = glMapArray.get(i).getAsJsonObject();

			GLDistributionInfo distInfo = new GLDistributionInfo();

			distInfo.setGlAcct(jsonGlObject.get("acct").getAsString());
			distInfo.setSacct(jsonGlObject.get("subAcct").getAsString());
			distInfo.setCrAmt(jsonGlObject.get("crAmt").getAsString());
			distInfo.setDrAmt(jsonGlObject.get("drAmt").getAsString());
			distInfo.setRemark(jsonGlObject.get("remark").getAsString());
			glebsMapInputs.add(distInfo);
		}

		JsonArray glInvArray = jsonObject.get("invTrn").getAsJsonArray();

		double totInvcAmt = 0.0;

		List<VchrInfo> vchrInfoList = new ArrayList<VchrInfo>();

		for (int i = 0; i < glInvArray.size(); i++) {
			JsonObject jsonInvObject = glInvArray.get(i).getAsJsonObject();

			VchrInfo info = new VchrInfo();

			String vchrNo = jsonInvObject.get("vchrNo").getAsString();
			System.out.println(jsonInvObject.get("amt").getAsString());

			info.setVchrPfx(vchrNo.substring(0, 2));
			info.setVchrNo(vchrNo.substring(3, vchrNo.length()));
			info.setVchrBrh(jsonInvObject.get("brh").getAsString());
			info.setVchrVenId(jsonInvObject.get("ven").getAsString());
			info.setDiscAmt(jsonInvObject.get("discAmt").getAsDouble());
			info.setVchrAmt(jsonInvObject.get("amt").getAsDouble());
			info.setVchrDueDtStr(jsonInvObject.get("due").getAsString());
			info.setVchrInvNo(jsonInvObject.get("ref").getAsString());

			vchrInfoList.add(info);

			totInvcAmt = totInvcAmt + Double.parseDouble(jsonInvObject.get("amt").getAsString());
		}
		
		totInvcAmt = Double.parseDouble(String.format("%.2f", totInvcAmt));
		

		GLInfoDAO glInfoDAO = new GLInfoDAO();

		List<EBSGLAccountInfo> glCombination = glInfoDAO.getGLAccount("1", typCd);

		for (int i = 0; i < glCombination.size(); i++) {
			EBSGLAccountInfo accountInfo = glCombination.get(i);

			if (accountInfo.getPosSeq().equals("P2") && accountInfo.getTrnTyp().equals("D")) {
				System.out.println(accountInfo.getGlAcct());
				System.out.println(accountInfo.getGlSubAcct());

				GLDistributionInfo distInfo = new GLDistributionInfo();
				distInfo.setGlAcct(accountInfo.getGlAcct());
				distInfo.setSacct(accountInfo.getGlSubAcct());
				distInfo.setCrAmt("0");
				distInfo.setDrAmt(String.valueOf(totInvcAmt));
				distInfo.setRemark("");

				if (totInvcAmt > 0) {
					glebsMapInputs.add(distInfo);
				}
			}
		}

		/* VALIDATE AMOUNT */
		double drAmt = 0;
		double crAmt = 0;
		for (int i = 0; i < glebsMapInputs.size(); i++) {
			drAmt = drAmt + Double.parseDouble(glebsMapInputs.get(i).getDrAmt());
			crAmt = crAmt + Double.parseDouble(glebsMapInputs.get(i).getCrAmt());
		}

		if (crAmt == drAmt) {
			BankStmtDAO dao = new BankStmtDAO();

			dao.addLedgerDetails(starManResponse, Integer.parseInt(upldId), Integer.parseInt(hdrId),
					Integer.parseInt(trnId), glebsMapInputs, cmpyId, usrId, rmk, typCd, narrDesc, postDt);

			if (starManResponse.output.rtnSts == 0 && starManResponse.output.glEntry.length() > 0) {
				for (int i = 0; i < vchrInfoList.size(); i++) {
					dao.addVoucherTrn(vchrInfoList.get(i), starManResponse.output.glEntry);
				}
			}
		} else {
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add("Proof should be 0 to post transaction on General Ledger");
		}

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputStmt(starManResponse));
	}

	@POST
	@Secured
	@Path("/get-gl-data")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getGLTransactions(@HeaderParam("user-id") String usrId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BrowseResponse<GLEntryDataBrwose> starBrowseResponse = new BrowseResponse<GLEntryDataBrwose>();
		starBrowseResponse.setOutput(new GLEntryDataBrwose());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String trnRef = jsonObject.get("trnRef").getAsString();

		BankStmtDAO dao = new BankStmtDAO();

		dao.getGLTransaction(starBrowseResponse, trnRef);

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblGLEntry.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputGlEntry(starBrowseResponse));
	}

	@POST
	@Secured
	@Path("/get-bnk-stmt-ftp")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getBankStmtList(@HeaderParam("user-id") String usrId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BrowseResponse<BankStmtListBrowse> starBrowseResponse = new BrowseResponse<BankStmtListBrowse>();
		starBrowseResponse.setOutput(new BankStmtListBrowse());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String trntype = jsonObject.get("typ").getAsString();

		getStatementList(starBrowseResponse, trntype);

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblBnkStmt.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputStmtList(starBrowseResponse));
	}

	@POST
	@Secured
	@Path("/process-bnk-ftp")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response processBnkStmt(@HeaderParam("user-id") String usrId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BankStmtDAO bankStmtDAO = new BankStmtDAO();
		BankStmtDetails bankStmtDetails = new BankStmtDetails();
		LockBoxDetails lockBoxDetails = new LockBoxDetails();
		ManageDirectory directory = new ManageDirectory();

		MaintanenceResponse<BnkStmtManOutput> starMaintenanceResponse = new MaintanenceResponse<BnkStmtManOutput>();
		starMaintenanceResponse.setOutput(new BnkStmtManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		List<BnkStmtInput> documnetNmList = new ArrayList<BnkStmtInput>();
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String flNm = jsonObject.get("flNm").getAsString();
		String flTyp = jsonObject.get("flTyp").getAsString();

		BnkStmtInput bnkStmtInp = new BnkStmtInput();
		String rtnMsg = "";
		String flExtn = "txt";

		int lstBnkId = bankStmtDAO.getBnkStmtCount();

		lstBnkId = lstBnkId + 1;

		String sysFlNm = "";

		if (flTyp.equals("EBS")) {
			sysFlNm = "BAI_" + lstBnkId + "_" + formattedDate();
		} else if (flTyp.equals("LCK")) {
			sysFlNm = "Lockbox_" + lstBnkId + "_" + formattedDate();
		}

		bnkStmtInp.setFlExt(flExtn);
		bnkStmtInp.setFlNm(sysFlNm + ".txt");
		bnkStmtInp.setFlOrgNm(flNm);
		bnkStmtInp.setId(lstBnkId);

		boolean flg = getFTPFile(flNm,
				CommonConstants.ROOT_DIR + "/" + CommonConstants.BAI_DIR + "/" + bnkStmtInp.getFlNm(), flTyp);

		documnetNmList.add(bnkStmtInp);
		if (flg == true) {

			if (flTyp.equals("EBS")) {
				ProcessBAIFile baiFile = new ProcessBAIFile();
				baiFile.readBAIFile(CommonConstants.ROOT_DIR + CommonConstants.BAI_DIR + "/" + sysFlNm + "." + flExtn,
						bankStmtDetails);
			} else if (flTyp.equals("LCK")) {
				ProcessLockbox lockbox = new ProcessLockbox();
				lockbox.getLockboxDetails(
						CommonConstants.ROOT_DIR + CommonConstants.BAI_DIR + "/" + sysFlNm + "." + flExtn,
						lockBoxDetails);
			}
		}

		if (documnetNmList.size() > 0 && rtnMsg.length() == 0) {

			for (int i = 0; i < documnetNmList.size(); i++) {

				if (flTyp.equals("EBS")) {
					bankStmtDAO.addBnkStmtDtls(starMaintenanceResponse, documnetNmList.get(i), bankStmtDetails, usrId, cmpyId);
				} else if (flTyp.equals("LCK")) {
					bankStmtDAO.addLockBoxDtls(starMaintenanceResponse, documnetNmList.get(i), lockBoxDetails, usrId, cmpyId);
				}

				if (starMaintenanceResponse.output.rtnSts == 0) {

					for (int j = 0; j < bankStmtDetails.getBnkHdrList().size(); j++) {
						BankHdrDtls bankHdrDtls = bankStmtDetails.getBnkHdrList().get(j);

						for (int k = 0; k < bankHdrDtls.getListBnkTrn().size(); k++) {
							BankTrnDetails bankTrnDetails = bankHdrDtls.getListBnkTrn().get(k);

							bankTrnDetails.getTypeCode();

							/* AUTO LEDGER ENTRY FOR ZBA CREDIT AND ZBA DEBIT TRANSACIONS */
							if (bankTrnDetails.getTypeCode().equals("275")) {
								GLInfoDAO dao = new GLInfoDAO();
								dao.addLedgerMain(bankStmtDetails.getUpldId(), bankHdrDtls.getHdrId(), bankTrnDetails,
										cmpyId, usrId, "ZBA CREDIT 275", bankStmtDetails.getCrtdDt());

							} else if (bankTrnDetails.getTypeCode().equals("575")) {
								GLInfoDAO dao = new GLInfoDAO();
								dao.addLedgerMain(bankStmtDetails.getUpldId(), bankHdrDtls.getHdrId(), bankTrnDetails,
										cmpyId, usrId, "ZBA DEBIT 575", bankStmtDetails.getCrtdDt());
							} else if (bankTrnDetails.getTypeCode().equals("581")) {
								GLInfoDAO dao = new GLInfoDAO();
								dao.addLedgerMain(bankStmtDetails.getUpldId(), bankHdrDtls.getHdrId(), bankTrnDetails,
										cmpyId, usrId, "Disbursing Debit 581", bankStmtDetails.getCrtdDt());
							}

						}

					}

				}
			}
		} else {
			starMaintenanceResponse.output.rtnSts = 1;
			starMaintenanceResponse.output.messages.add(rtnMsg);
		}

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starMaintenanceResponse));
	}

	@POST
	@Secured
	@Path("/read-je")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getPostedTransaction(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<APPostingBrowseInfo> starBrowseResponse = new BrowseResponse<APPostingBrowseInfo>();
		starBrowseResponse.setOutput(new APPostingBrowseInfo());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String trnRef = jsonObject.get("trnRef").getAsString();

		BankStmtDAO bankStmtDAO = new BankStmtDAO();

		bankStmtDAO.getJournalEntryTrs(starBrowseResponse, trnRef);

		bankStmtDAO.getJournalEntryTrsAccount(starBrowseResponse, trnRef);

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputPostingAP(starBrowseResponse));
	}

	public void getStatementList(BrowseResponse<BankStmtListBrowse> starBrowseResponse, String trnType) {
		Session session = null;
		Channel channel = null;
		try {
			JSch ssh = new JSch();

			if (trnType.equals("EBS")) {

				session = ssh.getSession("PLATEP_ADW34831_IN_P:", "sftp.tradinggrid.gxs.com", 22);
				session.setConfig("StrictHostKeyChecking", "no");
				session.setPassword("7508521U");
			} else if (trnType.equals("LCK")) {
				session = ssh.getSession("PLUS001P", "FTX-SERVSH.bnymellon.com", 22);
				session.setConfig("StrictHostKeyChecking", "no");
				session.setPassword("DJRG8JZ9");
			}
			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();

			ChannelSftp channelSftp = (ChannelSftp) channel;

			String curDirectory = channelSftp.pwd();

			if (trnType.equals("LCK")) {
				channelSftp.cd("/outbound/ylx");
				curDirectory = "/outbound/ylx";
			}

			System.out.println(curDirectory);

			Vector filelist = channelSftp.ls(curDirectory);

			for (int i = 0; i < 5; i++) {

				BankStmtList bankStmtList = new BankStmtList();

				LsEntry entry = (LsEntry) filelist.get(i);
				System.out.println(entry.getFilename());

				bankStmtList.setFlNm(entry.getFilename());

				Vector vec = channelSftp.ls(entry.getFilename());

				if (vec != null && vec.size() == 1) {
					LsEntry details = (LsEntry) vec.get(0);
					SftpATTRS attrs = details.getAttrs();

					int t = attrs.getMTime();
					Date modTime = new Date(t * 1000L);
					System.out.println(modTime);

					String pattern = "EEE MMM dd HH:mm:ss z yyyy";

					String formatPattern = "MM/dd/yyyy";

					String formattedDate = "";

					SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

					SimpleDateFormat simpleDateFormat1 = new SimpleDateFormat(formatPattern);

					try {
						Date date = simpleDateFormat.parse(modTime.toString());

						formattedDate = simpleDateFormat1.format(date);

					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					BankStmtDAO bankStmtDAO = new BankStmtDAO();

					int flg = bankStmtDAO.getBankTrnExist(formattedDate, trnType);

					if (trnType.equals("LCK")) {
						bankStmtList.setFlType("LCK");
					} else {
						bankStmtList.setFlType("EBS");
					}

					bankStmtList.setFlg(flg);
					bankStmtList.setDtTm(formattedDate);
				}

				starBrowseResponse.output.fldTblBnkStmt.add(bankStmtList);
			}

		} catch (JSchException e) {
			e.printStackTrace();
		} catch (SftpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (channel != null) {
				channel.disconnect();
			}
			if (session != null) {
				session.disconnect();
			}
		}
	}

	public boolean getFTPFile(String flNm, String destPath, String flTyp) {
		Session session = null;
		Channel channel = null;
		boolean flg = false;
		try {
			JSch ssh = new JSch();
			if (flTyp.equals("EBS")) {
				session = ssh.getSession("PLATEP_ADW34831_IN_P:", "sftp.tradinggrid.gxs.com", 22);
				session.setConfig("StrictHostKeyChecking", "no");
				session.setPassword("7508521U");
			} else if (flTyp.equals("LCK")) {
				session = ssh.getSession("PLUS001P", "FTX-SERVSH.bnymellon.com", 22);
				session.setConfig("StrictHostKeyChecking", "no");
				session.setPassword("DJRG8JZ9");
			}
			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();

			ChannelSftp channelSftp = (ChannelSftp) channel;

			String curDirectory = channelSftp.pwd();

			if (flTyp.equals("LCK")) {
				channelSftp.cd("/outbound/ylx");
				curDirectory = "/outbound/ylx";
			}

			System.out.println(curDirectory);

			channelSftp.get(flNm, destPath);

			flg = true;

		} catch (JSchException e) {
			e.printStackTrace();

		} catch (SftpException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (channel != null) {
				channel.disconnect();
			}
			if (session != null) {
				session.disconnect();
			}
		}

		return flg;
	}

	private String formattedDate() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MMddyyyyHHmmss");
		LocalDateTime now = LocalDateTime.now();
		return dtf.format(now);
	}

	private GenericEntity<?> getGenericBrowseOutput(BrowseResponse<InvoiceInfoBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<InvoiceInfoBrowseOutput>>(starBrowseResponse) {
		};
	}

	private GenericEntity<?> getGenericBrowseOutputStmt(BrowseResponse<BankStmtBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<BankStmtBrowseOutput>>(starBrowseResponse) {
		};
	}

	private GenericEntity<?> getGenericBrowseOutputStmtList(BrowseResponse<BankStmtListBrowse> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<BankStmtListBrowse>>(starBrowseResponse) {
		};
	}

	private GenericEntity<?> getGenericBrowseOutputStmtLckBx(
			BrowseResponse<LockBoxStmtBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<LockBoxStmtBrowseOutput>>(starBrowseResponse) {
		};
	}

	private GenericEntity<?> getGenericBrowseOutputGlEntry(BrowseResponse<GLEntryDataBrwose> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<GLEntryDataBrwose>>(starBrowseResponse) {
		};
	}

	private GenericEntity<?> getGenericBrowseOutputPostingAP(BrowseResponse<APPostingBrowseInfo> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<APPostingBrowseInfo>>(starBrowseResponse) {
		};
	}

	private GenericEntity<?> getGenericMaintenanceOutput(
			MaintanenceResponse<BnkStmtManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<BnkStmtManOutput>>(starMaintenanceResponse) {
		};
	}

	private GenericEntity<?> getGenericMaintenanceOutputStmt(
			MaintanenceResponse<BankStmtManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<BankStmtManOutput>>(starMaintenanceResponse) {
		};
	}
	
	@POST
	@Secured
	@Path("/del-trans")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response updateTrnStatus(@HeaderParam("user-id") String usrId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		MaintanenceResponse<BankStmtManOutput> starManResponse = new MaintanenceResponse<BankStmtManOutput>();
		starManResponse.setOutput(new BankStmtManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String ebsId = jsonObject.get("ebsId").getAsString();
		String hdrId = jsonObject.get("hdrId").getAsString();
		String trnId = jsonObject.get("trnId").getAsString();
		String upldId = jsonObject.get("upldId").getAsString();
		String trnTyp = jsonObject.get("typeCode").getAsString();
		
		BankStmtDAO bankStmtDAO = new BankStmtDAO();
		bankStmtDAO.updateTrnStatus(trnTyp, trnId, hdrId, upldId);

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputStmt(starManResponse));
	}
	
}
