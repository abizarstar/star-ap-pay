package com.star.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.dao.ReportDAO;
import com.star.linkage.report.LockBoxInforBrowseOutput;

@Path("/report")
public class ReportService {

	@POST
	@Secured
	@Path("/read-lckbx-rpt")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getLockboxInformation(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BrowseResponse<LockBoxInforBrowseOutput> starBrowseResponse = new BrowseResponse<LockBoxInforBrowseOutput>();
		starBrowseResponse.setOutput(new LockBoxInforBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String frmDpstDt = jsonObject.get("frmDpstDt").getAsString();
		String toDpstDt = jsonObject.get("toDpstDt").getAsString();
		String cusId = jsonObject.get("cus").getAsString();
		String sts = jsonObject.get("sts").getAsString();
		
		
		ReportDAO reportDAO = new ReportDAO();
		
		reportDAO.readLockBoxTrn(starBrowseResponse, frmDpstDt, toDpstDt, cusId, sts);
		
		return starResponseBuilder.getSuccessResponse(getLockBoxOutput(starBrowseResponse));
	}
	
	private GenericEntity<?> getLockBoxOutput(BrowseResponse<LockBoxInforBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<LockBoxInforBrowseOutput>>(starBrowseResponse) {
		};
	}
	
}
