package com.star.service;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.MaintanenceResponse;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.dao.WkfInfoDAO;
import com.star.linkage.wkf.WkfAprvr;
import com.star.linkage.wkf.WkfChkBrowseOutput;
import com.star.linkage.wkf.WorkflowInput;
import com.star.linkage.wkf.WorkflowManOutput;
import com.star.linkage.wkf.WorkflowStepInput;

@Path("/wkf")
public class WkfService {
	@POST
	@Secured
	@Path("/read-wkf-chk")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getWkfChk(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data)
			throws Exception {
		BrowseResponse<WkfChkBrowseOutput> starBrowseResponse = new BrowseResponse<WkfChkBrowseOutput>();
		starBrowseResponse.setOutput(new WkfChkBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String wfName = jsonObject.get("wfName").getAsString();

		WkfInfoDAO wkf = new WkfInfoDAO();

		// apInfo.createWkf(starBrowseResponse, wName,login);

		wkf.wkfChk(starBrowseResponse, wfName);

		return starResponseBuilder.getSuccessResponse(getWkfChkBrowseOutput(starBrowseResponse));
	}

	private GenericEntity<?> getWkfChkBrowseOutput(BrowseResponse<WkfChkBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<WkfChkBrowseOutput>>(starBrowseResponse) {
		};
	}

	@POST
	@Secured
	@Path("/vldt-stp")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getAPWkf(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data)
			throws Exception {

		WkfInfoDAO wkf = new WkfInfoDAO();
		MaintanenceResponse<WorkflowManOutput> starManResponse = new MaintanenceResponse<WorkflowManOutput>();
		starManResponse.setOutput(new WorkflowManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		WorkflowInput wrkflow = new WorkflowInput();
		wrkflow.setWkfNm(jsonObject.get("wkfName").getAsString());

		WorkflowStepInput workflowStepInput = new WorkflowStepInput();
		workflowStepInput.setWkfId(jsonObject.get("wkfId").getAsString());
		workflowStepInput.setStepName(jsonObject.get("stepName").getAsString());
		workflowStepInput.setAmtFlg(jsonObject.get("amtFlg").getAsInt());
		workflowStepInput.setBrhFlg(jsonObject.get("brhFlg").getAsInt());
		workflowStepInput.setVenFlg(jsonObject.get("venFlg").getAsInt());
		workflowStepInput.setDayFlg(jsonObject.get("dayFlg").getAsInt());
		workflowStepInput.setVchrCatFlg(jsonObject.get("catFlg").getAsInt());
		workflowStepInput.setAmtFrm(jsonObject.get("amtFrm").getAsFloat());
		workflowStepInput.setAmtTo(jsonObject.get("amtTo").getAsFloat());
		workflowStepInput.setBrh(jsonObject.get("brh").getAsString());
		workflowStepInput.setVenId(jsonObject.get("venId").getAsString());
		workflowStepInput.setVchrCat(jsonObject.get("vchrCat").getAsString());
		workflowStepInput.setApvr(jsonObject.get("apvr").getAsString());
		workflowStepInput.setDayBef(jsonObject.get("dayBef").getAsInt());
		workflowStepInput.setDays(jsonObject.get("days").getAsInt());
		workflowStepInput.setCrcnFlg(jsonObject.get("costRecon").getAsInt());

		BrowseResponse<WkfChkBrowseOutput> starBrowseResponse = new BrowseResponse<WkfChkBrowseOutput>();
		starBrowseResponse.setOutput(new WkfChkBrowseOutput());

		if (workflowStepInput.getWkfId().length() == 0) {
			wkf.wkfChk(starBrowseResponse, wrkflow.getWkfNm());
		}

		if (starBrowseResponse.output.nbrRecRtn == 0) {
			// wkf.chkSaveCondition(starBrowseResponse, workflowStepInput);
			wkf.validateStep(starBrowseResponse, workflowStepInput);
		} else {
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.setErrorMessage("Workflow " + wrkflow.getWkfNm() + " alredy Exist");
		}
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblWkfChk.size();
		return starResponseBuilder.getSuccessResponse(getWkfChkBrowseOutput(starBrowseResponse));
	}

	@POST
	@Secured
	@Path("/create-wkf")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response SaveWkf(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data)
			throws Exception {

		WkfInfoDAO wkf = new WkfInfoDAO();
		MaintanenceResponse<WorkflowManOutput> starManResponse = new MaintanenceResponse<WorkflowManOutput>();
		starManResponse.setOutput(new WorkflowManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		WorkflowInput wrkflow = new WorkflowInput();
		wrkflow.setWkfNm(jsonObject.get("wkfName").getAsString());

		WorkflowStepInput workflowStepInput = new WorkflowStepInput();
		workflowStepInput.setWkfId(jsonObject.get("wkfId").getAsString());
		workflowStepInput.setStepName(jsonObject.get("stepName").getAsString());
		workflowStepInput.setAmtFlg(jsonObject.get("amtFlg").getAsInt());
		workflowStepInput.setBrhFlg(jsonObject.get("brhFlg").getAsInt());
		workflowStepInput.setVenFlg(jsonObject.get("venFlg").getAsInt());
		workflowStepInput.setDayFlg(jsonObject.get("dayFlg").getAsInt());
		workflowStepInput.setAmtFrm(jsonObject.get("amtFrm").getAsFloat());
		workflowStepInput.setAmtTo(jsonObject.get("amtTo").getAsFloat());
		workflowStepInput.setBrh(jsonObject.get("brh").getAsString());
		workflowStepInput.setVenId(jsonObject.get("venId").getAsString());
		workflowStepInput.setVchrCat(jsonObject.get("vchrCat").getAsString());
		workflowStepInput.setVchrCatFlg(jsonObject.get("catFlg").getAsInt());

		JsonArray apvrList = jsonObject.get("apvr").getAsJsonArray();

		List<WkfAprvr> wkfAprvrList = new ArrayList<WkfAprvr>();

		for (int i = 0; i < apvrList.size(); i++) {
			JsonObject apvrObj = apvrList.get(i).getAsJsonObject();

			WkfAprvr aprvr = new WkfAprvr();

			aprvr.setOrder(apvrObj.get("order").getAsInt());
			aprvr.setUsrId(apvrObj.get("aprvId").getAsString());

			wkfAprvrList.add(aprvr);
		}

		workflowStepInput.setWkfAprvrList(wkfAprvrList);

		// workflowStepInput.setApvr(jsonObject.get("apvr").getAsString());
		workflowStepInput.setDayBef(jsonObject.get("dayBef").getAsInt());
		workflowStepInput.setDays(jsonObject.get("days").getAsInt());
		workflowStepInput.setCrcnFlg(jsonObject.get("costRecon").getAsInt());

		BrowseResponse<WkfChkBrowseOutput> starBrowseResponse = new BrowseResponse<WkfChkBrowseOutput>();
		starBrowseResponse.setOutput(new WkfChkBrowseOutput());

		if (workflowStepInput.getWkfId().length() == 0) {
			wkf.wkfChk(starBrowseResponse, wrkflow.getWkfNm());
		}

		if (starBrowseResponse.output.nbrRecRtn == 0) {
			// wkf.chkSaveCondition(starBrowseResponse, workflowStepInput);
			wkf.validateStep(starBrowseResponse, workflowStepInput);
			if (starBrowseResponse.output.fldTblWkfChk.size() == 0) {
				if (workflowStepInput.getWkfId().length() == 0) {
					wkf.wkfSave(starManResponse, wrkflow, workflowStepInput, userId);
				} else {
					wkf.updateWorkflow(starManResponse, wrkflow, workflowStepInput, userId);
				}
			} else {
				starBrowseResponse.output.rtnSts = 1;
				wkf.updWkf(userId, cmpyId, starBrowseResponse, workflowStepInput);
				starBrowseResponse.output.setErrorMessage("flg status is one Exist");
			}
		} else {
			starBrowseResponse.output.rtnSts = 1;
			starBrowseResponse.output.setErrorMessage("Workflow " + wrkflow.getWkfNm() + " already Exist");
		}
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblWkfChk.size();
		return starResponseBuilder.getSuccessResponse(getWkfChkBrowseOutput(starBrowseResponse));
	}
}
