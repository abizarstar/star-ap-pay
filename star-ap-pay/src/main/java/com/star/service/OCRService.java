package com.star.service;

import java.io.FileOutputStream;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.binary.Base64;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.MaintanenceResponse;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.common.abbyy.PerformOCR;
import com.star.dao.OCRProcessDAO;
import com.star.dao.PaymentDAO;
import com.star.linkage.ocr.OCRBrowseOutput;
import com.star.linkage.ocr.OCRManOutput;
import com.star.linkage.payment.PaymentBrowseOutput;
import com.star.linkage.payment.PaymentInfo;
import com.star.linkage.payment.PaymentManOutput;
import com.star.modal.CstmOcrVenMap;

@Path("/ocr")
public class OCRService {

	@POST
	@Path("/recognize")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response recognize(String data) throws Exception {
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		String[] base64ImgString = jsonObject.get("imgValue").getAsString().split(",");

		String[] tmpStr = base64ImgString[0].split(";");

		String[] imgExtn = tmpStr[0].split("/");

		String imgFlPath = CommonConstants.ROOT_DIR + "OCR_" + (new Date()).getTime() + "." + imgExtn[1];

		String txtFlPath = CommonConstants.ROOT_DIR + "OCR_" + (new Date()).getTime() + ".txt";

		byte[] base64DecodedByteArray = Base64.decodeBase64(base64ImgString[1]);

		FileOutputStream imageOutFile = new FileOutputStream(imgFlPath);
		imageOutFile.write(base64DecodedByteArray);
		imageOutFile.close();
		System.out.println("Image successfully encoded and decoded");

		PerformOCR performOCR = new PerformOCR();

		String[] inputArray = new String[3];

		inputArray[0] = "recognize";
		inputArray[1] = imgFlPath;
		inputArray[2] = txtFlPath;

		/* performOCR.performOcrOperation(inputArray); */

		String returnStr = "";

		returnStr = performOCR.performTessarect(inputArray);

		/* returnStr = readFile(txtFlPath); */

		System.out.println(returnStr);

		return Response.status(200).entity(returnStr).build();

	}
	
	@POST
	@Secured
	@Path("/save")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response addUpdateBank(@HeaderParam("user-id") String userId, String data) throws Exception {

		MaintanenceResponse<OCRManOutput> starManResponse = new MaintanenceResponse<OCRManOutput>();
		starManResponse.setOutput(new OCRManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String venIdFld = jsonObject.get("vendId").getAsString();
		String venNmStx = jsonObject.get("vendNmStx").getAsString();
		String venNmFld = jsonObject.get("vendNm").getAsString();
		String invNoFld = jsonObject.get("invNo").getAsString();
		String invDtFld = jsonObject.get("invDt").getAsString();
		String invAmtFld = jsonObject.get("invAmt").getAsString();
		String invCryFld = jsonObject.get("invCry").getAsString();
		
		OCRProcessDAO ocrProcessDAO = new OCRProcessDAO();
		
		List<CstmOcrVenMap> listVendor = ocrProcessDAO.getVendorList(venIdFld);
		
		if(listVendor.size() == 0)
		{
			ocrProcessDAO.saveOCRData(starManResponse, venIdFld, venNmFld, venNmStx, invNoFld, invDtFld, invAmtFld, invCryFld);
		}
		else
		{
			ocrProcessDAO.updateOCRData(starManResponse, venIdFld, venNmFld, venNmStx, invNoFld, invDtFld, invAmtFld, invCryFld);
		}
		
			
		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starManResponse));
	}
	
	
	@POST
	@Secured
	@Path("/del")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response delVendorMap(@HeaderParam("user-id") String userId, String data) throws Exception {

		MaintanenceResponse<OCRManOutput> starManResponse = new MaintanenceResponse<OCRManOutput>();
		starManResponse.setOutput(new OCRManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String venId = jsonObject.get("venId").getAsString();
		
		OCRProcessDAO ocrProcessDAO = new OCRProcessDAO();
		
		ocrProcessDAO.deleteOcrInfo(starManResponse, venId);
		
		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starManResponse));
	}
	
	
	@POST
	@Secured
	@Path("/read-ven")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getVendorList(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<OCRBrowseOutput> starBrowseResponse = new BrowseResponse<OCRBrowseOutput>();
		starBrowseResponse.setOutput(new OCRBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String venIdFld = jsonObject.get("vendId").getAsString();
		
		OCRProcessDAO ocrProcessDAO = new OCRProcessDAO();
		
		starBrowseResponse.output.fldTblVenList = ocrProcessDAO.getVendorList(venIdFld);
		
			
		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));
	}
	
	private GenericEntity<?> getGenericBrowseOutput(BrowseResponse<OCRBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<OCRBrowseOutput>>(starBrowseResponse) {
		};
	}
	
	private GenericEntity<?> getGenericMaintenanceOutput(
			MaintanenceResponse<OCRManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<OCRManOutput>>(starMaintenanceResponse) {
		};
	}

}
