package com.star.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.MaintanenceResponse;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.dao.GLInfoDAO;
import com.star.linkage.bank.BankManOutput;
import com.star.linkage.gl.GLBrowseOutput;
import com.star.linkage.gl.GLPostingData;
import com.star.linkage.gl.GLPostingDataBrowseOutput;
import com.star.linkage.gl.GlPostingDataManOutput;
import com.star.linkage.gl.SubAcctInfoBrowse;

@Path("/gl")
public class GLService {

	@POST
	@Secured
	@Path("/read")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getGL(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<GLBrowseOutput> starBrowseResponse = new BrowseResponse<GLBrowseOutput>();
		starBrowseResponse.setOutput(new GLBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		// JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		// String cmpyId = jsonObject.get("cmpyId").getAsString();

		GLInfoDAO vendorInfoDAO = new GLInfoDAO();

		vendorInfoDAO.readGLList(starBrowseResponse);
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblGL.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));
	}

	@POST
	@Secured
	@Path("/sub-acct-read")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getSubAcct(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BrowseResponse<SubAcctInfoBrowse> starBrowseResponse = new BrowseResponse<SubAcctInfoBrowse>();
		starBrowseResponse.setOutput(new SubAcctInfoBrowse());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		GLInfoDAO vendorInfoDAO = new GLInfoDAO();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String glAcct = jsonObject.get("glAcct").getAsString();

		vendorInfoDAO.readSubAcctList(starBrowseResponse, glAcct, cmpyId);
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblSubAcct.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputSubAcct(starBrowseResponse));
	}

	@POST
	@Secured
	@Path("/add-acct-post")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response addGlPostingInfo(@HeaderParam("user-id") String userId, String data) throws Exception {

		MaintanenceResponse<GlPostingDataManOutput> starManResponse = new MaintanenceResponse<GlPostingDataManOutput>();
		starManResponse.setOutput(new GlPostingDataManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		GLInfoDAO vendorInfoDAO = new GLInfoDAO();

		GLPostingData glPostingInfo = new Gson().fromJson(data, GLPostingData.class);

		if (glPostingInfo.getCrAcct1().equals(glPostingInfo.getDrAcct1())) {
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add("Credit and Debit Account can not be same");
		}

		if (starManResponse.output.rtnSts == 0) {
			vendorInfoDAO.addBankDetails(starManResponse, glPostingInfo);
		}

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starManResponse));
	}

	@POST
	@Secured
	@Path("/get-acct-post")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getGlPostingInfo(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<GLPostingDataBrowseOutput> starBrowseResponse = new BrowseResponse<GLPostingDataBrowseOutput>();
		starBrowseResponse.setOutput(new GLPostingDataBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		GLInfoDAO glInfoDAO = new GLInfoDAO();

		glInfoDAO.getGLPostingDetails(starBrowseResponse);

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputGlPosting(starBrowseResponse));
	}

	@POST
	@Secured
	@Path("/del-acct-post")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response delGlPostingInfo(@HeaderParam("user-id") String userId, String data) throws Exception {

		MaintanenceResponse<GlPostingDataManOutput> starManResponse = new MaintanenceResponse<GlPostingDataManOutput>();
		starManResponse.setOutput(new GlPostingDataManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		String postId = "";
		GLInfoDAO vendorInfoDAO = new GLInfoDAO();
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		postId = jsonObject.get("postId").getAsString();
		vendorInfoDAO.delGlPosInfo(starManResponse, postId);

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starManResponse));
	}

	private GenericEntity<?> getGenericBrowseOutputGlPosting(
			BrowseResponse<GLPostingDataBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<GLPostingDataBrowseOutput>>(starBrowseResponse) {
		};
	}

	private GenericEntity<?> getGenericMaintenanceOutput(
			MaintanenceResponse<GlPostingDataManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<GlPostingDataManOutput>>(starMaintenanceResponse) {
		};
	}

	private GenericEntity<?> getGenericBrowseOutput(BrowseResponse<GLBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<GLBrowseOutput>>(starBrowseResponse) {
		};
	}

	private GenericEntity<?> getGenericBrowseOutputSubAcct(BrowseResponse<SubAcctInfoBrowse> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<SubAcctInfoBrowse>>(starBrowseResponse) {
		};
	}

}
