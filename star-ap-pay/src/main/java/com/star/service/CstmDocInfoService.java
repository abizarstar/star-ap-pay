package com.star.service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.BodyPart;
import org.glassfish.jersey.media.multipart.ContentDisposition;
import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.CommonFunctions;
import com.star.common.MaintanenceResponse;
import com.star.common.ManageDirectory;
import com.star.common.ParseInvoice;
import com.star.common.Secured;
import com.star.common.SendExternalReq;
import com.star.common.StarResponseBuilder;
import com.star.common.ValidateValue;
import com.star.common.abbyy.PerformOCR;
import com.star.dao.CstmDocInfoDAO;
import com.star.dao.CstmVchrInfoDAO;
import com.star.linkage.auth.AuthPlnBrowseOutput;
import com.star.linkage.cstmdocinfo.CstmDocAddBrowseOutput;
import com.star.linkage.cstmdocinfo.CstmDocAddInfoInput;
import com.star.linkage.cstmdocinfo.CstmDocInfoBrowseOutput;
import com.star.linkage.cstmdocinfo.CstmDocInfoInput;
import com.star.linkage.cstmdocinfo.CstmDocInfoManOutput;
import com.star.linkage.cstmdocinfo.DocumentHeader;
import com.star.linkage.cstmdocinfo.GetReconBrowse;
import com.star.linkage.cstmdocinfo.ValidateInvoiceBrowse;
import com.star.linkage.cstmdocinfo.ValidatePOBrowse;
import com.star.linkage.cstmdocinfo.VchrInfo;
import com.star.linkage.cstmvchrinfo.CstmVchrInfoInput;
import com.star.linkage.cstmvchrinfo.CstmVchrInfoManOutput;
import com.star.modal.CstmDocAddInfo;
import com.star.modal.CstmDocInfo;

@Path("/cstmdoc")
public class CstmDocInfoService {

	@POST
	@Secured
	@Path("/readdoc")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getDocument(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			@HeaderParam("user-grp") String userGrp, String data) throws Exception {

		List<DocumentHeader> tblHeaderList = new ArrayList<DocumentHeader>();

		BrowseResponse<CstmDocInfoBrowseOutput> starBrowseResponse = new BrowseResponse<CstmDocInfoBrowseOutput>();

		starBrowseResponse.setOutput(new CstmDocInfoBrowseOutput());

		try {
			CstmDocInfoDAO docInfoDAO = new CstmDocInfoDAO();

			JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

			String trnSts = jsonObject.get("trnSts").getAsString();
			String year = jsonObject.get("year").getAsString();

			int pageNo = 0;

			if (jsonObject.has("pageNo")) {
				pageNo = Integer.parseInt(jsonObject.get("pageNo").getAsString());
			}

			if (!trnSts.equals("H") && !trnSts.equals("R")) {
				docInfoDAO.readStxFields(starBrowseResponse, trnSts, pageNo, cmpyId, year);
			}

			if (trnSts.length() == 0 && pageNo == 0) {
				docInfoDAO.readFields(starBrowseResponse, trnSts, userId, userGrp, year);
			}

			if (trnSts.equals("H") || trnSts.equals("R")) {
				docInfoDAO.readFields(starBrowseResponse, trnSts, userId, userGrp, year);
			}

			docInfoDAO.readCounts(starBrowseResponse, userId, cmpyId, year);
			docInfoDAO.readCountDocument(starBrowseResponse, userId, userGrp, year);

			StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

			starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblDoc.size();

			starBrowseResponse.output.fldTblHdrList = tblHeaderList;

			if (starBrowseResponse.output.toReviewCount > pageNo * 100) {
				starBrowseResponse.output.eof = 0;
			} else {
				starBrowseResponse.output.eof = 1;
			}

			return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));

		} catch (Exception e) {
			throw e;
		}

	}

	@POST
	@Secured
	@Path("/viewdoc")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response veiwRecord(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		List<DocumentHeader> tblHeaderList = new ArrayList<DocumentHeader>();

		BrowseResponse<CstmDocInfoBrowseOutput> starBrowseResponse = new BrowseResponse<CstmDocInfoBrowseOutput>();

		starBrowseResponse.setOutput(new CstmDocInfoBrowseOutput());

		try {
			CstmDocInfoDAO docInfoDAO = new CstmDocInfoDAO();

			JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

			int ctlNo = jsonObject.get("ctlNo").getAsInt();

			docInfoDAO.viewRecord(starBrowseResponse, ctlNo, cmpyId);

			StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

			starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblDoc.size();

			// starBrowseResponse.output.fldTblHdrList = tblHeaderList;
			starBrowseResponse.output.fldTblHdrList = tblHeaderList;
			return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));

		} catch (Exception e) {
			throw e;
		}

	}

	private GenericEntity<?> getGenericBrowseOutput(BrowseResponse<CstmDocInfoBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<CstmDocInfoBrowseOutput>>(starBrowseResponse) {
		};
	}

	@POST
	@Secured
	@RolesAllowed("ADMIN")
	@Path("/adddoc")
	@Consumes({ MediaType.MULTIPART_FORM_DATA })
	public Response uploadMultiplePdfFiles(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			@FormDataParam("file") FormDataBodyPart body,@FormDataParam("category") String category) throws Exception {
		// TODO ,@HeaderParam("user-id") String userId
		ManageDirectory directory = new ManageDirectory();

		int fileCount = 0;

		ValidateValue validateValue = new ValidateValue();

		for (BodyPart part : body.getParent().getBodyParts()) {

			System.out.println(part.getContentDisposition().getParameters().size());

			InputStream is = part.getEntityAs(InputStream.class);
			ContentDisposition meta = part.getContentDisposition();

			if (meta.getFileName() != null) {

				/* Calling Doc Sequence to generate unique Doc Name */
				CstmDocInfoDAO cstmDocInfoDAO = new CstmDocInfoDAO();

				int ctlNo = cstmDocInfoDAO.getCtlNo();

				String strCtlNo = String.valueOf(ctlNo);

				String fileName = "0000000000".substring(strCtlNo.length()) + strCtlNo;

				String docNmIntrnl = "upload" + "-" + fileName;

				String fileExtn = validateValue.getFileExtension(meta.getFileName());

				/* CREATE FILE BASED ON THE INPUT */
				directory.createFile(docNmIntrnl, fileExtn, is);

				String imgFlPath = docNmIntrnl + "." + fileExtn;
				String txtFlPath = docNmIntrnl + ".txt";

				PerformOCR performOCR = new PerformOCR();

				String[] inputArray = new String[3];

				inputArray[0] = "recognize";
				inputArray[1] = CommonConstants.ROOT_DIR + imgFlPath;
				inputArray[2] = CommonConstants.ROOT_DIR + txtFlPath;

				performOCR.performOcrOperation(inputArray);

				MaintanenceResponse<CstmDocInfoManOutput> starMaintenanceDocResponse = new MaintanenceResponse<CstmDocInfoManOutput>();

				starMaintenanceDocResponse.setOutput(new CstmDocInfoManOutput());

				CstmDocInfoInput cstmDocInfoInput = new CstmDocInfoInput();

				cstmDocInfoInput.setCtlNo(ctlNo);
				// TODO
				cstmDocInfoInput.setGatCtlNo(1);
				cstmDocInfoInput.setFlPdf(imgFlPath);
				cstmDocInfoInput.setFlPdfName(meta.getFileName());
				cstmDocInfoInput.setFlTxt(txtFlPath);
				cstmDocInfoInput.setSrcFlg("S");
				cstmDocInfoInput.setIsPrs(false);
				cstmDocInfoInput.setUpldBy(userId);

				cstmDocInfoDAO.addCstmDocument(starMaintenanceDocResponse, cstmDocInfoInput);

				MaintanenceResponse<CstmVchrInfoManOutput> starMaintenanceResponse = new MaintanenceResponse<CstmVchrInfoManOutput>();

				starMaintenanceResponse.setOutput(new CstmVchrInfoManOutput());

				CstmVchrInfoDAO cstmVchrInfoDAO = new CstmVchrInfoDAO();

				List<CstmVchrInfoInput> cstmVchrInfo = new ArrayList<CstmVchrInfoInput>();
				
				ParseInvoice parseInvoice = new ParseInvoice();

				parseInvoice.parseInvoiceData(cmpyId, userId, txtFlPath, cstmVchrInfo, ctlNo, cstmVchrInfoDAO);

				WkfMapService mapService = new WkfMapService();
				
				for(int i =0; i < cstmVchrInfo.size(); i++)
				{
					if(i > 0)
					{
						ctlNo = cstmDocInfoDAO.getCtlNo();
	
						starMaintenanceDocResponse = new MaintanenceResponse<CstmDocInfoManOutput>();
	
						starMaintenanceDocResponse.setOutput(new CstmDocInfoManOutput());
	
						cstmDocInfoInput = new CstmDocInfoInput();
	
						cstmDocInfoInput.setCtlNo(ctlNo);
						// TODO
						cstmDocInfoInput.setGatCtlNo(1);
						cstmDocInfoInput.setFlPdf(imgFlPath);
						cstmDocInfoInput.setFlPdfName(meta.getFileName());
						cstmDocInfoInput.setFlTxt(txtFlPath);
						cstmDocInfoInput.setSrcFlg("S");
						cstmDocInfoInput.setIsPrs(false);
						cstmDocInfoInput.setUpldBy(userId);
	
						cstmDocInfoDAO.addCstmDocument(starMaintenanceDocResponse, cstmDocInfoInput);
					}
					
					cstmVchrInfo.get(i).setVchrCtlNo(ctlNo);
					
					/* VALIDATE WORK FLOW SERVICE */
					mapService.validateVouWkf(cmpyId, userId, cstmVchrInfo.get(i));
					cstmVchrInfo.get(i).setVchrCat(category.trim());
					cstmVchrInfoDAO.addCstmVoucher(starMaintenanceResponse, cstmVchrInfo.get(i));
					
				}

				fileCount = fileCount + 1;
			}

		}

		System.out.println(fileCount);

		return Response.ok("Data uploaded successfully !!").build();
	}
	
	@POST
	@Secured
	@RolesAllowed("ADMIN")
	@Path("/adddoc-addtn")
	@Consumes({ MediaType.MULTIPART_FORM_DATA })
	public Response uploadAdditionalFiles(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			@FormDataParam("file") FormDataBodyPart body,@FormDataParam("ctlNo") String ctlNo,
			@FormDataParam("cmnt") String cmnt,@FormDataParam("vchrNo") String vchrNo) throws Exception {
		
		MaintanenceResponse<CstmDocInfoManOutput> starMaintenanceDocResponse = new MaintanenceResponse<CstmDocInfoManOutput>();
		
		starMaintenanceDocResponse.setOutput(new CstmDocInfoManOutput());
		
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		ManageDirectory directory = new ManageDirectory();

		int fileCount = 0;

		ValidateValue validateValue = new ValidateValue();

		for (BodyPart part : body.getParent().getBodyParts()) {

			System.out.println(part.getContentDisposition().getParameters().size());

			InputStream is = part.getEntityAs(InputStream.class);
			ContentDisposition meta = part.getContentDisposition();

			if (meta.getFileName() != null) {

				/* Calling Doc Sequence to generate unique Doc Name */
				CstmDocInfoDAO cstmDocInfoDAO = new CstmDocInfoDAO();

				int idocCount = cstmDocInfoDAO.getAdditionalDocCount(ctlNo);
				
				idocCount = idocCount + 1;
				
				String fileName = "0000000000".substring(String.valueOf(idocCount).length()) + idocCount;

				String docNmIntrnl = "Additional_Doc" + "-" + fileName;

				String fileExtn = validateValue.getFileExtension(meta.getFileName());

				/* CREATE FILE BASED ON THE INPUT */
				directory.createFile(docNmIntrnl, fileExtn, is);

				String imgFlPath = docNmIntrnl + "." + fileExtn;
				
				

				CstmDocAddInfoInput cstmDocAddInfoInput = new CstmDocAddInfoInput();

				cstmDocAddInfoInput.setDocCtlNo(Integer.parseInt(ctlNo));
				// TODO
				cstmDocAddInfoInput.setFlNm(imgFlPath);
				cstmDocAddInfoInput.setFlOrgName(meta.getFileName());
				cstmDocAddInfoInput.setUpldBy(userId);
				cstmDocAddInfoInput.setVchrNo(vchrNo);

				cstmDocInfoDAO.addCstmAddDocument(starMaintenanceDocResponse, cstmDocAddInfoInput);

				MaintanenceResponse<CstmVchrInfoManOutput> starMaintenanceResponse = new MaintanenceResponse<CstmVchrInfoManOutput>();

				starMaintenanceResponse.setOutput(new CstmVchrInfoManOutput());

				fileCount = fileCount + 1;
			}

		}

		System.out.println(fileCount);

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputUploadDoc(starMaintenanceDocResponse));
	}

	@POST
	@Path("/con")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getTxtFromPDF(String data) throws Exception {
		// JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		// String[] base64ImgString =
		// jsonObject.get("imgValue").getAsString().split(",");

		// String[] tmpStr = base64ImgString[0].split(";");

		// String[] imgExtn = tmpStr[0].split("/");

		// String imgFlPath = CommonConstants.ROOT_DIR + "OCR_" + (new Date()).getTime()
		// + "." + imgExtn[1];
		// String txtFlPath = CommonConstants.ROOT_DIR + "OCR_" + (new Date()).getTime()
		// + ".txt";

		String imgFlPath = CommonConstants.ROOT_DIR + "Test.pdf";
		String txtFlPath = CommonConstants.ROOT_DIR + "Test.txt";

		// byte[] base64DecodedByteArray = Base64.decodeBase64(base64ImgString[1]);

		// FileOutputStream imageOutFile = new FileOutputStream(imgFlPath);
		// imageOutFile.write(base64DecodedByteArray);
		// imageOutFile.close();
		// System.out.println("Image successfully encoded and decoded");

		PerformOCR performOCR = new PerformOCR();

		String[] inputArray = new String[3];

		inputArray[0] = "recognize";
		inputArray[1] = imgFlPath;
		inputArray[2] = txtFlPath;

		performOCR.performOcrOperation(inputArray);

		String returnStr = "";

		// returnStr = performOCR.performTessarect(inputArray);

		// returnStr = readFile(txtFlPath);

		// System.out.println(returnStr);

		return Response.status(200).entity(returnStr).build();

	}

	@POST
	@Secured
	@Path("/pdf")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getPdf(@HeaderParam("user-id") String userId, String data) throws Exception {

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		int ctlNo = jsonObject.get("ctlNo").getAsInt();

		CstmDocInfoDAO cstmDocInfoDAO = new CstmDocInfoDAO();

		String PdfLocation = cstmDocInfoDAO.getPdf(ctlNo);

		return Response.status(200).entity(PdfLocation).build();
	}

	@GET
	@Path("/download")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response downloadPdfFile(@HeaderParam("user-id") String userId, @QueryParam("ctlNo") int ctlNo)
			throws Exception {

		ManageDirectory directory = new ManageDirectory();

		/*
		 * BrowseResponse<CstmDocInfoBrowseOutput> starBrowseResponse = new
		 * BrowseResponse<CstmDocInfoBrowseOutput>();
		 * 
		 * starBrowseResponse.setOutput(new CstmDocInfoBrowseOutput());
		 */

		/*
		 * JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		 * 
		 * int ctlNo = jsonObject.get("ctlNo").getAsInt();
		 */

		CstmDocInfoDAO docInfoDAO = new CstmDocInfoDAO();

		List<Object[]> lst = docInfoDAO.getPdfName(ctlNo);

		String fileName = "", origName = "";

		for (Object[] cstmVal : lst) {

			fileName = String.valueOf(cstmVal[0]);
			origName = String.valueOf(cstmVal[1]);
		}

		InputStream fileInputStream;
		javax.ws.rs.core.Response.ResponseBuilder responseBuilder = null;

		fileInputStream = directory.downloadPdf(fileName);

		responseBuilder = javax.ws.rs.core.Response.ok((Object) fileInputStream);

		responseBuilder.type("application/pdf");
		responseBuilder.header("Content-Disposition", "attachment;filename=" + origName);

		return responseBuilder.build();
	}

	@GET
	@Path("/downloadget")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response viewPdfFile(@HeaderParam("user-id") String userId, @QueryParam("ctlNo") int ctlNo)
			throws Exception {

		ManageDirectory directory = new ManageDirectory();

		CstmDocInfoDAO docInfoDAO = new CstmDocInfoDAO();

		List<Object[]> lst = docInfoDAO.getPdfName(ctlNo);

		String fileName = "", origName = "";

		for (Object[] cstmVal : lst) {

			fileName = String.valueOf(cstmVal[0]);
			origName = String.valueOf(cstmVal[1]);
		}

		InputStream fileInputStream;
		javax.ws.rs.core.Response.ResponseBuilder responseBuilder = null;

		fileInputStream = directory.downloadPdf(fileName);

		responseBuilder = javax.ws.rs.core.Response.ok((Object) fileInputStream);

		responseBuilder.type("application/pdf");
		responseBuilder.header("Content-Disposition", "inline;filename=" + origName);

		return responseBuilder.build();

	}
	
	
	@GET
	@Path("/downloadAddtn")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response getDownloadPdfFile(@HeaderParam("user-id") String userId, @QueryParam("ctlNo") int ctlNo)
			throws Exception {

		ManageDirectory directory = new ManageDirectory();

		ValidateValue validateValue = new ValidateValue();
		
		CstmDocInfoDAO docInfoDAO = new CstmDocInfoDAO();

		List<Object[]> lst = docInfoDAO.getDocAddtn(ctlNo);

		String fileName = "", origName = "";

		for (Object[] cstmVal : lst) {

			origName = String.valueOf(cstmVal[0]);
			fileName = String.valueOf(cstmVal[1]);
		}
		
		String fileExtn = validateValue.getFileExtension(fileName);
		
		InputStream fileInputStream;
		javax.ws.rs.core.Response.ResponseBuilder responseBuilder = null;

		fileInputStream = directory.downloadPdf(fileName);

		responseBuilder = javax.ws.rs.core.Response.ok((Object) fileInputStream);

		responseBuilder.type("application/" + fileExtn);
		responseBuilder.header("Content-Disposition", "inline;filename=" + origName);

		return responseBuilder.build();

	}
	
	@POST
	@Path("/upddoc")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response updateVoucherInfo(@HeaderParam("user-id") String userId, String data) throws Exception {
		MaintanenceResponse<CstmDocInfoManOutput> starMaintenanceResponse = new MaintanenceResponse<CstmDocInfoManOutput>();

		starMaintenanceResponse.setOutput(new CstmDocInfoManOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		// DocInfo detailsInput = new Gson().fromJson(data, DocInfo.class);

		VchrInfo detailsInput = new VchrInfo();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		CommonFunctions objCom = new CommonFunctions();

		detailsInput.setVchrCtlNo(jsonObject.get("vchrCtlNo").getAsInt());
		detailsInput.setVchrInvNo(jsonObject.get("vchrInvNo").getAsString());
		detailsInput.setVchrVenId(jsonObject.get("vchrVenId").getAsString());
		detailsInput.setVchrVenNm(jsonObject.get("vchrVenNm").getAsString());
		detailsInput.setVchrBrh(jsonObject.get("vchrBrh").getAsString());
		detailsInput.setVchrAmt(Double.parseDouble(jsonObject.get("vchrAmt").getAsString()));
		detailsInput.setVchrExtRef(jsonObject.get("vchrExtRef").getAsString());
		detailsInput.setVchrInvDt(objCom.formatDateWithoutTime(jsonObject.get("vchrInvDt").getAsString()));
		detailsInput.setVchrDueDt(objCom.formatDateWithoutTime(jsonObject.get("vchrDueDt").getAsString()));
		detailsInput.setVchrPayTerm(jsonObject.get("vchrPayTerm").getAsString());
		detailsInput.setVchrCry(jsonObject.get("vchrCry").getAsString());
		
		/* SET PO NO ITEM IF AVAILABLE */
		if (jsonObject.get("poNo").getAsString().length() > 0) {
			String poStr = jsonObject.get("poNo").getAsString();

			String[] arrPoStr = poStr.split("-");

			detailsInput.setPoPfx("PO");
			detailsInput.setPoNo(arrPoStr[0]);

			if (arrPoStr.length > 1) {
				detailsInput.setPoItm(arrPoStr[1]);
			}
			else
			{
				detailsInput.setPoItm("");
			}
		}
		else
		{
			detailsInput.setPoNo("");
			detailsInput.setPoItm("");
			
		}

		CstmDocInfoDAO cstmDocDAO = new CstmDocInfoDAO();

		JsonArray reconList = jsonObject.get("costRecon").getAsJsonArray();

		JsonArray glList = jsonObject.get("glData").getAsJsonArray();

		cstmDocDAO.updDocDtls(starMaintenanceResponse, detailsInput, reconList, glList);

		System.out.println(detailsInput.getVchrCtlNo());
		System.out.println(detailsInput.getVchrVenId());

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starMaintenanceResponse));

	}

	private GenericEntity<?> getGenericMaintenanceOutput(MaintanenceResponse<CstmDocInfoManOutput> starBrowseResponse) {
		return new GenericEntity<MaintanenceResponse<CstmDocInfoManOutput>>(starBrowseResponse) {
		};
	}
	
	private GenericEntity<?> getGenericMaintenanceOutputUploadDoc(MaintanenceResponse<CstmDocInfoManOutput> starBrowseResponse) {
		return new GenericEntity<MaintanenceResponse<CstmDocInfoManOutput>>(starBrowseResponse) {
		};
	}

	@POST
	@Secured
	@Path("/prsdoc")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response prsOrPaidRecord(@HeaderParam("user-id") String userId, String data) throws Exception {

		List<DocumentHeader> tblHeaderList = new ArrayList<DocumentHeader>();

		BrowseResponse<CstmDocInfoBrowseOutput> starBrowseResponse = new BrowseResponse<CstmDocInfoBrowseOutput>();

		starBrowseResponse.setOutput(new CstmDocInfoBrowseOutput());

		try {
			CstmDocInfoDAO docInfoDAO = new CstmDocInfoDAO();

			JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

			Boolean isPrs = jsonObject.get("isPrs").getAsBoolean();

			docInfoDAO.isPrsOrPaidFields(starBrowseResponse, isPrs);

			StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

			starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblDoc.size();

			// starBrowseResponse.output.fldTblHdrList = tblHeaderList;
			starBrowseResponse.output.fldTblHdrList = tblHeaderList;
			return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));

		} catch (Exception e) {
			throw e;
		}

	}

	@POST
	@Secured
	@Path("/deldoc")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response deleletDoc(@HeaderParam("user-id") String userId, String data) throws Exception {

		MaintanenceResponse<CstmDocInfoManOutput> starManResponse = new MaintanenceResponse<CstmDocInfoManOutput>();

		starManResponse.setOutput(new CstmDocInfoManOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		int ctlNo = jsonObject.get("ctlNo").getAsInt();

		CstmDocInfoDAO cstmDocInfoDAO = new CstmDocInfoDAO();

		cstmDocInfoDAO.deleteDoc(starManResponse, ctlNo);

		return starResponseBuilder.getSuccessResponse(getGenericManOutput(starManResponse));

	}

	@POST
	@Secured
	@Path("/rejdoc")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response rejectDoc(@HeaderParam("user-id") String userId, String data) throws Exception {

		MaintanenceResponse<CstmDocInfoManOutput> starManResponse = new MaintanenceResponse<CstmDocInfoManOutput>();

		starManResponse.setOutput(new CstmDocInfoManOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		int ctlNo = jsonObject.get("ctlNo").getAsInt();
		String trnRmk = jsonObject.get("trnRmk").getAsString();

		CstmDocInfoDAO cstmDocInfoDAO = new CstmDocInfoDAO();

		cstmDocInfoDAO.rejectDoc(starManResponse, ctlNo, trnRmk);

		return starResponseBuilder.getSuccessResponse(getGenericManOutput(starManResponse));

	}

	private GenericEntity<?> getGenericManOutput(MaintanenceResponse<CstmDocInfoManOutput> starBrowseResponse) {
		return new GenericEntity<MaintanenceResponse<CstmDocInfoManOutput>>(starBrowseResponse) {
		};
	}

	@POST
	@Secured
	@Path("/validate-inv")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response validateInv(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<ValidateInvoiceBrowse> starBrowseResponse = new BrowseResponse<ValidateInvoiceBrowse>();

		starBrowseResponse.setOutput(new ValidateInvoiceBrowse());

		try {
			CstmDocInfoDAO docInfoDAO = new CstmDocInfoDAO();

			JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

			String venId = jsonObject.get("venId").getAsString();
			String invNo = jsonObject.get("invNo").getAsString();

			String vchrNo = docInfoDAO.validateInvoice(venId, invNo);

			if (vchrNo.length() > 0) {
				starBrowseResponse.output.vchrRefNo = vchrNo;
				starBrowseResponse.output.rtnSts = 1;
			}

			StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

			return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputValidInv(starBrowseResponse));

		} catch (Exception e) {
			throw e;
		}

	}

	@POST
	@Secured
	@Path("/validate-po")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response validatePO(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<ValidatePOBrowse> starBrowseResponse = new BrowseResponse<ValidatePOBrowse>();

		starBrowseResponse.setOutput(new ValidatePOBrowse());

		try {
			CstmDocInfoDAO docInfoDAO = new CstmDocInfoDAO();

			JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

			String poNo = jsonObject.get("poNo").getAsString();
			String poItm = jsonObject.get("poItm").getAsString();

			String vendorId = docInfoDAO.validatePO(poNo, poItm);

			if (vendorId.length() > 0) {
				starBrowseResponse.output.venId = vendorId;
				starBrowseResponse.output.rtnSts = 0;
			} else {
				starBrowseResponse.output.rtnSts = 1;
			}

			StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

			return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputValidPO(starBrowseResponse));

		} catch (Exception e) {
			throw e;
		}

	}

	@POST
	@Secured
	@Path("/get-recon-data")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getReconData(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		BrowseResponse<GetReconBrowse> starBrowseResponse = new BrowseResponse<GetReconBrowse>();

		starBrowseResponse.setOutput(new GetReconBrowse());

		try {
			CstmDocInfoDAO docInfoDAO = new CstmDocInfoDAO();

			JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

			String venId = jsonObject.get("venId").getAsString();
			String poNo = jsonObject.get("poNo").getAsString();
			String cry = jsonObject.get("cry").getAsString();
			String exRef = jsonObject.get("exRef").getAsString();

			VchrInfo vchrInfo = new VchrInfo();

			docInfoDAO.getReconDetails(cmpyId, venId, poNo, cry, exRef, vchrInfo);

			starBrowseResponse.output.reconAmt = vchrInfo.getReconAmt();
			starBrowseResponse.output.reconCount = vchrInfo.getReconCount();
			starBrowseResponse.output.reconAmtStr = vchrInfo.getReconAmtStr();

			StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

			return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputRecon(starBrowseResponse));

		} catch (Exception e) {
			throw e;
		}

	}

	private GenericEntity<?> getGenericBrowseOutputRecon(BrowseResponse<GetReconBrowse> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<GetReconBrowse>>(starBrowseResponse) {
		};
	}

	private GenericEntity<?> getGenericBrowseOutputValidInv(BrowseResponse<ValidateInvoiceBrowse> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<ValidateInvoiceBrowse>>(starBrowseResponse) {
		};
	}

	private GenericEntity<?> getGenericBrowseOutputValidPO(BrowseResponse<ValidatePOBrowse> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<ValidatePOBrowse>>(starBrowseResponse) {
		};
	}

	@POST
	@Secured
	@Path("/updSts")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response updateDocStatus(@HeaderParam("user-id") String userId, String data) throws Exception {

		MaintanenceResponse<CstmDocInfoManOutput> starManResponse = new MaintanenceResponse<CstmDocInfoManOutput>();

		starManResponse.setOutput(new CstmDocInfoManOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		int ctlNo = jsonObject.get("ctlNo").getAsInt();
		String rmk = jsonObject.get("rmk").getAsString();
		String trnSts = jsonObject.get("trnSts").getAsString();

		CstmDocInfoDAO cstmDocInfoDAO = new CstmDocInfoDAO();

		cstmDocInfoDAO.updateDocStatus(starManResponse, ctlNo, trnSts, rmk);

		return starResponseBuilder.getSuccessResponse(getGenericManOutput(starManResponse));

	}

	@POST
	@Secured
	@Path("/readdoc-addtn")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getAddtnDocument(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			@HeaderParam("user-grp") String userGrp, String data) throws Exception {

		BrowseResponse<CstmDocAddBrowseOutput> starBrowseResponse = new BrowseResponse<CstmDocAddBrowseOutput>();

		starBrowseResponse.setOutput(new CstmDocAddBrowseOutput());

		try {
			CstmDocInfoDAO docInfoDAO = new CstmDocInfoDAO();

			JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

			String ctlNo = jsonObject.get("ctlNo").getAsString();
			String vchrNo = jsonObject.get("vchrNo").getAsString();
			String poNo = jsonObject.get("poNo").getAsString();

			docInfoDAO.getAdditionalDocList(starBrowseResponse, ctlNo, vchrNo);
			docInfoDAO.getAdditionalSTXDocList(starBrowseResponse, poNo, vchrNo, cmpyId,ctlNo);

			StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

			starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblDocAdd.size();

			return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputAddDoc(starBrowseResponse));

		} catch (Exception e) {
			throw e;
		}

	}
	
	
	@POST
	@Secured
	@Path("/deldoc-addtn")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response delAddtnDocument(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			@HeaderParam("user-grp") String userGrp, String data) throws Exception {

		MaintanenceResponse<CstmDocInfoManOutput> starMaintenanceDocResponse = new MaintanenceResponse<CstmDocInfoManOutput>();
		starMaintenanceDocResponse.setOutput(new CstmDocInfoManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		CstmDocInfoDAO docInfoDAO = new CstmDocInfoDAO();
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String docCtlNo = jsonObject.get("ctlNo").getAsString();
		docInfoDAO.deleteAdditionDoc(starMaintenanceDocResponse, docCtlNo);

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputUploadDoc(starMaintenanceDocResponse));

	}
	
	private GenericEntity<?> getGenericBrowseOutputAddDoc(BrowseResponse<CstmDocAddBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<CstmDocAddBrowseOutput>>(starBrowseResponse) {
		};
	}

	
	
}
