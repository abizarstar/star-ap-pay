package com.star.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.auth.oauth2.TokenResponse;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.auth.oauth2.GoogleRefreshTokenRequest;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.ListMessagesResponse;
import com.google.api.services.gmail.model.Message;
import com.google.api.services.gmail.model.MessagePart;
import com.google.api.services.gmail.model.MessagePartBody;
import com.google.api.services.gmail.model.MessagePartHeader;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.MaintanenceResponse;
import com.star.common.ReadMailService;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.common.UpldFileToApp;
import com.star.dao.UsrEmlConfigDAO;
import com.star.linkage.eml.EmailConfigBrowse;
import com.star.linkage.eml.EmailConfigDetails;
import com.star.linkage.eml.EmailConfigMan;
import com.star.linkage.payment.PaymentManOutput;
import com.star.modal.UsrEmlConfigDtls;

@Path("/config-eml")
public class ConfigEmailService {

	private Gmail service = null;
	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

	@POST
	@Secured
	@Path("/read")
	@Consumes({ "application/json" })
	public void getWorkflow(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data)
			throws Exception {

		BrowseResponse<EmailConfigBrowse> starBrowseResponse = new BrowseResponse<EmailConfigBrowse>();
		starBrowseResponse.setOutput(new EmailConfigBrowse());

		UsrEmlConfigDAO configDAO = new UsrEmlConfigDAO();

		ReadMailService mailService = new ReadMailService();

		configDAO.getEmlDetails(starBrowseResponse);

		for (int i = 0; i < starBrowseResponse.output.fldTblEmlConfig.size(); i++) {

			String eml = starBrowseResponse.output.fldTblEmlConfig.get(i).getUsrEml();
			String typ = starBrowseResponse.output.fldTblEmlConfig.get(i).getLgnTyp();

			String curDt = new SimpleDateFormat("yyyy/MM/dd").format(new Date());

			UsrEmlConfigDtls usrEmlConfig = configDAO.getRefreshToken(eml);

			// getEmailviaToken(usrEmlConfig, curDt, userId, cmpyId, eml);

			mailService.readEmail(starBrowseResponse.output.fldTblEmlConfig.get(i).getUsrEml(),
					starBrowseResponse.output.fldTblEmlConfig.get(i).getChkSum(), usrEmlConfig, userId, cmpyId, typ);

		}

	}

	private void getEmailviaToken(UsrEmlConfigDtls usrEmlConfig, String curDt, String userId, String cmpyId, String eml)
			throws IOException, GeneralSecurityException {
		UsrEmlConfigDAO configDAO = new UsrEmlConfigDAO();

		TokenResponse response = new GoogleRefreshTokenRequest(new NetHttpTransport(), new JacksonFactory(),
				usrEmlConfig.getRefreshToekn(), CommonConstants.CLIENT_ID, CommonConstants.CLIENT_SECRET).execute();

		System.out.println("Access token: " + response.getAccessToken());

		String accessToken = response.getAccessToken();

		Credential credential = new GoogleCredential().setAccessToken(accessToken);

		final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();

		service = new Gmail.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
				.setApplicationName(CommonConstants.APPLICATION_NAME).build();

		// "me" is special way of using currently logged in user
		String emailId = "me";

		// Follows rules from here: https://support.google.com/mail/answer/7190?hl=en
		String query = "has:attachment filename:pdf subject:Invoice posco";// label:BlogExample

		query = query + " after:" + curDt;

		// Retrieves all the messages using the query string
		ListMessagesResponse responseEml = service.users().messages().list(emailId).setQ(query).execute();

		List<Message> messages = new ArrayList<Message>();
		while (responseEml.getMessages() != null) {
			messages.addAll(responseEml.getMessages());
			if (responseEml.getNextPageToken() != null) {
				String pageToken = responseEml.getNextPageToken();
				responseEml = service.users().messages().list(emailId).setQ(query).setPageToken(pageToken).execute();
			} else {
				break;
			}
		}

		int numMessages = messages.size();

		int iCount = 0;

		for (Message message : messages) {

			message = service.users().messages().get(emailId, message.getId()).setFormat("FULL").execute();

			if (message.getInternalDate() <= usrEmlConfig.getLstRfshTm()) {
				continue;
			}

			MessagePart messagePart = message.getPayload();
			String messageContent = "";
			String subject = "";

			if (messagePart != null) {
				List<MessagePartHeader> headers = messagePart.getHeaders();
				for (MessagePartHeader header : headers) {
					// find the subject header.
					if (header.getName().equals("Subject")) {
						subject = header.getValue().trim();
						break;
					}
				}
			}

			// Create a subdirectory and file name from the subject
			// Parse the header to remove characters that can't be in a file path name
			String subdirName = subject.replaceAll("/", "-").replaceAll("<", "-").replaceAll(">", "-")
					.replaceAll(":", "-").replaceAll("\\\\", "-").replaceAll("\\|", "-").replaceAll("\\?", "")
					.replaceAll("\\*", "-").replaceAll("\"", "-").trim();
			String emailFileName = subdirName + ".txt";

			File subDir = new File("F:\\ocr-ap\\eml\\" + subdirName);
			if (!subDir.exists()) {
				subDir.mkdirs();
			}

			messageContent = getContent(message);

			/*
			 * try { // Create a text file for the raw data File emailTextFile = new
			 * File(subDir, emailFileName);
			 * 
			 * if (emailTextFile.exists() || emailTextFile.createNewFile()) { BufferedWriter
			 * bw = new BufferedWriter(new FileWriter(emailTextFile)); bw.write(subject);
			 * bw.newLine(); bw.newLine(); bw.write(messageContent); bw.flush(); bw.close();
			 * } } catch (IOException ioe) { ioe.printStackTrace(); }
			 */
			List<String> filenames = new ArrayList<String>();
			getAttachments(message.getPayload().getParts(), filenames, "F:\\ocr-ap\\eml\\" + subdirName, emailId,
					userId, cmpyId);

			if (iCount == 0) {
				configDAO.updLstRefreshTime(eml, message.getInternalDate());
			}

			iCount = iCount + 1;

		}
	}

	private String getContent(Message message) {
		StringBuilder stringBuilder = new StringBuilder();
		try {
			getPlainTextFromMessageParts(message.getPayload().getParts(), stringBuilder);
			if (stringBuilder.length() == 0) {
				stringBuilder.append(message.getPayload().getBody().getData());
			}
			byte[] bodyBytes = Base64.decodeBase64(stringBuilder.toString());
			String text = new String(bodyBytes, "UTF-8");
			return text;
		} catch (UnsupportedEncodingException e) {
			System.out.println("UnsupportedEncoding: " + e.toString());
			return message.getSnippet();
		}
	}

	/**
	 * Retrieves the attachments from the email message provided and writes them to
	 * the output dir
	 * 
	 * @param service
	 * @param message
	 * @param fileNames
	 * @param dir
	 */
	private void getAttachments(List<MessagePart> messageParts, List<String> fileNames, String dir, String userId,
			String prsUsrId, String cmpyId) {

		if (!dir.endsWith("/")) {
			dir += "/";
		}

		if (messageParts != null) {
			for (MessagePart part : messageParts) {
				// For each part, see if it has a file name, if it does it's an attachment
				if ((part.getFilename() != null && part.getFilename().length() > 0)) {
					String filename = part.getFilename();
					String attId = part.getBody().getAttachmentId();
					MessagePartBody attachPart;
					FileOutputStream fileOutFile = null;
					try {
						// Go get the attachment part and get the bytes
						attachPart = service.users().messages().attachments().get(userId, part.getPartId(), attId)
								.execute();
						byte[] fileByteArray = Base64.decodeBase64(attachPart.getData());

						UpldFileToApp fileToApp = new UpldFileToApp();

						fileToApp.processFile(cmpyId, prsUsrId, "M", part.getFilename(), fileByteArray);

						// Write the attachment to the output dir
						/*
						 * fileOutFile = new FileOutputStream(dir + filename);
						 * fileOutFile.write(fileByteArray); fileOutFile.close();
						 */
						fileNames.add(filename);
					} catch (IOException e) {
						System.out.println("IO Exception processing attachment: " + filename);
					} finally {
						if (fileOutFile != null) {
							try {
								fileOutFile.close();
							} catch (IOException e) {
								// probably doesn't matter
							}
						}
					}
				} else if (part.getMimeType().equals("multipart/related")) {
					if (part.getParts() != null) {
						getAttachments(part.getParts(), fileNames, dir, userId, prsUsrId, cmpyId);
					}
				}
			}
		}
	}

	/**
	 * Compiles all of the message parts into on message
	 * 
	 * @param messageParts
	 * @param stringBuilder
	 */
	private void getPlainTextFromMessageParts(List<MessagePart> messageParts, StringBuilder stringBuilder) {
		if (messageParts != null) {

			for (MessagePart messagePart : messageParts) {
				if (messagePart.getMimeType().equals("text/plain")) {
					stringBuilder.append(messagePart.getBody().getData());
				}

				if (messagePart.getParts() != null) {
					getPlainTextFromMessageParts(messagePart.getParts(), stringBuilder);
				}
			}
		}
	}

	public String get(String url) throws ClientProtocolException, IOException {
		return execute(new HttpGet(url));
	}

	private String execute(HttpRequestBase request) throws ClientProtocolException, IOException {
		HttpClient httpClient = new DefaultHttpClient();
		HttpResponse response = httpClient.execute(request);

		HttpEntity entity = response.getEntity();
		String body = EntityUtils.toString(entity);

		if (response.getStatusLine().getStatusCode() != 200) {
			throw new RuntimeException(
					"Expected 200 but got " + response.getStatusLine().getStatusCode() + ", with body " + body);
		}

		return body;
	}

	@POST
	@Secured
	@Path("/get-list")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getEmailList(@HeaderParam("user-id") String userId, @HeaderParam("user-grp") String userGrp,
			String data) throws Exception {

		BrowseResponse<EmailConfigBrowse> starBrowseResponse = new BrowseResponse<EmailConfigBrowse>();
		starBrowseResponse.setOutput(new EmailConfigBrowse());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		UsrEmlConfigDAO configDAO = new UsrEmlConfigDAO();

		configDAO.getEmlDetails(starBrowseResponse);

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblEmlConfig.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));
	}

	private GenericEntity<?> getGenericBrowseOutput(BrowseResponse<EmailConfigBrowse> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<EmailConfigBrowse>>(starBrowseResponse) {
		};
	}
	
	@POST
	@Secured
	@Path("/add")
	@Consumes({ "application/json" })
	public Response validateEmail(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data)
			throws Exception {
		
		MaintanenceResponse<EmailConfigMan> starManResponse = new MaintanenceResponse<EmailConfigMan>();
		starManResponse.setOutput(new EmailConfigMan());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		UsrEmlConfigDAO configDAO = new UsrEmlConfigDAO();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String eml = jsonObject.get("eml").getAsString();
		String chkSum = jsonObject.get("pass").getAsString();
		String type = jsonObject.get("typ").getAsString();
		
		ReadMailService  mailService = new ReadMailService();
		
		mailService.validateEmail(starManResponse, eml, chkSum, type);
		
		if(starManResponse.output.rtnSts == 0)
		{
			configDAO.addEmlInfo(eml, "", "", chkSum, type, userId);
		}
		
		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starManResponse));
		
	}
	
	private GenericEntity<?> getGenericMaintenanceOutput(
			MaintanenceResponse<EmailConfigMan> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<EmailConfigMan>>(starMaintenanceResponse) {
		};
	}

}
