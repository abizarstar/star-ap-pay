package com.star.service;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import com.star.common.SessionUtilGL;
import com.star.common.SessionUtilInformix;

@Path("/auth-page")
public class AuthPageRequest {

	@POST
	  @Path("/valid")
	  public Response valid() throws Exception
	  {
		
		SessionUtilInformix.resetSessionDetails();
		SessionUtilGL.resetSessionDetails();
		
		return Response.ok().build();
	  }
	
}
