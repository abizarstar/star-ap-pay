package com.star.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.MaintanenceResponse;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.dao.DbConfigDAO;
import com.star.linkage.dashboard.APOverviewBrowseOuput;
import com.star.linkage.dbconfig.DbConfigBrowseOutput;
import com.star.linkage.dbconfig.DbManOutput;
import com.star.linkage.wkf.WorkflowManOutput;

@Path("/db")
public class DbConfig {

	@POST
	@Secured
	@Path("/read-db")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getdbConfigs(@HeaderParam("cmpy-id") String cmpyId, String data) throws Exception {
		String conType = "";
		BrowseResponse<DbConfigBrowseOutput> starBrowseResponse = new BrowseResponse<DbConfigBrowseOutput>();
		starBrowseResponse.setOutput(new DbConfigBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		System.out.println("-------- Read DB service called -------");
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		if (jsonObject.get("conType").toString().length() > 0) {
			conType = jsonObject.get("conType").getAsString();
		}
		DbConfigDAO dao = new DbConfigDAO();

		dao.getconfigDb(starBrowseResponse, conType);
		return starResponseBuilder.getSuccessResponse(getDbConfig(starBrowseResponse));
	}

	@POST
	@Secured
	@Path("/config")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response dbConfigs(@HeaderParam("cmpy-id") String cmpyId, String data) throws Exception {

		MaintanenceResponse<DbManOutput> starManResponse = new MaintanenceResponse<DbManOutput>();
		starManResponse.setOutput(new DbManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		System.out.println("-------- Db Config service called -------");
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String driver = jsonObject.get("driver").getAsString();

		String url = jsonObject.get("url").getAsString();
		String host = jsonObject.get("host").getAsString();
		String port = jsonObject.get("port").getAsString();
	//	String db = jsonObject.get("db").getAsString();
		String user = jsonObject.get("user").getAsString();
		String password = jsonObject.get("password").getAsString();
		String conType = jsonObject.get("conType").getAsString();

		DbConfigDAO dao = new DbConfigDAO();

		dao.setconfigDb(starManResponse, driver, url, host, port, user, password, conType);
		return starResponseBuilder.getSuccessResponse(setDbConfig(starManResponse));
	}


	@POST
	@Secured
	@Path("/del-db")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response delDb(@HeaderParam("cmpy-id") String cmpyId, String data) throws Exception {

		MaintanenceResponse<DbManOutput> starManResponse = new MaintanenceResponse<DbManOutput>();
		starManResponse.setOutput(new DbManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		System.out.println("-------- Db Delete service called -------");
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String driver = jsonObject.get("driver").getAsString();

		DbConfigDAO dao = new DbConfigDAO();

		dao.delDb(starManResponse, driver);
		return starResponseBuilder.getSuccessResponse(setDbConfig(starManResponse));
	}

	private GenericEntity<?> getDbConfig(BrowseResponse<DbConfigBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<DbConfigBrowseOutput>>(starBrowseResponse) {
		};
	}

	private GenericEntity<?> setDbConfig(MaintanenceResponse<DbManOutput> starManResponse) {
		return new GenericEntity<MaintanenceResponse<DbManOutput>>(starManResponse) {
		};
	}

}
