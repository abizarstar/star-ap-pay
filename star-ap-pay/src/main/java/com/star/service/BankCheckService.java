package com.star.service;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.MaintanenceResponse;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.dao.CheckDAO;
import com.star.linkage.check.CheckBrowseOutput;
import com.star.linkage.check.CheckLotInfo;
import com.star.linkage.check.CheckManOutput;
import com.star.linkage.check.VoidCheckBrowse;
import com.star.linkage.check.VoidCheckInfo;
import com.star.linkage.cstmdocinfo.CstmDocInfoBrowseOutput;

@Path("/check")
public class BankCheckService {

	@POST
	@Secured
	@Path("/upd")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response addUpdateCheck(@HeaderParam("user-id") String userId, String data) throws Exception {

		MaintanenceResponse<CheckManOutput> starManResponse = new MaintanenceResponse<CheckManOutput>();
		starManResponse.setOutput(new CheckManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		List<CheckLotInfo> lotInfos = new ArrayList<CheckLotInfo>();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String bnkCode = jsonObject.get("bnkCode").getAsString();
		String acctId = jsonObject.get("acctId").getAsString();

		JsonArray lotArray = jsonObject.get("lotArr").getAsJsonArray();
		
		for(int i =0; i < lotArray.size(); i++)
		{
			CheckLotInfo checkLotInfo = new CheckLotInfo();
			
			JsonObject lotObject = lotArray.get(i).getAsJsonObject();
			
			checkLotInfo.setLotNo(lotObject.get("lotNo").getAsInt());
			checkLotInfo.setLotInfo(lotObject.get("shrtInfo").getAsString());
			checkLotInfo.setChkFrm(lotObject.get("chkFrm").getAsString());
			checkLotInfo.setChkTo(lotObject.get("chkTo").getAsString());
			checkLotInfo.setChkLotInfo(lotObject.get("sts").getAsString());
			
			lotInfos.add(checkLotInfo);
			
		}
		
		CheckDAO checkDAO = new CheckDAO();
		
		checkDAO.deleteOpenUnusedCheckLot(bnkCode, acctId);
		
		checkDAO.addCheckDetails(starManResponse, bnkCode, acctId, lotInfos);
			
		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starManResponse));
	}
	
	
	@POST
	@Secured
	@Path("/read")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response readCheckLot(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<CheckBrowseOutput> starBrowseResponse = new BrowseResponse<CheckBrowseOutput>();
		starBrowseResponse.setOutput(new CheckBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String bnkCode = jsonObject.get("bnkCode").getAsString();
		String acctId = jsonObject.get("acctId").getAsString();
		
		CheckDAO checkDAO = new CheckDAO();
		
		checkDAO.getCheckLotDetails(starBrowseResponse, bnkCode, acctId);
			
		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));
	}
	
	@POST
	@Secured
	@Path("/read-chk")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response readUsedCheck(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data) throws Exception {

		BrowseResponse<CstmDocInfoBrowseOutput> starBrowseResponse = new BrowseResponse<CstmDocInfoBrowseOutput>();
		starBrowseResponse.setOutput(new CstmDocInfoBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String bnkCode = jsonObject.get("bnkCode").getAsString();
		String acctId = jsonObject.get("acctId").getAsString();
		
		CheckDAO checkDAO = new CheckDAO();
		
		checkDAO.getCheckPaymentDetails(starBrowseResponse, bnkCode, acctId, cmpyId);
			
		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputChk(starBrowseResponse));
	}
	
	@POST
	@Secured
	@Path("/void-chk")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response voidCheck(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<CstmDocInfoBrowseOutput> starBrowseResponse = new BrowseResponse<CstmDocInfoBrowseOutput>();
		starBrowseResponse.setOutput(new CstmDocInfoBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		
		JsonArray checkList = jsonObject.get("voidChk").getAsJsonArray();
		String remark = jsonObject.get("rmk").getAsString();
		
		List<VoidCheckInfo> checkInfos = new ArrayList<VoidCheckInfo>(); 
		
		for(int i = 0; i < checkList.size(); i++)
		{
			VoidCheckInfo checkInfo = new VoidCheckInfo();
			
			JsonObject object = checkList.get(i).getAsJsonObject();
			
			checkInfo.setChkNo(object.get("chkNo").getAsString());
			checkInfo.setReqId(object.get("reqId").getAsString());
			checkInfo.setVchrNo(object.get("vchrNo").getAsString());
			
			checkInfos.add(checkInfo);
			
		}
		
		CheckDAO checkDAO = new CheckDAO();
		
		checkDAO.updVoidCheck(checkInfos, remark, userId);
			
		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputChk(starBrowseResponse));
	}
	
	
	@POST
	@Secured
	@Path("/void-chk-ind")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response voidCheckUnused(@HeaderParam("user-id") String userId, String data) throws Exception {

		MaintanenceResponse<CheckManOutput> starManResponse = new MaintanenceResponse<CheckManOutput>();
		starManResponse.setOutput(new CheckManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		
		String remark = jsonObject.get("rmk").getAsString();
		String bnkCode = jsonObject.get("bnkCode").getAsString();
		String acctNo = jsonObject.get("acctNo").getAsString();
		String chkNo = jsonObject.get("chkNo").getAsString();
		
		CheckDAO checkDAO = new CheckDAO();
		
		int iRcrdCount = checkDAO.validateVoidCheck(bnkCode, acctNo, chkNo);
		
		if(iRcrdCount > 0)
		{
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add("Check is already voided");
		}
		
		iRcrdCount = 0;
		iRcrdCount = checkDAO.validateVoidCheckNo(bnkCode, acctNo, chkNo);
		
		if(iRcrdCount == 0)
		{
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add("Invalid check number.");
		}
			
		if(starManResponse.output.rtnSts == 0)
		{
			checkDAO.addVoidCheck(starManResponse, bnkCode, acctNo, chkNo, remark, userId);
		}
		
		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starManResponse));
	}
	
	@POST
	@Secured
	@Path("/void-chk-lst")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response voidCheckLst(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<VoidCheckBrowse> starBrowseResponse = new BrowseResponse<VoidCheckBrowse>();
		starBrowseResponse.setOutput(new VoidCheckBrowse());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		
		CheckDAO checkDAO = new CheckDAO();
		
		checkDAO.getVoidCheckDetails(starBrowseResponse);
			
		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputVoidCheck(starBrowseResponse));
	}

	private GenericEntity<?> getGenericBrowseOutputVoidCheck(BrowseResponse<VoidCheckBrowse> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<VoidCheckBrowse>>(starBrowseResponse) {
		};
	}
	
	private GenericEntity<?> getGenericBrowseOutput(BrowseResponse<CheckBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<CheckBrowseOutput>>(starBrowseResponse) {
		};
	}
	
	private GenericEntity<?> getGenericBrowseOutputChk(BrowseResponse<CstmDocInfoBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<CstmDocInfoBrowseOutput>>(starBrowseResponse) {
		};
	}
	
	private GenericEntity<?> getGenericMaintenanceOutput(
			MaintanenceResponse<CheckManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<CheckManOutput>>(starMaintenanceResponse) {
		};
	}
	
}
