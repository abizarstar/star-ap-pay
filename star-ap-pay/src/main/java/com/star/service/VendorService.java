package com.star.service;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.MaintanenceResponse;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.dao.VendorInfoDAO;
import com.star.linkage.common.PaymentMethod;
import com.star.linkage.vendor.BankAccount;
import com.star.linkage.vendor.CompanyBrowseOutput;
import com.star.linkage.vendor.VendorBankBrowseOutput;
import com.star.linkage.vendor.VendorBrowseOutput;
import com.star.linkage.vendor.VendorDefaultGlSettingInput;
import com.star.linkage.vendor.VendorDefaultSettingBrowseOutput;
import com.star.linkage.vendor.VendorDefaultSettingInput;
import com.star.linkage.vendor.VendorEmailSetupBrowseOutput;
import com.star.linkage.vendor.VendorEmailSetupInput;
import com.star.linkage.vendor.VendorInput;
import com.star.linkage.vendor.VendorManOutput;

@Path("/vendor")
public class VendorService {

	@POST
	@Secured
	@Path("/read")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getVendor(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<VendorBrowseOutput> starBrowseResponse = new BrowseResponse<VendorBrowseOutput>();
		starBrowseResponse.setOutput(new VendorBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String cmpyId = jsonObject.get("cmpyId").getAsString();
		
		VendorInfoDAO vendorInfoDAO= new VendorInfoDAO();
		
		vendorInfoDAO.readVendorList(starBrowseResponse, cmpyId);
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblVendor.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));
	}
	
	@POST
	@Secured
	@Path("/readESP")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getVendorESP(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<VendorBrowseOutput> starBrowseResponse = new BrowseResponse<VendorBrowseOutput>();
		starBrowseResponse.setOutput(new VendorBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String cmpyId = jsonObject.get("cmpyId").getAsString();
		
		VendorInfoDAO vendorInfoDAO= new VendorInfoDAO();
		
		vendorInfoDAO.readVendorListESP(starBrowseResponse, cmpyId);
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblVendor.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));
	}
	
	@POST
	@Secured
	@Path("/read-cmpy")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getCompany(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<CompanyBrowseOutput> starBrowseResponse = new BrowseResponse<CompanyBrowseOutput>();
		starBrowseResponse.setOutput(new CompanyBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		VendorInfoDAO vendorInfoDAO= new VendorInfoDAO();
		
		vendorInfoDAO.readCompanyList(starBrowseResponse);
		
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblCompany.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputCompany(starBrowseResponse));
	}
	
	
	@POST
	@Secured
	@Path("/add")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response addVendorInfo(@HeaderParam("user-id") String userId, String data) throws Exception {

		MaintanenceResponse<VendorManOutput> starManResponse = new MaintanenceResponse<VendorManOutput>();
		starManResponse.setOutput(new VendorManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		
		VendorInput vendorInput = new VendorInput();
		
		vendorInput.setCmpyId(jsonObject.get("cmpyId").getAsString());
		vendorInput.setVenId(jsonObject.get("venId").getAsString());
		vendorInput.setCty(jsonObject.get("cty").getAsString());
		vendorInput.setPymntMtd(jsonObject.get("pymntMtd").getAsString());
		vendorInput.setCry(jsonObject.get("cry").getAsString());
		vendorInput.setVenNm(jsonObject.get("venNm").getAsString());
		vendorInput.setVenLongNm(jsonObject.get("venLongNm").getAsString());
		vendorInput.setCrtdBy(userId);
		
		JsonArray array = jsonObject.get("bankList").getAsJsonArray();
		
		List<BankAccount> listBank = new ArrayList<BankAccount>();
		
		for(int i = 0; i < array.size(); i++)
		{
			BankAccount account = new BankAccount();
			
			JsonObject jsonObj= array.get(i).getAsJsonObject();
			account.setAcctNm(jsonObj.get("bnkNm").getAsString());
			account.setAcctNo(jsonObj.get("acctNo").getAsString());
			account.setBnkKey(jsonObj.get("bnkKey").getAsString());
			account.setExtRef(jsonObj.get("extRef").toString());
			
			listBank.add(account);
		}
		
		vendorInput.setBankList(listBank);
		
		
		JsonArray pymntMthdArr = jsonObject.get("pymntMtdList").getAsJsonArray();
		
		List<PaymentMethod> listPaymentMethod = new ArrayList<PaymentMethod>();
		
		for(int i = 0; i < pymntMthdArr.size(); i++)
		{
			PaymentMethod paymentMethod = new PaymentMethod();
			
			JsonObject jsonObj= pymntMthdArr.get(i).getAsJsonObject();
			paymentMethod.setMthdId(jsonObj.get("mthdId").getAsString());
			paymentMethod.setMthdCd(jsonObj.get("mthdCd").getAsString());
			paymentMethod.setMthdDflt(jsonObj.get("mthdDflt").getAsBoolean());
			
			listPaymentMethod.add(paymentMethod);
		}
		
		vendorInput.setPymntMtdList(listPaymentMethod);
		
		VendorInfoDAO vendorInfoDAO= new VendorInfoDAO();
		
		int iRcrdCount = vendorInfoDAO.validateVendor(vendorInput);
		
		if(iRcrdCount == 0)
		{
			vendorInfoDAO.addVendorInfo(starManResponse, vendorInput);
		}
		else
		{
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(CommonConstants.ACCT_ERR);
		}

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starManResponse));
	}
	
	@POST
	@Secured
	@Path("/add-email-setup")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response addVendorEmailSetup(@HeaderParam("user-id") String userId, String data) throws Exception {

		MaintanenceResponse<VendorManOutput> starManResponse = new MaintanenceResponse<VendorManOutput>();
		starManResponse.setOutput(new VendorManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		
		VendorEmailSetupInput vendorEmailSetupInput = new VendorEmailSetupInput();
		
		//vendorEmailSetupInput.setCmpyId(jsonObject.get("cmpyId").getAsString());
		vendorEmailSetupInput.setVenId(jsonObject.get("venId").getAsString());
		vendorEmailSetupInput.setSubject(jsonObject.get("subj").getAsString());
		vendorEmailSetupInput.setBody(jsonObject.get("body").getAsString());
		
		
		VendorInfoDAO vendorInfoDAO= new VendorInfoDAO();
		
		int iRcrdCount = vendorInfoDAO.validateVendor(vendorEmailSetupInput);
		
		if(iRcrdCount == 0)
		{
			vendorInfoDAO.addVendorInfo(starManResponse, vendorEmailSetupInput);
		}
		else
		{
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(CommonConstants.ACCT_ERR);
		}

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starManResponse));
	}

	
	@POST
	@Secured
	@Path("/delete")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response updVendorInfo(@HeaderParam("user-id") String userId, String data) throws Exception {

		MaintanenceResponse<VendorManOutput> starManResponse = new MaintanenceResponse<VendorManOutput>();
		starManResponse.setOutput(new VendorManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		
		VendorInput vendorInput = new VendorInput();
		
		vendorInput.setCmpyId(jsonObject.get("cmpyId").getAsString());
		vendorInput.setVenId(jsonObject.get("venId").getAsString());
		
		VendorInfoDAO vendorInfoDAO= new VendorInfoDAO();
		
		vendorInfoDAO.deleteVendor(starManResponse, vendorInput);
		
		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starManResponse));
	}
	
	
	@POST
	@Secured
	@Path("/update")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response updateVendorInfo(@HeaderParam("user-id") String userId, String data) throws Exception {

		MaintanenceResponse<VendorManOutput> starManResponse = new MaintanenceResponse<VendorManOutput>();
		starManResponse.setOutput(new VendorManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		
		VendorInput vendorInput = new VendorInput();
		
		vendorInput.setCmpyId(jsonObject.get("cmpyId").getAsString());
		vendorInput.setVenId(jsonObject.get("venId").getAsString());
		vendorInput.setCty(jsonObject.get("cty").getAsString());
		vendorInput.setPymntMtd(jsonObject.get("pymntMtd").getAsString());
		vendorInput.setCry(jsonObject.get("cry").getAsString());
		vendorInput.setVenNm(jsonObject.get("venNm").getAsString());
		vendorInput.setVenLongNm(jsonObject.get("venLongNm").getAsString());
		vendorInput.setCrtdBy(userId);
		
		JsonArray array = jsonObject.get("bankList").getAsJsonArray();
		
		List<BankAccount> listBank = new ArrayList<BankAccount>();
		
		for(int i = 0; i < array.size(); i++)
		{
			BankAccount account = new BankAccount();
			
			JsonObject jsonObj= array.get(i).getAsJsonObject();
			account.setAcctNm(jsonObj.get("bnkNm").getAsString());
			account.setAcctNo(jsonObj.get("acctNo").getAsString());
			account.setBnkKey(jsonObj.get("bnkKey").getAsString());
			account.setExtRef(jsonObj.get("extRef").getAsString());
			
			listBank.add(account);
		}
		
		vendorInput.setBankList(listBank);
		
		JsonArray pymntMthdArr = jsonObject.get("pymntMtdList").getAsJsonArray();
		
		List<PaymentMethod> listPaymentMethod = new ArrayList<PaymentMethod>();
		
		for(int i = 0; i < pymntMthdArr.size(); i++)
		{
			PaymentMethod paymentMethod = new PaymentMethod();
			
			JsonObject jsonObj= pymntMthdArr.get(i).getAsJsonObject();
			paymentMethod.setMthdId(jsonObj.get("mthdId").getAsString());
			paymentMethod.setMthdCd(jsonObj.get("mthdCd").getAsString());
			paymentMethod.setMthdDflt(jsonObj.get("mthdDflt").getAsBoolean());
			
			listPaymentMethod.add(paymentMethod);
		}
		
		vendorInput.setPymntMtdList(listPaymentMethod);
		
		VendorInfoDAO vendorInfoDAO= new VendorInfoDAO();
		
		vendorInfoDAO.updateVendorInfo(starManResponse, vendorInput);
		
		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starManResponse));
	}
	
	@POST
	@Secured
	@Path("/update-email-setup")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response updateVendorEmailSetup(@HeaderParam("user-id") String userId, String data) throws Exception {

		MaintanenceResponse<VendorManOutput> starManResponse = new MaintanenceResponse<VendorManOutput>();
		starManResponse.setOutput(new VendorManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		
		VendorEmailSetupInput vendorEmailSetupInput = new VendorEmailSetupInput();
		
		//vendorEmailSetupInput.setCmpyId(jsonObject.get("cmpyId").getAsString());
		vendorEmailSetupInput.setVenId(jsonObject.get("venId").getAsString());
		vendorEmailSetupInput.setSubject(jsonObject.get("subj").getAsString());
		vendorEmailSetupInput.setBody(jsonObject.get("body").getAsString());
		
		
		VendorInfoDAO vendorInfoDAO= new VendorInfoDAO();
		
		vendorInfoDAO.updateVendorInfo(starManResponse, vendorEmailSetupInput);
		
		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starManResponse));
	}
	
	@POST
	@Secured
	@Path("/read-ven")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getVendorSpecific(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data) throws Exception {

		BrowseResponse<VendorBankBrowseOutput> starBrowseResponse = new BrowseResponse<VendorBankBrowseOutput>();
		starBrowseResponse.setOutput(new VendorBankBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String venId = jsonObject.get("venId").getAsString();
		
		VendorInfoDAO vendorInfoDAO= new VendorInfoDAO();
		
		vendorInfoDAO.readVendorInfo(starBrowseResponse, cmpyId, venId);
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblVendor.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputBank(starBrowseResponse));
	}
	
	@POST
	@Secured
	@Path("/read-email-setup")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getVendorEmailSetup(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data) throws Exception {

		BrowseResponse<VendorEmailSetupBrowseOutput> starBrowseResponse = new BrowseResponse<VendorEmailSetupBrowseOutput>();
		starBrowseResponse.setOutput(new VendorEmailSetupBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String venId = jsonObject.get("venId").getAsString();
		
		VendorInfoDAO vendorInfoDAO= new VendorInfoDAO();
		
		vendorInfoDAO.readVendorEmailSetup(starBrowseResponse, cmpyId, venId);
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblVendorES.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputEmailSetup(starBrowseResponse));
	}
	
	@POST
	@Secured
	@Path("/add-dflt-setting")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response addVendorDefaultSetting(@HeaderParam("user-id") String userId,  @HeaderParam("cmpy-id") String cmpyId, String data) throws Exception {

		MaintanenceResponse<VendorManOutput> starManResponse = new MaintanenceResponse<VendorManOutput>();
		starManResponse.setOutput(new VendorManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		
		VendorDefaultSettingInput vndrDfltInput = new VendorDefaultSettingInput();
				
		vndrDfltInput.setCmpyId(cmpyId);
		vndrDfltInput.setVenId(jsonObject.get("venId").getAsString());
		vndrDfltInput.setVchrBrh(jsonObject.get("vchrBrh").getAsString());
		vndrDfltInput.setVchrCat(jsonObject.get("vchrCat").getAsString());
		vndrDfltInput.setTrsStsActn(jsonObject.get("trsStsActn").getAsString());
		vndrDfltInput.setTrsSts(jsonObject.get("trsSts").getAsString());
		vndrDfltInput.setTrsRsn(jsonObject.get("trsRsn").getAsString());
		vndrDfltInput.setNtfUsr(jsonObject.get("ntfUsr").getAsString());
		vndrDfltInput.setIsActive(jsonObject.get("isActive").getAsString());
		vndrDfltInput.setCrtdBy(userId);
		
		JsonArray array = jsonObject.get("glAcctList").getAsJsonArray();
		
		List<VendorDefaultGlSettingInput> listGlAcct = new ArrayList<VendorDefaultGlSettingInput>();
		
		for(int i = 0; i < array.size(); i++)
		{
			VendorDefaultGlSettingInput glAcct = new VendorDefaultGlSettingInput();
			
			JsonObject jsonObj= array.get(i).getAsJsonObject();
			glAcct.setCmpyId(jsonObj.get("cmpyId").getAsString());
			glAcct.setVenId(jsonObj.get("venId").getAsString());
			glAcct.setBscGlAcct(jsonObj.get("bscGlAcct").getAsString());
			glAcct.setSacct(jsonObj.get("sacct").getAsString());
			glAcct.setSacctTxt(jsonObj.get("sacctTxt").getAsString());
			glAcct.setDistRmk(jsonObj.get("distRmk").getAsString());
			
			listGlAcct.add(glAcct);
		}
		
		vndrDfltInput.setGlAcctList(listGlAcct);
		
		VendorInfoDAO vendorInfoDAO= new VendorInfoDAO();
		
		vendorInfoDAO.saveVendorDefaultSetting(starManResponse, vndrDfltInput);

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starManResponse));
	}
	
	
	@POST
	@Secured
	@Path("/read-dflt-setting")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response readVendorDefaultSetting(@HeaderParam("user-id") String userId,  @HeaderParam("cmpy-id") String cmpyId, String data) throws Exception {

		BrowseResponse<VendorDefaultSettingBrowseOutput> starBrowseResponse = new BrowseResponse<VendorDefaultSettingBrowseOutput>();
		starBrowseResponse.setOutput(new VendorDefaultSettingBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		
		String venId = jsonObject.get("venId").getAsString();
		
		
				
		VendorInfoDAO vendorInfoDAO= new VendorInfoDAO();
		
		vendorInfoDAO.readVendorDefaultSetting(starBrowseResponse, cmpyId, venId, userId);
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblVndrDfltStng.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputVendorDefaultSetting(starBrowseResponse));
	}
	
	
	private GenericEntity<?> getGenericBrowseOutputBank(BrowseResponse<VendorBankBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<VendorBankBrowseOutput>>(starBrowseResponse) {
		};
	}
	
	private GenericEntity<?> getGenericBrowseOutputEmailSetup(BrowseResponse<VendorEmailSetupBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<VendorEmailSetupBrowseOutput>>(starBrowseResponse) {
		};
	}
	
	private GenericEntity<?> getGenericBrowseOutput(BrowseResponse<VendorBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<VendorBrowseOutput>>(starBrowseResponse) {
		};
	}
	
	private GenericEntity<?> getGenericBrowseOutputCompany(BrowseResponse<CompanyBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<CompanyBrowseOutput>>(starBrowseResponse) {
		};
	}
	
	private GenericEntity<?> getGenericMaintenanceOutput(
			MaintanenceResponse<VendorManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<VendorManOutput>>(starMaintenanceResponse) {
		};
	}
	
	private GenericEntity<?> getGenericBrowseOutputVendorDefaultSetting(BrowseResponse<VendorDefaultSettingBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<VendorDefaultSettingBrowseOutput>>(starBrowseResponse) {
		};
	}
	
}
