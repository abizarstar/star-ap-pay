package com.star.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.FTPDownloadUpload;
import com.star.common.MaintanenceResponse;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.dao.BankDAO;
import com.star.linkage.bank.BankBrowseOutput;
import com.star.linkage.bank.BankInfo;
import com.star.linkage.bank.BankManOutput;

@Path("/bank")
public class BankService {

		@POST
		@Secured
		@Path("/upd")
		@Consumes({ "application/json" })
		@Produces({ "application/json" })
		public Response addUpdateBank(@HeaderParam("user-id") String userId, String data) throws Exception {

			MaintanenceResponse<BankManOutput> starManResponse = new MaintanenceResponse<BankManOutput>();
			starManResponse.setOutput(new BankManOutput());
			StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
			
			BankInfo bankInput= new Gson().fromJson(data, BankInfo.class);
			
			BankDAO bankDAO = new BankDAO();
			
			int iRcrdCount = bankDAO.validateBnkCode(bankInput.getBnkCode());
			
			if(iRcrdCount > 0)
			{
				bankDAO.updBankDetails(starManResponse, bankInput);
			}
			else
			{
				bankDAO.addBankDetails(starManResponse, bankInput);
			}
				
			return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starManResponse));
		}
		
		private GenericEntity<?> getGenericMaintenanceOutput(
				MaintanenceResponse<BankManOutput> starMaintenanceResponse) {
			return new GenericEntity<MaintanenceResponse<BankManOutput>>(starMaintenanceResponse) {
			};
		}
		
		@POST
		@Secured
		@Path("/read")
		@Consumes({ "application/json" })
		@Produces({ "application/json" })
		public Response getBank(@HeaderParam("user-id") String userId, String data) throws Exception {

			BrowseResponse<BankBrowseOutput> starBrowseResponse = new BrowseResponse<BankBrowseOutput>();
			starBrowseResponse.setOutput(new BankBrowseOutput());
			StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

			JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
			String bnkCode = jsonObject.get("bnkCode").getAsString();
			
			BankDAO bankDAO = new BankDAO();
			bankDAO.getBankDetails(starBrowseResponse, bnkCode);
			
			starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblBank.size();
			
			return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));
		}
		
		
		@POST
		@Secured
		@Path("/list")
		@Consumes({ "application/json" })
		@Produces({ "application/json" })
		public Response getBankList(@HeaderParam("user-id") String userId, String data) throws Exception {

			BrowseResponse<BankBrowseOutput> starBrowseResponse = new BrowseResponse<BankBrowseOutput>();
			starBrowseResponse.setOutput(new BankBrowseOutput());
			StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
			
			BankDAO bankDAO = new BankDAO();
			bankDAO.getBankList(starBrowseResponse);
			
			starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblBank.size();
			
			return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));
		}
		
		private GenericEntity<?> getGenericBrowseOutput(BrowseResponse<BankBrowseOutput> starBrowseResponse) {
			return new GenericEntity<BrowseResponse<BankBrowseOutput>>(starBrowseResponse) {
			};
		}
		
		@POST
		@Secured
		@Path("/vldt-ftp")
		@Consumes({ "application/json" })
		@Produces({ "application/json" })
		public Response validateFtpDetails(@HeaderParam("user-id") String userId, String data) throws Exception {

			MaintanenceResponse<BankManOutput> starManResponse = new MaintanenceResponse<BankManOutput>();
			
			starManResponse.setOutput(new BankManOutput());
			
			StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
			
			BankInfo bankInput = new Gson().fromJson(data, BankInfo.class);
			
			String host = bankInput.getBnkHost();
			String port = bankInput.getBnkPort();
			String usrId = bankInput.getBnkFtpUser();
			String pass = bankInput.getBnkFtpPass();
						
			FTPDownloadUpload FTPDownloadUpload = new FTPDownloadUpload();	
			
			boolean ftpSts = FTPDownloadUpload.validateFTP(starManResponse, host, port, usrId, pass );
			
			if(ftpSts == false)
			{
				starManResponse.output.messages.add(CommonConstants.SERVER_ERR);
				starManResponse.output.rtnSts = 1;
			}
				
			return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starManResponse));
		}
}

