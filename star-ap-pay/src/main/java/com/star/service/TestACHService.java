package com.star.service;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.hibernate.Session;

import com.star.common.MaintanenceResponse;
import com.star.common.Secured;
import com.star.common.ach.GeneratePain001;
import com.star.dao.GLInfoDAO;
import com.star.dao.GetACHDetailsDAO;
import com.star.linkage.ach.InvoiceInfo;
import com.star.linkage.payment.PaymentManOutput;

@Path("/ach")
public class TestACHService {

	@POST
	@Secured
	@Path("/read")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getDocument(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data) throws Exception {

		
		GeneratePain001 pain001 = new GeneratePain001();
		
		
		/*CheckDAO checkDAO = new CheckDAO();
		
		ChkLotUsage chkLotUsage = checkDAO.getCurrentCheckNo("ROY", "0810008637");
		
		int iTotalChks = 0;
		
		if(chkLotUsage.getBnkCode() != null)
		{
			String endCheckNo = checkDAO.getEndLotCheck(chkLotUsage);
		
			List<CheckLotInfo> checkLotList = checkDAO.getChkLotList(chkLotUsage);
			
			for(int i = 0; i < checkLotList.size(); i++)
			{
				iTotalChks = iTotalChks + (Integer.parseInt(checkLotList.get(i).getChkTo()) - Integer.parseInt(checkLotList.get(i).getChkFrm())); 
			}
			
			iTotalChks = iTotalChks + (Integer.parseInt(endCheckNo) - Integer.parseInt(chkLotUsage.getCurChk()));
		}
		
		MaintanenceResponse<PaymentManOutput> starManResponse = new MaintanenceResponse<PaymentManOutput>();
		starManResponse.setOutput(new PaymentManOutput());
		
		checkDAO.getVenListForChecks(starManResponse, "TST123", iTotalChks);*/
		
		Session session = null;
		
		String achOutput = pain001.getInvoiceDetails(session, "TST1", "CTB", "638951");
		
		
		/* PERFORM GL TRANSACTION */
		List<InvoiceInfo> invVchrList = new ArrayList<InvoiceInfo>();
		
		GetACHDetailsDAO detailsDAO = new GetACHDetailsDAO();
		invVchrList = detailsDAO.getPropInvoices("TST1");
		
		GLInfoDAO glInfoDAO = new GLInfoDAO();
		
		MaintanenceResponse<PaymentManOutput> starManResponse = new MaintanenceResponse<PaymentManOutput>();
		starManResponse.setOutput(new PaymentManOutput());
		
		for(int i=0; i < invVchrList.size(); i++)
		{
			glInfoDAO.addLedgerEntry(starManResponse, cmpyId, userId, invVchrList.get(i));
		}
		
		/*GenerateACHFile achFile = new GenerateACHFile();
		
		
		String achOutput = achFile.getACHData(invoiceList);*/
		
		return Response.status(200).entity(achOutput).build();
	}
	
}
