package com.star.service;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.MaintanenceResponse;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.dao.EBSTypeCodeDAO;
import com.star.linkage.ebstypecode.EBSBrowseOutput;
import com.star.linkage.ebstypecode.GLEBSInfoBrowseOutput;
import com.star.linkage.ebstypecode.GLEBSMapBrowseOutput;
import com.star.linkage.ebstypecode.GLEBSMapInput;
import com.star.linkage.ebstypecode.GLEBSMapMntOutput;
import com.star.linkage.ebstypecode.TypeCodeBrowseOutput;

@Path("/ebsTypeCode")
public class EBSTypeCodeService {

	@POST
	@Secured
	@Path("/readEbs")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getEbs(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<EBSBrowseOutput> starBrowseResponse = new BrowseResponse<EBSBrowseOutput>();
		starBrowseResponse.setOutput(new EBSBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		//JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		//String cmpyId = jsonObject.get("cmpyId").getAsString();
		
		EBSTypeCodeDAO ebsTypeCodeDAO= new EBSTypeCodeDAO();
		
		ebsTypeCodeDAO.readEBSList(starBrowseResponse);
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblEbs.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));
	}
	
	private GenericEntity<?> getGenericBrowseOutput(BrowseResponse<EBSBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<EBSBrowseOutput>>(starBrowseResponse) {
		};
	}
	
	
	@POST
	@Secured
	@Path("/readTypeCode")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getTypeCode(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<TypeCodeBrowseOutput> starBrowseResponse = new BrowseResponse<TypeCodeBrowseOutput>();
		starBrowseResponse.setOutput(new TypeCodeBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String ebsId = jsonObject.get("ebsId").getAsString();
		
		EBSTypeCodeDAO ebsTypeCodeDAO= new EBSTypeCodeDAO();
		
		ebsTypeCodeDAO.readTypeCodeList(starBrowseResponse, ebsId);
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblTypeCode.size();

		return starResponseBuilder.getSuccessResponse(getGenericTypeCodeBrowseOutput(starBrowseResponse));
	}
	
	private GenericEntity<?> getGenericTypeCodeBrowseOutput(BrowseResponse<TypeCodeBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<TypeCodeBrowseOutput>>(starBrowseResponse) {
		};
	}
	
	
	@POST
	@Secured
	@Path("/addGlEbsMap")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response addGlEbsMapping(@HeaderParam("user-id") String userId, String data) throws Exception {
		
		MaintanenceResponse<GLEBSMapMntOutput> starManResponse = new MaintanenceResponse<GLEBSMapMntOutput>();
		starManResponse.setOutput(new GLEBSMapMntOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		
		JsonArray mapArray = jsonObject.get("mapArr").getAsJsonArray();
		
		List<GLEBSMapInput> glEbsList = new ArrayList<GLEBSMapInput>();
		
		for(int i =0; i < mapArray.size(); i++)
		{
			GLEBSMapInput glEbsMapInput = new GLEBSMapInput();
			
			JsonObject mapObject = mapArray.get(i).getAsJsonObject();
			
			glEbsMapInput.setEbsId( mapObject.get("ebsId").getAsString());
			glEbsMapInput.setTypCd( mapObject.get("typCd").getAsString());
			glEbsMapInput.setPost1( mapObject.get("pstng1").getAsString());
			glEbsMapInput.setPost2( mapObject.get("pstng2").getAsString());
			
			glEbsList.add(glEbsMapInput);
		}
		
		EBSTypeCodeDAO ebsTypeCodeDAO = new EBSTypeCodeDAO();
		
		
		ebsTypeCodeDAO.addGlEbsMapping(starManResponse, glEbsList);
		
		/*int iCount = ebsTypeCodeDAO.validateGlEBSMappping(glEbsMapInput.getEbsId(), glEbsMapInput.getTypCd());
		
		int iCountComb = ebsTypeCodeDAO.validateAccountComb(glEbsMapInput.getEbsId(), glEbsMapInput.getTypCd(), glEbsMapInput.getGlAcct(), glEbsMapInput.getSubAcct(), glEbsMapInput.getTrnType());*/
		
		/*if(iCount == 2)
		{
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(CommonConstants.TYP_CODE_ERR);
		}
		else if (iCountComb > 0)
		{
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(CommonConstants.GL_COMB_ERR);
		}
		else
		{*/
			
		/*}*/

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starManResponse));
	}
	
	private GenericEntity<?> getGenericMaintenanceOutput(
			MaintanenceResponse<GLEBSMapMntOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<GLEBSMapMntOutput>>(starMaintenanceResponse) {
		};
	}
	
	
	@POST
	@Secured
	@Path("/chkGLEbsMap")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response checkGlEBSMappping(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<GLEBSMapBrowseOutput> starBrowseResponse = new BrowseResponse<GLEBSMapBrowseOutput>();
		starBrowseResponse.setOutput(new GLEBSMapBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String ebsId = jsonObject.get("ebsId").getAsString();
		String typCd = jsonObject.get("typCd").getAsString();
		
		EBSTypeCodeDAO ebsTypeCodeDAO= new EBSTypeCodeDAO();
		
		ebsTypeCodeDAO.checkGlEBSMappping(starBrowseResponse, ebsId, typCd);
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblGlEbsMap.size();

		return starResponseBuilder.getSuccessResponse(getGenericChkMapBrowseOutput(starBrowseResponse));
	}
	
	private GenericEntity<?> getGenericChkMapBrowseOutput(BrowseResponse<GLEBSMapBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<GLEBSMapBrowseOutput>>(starBrowseResponse) {
		};
	}
	
	
	@POST
	@Secured
	@Path("/readMap")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response readGLEBSMappping(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<GLEBSMapBrowseOutput> starBrowseResponse = new BrowseResponse<GLEBSMapBrowseOutput>();
		starBrowseResponse.setOutput(new GLEBSMapBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String ebsId = jsonObject.get("ebsId").getAsString();
		
		EBSTypeCodeDAO ebsTypeCodeDAO= new EBSTypeCodeDAO();
		
		ebsTypeCodeDAO.readGlEBSMapping(starBrowseResponse, ebsId);
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblGlEbsMap.size();

		return starResponseBuilder.getSuccessResponse(getGenericReadMapBrowseOutput(starBrowseResponse));
	}
	
	
	@POST
	@Secured
	@Path("/read-typ-cd-post")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response readTypeCodePosting(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<GLEBSInfoBrowseOutput> starBrowseResponse = new BrowseResponse<GLEBSInfoBrowseOutput>();
		starBrowseResponse.setOutput(new GLEBSInfoBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String ebsId = jsonObject.get("ebsId").getAsString();
		String typCd = jsonObject.get("typCd").getAsString();
		
		EBSTypeCodeDAO ebsTypeCodeDAO= new EBSTypeCodeDAO();
		
		ebsTypeCodeDAO.getGLAccount(starBrowseResponse, ebsId, typCd);
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblGlEbsMap.size();

		return starResponseBuilder.getSuccessResponse(getGenericReadMapBrowseOutputPosting(starBrowseResponse));
	}
	
	private GenericEntity<?> getGenericReadMapBrowseOutputPosting(BrowseResponse<GLEBSInfoBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<GLEBSInfoBrowseOutput>>(starBrowseResponse) {
		};
	}
	
	private GenericEntity<?> getGenericReadMapBrowseOutput(BrowseResponse<GLEBSMapBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<GLEBSMapBrowseOutput>>(starBrowseResponse) {
		};
	}
	
	
	@POST
	@Secured
	@Path("/deleteGlEbsMap")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response deleteGlEbsMapping(@HeaderParam("user-id") String userId, String data) throws Exception {
		
		MaintanenceResponse<GLEBSMapMntOutput> starManResponse = new MaintanenceResponse<GLEBSMapMntOutput>();
		starManResponse.setOutput(new GLEBSMapMntOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		
		GLEBSMapInput glEbsMapInput = new GLEBSMapInput();
		
		glEbsMapInput.setEbsId( jsonObject.get("ebsId").getAsString() );
		glEbsMapInput.setTypCd( jsonObject.get("typCd").getAsString() );
		/*glEbsMapInput.setGlAcct( jsonObject.get("glAcct").getAsString() );*/
		
		EBSTypeCodeDAO ebsTypeCodeDAO = new EBSTypeCodeDAO();
		
		/*ebsTypeCodeDAO.deleteGLEBSMapping(starManResponse, glEbsMapInput);*/

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starManResponse));
	}
	
}
