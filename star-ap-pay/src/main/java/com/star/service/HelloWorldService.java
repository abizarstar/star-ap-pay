package com.star.service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

@Path("/hello")
public class HelloWorldService {

	@GET
    @Path("/{name}")
    public Response getMsg(@PathParam("name") String name) {
  
        String output = "Welcome ABC  : " + name;
        
        System.out.println(output);
        
       /* DocTypeDetailsDAO docTypeDetails = new DocTypeDetailsDAO();
        
        if(name.equals("add"))
        {
        	docTypeDetails.addDocType();
        }
        else  if(name.equals("del"))
        {
        	docTypeDetails.delDocType();
        }
        else  if(name.equals("upd"))
        {
        	docTypeDetails.updDocType();
        }*/
        
        return Response.status(200).entity(output).build();
  
    }
	
	
	/*@GET
    @Path("/{query}")
	@Consumes({MediaType.APPLICATION_JSON})
    public Response getUserName(@PathParam("query") Object username) {
  
		System.out.println("-----*************************------Hi Hi Hi Hi hI-----*******************---");
		
       String output = "Welcome Name  : " ;//+ String.valueOf(username);
        
        //System.out.println(output);
        
        
       // String output2 = "Welcome Name  : " + username;
        
        //System.out.println(output2);
        
      
        return Response.status(200).entity(output).build();
  
    }*/
	
	
	
	
}
