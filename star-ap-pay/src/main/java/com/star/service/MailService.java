package com.star.service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.ManageDirectory;
import com.star.common.Secured;
import com.star.common.SendEmail;
import com.star.common.remit.RemitDoc;
import com.star.dao.CstmDocInfoDAO;
import com.star.dao.GetCommonDAO;
import com.star.dao.PaymentDAO;
import com.star.dao.RemitDAO;
import com.star.dao.UserDetailsDAO;
import com.star.linkage.auth.UserBrowseOutput;
import com.star.linkage.company.Address;
import com.star.linkage.cstmdocinfo.CstmDocInfoBrowseOutput;
import com.star.linkage.cstmdocinfo.VchrInfo;
import com.star.linkage.other.MailInfoInput;
import com.star.linkage.payment.PaymentBrowseOutput;
import com.star.linkage.payment.PaymentInfo;
import com.star.linkage.payment.PaymentInfoOutput;

@Path("/sendmail")
public class MailService {

	public static String htmlTblStrt = "<table style='width:60%;     border: 1px solid #f3f3f3;'>";
	public static String htmlTblStrtWkf = "<table style='width:25%;     border: 1px solid #f3f3f3;'>";
	public static String htmlTblEnd = "</table>";
	public static String htmlTblHdrStrt = "<thead>";
	public static String htmlTblHdrEnd = "</thead>";
	public static String htmlTblBodyStrt = "<tbody>";
	public static String htmlTblBodyEnd = "</tbody>";
	public static String htmlTblTRStrt = "<tr>";
	public static String htmlTblTREnd = "</tr>";
	public static String htmlTblTDStrt = "<td>";
	public static String htmlTblTDStrtWkf = "<td style='border-bottom: 2px solid #f3f3f3;border-bottom-width: 1px'>";
	public static String htmlTblTDEnd = "</td>";
	public static String htmlTblTHStrt = "<th style='background: rgba(3, 169, 243, 0.15);     border-top: 0;     border-bottom-width: 1px;     font-family: \"roboto-medium\", sans-serif;     font-weight: initial;     border-bottom: 2px solid #f3f3f3;     font-size: 13px;'>";
	
	public static String htmlTblTHStrtWkf = "<th style='background: rgba(3, 169, 243, 0.15);border-top: 0;     border-bottom-width: 1px;     font-family: \"roboto-medium\", sans-serif;     font-weight: initial;     border-bottom: 2px solid #f3f3f3;     font-size: 13px;padding: 5px;text-align:right; font-weight:bold'>";
	
	public static String htmlTblTHEnd = "</th>";
	public static String htmlBody = "<html> <head>  <style>  .tbl-head{ background:rgba(3,169,243,0.15);border-top:0;border-bottom-width:1px;font-family:&quot;roboto-medium&quot;,sans-serif;font-weight:initial;border-bottom:2px solid #f3f3f3;font-size:14px; font-weight:bold;  }  </style>  </head> <body>";
	public static String htmlBodyEnd = "</body> </html>";

	public static String tempEmailBody = "<!DOCTYPE html> <html> 	<head> 	<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'> 	<style>table{border:1px solid rgba(33, 150, 243, 0.27);border-collapse: collapse;text-align: left;} table th, table td{padding:7px; border: 1px solid rgba(33, 150, 243, 0.27);} table th{background: rgba(33, 150, 243, 0.18); color: #1680d6;}</style> 	</head> 	<body> 		<p>New Invoice received for Review, below are the details</p> 		<br> 		<table> 			<thead></thead> 			<tbody> 				<tr> 					<th>Invoice No.</th> 					<td>91257532</td> 				</tr> 				<tr> 					<th>Uploaded On</th> 					<td>11/25/2020</td> 				</tr> 				<tr> 					<th>Uploaded By</th> 					<td>Rijo Thomas</td> 				</tr> 				<tr> 					<th>Amount</th> 					<td>1,789,043.83 USD</td> 				</tr> 				<tr> 					<th>Invoice Date</th> 					<td>04/24/2020</td> 				</tr> 				<tr> 					<th>Due Date</th> 					<td>05/24/2020</td> 				</tr> 			</tbody> 		</table> 		<br> 		Regards, 		<br>STAR Software 	</body> </html>";

	@POST
	@Secured
	@Path("/mail")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response sendEmail(@HeaderParam("user-id") String userId, String data) throws Exception {

		SendEmail sendmail = new SendEmail();

		MailInfoInput mailInfoInput = new MailInfoInput();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		String emlFrm = jsonObject.get("emlFrm").getAsString();
		String emlFrmNm = jsonObject.get("emlFrmNm").getAsString();
		String emlTo = jsonObject.get("emlTo").getAsString();
		String emlCc = jsonObject.get("emlCc").getAsString();
		String emlBcc = jsonObject.get("emlBcc").getAsString();
		String emlSub = jsonObject.get("emlSub").getAsString();
		String emlBody = jsonObject.get("emlBody").getAsString();
		String emlDocId = jsonObject.get("emlDocId").getAsString();

		mailInfoInput.setEmlFrm(emlFrm);
		mailInfoInput.setEmlFrmUsrNm(emlFrmNm);
		mailInfoInput.setEmlTo(emlTo);
		mailInfoInput.setEmlCc(emlCc);
		mailInfoInput.setEmlBcc(emlBcc);
		mailInfoInput.setEmlSub(emlSub);
		mailInfoInput.setEmlBody(emlBody);
		mailInfoInput.setEmlAttachIds(emlDocId);

		return sendmail.sendEmail(mailInfoInput);

	}
	
	
	@POST
	@Secured
	@Path("/mail-attch")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response sendEmailWithAttch(@HeaderParam("user-id") String userId, String data) throws Exception {

		SendEmail sendmail = new SendEmail();

		MailInfoInput mailInfoInput = new MailInfoInput();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		String emlFrm = jsonObject.get("emlFrm").getAsString();
		String emlFrmNm = jsonObject.get("emlFrmNm").getAsString();
		String emlTo = jsonObject.get("emlTo").getAsString();
		String emlCc = jsonObject.get("emlCc").getAsString();
		String emlBcc = jsonObject.get("emlBcc").getAsString();
		String emlSub = jsonObject.get("emlSub").getAsString();
		String emlBody = jsonObject.get("emlBody").getAsString();
		String emlDocId = jsonObject.get("emlDocId").getAsString();
		String cmpyId  = jsonObject.get("cmpyId").getAsString();
		String vchrNoStr  = jsonObject.get("vchrNoStr").getAsString();
		String pmntDt  = jsonObject.get("pmntDt").getAsString();
		

		mailInfoInput.setEmlFrm(emlFrm);
		mailInfoInput.setEmlFrmUsrNm(emlFrmNm);
		mailInfoInput.setEmlTo(emlTo);
		mailInfoInput.setEmlCc(emlCc);
		mailInfoInput.setEmlBcc(emlBcc);
		mailInfoInput.setEmlSub(emlSub);
		mailInfoInput.setEmlBody(emlBody);
		mailInfoInput.setEmlAttachIds(emlDocId);
		
		BrowseResponse<CstmDocInfoBrowseOutput> starBrowseResponse = new BrowseResponse<CstmDocInfoBrowseOutput>();
		ManageDirectory directory = new ManageDirectory();

		starBrowseResponse.setOutput(new CstmDocInfoBrowseOutput());
		
		String vchrNo[] = vchrNoStr.split(",");
		List<String> vchrList = new ArrayList<String>();
		
		vchrList = Arrays.asList(vchrNo);
		
		RemitDAO remitDAO = new RemitDAO();

		remitDAO.readVouchers(starBrowseResponse, "", "", "", pmntDt, "",vchrList);
		
		String venId = "";
		String flNm="";
		
		javax.ws.rs.core.Response.ResponseBuilder responseBuilder = null;
		
		if(starBrowseResponse.output.fldTblDoc.size() > 0)
		{
			venId = starBrowseResponse.output.fldTblDoc.get(0).getVchrVenId();
			
			GetCommonDAO commonDAO = new GetCommonDAO();
			
			Address cmpyAddr = commonDAO.getCompanyAddress(cmpyId);
			
			Address venAddr = commonDAO.getVendorAddress(cmpyId, venId);
			
			RemitDoc remitDoc = new RemitDoc();
			
			InputStream fileInputStream;
			flNm = remitDoc.genDocument(starBrowseResponse.output.fldTblDoc, cmpyAddr, venAddr, pmntDt);
			
			//fileInputStream = directory.downloadRemit(flNm);
			
			//flNm = flNm.substring(flNm.lastIndexOf("/")+1, flNm.length());
			
			//responseBuilder = javax.ws.rs.core.Response.ok((Object) fileInputStream);

			//responseBuilder.type("application/pdf");
			//responseBuilder.header("Content-Disposition", "attachment;filename=" + flNm);
			//responseBuilder.build();
		}
		return sendmail.sendEmailWithAttch(mailInfoInput,flNm);

	}

	@POST
	@Secured
	@Path("/prop")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response sendEmailProp(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data) throws Exception {

		BrowseResponse<PaymentBrowseOutput> starBrowseResponse = new BrowseResponse<PaymentBrowseOutput>();
		starBrowseResponse.setOutput(new PaymentBrowseOutput());

		SendEmail sendmail = new SendEmail();

		MailInfoInput mailInfoInput = new MailInfoInput();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		String trnTyp = jsonObject.get("trnTyp").getAsString();
		String reqId = jsonObject.get("reqId").getAsString();
		String mailOutput = "";
		
		if (trnTyp.equals("prop")) {
			PaymentInfo info = new PaymentInfo();

			info.setReqId(reqId);
			info.setPaySts("");

			PaymentDAO paymentDAO = new PaymentDAO();

			paymentDAO.readPayment(starBrowseResponse, info, userId, "", "", "", cmpyId);

			BrowseResponse<UserBrowseOutput> starBrowseResponseUsr = new BrowseResponse<UserBrowseOutput>();

			starBrowseResponseUsr.setOutput(new UserBrowseOutput());

			mailOutput = "";
			double vchrAmt = 0.0;

			mailOutput += htmlBody;
			/*mailOutput += "<div style='margin-bottom:10px'>Hello,</div>";*/

			for (int i = 0; i < starBrowseResponse.output.fldTblPayment.size(); i++) {
				if (starBrowseResponse.output.fldTblPayment.get(0).getPaySts().equals("S")) {
					mailOutput += "<div style='margin-bottom:10px'>Below request is pending for your Approval:</div>";
				}

				if (starBrowseResponse.output.fldTblPayment.get(0).getPaySts().equals("R")) {
					mailOutput += "<div style='margin-bottom:10px'>Below request has been Rejected:</div>";
				}

				if (starBrowseResponse.output.fldTblPayment.get(0).getPaySts().equals("A")) {
					mailOutput += "<div style='margin-bottom:10px'>Below request has been Approved:</div>";
				}

				mailOutput += "<div style='margin-bottom:10px'> <span style='color:#03a9f3!important;font-weight:bold'>Proposal Id</span> - "
						+ starBrowseResponse.output.fldTblPayment.get(i).getReqId() + "</div>";
				mailOutput += "<div style='margin-bottom:10px'> <span style='color:#03a9f3!important;font-weight:bold'>Payment Method</span> - "
						+ starBrowseResponse.output.fldTblPayment.get(i).getPayMthd() + "</div>";

				mailOutput += htmlTblStrt;

				mailOutput += htmlTblHdrStrt;
				mailOutput += htmlTblTHStrt + "Vendor" + htmlTblTHEnd;
				mailOutput += htmlTblTHStrt + "Voucher" + htmlTblTHEnd;
				mailOutput += htmlTblTHStrt + "Invoice" + htmlTblTHEnd;
				mailOutput += htmlTblTHStrt + "Amount" + htmlTblTHEnd;
				mailOutput += htmlTblTHStrt + "Branch" + htmlTblTHEnd;
				mailOutput += htmlTblTHStrt + "Notes" + htmlTblTHEnd;
				mailOutput += htmlTblHdrEnd;
				mailOutput += htmlTblBodyStrt;

				for (int j = 0; j < starBrowseResponse.output.fldTblPayment.get(i).getParamInvList().size(); j++) {
					VchrInfo vchrInfo = starBrowseResponse.output.fldTblPayment.get(i).getParamInvList().get(j);

					mailOutput += htmlTblTRStrt;
					mailOutput += htmlTblTDStrt + "<span style='color: #03a9f3 !important;font-size:15px;'>"
							+ vchrInfo.getVchrVenId() + " - " + vchrInfo.getVchrVenNm() + "</span><br>"
							+ "Invoice Date -" + vchrInfo.getVchrInvDtStr() + htmlTblTDEnd;
					mailOutput += htmlTblTDStrt + vchrInfo.getVchrPfx() + "-" + vchrInfo.getVchrNo() + htmlTblTDEnd;
					mailOutput += htmlTblTDStrt + vchrInfo.getVchrInvNo() + htmlTblTDEnd;
					mailOutput += htmlTblTDStrt + vchrInfo.getVchrAmtStr() + " " + vchrInfo.getVchrCry() + htmlTblTDEnd;
					mailOutput += htmlTblTDStrt + vchrInfo.getVchrBrh() + htmlTblTDEnd;
					mailOutput += htmlTblTDStrt + vchrInfo.getNote() + htmlTblTDEnd;
					mailOutput += htmlTblTREnd;
				}
			}

			mailOutput += htmlTblEnd;

			UserDetailsDAO dao = new UserDetailsDAO();

			dao.getUsersAll(starBrowseResponseUsr, starBrowseResponse.output.fldTblPayment.get(0).getCrtdById());

			String crtdByUsr = starBrowseResponseUsr.output.fldTblUsers.get(0).getEmailId();
			String crtdByUsrNm = starBrowseResponseUsr.output.fldTblUsers.get(0).getUserNm();

			starBrowseResponseUsr = new BrowseResponse<UserBrowseOutput>();
			starBrowseResponseUsr.setOutput(new UserBrowseOutput());

			dao.getUsersAll(starBrowseResponseUsr, starBrowseResponse.output.fldTblPayment.get(0).getActnById());

			String actnByUsr = starBrowseResponseUsr.output.fldTblUsers.get(0).getEmailId();
			String actnByUsrNm = starBrowseResponseUsr.output.fldTblUsers.get(0).getUserNm();

			mailOutput += "<div style='margin-top:20px'>Regards,</div>";
			if (starBrowseResponse.output.fldTblPayment.get(0).getPaySts().equals("S")) {
				mailOutput += "<div style='margin-top:10px'>" + crtdByUsrNm + "</div>";
			} else {
				mailOutput += "<div style='margin-top:10px'>" + actnByUsrNm + "</div>";
			}
			mailOutput += htmlBodyEnd;

			if (starBrowseResponse.output.fldTblPayment.get(0).getPaySts().equals("A")) {

				mailOutput = htmlBody + "<div style='margin-bottom:10px'>Hello " + crtdByUsrNm + ",</div>" + mailOutput;

				mailInfoInput.setEmlFrm(actnByUsr);
				mailInfoInput.setEmlTo(crtdByUsr);
				mailInfoInput.setEmlSub("Proposal - " + reqId + " has been Approved");
				mailInfoInput.setEmlBody(mailOutput);
			} else if (starBrowseResponse.output.fldTblPayment.get(0).getPaySts().equals("R")) {

				mailOutput = htmlBody + "<div style='margin-bottom:10px'>Hello " + crtdByUsrNm + ",</div>" + mailOutput;

				mailInfoInput.setEmlFrm(actnByUsr);
				mailInfoInput.setEmlTo(crtdByUsr);
				mailInfoInput.setEmlSub("Proposal - " + reqId + " has been Rejected");
				mailInfoInput.setEmlBody(mailOutput);
			} else if (starBrowseResponse.output.fldTblPayment.get(0).getPaySts().equals("S")) {

				mailOutput = htmlBody + "<div style='margin-bottom:10px'>Hello " + actnByUsrNm + ",</div>" + mailOutput;

				mailInfoInput.setEmlFrm(crtdByUsr);
				mailInfoInput.setEmlTo(actnByUsr);
				mailInfoInput.setEmlSub("Proposal - " + reqId + " has been pending for Approval");
				mailInfoInput.setEmlBody(mailOutput);
			}

		}
		else if(trnTyp.equals("wkf"))
		{
			BrowseResponse<UserBrowseOutput> starBrowseResponseUsr = new BrowseResponse<UserBrowseOutput>();
			starBrowseResponseUsr.setOutput(new UserBrowseOutput());
			
			UserDetailsDAO dao = new UserDetailsDAO();
			
			String upldUsr = jsonObject.get("upldUsr").getAsString();
			String nxtAprvr = jsonObject.get("nxtAprvr").getAsString();
			String lstAprvr = jsonObject.get("lstAprvr").getAsString();
			String rmk = jsonObject.get("rmk").getAsString();
			String sts = jsonObject.get("sts").getAsString();
			String nwSts = jsonObject.get("nwSts").getAsString();
			String ctlNo = jsonObject.get("ctlNo").getAsString();
			
			CstmDocInfoDAO docInfoDAO = new CstmDocInfoDAO();
			
			BrowseResponse<CstmDocInfoBrowseOutput> starBrowseResponseInvoice = new BrowseResponse<CstmDocInfoBrowseOutput>();
			starBrowseResponseInvoice.setOutput(new CstmDocInfoBrowseOutput());
			docInfoDAO.viewRecord(starBrowseResponseInvoice, Integer.parseInt(ctlNo), cmpyId);
			
			
			mailOutput += htmlTblStrtWkf;

			mailOutput += htmlTblHdrStrt;
			mailOutput += htmlTblTRStrt + htmlTblTHStrtWkf + "File Name" + htmlTblTHEnd + htmlTblTDStrtWkf + starBrowseResponseInvoice.output.fldTblDoc.get(0).getFlPdfName() + htmlTblTDEnd + htmlTblTREnd;
			mailOutput += htmlTblTRStrt + htmlTblTHStrtWkf + "Vendor" + htmlTblTHEnd + htmlTblTDStrtWkf + starBrowseResponseInvoice.output.fldTblDoc.get(0).getVchrVenId() + "-" + starBrowseResponseInvoice.output.fldTblDoc.get(0).getVchrVenNm() + htmlTblTDEnd  + htmlTblTREnd;
			mailOutput += htmlTblTRStrt + htmlTblTHStrtWkf + "Invoice" + htmlTblTHEnd + htmlTblTDStrtWkf + starBrowseResponseInvoice.output.fldTblDoc.get(0).getVchrInvNo() + htmlTblTDEnd + htmlTblTREnd;
			if(starBrowseResponseInvoice.output.fldTblDoc.get(0).getVchrAmtStr() != null)
			{
				mailOutput += htmlTblTRStrt + htmlTblTHStrtWkf + "Amount" + htmlTblTHEnd + htmlTblTDStrtWkf + starBrowseResponseInvoice.output.fldTblDoc.get(0).getVchrAmtStr() + " " + starBrowseResponseInvoice.output.fldTblDoc.get(0).getVchrCry() + htmlTblTDEnd + htmlTblTREnd;
			}
			else
			{
				mailOutput += htmlTblTRStrt + htmlTblTHStrtWkf + "Amount" + htmlTblTHEnd + htmlTblTDStrtWkf + "-" + htmlTblTDEnd + htmlTblTREnd;
			}
			mailOutput += htmlTblTRStrt + htmlTblTHStrtWkf + "Branch" + htmlTblTHEnd + htmlTblTDStrtWkf + starBrowseResponseInvoice.output.fldTblDoc.get(0).getVchrBrh() + htmlTblTDEnd + htmlTblTREnd;
			mailOutput += htmlTblTRStrt + htmlTblTHStrtWkf + "Invoice Date" + htmlTblTHEnd + htmlTblTDStrtWkf + starBrowseResponseInvoice.output.fldTblDoc.get(0).getVchrInvDtStr() + htmlTblTDEnd +  htmlTblTREnd;
			mailOutput += htmlTblTRStrt + htmlTblTHStrtWkf + "Comments" + htmlTblTHEnd + htmlTblTDStrtWkf + rmk + htmlTblTDEnd +  htmlTblTREnd;
			mailOutput += htmlTblHdrEnd;
			mailOutput += htmlTblBodyStrt;
			
			starBrowseResponseUsr = new BrowseResponse<UserBrowseOutput>();
			starBrowseResponseUsr.setOutput(new UserBrowseOutput());

			dao.getUsersAll(starBrowseResponseUsr, upldUsr);

			String upldByUsr = starBrowseResponseUsr.output.fldTblUsers.get(0).getEmailId();
			String upldByUsrNm = starBrowseResponseUsr.output.fldTblUsers.get(0).getUserNm();
			
			starBrowseResponseUsr = new BrowseResponse<UserBrowseOutput>();
			starBrowseResponseUsr.setOutput(new UserBrowseOutput());

			dao.getUsersAll(starBrowseResponseUsr, nxtAprvr);

			String nxtUsr = starBrowseResponseUsr.output.fldTblUsers.get(0).getEmailId();
			String nxtUsrNm = starBrowseResponseUsr.output.fldTblUsers.get(0).getUserNm();
			
			mailOutput = htmlBody + "<div style='margin-bottom:10px'>Hello " + nxtUsrNm + ", "
					+ "<br> <br> URL -  http://thscrapauto:8080/star-ocr-ap/rcvd-invc.html?btn=rvw&ctlNo=" + ctlNo
					+ "<br> <br> Below are the Invoice Details:<br></div>" 
					+ mailOutput;
			
			mailInfoInput.setEmlFrm(CommonConstants.DFLT_EMAIL);
			mailInfoInput.setEmlTo(nxtUsr);
			mailInfoInput.setEmlCc(upldByUsr);
			
			if(sts.equals("S"))
			{	
				mailInfoInput.setEmlSub("Workflow Update - Pending for Approval");
			}
			else if(nwSts.equals("S") && sts.equals("A"))
			{
				mailInfoInput.setEmlSub("Workflow Update - Approved and Assigned to " + nxtUsrNm);
			}
			else if(nwSts.equals("S") && sts.equals("B"))
			{
				mailInfoInput.setEmlSub("Workflow Update - Sent Back (Rejected)" );
			}
			else if(nwSts.equals("S") && sts.equals("F"))
			{
				mailInfoInput.setEmlSub("Workflow Update - Sent Forward (Rejected)");
			}
			else if(sts.equals("R"))
			{
				mailInfoInput.setEmlSub("Workflow Update - Rejected");
			}
			else if(sts.equals("A") && nwSts.equals("A"))
			{
				mailInfoInput.setEmlSub("Workflow Update - Approved");
			}
			else
			{
				mailInfoInput.setEmlSub("");
			}
			
			mailOutput += "<div style='margin-top:20px'>Regards,</div>";
			mailOutput += "<br> Star Software";
			
			mailInfoInput.setEmlBody(mailOutput);
			
			if(sts.equals("RV"))
			{
				return Response.ok().build();
			}
		}

		/*
		 * mailInfoInput.setEmlSub("Invoice - 91257532 received for Review" );
		 * 
		 * mailInfoInput.setEmlBody(tempEmailBody);
		 */
		
		return sendmail.sendEmail(mailInfoInput);

	}
	
}
