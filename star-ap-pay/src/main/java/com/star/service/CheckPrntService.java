package com.star.service;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.ManageDirectory;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.common.remit.CheckPrntDoc;
import com.star.dao.CheckPrntDAO;
import com.star.dao.GetCommonDAO;
import com.star.dao.RemitDAO;
import com.star.linkage.company.Address;
import com.star.linkage.cstmdocinfo.CstmDocInfoBrowseOutput;

@Path("/print-check")
public class CheckPrntService {

	@POST
	@Secured
	@Path("/read")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response readInvoiceInfo(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			@HeaderParam("user-grp") String userGrp, String data) throws Exception {

		BrowseResponse<CstmDocInfoBrowseOutput> starBrowseResponse = new BrowseResponse<CstmDocInfoBrowseOutput>();

		starBrowseResponse.setOutput(new CstmDocInfoBrowseOutput());

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		String invcDt = jsonObject.get("invcDt").getAsString();
		String invcDtTo = jsonObject.get("invcDtTo").getAsString();
		String venId = jsonObject.get("venId").getAsString();
		String pmntDt = jsonObject.get("pmntDt").getAsString();
		String cry = jsonObject.get("cry").getAsString();

		List<String> vchrList = new ArrayList<String>();
		
		CheckPrntDAO checkDocDAO = new CheckPrntDAO();

		checkDocDAO.readVouchers(starBrowseResponse, venId, invcDt, invcDtTo, pmntDt, cry, vchrList);

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblDoc.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputInvoices(starBrowseResponse));
	}

	private GenericEntity<?> getGenericBrowseOutputInvoices(
			BrowseResponse<CstmDocInfoBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<CstmDocInfoBrowseOutput>>(starBrowseResponse) {
		};
	}

	@GET
	@Path("/pdf")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response genCheck(@HeaderParam("user-id") String userId, @QueryParam("cmpyId") String cmpyId,
			@HeaderParam("user-grp") String userGrp, @QueryParam("vchrNo") String vchr, @QueryParam("pmntDt") String pmntDt, String data) throws Exception {

		BrowseResponse<CstmDocInfoBrowseOutput> starBrowseResponse = new BrowseResponse<CstmDocInfoBrowseOutput>();
		ManageDirectory directory = new ManageDirectory();

		starBrowseResponse.setOutput(new CstmDocInfoBrowseOutput());
		
		String vchrNo[] = vchr.split(",");
		List<String> vchrList = new ArrayList<String>();
		
		vchrList = Arrays.asList(vchrNo);
		
		CheckPrntDAO checkDocDAO = new CheckPrntDAO();

		checkDocDAO.readVouchers(starBrowseResponse, "", "", "", "", "",vchrList);
		
		String venId = "";
		
		javax.ws.rs.core.Response.ResponseBuilder responseBuilder = null;
		
		if(starBrowseResponse.output.fldTblDoc.size() > 0)
		{
			venId = starBrowseResponse.output.fldTblDoc.get(0).getVchrVenId();
			
			GetCommonDAO commonDAO = new GetCommonDAO();
			
			Address cmpyAddr = commonDAO.getCompanyAddress(cmpyId);
			
			Address venAddr = commonDAO.getVendorAddress(cmpyId, venId);
			
			CheckPrntDoc checkDoc = new CheckPrntDoc();
			
			InputStream fileInputStream;
			String flNm = checkDoc.genDocument(starBrowseResponse.output.fldTblDoc, cmpyAddr, venAddr, pmntDt);			
			
			String flNmStamp = checkDoc.waterMarkPDF(flNm);
			
			fileInputStream = directory.downloadRemit(flNmStamp);
			
			flNmStamp = flNmStamp.substring(flNmStamp.lastIndexOf("/")+1, flNmStamp.length());
			
			responseBuilder = javax.ws.rs.core.Response.ok((Object) fileInputStream);

			responseBuilder.type("application/pdf");
			responseBuilder.header("Content-Disposition", "attachment;filename=" + flNmStamp);
			
		}
		
		return responseBuilder.build();

	}
}
