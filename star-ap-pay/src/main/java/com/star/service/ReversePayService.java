package com.star.service;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.MaintanenceResponse;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.dao.ReversePayDAO;
import com.star.linkage.cstmdocinfo.CstmDocInfoBrowseOutput;
import com.star.linkage.payment.ReversePayManOutput;
import com.star.modal.CstmParamInv;

@Path("/reverse")
public class ReversePayService {

	@POST
	@Secured
	@Path("/read")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response readInvoiceInfo(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			@HeaderParam("user-grp") String userGrp, String data) throws Exception {

		BrowseResponse<CstmDocInfoBrowseOutput> starBrowseResponse = new BrowseResponse<CstmDocInfoBrowseOutput>();

		starBrowseResponse.setOutput(new CstmDocInfoBrowseOutput());

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		String reqId = jsonObject.get("reqId").getAsString();
		String venId = jsonObject.get("venId").getAsString();
		String pmntDt = jsonObject.get("pmntDt").getAsString();
		String cry = jsonObject.get("cry").getAsString();

		List<String> vchrList = new ArrayList<String>();
		
		ReversePayDAO revPayDAO = new ReversePayDAO();

		revPayDAO.readVouchers(starBrowseResponse, venId, reqId, pmntDt, cry, vchrList);

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblDoc.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputInvoices(starBrowseResponse));
	}
	
	
	@POST
	@Secured
	@Path("/upd-sts")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response updateStatus(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data) throws Exception {

		MaintanenceResponse<ReversePayManOutput> starManResponse = new MaintanenceResponse<ReversePayManOutput>();
		starManResponse.setOutput(new ReversePayManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		
		JsonArray vchrArr = jsonObject.get("vchr").getAsJsonArray();
		List<CstmParamInv> vchrInfos = new ArrayList<CstmParamInv>();
		
		for(int i =0; i < vchrArr.size(); i++)
		{
			CstmParamInv paramInv = new CstmParamInv();
			
			JsonObject vchrObject = vchrArr.get(i).getAsJsonObject();
			
			paramInv.setReqId(vchrObject.get("reqId").getAsString());
			paramInv.setInvNo(vchrObject.get("vchrNo").getAsString());
			
			vchrInfos.add(paramInv);
			
		}
		
		ReversePayDAO revDAO = new ReversePayDAO();

		revDAO.updateInvoice(starManResponse, vchrInfos, userId, cmpyId);
		

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starManResponse));
	}

	private GenericEntity<?> getGenericBrowseOutputInvoices(
			BrowseResponse<CstmDocInfoBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<CstmDocInfoBrowseOutput>>(starBrowseResponse) {
		};
	}
	
	private GenericEntity<?> getGenericMaintenanceOutput(
			MaintanenceResponse<ReversePayManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<ReversePayManOutput>>(starMaintenanceResponse) {
		};
	}
}
