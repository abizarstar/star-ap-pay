package com.star.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.CommonFunctions;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.dao.EBSTransactionDAO;
import com.star.linkage.ach.InvoiceInfo;
import com.star.linkage.cstmdocinfo.CstmDocInfoBrowseOutput;
import com.star.linkage.cstmdocinfo.VchrInfo;
import com.star.linkage.payment.PaymentInfo;

@Path("/ebs")
public class EBSTransactionService {

	@POST
	@Secured
	@Path("/read-vch")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getBank(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			@HeaderParam("user-grp") String userGrp, String data) throws Exception {

		BrowseResponse<CstmDocInfoBrowseOutput> starBrowseResponse = new BrowseResponse<CstmDocInfoBrowseOutput>();
		starBrowseResponse.setOutput(new CstmDocInfoBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String venId = jsonObject.get("venId").getAsString();
		String typCd = jsonObject.get("typCd").getAsString();
	
		PaymentInfo info = new PaymentInfo();
		info.setVenId(venId);
		
		EBSTransactionDAO ebsDao = new EBSTransactionDAO();

		ebsDao.readStxVouchers(starBrowseResponse, venId, typCd);

		List<VchrInfo> fldTblVchrList = starBrowseResponse.output.fldTblDoc;
			
		Collections.sort(fldTblVchrList, new Comparator<VchrInfo>() {
			public int compare(VchrInfo s1, VchrInfo s2) {
				
				int c;
				CommonFunctions commonFunctions = new CommonFunctions();
				
				String d1 = commonFunctions.formatDateWithoutTime(s1.getPmntDt());
				Date x1 = commonFunctions.formatDateWithoutTime(d1);

				String d2 = commonFunctions.formatDateWithoutTime(s2.getPmntDt());
				Date x2 = commonFunctions.formatDateWithoutTime(d2);
				
			    c = x1.compareTo(x2);
			    
			    if (c != 0)
			    {
			    	return c;
			    }
			    
			    String c1 = s1.getReqId();
				String c2 = s2.getReqId();
			
			    c = c1.compareTo(c2);
			    
			    if (c != 0)
			    {
			    	return c;
			    }
			    
			    String str1 = s1.getVchrVenId();
				String str2 = s2.getVchrVenId();
		    	
				return str1.compareTo(str2);
			}
		});
		
		
		Collections.reverse(fldTblVchrList);
		
		/*Collections.sort(fldTblVchrList, new Comparator<VchrInfo>() {
			public int compare(VchrInfo s1, VchrInfo s2) {
				return s1.getVchrVenId().compareTo(s2.getVchrVenId());
			}
		});*/
		
		starBrowseResponse.output.fldTblDoc = fldTblVchrList;
		
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblDoc.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));
	}
	
	
	
	private GenericEntity<?> getGenericBrowseOutput(BrowseResponse<CstmDocInfoBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<CstmDocInfoBrowseOutput>>(starBrowseResponse) {
		};
	}
	
}
