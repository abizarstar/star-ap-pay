package com.star.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.dao.DashboardDAO;
import com.star.linkage.dashboard.APOverviewBrowseOuput;

@Path("/dshbrd")
public class DashboardService {

	@POST
	@Secured
	@Path("/ovrvw")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getAPDisbursment(@HeaderParam("cmpy-id") String cmpyId, String data) throws Exception {
		
		BrowseResponse<APOverviewBrowseOuput> starBrowseResponse = new BrowseResponse<APOverviewBrowseOuput>();
		starBrowseResponse.setOutput(new APOverviewBrowseOuput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String year = jsonObject.get("year").getAsString();
		
		DashboardDAO dao = new DashboardDAO();
		
		dao.getOverviewTotals(starBrowseResponse, cmpyId, year);
		
		return starResponseBuilder.getSuccessResponse(getOverviewDashboard(starBrowseResponse));
	}

	private GenericEntity<?> getOverviewDashboard(
			BrowseResponse<APOverviewBrowseOuput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<APOverviewBrowseOuput>>(starBrowseResponse) {
		};
	}

}
