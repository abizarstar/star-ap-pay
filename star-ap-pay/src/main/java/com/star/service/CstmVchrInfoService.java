package com.star.service;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.security.RolesAllowed;
import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.glassfish.jersey.media.multipart.FormDataBodyPart;
import org.glassfish.jersey.media.multipart.FormDataParam;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.CommonFunctions;
import com.star.common.MaintanenceResponse;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.dao.CstmVchrInfoDAO;
import com.star.linkage.cstmvchrinfo.CstmVchrInfoBrowseOutput;
import com.star.linkage.cstmvchrinfo.CstmVchrInfoInput;
import com.star.linkage.cstmvchrinfo.CstmVchrInfoManOutput;
import com.star.linkage.cstmvchrinfo.VoucherHeader;
import com.star.linkage.cstmvchrinfo.VoucherReconInfoBrowseOutput;


@Path("/vchr")
public class CstmVchrInfoService {

	
	@POST
	@Secured
	@Path("/readvchr")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response readVoucher(@HeaderParam("user-id") String userId, String data) throws Exception {

		List<VoucherHeader> tblHeaderList = new ArrayList<VoucherHeader>();

		BrowseResponse<CstmVchrInfoBrowseOutput> starBrowseResponse = new BrowseResponse<CstmVchrInfoBrowseOutput>();

		starBrowseResponse.setOutput(new CstmVchrInfoBrowseOutput());
	
		try {
			CstmVchrInfoDAO vchrInfoDAO = new CstmVchrInfoDAO();
			
			vchrInfoDAO.readFields(starBrowseResponse);

			StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
			
			starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblDoc.size();

			//starBrowseResponse.output.fldTblHdrList = tblHeaderList;
			starBrowseResponse.output.fldTblHdrList = tblHeaderList;
			return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));

		} catch (Exception e) {
			throw e;
		}

	}
	
	private GenericEntity<?> getGenericBrowseOutput(BrowseResponse<CstmVchrInfoBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<CstmVchrInfoBrowseOutput>>(starBrowseResponse) {
		};
	}
		
	@POST
	@Secured
	@Path("/addvchr")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response addVoucher() throws Exception {
			
		String txtFlPath ="C:\\temp\\upload-0000000005.txt";
		
		 List<String> list = new ArrayList<String>(); 
		   
		    File file = new File(txtFlPath);
		    if(file.exists()){
		        try { 
		            list = Files.readAllLines(file.toPath(),Charset.defaultCharset());
		        } catch (IOException ex) {
		            ex.printStackTrace();
		        }
		    }
		    
		    String venNm ="";
		    String invDate ="";
		    String invNo ="";
		    String dueDate ="";
		    String totalDue ="";
		    String phone ="";
		    String webSite ="";
		    String terms ="";
		    String [] rel = null;
		    String [] ter = null;
		    String firstLine = null;
		    Boolean flg = false ;
		    for(String line : list){
		    	try
		    	{
		    		if (line != null)
			    	{
				    	if (flg == false)
				    	{
				    		firstLine =	line.toString().trim();
				    		Boolean flgChr =false;
				    		String chr="";
				    		Integer j=0;
				    		int count=0;
				    		for (int i = 3; i < firstLine.length(); i++) {
				    			chr =  String.valueOf(firstLine.charAt(i));
				    			count = (int)firstLine.charAt(i);
				    			if (count != 32)
				    			{
				    				venNm = venNm + chr;
				    				flgChr =true;
				    				j=0;
				    			}
				    			else
				    			{
				    				j+=1;
				    				if (j==1 && flgChr==true)
					    			{
				    					venNm = venNm + " ";
					    			}
				    			}
				    			                       
				    			if (j>=2 && flgChr==true)
				    			{
				    				break;
				    			}
				    			
				    			//j = (int)venNm.charAt(i);
				    		}
				    		
				    		if (venNm != null)
				    		{
				    			venNm=venNm.trim();
				    		}
				    		flg = true;
				    	}
		    			rel = line.split(":");
				    	if (rel.length>1)
				    	{
				    		if (rel[0].toString().contains("Invoice date"))
				    		{
				    			if (rel[1] != null)
				    			{
				    				invDate =rel[1].toString().trim();
				    			}
				    		}
				    		else if (rel[0].toString().contains("Invoice number"))
				    		{
				    			if (rel[1] != null)
				    			{
				    			invNo =rel[1].toString().trim();
				    			}
				    		}
				    		else if (rel[0].toString().contains("Due date"))
				    		{
				    			if (rel[1] != null)
				    			{
				    			dueDate =rel[1].toString().trim();
				    			}
				    		}
				    		else if (rel[0].toString().contains("Terms"))
				    		{
				    			if (rel[1] != null)
				    			{
					    			terms =rel[1].toString().trim();
					    			
					    			ter = terms.split(";");
							    	if (ter.length>1)
							    	{
							    		terms =ter[1].toString().trim();
							    	}
				    			}
				    		}
				    		else if (rel[0].toString().contains("Total due"))
				    		{
				    			if (rel[1] != null)
				    			{
					    			totalDue =rel[1].toString().trim();
					    			
					    			Boolean isDigit;
					    			char c = totalDue.charAt(0);
					    			isDigit= (c >= '0' && c <= '9');
					    			
					    			if (isDigit == false)
					    			{
					    				if (totalDue.startsWith(".")==false)
					    				{
					    					totalDue = totalDue.substring(1, totalDue.length());
					    				}
					    			}
				    			}
				    		}
				    		else if (rel[0].toString().contains("Phone"))
				    		{
				    			if (rel[1] != null)
				    			{
				    			phone =rel[1].toString().trim();
				    			}
				    		}
				    		else if (rel[0].toString().contains("Website"))
				    		{
				    			if (rel[1] != null)
				    			{
				    			webSite =rel[1].toString().trim();
				    			}
				    		}
				    		}
				    	}	
		    	}
		    	catch(Exception ex)
		    	{
		    		
		    	}
		  }
		    
		    
		    MaintanenceResponse<CstmVchrInfoManOutput> starMaintenanceResponse = new MaintanenceResponse<CstmVchrInfoManOutput>();

			starMaintenanceResponse.setOutput(new CstmVchrInfoManOutput());
						
			CstmVchrInfoDAO cstmVchrInfoDAO = new CstmVchrInfoDAO();
			
			CstmVchrInfoInput cstmVchrInfo = new CstmVchrInfoInput();
			
			int vchrCtlNo = cstmVchrInfoDAO.getVchrCtlNo();
				
			List<String> venDtls = cstmVchrInfoDAO.getVenderID(venNm);
		    //TODO
			String brh ="CHI";
			String extRef ="TXT";
						
			CommonFunctions objCom = new CommonFunctions();
						
			cstmVchrInfo.setVchrCtlNo(vchrCtlNo);
			cstmVchrInfo.setVchrInvNo(invNo);
			
			if(venDtls.size() > 0)
			{
				cstmVchrInfo.setVchrVenId(venDtls.get(0));
			}
			cstmVchrInfo.setVchrVenNm(venNm);
			cstmVchrInfo.setVchrBrh(brh);
			cstmVchrInfo.setVchrInvDt(objCom.formatDateWithoutTime(invDate));
			cstmVchrInfo.setVchrDueDt(objCom.formatDateWithoutTime(dueDate));
			cstmVchrInfo.setVchrExtRef(extRef);
			cstmVchrInfo.setVchrPayTerm(terms);
			cstmVchrInfo.setVchrAmt(Double.parseDouble(totalDue));
			
			cstmVchrInfoDAO.addCstmVoucher(starMaintenanceResponse, cstmVchrInfo);
		
		return Response.ok("Voucher Info saved successfully !!").build();
	}
	
	
	@POST
	@Secured
	@Path("/recon")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response readReconDetails(@HeaderParam("user-id") String userId, String data) throws Exception {


		BrowseResponse<VoucherReconInfoBrowseOutput> starBrowseResponse = new BrowseResponse<VoucherReconInfoBrowseOutput>();

		starBrowseResponse.setOutput(new VoucherReconInfoBrowseOutput());
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		
		String cmpyId = jsonObject.get("cmpyId").getAsString();
		String brhId = jsonObject.get("brhId").getAsString();
		String venId = jsonObject.get("venId").getAsString();
		String cry = jsonObject.get("cry").getAsString();
		String ctlNo = jsonObject.get("ctlNo").getAsString();
		
		CstmVchrInfoDAO vchrInfoDAO = new CstmVchrInfoDAO();
		
		vchrInfoDAO.readVoucherRecon(starBrowseResponse, cmpyId, brhId, venId, cry, ctlNo);

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblRecon.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputRecon(starBrowseResponse));
	}
	
	private GenericEntity<?> getGenericBrowseOutputRecon(BrowseResponse<VoucherReconInfoBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<VoucherReconInfoBrowseOutput>>(starBrowseResponse) {
		};
	}
	
	

}
