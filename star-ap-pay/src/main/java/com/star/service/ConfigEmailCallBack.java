package com.star.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.google.common.collect.ImmutableMap;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.dao.UsrEmlConfigDAO;

/**
 * Servlet implementation class ConfigEmailCallBack
 */
public class ConfigEmailCallBack extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private final String clientId = "135558245086-14tjdi1gatcpo352v9kjveb4sl1f7np3.apps.googleusercontent.com";
	private final String clientSecret = "UxBcOKMBozP8JSU83GMiRHpf";  
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ConfigEmailCallBack() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		   if (request.getParameter("error") != null) {
			   response.getWriter().println(request.getParameter("error"));
		    return;
		   }
		   
		   // google returns a code that can be exchanged for a access token
		   String code = request.getParameter("code");
		   
		   // get the access token by post to Google
		   String body = post("https://accounts.google.com/o/oauth2/token", ImmutableMap.<String,String>builder()
		     .put("code", code)
		     .put("client_id", clientId)
		     .put("client_secret", clientSecret)
		     .put("redirect_uri", "http://localhost:8080/star-ocr-ap/ConfigEmailCallBack")
		     .put("grant_type", "authorization_code").build());

		   JsonObject jsonObject = null;
		   
		   jsonObject = (JsonObject)new JsonParser().parse(body);
		   
		   String accessToken =  jsonObject.get("access_token").getAsString();
		   
		   String refreshToken =  jsonObject.get("refresh_token").getAsString();
		   
		   request.getSession().setAttribute("access_token", accessToken);
		   
		   String json = get(new StringBuilder("https://www.googleapis.com/oauth2/v1/userinfo?access_token=").append(accessToken).toString());
		   
		   
		   jsonObject = (JsonObject)new JsonParser().parse(json);
		   
		   String email =  jsonObject.get("email").getAsString();
		   
		   if(email.length() > 0)
		   {
			   UsrEmlConfigDAO configDAO = new UsrEmlConfigDAO();
			   
			   if(configDAO.validateEmail(email) == 0)
			   {
				   configDAO.addEmlInfo(email, accessToken, refreshToken, "", "API", "");
			   }
			   else
			   {
				   configDAO.updEmlInfo(email, accessToken, refreshToken);
			   }
		   }
		   
		   response.sendRedirect("email-config.html");
	
	}
	
	public String get(String url) throws ClientProtocolException, IOException {
		  return execute(new HttpGet(url));
		 }

	 public String post(String url, Map<String,String> formParameters) throws ClientProtocolException, IOException { 
		  HttpPost request = new HttpPost(url);
		   
		  List <NameValuePair> nvps = new ArrayList <NameValuePair>();
		  
		  for (String key : formParameters.keySet()) {
		   nvps.add(new BasicNameValuePair(key, formParameters.get(key))); 
		  }

		  request.setEntity(new UrlEncodedFormEntity(nvps));
		  
		  return execute(request);
		 }
	 
	 private String execute(HttpRequestBase request) throws ClientProtocolException, IOException {
		  HttpClient httpClient = new DefaultHttpClient();
		  HttpResponse response = httpClient.execute(request);
		     
		  HttpEntity entity = response.getEntity();
		     String body = EntityUtils.toString(entity);

		  if (response.getStatusLine().getStatusCode() != 200) {
		   throw new RuntimeException("Expected 200 but got " + response.getStatusLine().getStatusCode() + ", with body " + body);
		  }

		     return body;
		 }
}
