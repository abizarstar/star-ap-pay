package com.star.service;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.soap.SOAPFaultException;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.invera.stratix.schema.common.datatypes.AuthenticationToken;
import com.invera.stratix.services.StratixFault;
import com.invera.stratix.services.advPmt.AdvPmntDistService;
import com.invera.stratix.services.advPmt.AdvancePmtDist;
import com.invera.stratix.services.arDist.ARDist;
import com.invera.stratix.services.arDist.AcctRecDistService;
import com.invera.stratix.services.cashRcpt.CashRct;
import com.invera.stratix.services.cashRcpt.CashReceiptService;
import com.invera.stratix.services.cashRcptSsn.CashRcptSsnService;
import com.invera.stratix.services.cashRcptSsn.CashRctSession;
import com.invera.stratix.services.glDist.GLDist;
import com.invera.stratix.services.glDist.GLDistributionService;
import com.invera.stratix.services.unDebt.UnappliedDebitDist;
import com.invera.stratix.services.unDebt.UnpldDebitDistService;
import com.star.common.CommonConstants;
import com.star.common.CommonFunctions;
import com.star.common.MaintanenceResponse;
import com.star.common.StarResponseBuilder;
import com.star.dao.ARInfoDAO;
import com.star.dao.BankStmtDAO;
import com.star.dao.GLInfoDAO;
import com.star.linkage.rcpt.RcptInfo;
import com.star.linkage.rcpt.RcptMaintenanceOutput;
import com.ws.samples.ServiceHelper;
import com.ws.samples.handler.MsgSOAPHandler;
import com.ws.samples.handler.SecSOAPHandler;

@Path("/rcpt")
public class CashReciptService {

	@POST
	@Path("/crtssn")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response createSession(String data, @HeaderParam("user-id") String userId,
			@HeaderParam("auth-token") String authToken, @HeaderParam("app-host") String appHost,
			@HeaderParam("app-port") String appPort, @HeaderParam("cmpy-id") String cmpyId) throws Exception {

		MaintanenceResponse<RcptMaintenanceOutput> starManResponse = new MaintanenceResponse<RcptMaintenanceOutput>();
		starManResponse.setOutput(new RcptMaintenanceOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		SecSOAPHandler securityHandler = null;
		MsgSOAPHandler messageHandler = null;
		CommonFunctions objCom = new CommonFunctions();
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		int iSessionNo;
		String ssnId = jsonObject.get("ssnId").getAsString();
		String brh = jsonObject.get("brh").getAsString();
		String jrnlDt = jsonObject.get("jrnlDt").getAsString();
		String bnk = jsonObject.get("bnk").getAsString();
		String dpstExchngRt = jsonObject.get("dpstExchngRt").getAsString();
		String dpstDt = jsonObject.get("dpstDt").getAsString();
		String cry1 = jsonObject.get("cry1").getAsString();
		String dpstExchngRt1 = jsonObject.get("dpstExchngRt1").getAsString();
		String cry2 = jsonObject.get("cry2").getAsString();
		String dpstExchngRt2 = jsonObject.get("dpstExchngRt2").getAsString();
		String cry3 = jsonObject.get("cry3").getAsString();
		String dpstExchngRt3 = jsonObject.get("dpstExchngRt3").getAsString();
		String dpstAmt = jsonObject.get("dpstAmt").getAsString();
		String nbrChk = jsonObject.get("nbrChk").getAsString();

		CreateCashRecipt cashReciptService = new CreateCashRecipt();
		securityHandler = cashReciptService.createSecurityHandlers(userId, authToken, appHost, appPort, cmpyId);
		messageHandler = cashReciptService.createMessageHandlers(userId, authToken, appHost, appPort, cmpyId);

		try {

			CashRcptSsnService cashReceiptSessionService = ServiceHelper.createRcptSsnService(messageHandler,
					securityHandler);

			messageHandler.clearMessages();

			CashRctSession cashRctSession = new CashRctSession();

			/* cashRctSession.setSessionIdentity(value); */
			if (brh.trim().length() > 0) {
				cashRctSession.setCashRctBranchId(brh);
			}
			if (jrnlDt.trim().length() > 0) {

				String tmpJrnlDt = objCom.formatDateWithoutTimeVou(jrnlDt);
				XMLGregorianCalendar xmlJrnlDt = DatatypeFactory.newInstance().newXMLGregorianCalendar(tmpJrnlDt);
				cashRctSession.setJournalDate(xmlJrnlDt);
			}
			if (bnk.trim().length() > 0) {
				cashRctSession.setBankId(bnk);
			}

			if (dpstDt.trim().length() > 0) {

				String tmpDepositDt = objCom.formatDateWithoutTimeVou(dpstDt);
				XMLGregorianCalendar xmlDepositDt = DatatypeFactory.newInstance().newXMLGregorianCalendar(tmpDepositDt);
				cashRctSession.setDepositDate(xmlDepositDt);
			}
			if (cry1.trim().length() > 0) {
				cashRctSession.setArCurrency1(cry1);
			}
			if (dpstExchngRt1.trim().length() > 0) {
				cashRctSession.setArCrossExchangeRate1(new BigDecimal(dpstExchngRt1));
			}
			if (cry2.trim().length() > 0) {
				cashRctSession.setArCurrency2(cry2);
			}
			if (dpstExchngRt2.trim().length() > 0) {
				cashRctSession.setArCrossExchangeRate2(new BigDecimal(dpstExchngRt2));
			}
			if (cry3.trim().length() > 0) {
				cashRctSession.setArCurrency3(cry3);
			}
			if (dpstExchngRt3.trim().length() > 0) {
				cashRctSession.setArCrossExchangeRate3(new BigDecimal(dpstExchngRt3));
			}
			if (dpstAmt.trim().length() > 0) {
				cashRctSession.setControlDepositAmount(new BigDecimal(dpstAmt));
			}
			if (nbrChk.trim().length() > 0) {
				cashRctSession.setControlNumberOfChecks(Integer.parseInt(nbrChk));
			}

			if (ssnId.trim().length() > 0) {
				iSessionNo = Integer.parseInt(ssnId);
			} else {
				iSessionNo = cashReceiptSessionService.createCashRctSession(cashRctSession);
			}

			System.out.println(iSessionNo);

			String cashRcpt = "";

			if (jsonObject.has("cusId")) {
				jsonObject.addProperty("ssnId", String.valueOf(iSessionNo));

				cashRcpt = cashReciptService.createRcpt(securityHandler, messageHandler, jsonObject.toString());
			}

			starManResponse.output.ssnId = iSessionNo;
			starManResponse.output.rcptId = cashRcpt;
		}

		catch (SOAPFaultException | StratixFault fault) {

			System.out.println("ERROR");

			System.out.println(fault.getMessage());

			fault.printStackTrace();

			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(fault.getMessage());
		} finally {
			starManResponse.output.authToken = securityHandler.getAuthenticationToken().getValue();
		}

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputSsn(starManResponse));

	}

	private GenericEntity<?> getGenericMaintenanceOutputSsn(
			MaintanenceResponse<RcptMaintenanceOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<RcptMaintenanceOutput>>(starMaintenanceResponse) {
		};
	}

	@POST
	@Path("/crtRcpt")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response createRcpt(String data, @HeaderParam("user-id") String userId,
			@HeaderParam("auth-token") String authToken, @HeaderParam("app-host") String appHost,
			@HeaderParam("app-port") String appPort, @HeaderParam("cmpy-id") String cmpyId) throws Exception {

		MaintanenceResponse<RcptMaintenanceOutput> starManResponse = new MaintanenceResponse<RcptMaintenanceOutput>();
		starManResponse.setOutput(new RcptMaintenanceOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		SecSOAPHandler securityHandler = null;
		MsgSOAPHandler messageHandler = null;

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		String invNo = jsonObject.get("invNo").getAsString();
		String cusId = jsonObject.get("cusId").getAsString();
		String ssnId = jsonObject.get("ssnId").getAsString();
		String checkNo = jsonObject.get("chkNo").getAsString();
		String checkDesc = jsonObject.get("desc").getAsString();
		String checkAmt = jsonObject.get("cusChkAmt").getAsString();

		CreateCashRecipt cashReciptService = new CreateCashRecipt();
		securityHandler = cashReciptService.createSecurityHandlers(userId, authToken, appHost, appPort, cmpyId);
		messageHandler = cashReciptService.createMessageHandlers(userId, authToken, appHost, appPort, cmpyId);

		try {

			CashReceiptService cashReceiptService = ServiceHelper.createRcptService(messageHandler, securityHandler);

			messageHandler.clearMessages();

			CashRct cashRct = new CashRct();

			/*
			 * if( invNo.trim().length() > 0 ) { cashRct.setCashRctIdentity(invNo); }
			 */

			if (cusId.trim().length() > 0) {
				cashRct.setCustomerId(cusId);
			}

			if (ssnId.trim().length() > 0) {
				cashRct.setSessionIdentity(Integer.parseInt(ssnId));
			}

			if (checkNo.trim().length() > 0) {
				cashRct.setCustomerCheckNumber(checkNo);
			}

			if (checkDesc.trim().length() > 0) {
				cashRct.setCashRctDescription(checkDesc);
			}

			if (checkAmt.trim().length() > 0) {
				cashRct.setCustomerCheckAmount(new BigDecimal(checkAmt));
			}

			String cashRcpt = cashReceiptService.createCashRct(cashRct);

			System.out.println("CASH RCPT---->" + cashRcpt);

			if (invNo.trim().length() > 0) {
				createArDistribution(cashRcpt, securityHandler, messageHandler, invNo, checkAmt);
			}

		}

		catch (SOAPFaultException | StratixFault fault) {

			System.out.println("ERROR");

			System.out.println(fault.getMessage());

			fault.printStackTrace();

			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(fault.getMessage());
		} finally {
			starManResponse.output.authToken = securityHandler.getAuthenticationToken().getValue();
		}

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputCashRcpt(starManResponse));

	}

	private GenericEntity<?> getGenericMaintenanceOutputCashRcpt(
			MaintanenceResponse<RcptMaintenanceOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<RcptMaintenanceOutput>>(starMaintenanceResponse) {
		};
	}

	@POST
	@Path("/arDist")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response createARDistribution(String data, @HeaderParam("user-id") String userId,
			@HeaderParam("auth-token") String authToken, @HeaderParam("app-host") String appHost,
			@HeaderParam("app-port") String appPort, @HeaderParam("cmpy-id") String cmpyId) throws Exception {

		MaintanenceResponse<RcptMaintenanceOutput> starManResponse = new MaintanenceResponse<RcptMaintenanceOutput>();
		starManResponse.setOutput(new RcptMaintenanceOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		SecSOAPHandler securityHandler = null;
		MsgSOAPHandler messageHandler = null;
		// CommonFunctions objCom = new CommonFunctions();
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		String discTknAmt = jsonObject.get("discTknAmt").getAsString();
		String invcNo = jsonObject.get("invcNo").getAsString();
		String pmtAmt = jsonObject.get("pmtAmt").getAsString();
		String refItmNo = jsonObject.get("refItmNo").getAsString();
		String wrtOffAmt = jsonObject.get("wrtOffAmt").getAsString();
		String cashRcptNo = jsonObject.get("cashRcptNo").getAsString();

		CreateCashRecipt cashReciptService = new CreateCashRecipt();
		securityHandler = cashReciptService.createSecurityHandlers(userId, authToken, appHost, appPort, cmpyId);
		messageHandler = cashReciptService.createMessageHandlers(userId, authToken, appHost, appPort, cmpyId);

		try {

			AcctRecDistService arDistributionService = ServiceHelper.createARDist(messageHandler, securityHandler);

			messageHandler.clearMessages();

			ARDist arDist = new ARDist();

			if (discTknAmt.trim().length() > 0) {
				arDist.setDiscountTakenAmount(new BigDecimal(discTknAmt));
			}

			if (invcNo.trim().length() > 0) {
				arDist.setInvoiceNumber(invcNo);
			}

			if (pmtAmt.trim().length() > 0) {
				arDist.setPaymentAmount(new BigDecimal(pmtAmt));
			}

			if (refItmNo.trim().length() > 0) {
				arDist.setRefItemNumber(Integer.parseInt(refItmNo));
			}

			if (wrtOffAmt.trim().length() > 0) {
				arDist.setWriteOffAmount(new BigDecimal(wrtOffAmt));
			}

			int iPymntEntryRef = arDistributionService.createARDist(cashRcptNo, arDist);

			System.out.println("Payment Entry REF--->" + iPymntEntryRef);

			starManResponse.output.refId = iPymntEntryRef;
		} catch (SOAPFaultException | StratixFault fault) {

			System.out.println("ERROR");

			System.out.println(fault.getMessage());

			fault.printStackTrace();

			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(fault.getMessage());
		} finally {
			starManResponse.output.authToken = securityHandler.getAuthenticationToken().getValue();
		}

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputAdvPmt(starManResponse));

	}

	@POST
	@Path("/advPmt")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response createAdvPmt(String data, @HeaderParam("user-id") String userId,
			@HeaderParam("auth-token") String authToken, @HeaderParam("app-host") String appHost,
			@HeaderParam("app-port") String appPort, @HeaderParam("cmpy-id") String cmpyId) throws Exception {

		MaintanenceResponse<RcptMaintenanceOutput> starManResponse = new MaintanenceResponse<RcptMaintenanceOutput>();
		starManResponse.setOutput(new RcptMaintenanceOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		SecSOAPHandler securityHandler = null;
		MsgSOAPHandler messageHandler = null;
		// CommonFunctions objCom = new CommonFunctions();
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		String advAmt = jsonObject.get("advAmt").getAsString();
		String desc = jsonObject.get("desc").getAsString();
		String cashRcptNo = jsonObject.get("cashRcptNo").getAsString();

		CreateCashRecipt cashReciptService = new CreateCashRecipt();
		securityHandler = cashReciptService.createSecurityHandlers(userId, authToken, appHost, appPort, cmpyId);
		messageHandler = cashReciptService.createMessageHandlers(userId, authToken, appHost, appPort, cmpyId);

		try {

			AdvPmntDistService adPaymentDistributionService = ServiceHelper.createAdvPmt(messageHandler,
					securityHandler);

			messageHandler.clearMessages();

			AdvancePmtDist advPmt = new AdvancePmtDist();

			if (advAmt.trim().length() > 0) {
				advPmt.setAdvanceAmount(new BigDecimal(advAmt));
			}

			if (desc.trim().length() > 0) {
				advPmt.setAdvancePmtDescription(desc);
			}

			int iAdvPmtRef = adPaymentDistributionService.createAdvancePmtDist(cashRcptNo, advPmt);

			System.out.println("AR REF--->" + iAdvPmtRef);

			starManResponse.output.refId = iAdvPmtRef;
		} catch (SOAPFaultException | StratixFault fault) {

			System.out.println("ERROR");

			System.out.println(fault.getMessage());

			fault.printStackTrace();

			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(fault.getMessage());
		} finally {
			starManResponse.output.authToken = securityHandler.getAuthenticationToken().getValue();
		}

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputAdvPmt(starManResponse));

	}

	private GenericEntity<?> getGenericMaintenanceOutputAdvPmt(
			MaintanenceResponse<RcptMaintenanceOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<RcptMaintenanceOutput>>(starMaintenanceResponse) {
		};
	}

	@POST
	@Path("/delAdvPmt")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response deleteAdvPmt(String data, @HeaderParam("user-id") String userId,
			@HeaderParam("auth-token") String authToken, @HeaderParam("app-host") String appHost,
			@HeaderParam("app-port") String appPort, @HeaderParam("cmpy-id") String cmpyId) throws Exception {

		MaintanenceResponse<RcptMaintenanceOutput> starManResponse = new MaintanenceResponse<RcptMaintenanceOutput>();
		starManResponse.setOutput(new RcptMaintenanceOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		SecSOAPHandler securityHandler = null;
		MsgSOAPHandler messageHandler = null;
		// CommonFunctions objCom = new CommonFunctions();
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		String refPfx = jsonObject.get("refPfx").getAsString();
		String refNo = jsonObject.get("refNo").getAsString();
		String refItm = jsonObject.get("refItm").getAsString();

		CreateCashRecipt cashReciptService = new CreateCashRecipt();
		securityHandler = cashReciptService.createSecurityHandlers(userId, authToken, appHost, appPort, cmpyId);
		messageHandler = cashReciptService.createMessageHandlers(userId, authToken, appHost, appPort, cmpyId);

		try {
			AdvPmntDistService adPaymentDistributionService = ServiceHelper.createAdvPmt(messageHandler,
					securityHandler);

			messageHandler.clearMessages();
			
			String cashRctIdentity = refPfx + "-" + refNo;
			Integer refItemNumber = Integer.parseInt(refItm);

			adPaymentDistributionService.deleteAdvancePmtDist(cashRctIdentity, refItemNumber);

		} catch (SOAPFaultException | StratixFault fault) {

			System.out.println("ERROR");
			System.out.println(fault.getMessage());

			fault.printStackTrace();

			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(fault.getMessage());
		} finally {
			starManResponse.output.authToken = securityHandler.getAuthenticationToken().getValue();
		}

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputAdvPmtDelete(starManResponse));

	}

	private GenericEntity<?> getGenericMaintenanceOutputAdvPmtDelete(
			MaintanenceResponse<RcptMaintenanceOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<RcptMaintenanceOutput>>(starMaintenanceResponse) {
		};
	}

	@POST
	@Path("/unDebit")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response createUnappliedDebit(String data, @HeaderParam("user-id") String userId,
			@HeaderParam("auth-token") String authToken, @HeaderParam("app-host") String appHost,
			@HeaderParam("app-port") String appPort, @HeaderParam("cmpy-id") String cmpyId) throws Exception {

		MaintanenceResponse<RcptMaintenanceOutput> starManResponse = new MaintanenceResponse<RcptMaintenanceOutput>();
		starManResponse.setOutput(new RcptMaintenanceOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		SecSOAPHandler securityHandler = null;
		MsgSOAPHandler messageHandler = null;
		CommonFunctions objCom = new CommonFunctions();
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		String drAmt = jsonObject.get("drAmt").getAsString();
		String desc = jsonObject.get("desc").getAsString();
		String cashRcptNo = jsonObject.get("cashRcptNo").getAsString();

		CreateCashRecipt cashReciptService = new CreateCashRecipt();
		securityHandler = cashReciptService.createSecurityHandlers(userId, authToken, appHost, appPort, cmpyId);
		messageHandler = cashReciptService.createMessageHandlers(userId, authToken, appHost, appPort, cmpyId);

		try {

			UnpldDebitDistService unappliedDebitDistributionService = ServiceHelper.createUnDebit(messageHandler,
					securityHandler);

			messageHandler.clearMessages();

			UnappliedDebitDist debitDist = new UnappliedDebitDist();

			debitDist.setUnappliedDebitAmount(new BigDecimal(drAmt));
			debitDist.setUnappliedDebitDescription(desc);

			int iRefId = unappliedDebitDistributionService.createUnappliedDebitDist(cashRcptNo, debitDist);

			System.out.println("Unapplied Debit REF--->" + iRefId);

			starManResponse.output.refId = iRefId;
		} catch (SOAPFaultException | StratixFault fault) {

			System.out.println("ERROR");

			System.out.println(fault.getMessage());

			fault.printStackTrace();

			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(fault.getMessage());
		} finally {
			starManResponse.output.authToken = securityHandler.getAuthenticationToken().getValue();
		}

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputAdvPmt(starManResponse));

	}
	

	@POST
	@Path("/delUnDebit")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response deleteUnappliedDebit(String data, @HeaderParam("user-id") String userId,
			@HeaderParam("auth-token") String authToken, @HeaderParam("app-host") String appHost,
			@HeaderParam("app-port") String appPort, @HeaderParam("cmpy-id") String cmpyId) throws Exception {

		MaintanenceResponse<RcptMaintenanceOutput> starManResponse = new MaintanenceResponse<RcptMaintenanceOutput>();
		starManResponse.setOutput(new RcptMaintenanceOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		SecSOAPHandler securityHandler = null;
		MsgSOAPHandler messageHandler = null;
		// CommonFunctions objCom = new CommonFunctions();
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		String refPfx = jsonObject.get("refPfx").getAsString();
		String refNo = jsonObject.get("refNo").getAsString();
		String refItm = jsonObject.get("refItm").getAsString();

		CreateCashRecipt cashReciptService = new CreateCashRecipt();
		securityHandler = cashReciptService.createSecurityHandlers(userId, authToken, appHost, appPort, cmpyId);
		messageHandler = cashReciptService.createMessageHandlers(userId, authToken, appHost, appPort, cmpyId);

		try {
			UnpldDebitDistService unappliedDebitDistributionService = ServiceHelper.createUnDebit(messageHandler,
					securityHandler);

			messageHandler.clearMessages();
			
			String cashRctIdentity = refPfx + "-" + refNo;
			Integer refItemNumber = Integer.parseInt(refItm);

			unappliedDebitDistributionService.deleteUnappliedDebitDist(cashRctIdentity, refItemNumber);

		} catch (SOAPFaultException | StratixFault fault) {

			System.out.println("ERROR");
			System.out.println(fault.getMessage());

			fault.printStackTrace();

			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(fault.getMessage());
		} finally {
			starManResponse.output.authToken = securityHandler.getAuthenticationToken().getValue();
		}

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputAdvPmtDelete(starManResponse));

	}

	@POST
	@Path("/glDist")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response createGLDistribution(String data, @HeaderParam("user-id") String userId,
			@HeaderParam("auth-token") String authToken, @HeaderParam("app-host") String appHost,
			@HeaderParam("app-port") String appPort, @HeaderParam("cmpy-id") String cmpyId) throws Exception {

		MaintanenceResponse<RcptMaintenanceOutput> starManResponse = new MaintanenceResponse<RcptMaintenanceOutput>();
		starManResponse.setOutput(new RcptMaintenanceOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		SecSOAPHandler securityHandler = null;
		MsgSOAPHandler messageHandler = null;
		CommonFunctions objCom = new CommonFunctions();
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		String glAcc = jsonObject.get("glAcc").getAsString();
		String glSubAcc = jsonObject.get("glSubAcc").getAsString();
		String drAmt = jsonObject.get("drAmt").getAsString();
		String crAmt = jsonObject.get("crAmt").getAsString();
		String remark = jsonObject.get("remark").getAsString();
		String cashRcptNo = jsonObject.get("cashRcptNo").getAsString();

		GLInfoDAO glInfoDAO = new GLInfoDAO();

		glSubAcc = glInfoDAO.getSubAcctFmtd(glSubAcc);

		CreateCashRecipt cashReciptService = new CreateCashRecipt();
		securityHandler = cashReciptService.createSecurityHandlers(userId, authToken, appHost, appPort, cmpyId);
		messageHandler = cashReciptService.createMessageHandlers(userId, authToken, appHost, appPort, cmpyId);

		try {

			GLDistributionService glDistributionService = ServiceHelper.createGLDist(messageHandler, securityHandler);

			messageHandler.clearMessages();

			GLDist glDist = new GLDist();

			if (glAcc.trim().length() > 0) {
				glDist.setBasicGLAccount(glAcc);
			}

			if (glSubAcc.trim().length() > 0) {
				glDist.setSubaccount(glSubAcc);
			}

			if (drAmt.trim().length() > 0) {
				glDist.setDebitAmount(new BigDecimal(drAmt));
			}

			if (crAmt.trim().length() > 0) {
				glDist.setCreditAmount(new BigDecimal(crAmt));
			}

			if (remark.trim().length() > 0) {
				glDist.setDistributionRemarks(remark);
			}

			/*
			 * glDist.setBasicGLAccount(value); glDist.setCreditAmount(value);
			 * glDist.setDebitAmount(value); glDist.setSubaccount(value);
			 */

			int iRefId = glDistributionService.createGLDist(cashRcptNo, glDist);

			System.out.println("GL REF--->" + iRefId);

			starManResponse.output.refId = iRefId;
		} catch (SOAPFaultException | StratixFault fault) {

			System.out.println("ERROR");

			System.out.println(fault.getMessage());

			fault.printStackTrace();

			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(fault.getMessage());
		} finally {
			starManResponse.output.authToken = securityHandler.getAuthenticationToken().getValue();
		}

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputAdvPmt(starManResponse));

	}
	

	@POST
	@Path("/delGlDist")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response deleteGLDistribution(String data, @HeaderParam("user-id") String userId,
			@HeaderParam("auth-token") String authToken, @HeaderParam("app-host") String appHost,
			@HeaderParam("app-port") String appPort, @HeaderParam("cmpy-id") String cmpyId) throws Exception {

		MaintanenceResponse<RcptMaintenanceOutput> starManResponse = new MaintanenceResponse<RcptMaintenanceOutput>();
		starManResponse.setOutput(new RcptMaintenanceOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		SecSOAPHandler securityHandler = null;
		MsgSOAPHandler messageHandler = null;
		// CommonFunctions objCom = new CommonFunctions();
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		String refPfx = jsonObject.get("refPfx").getAsString();
		String refNo = jsonObject.get("refNo").getAsString();
		String refItm = jsonObject.get("refItm").getAsString();

		CreateCashRecipt cashReciptService = new CreateCashRecipt();
		securityHandler = cashReciptService.createSecurityHandlers(userId, authToken, appHost, appPort, cmpyId);
		messageHandler = cashReciptService.createMessageHandlers(userId, authToken, appHost, appPort, cmpyId);

		try {
			GLDistributionService glDistributionService = ServiceHelper.createGLDist(messageHandler, securityHandler);

			messageHandler.clearMessages();
			
			String cashRctIdentity = refPfx + "-" + refNo;
			Integer refItemNumber = Integer.parseInt(refItm);

			glDistributionService.deleteGLDist(cashRctIdentity, refItemNumber);

		} catch (SOAPFaultException | StratixFault fault) {

			System.out.println("ERROR");
			System.out.println(fault.getMessage());

			fault.printStackTrace();

			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(fault.getMessage());
		} finally {
			starManResponse.output.authToken = securityHandler.getAuthenticationToken().getValue();
		}

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputAdvPmtDelete(starManResponse));

	}

	public void createArDistribution(String chkReceipt, SecSOAPHandler securityHandler, MsgSOAPHandler messageHandler,
			String invNo, String checkAmt) {
		try {

			AcctRecDistService arDistributionService = ServiceHelper.createARDist(messageHandler, securityHandler);

			messageHandler.clearMessages();

			ARDist arDist = new ARDist();

			// arDist.setInvoiceNumber("CHI SI-2512");
			// arDist.setPaymentAmount(new BigDecimal(200));

			arDist.setInvoiceNumber(invNo);
			arDist.setPaymentAmount(new BigDecimal(checkAmt));
			/*
			 * arDist.setWriteOffAmount(new BigDecimal(100));
			 * arDist.setDiscountTakenAmount(new BigDecimal(20));
			 */

			int iArDistRef = arDistributionService.createARDist(chkReceipt, arDist);

			System.out.println("AR REF--->" + iArDistRef);
		}

		catch (SOAPFaultException | StratixFault fault) {

			System.out.println("ERROR");

			System.out.println(fault.getMessage());

			fault.printStackTrace();

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void createHandlers(SecSOAPHandler securityHandler, MsgSOAPHandler messageHandler,
			@HeaderParam("user-id") String userId, @HeaderParam("auth-token") String authToken,
			@HeaderParam("app-host") String appHost, @HeaderParam("app-port") String appPort,
			@HeaderParam("cmpy-id") String cmpyId) {
		ServiceHelper.setWpaEndpointProtocol("http");

		ServiceHelper.setStxEndpointHostAndPort(appHost, Integer.parseInt(appPort));

		messageHandler = new MsgSOAPHandler();
		AuthenticationToken authenticationToken = new AuthenticationToken();

		authenticationToken.setUsername(userId);
		authenticationToken.setValue(authToken);

		securityHandler = new SecSOAPHandler(authenticationToken);
	}

	@POST
	@Path("/crtssn-mul")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response createRcptMultiple(String data, @HeaderParam("user-id") String userId,
			@HeaderParam("auth-token") String authToken, @HeaderParam("app-host") String appHost,
			@HeaderParam("app-port") String appPort, @HeaderParam("cmpy-id") String cmpyId) throws Exception {

		MaintanenceResponse<RcptMaintenanceOutput> starManResponse = new MaintanenceResponse<RcptMaintenanceOutput>();
		starManResponse.setOutput(new RcptMaintenanceOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		boolean authValidate = true;

		int iSessionNo = 0;
		String tmpBranch = "";
		String cashRcpt = "";
		SecSOAPHandler securityHandler = null;
		MsgSOAPHandler messageHandler = null;
		CommonFunctions objCom = new CommonFunctions();
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		String ssnId = jsonObject.get("ssnId").getAsString();
		String rcpt = jsonObject.get("rcpt").getAsString();
		String payAmt = jsonObject.get("payAmt").getAsString();
		String bnkCode = jsonObject.get("bnk").getAsString();
		String postDt = jsonObject.get("post").getAsString();
		String jrnlDt = jsonObject.get("jrnl").getAsString();
		
		ARInfoDAO arInfoDAO = new ARInfoDAO();
		
		int vldDays = arInfoDAO.vldJournalDays(postDt);
		
		if(vldDays > 0)
		{
			starManResponse.output.rtnSts = 2;
			starManResponse.output.messages.add(CommonConstants.JRNL_DT_ERR.replace("<day>", String.valueOf(vldDays)));
			
			return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputSsn(starManResponse));
		}
		
		GregorianCalendar calendar = new GregorianCalendar();
		
		if(postDt.length() > 0)
		{
			int month = Integer.parseInt(postDt.substring(0, 2));
			int day = Integer.parseInt(postDt.substring(3, 5));
			int year = Integer.parseInt(postDt.substring(6, 10));

			calendar.set(year, month - 1, day);
		}
		
		JsonArray trnArray = jsonObject.get("invTrn").getAsJsonArray();

		List<RcptInfo> rcptInfoList = new ArrayList<RcptInfo>();

		for (int i = 0; i < trnArray.size(); i++) {
			JsonObject invTrn = trnArray.get(i).getAsJsonObject();
			RcptInfo info = new RcptInfo();

			info.setInvNo(invTrn.get("arNo").getAsString());
			info.setBrh(invTrn.get("brh").getAsString());
			info.setCusNo(invTrn.get("cus").getAsString());
			info.setAmount(invTrn.get("amt").getAsString());
			info.setUpdRef(invTrn.get("ref").getAsString());
			info.setDiscAmt(invTrn.get("discAmt").getAsString());
			info.setDueDt(invTrn.get("due").getAsString());
			info.setDiscFlg(invTrn.get("discFlg").getAsString());

			rcptInfoList.add(info);
		}

		Collections.sort(rcptInfoList, new Comparator<RcptInfo>() {
			public int compare(RcptInfo s1, RcptInfo s2) {
				return s1.getBrh().compareToIgnoreCase(s2.getBrh());
			}
		});

		CreateCashRecipt cashReciptService = new CreateCashRecipt();
		securityHandler = cashReciptService.createSecurityHandlers(userId, authToken, appHost, appPort, cmpyId);
		messageHandler = cashReciptService.createMessageHandlers(userId, authToken, appHost, appPort, cmpyId);

		CashRcptSsnService cashReceiptSessionService = ServiceHelper.createRcptSsnService(messageHandler,
				securityHandler);
		
		try {

			messageHandler.clearMessages();

			for (int i = 0; i < rcptInfoList.size(); i++) {

				CashRctSession cashRctSession = new CashRctSession();

				cashRctSession.setCashRctBranchId(rcptInfoList.get(i).getBrh());
				cashRctSession.setBankId(bnkCode);
				
				if(postDt.length() > 0)
				{
					String tmpJrnlDt = objCom.formatDateWithoutTimeVou(postDt);
					XMLGregorianCalendar xmlJrnlDt = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
					cashRctSession.setJournalDate(xmlJrnlDt);
	
					String tmpDepositDt = objCom.formatDateWithoutTimeVou(jrnlDt);
					XMLGregorianCalendar xmlDepositDt = DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
					cashRctSession.setDepositDate(xmlDepositDt);
				}

				if (!tmpBranch.equals(rcptInfoList.get(i).getBrh())) {

					if (ssnId.length() == 0 && rcpt.length() == 0 && i==0) {
						iSessionNo = cashReceiptSessionService.createCashRctSession(cashRctSession);

						jsonObject.addProperty("ssnId", String.valueOf(iSessionNo));
						jsonObject.addProperty("cusId", rcptInfoList.get(i).getCusNo());
						jsonObject.addProperty("chkNo", "");
						jsonObject.addProperty("desc", "");
						jsonObject.addProperty("cusChkAmt", payAmt);
						cashRcpt = cashReciptService.createRcpt(securityHandler, messageHandler, jsonObject.toString());
					}
					else if (ssnId.length() != 0 && rcpt.length() != 0)
					{
						iSessionNo = Integer.parseInt(ssnId);
						cashRcpt = rcpt;
					}
				}

				if (iSessionNo != 0 && cashRcpt.length() > 0) {

					if (rcpt.length() > 0) {
						cashRcpt = rcpt;
					}
					
					if(rcptInfoList.get(i).getUpdRef().length() > 0)
					{
						jsonObject.addProperty("rcptNo", cashRcpt);
						jsonObject.addProperty("checkAmt", rcptInfoList.get(i).getAmount());
						jsonObject.addProperty("invNo", rcptInfoList.get(i).getUpdRef());
						jsonObject.addProperty("discFlg", rcptInfoList.get(i).getDiscFlg());
						jsonObject.addProperty("discAmt", rcptInfoList.get(i).getDiscAmt());
						cashReciptService.createARDistribution(securityHandler, messageHandler, jsonObject.toString());
					}
					
					BankStmtDAO dao = new BankStmtDAO();
					dao.addInvoiceTrn(rcptInfoList.get(i), iSessionNo+ "-" + cashRcpt);
				}

				tmpBranch = rcptInfoList.get(i).getBrh();

			}
			
			
			starManResponse.output.rcptId = cashRcpt;
			starManResponse.output.ssnId = iSessionNo;
			
		} catch (SOAPFaultException | StratixFault fault) {

			System.out.println("ERROR");

			System.out.println(fault.getMessage());
			
			if(fault.getLocalizedMessage().contains("3254 - STRATIX session not found"))
			{
				authValidate = false;
			}
			else if(fault.getLocalizedMessage().contains("nvalid Authentication Token."))
			{
				authValidate = false;
			}
			
			if(authValidate == true)
			{
				cashReceiptSessionService.deleteCashRctSession(iSessionNo);
			}

			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(fault.getMessage());
		} finally {
			
			if(authValidate == true)
			{
				starManResponse.output.authToken = securityHandler.getAuthenticationToken().getValue();
			}
			else
			{
				starManResponse.output.authToken = "";
			}
		}

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputSsn(starManResponse));

	}

}
