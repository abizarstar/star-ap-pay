package com.star.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.MaintanenceResponse;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.dao.ValidateGLDAO;
import com.star.linkage.ach.InvoiceInfo;
import com.star.linkage.ach.InvoiceInfoBrowse;
import com.star.linkage.ebs.BankStmtManOutput;
import com.star.linkage.ebs.BankStmtTrnBrowse;
import com.star.linkage.ebs.BankStmtTrnOutput;
import com.star.linkage.gl.JournalInfo;
import com.star.linkage.payment.PaymentManOutput;

@Path("/validate-bai-gl")
public class ValidateGLService {

	@POST
	@Secured
	@Path("/read")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getStatementTrn(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<BankStmtTrnBrowse> starBrowseResponse = new BrowseResponse<BankStmtTrnBrowse>();
		starBrowseResponse.setOutput(new BankStmtTrnBrowse());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String stmntDt = jsonObject.get("stmntDt").getAsString();
		String trnType = jsonObject.get("trnType").getAsString();
		String stmtType = jsonObject.get("stmtType").getAsString();

		ValidateGLDAO validateGLDAO = new ValidateGLDAO();

		validateGLDAO.readStmtTrn(starBrowseResponse, stmntDt, trnType, stmtType);

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblBnkTrn.size();

		if (starBrowseResponse.output.fldTblBnkTrn.size() < CommonConstants.DB_OUT_REC) {
			starBrowseResponse.output.eof = 1;
		} else {
			starBrowseResponse.output.eof = 0;
		}

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputStmt(starBrowseResponse));
	}

	@POST
	@Secured
	@Path("/read-pay")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getPaymentData(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<InvoiceInfoBrowse> starBrowseResponse = new BrowseResponse<InvoiceInfoBrowse>();
		starBrowseResponse.setOutput(new InvoiceInfoBrowse());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String propId = jsonObject.get("propId").getAsString();

		ValidateGLDAO validateGLDAO = new ValidateGLDAO();

		validateGLDAO.getPropInvoices(starBrowseResponse, propId);

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblInvoice.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputStmtPay(starBrowseResponse));
	}

	@POST
	@Secured
	@Path("/prs-pymnt-rcrd")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response processPaymentRecord(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		MaintanenceResponse<BankStmtManOutput> starManResponse = new MaintanenceResponse<BankStmtManOutput>();
		starManResponse.setOutput(new BankStmtManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		BrowseResponse<InvoiceInfoBrowse> starBrowseResponse = new BrowseResponse<InvoiceInfoBrowse>();
		starBrowseResponse.setOutput(new InvoiceInfoBrowse());

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		String post1 = jsonObject.get("post1").getAsString();
		String reqId = jsonObject.get("reqId").getAsString();

		Set<String> set = Stream.of(reqId.trim().split("\\s*,\\s*")).collect(Collectors.toSet());

		List<String> mainList = new ArrayList<String>();
		mainList.addAll(set);

		for (int j = 0; j < mainList.size(); j++) {

			List<InvoiceInfo> invVchrList = new ArrayList<InvoiceInfo>();

			ValidateGLDAO gldao = new ValidateGLDAO();
			invVchrList = gldao.getPropInvoices(starBrowseResponse, mainList.get(j));

			Collections.sort(invVchrList, new Comparator<InvoiceInfo>() {
				public int compare(InvoiceInfo s1, InvoiceInfo s2) {
					return s1.getVchrVenId().compareToIgnoreCase(s2.getVchrVenId());
				}
			});

			List<InvoiceInfo> invVchrs = gldao.getDistinctVendorInfo(invVchrList);

			MaintanenceResponse<PaymentManOutput> starManResponsePay = new MaintanenceResponse<PaymentManOutput>();
			starManResponsePay.setOutput(new PaymentManOutput());

			/* NOT REQUIRED AS WE ARE SHOWING UPDATED DISBURSEMENT IN PROPOSAL SCREEN */
			/* glInfoDAO = new GLInfoDAO(); */

			for (int i = 0; i < invVchrs.size(); i++) {
				
				JournalInfo info = gldao.getJournalReference(reqId, invVchrs.get(i).getVchrVenId());
				
				if(info == null )
				{
					if(invVchrs.get(i).getVchrAmt() > 0)
					{
						gldao.addLedgerEntryOnLoad(starManResponsePay, cmpyId, userId, invVchrs.get(i), reqId,
						invVchrs.get(i).getPaymentDt());
					}
				}
			}
		}

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputStmt(starManResponse));
	}

	@POST
	@Secured
	@Path("/prs-rcrd")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response processRecord(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		MaintanenceResponse<BankStmtManOutput> starManResponse = new MaintanenceResponse<BankStmtManOutput>();
		starManResponse.setOutput(new BankStmtManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		String post1 = jsonObject.get("post1").getAsString();
		String post2 = jsonObject.get("post2").getAsString();
		JsonArray baiArray = jsonObject.get("baiArray").getAsJsonArray();

		List<BankStmtTrnOutput> baiInfos = new ArrayList<BankStmtTrnOutput>();

		for (int i = 0; i < baiArray.size(); i++) {
			BankStmtTrnOutput bnkStmtTrn = new BankStmtTrnOutput();

			JsonObject lotObject = baiArray.get(i).getAsJsonObject();

			bnkStmtTrn.setId(lotObject.get("id").getAsString());
			bnkStmtTrn.setHdrId(lotObject.get("hdrId").getAsInt());
			bnkStmtTrn.setUpldId(lotObject.get("upldId").getAsInt());
			bnkStmtTrn.setTypCode(lotObject.get("typCode").getAsString());
			bnkStmtTrn.setTrnType(lotObject.get("trnTyp").getAsString());

			baiInfos.add(bnkStmtTrn);

		}

		ValidateGLDAO validateGLDAO = new ValidateGLDAO();

		validateGLDAO.processTrn(starManResponse, baiInfos, userId, cmpyId, post1, post2);

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputStmt(starManResponse));
	}

	private GenericEntity<?> getGenericBrowseOutputStmtPay(BrowseResponse<InvoiceInfoBrowse> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<InvoiceInfoBrowse>>(starBrowseResponse) {
		};
	}

	private GenericEntity<?> getGenericBrowseOutputStmt(BrowseResponse<BankStmtTrnBrowse> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<BankStmtTrnBrowse>>(starBrowseResponse) {
		};
	}

	private GenericEntity<?> getGenericMaintenanceOutputStmt(
			MaintanenceResponse<BankStmtManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<BankStmtManOutput>>(starMaintenanceResponse) {
		};
	}

}
