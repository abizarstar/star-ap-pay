package com.star.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.MaintanenceResponse;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.dao.VendorInfoDAO;
import com.star.dao.WorkflowDAO;
import com.star.linkage.vendor.VendorInput;
import com.star.linkage.wkf.WorkflowBrowseOutput;
import com.star.linkage.wkf.WorkflowManOutput;

@Path("/workflow")
public class WorkflowService {
	
	@POST
	@Secured
	@Path("/read")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getWorkflow(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data) throws Exception {
		BrowseResponse<WorkflowBrowseOutput> starBrowseResponse = new BrowseResponse<WorkflowBrowseOutput>();
		starBrowseResponse.setOutput(new WorkflowBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String wkfId = jsonObject.get("wkfId").getAsString();

		WorkflowDAO workflow = new WorkflowDAO();

		workflow.readWorkflow(starBrowseResponse, wkfId);
		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblWorkflow.size();
		return starResponseBuilder.getSuccessResponse(getBrowseOutputWorkflow(starBrowseResponse));
	}

	private GenericEntity<?> getBrowseOutputWorkflow(BrowseResponse<WorkflowBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<WorkflowBrowseOutput>>(starBrowseResponse) {
		};
	}

	@POST
	@Secured
	@Path("/delete")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response deleteWorkflow(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data) throws Exception {
		MaintanenceResponse<WorkflowManOutput> starManResponse = new MaintanenceResponse<WorkflowManOutput>();
		starManResponse.setOutput(new WorkflowManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		
		String wkfId = jsonObject.get("wkfId").getAsString();
		String wkfFlg = jsonObject.get("flg").getAsString();
		
		WorkflowDAO workflow = new WorkflowDAO();

		workflow.deleteWorkflow(starManResponse, wkfId, wkfFlg, userId);
		
		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starManResponse));
	}
	
	private GenericEntity<?> getGenericMaintenanceOutput(
			MaintanenceResponse<WorkflowManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<WorkflowManOutput>>(starMaintenanceResponse) {
		};
	}

}
