package com.star.service;

import java.math.BigDecimal;

import javax.ws.rs.HeaderParam;
import javax.xml.ws.soap.SOAPFaultException;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.invera.stratix.schema.common.datatypes.AuthenticationToken;
import com.invera.stratix.services.StratixFault;
import com.invera.stratix.services.arDist.ARDist;
import com.invera.stratix.services.arDist.AcctRecDistService;
import com.invera.stratix.services.cashRcpt.CashRct;
import com.invera.stratix.services.cashRcpt.CashReceiptService;
import com.invera.stratix.services.cashRcptSsn.CashRctSession;
import com.invera.stratix.services.cashRcptSsn.CashRcptSsnService;
import com.star.dao.BankStmtDAO;
import com.star.linkage.rcpt.RcptInfoInput;
import com.star.linkage.rcpt.RcptSsnInfoInput;
import com.ws.samples.ServiceHelper;
import com.ws.samples.handler.MsgSOAPHandler;
import com.ws.samples.handler.SecSOAPHandler;

public class CreateCashRecipt {

	public int createSession(SecSOAPHandler securityHandler, MsgSOAPHandler messageHandler, String data)
			throws Exception {
		
		int iSessionNo = 0 ;
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		
		String arBrh = jsonObject.get("brh").getAsString();
		String stxBnk = jsonObject.get("stxBnk").getAsString();
		String checkAmt = jsonObject.get("checkAmt").getAsString();
		
		RcptSsnInfoInput rcptInfoInput = new RcptSsnInfoInput();

		rcptInfoInput.setBankId(stxBnk);
		rcptInfoInput.setBranchId(arBrh);
		rcptInfoInput.setDepositAmount(new BigDecimal(checkAmt));
		/*rcptInfoInput.setExpectedChecks(5);*/

		try {

			CashRcptSsnService cashReceiptSessionService = ServiceHelper.createRcptSsnService(messageHandler,
					securityHandler);

			messageHandler.clearMessages();

			CashRctSession cashRctSession = new CashRctSession();

			/* cashRctSession.setSessionIdentity(value); */
			cashRctSession.setCashRctBranchId(rcptInfoInput.getBranchId());
			/* cashRctSession.setJournalDate(value); */
			cashRctSession.setBankId(rcptInfoInput.getBankId());
			/* cashRctSession.setDepositExchangeRate(value); */
			/*
			 * cashRctSession.setDepositDate(value); cashRctSession.setArCurrency1(value);
			 * cashRctSession.setArCrossExchangeRate1(value);
			 * cashRctSession.setArCurrency2(value);
			 * cashRctSession.setArCrossExchangeRate2(value);
			 * cashRctSession.setArCurrency3(value);
			 * cashRctSession.setArCrossExchangeRate3(value);
			 */
			cashRctSession.setControlDepositAmount(rcptInfoInput.getDepositAmount());
			cashRctSession.setControlNumberOfChecks(rcptInfoInput.getExpectedChecks());

			iSessionNo = cashReceiptSessionService.createCashRctSession(cashRctSession);

			System.out.println(iSessionNo);

			/*
			 * cashReceiptSessionService.modifyCashRctSession(cashRctSession);
			 * cashReceiptSessionService.deleteCashRctSession();
			 */

		}

		catch (SOAPFaultException | StratixFault fault) {

			System.out.println("ERROR");

			System.out.println(fault.getMessage());

			fault.printStackTrace();

		}
		
		return iSessionNo;

	}
	
	public String createRcpt(SecSOAPHandler securityHandler, MsgSOAPHandler messageHandler, String data) throws Exception {
		
		String cashRcpt = "";
		
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String cusId = jsonObject.get("cusId").getAsString();
		String ssnId = jsonObject.get("ssnId").getAsString();
		String checkNo = jsonObject.get("chkNo").getAsString();
		String checkDesc = jsonObject.get("desc").getAsString();
		String checkAmt = jsonObject.get("cusChkAmt").getAsString();
		
		String trnId = "";
		String hdrId = "";
		String upldId = "";
		String trnTypRef = "";
		
		if(jsonObject.has("trn"))
		{
			trnId = jsonObject.get("trn").getAsString();
		}
		
		if(jsonObject.has("hdr"))
		{
			hdrId = jsonObject.get("hdr").getAsString();
		}
		
		if(jsonObject.has("upld"))
		{
			upldId = jsonObject.get("upld").getAsString();
		}
		
		if(jsonObject.has("trnTypRef"))
		{
			trnTypRef = jsonObject.get("trnTypRef").getAsString();
		}
		
		
		
		RcptInfoInput rcptInfoInput = new RcptInfoInput();

		rcptInfoInput.setSsnId(Integer.parseInt(ssnId));
		rcptInfoInput.setCusId(cusId);
		rcptInfoInput.setCusChkAmt(new BigDecimal(checkAmt));
		rcptInfoInput.setCusChkNo(checkNo);
		rcptInfoInput.setCusRctDesc(checkDesc);
		
		CashReceiptService cashReceiptService = ServiceHelper.createRcptService(messageHandler, securityHandler);

		try {

			messageHandler.clearMessages();

			CashRct cashRct = new CashRct();

			cashRct.setSessionIdentity(rcptInfoInput.getSsnId());
			/* cashRct.setCashRctIdentity(rcptInfoInput.getid); */
			cashRct.setCustomerId(rcptInfoInput.getCusId());
			cashRct.setCustomerCheckAmount(rcptInfoInput.getCusChkAmt());
			cashRct.setCustomerCheckNumber(rcptInfoInput.getCusChkNo());
			cashRct.setCashRctDescription(rcptInfoInput.getCusRctDesc());

			cashRcpt = cashReceiptService.createCashRct(cashRct);
			
			/* UPDATE TRANSACTION AGAINST BANK STATEMENT */
			BankStmtDAO bankStmtDAO = new BankStmtDAO();
			
			bankStmtDAO.updateStmtTrn(ssnId + "-" + cashRcpt, trnId, hdrId, upldId, trnTypRef);
			
		}

		catch (SOAPFaultException | StratixFault fault) {

			System.out.println("ERROR");

			System.out.println(fault.getMessage());
			
			cashReceiptService.deleteCashRct(cashRcpt);

			fault.printStackTrace();

		}
		
		return cashRcpt;

	}
	
	public void createARDistribution(SecSOAPHandler securityHandler, MsgSOAPHandler messageHandler, String data) throws Exception {

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String invNo = jsonObject.get("invNo").getAsString();
		String checkAmt = jsonObject.get("checkAmt").getAsString();
		String rcptNo = jsonObject.get("rcptNo").getAsString();
		String discFlg = jsonObject.get("discFlg").getAsString();
		String discAmt = jsonObject.get("discAmt").getAsString();
		
		double totAmount = 0;
		
		try {

			AcctRecDistService arDistributionService = ServiceHelper.createARDist(messageHandler,
					securityHandler);

			messageHandler.clearMessages();

			ARDist arDist = new ARDist();

			arDist.setInvoiceNumber(invNo);
			/*
			 * arDist.setWriteOffAmount(new BigDecimal(100));
			 * arDist.setDiscountTakenAmount(new BigDecimal(20));
			 */
			
			if(discFlg.equals("Y"))
			{
				arDist.setDiscountTakenAmount(new BigDecimal(discAmt));
				
				totAmount = Double.parseDouble(checkAmt);
			}
			else
			{
				totAmount = Double.parseDouble(checkAmt);
			}
			
			arDist.setPaymentAmount(new BigDecimal(checkAmt));

			int iArDistRef = arDistributionService.createARDist(rcptNo, arDist);

		}

		catch (SOAPFaultException | StratixFault fault) {

			System.out.println("ERROR");

			System.out.println(fault.getMessage());

			fault.printStackTrace();

		}

	}
	
	
	public SecSOAPHandler createSecurityHandlers(@HeaderParam("user-id") String userId, @HeaderParam("auth-token") String authToken,
			@HeaderParam("app-host") String appHost, @HeaderParam("app-port") String appPort,
			@HeaderParam("cmpy-id") String cmpyId) {
		ServiceHelper.setWpaEndpointProtocol("http");

		ServiceHelper.setStxEndpointHostAndPort(appHost, Integer.parseInt(appPort));

		AuthenticationToken authenticationToken = new AuthenticationToken();

		authenticationToken.setUsername(userId);
		authenticationToken.setValue(authToken);

		SecSOAPHandler securityHandler = new SecSOAPHandler(authenticationToken);
		
		return securityHandler;
	}
	
	public MsgSOAPHandler createMessageHandlers(@HeaderParam("user-id") String userId, @HeaderParam("auth-token") String authToken,
			@HeaderParam("app-host") String appHost, @HeaderParam("app-port") String appPort,
			@HeaderParam("cmpy-id") String cmpyId) {
		ServiceHelper.setWpaEndpointProtocol("http");

		ServiceHelper.setStxEndpointHostAndPort(appHost, Integer.parseInt(appPort));

		MsgSOAPHandler messageHandler = new MsgSOAPHandler();
		
		return messageHandler;
	}



}
