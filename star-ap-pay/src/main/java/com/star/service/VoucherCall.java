
package com.star.service;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.soap.SOAPFaultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.invera.stratix.schema.common.datatypes.AuthenticationToken;
import com.invera.stratix.services.CostReconDistService;
import com.invera.stratix.services.CreateVoucherOutput;
import com.invera.stratix.services.GLDist;
import com.invera.stratix.services.GLDistributionService;
import com.invera.stratix.services.IRCSelect;
import com.invera.stratix.services.SelectMatchingIRC;
import com.invera.stratix.services.StratixFault;
import com.invera.stratix.services.Voucher;
import com.invera.stratix.services.VoucherService;
import com.star.common.CommonConstants;
import com.star.common.CommonFunctions;
import com.star.common.HandlerService;
import com.star.common.MaintanenceResponse;
import com.star.common.SendExternalReq;
import com.star.common.StarResponseBuilder;
import com.star.dao.CstmDocInfoDAO;
import com.star.dao.CstmVchrInfoDAO;
import com.star.linkage.auth.DeleteVoucherInput;
import com.star.linkage.cstmdocinfo.CstmDocInfoManOutput;
import com.star.linkage.cstmdocinfo.VchrInfo;
import com.star.linkage.cstmvchrinfo.CstmVchrInfoManOutput;
import com.ws.samples.ServiceHelper;
import com.ws.samples.handler.MsgSOAPHandler;
import com.ws.samples.handler.SecSOAPHandler;

@Path("/vour")
public class VoucherCall {

	private static Logger logger = LoggerFactory.getLogger(VoucherCall.class);

	@POST
	@Path("/crtvour")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response createVoucher(String data, @HeaderParam("user-id") String userId,
			@HeaderParam("auth-token") String authToken, @HeaderParam("app-host") String appHost,
			@HeaderParam("app-port") String appPort, @HeaderParam("cmpy-id") String cmpyId) throws Exception {

		String output = "";
		SecSOAPHandler securityHandler = null;
		MsgSOAPHandler messageHandler = null;
		boolean authValidate = true;

		MaintanenceResponse<CstmVchrInfoManOutput> starManResponse = new MaintanenceResponse<CstmVchrInfoManOutput>();
		starManResponse.setOutput(new CstmVchrInfoManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		CstmVchrInfoDAO cstmVchrInfoDAO = new CstmVchrInfoDAO();

		VchrInfo detailsInput = new VchrInfo();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		CommonFunctions objCom = new CommonFunctions();

		JsonArray glArray = jsonObject.get("glData").getAsJsonArray();
		JsonArray costReconArray = jsonObject.get("costRecon").getAsJsonArray();

		detailsInput.setCmpyId(jsonObject.get("vchrCmpyId").getAsString());
		detailsInput.setVchrPfx(jsonObject.get("vchrPfx").getAsString());
		detailsInput.setVchrCtlNo(jsonObject.get("vchrCtlNo").getAsInt());
		detailsInput.setVchrInvNo(jsonObject.get("vchrInvNo").getAsString());
		detailsInput.setVchrVenId(jsonObject.get("vchrVenId").getAsString());
		detailsInput.setVchrVenNm(jsonObject.get("vchrVenNm").getAsString());
		detailsInput.setVchrBrh(jsonObject.get("vchrBrh").getAsString());
		detailsInput.setVchrAmt(Double.parseDouble(jsonObject.get("vchrAmt").getAsString()));
		detailsInput.setVchrExtRef(jsonObject.get("vchrExtRef").getAsString());
		detailsInput.setVchrInvDtStr(objCom.formatDateWithoutTimeVou(jsonObject.get("vchrInvDt").getAsString()));
		detailsInput.setVchrPayTerm(jsonObject.get("vchrPayTerm").getAsString());
		detailsInput.setVchrCry(jsonObject.get("vchrCry").getAsString());
		detailsInput.setVchrCat(jsonObject.get("vchrCat").getAsString());
		detailsInput.setVoyageNo(jsonObject.get("voyage").getAsString());
		detailsInput.setVchrRmk(jsonObject.get("vchrRmk").getAsString());

		if (jsonObject.get("discDt").getAsString().length() > 0) {
			detailsInput.setVchrDiscDtStr(objCom.formatDateWithoutTimeVou(jsonObject.get("discDt").getAsString()));
		}

		if (jsonObject.get("vchrDueDt").getAsString().length() > 0) {
			detailsInput.setVchrDueDtStr(objCom.formatDateWithoutTimeVou(jsonObject.get("vchrDueDt").getAsString()));
		}

		if (jsonObject.get("discAmt").getAsString().length() > 0) {
			detailsInput.setDiscAmt(Double.parseDouble(jsonObject.get("discAmt").getAsString()));
		}

		/* SET PO NO ITEM IF AVAILABLE */
		if (jsonObject.get("poNo").getAsString().length() > 0) {
			String poStr = jsonObject.get("poNo").getAsString();

			String[] arrPoStr = poStr.split("-");

			detailsInput.setPoPfx("PO");
			detailsInput.setPoNo(arrPoStr[0]);

			if (arrPoStr.length > 1) {
				detailsInput.setPoItm(arrPoStr[1]);
			}

		}

		String invDt = detailsInput.getVchrInvDtStr();

		XMLGregorianCalendar xmlInvDt = DatatypeFactory.newInstance().newXMLGregorianCalendar(invDt);
		System.out.println(xmlInvDt);

		String dueDt = "";
		XMLGregorianCalendar xmlDueDt = null;
		if (detailsInput.getVchrDueDtStr() != null && detailsInput.getVchrDueDtStr().length() > 0) {
			dueDt = detailsInput.getVchrDueDtStr();
			xmlDueDt = DatatypeFactory.newInstance().newXMLGregorianCalendar(dueDt);
			System.out.println(xmlDueDt);
		}

		String discountDt = "";
		XMLGregorianCalendar xmlDiscountDt = null;

		if (detailsInput.getVchrDiscDtStr() != null && detailsInput.getVchrDiscDtStr().length() > 0) {
			discountDt = detailsInput.getVchrDiscDtStr();
			xmlDiscountDt = DatatypeFactory.newInstance().newXMLGregorianCalendar(discountDt);
			System.out.println(xmlDiscountDt);
		}

		CreateCashRecipt cashReciptService = new CreateCashRecipt();
		securityHandler = cashReciptService.createSecurityHandlers(userId, authToken, appHost, appPort, cmpyId);
		messageHandler = cashReciptService.createMessageHandlers(userId, authToken, appHost, appPort, cmpyId);

		try {

			Voucher voucher = new Voucher();

			voucher.setCompanyId(detailsInput.getCmpyId());
			voucher.setVoucherPrefix(detailsInput.getVchrPfx());
			// voucher.setVoucherNumber(voucherNumber);
			// voucher.setSessionId(sessionId);
			/* voucher.setEntryDate(xmlInvDt); */
			voucher.setVendorId(detailsInput.getVchrVenId());
			voucher.setVendorInvoiceNumber(detailsInput.getVchrInvNo());

			if (detailsInput.getVchrExtRef() != null && detailsInput.getVchrExtRef().length() > 0) {
				voucher.setExtenralReference(detailsInput.getVchrExtRef());
			}
			// voucher.setMaterialTransferNumber(materialTransferNumber);
			voucher.setVendorInvoiceDate(xmlInvDt);

			if (detailsInput.getPoPfx() != null && detailsInput.getPoPfx().length() > 0) {
				voucher.setPurchaseOrderPrefix(detailsInput.getPoPfx());
			}

			if (detailsInput.getPoNo() != null && detailsInput.getPoNo().length() > 0) {
				voucher.setPurchaseOrderNumber(Integer.parseInt(detailsInput.getPoNo()));
			}

			if (detailsInput.getPoItm() != null && detailsInput.getPoItm().length() > 0) {
				voucher.setPurchaseOrderItem(Integer.parseInt(detailsInput.getPoItm()));
			}

			voucher.setVoucherBranch(detailsInput.getVchrBrh());
			// voucher.setPretaxVoucherAmount(pretaxVoucherAmount);
			voucher.setVoucherAmount(new BigDecimal(String.valueOf(detailsInput.getVchrAmt())));
			// voucher.setDiscountableAmount(discountableAmount);
			// voucher.setVoucherDescription(voucherDescription);
			voucher.setVoucherCurrency(detailsInput.getVchrCry());
			// voucher.setExchangeRate(exchangeRate);
			// voucher.setPaymentTerm(paymentTerm);
			// voucher.setDiscountTerm(discountTerm);
			voucher.setDueDate(xmlDueDt);

			if (detailsInput.getVchrDiscDtStr() != null && detailsInput.getVchrDiscDtStr().length() > 0) {
				voucher.setDiscountDate(xmlDiscountDt);
			}

			if (detailsInput.getDiscAmt() > 0) {
				voucher.setDiscountAmount(new BigDecimal(detailsInput.getDiscAmt()));
			}

			if (detailsInput.getVoyageNo().length() > 0) {
				voucher.setVoyageNumber(detailsInput.getVoyageNo());
			}

			if (detailsInput.getVchrRmk().length() > 0) {
				voucher.setCheckItemRemarks(detailsInput.getVchrRmk());
			}

			// voucher.setPaymentType(paymentType);
			// voucher.setVoucherCrossRefNo(voucherCrossRefNo);
			// voucher.setAuthorizationReference(authorizationReference);
			voucher.setVoucherCategory(detailsInput.getVchrCat());
			// voucher.setServiceFulfillmentDate(serviceFulfillmentDate);
			// voucher.setPrepaymentEligibility(prepaymentEligibility);
			// voucher.setTransactionStatusAction("A");
			// voucher.setTransactionReason(transactionReason);
			// voucher.setTransactionStatus("OK");
			// voucher.setTransactionStatusRemarks(transactionStatusRemarks);
			/* voucher.setPaymentStatusAction(""); */
			// voucher.setPaymentReason(paymentReason);
			/* voucher.setPaymentStatus(""); */
			// voucher.setPaymentStatusRemarks(paymentStatusRemarks);

			VoucherService voucherService = ServiceHelper.createVouchService(messageHandler, securityHandler);
			// QdsService qdsService = ServiceHelper.createQdsService(messageHandler,
			// securityHandler);

			messageHandler.clearMessages();

			CreateVoucherOutput createVoucherOutput = new CreateVoucherOutput();

			createVoucherOutput = voucherService.createVoucher(voucher);

			starManResponse.output.vchrPfx = createVoucherOutput.getVoucherPrefix();
			starManResponse.output.vchrNo = createVoucherOutput.getVoucherNumber();
			starManResponse.output.ssnId = createVoucherOutput.getSessionId();

			System.err.println("Vouhcer - " + starManResponse.output.vchrPfx + "-" + starManResponse.output.vchrNo);

			detailsInput
					.setVchrNo(createVoucherOutput.getVoucherPrefix() + "-" + createVoucherOutput.getVoucherNumber());

			double balanceTotal = 0;
			
			SendExternalReq externalReq = new SendExternalReq();
			
			if (costReconArray.size() > 0) {

				cstmVchrInfoDAO.deleteReconData(costReconArray);

				for (int i = 0; i < costReconArray.size(); i++) {
					
					String PRS_MD = "1";
					JsonObject costReconVal = costReconArray.get(i).getAsJsonObject();

					int crcnNo = costReconVal.get("crcnNo").getAsInt();
					double balAmt = costReconVal.get("balAmt").getAsDouble();

					JsonObject object = new JsonObject();
					object.addProperty("prsMd", PRS_MD);
					object.addProperty("refPfx", createVoucherOutput.getVoucherPrefix());
					object.addProperty("refNo", createVoucherOutput.getVoucherNumber());
					object.addProperty("crcnPfx", "IR");
					object.addProperty("crcnNo", crcnNo);
					object.addProperty("dsamt", balAmt);

					String jsonInputString = object.toString();

					externalReq.createReconTransaction(appHost, appPort, authToken, "1", "irzcri", "WMN", userId,
							cmpyId, "en", "USA", costReconArray, jsonInputString, starManResponse);
					
					balanceTotal = balanceTotal + balAmt;
				}
				
				/*
				 * addIRCInformation(messageHandler, securityHandler,
				 * createVoucherOutput.getVoucherPrefix(),
				 * createVoucherOutput.getVoucherNumber(), cmpyId, costReconArray,
				 * voucher.getVendorId());
				 */
			}

			if (glArray.size() > 0) {
				addVoucherGLInformation(messageHandler, securityHandler, createVoucherOutput.getVoucherPrefix(),
						createVoucherOutput.getVoucherNumber(), cmpyId, glArray);
			}
			
			for (int i = 0; i < glArray.size(); i++) {
				
				JsonObject jsonObjectGL = glArray.get(i).getAsJsonObject();
				
				balanceTotal = balanceTotal + jsonObjectGL.get("drAmt").getAsDouble();
				balanceTotal = balanceTotal - jsonObjectGL.get("crAmt").getAsDouble();
				
			}
			
			
			if(balanceTotal == detailsInput.getVchrAmt() && glArray.size() == 0)
			{
				JsonObject object = new JsonObject();
				object.addProperty("prsMd", "1");
				object.addProperty("allBrhFlg", "0");
				object.addProperty("frstTmFlg", "1");
				object.addProperty("refPfx", createVoucherOutput.getVoucherPrefix());
				object.addProperty("refNo", createVoucherOutput.getVoucherNumber());
				object.addProperty("refItm", "0");

				String jsonInputString = object.toString();
				
				externalReq.createReconTransaction(appHost, appPort, authToken, "1", "apzvcu", "WMN", userId,
						cmpyId, "en", "USA", costReconArray, jsonInputString, starManResponse);
			}

			CstmDocInfoDAO cstmDocInfoDAO = new CstmDocInfoDAO();

			MaintanenceResponse<CstmDocInfoManOutput> starMaintenanceResponse = new MaintanenceResponse<CstmDocInfoManOutput>();

			starMaintenanceResponse.setOutput(new CstmDocInfoManOutput());

			cstmDocInfoDAO.updStxReference(starMaintenanceResponse, detailsInput);

		}

		catch (SOAPFaultException | StratixFault fault) {

			if (starManResponse.output.vchrPfx != null && starManResponse.output.vchrPfx.length() > 0) {

				deleteVoucher(messageHandler, securityHandler, starManResponse.output.vchrPfx,
						starManResponse.output.vchrNo, cmpyId);

				System.err.println("Error Vouhcer Deleted");

				/* DELETE RECON DATA IF AVAILABLE */
				if (costReconArray.size() > 0) {
					cstmVchrInfoDAO.deleteReconData(costReconArray);
				}
			}

			cstmVchrInfoDAO.getErrorMsg(userId, starManResponse);

			if (fault.getLocalizedMessage().contains("3254 - STRATIX session not found")) {
				authValidate = false;
			} else if (fault.getLocalizedMessage().contains("nvalid Authentication Token.")) {
				authValidate = false;
			}

			/* starManResponse.output.messages.add(CommonConstants.VCHR_ERR); */

			starManResponse.output.rtnSts = 1;

			System.out.println(fault.getMessage());

			starManResponse.output.messages.add(fault.getLocalizedMessage());

		} 
		catch (Exception exception)
		{
			if (starManResponse.output.vchrPfx != null && starManResponse.output.vchrPfx.length() > 0) {

				deleteVoucher(messageHandler, securityHandler, starManResponse.output.vchrPfx,
						starManResponse.output.vchrNo, cmpyId);

				System.err.println("Error Vouhcer Deleted");

				/* DELETE RECON DATA IF AVAILABLE */
				if (costReconArray.size() > 0) {
					cstmVchrInfoDAO.deleteReconData(costReconArray);
				}
			}
			
			starManResponse.output.rtnSts = 1;
			
		}
		finally {

			if (authValidate == true) {
				starManResponse.output.authToken = securityHandler.getAuthenticationToken().getValue();
			} else {
				starManResponse.output.authToken = "";
			}
		}

		System.out.println(output);

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputUpdate(starManResponse));

	}

	@POST
	@Path("/delvour")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response deleteVoucher(String data, @HeaderParam("user-id") String userId,
			@HeaderParam("auth-token") String authToken, @HeaderParam("app-host") String appHost,
			@HeaderParam("app-port") String appPort, @HeaderParam("cmpy-id") String cmpyId) throws Exception {

		HandlerService handlerService = new HandlerService();

		MaintanenceResponse<CstmVchrInfoManOutput> starManResponse = new MaintanenceResponse<CstmVchrInfoManOutput>();
		starManResponse.setOutput(new CstmVchrInfoManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		try {

			JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
			String voucherPrefix = jsonObject.get("vchrPfx").getAsString();
			String voucherNumber = jsonObject.get("vchrNo").getAsString();

			MsgSOAPHandler messageHandler = handlerService.createMessageHandlers(userId, authToken, appHost, appPort,
					cmpyId);
			SecSOAPHandler securityHandler = handlerService.createSecurityHandlers(userId, authToken, appHost, appPort,
					cmpyId);

			try {

				VoucherService voucherService = ServiceHelper.createVouchService(messageHandler, securityHandler);

				messageHandler.clearMessages();

				voucherService.deleteVoucher(cmpyId, voucherPrefix, Integer.parseInt(voucherNumber));

			}

			catch (SOAPFaultException | StratixFault fault) {

				System.out.println("ERROR");

				System.out.println(fault.getMessage());

				fault.printStackTrace();

				starManResponse.output.rtnSts = 1;

			} finally {
				starManResponse.output.authToken = securityHandler.getAuthenticationToken().getValue();
			}

			return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputUpdate(starManResponse));

		} catch (Exception e) {
			throw e;
		}

	}

	@POST
	@Path("/irc")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response updateIRC(String data, @HeaderParam("user-id") String userId,
			@HeaderParam("auth-token") String authToken, @HeaderParam("app-host") String appHost,
			@HeaderParam("app-port") String appPort, @HeaderParam("cmpy-id") String cmpyId) throws Exception {

		String output = "";
		try {

			DeleteVoucherInput deleteVoucherInput = new Gson().fromJson(data, DeleteVoucherInput.class);

			String host = appHost;
			String port = appPort;
			String companyId = cmpyId;
			String voucherPrefix = "VR";
			String voucherNumber = "8734";

			ServiceHelper.setWpaEndpointProtocol("http");

			ServiceHelper.setStxEndpointHostAndPort(host, Integer.parseInt(port));

			MsgSOAPHandler messageHandler = new MsgSOAPHandler();
			AuthenticationToken authenticationToken = new AuthenticationToken();

			System.out.println("TOKEN---> " + authenticationToken.getValue());

			authenticationToken.setUsername(userId);
			authenticationToken.setValue(authToken);

			SecSOAPHandler securityHandler = new SecSOAPHandler(authenticationToken);

			System.out.println("TOKEN---> " + authenticationToken.getValue());
			try {

				SelectMatchingIRC irc = new SelectMatchingIRC();

				irc.setCompanyId("SQ1");
				irc.setVoucherNumber(Integer.parseInt(voucherNumber));
				irc.setVoucherPrefix(voucherPrefix);

				List<IRCSelect> ircSelects = new ArrayList<IRCSelect>();

				IRCSelect ircSelect = new IRCSelect();

				/*
				 * ircSelect.setPurchaseOrderNumber(1478); ircSelect.setPurchaseOrderItem(1);
				 */

				ircSelect.setVendorId("1040");
				ircSelect.setMillOrderNumber("RC-863-1");

				ircSelects.add(ircSelect);

				CostReconDistService costReconService = ServiceHelper.createCostReconService(messageHandler,
						securityHandler);

				messageHandler.clearMessages();

				costReconService.selectMatchingIRC("SQ1", voucherPrefix, Integer.parseInt(voucherNumber), ircSelects);

				costReconService.applyIRCDist("SQ1", voucherPrefix, Integer.parseInt(voucherNumber));

			}

			catch (SOAPFaultException fault) {

				System.out.println("ERROR");

				System.out.println(fault.getMessage());

				fault.printStackTrace();

			}

			System.out.println(output);

			return Response.status(200).entity(output).build();

		} catch (Exception e) {
			throw e;
		}

	}

	@POST
	@Path("gl-dist")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response updateGL(String data, @HeaderParam("user-id") String userId,
			@HeaderParam("auth-token") String authToken, @HeaderParam("app-host") String appHost,
			@HeaderParam("app-port") String appPort, @HeaderParam("cmpy-id") String cmpyId) throws Exception {

		String output = "";
		HandlerService handlerService = new HandlerService();

		try {

			String voucherPrefix = "VR";
			String voucherNumber = "8734";

			MsgSOAPHandler messageHandler = handlerService.createMessageHandlers(userId, authToken, appHost, appPort,
					cmpyId);
			SecSOAPHandler securityHandler = handlerService.createSecurityHandlers(userId, authToken, appHost, appPort,
					cmpyId);
			try {

				GLDist glDist = new GLDist();

				glDist.setBasicGLAccount("6160");
				glDist.setSubaccount("CEN,MID,BIR,00");

				glDist.setCreditAmount(new BigDecimal(100));

				List<GLDist> glDistsList = new ArrayList<GLDist>();

				glDistsList.add(glDist);

				GLDistributionService glDistService = ServiceHelper.createVouGLService(messageHandler, securityHandler);

				messageHandler.clearMessages();

				glDistService.createGLDist("SQ1", voucherPrefix, Integer.parseInt(voucherNumber), glDistsList);

				/*
				 * costReconService.applyIRCDist(voucherPrefix,
				 * Integer.parseInt(voucherNumber));
				 * 
				 * costReconService.selectMatchingIRC(voucherPrefix,
				 * Integer.parseInt(voucherNumber), ircSelects);
				 */

			}

			catch (SOAPFaultException fault) {

				System.out.println("ERROR");

				System.out.println(fault.getMessage());

				fault.printStackTrace();

			}

			System.out.println(output);

			return Response.status(200).entity(output).build();

		} catch (Exception e) {
			throw e;
		}

	}

	public void addVoucherGLInformation(MsgSOAPHandler messageHandler, SecSOAPHandler securityHandler, String vchrPfx,
			int vchrNo, String cmpyId, JsonArray glList) throws StratixFault, MalformedURLException {

		List<GLDist> glDistsList = new ArrayList<GLDist>();

		for (int i = 0; i < glList.size(); i++) {
			JsonObject jsonObject = glList.get(i).getAsJsonObject();

			GLDist glDist = new GLDist();

			glDist.setBasicGLAccount(jsonObject.get("glAcc").getAsString());

			String subAcctValue = jsonObject.get("glSubAcc").getAsString();

			System.out.println(subAcctValue.trim().length());

			if (subAcctValue.trim().length() > 0) {

				subAcctValue = subAcctValue.replace("-", ",");

				glDist.setSubaccount(subAcctValue);
			}

			if (Double.parseDouble(jsonObject.get("drAmt").getAsString()) > 0) {
				glDist.setDebitAmount(new BigDecimal(jsonObject.get("drAmt").getAsString()));
			}

			if (Double.parseDouble(jsonObject.get("crAmt").getAsString()) > 0) {
				glDist.setCreditAmount(new BigDecimal(jsonObject.get("crAmt").getAsString()));
			}

			if (jsonObject.get("remark").getAsString().trim().length() > 0) {
				glDist.setDistributionRemarks(jsonObject.get("remark").getAsString());
			}

			glDistsList.add(glDist);
		}

		GLDistributionService glDistService = ServiceHelper.createVouGLService(messageHandler, securityHandler);

		messageHandler.clearMessages();

		glDistService.createGLDist(cmpyId, vchrPfx, vchrNo, glDistsList);
	}

	public void addIRCInformation(MsgSOAPHandler messageHandler, SecSOAPHandler securityHandler, String vchrPfx,
			int vchrNo, String cmpyId, JsonArray costRecon, String venId) throws StratixFault, MalformedURLException {

		List<IRCSelect> ircSelects = new ArrayList<IRCSelect>();

		for (int i = 0; i < costRecon.size(); i++) {

			ircSelects = new ArrayList<IRCSelect>();

			JsonObject jsonObject = costRecon.get(i).getAsJsonObject();

			String trsNo = jsonObject.get("trsNo").getAsString();
			String poNo = jsonObject.get("poNo").getAsString();
			String costNo = jsonObject.get("costNo").getAsString();
			venId = jsonObject.get("reconVenId").getAsString();

			System.err.println("Vouhcer - " + venId + "-" + trsNo);

			int iLstIndex = trsNo.lastIndexOf("-");
			trsNo = trsNo.substring(0, iLstIndex);

			String[] poStr = poNo.split("-");

			IRCSelect ircSelect = new IRCSelect();

			ircSelect.setVendorId(venId);

			if (poNo.length() > 0) {
				ircSelect.setPurchaseOrderNumber(Integer.parseInt(poStr[1]));
				ircSelect.setPurchaseOrderItem(Integer.parseInt(poStr[2]));
			}

			if (trsNo.contains("RC")) {
				ircSelect.setMillOrderNumber(trsNo);
			} else if (trsNo.contains("TR")) {
				String[] transport = trsNo.split("-");
				ircSelect.setTransportNumber(Integer.parseInt(transport[1]));
			} else if (trsNo.contains("IP")) {
				String[] job = trsNo.split("-");
				ircSelect.setServiceOrderNumber(Integer.parseInt(job[1]));
			} else if (trsNo.contains("SO")) {
				String[] salesOrder = trsNo.split("-");
				ircSelect.setSalesOrderNumber(Integer.parseInt(salesOrder[1]));
				ircSelect.setSalesOrderItem(Integer.parseInt(salesOrder[2]));
			}

			ircSelect.setChargeNumber(Integer.parseInt(costNo));

			ircSelects.add(ircSelect);

			CostReconDistService costReconService = ServiceHelper.createCostReconService(messageHandler,
					securityHandler);

			messageHandler.clearMessages();

			System.err.println("Vouhcer - " + cmpyId + "-" + vchrPfx + "-" + vchrNo);

			costReconService.selectMatchingIRC(cmpyId, vchrPfx, vchrNo, ircSelects);

			System.err.println("Vouhcer - " + "Applied");

			costReconService.applyIRCDist(cmpyId, vchrPfx, vchrNo);

			System.err.println("Vouhcer - " + "Applied");

		}

	}

	public void deleteVoucher(MsgSOAPHandler messageHandler, SecSOAPHandler securityHandler, String vchrPfx, int vchrNo,
			String cmpyId) {

		try {
			VoucherService voucherService = ServiceHelper.createVouchService(messageHandler, securityHandler);

			messageHandler.clearMessages();

			voucherService.deleteVoucher(cmpyId, vchrPfx, vchrNo);
		} catch (SOAPFaultException | StratixFault fault) {
			System.out.println("ERROR");

			System.out.println(fault.getMessage());

			fault.printStackTrace();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private static String printMessages(MsgSOAPHandler messageHandler) {

		List<String> messages = messageHandler.getMessageList();

		String errMsg = "";

		for (String message : messages) {
			if (message != null) {
				System.out.println(message);

				errMsg = errMsg + message + "<br>";
			}
		}

		return errMsg;

	}

	private GenericEntity<?> getGenericMaintenanceOutputUpdate(
			MaintanenceResponse<CstmVchrInfoManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<CstmVchrInfoManOutput>>(starMaintenanceResponse) {
		};
	}
}
