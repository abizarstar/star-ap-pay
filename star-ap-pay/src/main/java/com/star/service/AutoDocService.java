package com.star.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import com.star.common.CommonConstants;
import com.star.common.MaintanenceResponse;
import com.star.common.ManageDirectory;
import com.star.common.ParseInvoice;
import com.star.common.Secured;
import com.star.common.ValidateValue;
import com.star.common.abbyy.PerformOCR;
import com.star.dao.AutoDocDAO;
import com.star.dao.CstmDocInfoDAO;
import com.star.dao.CstmVchrInfoDAO;
import com.star.linkage.automate.PullDirecInfo;
import com.star.linkage.cstmdocinfo.CstmDocInfoInput;
import com.star.linkage.cstmdocinfo.CstmDocInfoManOutput;
import com.star.linkage.cstmvchrinfo.CstmVchrInfoInput;
import com.star.linkage.cstmvchrinfo.CstmVchrInfoManOutput;

@Path("/doc-service")
public class AutoDocService {

	@POST
	@Secured
	@Path("/get-doc")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getDocument(@HeaderParam("user-id") String userId, String data) throws Exception {

		AutoDocDAO autoDocDAO = new AutoDocDAO();

		List<PullDirecInfo> listDirectory = autoDocDAO.getDirectoryList();

		for (int i = 0; i < listDirectory.size(); i++) {
			if (listDirectory.get(i).getDirType().equals("LC")) {
				File folderPath = new File(listDirectory.get(i).getRootPath());

				listFilesForFolder(listDirectory.get(i).getRootPath(), folderPath);
			}
		}

		return Response.ok("Document Uploaded").build();
	}

	public void listFilesForFolder(String rootPath, final File folder) {
		for (final File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				// listFilesForFolder(rootPath, fileEntry);
			} else {
				if (fileEntry.getName()
						.substring(fileEntry.getName().lastIndexOf(".") + 1, fileEntry.getName().length())
						.equalsIgnoreCase("PDF")) {
					System.out.println(fileEntry.getName());

					moveFile(rootPath, fileEntry.getName(), fileEntry.getName());

				}
			}
		}
	}

	public void moveFile(String rootLoc, String srcFl, String dest) {
		srcFl = rootLoc + "/" + srcFl;

		String dirLoc = rootLoc + "/" + "arch-fol/";

		dest = rootLoc + "/" + "arch-fol/" + dest;

		File directory = new File(dirLoc);
		if (!directory.exists()) {
			directory.mkdir();
			// If you require it to make the entire directory path including parents,
			// use directory.mkdirs(); here instead.
		}

		java.nio.file.Path source = Paths.get(srcFl);
		java.nio.file.Path target = Paths.get(dest);

		try {

			Files.move(source, target);

		} catch (IOException e) {

			System.out.println(e.getMessage());
		}
	}

	public void processDocument(String cmpyId, String usrId, String flNm) {
		int fileCount = 0;

		ValidateValue validateValue = new ValidateValue();
		ManageDirectory directory = new ManageDirectory();
		/* Calling Doc Sequence to generate unique Doc Name */
		CstmDocInfoDAO cstmDocInfoDAO = new CstmDocInfoDAO();

		try {
			int ctlNo = cstmDocInfoDAO.getCtlNo();

			String strCtlNo = String.valueOf(ctlNo);

			String fileName = "0000000000".substring(strCtlNo.length()) + strCtlNo;

			String docNmIntrnl = "upload" + "-" + fileName;

			String fileExtn = validateValue.getFileExtension(flNm);

			File file = new File(flNm);

			/* CREATE FILE BASED ON THE INPUT */
			directory.createFile(docNmIntrnl, fileExtn, new FileInputStream(file));

			String imgFlPath = docNmIntrnl + "." + fileExtn;
			String txtFlPath = docNmIntrnl + ".txt";

			PerformOCR performOCR = new PerformOCR();

			String[] inputArray = new String[3];

			inputArray[0] = "recognize";
			inputArray[1] = CommonConstants.ROOT_DIR + imgFlPath;
			inputArray[2] = CommonConstants.ROOT_DIR + txtFlPath;

			performOCR.performOcrOperation(inputArray);

			MaintanenceResponse<CstmDocInfoManOutput> starMaintenanceDocResponse = new MaintanenceResponse<CstmDocInfoManOutput>();

			starMaintenanceDocResponse.setOutput(new CstmDocInfoManOutput());

			CstmDocInfoInput cstmDocInfoInput = new CstmDocInfoInput();

			cstmDocInfoInput.setCtlNo(ctlNo);
			// TODO
			cstmDocInfoInput.setGatCtlNo(1);
			cstmDocInfoInput.setFlPdf(imgFlPath);
			cstmDocInfoInput.setFlPdfName(flNm);
			cstmDocInfoInput.setFlTxt(txtFlPath);
			cstmDocInfoInput.setSrcFlg("S");
			cstmDocInfoInput.setIsPrs(false);
			cstmDocInfoInput.setUpldBy("star");

			cstmDocInfoDAO.addCstmDocument(starMaintenanceDocResponse, cstmDocInfoInput);

			MaintanenceResponse<CstmVchrInfoManOutput> starMaintenanceResponse = new MaintanenceResponse<CstmVchrInfoManOutput>();

			starMaintenanceResponse.setOutput(new CstmVchrInfoManOutput());

			CstmVchrInfoDAO cstmVchrInfoDAO = new CstmVchrInfoDAO();

			List<CstmVchrInfoInput> cstmVchrInfo = new ArrayList<CstmVchrInfoInput>();

			ParseInvoice parseInvoice = new ParseInvoice();

			parseInvoice.parseInvoiceData(cmpyId, usrId, txtFlPath, cstmVchrInfo, ctlNo, cstmVchrInfoDAO);

			WkfMapService mapService = new WkfMapService();

			for(int i =0 ; i < cstmVchrInfo.size(); i++)
			{
				/* VALIDATE WORK FLOW SERVICE */
				mapService.validateVouWkf("SQ1", "star", cstmVchrInfo.get(i));

				cstmVchrInfoDAO.addCstmVoucher(starMaintenanceResponse, cstmVchrInfo.get(i));
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
