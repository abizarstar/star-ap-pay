package com.star.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.jxls.common.Context;
import org.jxls.util.JxlsHelper;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.ManageDirectory;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.dao.APInfoDAO;
import com.star.dao.CstmVchrInfoDAO;
import com.star.dao.PaymentDAO;
import com.star.dao.ReportDAO;
import com.star.linkage.ap.APDesCheckInfoBrowseOutput;
import com.star.linkage.cstmdocinfo.CstmDocInfoBrowseOutput;
import com.star.linkage.cstmvchrinfo.VoucherReconInfoBrowseOutput;
import com.star.linkage.report.LockBoxInforBrowseOutput;

@Path("/export")
public class ExportService {

	@POST
	@Secured
	@Path("/get-recon")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public void getReconData(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data)
			throws Exception {

		BrowseResponse<VoucherReconInfoBrowseOutput> starBrowseResponse = new BrowseResponse<VoucherReconInfoBrowseOutput>();
		starBrowseResponse.setOutput(new VoucherReconInfoBrowseOutput());
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		String brhId = jsonObject.get("brhId").getAsString();
		String venId = jsonObject.get("venId").getAsString();
		String cry = jsonObject.get("cry").getAsString();

		CstmVchrInfoDAO vchrInfoDAO = new CstmVchrInfoDAO();

		List costReconExport = vchrInfoDAO.readVoucherReconExport(cmpyId, brhId, venId, cry);

		File initialFile = new File("xls/cost-recon-tmpl.xlsx");

		try (InputStream is = getClass().getClassLoader().getResourceAsStream("xls/cost-recon-tmpl.xlsx")) {
			try (OutputStream os = new FileOutputStream("G:\\Reconcile_List.xlsx")) {
				Context context = new Context();
				context.putVar("costReconExports", costReconExport);
				JxlsHelper.getInstance().processTemplate(is, os, context);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@GET
	@Path("/recon")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response downloadReconFile(@HeaderParam("user-id") String userId, @QueryParam("cmpyId") String cmpyId,
			@QueryParam("venId") String venId, @QueryParam("brh") String brh, @QueryParam("cry") String cry,
			@QueryParam("exRef") String exRef) throws Exception {
		ManageDirectory directory = new ManageDirectory();

		InputStream fileInputStream;
		javax.ws.rs.core.Response.ResponseBuilder responseBuilder = null;
		
		CstmVchrInfoDAO vchrInfoDAO = new CstmVchrInfoDAO();

		List costReconExport = vchrInfoDAO.readVoucherReconExport(cmpyId, brh, venId, cry);

		String flNm = "Reconcile_List_" + new Date().getTime() + ".xlsx";
		String flNmPath = CommonConstants.ROOT_DIR + flNm;
		
		try (InputStream is = getClass().getClassLoader().getResourceAsStream("xls/cost-recon-tmpl.xlsx")) {
			try (OutputStream os = new FileOutputStream(flNmPath)) {
				Context context = new Context();
				context.putVar("costReconExports", costReconExport);
				JxlsHelper.getInstance().processTemplate(is, os, context);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		fileInputStream = directory.downloadFile(flNmPath);

		responseBuilder = javax.ws.rs.core.Response.ok((Object) fileInputStream);

		responseBuilder.type("application/xlsx");
		responseBuilder.header("Content-Disposition", "attachment;filename=" + flNm);
		
		
		
		return responseBuilder.build();
	}
	
	@GET
	@Path("/chk-disb")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response downloadCheckDisb(@HeaderParam("user-id") String userId, @QueryParam("cmpyId") String cmpyId,
			@QueryParam("venId") String venId, @QueryParam("brh") String brh, @QueryParam("invcNo") String invcNo,
			@QueryParam("frmInvDt") String frmInvDt, @QueryParam("toInvcDt") String toInvcDt) throws Exception {
		
		BrowseResponse<APDesCheckInfoBrowseOutput> starBrowseResponse = new BrowseResponse<APDesCheckInfoBrowseOutput>();
		starBrowseResponse.setOutput(new APDesCheckInfoBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		APInfoDAO apInfo = new APInfoDAO();
		List apChkDsbExport = apInfo.readDsbCheck(starBrowseResponse, cmpyId, invcNo, venId, brh, frmInvDt, toInvcDt);
		
		
		ManageDirectory directory = new ManageDirectory();

		InputStream fileInputStream;
		javax.ws.rs.core.Response.ResponseBuilder responseBuilder = null;

		String flNm = "AP_check_disb_" + new Date().getTime() + ".xlsx";
		String flNmPath = CommonConstants.ROOT_DIR + flNm;
		
		try (InputStream is = getClass().getClassLoader().getResourceAsStream("xls/ap-chk-disb.xlsx")) {
			try (OutputStream os = new FileOutputStream(flNmPath)) {
				Context context = new Context();
				context.putVar("apChkDsbExports", apChkDsbExport);
				JxlsHelper.getInstance().processTemplate(is, os, context);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		fileInputStream = directory.downloadFile(flNmPath);

		responseBuilder = javax.ws.rs.core.Response.ok((Object) fileInputStream);

		responseBuilder.type("application/xlsx");
		responseBuilder.header("Content-Disposition", "attachment;filename=" + flNm);
		
		
		
		return responseBuilder.build();
	}
	
	@GET
	@Path("/vch-pay-info")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response payProposalVouchers(@QueryParam("usrId") String usrId, @QueryParam("cmpyId") String cmpyId,
			@QueryParam("venIdFrm") String venIdFrm, @QueryParam("venIdTo") String venIdTo, 
			@QueryParam("invcDt") String invcDt, @QueryParam("invcDtTo") String invcDtTo, 
			@QueryParam("dueDt") String dueDt, @QueryParam("dueDtTo") String dueDtTo,
			@QueryParam("discDt") String discDt, @QueryParam("discDtTo") String discDtTo
			, @QueryParam("payMthd") String payMthd) throws Exception {
		
		BrowseResponse<CstmDocInfoBrowseOutput> starBrowseResponse = new BrowseResponse<CstmDocInfoBrowseOutput>();
		starBrowseResponse.setOutput(new CstmDocInfoBrowseOutput());
		
		PaymentDAO paymentDAO= new PaymentDAO();
		List vchrInfoList = paymentDAO.readStxVouchersExport(usrId, venIdFrm, venIdTo, payMthd, invcDt, invcDtTo, dueDt, dueDtTo, discDt, discDtTo);
		
		
		ManageDirectory directory = new ManageDirectory();

		InputStream fileInputStream;
		javax.ws.rs.core.Response.ResponseBuilder responseBuilder = null;

		String flNm = "Voucher_Pay_Prop_" + new Date().getTime() + ".xlsx";
		String flNmPath = CommonConstants.ROOT_DIR + flNm;
		
		try (InputStream is = getClass().getClassLoader().getResourceAsStream("xls/vchr-pay-prop.xlsx")) {
			try (OutputStream os = new FileOutputStream(flNmPath)) {
				Context context = new Context();
				context.putVar("vchrInfoLists", vchrInfoList);
				context.putVar("venRange", venIdFrm + "-" + venIdTo);
				context.putVar("dueRange", dueDt + "-" + dueDtTo);
				context.putVar("invRange", invcDt + "-" + invcDtTo);
				context.putVar("discRange", discDt + "-" + discDtTo);
				context.putVar("payRange", payMthd);
				JxlsHelper.getInstance().processTemplate(is, os, context);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		fileInputStream = directory.downloadFile(flNmPath);

		responseBuilder = javax.ws.rs.core.Response.ok((Object) fileInputStream);

		responseBuilder.type("application/xlsx");
		responseBuilder.header("Content-Disposition", "attachment;filename=" + flNm);
		
		
		
		return responseBuilder.build();
	}
	
	@GET
	@Path("/lck-rpt")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response downloadLockboxReport(@HeaderParam("user-id") String userId, @QueryParam("cmpyId") String cmpyId,
			@QueryParam("cusId") String cusId, @QueryParam("sts") String sts, @QueryParam("frmDpstDt") String frmDpstDt,
			@QueryParam("toDpstDt") String toDpstDt) throws Exception {
		
		BrowseResponse<LockBoxInforBrowseOutput> starBrowseResponse = new BrowseResponse<LockBoxInforBrowseOutput>();
		starBrowseResponse.setOutput(new LockBoxInforBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		ReportDAO reportDAO = new ReportDAO();
		List dataList = reportDAO.readLockBoxTrn(starBrowseResponse, frmDpstDt, toDpstDt, cusId, sts);
		
		
		ManageDirectory directory = new ManageDirectory();

		InputStream fileInputStream;
		javax.ws.rs.core.Response.ResponseBuilder responseBuilder = null;

		String flNm = "Lockbox_report_" + new Date().getTime() + ".xlsx";
		String flNmPath = CommonConstants.ROOT_DIR + flNm;
		
		try (InputStream is = getClass().getClassLoader().getResourceAsStream("xls/lckbx-rpt.xlsx")) {
			try (OutputStream os = new FileOutputStream(flNmPath)) {
				Context context = new Context();
				context.putVar("dataList", dataList);
				JxlsHelper.getInstance().processTemplate(is, os, context);
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		fileInputStream = directory.downloadFile(flNmPath);

		responseBuilder = javax.ws.rs.core.Response.ok((Object) fileInputStream);

		responseBuilder.type("application/xlsx");
		responseBuilder.header("Content-Disposition", "attachment;filename=" + flNm);
		
		return responseBuilder.build();
	}

}
