package com.star.service;

import java.io.InputStream;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.CommonConstants;
import com.star.common.MaintanenceResponse;
import com.star.common.ManageDirectory;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.dao.CheckDAO;
import com.star.dao.CstmDocInfoDAO;
import com.star.dao.PaymentDAO;
import com.star.linkage.check.CheckLotInfo;
import com.star.linkage.cstmdocinfo.CstmDocInfoBrowseOutput;
import com.star.linkage.payment.PaymentBrowseOutput;
import com.star.linkage.payment.PaymentInfo;
import com.star.linkage.payment.PaymentManOutput;
import com.star.modal.ChkLotUsage;

@Path("/payment")
public class PaymentService {

	@POST
	@Secured
	@Path("/add")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response addUpdateBank(@HeaderParam("user-id") String userId, String data) throws Exception {

		MaintanenceResponse<PaymentManOutput> starManResponse = new MaintanenceResponse<PaymentManOutput>();
		starManResponse.setOutput(new PaymentManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		PaymentInfo paymentInfo = new Gson().fromJson(data, PaymentInfo.class);

		PaymentDAO paymentDAO = new PaymentDAO();

		int iRcrdCount = paymentDAO.validateReqId(paymentInfo);

		if (iRcrdCount == 0) {
			paymentDAO.addPayment(starManResponse, paymentInfo, userId);
		} else {
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add(CommonConstants.REQ_ERR);
		}

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starManResponse));
	}

	@POST
	@Secured
	@Path("/read")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getBank(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			@HeaderParam("user-grp") String userGrp, String data) throws Exception {

		BrowseResponse<PaymentBrowseOutput> starBrowseResponse = new BrowseResponse<PaymentBrowseOutput>();
		starBrowseResponse.setOutput(new PaymentBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String reqId = jsonObject.get("reqId").getAsString();
		String paySts = jsonObject.get("paySts").getAsString();
		String venId = jsonObject.get("venId").getAsString();
		String vchrNo = jsonObject.get("vchrNo").getAsString();
		int pageNo=jsonObject.get("pageNo").getAsInt();
		String issueDate=jsonObject.get("issueDate").getAsString();
		System.out.println(issueDate);
		PaymentInfo info = new PaymentInfo();

		info.setPaySts(paySts);
		info.setReqId(reqId);
		info.setVenId(venId);
		info.setVchrNo(vchrNo);

		PaymentDAO paymentDAO = new PaymentDAO();

		paymentDAO.readPayment(starBrowseResponse, info, userId, venId, vchrNo, userGrp, cmpyId,pageNo,issueDate);

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblPayment.size();
		
		if (starBrowseResponse.output.fldTblPayment.size() < CommonConstants.DB_OUT_REC) {
			starBrowseResponse.output.eof = 1;
		} else {
			starBrowseResponse.output.eof = 0;
		}
		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));
	}

	@POST
	@Secured
	@Path("/upd-sts")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response updateStatus(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data) throws Exception {

		MaintanenceResponse<PaymentManOutput> starManResponse = new MaintanenceResponse<PaymentManOutput>();
		starManResponse.setOutput(new PaymentManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		PaymentInfo paymentInfo = new Gson().fromJson(data, PaymentInfo.class);

		PaymentDAO paymentDAO = new PaymentDAO();

		/* VALIDATE FOR CHECK LOT */
		if (paymentInfo.getPaySts().equals("A") && paymentInfo.getPayMthd().contains("CHECK")) {
			CheckDAO checkDAO = new CheckDAO();

			ChkLotUsage chkLotUsage = checkDAO.getCurrentCheckNo(paymentInfo.getBankCd(), paymentInfo.getAcctNo());

			int iTotalChks = 0;

			if (chkLotUsage.getBnkCode() != null) {
				String endCheckNo = checkDAO.getEndLotCheck(chkLotUsage);

				List<CheckLotInfo> checkLotList = checkDAO.getChkLotList(chkLotUsage);

				for (int i = 0; i < checkLotList.size(); i++) {
					iTotalChks = iTotalChks + (Integer.parseInt(checkLotList.get(i).getChkTo())
							- Integer.parseInt(checkLotList.get(i).getChkFrm()));

					iTotalChks = iTotalChks + 1;
				}

				iTotalChks = iTotalChks + (Integer.parseInt(endCheckNo) - Integer.parseInt(chkLotUsage.getCurChk()));
			}

			checkDAO.getVenListForChecks(starManResponse, paymentInfo.getReqId(), iTotalChks);
		}

		if (starManResponse.output.rtnSts == 0) {
			paymentDAO.updatePayment(starManResponse, paymentInfo, userId, cmpyId);
		}

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputUpdate(starManResponse));
	}

	@POST
	@Secured
	@Path("/read-inv")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getInvoices(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data) throws Exception {

		BrowseResponse<CstmDocInfoBrowseOutput> starBrowseResponse = new BrowseResponse<CstmDocInfoBrowseOutput>();

		starBrowseResponse.setOutput(new CstmDocInfoBrowseOutput());

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		String invcDt = jsonObject.get("invcDt").getAsString();
		String invcDtTo = jsonObject.get("invcDtTo").getAsString();
		String dueDt = jsonObject.get("dueDt").getAsString();
		String dueDtTo = jsonObject.get("dueDtTo").getAsString();
		String discDt = jsonObject.get("discDt").getAsString();
		String discDtTo = jsonObject.get("discDtTo").getAsString();
		String venIdFrm = jsonObject.get("venIdFrm").getAsString();
		String venIdTo = jsonObject.get("venIdTo").getAsString();
		String paymentMethod = jsonObject.get("payMthd").getAsString();
		String disbNo = jsonObject.get("disbNo").getAsString();

		PaymentDAO paymentDAO = new PaymentDAO();

		paymentDAO.readStxVouchers(starBrowseResponse, userId, cmpyId, venIdFrm, venIdTo, paymentMethod, invcDt, invcDtTo, dueDt,
				dueDtTo, discDt, discDtTo, disbNo);

		/*
		 * paymentDAO.readVouchers(starBrowseResponse, venIdFrm, venIdTo,
		 * paymentMethod);
		 */

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblDoc.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputInvoices(starBrowseResponse));

	}

	@POST
	@Secured
	@Path("/read-inv-esp")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getInvoicesESP(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId, String data) throws Exception {

		BrowseResponse<CstmDocInfoBrowseOutput> starBrowseResponse = new BrowseResponse<CstmDocInfoBrowseOutput>();

		starBrowseResponse.setOutput(new CstmDocInfoBrowseOutput());

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		String invcDt = jsonObject.get("invcDt").getAsString();
		String invcDtTo = jsonObject.get("invcDtTo").getAsString();
		String dueDt = jsonObject.get("dueDt").getAsString();
		String dueDtTo = jsonObject.get("dueDtTo").getAsString();
		String discDt = jsonObject.get("discDt").getAsString();
		String discDtTo = jsonObject.get("discDtTo").getAsString();
		String venIdFrm = jsonObject.get("venIdFrm").getAsString();
		String venIdTo = jsonObject.get("venIdTo").getAsString();
		String paymentMethod = jsonObject.get("payMthd").getAsString();
		String disbNo = jsonObject.get("disbNo").getAsString();

		PaymentDAO paymentDAO = new PaymentDAO();

		paymentDAO.readESPVouchers(starBrowseResponse, userId, cmpyId, venIdFrm, venIdTo, paymentMethod, invcDt, invcDtTo, dueDt,
				dueDtTo, discDt, discDtTo, disbNo);

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		starBrowseResponse.output.nbrRecRtn = starBrowseResponse.output.fldTblDoc.size();

		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutputInvoices(starBrowseResponse));

	}

	@GET
	@Path("/dwnld-ach")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
	public Response downloadAchFile(@HeaderParam("user-id") String userId, @QueryParam("flNm") String flNm,
			@QueryParam("forceDwnld") Integer forceDwnld) throws Exception {
		ManageDirectory directory = new ManageDirectory();

		InputStream fileInputStream;
		javax.ws.rs.core.Response.ResponseBuilder responseBuilder = null;

		fileInputStream = directory.downloadAchFile(flNm);

		if (fileInputStream != null && forceDwnld == 0) {
			PaymentDAO paymentDAO = new PaymentDAO();

			// paymentDAO.updateAchFileSentStatus(flNm);
		}

		responseBuilder = javax.ws.rs.core.Response.ok((Object) fileInputStream);

		responseBuilder.type("application/xml");
		responseBuilder.header("Content-Disposition", "attachment;filename=" + flNm);

		return responseBuilder.build();
	}

	@POST
	@Secured
	@Path("/sftp-ach")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response sftpAchFile(@HeaderParam("user-id") String userId, @HeaderParam("cmpy-id") String cmpyId,
			String data) throws Exception {

		MaintanenceResponse<PaymentManOutput> starManResponse = new MaintanenceResponse<PaymentManOutput>();

		starManResponse.setOutput(new PaymentManOutput());

		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();

		String reqId = jsonObject.get("reqId").getAsString();
		String flNm = jsonObject.get("flNm").getAsString();

		PaymentDAO paymentDAO = new PaymentDAO();
		paymentDAO.sftpAchFile(starManResponse, cmpyId, reqId, flNm);

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutputUpdate(starManResponse));

	}
	
	
	@POST
	@Secured
	@Path("/delete")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response deleteProposal(@HeaderParam("user-id") String userId, String data) throws Exception {

		MaintanenceResponse<PaymentManOutput> starManResponse = new MaintanenceResponse<PaymentManOutput>();
		starManResponse.setOutput(new PaymentManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String reqId = jsonObject.get("reqId").getAsString();
		String paySts = jsonObject.get("paySts").getAsString();

		PaymentDAO paymentDAO = new PaymentDAO();
		
		if(paySts.equals("P"))
		{
			paymentDAO.deleteProposal(starManResponse, reqId, paySts);
		}
		else
		{
			starManResponse.output.rtnSts = 1;
			starManResponse.output.messages.add("Proposal shoule be in pending status");
		}

		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starManResponse));
	}

	private GenericEntity<?> getGenericBrowseOutputInvoices(
			BrowseResponse<CstmDocInfoBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<CstmDocInfoBrowseOutput>>(starBrowseResponse) {
		};
	}

	private GenericEntity<?> getGenericMaintenanceOutputUpdate(
			MaintanenceResponse<PaymentManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<PaymentManOutput>>(starMaintenanceResponse) {
		};
	}

	private GenericEntity<?> getGenericMaintenanceOutput(
			MaintanenceResponse<PaymentManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<PaymentManOutput>>(starMaintenanceResponse) {
		};
	}

	private GenericEntity<?> getGenericBrowseOutput(BrowseResponse<PaymentBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<PaymentBrowseOutput>>(starBrowseResponse) {
		};
	}

}
