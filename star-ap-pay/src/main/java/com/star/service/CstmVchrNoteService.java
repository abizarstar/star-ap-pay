package com.star.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.MaintanenceResponse;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.dao.VchrNotesDAO;
import com.star.linkage.cstmvchrinfo.NotesInfoAddManOutput;
import com.star.linkage.cstmvchrinfo.NotesInfoBrowse;
import com.star.linkage.cstmvchrinfo.NotesInfoManOutput;

@Path("/vchr-note")
public class CstmVchrNoteService {

	@POST
	@Secured
	@Path("/read")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response readVoucherNote(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<NotesInfoBrowse> starBrowseResponse = new BrowseResponse<NotesInfoBrowse>();
		starBrowseResponse.setOutput(new NotesInfoBrowse());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		int ctlNo = jsonObject.get("ctlNo").getAsInt();
		VchrNotesDAO vchrNote = new VchrNotesDAO();
		vchrNote.readVoucherNote(starBrowseResponse, ctlNo);
		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));

	}

	private GenericEntity<?> getGenericBrowseOutput(BrowseResponse<NotesInfoBrowse> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<NotesInfoBrowse>>(starBrowseResponse) {
		};
	}

	@POST
	@Secured
	@Path("/add")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response addVoucherNote(@HeaderParam("user-id") String userId, String data) throws Exception {

		MaintanenceResponse<NotesInfoManOutput> starManResponse = new MaintanenceResponse<NotesInfoManOutput>();
		starManResponse.setOutput(new NotesInfoManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		
		int ctlNo = jsonObject.get("ctlNo").getAsInt();
		String note = jsonObject.get("note").getAsString();
		
		VchrNotesDAO vchrNote = new VchrNotesDAO();
		vchrNote.addVoucherNote(starManResponse, userId, ctlNo, note);
		
		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starManResponse));
	}

	@POST
	@Secured
	@Path("/del")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response delVoucherNote(@HeaderParam("user-id") String userId, String data) throws Exception {

		MaintanenceResponse<NotesInfoManOutput> starManResponse = new MaintanenceResponse<NotesInfoManOutput>();
		starManResponse.setOutput(new NotesInfoManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		int notesId = jsonObject.get("notesId").getAsInt();
		VchrNotesDAO vchrNote = new VchrNotesDAO();
		vchrNote.dltVoucherNote(starManResponse, notesId);
		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starManResponse));

	}

	private GenericEntity<?> getGenericMaintenanceOutput(
			MaintanenceResponse<NotesInfoManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<NotesInfoManOutput>>(starMaintenanceResponse) {
		};
	}

}
