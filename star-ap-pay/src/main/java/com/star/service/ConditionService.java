package com.star.service;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.star.common.BrowseResponse;
import com.star.common.MaintanenceResponse;
import com.star.common.Secured;
import com.star.common.StarResponseBuilder;
import com.star.dao.BankDAO;
import com.star.dao.ConditionDAO;
import com.star.dao.PaymentDAO;
import com.star.linkage.bank.BankBrowseOutput;
import com.star.linkage.bank.BankManOutput;
import com.star.linkage.condition.ConditionBrowseOutput;
import com.star.linkage.condition.ConditionInfo;
import com.star.linkage.condition.ConditionManOutput;
import com.star.linkage.payment.PaymentInfo;
import com.star.linkage.payment.PaymentManOutput;

@Path("/condition")
public class ConditionService {

	@POST
	@Secured
	@Path("/add")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response addUpdateBank(@HeaderParam("user-id") String userId, String data) throws Exception {

		MaintanenceResponse<ConditionManOutput> starManResponse = new MaintanenceResponse<ConditionManOutput>();
		starManResponse.setOutput(new ConditionManOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();
		
		ConditionInfo conditionInfo = new Gson().fromJson(data, ConditionInfo.class);
		
		ConditionDAO conditionDAO = new ConditionDAO();
		
		conditionDAO.addCondition(starManResponse, conditionInfo);
		
		return starResponseBuilder.getSuccessResponse(getGenericMaintenanceOutput(starManResponse));
	}
	
	@POST
	@Secured
	@Path("/read")
	@Consumes({ "application/json" })
	@Produces({ "application/json" })
	public Response getBank(@HeaderParam("user-id") String userId, String data) throws Exception {

		BrowseResponse<ConditionBrowseOutput> starBrowseResponse = new BrowseResponse<ConditionBrowseOutput>();
		starBrowseResponse.setOutput(new ConditionBrowseOutput());
		StarResponseBuilder starResponseBuilder = new StarResponseBuilder();

		JsonObject jsonObject = new JsonParser().parse(data).getAsJsonObject();
		String usrId = jsonObject.get("usrId").getAsString();
		
		ConditionDAO conditionDAO = new ConditionDAO();
		
		conditionDAO.getUserCondition(starBrowseResponse, usrId);
		
		return starResponseBuilder.getSuccessResponse(getGenericBrowseOutput(starBrowseResponse));
	}
	
	private GenericEntity<?> getGenericBrowseOutput(BrowseResponse<ConditionBrowseOutput> starBrowseResponse) {
		return new GenericEntity<BrowseResponse<ConditionBrowseOutput>>(starBrowseResponse) {
		};
	}
	
	private GenericEntity<?> getGenericMaintenanceOutput(
			MaintanenceResponse<ConditionManOutput> starMaintenanceResponse) {
		return new GenericEntity<MaintanenceResponse<ConditionManOutput>>(starMaintenanceResponse) {
		};
	}
}
