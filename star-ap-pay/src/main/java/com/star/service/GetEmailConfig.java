package com.star.service;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class GetEmailConfig
 */
public class GetEmailConfig extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private final String clientId = "135558245086-14tjdi1gatcpo352v9kjveb4sl1f7np3.apps.googleusercontent.com";
	 private final String clientSecret = "zJpDtrqk7is9OwjDNWi5CzOK";
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetEmailConfig() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		 // redirect to google for authorization
		   StringBuilder oauthUrl = new StringBuilder().append("https://accounts.google.com/o/oauth2/auth")
		   .append("?client_id=").append(clientId) // the client id from the api console registration
		   .append("&response_type=code")
		   .append("&scope=openid%20email%20https://www.googleapis.com/auth/gmail.readonly") // scope is the api permissions we are requesting
		   .append("&redirect_uri=http://localhost:8080/star-ocr-ap/ConfigEmailCallBack") // the servlet that google redirects to after authorization
		   .append("&state=this_can_be_anything_to_help_correlate_the_response%3Dlike_session_id")
		   .append("&access_type=offline") // here we are asking to access to user's data while they are not signed in
		   .append("&approval_prompt=force"); // this requires them to verify which account to use, if they are already signed in
		   
		   response.sendRedirect(oauthUrl.toString());
	}

}
