package com.star.linkage.ar;

public class ARPaymentInfo {

	String cmpyId;
	String refPfx;
	String refNo;
	String refItm;
	String arPfx;
	String arNo;
	String dsAmt;
	String rcvdAmt;
	String updRef;
	String discAmt;
	String dueDt;
	String brh;
	String cusId;
	String discTknAmt;

	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}

	public String getRefPfx() {
		return refPfx;
	}
	public void setRefPfx(String refPfx) {
		this.refPfx = refPfx;
	}
	
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	
	public String getRefItm() {
		return refItm;
	}
	public void setRefItm(String refItm) {
		this.refItm = refItm;
	}
	
	public String getArPfx() {
		return arPfx;
	}
	public void setArPfx(String arPfx) {
		this.arPfx = arPfx;
	}
	
	public String getArNo() {
		return arNo;
	}
	public void setArNo(String arNo) {
		this.arNo = arNo;
	}

	public String getDsAmt() {
		return dsAmt;
	}
	public void setDsAmt(String dsAmt) {
		this.dsAmt = dsAmt;
	}
	
	public String getRcvdAmt() {
		return rcvdAmt;
	}
	public void setRcvdAmt(String rcvdAmt) {
		this.rcvdAmt = rcvdAmt;
	}
	
	public String getUpdRef() {
		return updRef;
	}
	public void setUpdRef(String updRef) {
		this.updRef = updRef;
	}
	public String getDiscAmt() {
		return discAmt;
	}
	public void setDiscAmt(String discAmt) {
		this.discAmt = discAmt;
	}
	public String getDueDt() {
		return dueDt;
	}
	public void setDueDt(String dueDt) {
		this.dueDt = dueDt;
	}
	public String getBrh() {
		return brh;
	}
	public void setBrh(String brh) {
		this.brh = brh;
	}
	public String getCusId() {
		return cusId;
	}
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	public String getDiscTknAmt() {
		return discTknAmt;
	}
	public void setDiscTknAmt(String discTknAmt) {
		this.discTknAmt = discTknAmt;
	}
	
	
	
}
