package com.star.linkage.ar;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;

public class ARDesboardBrowseOutput extends BrowseOutput {

	public List<ARDesboard> fldTblARdsbrd = new ArrayList<ARDesboard>();
	public BigDecimal chkProof;
	public String rcptSts;
	
}
