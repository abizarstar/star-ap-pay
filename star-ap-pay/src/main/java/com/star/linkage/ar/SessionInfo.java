package com.star.linkage.ar;

import java.util.Date;

public class SessionInfo {

	private String cmpyId;
	private String ssnId;
	private String brh;
	private String jrnlDt;
	private String lgnNm;
	private String bnk;
	private String totNbrChk;
	private String totDepAmt;
	private String totDepAmtStr;
	private String sts;
	private String depCry;
	private String depExrt;
	private String depDt;
	private String arCry1;
	private String arXexrt1;
	private String arCry2;
	private String arXexrt2;
	private String arCry3;
	private String arXexrt3;


	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}

	public String getSsnId() {
		return ssnId;
	}
	public void setSsnId(String ssnId) {
		this.ssnId = ssnId;
	}
	
	public String getBrh() {
		return brh;
	}
	public void setBrh(String brh) {
		this.brh = brh;
	}
	
	public String getJrnlDt() {
		return jrnlDt;
	}
	public void setJrnlDt(String jrnlDt) {
		this.jrnlDt = jrnlDt;
	}
	
	public String getLgnNm() {
		return lgnNm;
	}
	public void setLgnNm(String lgnNm) {
		this.lgnNm = lgnNm;
	}
	
	public String getBnk() {
		return bnk;
	}
	public void setBnk(String bnk) {
		this.bnk = bnk;
	}

	public String getTotNbrChk() {
		return totNbrChk;
	}
	public void setTotNbrChk(String totNbrChk) {
		this.totNbrChk = totNbrChk;
	}
	
	public String getTotDepAmt() {
		return totDepAmt;
	}
	public void setTotDepAmt(String totDepAmt) {
		this.totDepAmt = totDepAmt;
	}
	
	public String getSts() {
		return sts;
	}
	public void setSts(String sts) {
		this.sts = sts;
	}
	
	public String getDepCry() {
		return depCry;
	}
	public void setDepCry(String depCry) {
		this.depCry = depCry;
	}
	
	public String getDepExrt() {
		return depExrt;
	}
	public void setDepExrt(String depExrt) {
		this.depExrt = depExrt;
	}
	
	public String getDepDt() {
		return depDt;
	}
	public void setDepDt(String depDt) {
		this.depDt = depDt;
	}
	
	public String getArCry1() {
		return arCry1;
	}
	public void setArCry1(String arCry1) {
		this.arCry1 = arCry1;
	}
	
	public String getArXexrt1() {
		return arXexrt1;
	}
	public void setArXexrt1(String arXexrt1) {
		this.arXexrt1 = arXexrt1;
	}
	
	public String getArCry2() {
		return arCry2;
	}
	public void setArCry2(String arCry2) {
		this.arCry2 = arCry2;
	}
	
	public String getArXexrt2() {
		return arXexrt2;
	}
	public void setArXexrt2(String arXexrt2) {
		this.arXexrt2 = arXexrt2;
	}
	
	public String getArCry3() {
		return arCry3;
	}
	public void setArCry3(String arCry3) {
		this.arCry3 = arCry3;
	}
	
	public String getArXexrt3() {
		return arXexrt3;
	}
	public void setArXexrt3(String arXexrt3) {
		this.arXexrt3 = arXexrt3;
	}
	public String getTotDepAmtStr() {
		return totDepAmtStr;
	}
	public void setTotDepAmtStr(String totDepAmtStr) {
		this.totDepAmtStr = totDepAmtStr;
	}
	
	
	
}
