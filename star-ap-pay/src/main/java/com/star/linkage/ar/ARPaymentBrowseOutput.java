package com.star.linkage.ar;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;

public class ARPaymentBrowseOutput extends BrowseOutput {

	public List<ARPaymentInfo> fldTblARPymnt = new ArrayList<ARPaymentInfo>();
	public BigDecimal chkProof;
	public String rcptSts;
	public String customerId;
	public String ssnBrh;
	public String jrnlDt;
	public String crtdDt;
	public String crtdUsr;
	
	
}
