package com.star.linkage.ar;

public class GLDistributionInfo {

	String glAcct;
	String desc;
	String sacct;
	String drAmt;
	String crAmt;
	String rmk;
	String refPfx;
	String refNo;
	String refItm;

	public String getGlAcct() {
		return glAcct;
	}
	public void setGlAcct(String glAcct) {
		this.glAcct = glAcct;
	}
	
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getSacct() {
		return sacct;
	}
	public void setSacct(String sacct) {
		this.sacct = sacct;
	}

	public String getDrAmt() {
		return drAmt;
	}
	public void setDrAmt(String drAmt) {
		this.drAmt = drAmt;
	}

	public String getCrAmt() {
		return crAmt;
	}
	public void setCrAmt(String crAmt) {
		this.crAmt = crAmt;
	}
	
	public String getRmk() {
		return rmk;
	}
	public void setRmk(String rmk) {
		this.rmk = rmk;
	}
	
	public String getRefPfx() {
		return refPfx;
	}
	public void setRefPfx(String refPfx) {
		this.refPfx = refPfx;
	}
	
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	
	public String getRefItm() {
		return refItm;
	}
	public void setRefItm(String refItm) {
		this.refItm = refItm;
	}
}
