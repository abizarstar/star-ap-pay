package com.star.linkage.ar;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;

public class ARPmtRecvDataBrowseOutput extends BrowseOutput {

	public List<ARPmtRecvData> fldTblARPmtRecvdata = new ArrayList<ARPmtRecvData>();
	public BigDecimal chkProof;
	public String rcptSts;
	
}
