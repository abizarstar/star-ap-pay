package com.star.linkage.ar;

public class ARPmtRecvData {

	private String cmpyId;
	private String arPfx;
	private String crcpNo;
	private String crcpBrh;
	private String jrnlDt;
	private String updtDt;
	private String lgnId;
	private String depCry;
	private String depExrt;
	private String depDt;
	private String cusId;
	private String cusNm;
	private String chkNo;
	private String desc30;
	private String chkAmt;
	
	
	public String getCusNm() {
		return cusNm;
	}
	public void setCusNm(String cusNm) {
		this.cusNm = cusNm;
	}
	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}
	public String getArPfx() {
		return arPfx;
	}
	public void setArPfx(String arPfx) {
		this.arPfx = arPfx;
	}
	public String getCrcpNo() {
		return crcpNo;
	}
	public void setCrcpNo(String crcpNo) {
		this.crcpNo = crcpNo;
	}
	public String getCrcpBrh() {
		return crcpBrh;
	}
	public void setCrcpBrh(String crcpBrh) {
		this.crcpBrh = crcpBrh;
	}
	public String getJrnlDt() {
		return jrnlDt;
	}
	public void setJrnlDt(String jrnlDt) {
		this.jrnlDt = jrnlDt;
	}
	public String getUpdtDt() {
		return updtDt;
	}
	public void setUpdtDt(String updtDt) {
		this.updtDt = updtDt;
	}
	public String getLgnId() {
		return lgnId;
	}
	public void setLgnId(String lgnId) {
		this.lgnId = lgnId;
	}
	public String getDepCry() {
		return depCry;
	}
	public void setDepCry(String depCry) {
		this.depCry = depCry;
	}
	public String getDepExrt() {
		return depExrt;
	}
	public void setDepExrt(String depExrt) {
		this.depExrt = depExrt;
	}
	public String getDepDt() {
		return depDt;
	}
	public void setDepDt(String depDt) {
		this.depDt = depDt;
	}
	public String getCusId() {
		return cusId;
	}
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	public String getChkNo() {
		return chkNo;
	}
	public void setChkNo(String chkNo) {
		this.chkNo = chkNo;
	}
	public String getDesc30() {
		return desc30;
	}
	public void setDesc30(String desc30) {
		this.desc30 = desc30;
	}
	public String getChkAmt() {
		return chkAmt;
	}
	public void setChkAmt(String chkAmt) {
		this.chkAmt = chkAmt;
	}
	
}
