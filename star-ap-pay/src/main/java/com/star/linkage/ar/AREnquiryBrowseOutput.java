package com.star.linkage.ar;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;

public class AREnquiryBrowseOutput extends BrowseOutput {

	public List<AREnquiryInfo> fldTblAREnqry = new ArrayList<AREnquiryInfo>();
	public BigDecimal chkProof;
	
}
