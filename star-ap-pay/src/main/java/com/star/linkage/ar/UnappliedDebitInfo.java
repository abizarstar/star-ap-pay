package com.star.linkage.ar;

public class UnappliedDebitInfo {

	String desc;
	String drAmt;
	String cry;
	String refPfx;
	String refNo;
	String refItm;
	
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getDrAmt() {
		return drAmt;
	}
	public void setDrAmt(String drAmt) {
		this.drAmt = drAmt;
	}
	
	public String getCry() {
		return cry;
	}
	public void setCry(String cry) {
		this.cry = cry;
	}
	
	public String getRefPfx() {
		return refPfx;
	}
	public void setRefPfx(String refPfx) {
		this.refPfx = refPfx;
	}
	
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	
	public String getRefItm() {
		return refItm;
	}
	public void setRefItm(String refItm) {
		this.refItm = refItm;
	}
}
