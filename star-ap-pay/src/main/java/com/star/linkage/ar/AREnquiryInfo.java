package com.star.linkage.ar;

public class AREnquiryInfo {

	private String cmpyId;
	private String arPfx;
	private String arNo;
	private String cusId;
	private String cusNm;
	private String desc;
	private String cry;
	private String dueDt;
	private String brh;
	private String updRef;
	private String invDt;
	private String origAmt;
	private String origAmtStr;
	private String ipAmt;
	private String ipAmtStr;
	private String balamt;
	private String balamtStr;
	private String discAmt;
	private String discAmtStr;
	private boolean processFlg;
	private String discFlg;
	private String discDt;
	

	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}
	
	public String getArPfx() {
		return arPfx;
	}
	public void setArPfx(String arPfx) {
		this.arPfx = arPfx;
	}
	
	public String getArNo() {
		return arNo;
	}
	public void setArNo(String arNo) {
		this.arNo = arNo;
	}

	public String getCusId() {
		return cusId;
	}
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public String getCry() {
		return cry;
	}
	public void setCry(String cry) {
		this.cry = cry;
	}

	public String getDueDt() {
		return dueDt;
	}
	public void setDueDt(String dueDt) {
		this.dueDt = dueDt;
	}
	
	public String getBrh() {
		return brh;
	}
	public void setBrh(String brh) {
		this.brh = brh;
	}
	
	public String getUpdRef() {
		return updRef;
	}
	public void setUpdRef(String updRef) {
		this.updRef = updRef;
	}
	
	public String getInvDt() {
		return invDt;
	}
	public void setInvDt(String invDt) {
		this.invDt = invDt;
	}
	
	public String getOrigAmt() {
		return origAmt;
	}
	public void setOrigAmt(String origAmt) {
		this.origAmt = origAmt;
	}
	
	public String getIpAmt() {
		return ipAmt;
	}
	public void setIpAmt(String ipAmt) {
		this.ipAmt = ipAmt;
	}
	
	public String getBalamt() {
		return balamt;
	}
	public void setBalamt(String balamt) {
		this.balamt = balamt;
	}
	public String getOrigAmtStr() {
		return origAmtStr;
	}
	public void setOrigAmtStr(String origAmtStr) {
		this.origAmtStr = origAmtStr;
	}
	public String getIpAmtStr() {
		return ipAmtStr;
	}
	public void setIpAmtStr(String ipAmtStr) {
		this.ipAmtStr = ipAmtStr;
	}
	public String getBalamtStr() {
		return balamtStr;
	}
	public void setBalamtStr(String balamtStr) {
		this.balamtStr = balamtStr;
	}
	public String getDiscAmt() {
		return discAmt;
	}
	public void setDiscAmt(String discAmt) {
		this.discAmt = discAmt;
	}
	public String getDiscAmtStr() {
		return discAmtStr;
	}
	public void setDiscAmtStr(String discAmtStr) {
		this.discAmtStr = discAmtStr;
	}
	public boolean isProcessFlg() {
		return processFlg;
	}
	public void setProcessFlg(boolean processFlg) {
		this.processFlg = processFlg;
	}
	public String getCusNm() {
		return cusNm;
	}
	public void setCusNm(String cusNm) {
		this.cusNm = cusNm;
	}
	public String getDiscFlg() {
		return discFlg;
	}
	public void setDiscFlg(String discFlg) {
		this.discFlg = discFlg;
	}
	public String getDiscDt() {
		return discDt;
	}
	public void setDiscDt(String discDt) {
		this.discDt = discDt;
	}
	
	
	
}
