package com.star.linkage.ar;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;

public class UnappliedDebitBrowseOutput extends BrowseOutput {

	public List<UnappliedDebitInfo> fldTblUnappliedDr = new ArrayList<UnappliedDebitInfo>();
	public BigDecimal chkProof;
	
}
