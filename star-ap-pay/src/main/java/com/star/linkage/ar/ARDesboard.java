package com.star.linkage.ar;

public class ARDesboard {

	private String cmpyId;
	private String arPfx;
	private String arNo;
	private String desc30;
	private String cusId;
	private String cusNm;
	private String dueDt;
	private String aging;
	private String brh;
	private String updRef;
	private String invDt;
	private String origAmt;
	private String ipAmt;
	private String balamt;
	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}
	public String getArPfx() {
		return arPfx;
	}
	public void setArPfx(String arPfx) {
		this.arPfx = arPfx;
	}
	public String getArNo() {
		return arNo;
	}
	public void setArNo(String arNo) {
		this.arNo = arNo;
	}
	public String getDesc30() {
		return desc30;
	}
	public void setDesc30(String desc30) {
		this.desc30 = desc30;
	}
	public String getCusId() {
		return cusId;
	}
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	public String getCusNm() {
		return cusNm;
	}
	public void setCusNm(String cusNm) {
		this.cusNm = cusNm;
	}
	public String getDueDt() {
		return dueDt;
	}
	public void setDueDt(String dueDt) {
		this.dueDt = dueDt;
	}
	public String getAging() {
		return aging;
	}
	public void setAging(String aging) {
		this.aging = aging;
	}
	public String getBrh() {
		return brh;
	}
	public void setBrh(String brh) {
		this.brh = brh;
	}
	public String getUpdRef() {
		return updRef;
	}
	public void setUpdRef(String updRef) {
		this.updRef = updRef;
	}
	public String getInvDt() {
		return invDt;
	}
	public void setInvDt(String invDt) {
		this.invDt = invDt;
	}
	public String getOrigAmt() {
		return origAmt;
	}
	public void setOrigAmt(String origAmt) {
		this.origAmt = origAmt;
	}
	public String getIpAmt() {
		return ipAmt;
	}
	public void setIpAmt(String ipAmt) {
		this.ipAmt = ipAmt;
	}
	public String getBalamt() {
		return balamt;
	}
	public void setBalamt(String balamt) {
		this.balamt = balamt;
	}
	
}
