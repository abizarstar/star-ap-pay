package com.star.linkage.ar;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;

public class GLDistributionBrowseOutput extends BrowseOutput {

	public List<GLDistributionInfo> fldTblGLDist = new ArrayList<GLDistributionInfo>();
	public BigDecimal chkProof;
	
}
