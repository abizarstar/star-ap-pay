package com.star.linkage.ar;

import java.math.BigDecimal;

public class CashReceiptInfo {

	private String cmpyId;
	private String crPfx;
	private String crNo;
	private String cusId;
	private String chkAmt;
	private String chkAmtStr;
	private String chkNo;
	private String desc;
	private String sts;
	private BigDecimal chkProof;

	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}

	public String getCrPfx() {
		return crPfx;
	}
	public void setCrPfx(String crPfx) {
		this.crPfx = crPfx;
	}

	public String getCrNo() {
		return crNo;
	}
	public void setCrNo(String crNo) {
		this.crNo = crNo;
	}
	
	public String getCusId() {
		return cusId;
	}
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
	public String getChkAmt() {
		return chkAmt;
	}
	public void setChkAmt(String chkAmt) {
		this.chkAmt = chkAmt;
	}
	
	public String getChkNo() {
		return chkNo;
	}
	public void setChkNo(String chkNo) {
		this.chkNo = chkNo;
	}
	
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public String getSts() {
		return sts;
	}
	public void setSts(String sts) {
		this.sts = sts;
	}
	
	public BigDecimal getChkProof() {
		return chkProof;
	}
	public void setChkProof(BigDecimal chkProof) {
		this.chkProof = chkProof;
	}
	public String getChkAmtStr() {
		return chkAmtStr;
	}
	public void setChkAmtStr(String chkAmtStr) {
		this.chkAmtStr = chkAmtStr;
	}
	
	
	
}
