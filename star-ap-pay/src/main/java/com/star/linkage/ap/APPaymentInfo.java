package com.star.linkage.ap;

public class APPaymentInfo {

	String cmpyId;
	String refPfx;
	String refNo;
	String refItm;
	String arPfx;
	String arNo;
	String dsAmt;
	String rcvdAmt;
	String updRef;

	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}

	public String getRefPfx() {
		return refPfx;
	}
	public void setRefPfx(String refPfx) {
		this.refPfx = refPfx;
	}
	
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	
	public String getRefItm() {
		return refItm;
	}
	public void setRefItm(String refItm) {
		this.refItm = refItm;
	}
	
	public String getArPfx() {
		return arPfx;
	}
	public void setArPfx(String arPfx) {
		this.arPfx = arPfx;
	}
	
	public String getArNo() {
		return arNo;
	}
	public void setArNo(String arNo) {
		this.arNo = arNo;
	}

	public String getDsAmt() {
		return dsAmt;
	}
	public void setDsAmt(String dsAmt) {
		this.dsAmt = dsAmt;
	}
	
	public String getRcvdAmt() {
		return rcvdAmt;
	}
	public void setRcvdAmt(String rcvdAmt) {
		this.rcvdAmt = rcvdAmt;
	}
	
	public String getUpdRef() {
		return updRef;
	}
	public void setUpdRef(String updRef) {
		this.updRef = updRef;
	}
}
