package com.star.linkage.ap;

import java.util.Date;

public class APType {

	String cmpyId;
	String apPx;
	String apNo;
	String venId;
	String venInvNo;
	String venInvDdt;
	String extlRef;
	String desc30;
	String poPfx;
	String poNo;
	String poItm;
	String cry;
	String exrt;
	String exRtTyp;
	String pttrm;
	String discTrm;
	String dueNbrDy;
	String discNbrDy;
	String discRt;
	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}
	public String getApPx() {
		return apPx;
	}
	public void setApPx(String apPx) {
		this.apPx = apPx;
	}
	public String getApNo() {
		return apNo;
	}
	public void setApNo(String apNo) {
		this.apNo = apNo;
	}
	public String getVenId() {
		return venId;
	}
	public void setVenId(String venId) {
		this.venId = venId;
	}
	public String getVenInvNo() {
		return venInvNo;
	}
	public void setVenInvNo(String venInvNo) {
		this.venInvNo = venInvNo;
	}
	public String getVenInvDdt() {
		return venInvDdt;
	}
	public void setVenInvDdt(String venInvDdt) {
		this.venInvDdt = venInvDdt;
	}
	public String getExtlRef() {
		return extlRef;
	}
	public void setExtlRef(String extlRef) {
		this.extlRef = extlRef;
	}
	public String getDesc30() {
		return desc30;
	}
	public void setDesc30(String desc30) {
		this.desc30 = desc30;
	}
	public String getPoPfx() {
		return poPfx;
	}
	public void setPoPfx(String poPfx) {
		this.poPfx = poPfx;
	}
	public String getPoNo() {
		return poNo;
	}
	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}
	public String getPoItm() {
		return poItm;
	}
	public void setPoItm(String poItm) {
		this.poItm = poItm;
	}
	public String getCry() {
		return cry;
	}
	public void setCry(String cry) {
		this.cry = cry;
	}
	public String getExrt() {
		return exrt;
	}
	public void setExrt(String exrt) {
		this.exrt = exrt;
	}
	public String getExRtTyp() {
		return exRtTyp;
	}
	public void setExRtTyp(String exRtTyp) {
		this.exRtTyp = exRtTyp;
	}
	public String getPttrm() {
		return pttrm;
	}
	public void setPttrm(String pttrm) {
		this.pttrm = pttrm;
	}
	public String getDiscTrm() {
		return discTrm;
	}
	public void setDiscTrm(String discTrm) {
		this.discTrm = discTrm;
	}
	public String getDueNbrDy() {
		return dueNbrDy;
	}
	public void setDueNbrDy(String dueNbrDy) {
		this.dueNbrDy = dueNbrDy;
	}
	public String getDiscNbrDy() {
		return discNbrDy;
	}
	public void setDiscNbrDy(String discNbrDy) {
		this.discNbrDy = discNbrDy;
	}
	public String getDiscRt() {
		return discRt;
	}
	public void setDiscRt(String discRt) {
		this.discRt = discRt;
	}
	public String getDiscDt() {
		return discDt;
	}
	public void setDiscDt(String discDt) {
		this.discDt = discDt;
	}
	public String getDueDt() {
		return dueDt;
	}
	public void setDueDt(String dueDt) {
		this.dueDt = dueDt;
	}
	public String getDiscAmt() {
		return discAmt;
	}
	public void setDiscAmt(String discAmt) {
		this.discAmt = discAmt;
	}
	public String getOpaPfx() {
		return opaPfx;
	}
	public void setOpaPfx(String opaPfx) {
		this.opaPfx = opaPfx;
	}
	public String getOpaNo() {
		return opaNo;
	}
	public void setOpaNo(String opaNo) {
		this.opaNo = opaNo;
	}
	public String getApBrh() {
		return apBrh;
	}
	public void setApBrh(String apBrh) {
		this.apBrh = apBrh;
	}
	public String getEntDt() {
		return entDt;
	}
	public void setEntDt(String entDt) {
		this.entDt = entDt;
	}
	public String getChItmRmk() {
		return chItmRmk;
	}
	public void setChItmRmk(String chItmRmk) {
		this.chItmRmk = chItmRmk;
	}
	public String getPmtTyp() {
		return pmtTyp;
	}
	public void setPmtTyp(String pmtTyp) {
		this.pmtTyp = pmtTyp;
	}
	public String getVchrXrefPfx() {
		return vchrXrefPfx;
	}
	public void setVchrXrefPfx(String vchrXrefPfx) {
		this.vchrXrefPfx = vchrXrefPfx;
	}
	public String getVchrXrefNo() {
		return vchrXrefNo;
	}
	public void setVchrXrefNo(String vchrXrefNo) {
		this.vchrXrefNo = vchrXrefNo;
	}
	public String getOrigAmt() {
		return origAmt;
	}
	public void setOrigAmt(String origAmt) {
		this.origAmt = origAmt;
	}
	public String getIpAmt() {
		return ipAmt;
	}
	public void setIpAmt(String ipAmt) {
		this.ipAmt = ipAmt;
	}
	public String getBalAmt() {
		return balAmt;
	}
	public void setBalAmt(String balAmt) {
		this.balAmt = balAmt;
	}
	public String getArchTrs() {
		return archTrs;
	}
	public void setArchTrs(String archTrs) {
		this.archTrs = archTrs;
	}
	String discDt;
	String dueDt;
	String discAmt;
	String opaPfx;
	String opaNo;
	String apBrh;
	String entDt;
	String chItmRmk;
	String pmtTyp;
	String vchrXrefPfx;
	String vchrXrefNo;
	String origAmt;
	String ipAmt;
	String balAmt;
	String archTrs;
}
