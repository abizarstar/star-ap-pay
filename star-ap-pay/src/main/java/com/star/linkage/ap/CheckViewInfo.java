package com.star.linkage.ap;

public class CheckViewInfo {

	String refPfx;
	String refNo;
	String invNo;
	String pmtAmt;
	String cry;
	String dsbAmt;
	String exrt;
	
	public String getRefPfx() {
		return refPfx;
	}
	public void setRefPfx(String refPfx) {
		this.refPfx = refPfx;
	}
	
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	
	public String getInvNo() {
		return invNo;
	}
	public void setInvNo(String invNo) {
		this.invNo = invNo;
	}
	
	public String getPmtAmt() {
		return pmtAmt;
	}
	public void setPmtAmt(String pmtAmt) {
		this.pmtAmt = pmtAmt;
	}
	
	public String getCry() {
		return cry;
	}
	public void setCry(String cry) {
		this.cry = cry;
	}
	
	public String getDsbAmt() {
		return dsbAmt;
	}
	public void setDsbAmt(String dsbAmt) {
		this.dsbAmt = dsbAmt;
	}
	
	public String getExrt() {
		return exrt;
	}
	public void setExrt(String exrt) {
		this.exrt = exrt;
	}
}
