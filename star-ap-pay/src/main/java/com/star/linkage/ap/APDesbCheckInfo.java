package com.star.linkage.ap;

public class APDesbCheckInfo {

	private String cmpyId;
	private String vndrId;
	private String vndrNm;
	private String invcNo;
	private String invcDt;
	private String dueDt;
	private String chkNo;
	private String po;
	private String vchrPfx;
	private String vchrNo;
	private String brh;
	private String amt;
	private String amtStr;
	private String disAmt;
	private String disAmtStr;
	private String entrDt;
	private String checkNo;
	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}
	public String getVndrId() {
		return vndrId;
	}
	public void setVndrId(String vndrId) {
		this.vndrId = vndrId;
	}
	public String getInvcNo() {
		return invcNo;
	}
	public void setInvcNo(String invcNo) {
		this.invcNo = invcNo;
	}
	public String getInvcDt() {
		return invcDt;
	}
	public void setInvcDt(String invcDt) {
		this.invcDt = invcDt;
	}
	public String getDueDt() {
		return dueDt;
	}
	public void setDueDt(String dueDt) {
		this.dueDt = dueDt;
	}
	public String getPo() {
		return po;
	}
	public void setPo(String po) {
		this.po = po;
	}
	public String getVchrNo() {
		return vchrNo;
	}
	public void setVchrNo(String vchrNo) {
		this.vchrNo = vchrNo;
	}
	public String getBrh() {
		return brh;
	}
	public void setBrh(String brh) {
		this.brh = brh;
	}
	public String getAmt() {
		return amt;
	}
	public void setAmt(String amt) {
		this.amt = amt;
	}
	public String getDisAmt() {
		return disAmt;
	}
	public void setDisAmt(String disAmt) {
		this.disAmt = disAmt;
	}
	public String getEntrDt() {
		return entrDt;
	}
	public void setEntrDt(String entrDt) {
		this.entrDt = entrDt;
	}
	
	public String getCheckNo() {
		return checkNo;
	}
	public void setCheckNo(String checkNo) {
		this.checkNo = checkNo;
	}
	public String getChkNo() {
		return chkNo;
	}
	public void setChkNo(String chkNo) {
		this.chkNo = chkNo;
	}
	public String getVndrNm() {
		return vndrNm;
	}
	public void setVndrNm(String vndrNm) {
		this.vndrNm = vndrNm;
	}
	public String getVchrPfx() {
		return vchrPfx;
	}
	public void setVchrPfx(String vchrPfx) {
		this.vchrPfx = vchrPfx;
	}
	public String getAmtStr() {
		return amtStr;
	}
	public void setAmtStr(String amtStr) {
		this.amtStr = amtStr;
	}
	public String getDisAmtStr() {
		return disAmtStr;
	}
	public void setDisAmtStr(String disAmtStr) {
		this.disAmtStr = disAmtStr;
	}
	
	
	
}
