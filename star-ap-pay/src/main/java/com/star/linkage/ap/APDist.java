package com.star.linkage.ap;

import java.util.Date;

public class APDist {

	String cmpyId;
	String chkBatId;
	String chkSeqNo;
	String chkItm;
	String venInvNo;
	String desc30;
	String venInvDt;
	String opaPfx;
	String opaNo;
	String dsamt;
	String discTknAmt;
	String apCry;
	String pdInAmt;
	String chkItmRmk;
	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}
	public String getChkBatId() {
		return chkBatId;
	}
	public void setChkBatId(String chkBatId) {
		this.chkBatId = chkBatId;
	}
	public String getChkSeqNo() {
		return chkSeqNo;
	}
	public void setChkSeqNo(String chkSeqNo) {
		this.chkSeqNo = chkSeqNo;
	}
	public String getChkItm() {
		return chkItm;
	}
	public void setChkItm(String chkItm) {
		this.chkItm = chkItm;
	}
	public String getVenInvNo() {
		return venInvNo;
	}
	public void setVenInvNo(String venInvNo) {
		this.venInvNo = venInvNo;
	}
	public String getDesc30() {
		return desc30;
	}
	public void setDesc30(String desc30) {
		this.desc30 = desc30;
	}
	public String getVenInvDt() {
		return venInvDt;
	}
	public void setVenInvDt(String venInvDt) {
		this.venInvDt = venInvDt;
	}
	public String getOpaPfx() {
		return opaPfx;
	}
	public void setOpaPfx(String opaPfx) {
		this.opaPfx = opaPfx;
	}
	public String getOpaNo() {
		return opaNo;
	}
	public void setOpaNo(String opaNo) {
		this.opaNo = opaNo;
	}
	public String getDsamt() {
		return dsamt;
	}
	public void setDsamt(String dsamt) {
		this.dsamt = dsamt;
	}
	public String getDiscTknAmt() {
		return discTknAmt;
	}
	public void setDiscTknAmt(String discTknAmt) {
		this.discTknAmt = discTknAmt;
	}
	public String getApCry() {
		return apCry;
	}
	public void setApCry(String apCry) {
		this.apCry = apCry;
	}
	public String getPdInAmt() {
		return pdInAmt;
	}
	public void setPdInAmt(String pdInAmt) {
		this.pdInAmt = pdInAmt;
	}
	public String getChkItmRmk() {
		return chkItmRmk;
	}
	public void setChkItmRmk(String chkItmRmk) {
		this.chkItmRmk = chkItmRmk;
	}
	public String getApPfx() {
		return apPfx;
	}
	public void setApPfx(String apPfx) {
		this.apPfx = apPfx;
	}
	public String getApNo() {
		return apNo;
	}
	public void setApNo(String apNo) {
		this.apNo = apNo;
	}
	public String getApItm() {
		return apItm;
	}
	public void setApItm(String apItm) {
		this.apItm = apItm;
	}
	String apPfx;
	String apNo;
	String apItm;
	 }
