package com.star.linkage.ap;

public class OpenItemInfo {

	String dueDt;
	String invDt;
	String refPfx;
	String refNo;
	String invNo;
	String pmtTyp;
	String brh;
	String pmtStsActn;
	String discDt;
	String cry;
	String discAmt;
	String balAmt;
	String ipAmt;
	String inPrsBal;
	
	public String getDueDt() {
		return dueDt;
	}
	public void setDueDt(String dueDt) {
		this.dueDt = dueDt;
	}

	public String getInvDt() {
		return invDt;
	}
	public void setInvDt(String invDt) {
		this.invDt = invDt;
	}
	
	public String getRefPfx() {
		return refPfx;
	}
	public void setRefPfx(String refPfx) {
		this.refPfx = refPfx;
	}
	
	public String getRefNo() {
		return refNo;
	}
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}
	
	public String getInvNo() {
		return invNo;
	}
	public void setInvNo(String invNo) {
		this.invNo = invNo;
	}
	
	public String getPmtTyp() {
		return pmtTyp;
	}
	public void setPmtTyp(String pmtTyp) {
		this.pmtTyp = pmtTyp;
	}
	
	public String getBrh() {
		return brh;
	}
	public void setBrh(String brh) {
		this.brh = brh;
	}
	
	public String getPmtStsActn() {
		return pmtStsActn;
	}
	public void setPmtStsActn(String pmtStsActn) {
		this.pmtStsActn = pmtStsActn;
	}
	
	public String getDiscDt() {
		return discDt;
	}
	public void setDiscDt(String discDt) {
		this.discDt = discDt;
	}
	
	public String getCry() {
		return cry;
	}
	public void setCry(String cry) {
		this.cry = cry;
	}
	
	public String getDiscAmt() {
		return discAmt;
	}
	public void setDiscAmt(String discAmt) {
		this.discAmt = discAmt;
	}
	
	public String getBalAmt() {
		return balAmt;
	}
	public void setBalAmt(String balAmt) {
		this.balAmt = balAmt;
	}
	
	public String getIpAmt() {
		return ipAmt;
	}
	public void setIpAmt(String ipAmt) {
		this.ipAmt = ipAmt;
	}
	
	public String getInPrsBal() {
		return inPrsBal;
	}
	public void setInPrsBal(String inPrsBal) {
		this.inPrsBal = inPrsBal;
	}
}
