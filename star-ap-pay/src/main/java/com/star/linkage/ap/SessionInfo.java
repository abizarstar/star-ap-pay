package com.star.linkage.ap;

import java.util.Date;

public class SessionInfo {

	String cmpyId;
	String ssnId;
	String brh;
	String jrnlDt;
	String lgnNm;
	String bnk;
	String totNbrChk;
	String totDepAmt;
	String sts;

	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}

	public String getSsnId() {
		return ssnId;
	}
	public void setSsnId(String ssnId) {
		this.ssnId = ssnId;
	}
	
	public String getBrh() {
		return brh;
	}
	public void setBrh(String brh) {
		this.brh = brh;
	}
	
	public String getJrnlDt() {
		return jrnlDt;
	}
	public void setJrnlDt(String jrnlDt) {
		this.jrnlDt = jrnlDt;
	}
	
	public String getLgnNm() {
		return lgnNm;
	}
	public void setLgnNm(String lgnNm) {
		this.lgnNm = lgnNm;
	}
	
	public String getBnk() {
		return bnk;
	}
	public void setBnk(String bnk) {
		this.bnk = bnk;
	}

	public String getTotNbrChk() {
		return totNbrChk;
	}
	public void setTotNbrChk(String totNbrChk) {
		this.totNbrChk = totNbrChk;
	}
	
	public String getTotDepAmt() {
		return totDepAmt;
	}
	public void setTotDepAmt(String totDepAmt) {
		this.totDepAmt = totDepAmt;
	}
	
	public String getSts() {
		return sts;
	}
	public void setSts(String sts) {
		this.sts = sts;
	}
}
