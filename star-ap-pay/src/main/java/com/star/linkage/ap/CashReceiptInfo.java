package com.star.linkage.ap;

import java.util.Date;

public class CashReceiptInfo {

	String cmpyId;
	String crPfx;
	String crNo;
	String cusId;
	String chkAmt;
	String chkNo;
	String desc;
	String sts;

	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}

	public String getCrPfx() {
		return crPfx;
	}
	public void setCrPfx(String crPfx) {
		this.crPfx = crPfx;
	}

	public String getCrNo() {
		return crNo;
	}
	public void setCrNo(String crNo) {
		this.crNo = crNo;
	}
	
	public String getCusId() {
		return cusId;
	}
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	
	public String getChkAmt() {
		return chkAmt;
	}
	public void setChkAmt(String chkAmt) {
		this.chkAmt = chkAmt;
	}
	
	public String getChkNo() {
		return chkNo;
	}
	public void setChkNo(String chkNo) {
		this.chkNo = chkNo;
	}
	
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public String getSts() {
		return sts;
	}
	public void setSts(String sts) {
		this.sts = sts;
	}
}
