package com.star.linkage.ap;

import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;

public class GLDistributionBrowseOutput extends BrowseOutput {

	public List<GLDistributionInfo> fldTblGLDist = new ArrayList<GLDistributionInfo>();
	
}
