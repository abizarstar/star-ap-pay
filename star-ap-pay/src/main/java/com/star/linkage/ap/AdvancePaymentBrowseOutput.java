package com.star.linkage.ap;

import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;

public class AdvancePaymentBrowseOutput extends BrowseOutput {

	public List<AdvancePaymentInfo> fldTblAdvPmt = new ArrayList<AdvancePaymentInfo>();
	
}
