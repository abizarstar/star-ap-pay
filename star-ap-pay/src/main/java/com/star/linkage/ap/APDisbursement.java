package com.star.linkage.ap;

public class APDisbursement {

	private String cmpyId;
	private String dsbPfx;
	private String dsbNo;
	private String ssnId;
	private String venId;
	private String venNm;
	private String pmtType;
	private String venChkNo;
	private String venChkDt;
	private String venChkAmt;
	private String bnkPmtRef;
	private String disbSts;
	
	public String getSsnId() {
		return ssnId;
	}
	public void setSsnId(String ssnId) {
		this.ssnId = ssnId;
	}public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}
	public String getDsbPfx() {
		return dsbPfx;
	}
	public void setDsbPfx(String dsbPfx) {
		this.dsbPfx = dsbPfx;
	}
	public String getDsbNo() {
		return dsbNo;
	}
	public void setDsbNo(String dsbNo) {
		this.dsbNo = dsbNo;
	}
	public String getVenId() {
		return venId;
	}
	public void setVenId(String venId) {
		this.venId = venId;
	}
	public String getPmtType() {
		return pmtType;
	}
	public void setPmtType(String pmtType) {
		this.pmtType = pmtType;
	}
	public String getVenChkNo() {
		return venChkNo;
	}
	public void setVenChkNo(String venChkNo) {
		this.venChkNo = venChkNo;
	}
	public String getVenChkDt() {
		return venChkDt;
	}
	public void setVenChkDt(String venChkDt) {
		this.venChkDt = venChkDt;
	}
	public String getVenChkAmt() {
		return venChkAmt;
	}
	public void setVenChkAmt(String venChkAmt) {
		this.venChkAmt = venChkAmt;
	}
	public String getBnkPmtRef() {
		return bnkPmtRef;
	}
	public void setBnkPmtRef(String bnkPmtRef) {
		this.bnkPmtRef = bnkPmtRef;
	}
	public String getVenNm() {
		return venNm;
	}
	public void setVenNm(String venNm) {
		this.venNm = venNm;
	}
	public String getDisbSts() {
		return disbSts;
	}
	public void setDisbSts(String disbSts) {
		this.disbSts = disbSts;
	}
	
	
	
}
