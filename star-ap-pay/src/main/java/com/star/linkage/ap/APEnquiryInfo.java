package com.star.linkage.ap;

public class APEnquiryInfo {

	private String cmpyId;
	private String ssnId;
	private String dsbBrh;
	private String jrnlDt;
	private String lgnId;
	private String dssBnk;
	private String dsbCry;
	private String dsbExrt;
	private String apCry1;
	private String apXexrt1;
	private String apCry2;
	private String apXexrt2;
	private String apCry3;
	private String apXexrt3;
	private int disbNo;
	private String disbAmt;
	
	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}
	public String getSsnId() {
		return ssnId;
	}
	public void setSsnId(String ssnId) {
		this.ssnId = ssnId;
	}
	public String getDsbBrh() {
		return dsbBrh;
	}
	public void setDsbBrh(String dsbBrh) {
		this.dsbBrh = dsbBrh;
	}
	public String getJrnlDt() {
		return jrnlDt;
	}
	public void setJrnlDt(String jrnlDt) {
		this.jrnlDt = jrnlDt;
	}
	public String getLgnId() {
		return lgnId;
	}
	public void setLgnId(String lgnId) {
		this.lgnId = lgnId;
	}
	public String getDssBnk() {
		return dssBnk;
	}
	public void setDssBnk(String dssBnk) {
		this.dssBnk = dssBnk;
	}
	public String getDsbCry() {
		return dsbCry;
	}
	public void setDsbCry(String dsbCry) {
		this.dsbCry = dsbCry;
	}
	public String getDsbExrt() {
		return dsbExrt;
	}
	public void setDsbExrt(String dsbExrt) {
		this.dsbExrt = dsbExrt;
	}
	public String getApCry1() {
		return apCry1;
	}
	public void setApCry1(String apCry1) {
		this.apCry1 = apCry1;
	}
	public String getApXexrt1() {
		return apXexrt1;
	}
	public void setApXexrt1(String apXexrt1) {
		this.apXexrt1 = apXexrt1;
	}
	public String getApCry2() {
		return apCry2;
	}
	public void setApCry2(String apCry2) {
		this.apCry2 = apCry2;
	}
	public String getApXexrt2() {
		return apXexrt2;
	}
	public void setApXexrt2(String apXexrt2) {
		this.apXexrt2 = apXexrt2;
	}
	public String getApCry3() {
		return apCry3;
	}
	public void setApCry3(String apCry3) {
		this.apCry3 = apCry3;
	}
	public String getApXexrt3() {
		return apXexrt3;
	}
	public void setApXexrt3(String apXexrt3) {
		this.apXexrt3 = apXexrt3;
	}
	public int getDisbNo() {
		return disbNo;
	}
	public void setDisbNo(int disbNo) {
		this.disbNo = disbNo;
	}
	public String getDisbAmt() {
		return disbAmt;
	}
	public void setDisbAmt(String disbAmt) {
		this.disbAmt = disbAmt;
	}

	
	
	
}
