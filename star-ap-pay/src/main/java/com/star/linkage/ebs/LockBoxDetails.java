package com.star.linkage.ebs;

import java.util.ArrayList;
import java.util.List;

public class LockBoxDetails {

	private List<LockBoxHdrInfo> hdrInfoLst = new ArrayList<LockBoxHdrInfo>();
	private List<LockBoxTrnInfo> trnInfoLst = new ArrayList<LockBoxTrnInfo>();
	public List<LockBoxHdrInfo> getHdrInfoLst() {
		return hdrInfoLst;
	}
	public void setHdrInfoLst(List<LockBoxHdrInfo> hdrInfoLst) {
		this.hdrInfoLst = hdrInfoLst;
	}
	public List<LockBoxTrnInfo> getTrnInfoLst() {
		return trnInfoLst;
	}
	public void setTrnInfoLst(List<LockBoxTrnInfo> trnInfoLst) {
		this.trnInfoLst = trnInfoLst;
	}
	
	
	
}
