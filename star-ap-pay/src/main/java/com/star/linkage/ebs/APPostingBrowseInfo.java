package com.star.linkage.ebs;

import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;
import com.star.linkage.ap.GLDistributionInfo;
import com.star.linkage.cstmdocinfo.VchrInfo;

public class APPostingBrowseInfo extends BrowseOutput{

	public List<GLDistributionInfo> fldTblGlDist = new ArrayList<GLDistributionInfo>();
	public List<VchrInfo> fldTblVchrInfo = new ArrayList<VchrInfo>();
	public String entryDt;
	public String narrDesc;
}
