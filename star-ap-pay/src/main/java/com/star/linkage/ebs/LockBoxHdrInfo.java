package com.star.linkage.ebs;

import java.util.ArrayList;
import java.util.List;

public class LockBoxHdrInfo{

	private String batchId;
	private String lockBoxNo;
	private String dpstDt;
	private int rcrdCount;
	private double batchTot;
	private String batchTotStr;
	private String upldOnStr;
	private String crtdDttsStr;
	private int id;
	private int ebsUpldId;
	private List<LockBoxTrnInfo> listTrn = new ArrayList<LockBoxTrnInfo>();
	
	public String getBatchId() {
		return batchId;
	}
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}
	public String getLockBoxNo() {
		return lockBoxNo;
	}
	public void setLockBoxNo(String lockBoxNo) {
		this.lockBoxNo = lockBoxNo;
	}
	public String getDpstDt() {
		return dpstDt;
	}
	public void setDpstDt(String dpstDt) {
		this.dpstDt = dpstDt;
	}
	public int getRcrdCount() {
		return rcrdCount;
	}
	public void setRcrdCount(int rcrdCount) {
		this.rcrdCount = rcrdCount;
	}
	public double getBatchTot() {
		return batchTot;
	}
	public void setBatchTot(double batchTot) {
		this.batchTot = batchTot;
	}
	public String getUpldOnStr() {
		return upldOnStr;
	}
	public void setUpldOnStr(String upldOnStr) {
		this.upldOnStr = upldOnStr;
	}
	public String getCrtdDttsStr() {
		return crtdDttsStr;
	}
	public void setCrtdDttsStr(String crtdDttsStr) {
		this.crtdDttsStr = crtdDttsStr;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getEbsUpldId() {
		return ebsUpldId;
	}
	public void setEbsUpldId(int ebsUpldId) {
		this.ebsUpldId = ebsUpldId;
	}
	public List<LockBoxTrnInfo> getListTrn() {
		return listTrn;
	}
	public void setListTrn(List<LockBoxTrnInfo> listTrn) {
		this.listTrn = listTrn;
	}
	public String getBatchTotStr() {
		return batchTotStr;
	}
	public void setBatchTotStr(String batchTotStr) {
		this.batchTotStr = batchTotStr;
	}
	
	
}
