package com.star.linkage.ebs;

import java.util.Date;

public class InvoicePmntInfo {

	private String reqId;
	private String invNo;
	private String chkNo;
	private String glEntrySts;
	private String pmntDt;
	private Date pmntDtVal;
	public String getReqId() {
		return reqId;
	}
	public void setReqId(String reqId) {
		this.reqId = reqId;
	}
	public String getInvNo() {
		return invNo;
	}
	public void setInvNo(String invNo) {
		this.invNo = invNo;
	}
	public String getChkNo() {
		return chkNo;
	}
	public void setChkNo(String chkNo) {
		this.chkNo = chkNo;
	}
	public String getGlEntrySts() {
		return glEntrySts;
	}
	public void setGlEntrySts(String glEntrySts) {
		this.glEntrySts = glEntrySts;
	}
	public String getPmntDt() {
		return pmntDt;
	}
	public void setPmntDt(String pmntDt) {
		this.pmntDt = pmntDt;
	}
	public Date getPmntDtVal() {
		return pmntDtVal;
	}
	public void setPmntDtVal(Date pmntDtVal) {
		this.pmntDtVal = pmntDtVal;
	}
	
	
}
