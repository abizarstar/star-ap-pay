package com.star.linkage.ebs;

import java.util.ArrayList;
import java.util.List;

public class BankStmtDetails {

	private int upldId;
	private String crtdDt = "";
	private String crtdTime = "";
	private String stmtUpldDt = "";
	private String stmtUpldTime = "";
	private List<BankHdrDtls> bnkHdrList = new ArrayList<BankHdrDtls>();
	
	public String getCrtdDt() {
		return crtdDt;
	}
	public void setCrtdDt(String crtdDt) {
		this.crtdDt = crtdDt;
	}
	public String getCrtdTime() {
		return crtdTime;
	}
	public void setCrtdTime(String crtdTime) {
		this.crtdTime = crtdTime;
	}
	public List<BankHdrDtls> getBnkHdrList() {
		return bnkHdrList;
	}
	public void setBnkHdrList(List<BankHdrDtls> bnkHdrList) {
		this.bnkHdrList = bnkHdrList;
	}
	public int getUpldId() {
		return upldId;
	}
	public void setUpldId(int upldId) {
		this.upldId = upldId;
	}
	public String getStmtUpldDt() {
		return stmtUpldDt;
	}
	public void setStmtUpldDt(String stmtUpldDt) {
		this.stmtUpldDt = stmtUpldDt;
	}
	public String getStmtUpldTime() {
		return stmtUpldTime;
	}
	public void setStmtUpldTime(String stmtUpldTime) {
		this.stmtUpldTime = stmtUpldTime;
	}
	
	
	
	
	
}
