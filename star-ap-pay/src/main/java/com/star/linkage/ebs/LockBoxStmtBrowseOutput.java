package com.star.linkage.ebs;

import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;

public class LockBoxStmtBrowseOutput extends BrowseOutput{

	public List<LockBoxHdrInfo> fldTblLckBxHdr = new ArrayList<LockBoxHdrInfo>();
}
