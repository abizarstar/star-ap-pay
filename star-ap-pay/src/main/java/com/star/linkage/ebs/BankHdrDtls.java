package com.star.linkage.ebs;

import java.util.ArrayList;
import java.util.List;

public class BankHdrDtls {

	private int hdrId;
	public int getHdrId() {
		return hdrId;
	}
	public void setHdrId(int hdrId) {
		this.hdrId = hdrId;
	}
	private String opnBal = "0";
	private String cloBal = "0";
	private String avlblClsBal = "0";
	private String totCreditTrn = "0";
	private String totDebitTrn = "0";
	private String crdTrnCount = "0";
	private String debTrnCount = "0";
	private String rcvrId = "";
	private List<BankTrnDetails> listBnkTrn = new ArrayList<BankTrnDetails>();
	
	public String getOpnBal() {
		return opnBal;
	}
	public void setOpnBal(String opnBal) {
		this.opnBal = opnBal;
	}
	public String getCloBal() {
		return cloBal;
	}
	public void setCloBal(String cloBal) {
		this.cloBal = cloBal;
	}
	public String getAvlblClsBal() {
		return avlblClsBal;
	}
	public void setAvlblClsBal(String avlblClsBal) {
		this.avlblClsBal = avlblClsBal;
	}
	public String getTotCreditTrn() {
		return totCreditTrn;
	}
	public void setTotCreditTrn(String totCreditTrn) {
		this.totCreditTrn = totCreditTrn;
	}
	public String getTotDebitTrn() {
		return totDebitTrn;
	}
	public void setTotDebitTrn(String totDebitTrn) {
		this.totDebitTrn = totDebitTrn;
	}
	public String getCrdTrnCount() {
		return crdTrnCount;
	}
	public void setCrdTrnCount(String crdTrnCount) {
		this.crdTrnCount = crdTrnCount;
	}
	public String getDebTrnCount() {
		return debTrnCount;
	}
	public void setDebTrnCount(String debTrnCount) {
		this.debTrnCount = debTrnCount;
	}
	
	public List<BankTrnDetails> getListBnkTrn() {
		return listBnkTrn;
	}
	public void setListBnkTrn(List<BankTrnDetails> listBnkTrn) {
		this.listBnkTrn = listBnkTrn;
	}
	public String getRcvrId() {
		return rcvrId;
	}
	public void setRcvrId(String rcvrId) {
		this.rcvrId = rcvrId;
	}
	
	
}
