package com.star.linkage.ebs;

public class BankTrnDetails {

	private String typeCode;
	private String trnAMount;
	private String bnkRef;
	private String custRef;
	private String adtnRef;
	private int id;
	
	
	public String getTypeCode() {
		return typeCode;
	}
	public void setTypeCode(String typeCode) {
		this.typeCode = typeCode;
	}
	public String getTrnAMount() {
		return trnAMount;
	}
	public void setTrnAMount(String trnAMount) {
		this.trnAMount = trnAMount;
	}
	public String getBnkRef() {
		return bnkRef;
	}
	public void setBnkRef(String bnkRef) {
		this.bnkRef = bnkRef;
	}
	public String getCustRef() {
		return custRef;
	}
	public void setCustRef(String custRef) {
		this.custRef = custRef;
	}
	public String getAdtnRef() {
		return adtnRef;
	}
	public void setAdtnRef(String adtnRef) {
		this.adtnRef = adtnRef;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
	
	
}
