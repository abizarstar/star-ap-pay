package com.star.linkage.ebs;

public class BankStmtTrnOutput extends BankStmtHdrOutput{

	private String typCode;
	private String typNm;
	private String trnAmount;
	private String trnType;
	private Boolean isPrs;
	private String bnkRef;
	private String custRef;
	private String adtnRef;
	private String erpRef;
	private String errMsg;	
	private String id;
	private String typCdTrnTyp;
	private String bnkCode;
	private String appRef;
	private String trnSts;
	private String post1Ref;
	private String post2Ref;
	
	
	
	public String getTypCode() {
		return typCode;
	}
	public void setTypCode(String typCode) {
		this.typCode = typCode;
	}
	public String getTypNm() {
		return typNm;
	}
	public void setTypNm(String typNm) {
		this.typNm = typNm;
	}
	public String getTrnAmount() {
		return trnAmount;
	}
	public void setTrnAmount(String trnAmount) {
		this.trnAmount = trnAmount;
	}
	public String getTrnType() {
		return trnType;
	}
	public void setTrnType(String trnType) {
		this.trnType = trnType;
	}
	public Boolean getIsPrs() {
		return isPrs;
	}
	public void setIsPrs(Boolean isPrs) {
		this.isPrs = isPrs;
	}
	
	
	
	public String getBnkRef() {
		return bnkRef;
	}
	public void setBnkRef(String bnkRef) {
		this.bnkRef = bnkRef;
	}
	public String getCustRef() {
		return custRef;
	}
	public void setCustRef(String custRef) {
		this.custRef = custRef;
	}
	public String getAdtnRef() {
		return adtnRef;
	}
	public void setAdtnRef(String adtnRef) {
		this.adtnRef = adtnRef;
	}
	public String getErpRef() {
		return erpRef;
	}
	public void setErpRef(String erpRef) {
		this.erpRef = erpRef;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTypCdTrnTyp() {
		return typCdTrnTyp;
	}
	public void setTypCdTrnTyp(String typCdTrnTyp) {
		this.typCdTrnTyp = typCdTrnTyp;
	}
	public String getBnkCode() {
		return bnkCode;
	}
	public void setBnkCode(String bnkCode) {
		this.bnkCode = bnkCode;
	}
	public String getAppRef() {
		return appRef;
	}
	public void setAppRef(String appRef) {
		this.appRef = appRef;
	}
	public String getTrnSts() {
		return trnSts;
	}
	public void setTrnSts(String trnSts) {
		this.trnSts = trnSts;
	}
	public String getPost1Ref() {
		return post1Ref;
	}
	public void setPost1Ref(String post1Ref) {
		this.post1Ref = post1Ref;
	}
	public String getPost2Ref() {
		return post2Ref;
	}
	public void setPost2Ref(String post2Ref) {
		this.post2Ref = post2Ref;
	}
	
	
}
