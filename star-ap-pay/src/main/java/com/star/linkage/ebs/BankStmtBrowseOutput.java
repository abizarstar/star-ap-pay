package com.star.linkage.ebs;

import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;

public class BankStmtBrowseOutput extends BrowseOutput {

	public List<BankStmtHdrOutput> fldTblBnkTrn = new ArrayList<BankStmtHdrOutput>();
	
}
