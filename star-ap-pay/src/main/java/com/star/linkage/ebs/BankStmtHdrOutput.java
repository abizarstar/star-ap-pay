package com.star.linkage.ebs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class BankStmtHdrOutput {

	private Date upldOn;
	private Date crtdDtts;
	private String upldOnStr;
	private String crtdDttsStr;
	private String totCrdTrn;
	private String crdTrnCount;
	private String totDebTrn;
	private String debTrnCount;
	private String rcvrId;
	private int hdrId;
	private int upldId;
	private List<BankStmtTrnOutput> listTrn = new ArrayList<BankStmtTrnOutput>();
	public Date getUpldOn() {
		return upldOn;
	}
	public void setUpldOn(Date upldOn) {
		this.upldOn = upldOn;
	}
	public Date getCrtdDtts() {
		return crtdDtts;
	}
	public void setCrtdDtts(Date crtdDtts) {
		this.crtdDtts = crtdDtts;
	}
	public String getTotCrdTrn() {
		return totCrdTrn;
	}
	public void setTotCrdTrn(String totCrdTrn) {
		this.totCrdTrn = totCrdTrn;
	}
	public String getCrdTrnCount() {
		return crdTrnCount;
	}
	public void setCrdTrnCount(String crdTrnCount) {
		this.crdTrnCount = crdTrnCount;
	}
	public String getTotDebTrn() {
		return totDebTrn;
	}
	public void setTotDebTrn(String totDebTrn) {
		this.totDebTrn = totDebTrn;
	}
	public String getDebTrnCount() {
		return debTrnCount;
	}
	public void setDebTrnCount(String debTrnCount) {
		this.debTrnCount = debTrnCount;
	}
	public List<BankStmtTrnOutput> getListTrn() {
		return listTrn;
	}
	public void setListTrn(List<BankStmtTrnOutput> listTrn) {
		this.listTrn = listTrn;
	}
	public String getUpldOnStr() {
		return upldOnStr;
	}
	public void setUpldOnStr(String upldOnStr) {
		this.upldOnStr = upldOnStr;
	}
	public String getCrtdDttsStr() {
		return crtdDttsStr;
	}
	public void setCrtdDttsStr(String crtdDttsStr) {
		this.crtdDttsStr = crtdDttsStr;
	}
	public String getRcvrId() {
		return rcvrId;
	}
	public void setRcvrId(String rcvrId) {
		this.rcvrId = rcvrId;
	}
	public int getHdrId() {
		return hdrId;
	}
	public void setHdrId(int hdrId) {
		this.hdrId = hdrId;
	}
	public int getUpldId() {
		return upldId;
	}
	public void setUpldId(int upldId) {
		this.upldId = upldId;
	}
	
	
	
	
}
