package com.star.linkage.ebs;

public class LockBoxTrnInfo{

	private String seqNo;
	private String chkNo;
	private String batchId;
	private String amount;
	private String amountStr;
	private String invNo;
	private int id;
	private int ebsUpldId;
	private String acctNo;
	private String erpRef;
	private String errMsg;
	private boolean isPrs;
	private String bnkCode;
	private String appRef;
	private String trnSts;
	private String customer;
	private String post1Ref;
	
	
	public String getSeqNo() {
		return seqNo;
	}
	public void setSeqNo(String seqNo) {
		this.seqNo = seqNo;
	}
	public String getChkNo() {
		return chkNo;
	}
	public void setChkNo(String chkNo) {
		this.chkNo = chkNo;
	}
	public String getBatchId() {
		return batchId;
	}
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getInvNo() {
		return invNo;
	}
	public void setInvNo(String invNo) {
		this.invNo = invNo;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getEbsUpldId() {
		return ebsUpldId;
	}
	public void setEbsUpldId(int ebsUpldId) {
		this.ebsUpldId = ebsUpldId;
	}
	public String getAcctNo() {
		return acctNo;
	}
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}
	public String getErpRef() {
		return erpRef;
	}
	public void setErpRef(String erpRef) {
		this.erpRef = erpRef;
	}
	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	public boolean isPrs() {
		return isPrs;
	}
	public void setPrs(boolean isPrs) {
		this.isPrs = isPrs;
	}
	public String getBnkCode() {
		return bnkCode;
	}
	public void setBnkCode(String bnkCode) {
		this.bnkCode = bnkCode;
	}
	public String getAppRef() {
		return appRef;
	}
	public void setAppRef(String appRef) {
		this.appRef = appRef;
	}
	public String getTrnSts() {
		return trnSts;
	}
	public void setTrnSts(String trnSts) {
		this.trnSts = trnSts;
	}
	public String getAmountStr() {
		return amountStr;
	}
	public void setAmountStr(String amountStr) {
		this.amountStr = amountStr;
	}
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getPost1Ref() {
		return post1Ref;
	}
	public void setPost1Ref(String post1Ref) {
		this.post1Ref = post1Ref;
	}
	
	
	
	
}
