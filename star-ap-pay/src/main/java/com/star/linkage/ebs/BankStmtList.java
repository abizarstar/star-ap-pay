package com.star.linkage.ebs;

public class BankStmtList {

	private String flNm;
	private String dtTm;
	private int flg ;
	private String flType;
	public String getFlNm() {
		return flNm;
	}
	public void setFlNm(String flNm) {
		this.flNm = flNm;
	}
	public String getDtTm() {
		return dtTm;
	}
	public void setDtTm(String dtTm) {
		this.dtTm = dtTm;
	}
	public int getFlg() {
		return flg;
	}
	public void setFlg(int flg) {
		this.flg = flg;
	}
	public String getFlType() {
		return flType;
	}
	public void setFlType(String flType) {
		this.flType = flType;
	}
	
	
}
