package com.star.linkage.stmt;

public class BnkStmtInput {

	String flNm;
	String flOrgNm;
	String flExt;
	int id;
	
	
	
	public String getFlNm() {
		return flNm;
	}
	public void setFlNm(String flNm) {
		this.flNm = flNm;
	}
	public String getFlOrgNm() {
		return flOrgNm;
	}
	public void setFlOrgNm(String flOrgNm) {
		this.flOrgNm = flOrgNm;
	}
	public String getFlExt() {
		return flExt;
	}
	public void setFlExt(String flExt) {
		this.flExt = flExt;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	
}
