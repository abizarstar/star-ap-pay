package com.star.linkage.stmt;

public class DocumentNm {

	String docName;
	String docOrgName;
	String docExtn;
	public String getDocName() {
		return docName;
	}
	public void setDocName(String docName) {
		this.docName = docName;
	}
	public String getDocOrgName() {
		return docOrgName;
	}
	public void setDocOrgName(String docOrgName) {
		this.docOrgName = docOrgName;
	}
	public String getDocExtn() {
		return docExtn;
	}
	public void setDocExtn(String docExtn) {
		this.docExtn = docExtn;
	}
	
	
}
