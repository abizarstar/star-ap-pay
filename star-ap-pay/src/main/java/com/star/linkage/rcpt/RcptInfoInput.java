package com.star.linkage.rcpt;

import java.math.BigDecimal;

import com.star.linkage.auth.UserDetails;

public class RcptInfoInput extends UserDetails {

	int ssnId;
	String cashRctId;
	String cusId;
	BigDecimal cusChkAmt;
	String cusChkNo;
	String cusRctDesc;
	String host;
	String port;
	
	public int getSsnId() {
		return ssnId;
	}
	public void setSsnId(int ssnId) {
		this.ssnId = ssnId;
	}
	public String getCashRctId() {
		return cashRctId;
	}
	public void setCashRctId(String cashRctId) {
		this.cashRctId = cashRctId;
	}
	public String getCusId() {
		return cusId;
	}
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	public BigDecimal getCusChkAmt() {
		return cusChkAmt;
	}
	public void setCusChkAmt(BigDecimal cusChkAmt) {
		this.cusChkAmt = cusChkAmt;
	}
	public String getCusChkNo() {
		return cusChkNo;
	}
	public void setCusChkNo(String cusChkNo) {
		this.cusChkNo = cusChkNo;
	}
	public String getCusRctDesc() {
		return cusRctDesc;
	}
	public void setCusRctDesc(String cusRctDesc) {
		this.cusRctDesc = cusRctDesc;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	
	
	
}
