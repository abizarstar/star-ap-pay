package com.star.linkage.rcpt;

import com.star.common.MaintenanceOutput;

public class RcptMaintenanceOutput extends MaintenanceOutput{

	public String authToken;
	public int ssnId;
	public String rcptId;
	public int refId;
}
