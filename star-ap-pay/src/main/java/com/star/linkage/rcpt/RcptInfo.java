package com.star.linkage.rcpt;

public class RcptInfo {

	private String invNo;
	private String brh;
	private String cusNo;
	private String amount;
	private String discAmt;
	private String updRef;
	private String dueDt;
	private String discFlg;
	
	
	public String getInvNo() {
		return invNo;
	}
	public void setInvNo(String invNo) {
		this.invNo = invNo;
	}
	public String getBrh() {
		return brh;
	}
	public void setBrh(String brh) {
		this.brh = brh;
	}
	public String getCusNo() {
		return cusNo;
	}
	public void setCusNo(String cusNo) {
		this.cusNo = cusNo;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getUpdRef() {
		return updRef;
	}
	public void setUpdRef(String updRef) {
		this.updRef = updRef;
	}
	public String getDiscAmt() {
		return discAmt;
	}
	public void setDiscAmt(String discAmt) {
		this.discAmt = discAmt;
	}
	public String getDueDt() {
		return dueDt;
	}
	public void setDueDt(String dueDt) {
		this.dueDt = dueDt;
	}
	public String getDiscFlg() {
		return discFlg;
	}
	public void setDiscFlg(String discFlg) {
		this.discFlg = discFlg;
	}
	
	
	
}
