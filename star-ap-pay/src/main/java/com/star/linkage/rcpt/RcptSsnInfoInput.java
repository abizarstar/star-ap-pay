package com.star.linkage.rcpt;

import java.math.BigDecimal;

import com.star.linkage.auth.UserDetails;

public class RcptSsnInfoInput extends UserDetails {

	String host;
	String port;
	String bankId;
	String branchId;
	BigDecimal depositAmount;
	
	int expectedChecks;
	
	
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getBankId() {
		return bankId;
	}
	public void setBankId(String bankId) {
		this.bankId = bankId;
	}
	public String getBranchId() {
		return branchId;
	}
	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}
	public BigDecimal getDepositAmount() {
		return depositAmount;
	}
	public void setDepositAmount(BigDecimal depositAmount) {
		this.depositAmount = depositAmount;
	}
	public int getExpectedChecks() {
		return expectedChecks;
	}
	public void setExpectedChecks(int expectedChecks) {
		this.expectedChecks = expectedChecks;
	}
	
	
	
	
}
