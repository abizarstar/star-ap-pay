package com.star.linkage.cstmdocinfo;

import java.util.Date;

public class CstmDocAddInfoInput {

	private int ctlNo;
	private int docCtlNo;
	private String flNm;
	private String flOrgName;
	private Date crtDtts;
	private String upldBy;
	private String vchrNo;
	
	public int getCtlNo() {
		return ctlNo;
	}
	public void setCtlNo(int ctlNo) {
		this.ctlNo = ctlNo;
	}
	public int getDocCtlNo() {
		return docCtlNo;
	}
	public void setDocCtlNo(int docCtlNo) {
		this.docCtlNo = docCtlNo;
	}
	public String getFlNm() {
		return flNm;
	}
	public void setFlNm(String flNm) {
		this.flNm = flNm;
	}
	public String getFlOrgName() {
		return flOrgName;
	}
	public void setFlOrgName(String flOrgName) {
		this.flOrgName = flOrgName;
	}
	public Date getCrtDtts() {
		return crtDtts;
	}
	public void setCrtDtts(Date crtDtts) {
		this.crtDtts = crtDtts;
	}
	public String getUpldBy() {
		return upldBy;
	}
	public void setUpldBy(String upldBy) {
		this.upldBy = upldBy;
	}
	public String getVchrNo() {
		return vchrNo;
	}
	public void setVchrNo(String vchrNo) {
		this.vchrNo = vchrNo;
	}
	
	
}
