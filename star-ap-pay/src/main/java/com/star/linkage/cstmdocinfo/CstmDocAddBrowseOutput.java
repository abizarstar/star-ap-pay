package com.star.linkage.cstmdocinfo;

import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;

public class CstmDocAddBrowseOutput extends BrowseOutput{
	
	public List<CstmDocAddOutput> fldTblDocAdd = new ArrayList<CstmDocAddOutput>();
	public List<CstmDocAddOutput> fldTblDocAddStx = new ArrayList<CstmDocAddOutput>();

}
