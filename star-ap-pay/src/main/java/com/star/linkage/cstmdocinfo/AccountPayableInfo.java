package com.star.linkage.cstmdocinfo;

public class AccountPayableInfo {

	private String balAmt;
	private String inPrsAmt;
	private String inPrsBal;
	private String glEntrySts;
	private String glEntry;
	private String glEntryDt;
	
	
	public String getBalAmt() {
		return balAmt;
	}
	public void setBalAmt(String balAmt) {
		this.balAmt = balAmt;
	}
	public String getInPrsAmt() {
		return inPrsAmt;
	}
	public void setInPrsAmt(String inPrsAmt) {
		this.inPrsAmt = inPrsAmt;
	}
	public String getInPrsBal() {
		return inPrsBal;
	}
	public void setInPrsBal(String inPrsBal) {
		this.inPrsBal = inPrsBal;
	}
	public String getGlEntrySts() {
		return glEntrySts;
	}
	public void setGlEntrySts(String glEntrySts) {
		this.glEntrySts = glEntrySts;
	}
	public String getGlEntry() {
		return glEntry;
	}
	public void setGlEntry(String glEntry) {
		this.glEntry = glEntry;
	}
	public String getGlEntryDt() {
		return glEntryDt;
	}
	public void setGlEntryDt(String glEntryDt) {
		this.glEntryDt = glEntryDt;
	}
	
	
	
}
