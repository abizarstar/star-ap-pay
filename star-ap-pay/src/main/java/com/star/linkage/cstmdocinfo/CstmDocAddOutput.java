package com.star.linkage.cstmdocinfo;

public class CstmDocAddOutput extends CstmDocAddInfoInput{

	private String crtDttsStr;
	private String docExtn;
	private String docPfx;
	private String docArcCtlNo;
	private String docArcVerNo;
	private String docArcCtlItm;
	private String primRef;

	public String getCrtDttsStr() {
		return crtDttsStr;
	}

	public void setCrtDttsStr(String crtDttsStr) {
		this.crtDttsStr = crtDttsStr;
	}

	public String getDocExtn() {
		return docExtn;
	}

	public void setDocExtn(String docExtn) {
		this.docExtn = docExtn;
	}

	public String getDocPfx() {
		return docPfx;
	}

	public void setDocPfx(String docPfx) {
		this.docPfx = docPfx;
	}

	public String getDocArcCtlNo() {
		return docArcCtlNo;
	}

	public void setDocArcCtlNo(String docArcCtlNo) {
		this.docArcCtlNo = docArcCtlNo;
	}

	public String getDocArcVerNo() {
		return docArcVerNo;
	}

	public void setDocArcVerNo(String docArcVerNo) {
		this.docArcVerNo = docArcVerNo;
	}

	public String getDocArcCtlItm() {
		return docArcCtlItm;
	}

	public void setDocArcCtlItm(String docArcCtlItm) {
		this.docArcCtlItm = docArcCtlItm;
	}

	public String getPrimRef() {
		return primRef;
	}

	public void setPrimRef(String primRef) {
		this.primRef = primRef;
	}
	
	
	
}
