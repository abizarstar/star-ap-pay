package com.star.linkage.vendor;

import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;

public class VendorEmailSetupBrowseOutput extends BrowseOutput{

	public List<VendorEmailSetupOutput> fldTblVendorES = new ArrayList<VendorEmailSetupOutput>();
}
