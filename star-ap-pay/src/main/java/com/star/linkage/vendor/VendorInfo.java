package com.star.linkage.vendor;

public class VendorInfo {

	String cmpyId;
	String venId;
	String venNm;
	String venLongNm;
	String venCry;
	String venAdminBrh;
	String pmtTyp;
	String venAdd1;
	String venAdd2;
	String venAdd3;
	String venCity;
	String venPostCode;
	String venContName;
	String venFaxNo;
	String venLang;
	
	public String getVenAdd1() {
		return venAdd1;
	}
	public void setVenAdd1(String venAdd1) {
		this.venAdd1 = venAdd1;
	}
	public String getVenAdd2() {
		return venAdd2;
	}
	public void setVenAdd2(String venAdd2) {
		this.venAdd2 = venAdd2;
	}
	public String getVenAdd3() {
		return venAdd3;
	}
	public void setVenAdd3(String venAdd3) {
		this.venAdd3 = venAdd3;
	}
	public String getVenCity() {
		return venCity;
	}
	public void setVenCity(String venCity) {
		this.venCity = venCity;
	}
	public String getVenPostCode() {
		return venPostCode;
	}
	public void setVenPostCode(String venPostCode) {
		this.venPostCode = venPostCode;
	}
	public String getVenContName() {
		return venContName;
	}
	public void setVenContName(String venContName) {
		this.venContName = venContName;
	}
	public String getVenFaxNo() {
		return venFaxNo;
	}
	public void setVenFaxNo(String venFaxNo) {
		this.venFaxNo = venFaxNo;
	}
	public String getVenLang() {
		return venLang;
	}
	public void setVenLang(String venLang) {
		this.venLang = venLang;
	}
	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}
	public String getVenId() {
		return venId;
	}
	public void setVenId(String venId) {
		this.venId = venId;
	}
	public String getVenNm() {
		return venNm;
	}
	public void setVenNm(String venNm) {
		this.venNm = venNm;
	}
	public String getVenLongNm() {
		return venLongNm;
	}
	public void setVenLongNm(String venLongNm) {
		this.venLongNm = venLongNm;
	}
	public String getVenCry() {
		return venCry;
	}
	public void setVenCry(String venCry) {
		this.venCry = venCry;
	}
	public String getVenAdminBrh() {
		return venAdminBrh;
	}
	public void setVenAdminBrh(String venAdminBrh) {
		this.venAdminBrh = venAdminBrh;
	}
	public String getPmtTyp() {
		return pmtTyp;
	}
	public void setPmtTyp(String pmtTyp) {
		this.pmtTyp = pmtTyp;
	}
	
	
	
}
