package com.star.linkage.vendor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.star.linkage.common.PaymentMethod;
import com.star.modal.CstmVenBnkInfo;

public class VendorEmailSetupInput {

	private String cmpyId;
	private String venId;
	private String subject;
	private String body;
	
	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}
	
	
	public String getVenId() {
		return venId;
	}
	public void setVenId(String venId) {
		this.venId = venId;
	}
	
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	
	
	
}
