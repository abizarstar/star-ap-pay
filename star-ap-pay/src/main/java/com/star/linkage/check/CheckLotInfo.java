package com.star.linkage.check;

public class CheckLotInfo {

	private int lotNo;
	private String lotInfo;
	private String chkFrm;
	private String chkTo;
	private String acctNo;
	private String bnkCode;
	private boolean chkLotUse;
	private boolean chkLotOpn;
	private String chkLotInfo;
	
	public String getAcctNo() {
		return acctNo;
	}
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}
	public String getBnkCode() {
		return bnkCode;
	}
	public void setBnkCode(String bnkCode) {
		this.bnkCode = bnkCode;
	}
	public int getLotNo() {
		return lotNo;
	}
	public void setLotNo(int lotNo) {
		this.lotNo = lotNo;
	}
	public String getLotInfo() {
		return lotInfo;
	}
	public void setLotInfo(String lotInfo) {
		this.lotInfo = lotInfo;
	}
	public String getChkFrm() {
		return chkFrm;
	}
	public void setChkFrm(String chkFrm) {
		this.chkFrm = chkFrm;
	}
	public String getChkTo() {
		return chkTo;
	}
	public void setChkTo(String chkTo) {
		this.chkTo = chkTo;
	}
	public boolean isChkLotUse() {
		return chkLotUse;
	}
	public void setChkLotUse(boolean chkLotUse) {
		this.chkLotUse = chkLotUse;
	}
	public boolean isChkLotOpn() {
		return chkLotOpn;
	}
	public void setChkLotOpn(boolean chkLotOpn) {
		this.chkLotOpn = chkLotOpn;
	}
	public String getChkLotInfo() {
		return chkLotInfo;
	}
	public void setChkLotInfo(String chkLotInfo) {
		this.chkLotInfo = chkLotInfo;
	}
	
	
}
