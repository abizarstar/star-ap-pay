package com.star.linkage.check;

import java.util.Date;

public class VoidCheckDetails {

	private String bnk;
	private String acctNo;
	private String chkNo;
	private Date chkVdOn;
	private String chkVdOnStr;
	public String getChkVdOnStr() {
		return chkVdOnStr;
	}
	public void setChkVdOnStr(String chkVdOnStr) {
		this.chkVdOnStr = chkVdOnStr;
	}
	private String chkVdBy;
	private String chkVdRmk;
	public String getBnk() {
		return bnk;
	}
	public void setBnk(String bnk) {
		this.bnk = bnk;
	}
	public String getAcctNo() {
		return acctNo;
	}
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}
	public String getChkNo() {
		return chkNo;
	}
	public void setChkNo(String chkNo) {
		this.chkNo = chkNo;
	}
	public Date getChkVdOn() {
		return chkVdOn;
	}
	public void setChkVdOn(Date chkVdOn) {
		this.chkVdOn = chkVdOn;
	}
	public String getChkVdBy() {
		return chkVdBy;
	}
	public void setChkVdBy(String chkVdBy) {
		this.chkVdBy = chkVdBy;
	}
	public String getChkVdRmk() {
		return chkVdRmk;
	}
	public void setChkVdRmk(String chkVdRmk) {
		this.chkVdRmk = chkVdRmk;
	}
	
	
	
}
