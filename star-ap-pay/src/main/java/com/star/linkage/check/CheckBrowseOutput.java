package com.star.linkage.check;

import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;

public class CheckBrowseOutput extends BrowseOutput{

	public List<CheckLotInfo> fldTblCheck = new ArrayList<CheckLotInfo>();
	public String curChkNo;
	public long curLotNo;
}
