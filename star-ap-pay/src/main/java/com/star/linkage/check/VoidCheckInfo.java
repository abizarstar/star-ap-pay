package com.star.linkage.check;

public class VoidCheckInfo {

	private String reqId;
	private String chkNo;
	private String vchrNo;
	
	
	public String getReqId() {
		return reqId;
	}
	public void setReqId(String reqId) {
		this.reqId = reqId;
	}
	public String getChkNo() {
		return chkNo;
	}
	public void setChkNo(String chkNo) {
		this.chkNo = chkNo;
	}
	public String getVchrNo() {
		return vchrNo;
	}
	public void setVchrNo(String vchrNo) {
		this.vchrNo = vchrNo;
	}
	
	
}
