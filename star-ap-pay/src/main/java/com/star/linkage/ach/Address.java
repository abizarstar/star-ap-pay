package com.star.linkage.ach;

public class Address {

	private String postalCode = "";
	private String townNm = ""; // CITY
	private String ctrySubDvsn = ""; // STATE PROVINCE
	private String cty = ""; // COUNTRY
	private String addr = ""; //ADDRESS
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public String getTownNm() {
		return townNm;
	}
	public void setTownNm(String townNm) {
		this.townNm = townNm;
	}
	public String getCtrySubDvsn() {
		return ctrySubDvsn;
	}
	public void setCtrySubDvsn(String ctrySubDvsn) {
		this.ctrySubDvsn = ctrySubDvsn;
	}
	public String getCty() {
		return cty;
	}
	public void setCty(String cty) {
		this.cty = cty;
	}
	public String getAddr() {
		return addr;
	}
	public void setAddr(String addr) {
		this.addr = addr;
	}
	
	
	
}
