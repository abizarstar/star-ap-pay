package com.star.linkage.ach;

public class PaymentTotals {

	double totalPropAmnt = 0.0;
	double totACHAmount = 0.0;
	double totWireAmount = 0.0;
	double totChkAmount = 0.0;
	int iACHCount = 0;
	int iWireCount = 0;
	int iChkCount = 0;
	public double getTotalPropAmnt() {
		return totalPropAmnt;
	}
	public void setTotalPropAmnt(double totalPropAmnt) {
		this.totalPropAmnt = totalPropAmnt;
	}
	public double getTotACHAmount() {
		return totACHAmount;
	}
	public void setTotACHAmount(double totACHAmount) {
		this.totACHAmount = totACHAmount;
	}
	public double getTotWireAmount() {
		return totWireAmount;
	}
	public void setTotWireAmount(double totWireAmount) {
		this.totWireAmount = totWireAmount;
	}
	public double getTotChkAmount() {
		return totChkAmount;
	}
	public void setTotChkAmount(double totChkAmount) {
		this.totChkAmount = totChkAmount;
	}
	public int getiACHCount() {
		return iACHCount;
	}
	public void setiACHCount(int iACHCount) {
		this.iACHCount = iACHCount;
	}
	public int getiWireCount() {
		return iWireCount;
	}
	public void setiWireCount(int iWireCount) {
		this.iWireCount = iWireCount;
	}
	public int getiChkCount() {
		return iChkCount;
	}
	public void setiChkCount(int iChkCount) {
		this.iChkCount = iChkCount;
	}
	
	
	
}
