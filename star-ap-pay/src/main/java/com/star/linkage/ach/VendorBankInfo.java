package com.star.linkage.ach;

public class VendorBankInfo {

	private String venId;
	private String payMthd;
	private String bnkRoutingNo;
	private String bnkNm;
	private String bnkAcctNo;
	private String venNm;
	
	public String getVenId() {
		return venId;
	}
	public void setVenId(String venId) {
		this.venId = venId;
	}
	public String getPayMthd() {
		return payMthd;
	}
	public void setPayMthd(String payMthd) {
		this.payMthd = payMthd;
	}
	
	public String getBnkRoutingNo() {
		return bnkRoutingNo;
	}
	public void setBnkRoutingNo(String bnkRoutingNo) {
		this.bnkRoutingNo = bnkRoutingNo;
	}
	public String getBnkNm() {
		return bnkNm;
	}
	public void setBnkNm(String bnkNm) {
		this.bnkNm = bnkNm;
	}
	public String getBnkAcctNo() {
		return bnkAcctNo;
	}
	public void setBnkAcctNo(String bnkAcctNo) {
		this.bnkAcctNo = bnkAcctNo;
	}
	public String getVenNm() {
		return venNm;
	}
	public void setVenNm(String venNm) {
		this.venNm = venNm;
	}
	
	
}
