package com.star.linkage.ach;

public class VendorAddress extends Address{

	private String vendorNm;
	
	public String getVendorNm() {
		return vendorNm;
	}
	public void setVendorNm(String vendorNm) {
		this.vendorNm = vendorNm;
	}

}
