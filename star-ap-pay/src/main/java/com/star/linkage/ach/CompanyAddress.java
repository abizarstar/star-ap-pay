package com.star.linkage.ach;

public class CompanyAddress extends Address{

	private String cmpyCoNm = "";
	private String cmpyNm = "";
	public String getCmpyCoNm() {
		return cmpyCoNm;
	}
	public void setCmpyCoNm(String cmpyCoNm) {
		this.cmpyCoNm = cmpyCoNm;
	}
	public String getCmpyNm() {
		return cmpyNm;
	}
	public void setCmpyNm(String cmpyNm) {
		this.cmpyNm = cmpyNm;
	}
	
	
	
}
