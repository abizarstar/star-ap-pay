package com.star.linkage.ach;

import com.star.linkage.cstmdocinfo.AccountPayableInfo;

public class InvoiceInfo extends AccountPayableInfo{
	
	private String reqId;
	private String vchrCtlNo;
	private String cmpyId;
	private String vchrPfx;
	private String vchrNo;
	private String vchrInvNo;
	private String vchrVenId;
	private double vchrAmt;
	private String vchrAmtStr;
	private double vchrDiscAmt;
	private String vchrCry;
	private String payMthd;
	private String notesToPayee;
	private String invDt;
	private double vchrVendTotal;
	private String paymentDt;
	private String chkNo;
	private double chkNetAmt;
	
	public double getChkNetAmt() {
		return chkNetAmt;
	}
	public void setChkNetAmt(double chkNetAmt) {
		this.chkNetAmt = chkNetAmt;
	}
	public String getChkNo() {
		return chkNo;
	}
	public void setChkNo(String chkNo) {
		this.chkNo = chkNo;
	}
	public String getVchrCtlNo() {
		return vchrCtlNo;
	}
	public void setVchrCtlNo(String vchrCtlNo) {
		this.vchrCtlNo = vchrCtlNo;
	}
	public String getVchrInvNo() {
		return vchrInvNo;
	}
	public void setVchrInvNo(String vchrInvNo) {
		this.vchrInvNo = vchrInvNo;
	}
	public String getVchrVenId() {
		return vchrVenId;
	}
	public void setVchrVenId(String vchrVenId) {
		this.vchrVenId = vchrVenId;
	}
	public double getVchrAmt() {
		return vchrAmt;
	}
	public void setVchrAmt(double vchrAmt) {
		this.vchrAmt = vchrAmt;
	}
	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}
	public String getVchrNo() {
		return vchrNo;
	}
	public void setVchrNo(String vchrNo) {
		this.vchrNo = vchrNo;
	}
	public double getVchrDiscAmt() {
		return vchrDiscAmt;
	}
	public void setVchrDiscAmt(double vchrDiscAmt) {
		this.vchrDiscAmt = vchrDiscAmt;
	}
	public String getVchrCry() {
		return vchrCry;
	}
	public void setVchrCry(String vchrCry) {
		this.vchrCry = vchrCry;
	}
	public String getVchrPfx() {
		return vchrPfx;
	}
	public void setVchrPfx(String vchrPfx) {
		this.vchrPfx = vchrPfx;
	}
	public String getPayMthd() {
		return payMthd;
	}
	public void setPayMthd(String payMthd) {
		this.payMthd = payMthd;
	}
	public String getNotesToPayee() {
		return notesToPayee;
	}
	public void setNotesToPayee(String notesToPayee) {
		this.notesToPayee = notesToPayee;
	}
	public String getInvDt() {
		return invDt;
	}
	public void setInvDt(String invDt) {
		this.invDt = invDt;
	}
	public String getVchrAmtStr() {
		return vchrAmtStr;
	}
	public void setVchrAmtStr(String vchrAmtStr) {
		this.vchrAmtStr = vchrAmtStr;
	}
	public double getVchrVendTotal() {
		return vchrVendTotal;
	}
	public void setVchrVendTotal(double vchrVendTotal) {
		this.vchrVendTotal = vchrVendTotal;
	}
	public String getReqId() {
		return reqId;
	}
	public void setReqId(String reqId) {
		this.reqId = reqId;
	}
	public String getPaymentDt() {
		return paymentDt;
	}
	public void setPaymentDt(String paymentDt) {
		this.paymentDt = paymentDt;
	}
	
	
}
