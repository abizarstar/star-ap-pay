package com.star.linkage.ach;

public class CompanyBankInfo {

	private String bnkName;
	private String routingNo;
	private String acctNm;
	private String acctNo;
	private String bnkCode;
	private String bnkAchId;
	private String bnkMmbId;
	
	
	public String getBnkName() {
		return bnkName;
	}
	public void setBnkName(String bnkName) {
		this.bnkName = bnkName;
	}
	public String getRoutingNo() {
		return routingNo;
	}
	public void setRoutingNo(String routingNo) {
		this.routingNo = routingNo;
	}
	public String getAcctNm() {
		return acctNm;
	}
	public void setAcctNm(String acctNm) {
		this.acctNm = acctNm;
	}
	public String getAcctNo() {
		return acctNo;
	}
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}
	public String getBnkCode() {
		return bnkCode;
	}
	public void setBnkCode(String bnkCode) {
		this.bnkCode = bnkCode;
	}
	public String getBnkAchId() {
		return bnkAchId;
	}
	public void setBnkAchId(String bnkAchId) {
		this.bnkAchId = bnkAchId;
	}
	public String getBnkMmbId() {
		return bnkMmbId;
	}
	public void setBnkMmbId(String bnkMmbId) {
		this.bnkMmbId = bnkMmbId;
	}
	
	
	
}
