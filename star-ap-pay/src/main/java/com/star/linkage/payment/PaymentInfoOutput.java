package com.star.linkage.payment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.star.linkage.company.CompanyInfo;
import com.star.linkage.cstmdocinfo.VchrInfo;
import com.star.modal.CstmPayParams;

public class PaymentInfoOutput extends CstmPayParams{

	
	private String reqDtStr;
	private String crtdDtStr;
	private double amtTotal;
	private String amtTotalStr;
	private List<VchrInfo> paramInvList = new ArrayList<VchrInfo>();
	private List<CompanyInfo> companyBnkList = new ArrayList<CompanyInfo>();
	private String actnRmk;
	private String flNm;
	private Boolean flSentSts;
	private String reqActnOnStr;
	private String payMthd;
	private String cry;
	private int ctlNo;
	private String crtdById;
	private String actnById;
	private String bnkCode;
	private String accountNo;
	private int totalReqId;
	private String issueDtStr;
	
	
	public String getIssueDtStr() {
		return issueDtStr;
	}
	public void setIssueDtStr(String issueDtStr) {
		this.issueDtStr = issueDtStr;
	}
	
	public List<CompanyInfo> getCompanyBnkList() {
		return companyBnkList;
	}
	public void setCompanyBnkList(List<CompanyInfo> companyBnkList) {
		this.companyBnkList = companyBnkList;
	}
	public String getReqDtStr() {
		return reqDtStr;
	}
	public void setReqDtStr(String reqDtStr) {
		this.reqDtStr = reqDtStr;
	}
	public String getCrtdDtStr() {
		return crtdDtStr;
	}
	public void setCrtdDtStr(String crtdDtStr) {
		this.crtdDtStr = crtdDtStr;
	}
	public List<VchrInfo> getParamInvList() {
		return paramInvList;
	}
	public void setParamInvList(List<VchrInfo> paramInvList) {
		this.paramInvList = paramInvList;
	}
	public double getAmtTotal() {
		return amtTotal;
	}
	public void setAmtTotal(double amtTotal) {
		this.amtTotal = amtTotal;
	}
	public String getActnRmk() {
		return actnRmk;
	}
	public void setActnRmk(String actnRmk) {
		this.actnRmk = actnRmk;
	}
	
	public String getFlNm() {
		return flNm;
	}
	public void setFlNm(String flNm) {
		this.flNm = flNm;
	}
	
	public Boolean getFlSentSts() {
		return flSentSts;
	}
	public void setFlSentSts(Boolean flSentSts) {
		this.flSentSts = flSentSts;
	}
	public String getReqActnOnStr() {
		return reqActnOnStr;
	}
	public void setReqActnOnStr(String reqActnOnStr) {
		this.reqActnOnStr = reqActnOnStr;
	}
	public String getAmtTotalStr() {
		return amtTotalStr;
	}
	public void setAmtTotalStr(String amtTotalStr) {
		this.amtTotalStr = amtTotalStr;
	}
	public String getPayMthd() {
		return payMthd;
	}
	public void setPayMthd(String payMthd) {
		this.payMthd = payMthd;
	}
	public String getCry() {
		return cry;
	}
	public void setCry(String cry) {
		this.cry = cry;
	}
	public int getCtlNo() {
		return ctlNo;
	}
	public void setCtlNo(int ctlNo) {
		this.ctlNo = ctlNo;
	}
	public String getCrtdById() {
		return crtdById;
	}
	public void setCrtdById(String crtdById) {
		this.crtdById = crtdById;
	}
	public String getActnById() {
		return actnById;
	}
	public void setActnById(String actnById) {
		this.actnById = actnById;
	}
	public String getBnkCode() {
		return bnkCode;
	}
	public void setBnkCode(String bnkCode) {
		this.bnkCode = bnkCode;
	}
	public String getAccountNo() {
		return accountNo;
	}
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	public int getTotalReqId() {
		return totalReqId;
	}
	public void setTotalReqId(int totalReqId) {
		this.totalReqId = totalReqId;
	}
	
	
}
