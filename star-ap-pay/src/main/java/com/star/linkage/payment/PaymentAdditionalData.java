package com.star.linkage.payment;

public class PaymentAdditionalData {

	private String vchrNo;
	private int pymntMthd;
	private String note;
	private String chkNo;
	public String getChkNo() {
		return chkNo;
	}
	public void setChkNo(String chkNo) {
		this.chkNo = chkNo;
	}
	public String getVchrNo() {
		return vchrNo;
	}
	public void setVchrNo(String vchrNo) {
		this.vchrNo = vchrNo;
	}
	public int getPymntMthd() {
		return pymntMthd;
	}
	public void setPymntMthd(int pymntMthd) {
		this.pymntMthd = pymntMthd;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	
	
	
}
