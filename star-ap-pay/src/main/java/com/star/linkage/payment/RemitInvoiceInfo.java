package com.star.linkage.payment;

public class RemitInvoiceInfo {

	private String reqId;
	private String invNo;
	private String chkNo;
	private String pmntDt;
	public String getReqId() {
		return reqId;
	}
	public void setReqId(String reqId) {
		this.reqId = reqId;
	}
	public String getInvNo() {
		return invNo;
	}
	public void setInvNo(String invNo) {
		this.invNo = invNo;
	}
	public String getChkNo() {
		return chkNo;
	}
	public void setChkNo(String chkNo) {
		this.chkNo = chkNo;
	}
	public String getPmntDt() {
		return pmntDt;
	}
	public void setPmntDt(String pmntDt) {
		this.pmntDt = pmntDt;
	}
	
	
	
	
}
