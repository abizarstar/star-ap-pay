package com.star.linkage.payment;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PaymentInfo {

	private String reqId;
	private List<String> invList = new ArrayList<String>();
	private List<PaymentAdditionalData> invcAddtnlData = new ArrayList<PaymentAdditionalData>();
	private String reqUserBy;
	private String reqActnBy;
	private String paySts;
	private String cmpyId;
	private String rmk;
	private String acctNo;
	private String bankCd;
	private String payMthd;
	private String venId;
	private String vchrNo;
	private double batchTotalAmt;
	private String issueDate;
	
	public double getBatchTotalAmt() {
		return batchTotalAmt;
	}
	public String getIssueDate() {
		return issueDate;
	}
	public void setIssueDate(String issueDate) {
		this.issueDate = issueDate;
	}
	public void setBatchTotalAmt(double batchTotalAmt) {
		this.batchTotalAmt = batchTotalAmt;
	}
	public String getReqId() {
		return reqId;
	}
	public void setReqId(String reqId) {
		this.reqId = reqId;
	}
	public List<String> getInvList() {
		return invList;
	}
	public void setInvList(List<String> invList) {
		this.invList = invList;
	}
	public String getReqUserBy() {
		return reqUserBy;
	}
	public void setReqUserBy(String reqUserBy) {
		this.reqUserBy = reqUserBy;
	}
	public String getRmk() {
		return rmk;
	}
	public void setRmk(String rmk) {
		this.rmk = rmk;
	}
	public String getReqActnBy() {
		return reqActnBy;
	}
	public void setReqActnBy(String reqActnBy) {
		this.reqActnBy = reqActnBy;
	}
	public String getPaySts() {
		return paySts;
	}
	public void setPaySts(String paySts) {
		this.paySts = paySts;
	}
	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}
	public List<PaymentAdditionalData> getInvcAddtnlData() {
		return invcAddtnlData;
	}
	public void setInvcAddtnlData(List<PaymentAdditionalData> invcAddtnlData) {
		this.invcAddtnlData = invcAddtnlData;
	}
	public String getAcctNo() {
		return acctNo;
	}
	public void setAcctNo(String acctNo) {
		this.acctNo = acctNo;
	}
	public String getBankCd() {
		return bankCd;
	}
	public void setBankCd(String bankCd) {
		this.bankCd = bankCd;
	}
	public String getPayMthd() {
		return payMthd;
	}
	public void setPayMthd(String payMthd) {
		this.payMthd = payMthd;
	}

	public String getVenId() {
		return venId;
	}
	public void setVenId(String venId) {
		this.venId = venId;
	}

	public String getVchrNo() {
		return vchrNo;
	}
	public void setVchrNo(String vchrNo) {
		this.vchrNo = vchrNo;
	}
	
	
}
