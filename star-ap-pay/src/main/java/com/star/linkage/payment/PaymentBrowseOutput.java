package com.star.linkage.payment;

import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;

public class PaymentBrowseOutput extends BrowseOutput{

	public List<PaymentInfoOutput> fldTblPayment = new ArrayList<PaymentInfoOutput>();
}
