package com.star.linkage.job;

public class JobInfo {
String task;
String taskValue;
public String getTask() {
	return task;
}
public void setTask(String task) {
	this.task = task;
}
public String getTaskValue() {
	return taskValue;
}
public void setTaskValue(String taskValue) {
	this.taskValue = taskValue;
}

}
