package com.star.linkage.job;

import com.star.common.BrowseOutput;

public class JobBrowseResponse<T_Output extends BrowseOutput> {

		public T_Output output = null;
		
		public void setOutput(T_Output output) {
			this.output = output; 
		}
		
	}