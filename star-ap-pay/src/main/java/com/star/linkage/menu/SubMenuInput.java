package com.star.linkage.menu;

public class SubMenuInput {

	private String menuId;
	private String subMenuId;
	private String isVw;
	public String getMenuId() {
		return menuId;
	}
	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}
	public String getSubMenuId() {
		return subMenuId;
	}
	public void setSubMenuId(String subMenuId) {
		this.subMenuId = subMenuId;
	}
	public String getIsVw() {
		return isVw;
	}
	public void setIsVw(String isVw) {
		this.isVw = isVw;
	}	
}
