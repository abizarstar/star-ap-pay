package com.star.linkage.menu;

import java.util.List;

public class MenuListInput {

	private String grpId;
	private List<MenuIsvwInput> menuId;
	private List<SubMenuInput> subMenList;

	public String getGrpId() {
		return grpId;
	}

	public void setGrpId(String grpId) {
		this.grpId = grpId;
	}

	public List<MenuIsvwInput> getMenuId() {
		return menuId;
	}

	public void setMenuId(List<MenuIsvwInput> menuId) {
		this.menuId = menuId;
	}

	public List<SubMenuInput> getSubMenList() {
		return subMenList;
	}

	public void setSubMenList(List<SubMenuInput> subMenList) {
		this.subMenList = subMenList;
	}
}
