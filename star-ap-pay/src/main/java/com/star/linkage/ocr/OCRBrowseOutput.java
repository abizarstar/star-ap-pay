package com.star.linkage.ocr;

import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;
import com.star.modal.CstmOcrVenMap;

public class OCRBrowseOutput extends BrowseOutput{

	public List<CstmOcrVenMap> fldTblVenList = new ArrayList<CstmOcrVenMap>();
}
