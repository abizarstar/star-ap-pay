package com.star.linkage.bank;

import java.util.ArrayList;
import java.util.List;

public class BankInfo {

	private String bnkCode = "";
	private String bnkCty = "";
	private String bnkRoutNo = "";
	private String bnkNm = "";
	private String bnkCity = "";
	private String bnkBrh = "";
	private String bnkCry = "";
	private String bnkSwiftCode = "";
	private String bnkHost = "";
	private String bnkPort = "";
	private String bnkFtpUser = "";
	private String bnkFtpPass = "";
	private String bnkKeyPath = "";
	private String bnkSftpTyp = "";
	private String bnkSftpFilePath = "";
	
	private List<BankAccntInfo> bankAcctList = new ArrayList<BankAccntInfo>();
	public String getBnkCode() {
		return bnkCode;
	}
	public void setBnkCode(String bnkCode) {
		this.bnkCode = bnkCode;
	}
	public String getBnkCty() {
		return bnkCty;
	}
	public void setBnkCty(String bnkCty) {
		this.bnkCty = bnkCty;
	}
	public String getBnkRoutNo() {
		return bnkRoutNo;
	}
	public void setBnkRoutNo(String bnkRoutNo) {
		this.bnkRoutNo = bnkRoutNo;
	}
	public String getBnkNm() {
		return bnkNm;
	}
	public void setBnkNm(String bnkNm) {
		this.bnkNm = bnkNm;
	}
	public String getBnkCity() {
		return bnkCity;
	}
	public void setBnkCity(String bnkCity) {
		this.bnkCity = bnkCity;
	}
	public String getBnkBrh() {
		return bnkBrh;
	}
	public void setBnkBrh(String bnkBrh) {
		this.bnkBrh = bnkBrh;
	}
	public String getBnkCry() {
		return bnkCry;
	}
	public void setBnkCry(String bnkCry) {
		this.bnkCry = bnkCry;
	}
	public String getBnkSwiftCode() {
		return bnkSwiftCode;
	}
	public void setBnkSwiftCode(String bnkSwiftCode) {
		this.bnkSwiftCode = bnkSwiftCode;
	}
	public String getBnkHost() {
		return bnkHost;
	}
	public void setBnkHost(String bnkHost) {
		this.bnkHost = bnkHost;
	}
	public String getBnkPort() {
		return bnkPort;
	}
	public void setBnkPort(String bnkPort) {
		this.bnkPort = bnkPort;
	}
	public String getBnkFtpUser() {
		return bnkFtpUser;
	}
	public void setBnkFtpUser(String bnkFtpUser) {
		this.bnkFtpUser = bnkFtpUser;
	}
	public String getBnkFtpPass() {
		return bnkFtpPass;
	}
	public void setBnkFtpPass(String bnkFtpPass) {
		this.bnkFtpPass = bnkFtpPass;
	}
	public String getBnkKeyPath() {
		return bnkKeyPath;
	}
	public void setBnkKeyPath(String bnkKeyPath) {
		this.bnkKeyPath = bnkKeyPath;
	}
	public List<BankAccntInfo> getBankAcctList() {
		return bankAcctList;
	}
	public void setBankAcctList(List<BankAccntInfo> bankAcctList) {
		this.bankAcctList = bankAcctList;
	}
	public String getBnkSftpTyp() {
		return bnkSftpTyp;
	}
	public void setBnkSftpTyp(String bnkSftpTyp) {
		this.bnkSftpTyp = bnkSftpTyp;
	}
	public String getBnkSftpFilePath() {
		return bnkSftpFilePath;
	}
	public void setBnkSftpFilePath(String bnkSftpFilePath) {
		this.bnkSftpFilePath = bnkSftpFilePath;
	}
	
	
	
	
}
