package com.star.linkage.bank;

public class BankAccntInfo {

	private String bnkAcct;
	private String bnkAcctDesc;
	private String bnkAcctNo;
	private String bnkRoutNo;
	private String bnkAchId;
	private String bnkMmbId;
	private boolean isDefault;


	public String getBnkAcct() {
		return bnkAcct;
	}
	public void setBnkAcct(String bnkAcct) {
		this.bnkAcct = bnkAcct;
	}
	public String getBnkAcctDesc() {
		return bnkAcctDesc;
	}
	public void setBnkAcctDesc(String bnkAcctDesc) {
		this.bnkAcctDesc = bnkAcctDesc;
	}
	public String getBnkAcctNo() {
		return bnkAcctNo;
	}
	public void setBnkAcctNo(String bnkAcctNo) {
		this.bnkAcctNo = bnkAcctNo;
	}
	public boolean isDefault() {
		return isDefault;
	}
	public void setDefault(boolean isDefault) {
		this.isDefault = isDefault;
	}
	public String getBnkRoutNo() {
		return bnkRoutNo;
	}
	public void setBnkRoutNo(String bnkRoutNo) {
		this.bnkRoutNo = bnkRoutNo;
	}
	public String getBnkAchId() {
		return bnkAchId;
	}
	public void setBnkAchId(String bnkAchId) {
		this.bnkAchId = bnkAchId;
	}
	public String getBnkMmbId() {
		return bnkMmbId;
	}
	public void setBnkMmbId(String bnkMmbId) {
		this.bnkMmbId = bnkMmbId;
	}
	
	
	
}
