package com.star.linkage.dbconfig;

public class DbInfo {
String driver;
String url;
String host;
String port;
String db;
String user;
String password;
String  conType;
public String getDriver() {
	return driver;
}
public void setDriver(String driver) {
	this.driver = driver;
}
public String getUrl() {
	return url;
}
public void setUrl(String url) {
	this.url = url;
}
public String getHost() {
	return host;
}
public void setHost(String host) {
	this.host = host;
}
public String getPort() {
	return port;
}
public void setPort(String port) {
	this.port = port;
}
public String getDb() {
	return db;
}
public void setDb(String db) {
	this.db = db;
}
public String getUser() {
	return user;
}
public void setUser(String user) {
	this.user = user;
}
public String getPassword() {
	return password;
}
public void setPassword(String password) {
	this.password = password;
}
public String getConType() {
	return conType;
}
public void setConType(String conType) {
	this.conType = conType;
}

}
