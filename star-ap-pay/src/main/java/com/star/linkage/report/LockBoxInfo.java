package com.star.linkage.report;

public class LockBoxInfo{

	private String batchId;
	private String invNo;
	private String chkNo;
	private String amountStr;
	private String dpstDt;
	private String ssnId;
	private String cashRcpt;
	private String cusId;
	private String cusNm;
	private String trnSts;
	private String jeRef;
	private String actvyDt;
	private String lgnUsr;
	private String frmAcctngPr;
	private String toAcctngPr;
	private String extDesc;
	private String extDesc1;
	
	public String getBatchId() {
		return batchId;
	}
	public void setBatchId(String batchId) {
		this.batchId = batchId;
	}
	public String getInvNo() {
		return invNo;
	}
	public void setInvNo(String invNo) {
		this.invNo = invNo;
	}
	public String getChkNo() {
		return chkNo;
	}
	public void setChkNo(String chkNo) {
		this.chkNo = chkNo;
	}
	public String getAmountStr() {
		return amountStr;
	}
	public void setAmountStr(String amountStr) {
		this.amountStr = amountStr;
	}
	public String getDpstDt() {
		return dpstDt;
	}
	public void setDpstDt(String dpstDt) {
		this.dpstDt = dpstDt;
	}
	public String getSsnId() {
		return ssnId;
	}
	public void setSsnId(String ssnId) {
		this.ssnId = ssnId;
	}
	public String getCashRcpt() {
		return cashRcpt;
	}
	public void setCashRcpt(String cashRcpt) {
		this.cashRcpt = cashRcpt;
	}
	public String getCusId() {
		return cusId;
	}
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	public String getCusNm() {
		return cusNm;
	}
	public void setCusNm(String cusNm) {
		this.cusNm = cusNm;
	}
	public String getTrnSts() {
		return trnSts;
	}
	public void setTrnSts(String trnSts) {
		this.trnSts = trnSts;
	}
		
	public String getJeRef() {
		return jeRef;
	}
	public void setJeRef(String jeRef) {
		this.jeRef = jeRef;
	}
	
	public String getActvyDt() {
		return actvyDt;
	}
	public void setActvyDt(String actvyDt) {
		this.actvyDt = actvyDt;
	}
	
	public String getLgnUsr() {
		return lgnUsr;
	}
	public void setLgnUsr(String lgnUsr) {
		this.lgnUsr = lgnUsr;
	}
	
	public String getFrmAcctngPr() {
		return frmAcctngPr;
	}
	public void setFrmAcctngPr(String frmAcctngPr) {
		this.frmAcctngPr = frmAcctngPr;
	}
	
	public String getToAcctngPr() {
		return toAcctngPr;
	}
	public void setToAcctngPr(String toAcctngPr) {
		this.toAcctngPr = toAcctngPr;
	}
	
	public String getExtDesc() {
		return extDesc;
	}
	public void setExtDesc(String extDesc) {
		this.extDesc = extDesc;
	}
	
	public String getExtDesc1() {
		return extDesc1;
	}
	public void setExtDesc1(String extDesc1) {
		this.extDesc1 = extDesc1;
	}
	
}

