package com.star.linkage.report;

public class LedgerInformation {

	private String jeRef;
	private String actvyDt;
	private String lgnUsr;
	private String frmAcctngPr;
	private String toAcctngPr;
	private String extDesc;
	private String extDesc1;
	
	public String getJeRef() {
		return jeRef;
	}
	public void setJeRef(String jeRef) {
		this.jeRef = jeRef;
	}
	public String getActvyDt() {
		return actvyDt;
	}
	public void setActvyDt(String actvyDt) {
		this.actvyDt = actvyDt;
	}
	public String getLgnUsr() {
		return lgnUsr;
	}
	public void setLgnUsr(String lgnUsr) {
		this.lgnUsr = lgnUsr;
	}
	public String getFrmAcctngPr() {
		return frmAcctngPr;
	}
	public void setFrmAcctngPr(String frmAcctngPr) {
		this.frmAcctngPr = frmAcctngPr;
	}
	public String getToAcctngPr() {
		return toAcctngPr;
	}
	public void setToAcctngPr(String toAcctngPr) {
		this.toAcctngPr = toAcctngPr;
	}
	public String getExtDesc() {
		return extDesc;
	}
	public void setExtDesc(String extDesc) {
		this.extDesc = extDesc;
	}
	public String getExtDesc1() {
		return extDesc1;
	}
	public void setExtDesc1(String extDesc1) {
		this.extDesc1 = extDesc1;
	}
	
	
}
