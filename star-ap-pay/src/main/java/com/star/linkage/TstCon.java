package com.star.linkage;

import java.sql.Connection;
import java.sql.DriverManager;

import org.hibernate.Session;
import org.hibernate.SessionFactory;

public class TstCon {
	
	public TstCon() {
		super();
		// TODO Auto-generated constructor stub
	}

	public static Connection getCon(String driver, String dbUrl, String dbUsrNm, String dbPass) throws Exception {
		Connection con = null;
		try {
			Class.forName(driver);
			con = DriverManager.getConnection(dbUrl, dbUsrNm, dbPass);
		} catch (Exception exp) {
			return con;
		}
		return con;
	}

}
