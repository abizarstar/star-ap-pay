package com.star.linkage.ebstypecode;

public class GLEBSMapInput {

	private String ebsId;
	private String ebsNm;
	private String typCd;
	private String typNm;
	private String post1;
	private String post2;
	
	public String getEbsId() {
		return ebsId;
	}
	public void setEbsId(String ebsId) {
		this.ebsId = ebsId;
	}
	
	public String getEbsNm() {
		return ebsNm;
	}
	public void setEbsNm(String ebsNm) {
		this.ebsNm = ebsNm;
	}
	
	public String getTypCd() {
		return typCd;
	}
	public void setTypCd(String typCd) {
		this.typCd = typCd;
	}
	
	public String getTypNm() {
		return typNm;
	}
	public void setTypNm(String typNm) {
		this.typNm = typNm;
	}
	public String getPost1() {
		return post1;
	}
	public void setPost1(String post1) {
		this.post1 = post1;
	}
	public String getPost2() {
		return post2;
	}
	public void setPost2(String post2) {
		this.post2 = post2;
	}
	
		
}
