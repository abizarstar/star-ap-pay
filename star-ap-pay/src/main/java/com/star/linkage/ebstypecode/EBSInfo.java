package com.star.linkage.ebstypecode;

public class EBSInfo {

	String ebsId;
	String ebsNm;

	public String getEbsId() {
		return ebsId;
	}
	public void setEbsId(String ebsId) {
		this.ebsId = ebsId;
	}
	public String getEbsNm() {
		return ebsNm;
	}
	public void setEbsNm(String ebsNm) {
		this.ebsNm = ebsNm;
	}
	
}
