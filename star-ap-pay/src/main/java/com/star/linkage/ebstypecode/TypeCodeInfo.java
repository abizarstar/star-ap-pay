package com.star.linkage.ebstypecode;

public class TypeCodeInfo {

	String typCd;
	String typNm;

	public String getTypCd() {
		return typCd;
	}
	public void setTypCd(String typCd) {
		this.typCd = typCd;
	}
	public String getTypNm() {
		return typNm;
	}
	public void setTypNm(String typNm) {
		this.typNm = typNm;
	}
	
}
