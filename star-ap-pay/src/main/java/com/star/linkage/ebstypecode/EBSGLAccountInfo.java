package com.star.linkage.ebstypecode;

public class EBSGLAccountInfo {

	private String ebsId;
	private String ebsNm;
	private String typCd;
	private String glAcct;
	private String glSubAcct;
	private String glAcct1;
	private String glSubAcct1;
	private String glAcct2;
	private String glSubAcct2;
	private String trnTyp;
	private String posSeq;
	
	
	public String getEbsId() {
		return ebsId;
	}
	public void setEbsId(String ebsId) {
		this.ebsId = ebsId;
	}
	public String getEbsNm() {
		return ebsNm;
	}
	public void setEbsNm(String ebsNm) {
		this.ebsNm = ebsNm;
	}
	public String getTypCd() {
		return typCd;
	}
	public void setTypCd(String typCd) {
		this.typCd = typCd;
	}
	public String getGlAcct() {
		return glAcct;
	}
	public void setGlAcct(String glAcct) {
		this.glAcct = glAcct;
	}
	public String getGlSubAcct() {
		return glSubAcct;
	}
	public void setGlSubAcct(String glSubAcct) {
		this.glSubAcct = glSubAcct;
	}
	public String getTrnTyp() {
		return trnTyp;
	}
	public void setTrnTyp(String trnTyp) {
		this.trnTyp = trnTyp;
	}
	public String getPosSeq() {
		return posSeq;
	}
	public void setPosSeq(String posSeq) {
		this.posSeq = posSeq;
	}
	public String getGlAcct1() {
		return glAcct1;
	}
	public void setGlAcct1(String glAcct1) {
		this.glAcct1 = glAcct1;
	}
	public String getGlSubAcct1() {
		return glSubAcct1;
	}
	public void setGlSubAcct1(String glSubAcct1) {
		this.glSubAcct1 = glSubAcct1;
	}
	public String getGlAcct2() {
		return glAcct2;
	}
	public void setGlAcct2(String glAcct2) {
		this.glAcct2 = glAcct2;
	}
	public String getGlSubAcct2() {
		return glSubAcct2;
	}
	public void setGlSubAcct2(String glSubAcct2) {
		this.glSubAcct2 = glSubAcct2;
	}
	
}
