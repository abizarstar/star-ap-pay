package com.star.linkage.gl;

public class GlEntryData {

	private String bscGLAcct;
	private String bscGlAcctDesc;
	private String bscGLSubAcct;
	private String ldgrId;
	private double crAmt;
	private double drAmt;
	private String rmk;
	
	
	public String getBscGLAcct() {
		return bscGLAcct;
	}
	public void setBscGLAcct(String bscGLAcct) {
		this.bscGLAcct = bscGLAcct;
	}
	public String getBscGLSubAcct() {
		return bscGLSubAcct;
	}
	public void setBscGLSubAcct(String bscGLSubAcct) {
		this.bscGLSubAcct = bscGLSubAcct;
	}
	public String getLdgrId() {
		return ldgrId;
	}
	public void setLdgrId(String ldgrId) {
		this.ldgrId = ldgrId;
	}
	public double getCrAmt() {
		return crAmt;
	}
	public void setCrAmt(double crAmt) {
		this.crAmt = crAmt;
	}
	public double getDrAmt() {
		return drAmt;
	}
	public void setDrAmt(double drAmt) {
		this.drAmt = drAmt;
	}
	public String getRmk() {
		return rmk;
	}
	public void setRmk(String rmk) {
		this.rmk = rmk;
	}
	public String getBscGlAcctDesc() {
		return bscGlAcctDesc;
	}
	public void setBscGlAcctDesc(String bscGlAcctDesc) {
		this.bscGlAcctDesc = bscGlAcctDesc;
	}
	
	
	
}
