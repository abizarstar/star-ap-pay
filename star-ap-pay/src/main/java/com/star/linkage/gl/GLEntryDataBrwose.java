package com.star.linkage.gl;

import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;

public class GLEntryDataBrwose extends BrowseOutput{

	public List<GlEntryData> fldTblGLEntry = new ArrayList<GlEntryData>();
	public String entryDt;
	public String narrDesc;
}
