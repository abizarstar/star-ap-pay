package com.star.linkage.gl;

public class SubAcctInfo {

	private String glAcct;
	private String subAcct;
	private String subAcctDesc;
	public String getGlAcct() {
		return glAcct;
	}
	public void setGlAcct(String glAcct) {
		this.glAcct = glAcct;
	}
	public String getSubAcct() {
		return subAcct;
	}
	public void setSubAcct(String subAcct) {
		this.subAcct = subAcct;
	}
	public String getSubAcctDesc() {
		return subAcctDesc;
	}
	public void setSubAcctDesc(String subAcctDesc) {
		this.subAcctDesc = subAcctDesc;
	}
	
	
}
