package com.star.linkage.dashboard;

import com.star.common.BrowseOutput;

public class APOverviewBrowseOuput extends BrowseOutput{

	public String totPayable;
	public String balance;
	public String overDueBalance;
	public String overDuePer;
	public String dueInvoice30Days;
	public String dueInvoice60Days;
	public String dueInvoice90Days;
	public String dueInvoice90PlusDays;
	
	
}

