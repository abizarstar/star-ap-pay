package com.star.linkage.invoice;

public class InvoiceInfo {

	String invoiceNo;
	String cusId;
	String arBrh;
	String amount;
	String ssnId;
	
	
	
	public String getInvoiceNo() {
		return invoiceNo;
	}
	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}
	public String getCusId() {
		return cusId;
	}
	public void setCusId(String cusId) {
		this.cusId = cusId;
	}
	public String getArBrh() {
		return arBrh;
	}
	public void setArBrh(String arBrh) {
		this.arBrh = arBrh;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getSsnId() {
		return ssnId;
	}
	public void setSsnId(String ssnId) {
		this.ssnId = ssnId;
	}
	
	
}
