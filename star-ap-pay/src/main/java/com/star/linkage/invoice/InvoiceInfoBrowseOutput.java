package com.star.linkage.invoice;

import java.util.ArrayList;
import java.util.List;

import com.star.common.BrowseOutput;

public class InvoiceInfoBrowseOutput extends BrowseOutput {

	public List<InvoiceInfo> fldTblInvoice = new ArrayList<InvoiceInfo>();
}
