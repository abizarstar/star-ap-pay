package com.star.linkage.company;

import java.util.ArrayList;
import java.util.List;

import com.star.linkage.bank.BankAccntInfo;

public class CompanyInfo {

	private String cmpyId;
	private String bankCode;
	private String bnkRoutNo;
	private List<BankAccntInfo> accoutnList = new ArrayList<BankAccntInfo>();
	public String getCmpyId() {
		return cmpyId;
	}
	public void setCmpyId(String cmpyId) {
		this.cmpyId = cmpyId;
	}
	public String getBankCode() {
		return bankCode;
	}
	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}
	public List<BankAccntInfo> getAccoutnList() {
		return accoutnList;
	}
	public void setAccoutnList(List<BankAccntInfo> accoutnList) {
		this.accoutnList = accoutnList;
	}
	public String getBnkRoutNo() {
		return bnkRoutNo;
	}
	public void setBnkRoutNo(String bnkRoutNo) {
		this.bnkRoutNo = bnkRoutNo;
	}
	
	
}
