package com.star.linkage.company;

public class Address {

	private String addr1;
	private String addr2;
	private String addr3;
	private String city;
	private String cty;
	private String pcd;
	private String stProv;
	public String getAddr1() {
		return addr1;
	}
	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}
	public String getAddr2() {
		return addr2;
	}
	public void setAddr2(String addr2) {
		this.addr2 = addr2;
	}
	public String getAddr3() {
		return addr3;
	}
	public void setAddr3(String addr3) {
		this.addr3 = addr3;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCty() {
		return cty;
	}
	public void setCty(String cty) {
		this.cty = cty;
	}
	public String getPcd() {
		return pcd;
	}
	public void setPcd(String pcd) {
		this.pcd = pcd;
	}
	public String getStProv() {
		return stProv;
	}
	public void setStProv(String stProv) {
		this.stProv = stProv;
	}
	
	
	
}
