
package com.invera.stratix.services.cashRcptSsn;

import java.math.BigDecimal;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for CashRctSession complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CashRctSession">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sessionIdentity" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="cashRctBranchId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="journalDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="bankId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="depositExchangeRate" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="depositDate" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="arCurrency1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="arCrossExchangeRate1" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="arCurrency2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="arCrossExchangeRate2" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="arCurrency3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="arCrossExchangeRate3" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="controlDepositAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="controlNumberOfChecks" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CashRctSession", propOrder = {
    "sessionIdentity",
    "cashRctBranchId",
    "journalDate",
    "bankId",
    "depositExchangeRate",
    "depositDate",
    "arCurrency1",
    "arCrossExchangeRate1",
    "arCurrency2",
    "arCrossExchangeRate2",
    "arCurrency3",
    "arCrossExchangeRate3",
    "controlDepositAmount",
    "controlNumberOfChecks"
})
public class CashRctSession {

    protected Integer sessionIdentity;
    protected String cashRctBranchId;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar journalDate;
    protected String bankId;
    protected BigDecimal depositExchangeRate;
    @XmlElement(required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar depositDate;
    protected String arCurrency1;
    protected BigDecimal arCrossExchangeRate1;
    protected String arCurrency2;
    protected BigDecimal arCrossExchangeRate2;
    protected String arCurrency3;
    protected BigDecimal arCrossExchangeRate3;
    protected BigDecimal controlDepositAmount;
    protected Integer controlNumberOfChecks;

    /**
     * Gets the value of the sessionIdentity property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSessionIdentity() {
        return sessionIdentity;
    }

    /**
     * Sets the value of the sessionIdentity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSessionIdentity(Integer value) {
        this.sessionIdentity = value;
    }

    /**
     * Gets the value of the cashRctBranchId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCashRctBranchId() {
        return cashRctBranchId;
    }

    /**
     * Sets the value of the cashRctBranchId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCashRctBranchId(String value) {
        this.cashRctBranchId = value;
    }

    /**
     * Gets the value of the journalDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getJournalDate() {
        return journalDate;
    }

    /**
     * Sets the value of the journalDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setJournalDate(XMLGregorianCalendar value) {
        this.journalDate = value;
    }

    /**
     * Gets the value of the bankId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankId() {
        return bankId;
    }

    /**
     * Sets the value of the bankId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankId(String value) {
        this.bankId = value;
    }

    /**
     * Gets the value of the depositExchangeRate property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getDepositExchangeRate() {
        return depositExchangeRate;
    }

    /**
     * Sets the value of the depositExchangeRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setDepositExchangeRate(BigDecimal value) {
        this.depositExchangeRate = value;
    }

    /**
     * Gets the value of the depositDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDepositDate() {
        return depositDate;
    }

    /**
     * Sets the value of the depositDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDepositDate(XMLGregorianCalendar value) {
        this.depositDate = value;
    }

    /**
     * Gets the value of the arCurrency1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArCurrency1() {
        return arCurrency1;
    }

    /**
     * Sets the value of the arCurrency1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArCurrency1(String value) {
        this.arCurrency1 = value;
    }

    /**
     * Gets the value of the arCrossExchangeRate1 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getArCrossExchangeRate1() {
        return arCrossExchangeRate1;
    }

    /**
     * Sets the value of the arCrossExchangeRate1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setArCrossExchangeRate1(BigDecimal value) {
        this.arCrossExchangeRate1 = value;
    }

    /**
     * Gets the value of the arCurrency2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArCurrency2() {
        return arCurrency2;
    }

    /**
     * Sets the value of the arCurrency2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArCurrency2(String value) {
        this.arCurrency2 = value;
    }

    /**
     * Gets the value of the arCrossExchangeRate2 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getArCrossExchangeRate2() {
        return arCrossExchangeRate2;
    }

    /**
     * Sets the value of the arCrossExchangeRate2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setArCrossExchangeRate2(BigDecimal value) {
        this.arCrossExchangeRate2 = value;
    }

    /**
     * Gets the value of the arCurrency3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getArCurrency3() {
        return arCurrency3;
    }

    /**
     * Sets the value of the arCurrency3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setArCurrency3(String value) {
        this.arCurrency3 = value;
    }

    /**
     * Gets the value of the arCrossExchangeRate3 property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getArCrossExchangeRate3() {
        return arCrossExchangeRate3;
    }

    /**
     * Sets the value of the arCrossExchangeRate3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setArCrossExchangeRate3(BigDecimal value) {
        this.arCrossExchangeRate3 = value;
    }

    /**
     * Gets the value of the controlDepositAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getControlDepositAmount() {
        return controlDepositAmount;
    }

    /**
     * Sets the value of the controlDepositAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setControlDepositAmount(BigDecimal value) {
        this.controlDepositAmount = value;
    }

    /**
     * Gets the value of the controlNumberOfChecks property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getControlNumberOfChecks() {
        return controlNumberOfChecks;
    }

    /**
     * Sets the value of the controlNumberOfChecks property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setControlNumberOfChecks(Integer value) {
        this.controlNumberOfChecks = value;
    }

}
