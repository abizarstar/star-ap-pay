
package com.invera.stratix.services.unDebt;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cashRctIdentity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="unappliedDebitDist" type="{http://stratix.invera.com/services}UnappliedDebitDist"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cashRctIdentity",
    "unappliedDebitDist"
})
@XmlRootElement(name = "ModifyUnappliedDebitDist")
public class ModUnpldDebitDist {

    @XmlElement(required = true)
    protected String cashRctIdentity;
    @XmlElement(required = true)
    protected UnappliedDebitDist unappliedDebitDist;

    /**
     * Gets the value of the cashRctIdentity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCashRctIdentity() {
        return cashRctIdentity;
    }

    /**
     * Sets the value of the cashRctIdentity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCashRctIdentity(String value) {
        this.cashRctIdentity = value;
    }

    /**
     * Gets the value of the unappliedDebitDist property.
     * 
     * @return
     *     possible object is
     *     {@link UnappliedDebitDist }
     *     
     */
    public UnappliedDebitDist getUnappliedDebitDist() {
        return unappliedDebitDist;
    }

    /**
     * Sets the value of the unappliedDebitDist property.
     * 
     * @param value
     *     allowed object is
     *     {@link UnappliedDebitDist }
     *     
     */
    public void setUnappliedDebitDist(UnappliedDebitDist value) {
        this.unappliedDebitDist = value;
    }

}
