
package com.invera.stratix.services.glDist;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cashRctIdentity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="glDist" type="{http://stratix.invera.com/services}GLDist"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cashRctIdentity",
    "glDist"
})
@XmlRootElement(name = "CreateGLDist")
public class CreateGLDist {

    @XmlElement(required = true)
    protected String cashRctIdentity;
    @XmlElement(required = true)
    protected GLDist glDist;

    /**
     * Gets the value of the cashRctIdentity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCashRctIdentity() {
        return cashRctIdentity;
    }

    /**
     * Sets the value of the cashRctIdentity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCashRctIdentity(String value) {
        this.cashRctIdentity = value;
    }

    /**
     * Gets the value of the glDist property.
     * 
     * @return
     *     possible object is
     *     {@link GLDist }
     *     
     */
    public GLDist getGlDist() {
        return glDist;
    }

    /**
     * Sets the value of the glDist property.
     * 
     * @param value
     *     allowed object is
     *     {@link GLDist }
     *     
     */
    public void setGlDist(GLDist value) {
        this.glDist = value;
    }

}
