
package com.invera.stratix.services.advPmt;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cashRctIdentity" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="advancePmtDist" type="{http://stratix.invera.com/services}AdvancePmtDist"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cashRctIdentity",
    "advancePmtDist"
})
@XmlRootElement(name = "CreateAdvancePmtDist")
public class CrtAdvPmtDist {

    @XmlElement(required = true)
    protected String cashRctIdentity;
    @XmlElement(required = true)
    protected AdvancePmtDist advancePmtDist;

    /**
     * Gets the value of the cashRctIdentity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCashRctIdentity() {
        return cashRctIdentity;
    }

    /**
     * Sets the value of the cashRctIdentity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCashRctIdentity(String value) {
        this.cashRctIdentity = value;
    }

    /**
     * Gets the value of the advancePmtDist property.
     * 
     * @return
     *     possible object is
     *     {@link AdvancePmtDist }
     *     
     */
    public AdvancePmtDist getAdvancePmtDist() {
        return advancePmtDist;
    }

    /**
     * Sets the value of the advancePmtDist property.
     * 
     * @param value
     *     allowed object is
     *     {@link AdvancePmtDist }
     *     
     */
    public void setAdvancePmtDist(AdvancePmtDist value) {
        this.advancePmtDist = value;
    }

}
