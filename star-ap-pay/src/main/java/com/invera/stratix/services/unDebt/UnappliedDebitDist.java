
package com.invera.stratix.services.unDebt;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for UnappliedDebitDist complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="UnappliedDebitDist">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="refItemNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="unappliedDebitDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="unappliedDebitAmount" type="{http://www.w3.org/2001/XMLSchema}decimal"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "UnappliedDebitDist", propOrder = {
    "refItemNumber",
    "unappliedDebitDescription",
    "unappliedDebitAmount"
})
public class UnappliedDebitDist {

    protected Integer refItemNumber;
    protected String unappliedDebitDescription;
    @XmlElement(required = true)
    protected BigDecimal unappliedDebitAmount;

    /**
     * Gets the value of the refItemNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRefItemNumber() {
        return refItemNumber;
    }

    /**
     * Sets the value of the refItemNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRefItemNumber(Integer value) {
        this.refItemNumber = value;
    }

    /**
     * Gets the value of the unappliedDebitDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnappliedDebitDescription() {
        return unappliedDebitDescription;
    }

    /**
     * Sets the value of the unappliedDebitDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnappliedDebitDescription(String value) {
        this.unappliedDebitDescription = value;
    }

    /**
     * Gets the value of the unappliedDebitAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getUnappliedDebitAmount() {
        return unappliedDebitAmount;
    }

    /**
     * Sets the value of the unappliedDebitAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setUnappliedDebitAmount(BigDecimal value) {
        this.unappliedDebitAmount = value;
    }

}
