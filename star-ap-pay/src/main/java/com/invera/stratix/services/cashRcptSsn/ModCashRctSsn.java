
package com.invera.stratix.services.cashRcptSsn;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cashRctSession" type="{http://stratix.invera.com/services}CashRctSession"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cashRctSession"
})
@XmlRootElement(name = "ModifyCashRctSession")
public class ModCashRctSsn {

    @XmlElement(required = true)
    protected CashRctSession cashRctSession;

    /**
     * Gets the value of the cashRctSession property.
     * 
     * @return
     *     possible object is
     *     {@link CashRctSession }
     *     
     */
    public CashRctSession getCashRctSession() {
        return cashRctSession;
    }

    /**
     * Sets the value of the cashRctSession property.
     * 
     * @param value
     *     allowed object is
     *     {@link CashRctSession }
     *     
     */
    public void setCashRctSession(CashRctSession value) {
        this.cashRctSession = value;
    }

}
