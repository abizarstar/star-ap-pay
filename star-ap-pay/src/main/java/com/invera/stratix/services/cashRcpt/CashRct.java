
package com.invera.stratix.services.cashRcpt;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CashRct complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CashRct">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="sessionIdentity" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="cashRctIdentity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="customerId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="customerCheckAmount" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="customerCheckNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cashRctDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CashRct", propOrder = {
    "sessionIdentity",
    "cashRctIdentity",
    "customerId",
    "customerCheckAmount",
    "customerCheckNumber",
    "cashRctDescription"
})
public class CashRct {

    protected Integer sessionIdentity;
    protected String cashRctIdentity;
    protected String customerId;
    protected BigDecimal customerCheckAmount;
    protected String customerCheckNumber;
    protected String cashRctDescription;

    /**
     * Gets the value of the sessionIdentity property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSessionIdentity() {
        return sessionIdentity;
    }

    /**
     * Sets the value of the sessionIdentity property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSessionIdentity(Integer value) {
        this.sessionIdentity = value;
    }

    /**
     * Gets the value of the cashRctIdentity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCashRctIdentity() {
        return cashRctIdentity;
    }

    /**
     * Sets the value of the cashRctIdentity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCashRctIdentity(String value) {
        this.cashRctIdentity = value;
    }

    /**
     * Gets the value of the customerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * Sets the value of the customerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerId(String value) {
        this.customerId = value;
    }

    /**
     * Gets the value of the customerCheckAmount property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCustomerCheckAmount() {
        return customerCheckAmount;
    }

    /**
     * Sets the value of the customerCheckAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCustomerCheckAmount(BigDecimal value) {
        this.customerCheckAmount = value;
    }

    /**
     * Gets the value of the customerCheckNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerCheckNumber() {
        return customerCheckNumber;
    }

    /**
     * Sets the value of the customerCheckNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerCheckNumber(String value) {
        this.customerCheckNumber = value;
    }

    /**
     * Gets the value of the cashRctDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCashRctDescription() {
        return cashRctDescription;
    }

    /**
     * Sets the value of the cashRctDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCashRctDescription(String value) {
        this.cashRctDescription = value;
    }

}
