
package com.invera.stratix.services.arDist;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cashRctIdentity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="arDist" type="{http://stratix.invera.com/services}ARDist"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cashRctIdentity",
    "arDist"
})
@XmlRootElement(name = "CreateARDist")
public class CreateARDist {

    protected String cashRctIdentity;
    @XmlElement(required = true)
    protected ARDist arDist;

    /**
     * Gets the value of the cashRctIdentity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCashRctIdentity() {
        return cashRctIdentity;
    }

    /**
     * Sets the value of the cashRctIdentity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCashRctIdentity(String value) {
        this.cashRctIdentity = value;
    }

    /**
     * Gets the value of the arDist property.
     * 
     * @return
     *     possible object is
     *     {@link ARDist }
     *     
     */
    public ARDist getArDist() {
        return arDist;
    }

    /**
     * Sets the value of the arDist property.
     * 
     * @param value
     *     allowed object is
     *     {@link ARDist }
     *     
     */
    public void setArDist(ARDist value) {
        this.arDist = value;
    }

}
