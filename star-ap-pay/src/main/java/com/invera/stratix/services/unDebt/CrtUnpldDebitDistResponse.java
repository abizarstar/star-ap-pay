
package com.invera.stratix.services.unDebt;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="refItemNumber" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "refItemNumber"
})
@XmlRootElement(name = "CreateUnappliedDebitDistResponse")
public class CrtUnpldDebitDistResponse {

    protected int refItemNumber;

    /**
     * Gets the value of the refItemNumber property.
     * 
     */
    public int getRefItemNumber() {
        return refItemNumber;
    }

    /**
     * Sets the value of the refItemNumber property.
     * 
     */
    public void setRefItemNumber(int value) {
        this.refItemNumber = value;
    }

}
