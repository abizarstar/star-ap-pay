<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>OCR AP Service Calls</title>

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<!-- jQuery library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<script src="assets/js/jquery.cookie.js"></script>
<script src="assets/js/jquery.fileDownload.js"></script>
<script src="assets/js/script.js"></script>

<style>
html, body {
	height: 100%;
}

.card-header {
	padding: 5px 10px;
}

.card-body {
	padding: 10px;
}

.btn {
	padding: 0.175rem .35rem;
	font-size: 0.9rem;
	border-radius: .15rem;
}

#overlay {
	background: #ffffff;
	color: #666666;
	position: absolute;
	height: 100%;
	width: 100%;
	z-index: 5000;
	top: 0;
	left: 0;
	float: left;
	text-align: center;
	padding-top: 5px;
	opacity: .80;
	padding-right: 20px;
}

.spinner {
	margin: 0 auto;
	height: 40px;
	width: 40px;
	animation: rotate 0.8s infinite linear;
	border: 3px solid firebrick;
	border-right-color: transparent;
	border-radius: 50%;
	float: right;
}

@
keyframes rotate { 0% {
	transform: rotate(0deg);
}
100%
{
transform
 
rotate
(360deg


);
}
}
</style>

<script>
	var inputData = {
		driver : "oracle1",
		url : "jdbcd:oracle:thin",
		host : "172.102.12.1",
		port : "5042",
		user : "mydb",
		password : " mydb123",
		conType : "F",
	};

	$.ajax({
		headers : ajaxHeader,
		method : "Post",
		data : JSON.stringify(inputData),
		//	url : rootAppName + '/rest/wkf/vldt-stp',
		//	url : rootAppName + '/rest/wkf/read-wkf-chk',
		//	url : rootAppName + '/rest/wkf/create-wkf',
		// url : rootAppName + '/rest/vchr-note/read',
		url : rootAppName + '/rest/supusr/read-all',
		success : function(response) {
		},
		error : function(jqXHR, textStatus, errorThrown) {
			if (jqXHR.status == 401) {
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
</script>

</head>
<body>

</body>
</html>