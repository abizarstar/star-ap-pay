<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>OCR AP Service Calls</title>

<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

<!-- jQuery library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
	integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
	crossorigin="anonymous"></script>
<script src="assets/js/jquery.cookie.js"></script>
<script src="assets/js/jquery.fileDownload.js"></script>
<script src="assets/js/script.js"></script>

<style>
html, body {
	height: 100%;
}

.card-header {
	padding: 5px 10px;
}

.card-body {
	padding: 10px;
}

.btn {
    padding: 0.175rem .35rem;
    font-size: 0.9rem;
    border-radius: .15rem;
}

#overlay {
	background: #ffffff;
	color: #666666;
	position: absolute;
	height: 100%;
	width: 100%;
	z-index: 5000;
	top: 0;
	left: 0;
	float: left;
	text-align: center;
	padding-top: 5px;
	opacity: .80;
	padding-right: 20px;
}

.spinner {
	margin: 0 auto;
	height: 40px;
	width: 40px;
	animation: rotate 0.8s infinite linear;
	border: 3px solid firebrick;
	border-right-color: transparent;
	border-radius: 50%;
	float: right;
}

@keyframes rotate {
    0% {
        transform: rotate(0deg);
    }
    100% {
        transform: rotate(360deg);
    }
}
</style>

<script>
	function addVendorInfo() {
		$('#overlay').show();
		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id' : 'abizarh',
			},
			url : rootAppName + '/rest/vendor/add',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				bankList: [{acctNo: "635859545", bnkNm: "ALCOA", bnkKey: "65985657", extRef: "TEST"}],
			cmpyId: "AP3",
			cry: "USD",
			cty: "USA",
			pymntMtd: "CCD",
			venId: "1070",
			venNm: "TEST",
			venLongNm: "TEST ABC"
			}),

			success : function(data) {

				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}
	
	
	function updateVendor() {
		$('#overlay').show();
		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id' : 'abizarh',
			},
			url : rootAppName + '/rest/vendor/update',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				cmpyId : "AP3",
				venId : "1010",
				acctNo : "62532542",
				bnkNm : "BNP Paribas",
				bnkKey : "96586565",
				cty : "USA",
				extRef : "",
			}),

			success : function(data) {

				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}

	function readSpecificVend() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id' : 'abizarh',
			},
			url : rootAppName + '/rest/vendor/read-ven',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				cmpyId : "",
				venId : "",
			}),

			success : function(data) {

				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}

	function delVendorInfo() {

		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id' : 'abizarh',
			},
			url : rootAppName + '/rest/vendor/delete',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				cmpyId : "AP3",
				venId : "1010",
				acctNo : "62532542"
			}),

			success : function(data) {

				$('#overlay').hide();
				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}

	function getCompany() {

		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id' : 'abizarh',
			},
			url : rootAppName + '/rest/vendor/read-cmpy',
			type : 'POST',
			dataType : 'json',
			success : function(data) {

				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {

				$('#overlay').hide();

				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}

	function getVendor() {
		$('#overlay').show();
		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id' : 'abizarh',
			},
			url : rootAppName + '/rest/vendor/read',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				cmpyId : "AP3"
			}),
			success : function(data) {

				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);

			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}

	function addBank() {

		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id' : 'abizarh',
			},
			url : rootAppName + '/rest/bank/upd',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				bnkCode : "BNP",
				bnkCty : "USA",
				bnkRoutNo : "23562985",
				bnkNm : "Bank of Scotland",
				bnkCity : "New York",
				bnkBrh : "New York",
				bnkCry : "USD",
				bnkSwiftCode : "",
				bnkHost : "",
				bnkFtpUser : "",
				bnkKeyPath : "",
				bnkKeyPath : "",
				bankAcctList : [ {
					bnkAcct : "Primary",
					bnkAcctDesc : "Main Account",
					bnkAcctNo : "123652545",
					isDefault : "false"
				}, {
					bnkAcct : "Secondary",
					bnkAcctDesc : "Main Account",
					bnkAcctNo : "123652544",
					isDefault : "true"
					
				} ]
			}),

			success : function(data) {

				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);

			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}

	function getBank() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id' : 'abizarh',
			},
			url : rootAppName + '/rest/bank/read',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				bnkCode : "AP3",
			}),

			success : function(data) {

				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}
	
	function getBankList() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id' : 'abizarh',
			},
			url : rootAppName + '/rest/bank/list',
			type : 'POST',
			dataType : 'json',
			success : function(data) {

				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}

	function addUser() {

		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id' : 'abizarh',
			},
			url : rootAppName + '/rest/user/add',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				usrId : "yogeshb",
				usrNm : "Yogesh Bhade",
				eml : "yogeshb@starsoftware.co",
				usrType : "STX",
				usrGrp : "1"
			}),
			success : function(data) {

				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}

	function getUser() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id' : 'abizarh',
			},
			url : rootAppName + '/rest/user/read-erp',
			type : 'POST',
			dataType : 'json',
			success : function(data) {

				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});
	}

	function getUserApp() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id' : 'abizarh',
			},
			url : rootAppName + '/rest/user/read-app',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				usrId : ""
			}),
			success : function(data) {

				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);

			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}

	function updateUser() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id' : 'abizarh',
			},
			url : rootAppName + '/rest/user/update',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				usrId : "yogeshb",
				usrNm : "Yogesh Bhade 1",
				eml : "yogeshb@starsoftware.co",
				usrType : "STX",
				usrGrp : "2"
			}),
			success : function(data) {

				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);

			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}

	function getMenuAll() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id' : 'abizarh',
			},
			url : rootAppName + '/rest/menu/read-all',
			type : 'POST',
			dataType : 'json',
			success : function(data) {

				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});
	}

	function getGroupMenu() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id' : 'abizarh',
			},
			url : rootAppName + '/rest/menu/read',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				grpId : "1"
			}),
			success : function(data) {

				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});
	}

	function addMenu() {
		$('#overlay').show();
		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id' : 'abizarh',
			},
			data : JSON.stringify({
				grpId : "2",
				menuLst : [ "1", "2", "3", "6" ],
				subMenuLst : [ {
					menuId : "6",
					subMenuId : "5"
				}, {
					menuId : "6",
					subMenuId : "6"
				} ]
			}),
			type : 'POST',
			dataType : 'json',
			url : rootAppName + '/rest/menu/update',
			success : function(data) {
				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			}
		});
	}
	
	function processInvoices() {
		$('#overlay').show();
		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id' : 'ruchit',
			},
			data : JSON.stringify({
				reqId : "TEST22111",
				cmpyId: "AP3",
				invList : ["VR-94", "VR-210"],
				reqUserBy : "",
				reqActnBy : "",
				paySts : "P"
			}),
			type : 'POST',
			dataType : 'json',
			url : rootAppName + '/rest/payment/add',
			success : function(data) {
				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			}
		});
	
}
	
	
	function getPayment() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id' : 'abizarh',
			},
			url : rootAppName + '/rest/payment/read',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				reqId : "",
				paySts:""
			}),
			success : function(data) {

				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});
	}
	
	function getPaymentApprove() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id' : 'abizarh',
			},
			url : rootAppName + '/rest/payment/read',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				reqId : "",
				paySts: "A"
			}),
			success : function(data) {

				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});
	}
	
	
	function getInvoices() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id' : 'abizarh',
			},
			url : rootAppName + '/rest/payment/read-inv',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				venIdFrm : "",
				venIdTo: "",
				payMthd: "CCD"
			}),
			success : function(data) {

				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});
	}
	
	function updateStatus() {
		$('#overlay').show();
		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id' : 'abizarh',
			},
			data : JSON.stringify({
				reqId : "TEST123",
				cmpyId: "AP3",
				invList : [ "1"],
				reqUserBy : "",
				reqActnBy : "yogeshb",
				paySts : "S"
			}),
			type : 'POST',
			dataType : 'json',
			url : rootAppName + '/rest/payment/upd-sts',
			success : function(data) {
				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			}
		});
	
}
	
	
	function updateStatusApprove() {
		$('#overlay').show();
		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id' : 'abizarh',
			},
			data : JSON.stringify({
				reqId : "TEST123",
				reqUserBy : "",
				reqActnBy : "",
				paySts : "A"
			}),
			type : 'POST',
			dataType : 'json',
			url : rootAppName + '/rest/payment/upd-sts',
			success : function(data) {
				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			}
		});
	
}
	
	function getCountry() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id' : 'abizarh',
			},
			url : rootAppName + '/rest/common/read-cty',
			type : 'POST',
			dataType : 'json',
			success : function(data) {

				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});
	}
		
	
	function addApproverData() {
		$('#overlay').show();
		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id' : 'abizarh',
			},
			data : JSON.stringify({
				usrList : ["abizarh", "yogeshb"],
				venList : [ "1010", "1020"],
				cond : "AND",
				amtList : [{frmAmoumt:"100", toAmount:"1000"}, {frmAmoumt:"3000", toAmount:"5000"}],
			}),
			type : 'POST',
			dataType : 'json',
			url : rootAppName + '/rest/condition/add',
			success : function(data) {
				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			}
		});
	
}
	
	
	function getConditionData() {
		$('#overlay').show();
		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id' : 'abizarh',
			},
			data : JSON.stringify({
				usrId:"abizhar"
			}),
			type : 'POST',
			dataType : 'json',
			url : rootAppName + '/rest/condition/read',
			success : function(data) {
				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			}
		});
	
}
	
	
	function addCompBank() {

		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id' : 'abizarh',
			},
			url : rootAppName + '/rest/company/add',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				cmpyId : "AP3",
				bnkCode : "AP3",
			}),

			success : function(data) {

				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);

			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}

	function getCompBank() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id' : 'abizarh',
			},
			url : rootAppName + '/rest/company/read',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				cmpyId : "",
			}),

			success : function(data) {

				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}
	
	function generateACH() {
		$('#overlay').show();

		$.ajax({
			headers :ajaxHeader,
			url : rootAppName + '/rest/ach/read',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				cmpyId : "",
			}),

			success : function(data) {

				$('#overlay').hide();

				var data1 = data;

				$("#output").html(data);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}
	
	
	function validateCon() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id' : 'abizarh',
			},
			url : rootAppName + '/rest/common/validate-con',
			type : 'POST',
			dataType : 'json',
			success : function(data) {

				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}
	
	function createSession() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id': localStorage.getItem("starOcrUserId"),
				'auth-token': localStorage.getItem("starOcrAuthToken"),
				'app-host' : localStorage.getItem("starOcrAppHost"),
				'app-port' : localStorage.getItem("starOcrAppPort"),
				'cmpy-id' : localStorage.getItem("starOcrCmpyId"),
			},
			url : rootAppName + '/rest/rcpt/crtssn',
			type : 'POST',
			dataType : 'json',
			success : function(data) {

				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}
	
	function createReceipt() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id': localStorage.getItem("starOcrUserId"),
				'auth-token': localStorage.getItem("starOcrAuthToken"),
				'app-host' : localStorage.getItem("starOcrAppHost"),
				'app-port' : localStorage.getItem("starOcrAppPort"),
				'cmpy-id' : localStorage.getItem("starOcrCmpyId"),
			},
			url : rootAppName + '/rest/rcpt/crtRcpt',
			type : 'POST',
			dataType : 'json',
			success : function(data) {

				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}
	
	function createARDist() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id': localStorage.getItem("starOcrUserId"),
				'auth-token': localStorage.getItem("starOcrAuthToken"),
				'app-host' : localStorage.getItem("starOcrAppHost"),
				'app-port' : localStorage.getItem("starOcrAppPort"),
				'cmpy-id' : localStorage.getItem("starOcrCmpyId"),
			},
			url : rootAppName + '/rest/rcpt/arDist',
			type : 'POST',
			dataType : 'json',
			success : function(data) {

				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}
	
	
	function createAdvPmt() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id': localStorage.getItem("starOcrUserId"),
				'auth-token': localStorage.getItem("starOcrAuthToken"),
				'app-host' : localStorage.getItem("starOcrAppHost"),
				'app-port' : localStorage.getItem("starOcrAppPort"),
				'cmpy-id' : localStorage.getItem("starOcrCmpyId"),
			},
			url : rootAppName + '/rest/rcpt/advPmt',
			type : 'POST',
			dataType : 'json',
			success : function(data) {

				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}
	
	function createUnappliedDebit() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id': localStorage.getItem("starOcrUserId"),
				'auth-token': localStorage.getItem("starOcrAuthToken"),
				'app-host' : localStorage.getItem("starOcrAppHost"),
				'app-port' : localStorage.getItem("starOcrAppPort"),
				'cmpy-id' : localStorage.getItem("starOcrCmpyId"),
			},
			url : rootAppName + '/rest/rcpt/unDebit',
			type : 'POST',
			dataType : 'json',
			success : function(data) {

				$('#overlay').hide();

				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}
	
	
	function getInvoiceInfo() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id': localStorage.getItem("starOcrUserId"),
				'auth-token': localStorage.getItem("starOcrAuthToken"),
				'app-host' : localStorage.getItem("starOcrAppHost"),
				'app-port' : localStorage.getItem("starOcrAppPort"),
				'cmpy-id' : localStorage.getItem("starOcrCmpyId"),
			},
			url : rootAppName + '/rest/bai/inv',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				invNo : "CHI SI-2770",
				checkNo:"65985",
				checkDesc:"TEST-ABC",
				checkAmt:"65",
				bnk:"CTB"
			}),
			success : function(data) {

				$('#overlay').hide();
				
				localStorage.setItem("starOcrAuthToken", response.authToken);
				
				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}
	
	function updateIRCVouhcer() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id': localStorage.getItem("starOcrUserId"),
				'auth-token': localStorage.getItem("starOcrAuthToken"),
				'app-host' : localStorage.getItem("starOcrAppHost"),
				'app-port' : localStorage.getItem("starOcrAppPort"),
				'cmpy-id' : localStorage.getItem("starOcrCmpyId"),
			},
			url : rootAppName + '/rest/vour/irc',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				invNo : "CHI SI-2770",
				checkNo:"65985",
				checkDesc:"TEST-ABC",
				checkAmt:"65",
				bnk:"CTB"
			}),
			success : function(data) {

				$('#overlay').hide();
				
				localStorage.setItem("starOcrAuthToken", response.authToken);
				
				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}
	
	function updateGL() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id': localStorage.getItem("starOcrUserId"),
				'auth-token': localStorage.getItem("starOcrAuthToken"),
				'app-host' : localStorage.getItem("starOcrAppHost"),
				'app-port' : localStorage.getItem("starOcrAppPort"),
				'cmpy-id' : localStorage.getItem("starOcrCmpyId"),
			},
			url : rootAppName + '/rest/vour/gl-dist',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				invNo : "CHI SI-2770",
				checkNo:"65985",
				checkDesc:"TEST-ABC",
				checkAmt:"65",
				bnk:"CTB"
			}),
			success : function(data) {

				$('#overlay').hide();
				
				localStorage.setItem("starOcrAuthToken", response.authToken);
				
				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}
	
	function uploadFile() {
		$("#uploadForm").submit(function(e) {

			e.preventDefault(); // avoid to execute the actual submit of the form

			console.log($('#input-file').prop('files'));

			var formData = new FormData();

			var filterdata = [];

			var formElement = document.forms.namedItem("uploadForm");

			var formData = new FormData(formElement);

			$.each(formData.getAll("file"), function(index, value) {
				if (value.name != "" && value.size > 0) {
					upload = true;
					fileName = value.name;
				}
			});

			var jsonArrayMain = [];

			var JSONObj = new Object();

			JSONObj.fldId = 'PO'
			JSONObj.fldVal = '1010'

			filterdata.push(JSONObj);

			console.log(filterdata);

			/* JSONObjMain.fldList = filterdata */

			jsonArrayMain.push(filterdata);

			console.log(jsonArrayMain);

			var filterdata = [];
			var JSONObj = new Object();

			JSONObj.fldId = 'PO'
			JSONObj.fldVal = '1011'

			filterdata.push(JSONObj);

			jsonArrayMain.push(filterdata);

			/* console.log(JSONObjMain); */
			console.log(jsonArrayMain);

			formData.append('stmtTyp', '1');

			$.ajax({
				headers : {
					'user-id' : 'abizhar',
					'auth-token' : localStorage.getItem("docMgmtAuthToken"),
				},

				type : "POST",
				url : rootAppName + '/rest/bai/pdf',
				data : formData, // serializes the form's elements.
				enctype : 'multipart/form-data',
				cache : false,
				processData : false,
				contentType : false,
				success : function(data) {
					alert(data); // show response from the php script.
				}
			});

		});
	}
	
	
	function getVouchers() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id': localStorage.getItem("starOcrUserId"),
				'auth-token': localStorage.getItem("starOcrAuthToken"),
				'app-host' : localStorage.getItem("starOcrAppHost"),
				'app-port' : localStorage.getItem("starOcrAppPort"),
				'cmpy-id' : localStorage.getItem("starOcrCmpyId"),
			},
			url : rootAppName + '/rest/cstmdoc/readdoc',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				trnSts : "",
			}),
			success : function(data) {

				$('#overlay').hide();
				
				localStorage.setItem("starOcrAuthToken", response.authToken);
				
				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}
	
	function getVouchersRecon() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id': localStorage.getItem("starOcrUserId"),
				'auth-token': localStorage.getItem("starOcrAuthToken"),
				'app-host' : localStorage.getItem("starOcrAppHost"),
				'app-port' : localStorage.getItem("starOcrAppPort"),
				'cmpy-id' : localStorage.getItem("starOcrCmpyId"),
			},
			url : rootAppName + '/rest/vchr/recon',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				cmpyId : "SQ1",
				brhId : "RTH",
				venId : "1040",
			}),
			success : function(data) {

				$('#overlay').hide();
				
				localStorage.setItem("starOcrAuthToken", response.authToken);
				
				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}
	
	
	
	function getVouchersConfirmed() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id': localStorage.getItem("starOcrUserId"),
				'auth-token': localStorage.getItem("starOcrAuthToken"),
				'app-host' : localStorage.getItem("starOcrAppHost"),
				'app-port' : localStorage.getItem("starOcrAppPort"),
				'cmpy-id' : localStorage.getItem("starOcrCmpyId"),
			},
			url : rootAppName + '/rest/cstmdoc/readdoc',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				trnSts : "S",
			}),
			success : function(data) {

				$('#overlay').hide();
				
				localStorage.setItem("starOcrAuthToken", response.authToken);
				
				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}
	
	function getVouchersApproved() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id': localStorage.getItem("starOcrUserId"),
				'auth-token': localStorage.getItem("starOcrAuthToken"),
				'app-host' : localStorage.getItem("starOcrAppHost"),
				'app-port' : localStorage.getItem("starOcrAppPort"),
				'cmpy-id' : localStorage.getItem("starOcrCmpyId"),
			},
			url : rootAppName + '/rest/cstmdoc/readdoc',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				trnSts : "A",
			}),
			success : function(data) {

				$('#overlay').hide();
				
				localStorage.setItem("starOcrAuthToken", response.authToken);
				
				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}
	
	
	function getStmt() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id': localStorage.getItem("starOcrUserId"),
				'auth-token': localStorage.getItem("starOcrAuthToken"),
				'app-host' : localStorage.getItem("starOcrAppHost"),
				'app-port' : localStorage.getItem("starOcrAppPort"),
				'cmpy-id' : localStorage.getItem("starOcrCmpyId"),
			},
			url : rootAppName + '/rest/bai/read',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				upldOnFrm : "10/02/2020",
				upldOnTo : "10/02/2020",
			}),
			success : function(data) {

				$('#overlay').hide();
				
				localStorage.setItem("starOcrAuthToken", response.authToken);
				
				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}
	
	
	function getLockBox() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id': localStorage.getItem("starOcrUserId"),
				'auth-token': localStorage.getItem("starOcrAuthToken"),
				'app-host' : localStorage.getItem("starOcrAppHost"),
				'app-port' : localStorage.getItem("starOcrAppPort"),
				'cmpy-id' : localStorage.getItem("starOcrCmpyId"),
			},
			url : rootAppName + '/rest/bai/read-lckbx',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				upldOnFrm : "10/14/2020",
				upldOnTo : "10/14/2020",
			}),
			success : function(data) {

				$('#overlay').hide();
				
				localStorage.setItem("starOcrAuthToken", response.authToken);
				
				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}
	
	
	function getCheckLot() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id': localStorage.getItem("starOcrUserId"),
				'auth-token': localStorage.getItem("starOcrAuthToken"),
				'app-host' : localStorage.getItem("starOcrAppHost"),
				'app-port' : localStorage.getItem("starOcrAppPort"),
				'cmpy-id' : localStorage.getItem("starOcrCmpyId"),
			},
			url : rootAppName + '/rest/check/read',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				bnkCode : "CTB",
				acctId : "0810008637",
			}),
			success : function(data) {

				$('#overlay').hide();
				
				localStorage.setItem("starOcrAuthToken", response.authToken);
				
				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}
	
	function validatePO() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id': localStorage.getItem("starOcrUserId"),
				'auth-token': localStorage.getItem("starOcrAuthToken"),
				'app-host' : localStorage.getItem("starOcrAppHost"),
				'app-port' : localStorage.getItem("starOcrAppPort"),
				'cmpy-id' : localStorage.getItem("starOcrCmpyId"),
			},
			url : rootAppName + '/rest/cstmdoc/validate-po',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				poNo : "1467",
				poItm : "",
			}),
			success : function(data) {

				$('#overlay').hide();
				
				localStorage.setItem("starOcrAuthToken", response.authToken);
				
				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}
	
	
	function checkEmail() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id': localStorage.getItem("starOcrUserId"),
				'auth-token': localStorage.getItem("starOcrAuthToken"),
				'app-host' : localStorage.getItem("starOcrAppHost"),
				'app-port' : localStorage.getItem("starOcrAppPort"),
				'cmpy-id' : localStorage.getItem("starOcrCmpyId"),
				'app-token' : localStorage.getItem("starOcrAppToken"),
			},
			url : rootAppName + '/rest/sendmail/prop',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				reqId : "RQ1",
			}),
			success : function(data) {

				$('#overlay').hide();
				
				localStorage.setItem("starOcrAuthToken", response.authToken);
				
				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}
	
	
	function getOpenItems() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id': localStorage.getItem("starOcrUserId"),
				'auth-token': localStorage.getItem("starOcrAuthToken"),
				'app-host' : localStorage.getItem("starOcrAppHost"),
				'app-port' : localStorage.getItem("starOcrAppPort"),
				'cmpy-id' : localStorage.getItem("starOcrCmpyId"),
			},
			url : rootAppName + '/rest/ap/opn-itm',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				venId : "",
				brh : "",
				cry : "",
			}),
			success : function(data) {

				$('#overlay').hide();
				
				localStorage.setItem("starOcrAuthToken", response.authToken);
				
				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}
	
	function getOverviewDashboard() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id': localStorage.getItem("starOcrUserId"),
				'auth-token': localStorage.getItem("starOcrAuthToken"),
				'app-host' : localStorage.getItem("starOcrAppHost"),
				'app-port' : localStorage.getItem("starOcrAppPort"),
				'cmpy-id' : localStorage.getItem("starOcrCmpyId"),
			},
			url : rootAppName + '/rest/dshbrd/ovrvw',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				venId : "",
				brh : "",
				cry : "",
			}),
			success : function(data) {

				$('#overlay').hide();
				
				localStorage.setItem("starOcrAuthToken", response.authToken);
				
				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}
	
	
	function uploadDocumentToServer() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id': localStorage.getItem("starOcrUserId"),
				'auth-token': localStorage.getItem("starOcrAuthToken"),
				'app-host' : localStorage.getItem("starOcrAppHost"),
				'app-port' : localStorage.getItem("starOcrAppPort"),
				'cmpy-id' : localStorage.getItem("starOcrCmpyId"),
				'app-token' : localStorage.getItem("starOcrAppToken"),
			},
			url : rootAppName + '/rest/doc-service/get-doc',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				venId : "",
				brh : "",
				cry : "",
			}),
			success : function(data) {

				$('#overlay').hide();
				
				localStorage.setItem("starOcrAuthToken", response.authToken);
				
				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}
	
	
	function configEml() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id': localStorage.getItem("starOcrUserId"),
				'auth-token': localStorage.getItem("starOcrAuthToken"),
				'app-host' : localStorage.getItem("starOcrAppHost"),
				'app-port' : localStorage.getItem("starOcrAppPort"),
				'cmpy-id' : localStorage.getItem("starOcrCmpyId"),
				'app-token' : localStorage.getItem("starOcrAppToken"),
			},
			url : rootAppName + '/rest/config-eml/read',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				venId : "",
				brh : "",
				cry : "",
			}),
			success : function(data) {

				$('#overlay').hide();
				
				localStorage.setItem("starOcrAuthToken", response.authToken);
				
				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}
	
	
	function exportExcel() {
		$('#overlay').show();

		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id': localStorage.getItem("starOcrUserId"),
				'auth-token': localStorage.getItem("starOcrAuthToken"),
				'app-host' : localStorage.getItem("starOcrAppHost"),
				'app-port' : localStorage.getItem("starOcrAppPort"),
				'cmpy-id' : localStorage.getItem("starOcrCmpyId"),
				'app-token' : localStorage.getItem("starOcrAppToken"),
			},
			url : rootAppName + '/rest/export/get-recon',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				venId : "1010",
				brhId : "",
				cry : "EUR",
			}),
			success : function(data) {

				$('#overlay').hide();
				
				localStorage.setItem("starOcrAuthToken", response.authToken);
				
				var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

				$("#output").html(data1);
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}
	
	function genRemitInvoices() {
		
		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id': localStorage.getItem("starOcrUserId"),
				'auth-token': localStorage.getItem("starOcrAuthToken"),
				'app-host' : localStorage.getItem("starOcrAppHost"),
				'app-port' : localStorage.getItem("starOcrAppPort"),
				'cmpy-id' : localStorage.getItem("starOcrCmpyId"),
				'app-token' : localStorage.getItem("starOcrAppToken"),
			},
			data : JSON.stringify({
				vchrArr : ["VR-1146", "VR-1206"],
			}),
			type : 'POST',
			dataType : 'json',
			url : rootAppName + '/rest/remit/pdf',
			success : function(data) {
					
			}
		});
	
}
	
	function getGL() {

		$.ajax({
			headers: ajaxHeader,
			url : rootAppName + '/rest/gl/read',
			type : 'POST',
			dataType : 'json',
			success : function(data) {

				getGL();
			},
			error : function(data) {
				console.log(data);
				$("#mainLoader").hide();
			}

		});

	}
	
</script>

</head>
<body>
	<div class='container-fluid h-100'>
		<div class='h-100' style='padding: 10px'>
			<div class='row h-100'>
				<div class='col-md-8'>
					
					<div class="card text-white bg-secondary mb-3">
						<div class="card-header">Bank Statement</div>
						<div class="card-body">
							<button class="btn btn-light" onclick='getStmt()'>Get
								Get Statement</button>	
								
								<button class="btn btn-light" onclick='getLockBox()'>Get
								Get Lockbox</button>
								
								<button class="btn btn-light" onclick='validatePO()'>Get
								Validate PO</button>	
								
								<button class="btn btn-light" onclick='getCheckLot()'>Get
								GET Check LOT</button>
								
								<button class="btn btn-light" onclick='checkEmail()'>Get
								Check Email</button>
								
								<button class="btn btn-light" onclick='getOpenItems()'>Get Open Item</button>
								
								
								<button class="btn btn-light" onclick='getOverviewDashboard()'>Dashboard</button>
								
								<button class="btn btn-light" onclick='uploadDocumentToServer()'>Get Document</button>
								
								<button class="btn btn-light" onclick='configEml()'>Config Email</button>
								
								<button class="btn btn-light" onclick='exportExcel()'>Export Excel</button>
								
								<button class="btn btn-light" onclick='genRemitInvoices()'>Generate Remittance</button>
								
								<button class="btn btn-light" onclick='getGL()'>GET GL</button>
								
						</div>
					</div>
					
					<div class="card text-white bg-secondary mb-3">
						<div class="card-header">Receipt</div>
						<div class="card-body">
							<button class="btn btn-light" onclick='createSession()'>Get
								Create Receipt Session</button>
								
							<button class="btn btn-light" onclick='createReceipt()'>Get
								Create Receipt</button>
								
							<button class="btn btn-light" onclick='createARDist()'>Get
								Create AR Distribution</button>
								
							<button class="btn btn-light" onclick='createAdvPmt()'>
								Create Adv Pmt</button>
								
							<button class="btn btn-light" onclick='createUnappliedDebit()'>
								Un Applied Debit</button>	
								
						</div>
					</div>
					
					<div class="card text-white bg-secondary mb-3">
						<div class="card-header">Invoice Detail</div>
						<div class="card-body">
								<h1>File Upload Example</h1>
			
				<!-- action="rest/docupload/pdf" -->
				<form id='uploadForm' name='uploadForm' enctype="multipart/form-data">
			
					<p>
						Select a file : <input type="file" name="file" id='input-file'
							size="45" accept=".pdf" multiple />
					</p>
					<!-- <input type='submit'></input> -->
					<button value="Upload PDF" onclick='uploadFile()' />
					upload
					</button>
			
				</form>
				</div>
					
					<div class="card text-white bg-secondary mb-3">
						<div class="card-header">Invoice Detail</div>
						<div class="card-body">
							<button class="btn btn-light" onclick='getInvoiceInfo()'>Get Invoice Info</button>
							
							<button class="btn btn-light" onclick='updateGL()'>Update GL</button>
							
							<button class="btn btn-light" onclick='updateIRCVouhcer()'>Update IRC</button>
							
							<button class="btn btn-light" onclick='getVouchers()'>Get Vouchers All</button>
							
							<button class="btn btn-light" onclick='getVouchersRecon()'>Get Vouchers Recon</button>
							
							<button class="btn btn-light" onclick='getVouchersConfirmed()'>Get Vouchers Confirmed</button>
							
							<button class="btn btn-light" onclick='getVouchersApproved()'>Get Vouchers Approved</button>
							
							<button class="btn btn-light" onclick='getWorkflowHis()'>Get Vouchers Approved</button>
								
						</div>
					</div>
					
					<div class="card text-white bg-secondary mb-3">
						<div class="card-header">Common</div>
						<div class="card-body">
							<button class="btn btn-light" onclick='getCountry()'>Get
								Country Information</button>
						</div>
					</div>
										
					
					<div class="card text-white bg-secondary mb-3">
						<div class="card-header">Vendor</div>
						<div class="card-body">
							<button class="btn btn-light" onclick='addVendorInfo()'>Add
								Vendor Information</button>
							&nbsp;&nbsp;
							<button class="btn btn-light" onclick='getCompany()'>Get
								Company</button>
							&nbsp;&nbsp;
							<button class="btn btn-light" onclick='getVendor()'>Get
								Vendor</button>
							&nbsp;&nbsp;
							<button class="btn btn-light" onclick='delVendorInfo()'>Delete
								Vendor</button>
							&nbsp;&nbsp;
							<button class="btn btn-light" onclick='readSpecificVend()'>Read
								Specific Vendor</button>
								
							&nbsp;&nbsp;
							<button class="btn btn-light" onclick='updateVendor()'>Update Vendor</button>
						</div>
					</div>

					<div class="card text-white bg-secondary mb-3">
						<div class="card-header">Bank</div>
						<div class="card-body">
							<button class="btn btn-light" onclick='addBank()'>Add
								Bank</button>
							&nbsp;&nbsp;
							<button class="btn btn-light" onclick='getBank()'>Get
								Bank</button>
								&nbsp;&nbsp;
							<button class="btn btn-light" onclick='getBankList()'>Get
								Bank List</button>
						</div>
					</div>
					
					<div class="card text-white bg-secondary mb-3">
						<div class="card-header">Company</div>
						<div class="card-body">
							<button class="btn btn-light" onclick='addCompBank()'>Add
								Company Bank</button>
							&nbsp;&nbsp;
							<button class="btn btn-light" onclick='getCompBank()'>Get
								Company Bank</button>
						</div>
					</div>


					<div class="card text-white bg-secondary mb-3">
						<div class="card-header">User</div>
						<div class="card-body">
							<button class="btn btn-light" onclick='getUser()'>Get
								User STX List</button>
							&nbsp;&nbsp;
							<button class="btn btn-light" onclick='addUser()'>Add
								User</button>
							&nbsp;&nbsp;
							<button class="btn btn-light" onclick='getUserApp()'>Get
								Application User List</button>
							&nbsp;&nbsp;
							<button class="btn btn-light" onclick='updateUser()'>Update
								User</button>
						</div>
					</div>

					<div class="card text-white bg-secondary mb-3">
						<div class="card-header">Menu Information</div>
						<div class="card-body">
							<button class="btn btn-light" onclick='getMenuAll()'>Read
								Menu All</button>
							&nbsp;&nbsp;
							<button class="btn btn-light" onclick='getGroupMenu()'>Get
								Group Menu</button>
							&nbsp;&nbsp;
							<button class="btn btn-light" onclick='addMenu()'>Add
								Update Menu</button>
						</div>
					</div>
					
					<div class="card text-white bg-secondary mb-3">
						<div class="card-header">Payment</div>
						<div class="card-body">
							<button class="btn btn-light" onclick='getInvoices()'>Get Invoices</button>
							&nbsp;&nbsp;
							<button class="btn btn-light" onclick='processInvoices()'>Save Payment Invoices</button>
							&nbsp;&nbsp;
							<button class="btn btn-light" onclick='getPayment()'>Read Payment</button>
							&nbsp;&nbsp;
							<button class="btn btn-light" onclick='getPaymentApprove()'>Read Payment Approve</button>
							&nbsp;&nbsp;
							<button class="btn btn-light" onclick='updateStatus()'>Update Payment Status PA</button>
							&nbsp;&nbsp;
							<button class="btn btn-light" onclick='updateStatusApprove()'>Approve/Reject Status</button>
						</div>
					</div>
					
					<div class="card text-white bg-secondary mb-3">
						<div class="card-header">Approver Setup</div>
						<div class="card-body">
							<button class="btn btn-light" onclick='addApproverData()'>Add Approver Details</button>
							&nbsp;&nbsp;
							<button class="btn btn-light" onclick='getConditionData()'>Get Condition</button>
						</div>
					</div>
					
					<div class="card text-white bg-secondary mb-3">
						<div class="card-header">ACH</div>
						<div class="card-body">
							<button class="btn btn-light" onclick='generateACH()'>Generate ACH File</button>
						</div>
					</div>
					
					<div class="card text-white bg-secondary mb-3">
						<div class="card-header">Validate Connection</div>
						<div class="card-body">
							<button class="btn btn-light" onclick='validateCon()'>Validate</button>
						</div>
					</div>

				</div>

				<div class='col-md-4'>
					<div id='output'
						style="height: 700px; width: 100%; font-size: 13px;font-weight:600;clear: both; border: 1px solid lightgrey; word-wrap: break-word; white-space: pre;overflow-y: auto;"></div>
					<div id="overlay" style='display: none;'>
						<div class="spinner"></div>
					</div>

				</div>
			</div>
		</div>
	</div>
</body>
</html>