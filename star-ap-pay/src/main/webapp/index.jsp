<html>
<head>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="assets/js/jquery.cookie.js"></script>
<script src="assets/js/jquery.fileDownload.js"></script>

<script>
	$(function() {
		/*  $.cookie('auth-token', localStorage.getItem("docMgmtAuthToken"));
		$.cookie('user-id', localStorage.getItem("docMgmtUserId"));  */
	});
	$(document).on("click", "a.fileDownloadPromise", function() {

		$.cookie('auth-token', localStorage.getItem("docMgmtAuthToken"));
		$.cookie('user-id', localStorage.getItem("docMgmtUserId"));

		$.fileDownload($(this).prop('href')).done(function() {
			alert('File download a success!');
		}).fail(function() {
			alert(' Server Error, unable to downlaod the file');
		});

		//$.cookie("auth-token", null);
		//$.cookie("user-id", null);

		return false; //this is critical to stop the click event which will trigger a normal file download
	});
</script>

<script>

var tokenValue = '';

var usr = '';

var appHost = '';

var appPort = '';


	function getData() {
		
		var usrNm = $("#usrName").val();
		
		$
				.ajax({
					url : rootAppName + '/rest/hello/' + usrNm,
					type : 'GET',
					success : function(data) {
						
						$("#output").html(data);
					},
					error : function(data) {
						console.log(data);
						$("#mainLoader").hide();
					}
				});
	}
	
	
	function addType() {
		$
				.ajax({
					headers : {
						'Accept' : 'application/json',
						'Content-Type' : 'application/json',
						'Authorization' : 'eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJleUpoYkdjaU9pSklVekkxTmlKOS5leUp6ZFdJaU9pSndZWE56ZDI5eVpDSXNJbVY0Y0NJNk1UVTRPVFExT0RnMk5IMC5CcElNNTNEZi1UWDR1bkpfYi1xYi1vUzRmQnk3d2cweVo4M1dfcTF6TkxnIiwiZXhwIjoxNTg5NDU4OTUyfQ.zHBkSovaX5zxrFXThVLuQzRQdNSa4ZoKS2tb56rLtUU'
					},
					data : JSON.stringify({
						typId : "",
						folId : "INV",
						typNm : "Accounts Payable2"
					}),
					url : '/doc-mgmt/rest/docfol/addType',
					type : 'POST',
					dataType : 'json',
					success : function(data) {

					},
					error : function(data) {
						console.log(data);
						$("#mainLoader").hide();
					}
				});
	}
	
	

	function callAuth()
	{	
	
		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json'
			},
			url : rootAppName + '/rest/authvour/auth',
			type : 'POST',
			dataType : 'json',
			data : JSON.stringify({
				usrId:"abizhar",
				usrNm:"mayankc",	
				password:"abizhar"
			}),
						
		   success: function(data) {
		    	
			   $("#output").html(data);
			   		  			   
		    	/* var arrResponse = JSON.parse(data.trim());
		    	
		    	tokenValue = arrResponse.authToken;
		    	alert(tokenValue)
		    	//usr = $("#user").val();
		    	appHost = arrResponse.appHost;
		    	appPort = arrResponse.appPort; */
		    	
		    	console.log(data); 
		    	//alert(data);
		    },
		error : function(data) {
			console.log(data);
			$("#mainLoader").hide();
		}
		
		});
		
	}
	
	function callCreateVoucher()
	{	
				
		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json',
				'user-id': 'invera',
				'auth-token': 'data:blowfish/octet-stream;base64,PpqBHpJC0CzTbqi7cCbBdM7KI/owgTxm59UAv3ESchsLvk2yRMmYG7pAZXI4sO+WMiM3YzjE1oWPZaRc5WlbcKqq3I8TSRsQZHhxAoH5UwMAN8KpCScGZDEm/htSNxPC',
				'app-host' : '172.20.35.229',
				'app-port' : '60977',
				'cmpy-id' : 'SQ8',
				'app-token': 'eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJzdGFyMTIzNDUiLCJleHAiOjE2MTU4MzcyMzh9.6BAcEnf6QB8EHaSc6hsXTtqXKop61L0kCbD7_fWJj0w'
			},
			url : 'http://localhost:8080/star-ocr-ap/rest/vour/crtvour',
			type : 'POST',
			dataType : 'json',
		    data: JSON.stringify({
		    	vchrCmpyId : 'SQ8',
				vchrPfx : 'VR',
				vchrCtlNo  : '10',
				vchrInvNo  : '52145-12',
				vchrVenId : '1010',
				vchrVenNm : 'TEST',
				vchrBrh : 'CHI',
				vchrAmt : '100',
				vchrExtRef : '',
				vchrInvDt : '10/02/2021',
				vchrInvDtStr : "",
				vchrDueDt : '11/02/2021',
				vchrDueDtStr : "",
				vchrPayTerm : '',
				vchrCry : 'USD',
				vchrCat : 'MAT',
				glData : [],
				poNo : '',
				costRecon : [],
				discDt: '',
				discAmt: '',
				voyage: '',
				vchrRmk: '',
			}),
		    success: function(data) {
		    	
		    	var arrResponse = JSON.parse(data.trim());
		    	
		    	tokenValue = arrResponse.authToken;
		    	usr = $("#user").val();
		    	
		    	
		    	console.log(data);
		    	alert(data);
		    }
		});
		
	}
	
	function callDeleteVoucher()
	{	
				
		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json'
			},
			url : rootAppName + '/rest/vour/delvour',
			type : 'POST',
			dataType : 'json',
		    data: JSON.stringify({
		    	companyId:$("#companyId").val(),
		    	voucherPrefix:$("#voucherPrefix").val(),
		    	voucherNumber:$("#voucherNumber").val(),
		    	userId:$("#userId").val(),
		    	authToken:$("#authToken").val(),
				host:$("#host").val(),
		    	port:$("#port").val()
			}),
		    success: function(data) {
		    	
		    	var arrResponse = JSON.parse(data.trim());
		    	
		    	tokenValue = arrResponse.authToken;
		    	usr = $("#user").val();
		    	
		    	
		    	console.log(data);
		    	alert(data);
		    }
		});
		
	}
	
	function uploadMultipleFile() {
		$("#uploadMultipleFiles")
				.submit(
						function(e) {

							e.preventDefault(); // avoid to execute the actual submit of the form

							console.log($('#input-file').prop('files'));

							var formData = new FormData();

							var formElement = document.forms.namedItem("uploadMultipleFiles");

							var formData = new FormData(formElement);

							
							$.each(formData.getAll("file"), function(index,
									value) {
								if (value.name != "" && value.size > 0) {
									upload = true;
									fileName = value.name;
								}
							});
						
							//formData.append('docFol', 'SAL-001-001');
							//formData.append('docType', 'SAL01');
							//formData.append('upldBy', 'abizar');
							//formData.append('fldInfo', JSON.stringify(jsonArrayMain));
							//formData.append('fldHdrId', 'DF0021');
							
							console.log(formData);

							var userpassword = "abizar:password";
							
							$
									.ajax({
										headers : {
											'user-id': localStorage.getItem("docMgmtUserId"),
										    'auth-token': localStorage.getItem("docMgmtAuthToken"),
										},

										type : "POST",
										url : rootAppName + '/rest/cstmdoc/adddoc',
										data : formData, // serializes the form's elements.
										enctype : 'multipart/form-data',
										cache : false,
										processData : false,
										contentType : false,
										success : function(data) {
											//alert(data); // show response from the php script.
										}
									});

						});
	}
	
	function pdfToTxt() {
		$
				.ajax({
					headers : {
						'Accept' : 'application/json',
						'Content-Type' : 'application/json',
						'user-id': localStorage.getItem("docMgmtUserId"),
					    'auth-token': localStorage.getItem("docMgmtAuthToken"),
					},
					data : JSON.stringify({imgValue : "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMUAAAA6CAYAAAAA/J08AAAVC0lEQVR4Xu1cZ3Sc5ZV+phfNjLrc5YZ7wd0U44LBgG3cgJTdkAC7QE6W7FkSEkI7EBZCkuXAkt2FJSHhJFk4gB0bA6a4YLAN7g3jbstdtqWRZjSjmZGmaPY8d/zJM6MZWSNpkBR/7w/7WPrK+957n1ueez9rFkdProO6VAmoEmiUgGZx9GRUlYcqAVUCFyWggkK1BlUCSRJQQaGahCoBFRSqDagSaF4CaqRQLUSVgBopVBtQJaBGCtUGVAlkJAE1fcpIXOrFl4MEVFBcDlpWz5iRBFRQZCQu9eLLQQIqKC4HLatnzEgCKigyEpd68eUgARUUl4OW1TNmJAEVFBmJK3ZxQxRwevU4XmFAjqkBI/rUt+IpHXtLZzsDp1JrfDocqzQg2qDB6H510Gs7Zla1ERRfnzLhfI0+rabyrA24MsVG092n1wFj+wXgsDR0rPbb8e0NDUBZpRGnnAYcOWfEzmNmDO4ZxL/NrmrHtzR91KV0o9yhAWDQR5FjjKIkL4zuuWHodYmG1VFnSCcg7u5MlQFlFQacdBqxrcwsNvPIAicsxo6xnUZQrNxjw5bDFhw6a0QgqG08Q661AcN712FASRBzxtaK0OPXqj027DpmxlcnzagLaUQJA0uCKC0OYf4EL7rlhrNqMN/kwyMNGmw9asbuE2ZsOmxFbZ0W1wzx48Esg4IyPnzWiPJqPQ6fM0mkMhmiGNOvDjZTzHColWBYA49fC7dfh75FIQztWY8RferQq+CiDjrqDGlBEQW+PmXG1jIzNh2ywuXTyb4fXdgJQBGNAqeqDFj1lQ2f77c2AmPiwADuv6EaBEe6VVGjx3+8X4Qz1Xrw+htH12JIj2ATAH2TBpyNd4k7iAL+oBbPLisWQ/0mQCEpW4MGu0+a8PInBWL0hbYInryjAj3yYgbPvfnqtSg7Z8SXh6zYVmaBr06D64b5sWiyR6KGcl1HnSGdThqiGgH6b98rxM5jls4DCmXDnoAWr6/NF8Fyo5MHBfAvs6qbDWX0mL9+twhWUxT3zHCh+wVFZcMwO8Mzww0aPP9+IbaXWb4xUPDcNPpnlhZL6lZkj+DZ75xHgS3SRCT+ei3e2eTAJ7ttoLObPa4W3766Bqa4KN9RZ2hOfy99VIgNB6ydDxTc9NHzRry8sgAnnQbYLQ24b2Y1Jl0RgJZJa9IicDYesuLdrQ7cPd2F4b27XtHZGqC9+GEhvjxo/UZBwbT2V8uKcKDc1CwoeJ5ylx7PLi1GhUcvwHnitkr0LgglHLUjztCcrP/4aT4+3m3rnKBgKF6zx4b/W58r3ok53g9vdKFXklB5wDPVBvzx0zyMLK3HggleaDuIMWiNYbflno4wqExAEX8t67yH5zsxpm9dpwbFkk0OvL0xt3OCgpJjCP7zujx8vi8H0WgUt4xlCPYkpFEU/NsbHais0ePeG6pBhirVqq7V4XilETV+raRkZKbycyJSvNvM6WsVgvO8W4/jlQbZD/9t1EeleB9QEmrCrMS/O9IAnHPrpU7ivVwsTvsVh9AjP9Qk6oXJLJ034pzbgFDk4h5JubKAHdYrMQImgyIc0UiEPevSyxkd1ga5h/e318oEFDV+naRalB1l9siCSoxMoo7bcoZUutFpgLycCPqXBJutQZluMwV0+3SIRAGDDigtCmLzYQuWbE4PCr7T6dHjRKUB3rqYLfFsvfLDKC0OQn+RH2qTyJvtUzAK/NfHBaJsplH3znBh8mC/GBQ3tOWIRdKm7091p0ybaGgHzpiw56RZmBrew1y2yqtDoF6L8QMDmDQwgNKixLDOE/Ha/WdM+OJAjJGgkdHQmRaYDVFMHFgHoz5mcMzqRpXWo9gRKybrQxrsOmEWIycjVh/SwunV4XSVHiP6BDF9uE9YGSUd5J52kP04bBWgKT9nlAxGALupAQ/cXJ3Wy94/0yWF7foDVhwq5zu1spc5Y72YOconimuP1VJQUHbbjlrwysoC1NZrcUW3IB6a55TiPH7FgyKTMyi6Yf5P3ZCEoW5OOw0IRjRSh84aXSuOL3nRpjYdtuDwOSNySddrAF+dVmTkq9dgR5pCm7a0/7QJ+07HWM76sEZAdazCiL7FMZ1OGFDXrKNsqQ6aBQVVuWF/jkQMevnBPYL40axqSaPKXQb8aW2egCFV2sQCjzTtOxtzYbM04PbJNRhYEgLN+MAZo4Bp72kTJl8RwHeurWlkR5SN06Bf/zxPGjp3TXdL6KfgNxy04s0vckUgCl1MDzF3nBcDugUFdJsOWkAac9yAOkwfXiu89wmnAX/b7MDmI1YM7lGPe6a75fpY/m3AiysKMKxXELdNrpHnMrqsO2DF0i0O8fjJtKtiUGP712HKUB8OlZtEIQRoWYVRAJ1njUjaMvDCe1qqlHTXxYOi0B7BL++oSKC8efbagFbe//52O9jfYFS9bbIH00f4ZG+pQJHpGWjYr6zKB1nHf77eJcZI3dCpUGeBoEbkSxYyfrG+WbzRIYCYPbYWU4f5pPhnNH9vuwMbD1kkKidTsootvbfNjr7FIclaimxhaaC+u9WOtXtzUGyP4O4ZLlzZtw6aFLVvJrK/ZEebiHxzQx5WfpUDNn5uGuPD/PEerNhlF978hzemTpuqvHr87uMCoWl/MqeqSSSh0bz0UYEYNwX0ras9MBtinp+Cee3TfHy2L0c8zj0z3NBqYt42FNbgf1fnY93+HOHffzLXiSJbRKIH6xk2gF5YUYiRvetw5zR3AuNy6KwJz71bCF+9TjwLFUoPRaG+tjYfD811Ymy/i3k330WHwCiX3KBTQHFF9yCuHeLH1YP94om5y72nTPjPDwvhCehw93Q3bhnjzUQnaa+NBwXTstnjvAnePxSORcQdx8w46zYI+K8b6se0Yb6U9Hhrz7Bipx1//jwPg7sH8ejCSlgvpIiM0M8tL8LeU2ZcNSiAB2c7ob2Q0lCnrBl478KJHiyalFh/0jH95r1ClFcbmoCC6TeZKQLogZurEhrCFR4dnl1aIhkEswXqyWFpGqEyUcAlQcGHnXXrhY1iKkQBXD/Ch32nTfjulJomxZvy8pW7beI1hvSsxyPznZLPxy96NRboq/fYJDX6qQAnZpDnavT45ZJiyR8JupkjfQn3EhD/s7JAxgDic2V6FOak72234+F5Tozsk1hYeut0eHpJkdQ3RY4wnry9UiIUIwgLPEab+RM8Cfkwz8n3cR+pvOzo0jr8bJ5TQKmseOqUgCcr1x4rHhSU5/j+AViMsffyT4K42qeTnJsGOmWoXxp8I3rXpczxFVBkegZ67De/yMOYvgE8OLcapgtpLPehPJOy/0Wc3g+Wm/D8B4XQaYHHFlaiT2FiysxexX9/ko/1+3OagILy//2afNw1zYUbRiXaQjy1bDFEBaRDk+q/TGXfIlBQ4Kwf/rCmQNIodrXnjvXiW9d4Us6nMLr8ZnmR1BIzRvrwoySDUja55agFL64oBIvURZM8+O61NfIrhlcWiUxhHripGtOGJwqCUeZXy4olt/zpXKd4JS7m8s+9S6M3SDc915roMehJV+y0SQEeX3yu22/Fq6sL5GeMTEwJmfKQBGCU2HPKjKsH+VOCIlXzLt5427O5l5A+2SJ45tsVAm4FFKzTzrj02FFmwdYyi4CDqeBNV3oxa7SviTyaY9CaO0PZeQPW7rWBUZLNQY0m2ji3tHhTrjQ141MgOqs3NuRh+TY7RpXGwJKqznpjQ66k1fH3Mjt5ZXUBNuy3StqUigFlqnz0nFHkkMqJZgUUfCgN/bfLi6RO4EzK44sqpcZItWh0Ty4uAcPe3PFe/GCqO+V1p6sMeGpJiQBt/IAAHrq1SkDGnz/9t2Ip4phW3XFVDCzKOlBulI4yjfzn8yoxrn8sIjAVe+KdEvl7woBAk+iUvIlZo1mHhCTleHNDrhTLBBa92FWD/MJUsZ5IxZC11qAyVVD89S0utFlXHbHgL+vyhNRgFPuHa92YdWWteGpltfUMMYbPgKMVBnj8OpEjG5rUf7xhK86KUZdp5oNzqprUN9xTKkqWEe/Xy2O9GUY9Kc6bWZyooC21ZbUoUiQLkSkUQTGoe2pQkK3696XFwiqw2LpvZur0gaB57K1uIsxRferw8IIqCcVUPusNCnjcAOam1Y31BvfCgux3HxWCxeYvFjgbm1LK88hUPM0RiPyWz11xGHLlVzZJC+lhGZaL7WGJdKkYsrYaVGuU1lJQ8Nnc/+trWQva5FVkaDhPVBDHCLXlDJQ1C3kSIr6gFn0KQmK0jBRkl+JBoTgrgoWgYI2ZaqUChXJmkgepaOXWyPFS92QFFOyE/3JJCTgyQgboZ3OdKamyeFBcN8yHB25yNRbU9Np/ZZFbr8Vd09wyU2UyNEid8daXDmw9asG88V4smORtTOH4vMff7iaRhwUX70m3yFzotNEE2pCGREWTzmQKcMLJkBzF1YMDuHOqu90M6lJKSff7TEDBZ2w+YpGCn+lpjrkBTyyqTGDCWgsKxYFwSpiTDnR8Ct2rPDMeFIpeKj06ITJYgxmSpnfTRQqe+bllRdhfbhI7mDMuPWlBe+P1bR1CzQooagJaPL24BCerDOhdEMbjt1U04cgphCoa8Vvd5G8a3a1xB+YA3Ee7bFi61YFe+SFJhxihyKqQr2ZaM3+iJ4GJYEOHBfqJSqMU5z+Y5k45s8Ww/8luOwb3rBcO/3S1QehTJU1iwbr7pBmf7c0RJoc11f03uISxaq/UozXAyBQU+6T2KpI+Dc/GUQ82TNtyBsrm7U0OfLDdLvUEGbz4mapUoIjXCxlDFtpKTyleDqkiRSiiwQsfFEpqO7q0Hj++pUp0lWqRrmeqSFtpy8oKKOhxSdlxIE2h0diPSF7kp59aXCIem/Pz7IQqiwzO+v1WnKo2iOFzJJpjzwQGi2BGgeRije997dM8GVFhgck8+pohgYTUiwbOKLBqjx3fm+KS66hgplrJuWi5W4+XVhQK73/LWK9w720xqLYoivdmCgqmTmT4yOywQfrogkpJOdtyhkqPHk8tKZYexX03uHDjqMReRCpQUC+/X50v1Dd19k8zXNI3SZ6lSzfmsWyLHW99mSf3LpzkkXeymRy/yJAu3ezA/IneJjNemcq9xaAgC/D8iiJsPWIRw2yupuAm+F0G836GWubkFGAyG7RmTw7+9Fm+0Gzfm+Ju5NLpyUnDUUicuRpZWpeyMEt1WHayOV7NIp1hlGxSv5IQiuxhMQ72TTjAOKRnEDeP8cpz+R4ClP2Q5D2+ujofa762SfORRT9XvCxYkDNHjm8YZYt9Ihv3zNIi+aaiuSlZ7pFG8uqqAmmQMlVZMNGL26/yNKanrT0DG3dPLikWtol6u/M6t9gDUzSObvxlfZ44nUHd6/HYIqewd+ypkM5/eVUBvAGt1KJMhQZ2r28s/Hm21z/Lk94UAcxULzcnIvphOk6WkhGdYLjpylp5RkluWM5DCp/1JylZNoLb+sVei0ChzLmwKca2OinZe6a7MG2YP+03E0x/SH+ys0ojWTDRI9QqPRWNnr2CN9bnQaeL4u5p7gSqjd8rMI+kh6ZRk4ZTjE75uoxehixYn8Kw0JJKE5Ms2bItDny8yybDjIxCpYVh9C0JInJBcf2Kg7jnelfjrBZBsXybA7eO90p3mr0LjRbiDUnVVtdqpZOvsG3Mjfn9CGXBPsxDc6tk5kdZ7NxylJ5AI6DZg2luxqslnoyjFcfOG/HCh4WyL45QMA1hOqJ8XcdrSG4cdxrwxYEcfLbPKkZHdu4fp7gTRvpbewbKlHQ7aXFG2TnjPCh2RETH51w6YSeVPtBCGRAFxvYPCDDe/jIXa77OEYaPnedrBvsljSKg2ITbcCBHnku9srnHfgPBpYEGq7/OkX4S6xPaAr8jYcZA+yEdy2zi3pmph1ZbIt/4ay4JimMVBpl9YtHJHJu9AS6OSEwd6hdjSEd/8tqPd9mx/ZhZDjNxQAC9C2PDeEQ9vQiLNPLd8Z15Cpheg990kJJL2LAGkg5R2fTqo/rU49qhfmE7lMWC64MddumT0DD5DL6Te+XME2uX/iUXm0cUNkeW+SYaEPNurS6Kc9UGnKwiYxIQB8CAveu4WRgqfozF89HYCdxBPYJS5/D3BAtTR/6eno0dbf4ueSCvpcpi8X/WZQBrBDJvTCOZO88Y4RNDJwERi2Cx5h076hyk7JYXFtaJPQqlWcZUpi1n4PfotAP2HLgneuqS3Ih4fdoDGaa/rmfKBgzrGZTCmAwiZUsbeH+HHbuPm+U6gpljGxxtp5Fz9okDqPxqkPtlv+iqwQHx/HR2lCl7MLRJOk4+k/Jl5/7mMbUYzRGPlgq1mesuCQqGJTI96RaNgnleumlQKoHhlJ9wcnKTjRyGWxoeZ27S3cfrl2x2iNfmJCU/ZldWbFBPg3AkxpOzWP7X2VWwxn3TqzBJe06YGyMG6wZlHCP+PBxCO1Wll2Yhn03PRenmWiJSZzBU0zuxyIw1/wxNxNE9L4Rpw/1YscMuYE9eA7sHm+TfLdVffHPqkvcwghoaxAHw60d60/hPiNvjDHzGxsMWsEvNlLTEEcZUZgG2CFhzfLjTBtLinIlitzw+teS4B+2JNsGIQQdHh8IakU7sYLlRqF3KPPnTZ9oOG7u0yZqATgBA2pwDiD2ZTVxSOC274JKgaNlj2vcqgoezVmQZZozwI9QQ84LKctXqEAhpEAxpJFx/cdAqHHb8t8jtuyP1aZeTBDodKOhJPtxpl6/afj6/UgrK5hbToz+sycePb66S3FZdqgTaKoFOBwqOeLAbTvrt+9e55f9UUqYw4w9L9oQMy9ajVvkxx0nayjq0VZjq/X8fEuh0oCD9xg+bWNiz2CJDwUIsvifBIk7YDrdeCuh5ExKbeH8fqlFP0VES6HSgYO3AIo6jFuyIkzpkYSej2ZrYJ6V2cwRFjoiwVjeOrJXRc3WpEmgvCXQ6UCgHY5OH/0kW6Td+B0HmgaDIMTZI95nz+qQc24txaC+Bqs/p+hLotKDo+qJVT9BVJaCCoqtqTt131iSggiJrolUf3FUloIKiq2pO3XfWJKCCImuiVR/cVSWggqKrak7dd9YkoIIia6JVH9xVJaCCoqtqTt131iSggiJrolUf3FUloIKiq2pO3XfWJKCCImuiVR/cVSWggqKrak7dd9YkoIIia6JVH9xVJaCCoqtqTt131iSggiJrolUf3FUlQFCs66qbV/etSiAbEvh/q5no8yRoeywAAAAASUVORK5CYII="}),
					url : rootAppName + '/rest/cstmdoc/con',
					type : 'POST',
					dataType : 'json',
					success : function(data) {

					},
					error : function(data) {
						console.log(data);
						$("#mainLoader").hide();
					}
				});
	}
	
	
	function getCstmDocument() {
		$
				.ajax({
					headers : {
						'Accept' : 'application/json',
						'Content-Type' : 'application/json',
						'user-id': localStorage.getItem("docMgmtUserId"),
					    'auth-token': localStorage.getItem("docMgmtAuthToken"),
					},
					data : JSON.stringify({
						trnSts : "R"  //R for reject
					}),
					url : rootAppName + '/rest/cstmdoc/readdoc',
					type : 'POST',
					dataType : 'json',
					success : function(response) {
					
						var loopSize = response.output.fldTblDoc.length;
																		
						var ctlNo = "", getCtlNo = "", flPdf = "", flPdfName = "", flTxt = "", crtDtts = "", prsDtts = "", tbody="";
						var vchrCtlNo = "", vchrInvNo = "", vchrVenId = "", vchrVenNm = "", vchrBrh = "", vchrAmt = "";
						var vchrPayTerm="", vchrInvDt = "", vchrDueDt="", vchrExtRef="";
						var vchrDueDtStr="", vchrInvDtStr ="", crtDttsStr="", usrNm ="";
						
						for( var i=0; i<loopSize; i++ ){
							
							ctlNo = response.output.fldTblDoc[i].ctlNo;
							getCtlNo = response.output.fldTblDoc[i].getCtlNo;
							flPdf = response.output.fldTblDoc[i].flPdf;
							flPdfName = response.output.fldTblDoc[i].flPdfName;
							flTxt = response.output.fldTblDoc[i].flTxt;
							crtDtts = response.output.fldTblDoc[i].crtDtts;
							crtDttsStr = response.output.fldTblDoc[i].crtDttsStr;
							
							vchrCtlNo = response.output.fldTblDoc[i].vchrCtlNo;
							vchrInvNo = response.output.fldTblDoc[i].vchrInvNo;
							vchrVenId = response.output.fldTblDoc[i].vchrVenId;
							vchrAmt = response.output.fldTblDoc[i].vchrAmt;
							vchrVenNm = response.output.fldTblDoc[i].vchrVenNm;
							vchrBrh = response.output.fldTblDoc[i].vchrBrh;
							vchrPayTerm = response.output.fldTblDoc[i].vchrPayTerm;
							vchrInvDt = response.output.fldTblDoc[i].vchrInvDt;
							vchrDueDt = response.output.fldTblDoc[i].vchrDueDt;
							vchrExtRef = response.output.fldTblDoc[i].vchrExtRef;
							vchrInvDtStr = response.output.fldTblDoc[i].vchrInvDtStr;
							vchrDueDtStr = response.output.fldTblDoc[i].vchrDueDtStr;
							usrNm = response.output.fldTblDoc[i].usrNm;
							
							tbody += '<tr>'+
							         '<td>' + ctlNo + '</td>' +
							         '<td>' + getCtlNo + '</td>' +
							         '<td>' + flPdf + '</td>' +
							         '<td>' + flPdfName + '</td>' +
							         '<td>' + flTxt + '</td>' +
							         '<td>' + crtDtts + '</td>' +
							         '<td>' + crtDttsStr + '</td>' +
							         '<td>' + vchrCtlNo + '</td>' +
							         '<td>' + vchrInvNo + '</td>' +
							         '<td>' + vchrAmt + '</td>' +
							         '<td>' + vchrPayTerm + '</td>' +
							         '<td>' + vchrInvDt + '</td>' +
							         '<td>' + vchrDueDt + '</td>' +
							         '<td>' + vchrVenId + '</td>' +
							         '<td>' + vchrVenNm + '</td>' +
							         '<td>' + vchrBrh + '</td>' +
							         '<td>' + vchrInvDtStr + '</td>' +
							         '<td>' + vchrDueDtStr + '</td>' +
							         '<td>' + usrNm + '</td>' +
								 '</tr>';
						}
						$("#tableDocs tbody").html( tbody );
						
					},
					error : function(data) {
						console.log(data);
						$("#mainLoader").hide();
					}
				});
	}
	
	
	function readCstmVoucherInfo() {
		$
				.ajax({
					headers : {
						'Accept' : 'application/json',
						'Content-Type' : 'application/json',
						'user-id': localStorage.getItem("docMgmtUserId"),
					    'auth-token': localStorage.getItem("docMgmtAuthToken"),
					},
					data : JSON.stringify({
						typId : "SAL02",
						
						fltrVal : ""
					}),
					url : rootAppName + '/rest/vchr/readvchr',
					type : 'POST',
					dataType : 'json',
					success : function(response) {
										
						var loopSize = response.output.fldTblDoc.length;
						
						var vchrCtlNo = "", vchrInvNo = "", vchrVenId = "", vchrVenNm = "", vchrBrh = "", vchrAmt = "";
						var vchrPayTerm="", vchrInvDt = "", vchrDueDt="", vchrExtRef="", tbody="";
						
					for( var i=0; i<loopSize; i++ ){
							
							vchrCtlNo = response.output.fldTblDoc[i].vchrCtlNo;
							vchrInvNo = response.output.fldTblDoc[i].vchrInvNo;
							vchrVenId = response.output.fldTblDoc[i].vchrVenId;
							vchrAmt = response.output.fldTblDoc[i].vchrAmt;
							vchrVenNm = response.output.fldTblDoc[i].vchrVenNm;
							vchrBrh = response.output.fldTblDoc[i].vchrBrh;
							vchrPayTerm = response.output.fldTblDoc[i].vchrPayTerm;
							vchrInvDt = response.output.fldTblDoc[i].vchrInvDt;
							vchrDueDt = response.output.fldTblDoc[i].vchrDueDt;
							vchrExtRef = response.output.fldTblDoc[i].vchrExtRef;
							
							tbody += '<tr>'+
							         '<td>' + vchrCtlNo + '</td>' +
							         '<td>' + vchrInvNo + '</td>' +
							         '<td>' + vchrAmt + '</td>' +
							         '<td>' + vchrPayTerm + '</td>' +
							         '<td>' + vchrInvDt + '</td>' +
							         '<td>' + vchrDueDt + '</td>' +
							         '<td>' + vchrVenId + '</td>' +
							         '<td>' + vchrVenNm + '</td>' +
							         '<td>' + vchrBrh + '</td>' +
								 '</tr>';
						}
						$("#tableDocs tbody").html(tbody);
						 //$("p").html("Testing");
					},
					error : function(data) {
						console.log(data);
						$("#mainLoader").hide();
					}
				});
	}
	
	
	function addCstmVoucherInfo() {
		$
				.ajax({
					headers : {
						'Accept' : 'application/json',
						'Content-Type' : 'application/json',
						'user-id': localStorage.getItem("docMgmtUserId"),
					    'auth-token': localStorage.getItem("docMgmtAuthToken"),
					},
					data : JSON.stringify({
						typId : "SAL02",
						
						fltrVal : ""
					}),
					url : rootAppName + '/rest/vchr/addvchr',
					type : 'POST',
					dataType : 'json',
					success : function(response) {
										
						
					},
					error : function(data) {
						console.log(data);
						$("#mainLoader").hide();
					}
				});
	}
	
	function getPdfFile()
	{	
				
		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json'
			},
			url : rootAppName + '/rest/cstmdoc/pdf',
			type : 'POST',
			dataType : 'json',
		    data: JSON.stringify({
		    	ctlNo:$("#ctlNo").val()
			}),
		    success: function(data) {
		    	console.log(data);
		    	var pdf = data;
		    	
		    	var arrResponse = JSON.parse(data.trim());
		    	
		    	var tbody="";
		    	
		    	tbody += '<tr>'+
		         '<td>' + pdf + '</td>' +
		       '</tr>';
	
	$("#tableDocs tbody").html(tbody);
		    	
		    	//tokenValue = arrResponse.authToken;
		    	//usr = $("#user").val();
		    	
		    	
		    	//console.log(data);
		    	//alert(data);
		    }
		});
		
	}
	
	function getPdfSrcget() {
		
	/* 	$.cookie('auth-token', localStorage.getItem("docMgmtAuthToken"));
		$.cookie('user-id', localStorage.getItem("docMgmtUserId")); */
		
		$.ajax({
			headers : {
				'Accept' : 'application/octet-stream',
				'Content-Type' : 'application/octet-stream',
				'auth-token' : localStorage.getItem("docMgmtAuthToken"),
				'user-id' : localStorage.getItem("docMgmtUserId")
			},
			data : "",
			url : rootAppName + '/rest/cstmdoc/downloadget?ctlNo=9',
			type : 'GET',
			//dataType: 'binary',
			success : function(response, status, xhr) {
				alert("SUCCESS");
				console.log(response);

				$("#docPreview").attr("src", encodeURIComponent(response));

				/* $.cookie("auth-token", null);
				$.cookie("user-id", null); */
			},
			error : function(data) {
				alert("ERROR");

				
			}
		});
	}
	
function getPdfSrc() {
	/* 
	$.cookie('auth-token', localStorage.getItem("docMgmtAuthToken"));
	$.cookie('user-id', localStorage.getItem("docMgmtUserId")); */
	
		$.ajax({
			headers : {
				'Accept' : 'application/octet-stream',
				'Content-Type' : 'application/octet-stream',
				'auth-token' : localStorage.getItem("docMgmtAuthToken"),
				'user-id' : localStorage.getItem("docMgmtUserId")
			},
			data : "",
			url : rootAppName + '/rest/cstmdoc/download?ctlNo=9',
			type : 'GET',
			//dataType: 'binary',
			success : function(response, status, xhr) {
				alert("SUCCESS");
				console.log(response);

				$("#docPreview").attr("src", encodeURIComponent(response));

				/* $.cookie("auth-token", null);
				$.cookie("user-id", null); */
			},
			error : function(data) {
				alert("ERROR");

			}
		});
	}
	
function updateVchrInfo() {
	
	$.ajax({
		headers : {
			'Accept' : 'application/json',
			'Content-Type' : 'application/json',
			'user-id': localStorage.getItem("docMgmtUserId"),
		    'auth-token': localStorage.getItem("docMgmtAuthToken"),
		},
		url : rootAppName + '/rest/cstmdoc/upddoc',
		type : 'POST',
		dataType : 'json',
		data : JSON.stringify({
			vchrCtlNo  : "16",
			vchrInvNo  : "6877",
			vchrVenId : "1800",
			vchrVenNm : "Star Test",
			vchrBrh : "CHI",
			vchrAmt : "80",
			vchrExtRef : "test",
			vchrInvDt : "07/16/2019",
			vchrInvDtStr : "",
			vchrDueDt : "17/08/2020",
			vchrDueDtStr : "",
			vchrPayTerm  : "45",
			vchrCry : "USD"
		}),
		success : function(data) {
			alert("Suc");
			//$('#overlay').hide();

			var data1 = JSON.stringify(data, null, 4);// Indented 4 spaces

			$("#output").html(data1);

		},
		error : function(data) {
			console.log(data);
			alert("ERROR");
		}

	});

}

function viewRecord() {
	$
			.ajax({
				headers : {
					'Accept' : 'application/json',
					'Content-Type' : 'application/json',
					'user-id': localStorage.getItem("docMgmtUserId"),
				    'auth-token': localStorage.getItem("docMgmtAuthToken"),
				},
				data : JSON.stringify({
					ctlNo : "17"
				}),
				url : rootAppName + '/rest/cstmdoc/viewdoc',
				type : 'POST',
				dataType : 'json',
				success : function(response) {
				
					var loopSize = response.output.fldTblDoc.length;
																	
					var ctlNo = "", getCtlNo = "", flPdf = "", flPdfName = "", flTxt = "", crtDtts = "", prsDtts = "", tbody="";
					var vchrCtlNo = "", vchrInvNo = "", vchrVenId = "", vchrVenNm = "", vchrBrh = "", vchrAmt = "";
					var vchrPayTerm="", vchrInvDt = "", vchrDueDt="", vchrExtRef="", usrNm="";
					
					for( var i=0; i<loopSize; i++ ){
						
						ctlNo = response.output.fldTblDoc[i].ctlNo;
						getCtlNo = response.output.fldTblDoc[i].getCtlNo;
						flPdf = response.output.fldTblDoc[i].flPdf;
						flPdfName = response.output.fldTblDoc[i].flPdfName;
						flTxt = response.output.fldTblDoc[i].flTxt;
						crtDtts = response.output.fldTblDoc[i].crtDtts;
						
						vchrCtlNo = response.output.fldTblDoc[i].vchrCtlNo;
						vchrInvNo = response.output.fldTblDoc[i].vchrInvNo;
						vchrVenId = response.output.fldTblDoc[i].vchrVenId;
						vchrAmt = response.output.fldTblDoc[i].vchrAmt;
						vchrVenNm = response.output.fldTblDoc[i].vchrVenNm;
						vchrBrh = response.output.fldTblDoc[i].vchrBrh;
						vchrPayTerm = response.output.fldTblDoc[i].vchrPayTerm;
						vchrInvDt = response.output.fldTblDoc[i].vchrInvDt;
						vchrDueDt = response.output.fldTblDoc[i].vchrDueDt;
						vchrExtRef = response.output.fldTblDoc[i].vchrExtRef;
						usrNm = response.output.fldTblDoc[i].usrNm;
						
						tbody += '<tr>'+
						         '<td>' + ctlNo + '</td>' +
						         '<td>' + getCtlNo + '</td>' +
						         '<td>' + flPdf + '</td>' +
						         '<td>' + flPdfName + '</td>' +
						         '<td>' + flTxt + '</td>' +
						         '<td>' + crtDtts + '</td>' +
						         '<td>' + vchrCtlNo + '</td>' +
						         '<td>' + vchrInvNo + '</td>' +
						         '<td>' + vchrAmt + '</td>' +
						         '<td>' + vchrPayTerm + '</td>' +
						         '<td>' + vchrInvDt + '</td>' +
						         '<td>' + vchrDueDt + '</td>' +
						         '<td>' + vchrVenId + '</td>' +
						         '<td>' + vchrVenNm + '</td>' +
						         '<td>' + vchrBrh + '</td>' +
						         '<td>' + usrNm + '</td>' +
							 '</tr>';
					}
					$("#tableViewRecord tbody").html( tbody );
					
				},
				error : function(data) {
					console.log(data);
					$("#mainLoader").hide();
				}
			});
}

function isPrsPaid() {
	$
			.ajax({
				headers : {
					'Accept' : 'application/json',
					'Content-Type' : 'application/json',
					'user-id': localStorage.getItem("docMgmtUserId"),
				    'auth-token': localStorage.getItem("docMgmtAuthToken"),
				},
				data : JSON.stringify({
					isPrs : "false"
				}),
				url : rootAppName + '/rest/cstmdoc/prsdoc',
				type : 'POST',
				dataType : 'json',
				success : function(response) {
				
					var loopSize = response.output.fldTblDoc.length;
																	
					var  tbody="";
					var  vchrVenNm = "",  vchrAmt = "", vchrVenId="";
					var  vchrInvDt = "",IsPrs ="", vchrInvDtStr="";
					
					for( var i=0; i<loopSize; i++ ){
						vchrAmt = response.output.fldTblDoc[i].vchrAmt;
						vchrVenNm = response.output.fldTblDoc[i].vchrVenNm;
						vchrInvDt = response.output.fldTblDoc[i].vchrInvDt;
						vchrInvDtStr = response.output.fldTblDoc[i].vchrInvDtStr;
						IsPrs = response.output.fldTblDoc[i].isPrs;
						vchrVenId =response.output.fldTblDoc[i].vchrVenId;
						
						tbody += '<tr>'+
						        '<td>' + vchrAmt + '</td>' +
						         '<td>' + vchrInvDt + '</td>' +
						         '<td>' + vchrInvDtStr + '</td>' +
						         '<td>' + vchrVenNm + '</td>' +
						         '<td>' + IsPrs + '</td>' +
						         '<td>' + vchrVenId + '</td>' +
						        '</tr>';
					}
					$("#tableIsPrsPaid tbody").html( tbody );
					
				},
				error : function(data) {
					console.log(data);
					$("#mainLoader").hide();
				}
			});
}

function delDoc() {
	$
			.ajax({
				headers : {
					'Accept' : 'application/json',
					'Content-Type' : 'application/json',
					'user-id': localStorage.getItem("docMgmtUserId"),
				    'auth-token': localStorage.getItem("docMgmtAuthToken"),
				},
				data : JSON.stringify({
					ctlNo : "16"
				}),
				url : rootAppName + '/rest/cstmdoc/deldoc',
				type : 'POST',
				dataType : 'json',
				success : function(response) {
				
					
				},
				error : function(data) {
					console.log(data);
					$("#mainLoader").hide();
				}
			});
}

function rejDoc() {
	$
			.ajax({
				headers : {
					'Accept' : 'application/json',
					'Content-Type' : 'application/json',
					'user-id': localStorage.getItem("docMgmtUserId"),
				    'auth-token': localStorage.getItem("docMgmtAuthToken"),
				},
				data : JSON.stringify({
					ctlNo : "17",
					trnRmk : "Star rejected this doc."
				}),
				url : rootAppName + '/rest/cstmdoc/rejdoc',
				type : 'POST',
				dataType : 'json',
				success : function(response) {
				
					
				},
				error : function(data) {
					console.log(data);
					$("#mainLoader").hide();
				}
			});
}


	
	</script>
</head>
<body>
	<input type='text' class='form-control' id='usrName'/>
	<br>
	<button class="btn btn-dark" onclick='getData()'>Submit</button>
	<br>
	<span id='output'></span>
	<br>
	<button class="btn btn-dark" onclick='callAuth()'>Auth</button>
	<br><br>
<br><br>userId<input type='text' name='userId' id='userId' value='mayankc'/>
<br>authToken<input type='text' name='authToken' id='authToken' value='VR'/>
<br>host<input type='text' name='host' id='host' value='172.20.100.70'/>
<br>port<input type='text' name='port' id='port' value='60967'/>
	
<br><br>companyId<input type='text' name='companyId' id='companyId' value='SQ1'/>
<br>voucherPrefix<input type='text' name='voucherPrefix' id='voucherPrefix' value='VR'/>
<br>vendorId<input type='text' name='vendorId' id='vendorId' value='1010'/>
<br>voucherAmount<input type='text' name='voucherAmount' id='voucherAmount' value='10'/>

<br>vendorInvoiceNumber<input type='text' name='vendorInvoiceNumber' id='vendorInvoiceNumber' value='RJT-T07'/>
<br>vendorInvoiceDate<input type='text' name='vendorInvoiceDate' id='vendorInvoiceDate' value='2020-01-02'/>
<br>entryDate<input type='text' name='entryDate' id='entryDate' value='2020-01-02'/>
<br>paymentStatusAction<input type='text' name='paymentStatusAction' id='paymentStatusAction' value='A'/>
<br>voucherCategory<input type='text' name='voucherCategory' id='voucherCategory' value='MAT'/>
<br>paymentStatus<input type='text' name='paymentStatus' id='paymentStatus' value='OK'/> 

<input type="button" class="btn btn-primary btn-sm" id="btnSubmit" onclick="callCreateVoucher()" value="Voucher"/>

<br><br>
<br><br>companyId<input type='text' name='companyId' id='companyId' value='SQ1'/>
<br>voucherPrefix<input type='text' name='voucherPrefix' id='voucherPrefix' value='VR'/>
<br>voucherNumber<input type='text' name='voucherNumber' id='voucherNumber' value='8726'/>

<input type="button" class="btn btn-primary btn-sm" id="btnSubmit" onclick="callDeleteVoucher()" value="Voucher"/>

&nbsp;&nbsp;
<!-- action="rest/docupload/pdf" -->
	<form id='uploadMultipleFiles' name='uploadMultipleFiles' enctype="multipart/form-data">

		<p>
			Select a file : <input type="file" name="file" id='input-file' size="45" accept=".pdf" multiple />
		</p>
		<!-- <input type='submit'></input> -->
		<button value="Upload PDF" onclick='uploadMultipleFile()'>upload multiple files</button>

	</form>
	
	&nbsp;&nbsp;
	<button class="btn btn-dark" onclick='getCstmDocument()'>Get
		Document</button>
	&nbsp;&nbsp;
		
	<!-- <table class="table" id="tableDocs">
											<thead class="thead-clr"></thead>
											<tbody></tbody>
										</table>  -->
	
	&nbsp;&nbsp;
	&nbsp;&nbsp;
<button class="btn btn-dark" onclick='pdfToTxt()'>pdfToTxt</button>
	&nbsp;&nbsp;



<!-- <input type='submit'></input> -->
	
	&nbsp;&nbsp;
	<button class="btn btn-dark" onclick='addCstmVoucherInfo()'>Save Voucher Info</button>
	&nbsp;&nbsp;
	
	&nbsp;&nbsp;
	<button class="btn btn-dark" onclick='readCstmVoucherInfo()'>get Voucher Info</button>
	&nbsp;&nbsp;
		
		
	
		
	<table class="table" id="tableDocs">
											<thead class="thead-clr"></thead>
											<tbody></tbody>
										</table>
	
	&nbsp;&nbsp;
	&nbsp;&nbsp;
<button class="btn btn-dark" onclick='pdfToTxt()'>pdfToTxt</button>
	&nbsp;&nbsp;
&nbsp;&nbsp;
	&nbsp;&nbsp;
	
<br><br>ControlID<input type='text' name='ctlNo' id='ctlNo' value='8'/>&nbsp;&nbsp;
	&nbsp;&nbsp;
	<button class="btn btn-dark" onclick='getPdfSrc()'>Get PDF</button>
	
	<!-- <iframe id="docPreview" src='' width="500" height="500"></iframe> -->
	
	<br><br>
	
	<button class="btn btn-dark" onclick='updateVchrInfo()'>UpdateVoucher</button>
	
	<!-- <div id='output' style="height: 700px; width: 100%; font-size: 13px;font-weight:600;clear: both; border: 1px solid lightgrey; word-wrap: break-word; white-space: pre;overflow-y: auto;"></div>
			 -->		
	
	<br><br>
		&nbsp;&nbsp;
	<button class="btn btn-dark" onclick='viewRecord()'>View record</button>
	&nbsp;&nbsp;
	<br><br>
	<table class="table" id="tableViewRecord">
											<thead class="thead-clr"></thead>
											<tbody></tbody>
										</table>
	
	<br><br>
		&nbsp;&nbsp;
	<button class="btn btn-dark" onclick='isPrsPaid()'>In Process (Or Paid)</button>
	&nbsp;&nbsp;
	<br><br>
	<table class="table" id="tableIsPrsPaid">
											<thead class="thead-clr"></thead>
											<tbody></tbody>
										</table>
	
	<br><br>
	
		&nbsp;&nbsp;
	<button class="btn btn-dark" onclick='isPrsPaid()'>In Process (Or Paid)</button>
	&nbsp;&nbsp;
	
	<br><br>
		&nbsp;&nbsp;
	<button class="btn btn-dark" onclick='delDoc()'>Delete Doc</button>
	&nbsp;&nbsp;
	
	<br><br>
		&nbsp;&nbsp;
	<button class="btn btn-dark" onclick='rejDoc()'>Reject Doc</button>
	&nbsp;&nbsp;
	
	</body>
</html>
