var initialCall = true;
var globalCountryList = "";
var globalGlSubAcc = "";
//var execPath = "/u/starsoft/bin/cstm_get_ocr_data";
var execPath = "/u/starsoft/bin/cstm_get_ocr_data_barrett";
var execPathCmnFld = "/u/starsoft/bin/cstm_cmmn_flds";
var execPathWkf = "/u/starsoft/bin/cstm_get_work_flow_data";

$(document).ready(function() {
	
	$("#appPath").val( window.location.pathname.split("/")[2] + "/" + window.location.search );
	
	var currentPathName = window.location.pathname;
	var currentPathArr = window.location.pathname.split("/");
	var currentPathNameLength = currentPathArr.length;
	if( currentPathNameLength >= 2 ){
		$("#appName").val( currentPathArr[ currentPathNameLength - 2 ] );
	}
	
	$('.date').daterangepicker({
		singleDatePicker: true,
		showDropdowns: true,
		opens: 'left',
		locale: {
			format: 'MM/DD/YYYY'
		}
	});
	
	//getCountryList();
	
	$('input[name="date-range"]').daterangepicker({
		autoUpdateInput: false,
		opens: 'left',
		locale: {
			format: 'MM/DD/YYYY'
		}
	}, function(start, end, label) {
		console.log("A new date selection was made: " + start.format('MM/DD/YYYY') + ' to ' + end.format('MM/DD/YYYY'));
	});

	$('input[name="date-range"]').on('apply.daterangepicker', function(ev, picker) {
		$(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
		$(this).trigger("change");
	});

	$('input[name="date-range"]').on('cancel.daterangepicker', function(ev, picker) {
		  $(this).val('');
		  $(this).trigger("change");
	});
	
	$(".section, #wfAddWrapper").hide();
	
	//pendInvcNo();
	
	var wfBlockCnt = 1;
	$(".select2").select2({
		maximumSelectionLength: 6
	});
		
	$('#vendors').val(['Vendor 1','Vendor 2','Vendor 3','Vendor 4','Vendor 5','Vendor 6']).trigger('change');

	$(document).on("keypress", ".number-only", function(e){
		//if the letter is not digit then display error and don't type anything
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			//display error message
			//$("#errmsg").html("Digits Only").show().fadeOut("slow");
			return false;
		}
	});
	
	$("input.number").keydown(function (event) {
		if (event.shiftKey == true) {
			event.preventDefault();
		}

		if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105) || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 37 || event.keyCode == 39 || event.keyCode == 46 || event.keyCode == 190) {

		} else {
			event.preventDefault();
		}
		
		if($(this).val().indexOf('.') !== -1 && event.keyCode == 190)
			event.preventDefault();
			
	});
	
	$(document).mouseup(function(e){
		var container = $("#adminListWrapper");
		
		if( !$("body").hasClass("swal2-shown") && e.target.attributes.length && e.target.attributes[0].nodeValue.indexOf("select2-results__option") < 0 && e.target.id != "adminListBtn" ){
			// if the target of the click isn't the container nor a descendant of the container
			if (!container.is(e.target) && container.has(e.target).length === 0) {
				container.hide();
			}
		}
	});
	
	$(document).on('keypress', "#voucherNo, #vendorId", function(e) {
		if(e.which == 13) {
			$("#search").trigger("click");
		}
	});
   
	$(document).on("change", "input[type='radio'][name='blockCndt']", function(){
		$(".condition-row").css({"transform": "scale(0)","height": "0"});
		var rowId = $("input[type='radio'][name='blockCndt']:checked").val();
		$("#"+rowId).css({"height": "initial","transform": "scale(1)"});
	});
   
	$(document).on("change", "input[type='checkbox'].blockCndt", function(){
		var rowId = $(this).attr("name");
		
		if( $(this).prop("checked") ){
			$("#"+rowId).css({"transform": "scale(1)","height": "initial"});
			$("#"+rowId).addClass("mb-2");
		}else{
			$("#"+rowId).css({"transform": "scale(0)","height": "0"});
			$("#"+rowId).removeClass("mb-2");
		}
	});
   
	$(document).on("change", "#amountType", function(){
		if( $("#amountType").val() == "range" ){
			$("#amountTo").removeAttr("readonly");
			$("#amount").attr("placeholder", "From");
			$("#amountTo").attr("placeholder", "To");
		}else{
			$("#amountTo").attr("readonly", true);
			$("#amount").attr("placeholder", "");
			$("#amountTo").attr("placeholder", "");
			$("#amountTo").val("");
			$("#amountTo").removeClass("border-danger");
		}
	});
	
	$(document).on("click", "#costReconTable tbody tr", function(){
		$("#costReconTable tbody tr").removeClass("active-tr");
		if( !$("#setPoNo").hasClass("hide-imp") ){
			$(this).addClass("active-tr");
		}
	});
	
	$(document).on("click", "#setPoNo", function(){
		var poNo = $("#costReconTable tbody tr.active-tr td:first-child").text();
		
		if( $("#editDataModal").hasClass("show") ){
			$("#poNo").val( poNo );
		}else if( $("#editTransModal").hasClass("show") ){
			$("#transPoNo").val( poNo );
		}
		$("#costReconModal").modal("hide");
	});
	
	$(document).on("click", "#adminListBtn", function(){
		$(".usr-error").html("");
		if( $("#adminListWrapper").css("display") == "block" ){
			$("#adminListWrapper").hide();
		}else{
			getAdminList();
		}
	});
	
	$(document).on("click", "#addAdm", function(){
		$(".usr-error").html("");
		
		var lgnId = $("#admUsr").val();
		var error = false;
		
		$("#admListDiv table tbody tr").each(function(index, val){
			if( $(val).attr("data-lgn-id") == lgnId ){
				$(".usr-error").html("This is already admin.");
				error = true;
				return false; // breaks
			}
		});
		
		if( error == false ){
			addAdmin(lgnId);
		}
	});
	
	$(document).on("click", ".delete-adm", function(){
		var lgnId = $(this).closest("tr").attr("data-lgn-id");
		
		swal({
			title: 'Are you sure?',
			text: "Do you really want to delete this admin ("+ lgnId +") ",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Delete'
		}).then((result) => {
			if (result.value) {
				deleteAdmin(lgnId);
			}
		})
	});
	
	$(document).on("click", "#addGlData", function(){
		
		$(".gl-form-wrapper .form-control, .gl-form-wrapper .select2-selection").removeClass("border-danger");
		
		var glAcc = $("#glAccount").val();
		var glSubAcc = $("#subAccount").val();
		var glSubAccTxt = $("#subAccount option:selected").text();
		var drAmt = $("#drAmt").val().trim();
		var crAmt = $("#crAmt").val().trim();
		var remark = $("#remark").val().trim();
		var error = false;
		
		if( glAcc == "" || glAcc == "-" ){
			error = true;
			$( "#glAccount" ).next().find(".select2-selection").addClass("border-danger");
		}
		
		if( glSubAcc == "" || glSubAcc == "-" ){
			error = true;
			$( "#subAccount" ).next().find(".select2-selection").addClass("border-danger");
		}
		
		if( (drAmt == "" || $.isNumeric(drAmt) == false || drAmt == 0) && (crAmt == "" || $.isNumeric(crAmt) == false || crAmt == 0) ){
			error = true;
			$( "#drAmt, #crAmt" ).addClass("border-danger");
		}
		
		if( remark == "" || remark.length == 0 ){
			remark = " ";
		}
		
		if( error == false ){
			
			if( $("#addGlData").hasClass("update") ){
				var uniqueId = $("#addGlData").attr("data-gl-id");
				$("#"+uniqueId).attr("data-glAcc", glAcc);
				$("#"+uniqueId).attr("data-glSubAcc", glSubAcc);
				$("#"+uniqueId).attr("data-drAmt", drAmt);
				$("#"+uniqueId).attr("data-crAmt", crAmt);
				$("#"+uniqueId).attr("data-remark", remark);
				
				var newData = '<td>'+ glAcc +'</td>'+
							'<td>'+ glSubAccTxt +'</td>'+
							'<td>'+ drAmt +'</td>'+
							'<td>'+ crAmt +'</td>'+
							'<td>'+ remark +'</td>'+
							'<td> <i class="icon-note menu-icon icon-pointer edit-gl-data"></i> &nbsp; <i class="icon-close menu-icon icon-pointer delete-gl-data"></i> </td>';
				$("#"+uniqueId).html(newData);
				
			}else{
				var d = new Date();
				var uniqueId = d.valueOf();
				
				var tr = '<tr id="'+ uniqueId +'" data-glAcc="'+ glAcc +'" data-glSubAcc="'+ glSubAcc +'" data-drAmt="'+ drAmt +'" data-crAmt="'+ crAmt +'" data-remark="'+ remark +'">'+
							'<td>'+ glAcc +'</td>'+
							'<td>'+ glSubAccTxt +'</td>'+
							'<td>'+ drAmt +'</td>'+
							'<td>'+ crAmt +'</td>'+
							'<td>'+ remark +'</td>'+
							'<td> <i class="icon-note menu-icon icon-pointer edit-gl-data"></i> &nbsp; <i class="icon-close menu-icon icon-pointer delete-gl-data"></i> </td>'+
						'</tr>';
				$("#glTbody").append(tr);
			}
			
			$("#remark").val("");
			$("#drAmt, #crAmt").val(0);
			$("#glAccount").val('-').trigger('change');	
			//$("#subAccount").html('<option values="-">-</option>');
			$("#subAccount").val($("#subAccount option:first").val()).trigger('change');
			
			$("#addGlData").removeClass("update").text("Add");
		}
	});
	
	$(document).on("click", ".edit-gl-data", function(){
		var $tr = $(this).closest( "tr" );
		
		$("#glAccount").val( $tr.attr("data-glAcc") ).trigger('change');
		globalGlSubAcc = $tr.attr("data-glSubAcc");
		$("#drAmt").val( $tr.attr("data-drAmt") );
		$("#crAmt").val( $tr.attr("data-crAmt") );
		$("#remark").val( $tr.attr("data-remark") );
		
		$("#addGlData").addClass("update").text("Update");
		$("#addGlData").attr("data-gl-id", $tr.attr("id"));
		
		$(".gl-form-wrapper").show();
		
	});
	
	$(document).on("click", ".delete-gl-data", function(){
		$(this).closest( "tr" ).remove();
	});
	
	$(document).on("click", "#closeGlForm", function(){
		$(".gl-form-wrapper").hide();
	});
	
	$(document).on("click", "#glFormBtn", function(){
		$("#remark").val("");
		$("#drAmt, #crAmt").val(0);
		$("#glAccount").val('-').trigger('change');	
		//$("#subAccount").html('<option values="-">-</option>');
		$("#subAccount").val($("#subAccount option:first").val()).trigger('change');
		$("#addGlData").removeClass("update").text("Add");
			
		$(".gl-form-wrapper").toggle();
	});
	
	$(document).on("click", "#reject, .reject", function(){
		
		if( $(this).hasClass("reject") ){
			var invNo = $(this).attr("data-invno");
			var ctlNo = $(this).attr("data-ctlno");
		}else{
			var invNo = $("#invNo").val();
			var ctlNo = $("#ctlNo").val();
		}
		
		swal({
			title: 'Are you sure?',
			text: "Do you really want to reject this Invoice ("+ invNo +") ",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Reject '
		}).then((result) => {
			if (result.value) {
				rejectDeleteInvoice("R", ctlNo);
			}
		})
	});
	
	$(document).on("click", ".delete", function(){
		
		var invNo = $(this).attr("data-invno");
		var ctlNo = $(this).attr("data-ctlno");
		
		swal({
			title: 'Are you sure?',
			text: "Do you really want to delete this Invoice ("+ invNo +") ",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Delete'
		}).then((result) => {
			if (result.value) {
				rejectDeleteInvoice("DEL", ctlNo);
			}
		})
	});
	
	$(document).on("click", ".cost-recon", function(){
		var venId = "";
		if( $(this).closest(".modal").attr("id") == "editTransModal"){
			venId = $("#transVndrNm").val().split("-")[0];
		}else if( $(this).closest(".modal").attr("id") == "editDataModal"){
			venId = $("#venId").val();
		}
		$("#invoiceVndrId").val(venId);
		getCostReconData(venId);
	});
	
	$(document).on("change", "#costReconVenId", function(){
		var venId = $(this).val();
		getCostReconData(venId);
	});
	
	
   
	$(document).on("click", "#addNewFolder, #closeNewFldrFrm", function(e){
		e.stopPropagation();
		$("#newFolderForm").toggleClass("show");
		$("body").toggleClass("open-add-folder-form");
	});
   
	$(document).on("click", "#saveNewFolder", function(){
		var newFolder = $("#newFolderName").val().trim();
		if( newFolder.length > 0 ){
			var newLi = '<li class="nav-item"><a class="nav-link" href="#" data-card-id="favourite" data-title="newFolder" data-from="100"><i class="icon-folder"></i>&nbsp; '+ newFolder +'</a></li>';
			$("#newFolderName").val('');
			$("#folderList ul").append(newLi);
			$("#newFolderForm").toggleClass("show");
			$("body").toggleClass("open-add-folder-form");
			if( $('a[href="#folderList"]').hasClass("collapsed") ){
				$('a[href="#folderList"]').trigger("click");
			}
			
			Swal.fire({
				title: 'New folder created successfully.',
				width: 400,
				padding: '1em 3em',
				timer: 5000,
			})
		}
	});
   
	$(document).on("click", ".add-block", function(){
		clearBlockModal();
		$("#editBlockMode").val(0);
		$("#editableWfblockId").val( $(this).attr("data-wf-block") );
		$("#blockModal").modal("show");
		
		getUsrBrhVndrList();
	});
	
	$(document).on("click", ".add-to-favourite", function(){
		$(this).toggleClass("text-warning");
	});
	
	$(document).on("click", "#newWfBtn", function(){
		$("#wfBlock1 .add-block").removeClass("hide-imp");
		$("#saveWfBtn").show();
		$("#wfTitle").attr("data-step-data", "");
		$("#wfName").val("");
		$("#wfOwner").val("").trigger("change");
		$("#wfStrtModal").modal("show");
		$("#wfAlert").html("");
	});
	
	$(document).on("click", ".edit-wf", function(){
		$("#mainLoader").show();
		
		var wkfId = $(this).parents("tr").attr("data-wkf-id");
		var pndgDoc = $(this).attr("data-pndg-doc");
		
		$("#saveWfBtn").attr("data-role", "update");
		$("#saveWfBtn").attr("data-wkf-id", wkfId);
		
		var arguments = "EDIT|" + wkfId + "|" + $("#loginId").val();
		var d = new Date();
		var n = d.getTime();
		var reportName = "cstm_edit_wkf_" + n + ".dat";
		
		$.ajax({
			data: {
				pgmPath: execPathWkf,
				Args: arguments,
				rptName: reportName
			},
			url: 'callPgmAndGetJSON.jsp',
			type: 'POST',
			dataType: 'text',
			success: function(data){
				//console.log(data);
				if (IsJsonString(data.trim())) {
					var arrResponse = JSON.parse(data.trim());
					//console.log(arrResponse);
					
					var wfBlockCnt = 2;
					
					var amountRow = 0, branchRow = 0, vendorRow = 0, daysRow = 0;
					var cndTyp = "", daysCndt = "", cndtn = "";
					if( arrResponse.amtFlg == 1 ){
						cndTyp = "amountRow";
						amountRow = 1;
						cndtn += (cndtn == "") ? '' : ' AND ';
						cndtn += "<span class='text-primary'>If amount between " + arrResponse.amtFrm + " - " + arrResponse.amtTo+ "</span>";
					}
					if( arrResponse.brhFlg == 1 ){
						cndTyp = "branchRow";
						branchRow = 1;
						cndtn += (cndtn == "") ? '' : ' AND ';
						cndtn += "<span class='text-primary'>If branch in (" + arrResponse.brh + ")</span>";
					}
					if( arrResponse.venFlg == 1 ){
						cndTyp = "vendorRow";
						vendorRow = 1;
						cndtn += (cndtn == "") ? '' : ' AND ';
						cndtn += "<span class='text-primary'>If vendor in (" + arrResponse.venId + ") </span>";
						cndtn += (arrResponse.costRecon == 1) ? " <span class='text-info'>Cost Reconciliation</span>" : '';
					}
					if( arrResponse.dayFlg == 1 ){
						cndTyp = "daysRow";
						daysRow = 1;
						cndtn += (cndtn == "") ? '' : ' AND ';
						if( arrResponse.dayBef == 1 ){
							daysCndt = "bfrInvDueDt";
							cndtn += "<span class='text-primary'>" + arrResponse.days + " days before invoice due date</span>";
						}else{
							daysCndt = "frmInvDt";
							cndtn += "<span class='text-primary'>" + arrResponse.days + " days from the invoice date</span>";
						}
					}

					var boxData = {};
					boxData.wkfId = arrResponse.wkfId;
					boxData.wkfName = arrResponse.wkfNm;
					boxData.crtdBy = $("#loginId").val();
					boxData.stpId = arrResponse.stpId;
					boxData.stepName = arrResponse.stpNm;
					boxData.amount = arrResponse.amtFrm;
					boxData.amountTo = arrResponse.amtTo;
					boxData.brh = arrResponse.brh;
					boxData.venId = arrResponse.venId;
					boxData.days = arrResponse.days;
					boxData.dayBef = arrResponse.dayBef;
					boxData.approvalTo = arrResponse.apvr;
					boxData.cndTyp = cndTyp;
					boxData.daysCndt = daysCndt;
					boxData.amountRow = amountRow;
					boxData.branchRow = branchRow;
					boxData.vendorRow = vendorRow;
					boxData.daysRow = daysRow;
					boxData.costRecon = arrResponse.costRecon;
					
					var boxDataStr = encodeURIComponent(JSON.stringify(boxData));
					
					var actionIcon = '';
					
					if( pndgDoc == 0 ){
						actionIcon = '<i class="icon-note edit-box" id="editBox'+wfBlockCnt+'" data-box-data="'+boxDataStr+'"></i> <i class="icon-close remove-box" id="closeBox'+wfBlockCnt+'"></i>';
						$("#saveWfBtn").show();
						$("#wfAlert").html('');
					}else{
						$("#saveWfBtn").hide();
						$("#wfAlert").html('<div class="alert alert-warning"><p class="m-0">This workflow is assign to '+ pndgDoc +' document(s), so it is not editable.</p></div>');
					}
					
					var boxHtml = '<div class="alert alert-success text-center" role="alert">Start <i class="add-block icon-plus hide-imp" data-wf-block="wfBlock1"></i></div><div class="row"> <div class="col"> <div class="wf-block" id="wfBlock'+wfBlockCnt+'"> '+ actionIcon +' <div class="text-center d-block"> <p class="condition-para"><span class="condition-span">'+cndtn+'</span></p> </div> <div class="text-center d-block"> <div class="alert alert-primary text-center m-0" role="alert"> <h4 class="mb-0">'+arrResponse.stpNm+'</h4><p class="mb-0"></p><p class="mb-0">Approver : '+arrResponse.apvr+'</p> </div> </div> <div class="row"> <div class="col"> <div class="text-center d-block"><img src="images/down-arrow24x24.png" class="down-arrow-img"></div> <div class="text-center d-block"> <div class="alert alert-success text-center" role="alert">End</div> </div> </div> </div> </div> </div> </div>';
					
					$("#workflowContent").removeClass("d-none");
					$("#wfTitle").text( arrResponse.wkfNm );
					$("#wfBlock1").html(boxHtml);
					
					$("#wfGridWrapper").hide();
					$("#wfAddWrapper").show();
					
					$("#mainLoader").hide();
				}
			},
			error: function(data){
				console.log(data.trim());
				$("#mainLoader").hide();
			}
		});
	});
	
	$(document).on("click", ".delete-wf", function(){
		var wkfId = $(this).parents("tr").attr("data-wkf-id");
		
		swal({
			title: 'Are you sure ?',
			text: "Do you really want to delete this workflow ?",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Delete'
		}).then((result) => {
			if (result.value) {
				deleteWorkFlow(wkfId);
			}
		})
	});
	
	$(document).on("click", "#saveWfBtn, #cancelWfBtn", function(){
		
		if( $(this).attr("id") == "saveWfBtn" ){
			
			if( $("#wfTitle").attr("data-step-data") != "" ){
				var boxData = JSON.parse(decodeURIComponent( $("#wfTitle").attr("data-step-data") ));
				console.log( boxData );
				
				var alertMode = "created";
				
				var amtFlg = 0, brhFlg = 0, venFlg = 0, dayFlg = 0, amtFrm = 0, amtTo = 0, days = 0, dayBef = 0, costRecon = 0;
				var brh = '', venId = '';
				
				var wkfId = ( $(this).attr("data-wkf-id") != undefined ) ? $(this).attr("data-wkf-id") : "";
				
				if( wkfId != "" && wkfId > 0 ){
					alertMode = "updated";
				}
				
				if( boxData.amountRow ){
					amtFlg = 1;
					amtFrm = boxData.amount;
					amtTo = boxData.amountTo;
				}

				if( boxData.branchRow ){
					brhFlg = 1;
					brh = boxData.brh.toString();
				}

				if( boxData.vendorRow ){
					venFlg = 1;
					venId = boxData.venId.toString();  
					
					if( boxData.costRecon ){
						costRecon = 1;
					}
				}

				if( boxData.daysRow ){
					dayFlg = 1;
					days = boxData.nbrDays;
				}
				
				if( boxData.daysCndt == "bfrInvDueDt"){
					dayBef = 1
				}

				var inptJson = {
					wkfId     : wkfId,
					wkfName   : boxData.wkfName,
					crtdBy    : boxData.crtdBy,
					stepName  : boxData.stepName,
					amtFlg    : amtFlg,
					brhFlg    : brhFlg,
					venFlg    : venFlg,
					dayFlg    : dayFlg,
					amtFrm    : amtFrm,
					amtTo     : amtTo,
					brh       : brh,
					venId     : venId,
					days      : days,
					dayBef    : dayBef,
					apvr      : boxData.approvalTo,
					costRecon : costRecon,
				};
				
				$("#mainLoader").show();

				var d = new Date();
				var n = d.getTime();
				var reportName = "cstm_insert_wkf_data_" + n + ".dat";
				var arguments = "INUP|" + JSON.stringify(inptJson);
				
				$.ajax({
					data: {
						Args: arguments,
						pgmPath: execPathWkf,
						rptName: reportName,
					},
					url: 'callPgmAndGetJSON.jsp',
					type: 'POST',
					dataType: 'text',
					success: function(data){
						if (IsJsonString(data.trim())) {
							var arrResponse = JSON.parse(data.trim());
							console.log(arrResponse);
							if( arrResponse.insFlg == 1 ){
								$("#wfBlock1 .row").remove();
								$("#wfAddWrapper").hide();
								$("#wfGridWrapper").show();
								
								swal({
									text: "Workflow " + alertMode + " successfully.",
									type: 'success',
								});
								
								getWorkFlows();
							}else{
								swal({
									text: "Something went wrong, please try again.",
									type: 'error',
								});
							}
							$("#mainLoader").hide();
						}
					},
					error: function(data){
						console.log(data);
						$("#mainLoader").hide();
					}
				});
			
			}else{
				swal({
					text: "Please add a step in this workflow.",
					type: 'error',
				});
			}
		}else{
			$("#saveWfBtn").attr("data-role", "save");
			$("#saveWfBtn").attr("data-wkf-id", "");
			
			$("#wfBlock1 .row").remove();
			$("#wfAddWrapper").hide();
			$("#wfGridWrapper").show();
		}
	});
	
	$(document).on("click", "#saveWfInfo", function(){
		$("#wfname-error").html("");
		
		if( $("#wfName").val() == "" ){
			$("#wfname-error").html("Please enter name for this workflow.");
			return;
		}else{
			$("#mainLoader").show();

			var d = new Date();
			var n = d.getTime();
			var reportName = "cstm_get_wkf_data_" + n + ".dat";
			var arguments = "CHK|" + $("#wfName").val().trim();
			
			$.ajax({
				data: {
					Args: arguments,
					pgmPath: execPathWkf,
					rptName: reportName,
				},
				url: 'callPgmAndGetJSON.jsp',
				type: 'POST',
				dataType: 'text',
				success: function(data){
					if (IsJsonString(data.trim())) {
						var arrResponse = JSON.parse(data.trim());
						
						if( arrResponse.wkfCnt == 0 ){
							$("#wfBlock1").removeClass("col-8");
							$("#wfTitle").text( $("#wfName").val() );
							$("#workflowContent").removeClass("d-none");
							
							$("#wfGridWrapper").hide();
							$("#wfAddWrapper").show();
							
							$("#wfStrtModal").modal("hide");
						}else{
							$("#wfname-error").html("This workflow is already exist, please choose diffrent name.");
						}
						$("#mainLoader").hide();
					}
				},
				error: function(data){
					console.log(data);
					$("#mainLoader").hide();
				}
			});
		}
	});
	
	$(document).on("click", "#saveBlock", function(){
		$("#mainLoader").show();
		
		$("#blockModal .form-control, #blockModal .select2-selection, .condition-checkbox-row").removeClass("border-danger");
		
		var editMode = false;
		var errorFlag = false;
		var destId = $("#editableWfblockId").val(); 
		
		if( $("#editBlockMode").val() == 0 ){
			wfBlockCnt++;
		}else{
			wfBlockCnt = destId.substring(7);
			editMode = true;
		}
		
		var cndtn = "";
		var stepName = $("#stepName").val();
		var assigneeMode = $("#assigneeMode").val();
		var message = $("#message").val();
		var approvalTo = $("#approvalTo").val();
		var emailTo = $("#emailTo").val();
		
		var amountRow = $("input[type='checkbox'][name='amountRow']").prop("checked");
		var branchRow = $("input[type='checkbox'][name='branchRow']").prop("checked");
		var vendorRow = $("input[type='checkbox'][name='vendorRow']").prop("checked");
		var daysRow = $("input[type='checkbox'][name='daysRow']").prop("checked");
		
		var costRecon = $("input[type='checkbox'][name='costRecon']").prop("checked");
		
		var amountTyp = $("#amountType").val();
		var amount = $("#amount").val();
		var amountTo = $("#amountTo").val();
		var brh = $("#brh").val();
		var venId = $("#vndr").val();
		var nbrDays = $("#nbrDays").val();
		var daysCndt = $("input[type='radio'][name='daysCndt']:checked").val();
		
		if( !amountRow && !branchRow && !vendorRow && !daysRow ){
			errorFlag = true;
			$(".condition-checkbox-row").addClass("border-danger");
		}
		
		if( amountRow ){
			//cndtn += "If amount " + amountTyp + " " + amount;
			if( amountTyp == "" || amountTyp == null ){
				errorFlag = true;
				$("#amountType").addClass("border-danger");
			}
			
			if( amount == "" ){
				errorFlag = true;
				$("#amount").addClass("border-danger");
			}
			
			if( amountTyp == "range" && amountTo=="" ){
				errorFlag = true;
				$("#amountTo").addClass("border-danger");
			}else if(amountTyp == "range" && parseFloat(amount) > parseFloat(amountTo) ){
				errorFlag = true;
				$("#amount, #amountTo").addClass("border-danger");
			}
			
			if( amountTyp == "range"){
				cndtn += "<span class='text-primary'>If amount between " + amount + " - " + amountTo + "</span>";
			}
		}
		
		if( branchRow ){
			cndtn += (cndtn == "") ? '' : ' AND ';
			cndtn += "<span class='text-primary'>If branch in (" + brh + ")</span>";
			if( brh == "" ){
				errorFlag = true;
				$("#brh").next().find(".select2-selection").addClass("border-danger");
			}
		}
		
		if( vendorRow ){
			cndtn += (cndtn == "") ? '' : ' AND ';
			cndtn += "<span class='text-primary'>If vendor in (" + venId + ") </span>";
			cndtn += (costRecon) ? "<span class='text-info'>Cost Reconciliation</span>" : '';
			
			if( venId == "" ){
				errorFlag = true;
				$("#vndr").next().find(".select2-selection").addClass("border-danger");
			}
		}
		
		if( daysRow ){
			cndtn += (cndtn == "") ? '' : ' AND ';
			
			if( daysCndt == "frmInvDt" ){
				cndtn += "<span class='text-primary'>" + nbrDays + " days from the invoice date</span>";
			}else{
				cndtn += "<span class='text-primary'>" + nbrDays + " days before invoice due date</span>";
			}
			
			if(nbrDays == ""){
				errorFlag = true;
				$("#nbrDays").addClass("border-danger");
			}
		}
		
		if( approvalTo == "" || approvalTo == null ){
			errorFlag = true;
			$("#approvalTo").addClass("border-danger");
			$("#appr-tab").trigger("click");
		}
		
		if( stepName == "" || stepName == null ){
			errorFlag = true;
			$("#stepName").addClass("border-danger");
		}
		
		if( errorFlag == true ){
			$("#mainLoader").hide();
			return;
		}
		
		var boxData = {};
		boxData.wkfName = $("#wfTitle").text();
		boxData.crtdBy = $("#loginId").val();
		boxData.stepName = stepName;
		boxData.assigneeMode = assigneeMode;
		boxData.message = message;
		boxData.approvalTo = approvalTo;
		boxData.emailTo = emailTo;
		boxData.amountRow = amountRow;
		boxData.branchRow = branchRow;
		boxData.vendorRow = vendorRow;
		boxData.daysRow = daysRow;
		boxData.amountTyp = amountTyp;
		boxData.amount = amount;
		boxData.amountTo = amountTo;
		boxData.brh = brh;
		boxData.venId = venId;
		boxData.nbrDays = nbrDays;
		boxData.daysCndt = daysCndt;
		boxData.costRecon = costRecon;
		
		var boxDataStr = encodeURIComponent(JSON.stringify(boxData));
		
		if(message != undefined){
			message = message.replace(/\n/g,"<br>");
		}else{
			message = "";
		}
		
		var rtnSts = validateCondition(boxData);
		
		if( rtnSts == false ){
			return;
		}
		
		if( editMode == true ){
			var blockStr = '<i class="icon-note edit-box" id="editBox'+wfBlockCnt+'" data-box-data="'+boxDataStr+'"></i> <i class="icon-close remove-box" id="closeBox'+wfBlockCnt+'"></i> <div class="text-center d-block"> <p class="condition-para"><span class="condition-span">'+cndtn+'</span></p> </div> <div class="text-center d-block"> <div class="alert alert-primary text-center m-0" role="alert"> <h4 class="mb-0">'+stepName+'</h4> <p class="mb-0">'+message+'</p> <p class="mb-0">Approver : '+approvalTo+'</p> </div> </div> <div class="row"> <div class="col"> <div class="text-center d-block"><img src="images/down-arrow24x24.png" class="down-arrow-img"></div> <div class="text-center d-block"> <div class="alert alert-success text-center" role="alert">End</div> </div> </div> </div>';
			$("#"+destId).html( blockStr );
		}else{
			if( $("#"+destId+" .row").length == 0 ){
				var blockStr = '<div class="row"> <div class="col"> <div class="wf-block" id="wfBlock'+wfBlockCnt+'"> <i class="icon-note edit-box" id="editBox'+wfBlockCnt+'" data-box-data="'+boxDataStr+'"></i> <i class="icon-close remove-box" id="closeBox'+wfBlockCnt+'"></i> <div class="text-center d-block"> <p class="condition-para"><span class="condition-span">'+cndtn+'</span></p> </div> <div class="text-center d-block"> <div class="alert alert-primary text-center m-0" role="alert"> <h4 class="mb-0">'+stepName+'</h4> <p class="mb-0">'+message+'</p> <p class="mb-0">Approver : '+approvalTo+'</p> </div> </div> <div class="row"> <div class="col"> <div class="text-center d-block"><img src="images/down-arrow24x24.png" class="down-arrow-img"></div> <div class="text-center d-block"> <div class="alert alert-success text-center" role="alert">End</div> </div> </div> </div> </div> </div> </div>';
				$("#"+destId).append( blockStr );
			}else{
				var blockStr = '<div class="col"> <div class="wf-block" id="wfBlock'+wfBlockCnt+'"> <i class="icon-note edit-box" id="editBox'+wfBlockCnt+'" data-box-data="'+boxDataStr+'"></i> <i class="icon-close remove-box" id="closeBox'+wfBlockCnt+'"></i> <div class="text-center d-block"> <p class="condition-para"><span class="condition-span">'+cndtn+'</span></p> </div> <div class="text-center d-block"> <div class="alert alert-primary text-center m-0" role="alert"> <h4 class="mb-0">'+stepName+'</h4> <p class="mb-0">'+message+'</p> <p class="mb-0">Approver : '+approvalTo+'</p> </div> </div> <div class="row"> <div class="col"> <div class="text-center d-block"><img src="images/down-arrow24x24.png" class="down-arrow-img"></div> <div class="text-center d-block"> <div class="alert alert-success text-center" role="alert">End</div> </div> </div> </div> </div> </div>';
				$("#"+destId+" .row:first").append( blockStr );
			}
		}
		
		$("#wfBlock1 .add-block").addClass("hide-imp");
		$("#wfTitle").attr("data-step-data", boxDataStr);
		
		manageWfWidth();
		
		$("#blockModal").modal("hide");
		$("#mainLoader").hide();
	});
	
	
	$(document).on('click', '.wf-block .remove-box', function(e) {
		var box = $(this).closest(".col");
		console.log(box);
		swal({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Delete '
		}).then((result) => {
			if (result.value) {
				$(box).remove();
				$("#wfBlock1 .add-block").removeClass("hide-imp");
				$("#wfTitle").attr("data-step-data", "");
				manageWfWidth();
			}
		})
	});
	
	$(document).on('click', '.wf-block .edit-box', function(e) {
		clearBlockModal();
		$("#editableWfblockId").val( $(this).parent().attr("id") );
		
		$(".condition-row").css({"transform": "scale(0)","height": "0"});
		
		$("input[type='checkbox'].blockCndt").prop("checked", false);
		
		var boxData = JSON.parse(decodeURIComponent($(this).attr("data-box-data")));
		console.log( boxData );
		$("#stepName").val( boxData.stepName );
		$("#assigneeMode").val( boxData.assigneeMode );
		$("#message").val( boxData.message );
		$("#approvalTo").val( boxData.approvalTo );
		$("#emailTo").val( boxData.emailTo ).trigger('change');
		//$("input[type='radio'][name='blockCndt'][value='"+boxData.cndTyp+"']").prop('checked', true);
		
		if( boxData.amountRow ){
			$("input[type='checkbox'][name='amountRow']").prop('checked', true);
			$("#amount").val( boxData.amount );
			$("#amountTo").val( boxData.amountTo );
			$("#amountTo").removeAttr("readonly");
			$("#amountRow").css({"height": "initial","transform": "scale(1)"});
			$("#amountRow").addClass("mb-2");
		}
		if( boxData.branchRow ){
			$("input[type='checkbox'][name='branchRow']").prop('checked', true);
			$("#brh").val( boxData.brh ).trigger('change');
			$("#branchRow").css({"height": "initial","transform": "scale(1)"});
			$("#branchRow").addClass("mb-2");
		}
		if( boxData.vendorRow ){
			$("input[type='checkbox'][name='vendorRow']").prop('checked', true);
			$("#vndr").val( boxData.venId ).trigger('change');
			$("#vendorRow").css({"height": "initial","transform": "scale(1)"});
			$("#vendorRow").addClass("mb-2");
			
			if( boxData.costRecon ){
				$("input[type='checkbox'][name='costRecon']").prop('checked', true);
			}else{
				$("input[type='checkbox'][name='costRecon']").prop('checked', false);
			}
		}
		if( boxData.daysRow ){
			$("input[type='checkbox'][name='daysRow']").prop('checked', true);
			$("#nbrDays").val( boxData.nbrDays );
			$("input[type='radio'][name='daysCndt'][value='"+boxData.daysCndt+"']").prop('checked', true);
			$("#daysRow").css({"height": "initial","transform": "scale(1)"});
			$("#daysRow").addClass("mb-2");
		}

		$("#editBlockMode").val(1);
		$("#blockModal").modal("show");
		
		getUsrBrhVndrList(boxData.approvalTo, boxData.brh, boxData.venId);
	});
	
	$('#sidebar').on('click', '.nav-link', function(e) {
		if( $(this).hasClass("has-child") ){
			return;
		}
		
		var $this = $(this);
		
		if( $("#wfAddWrapper").css("display") == "block" ){
			swal({
				title: 'Are you sure?',
				text: "Please stay on this page and save workflow otherwise your changes will be lost.",
				type: 'warning',
				showCancelButton: true,
				confirmButtonColor: '#3085d6',
				cancelButtonColor: '#d33',
				confirmButtonText: 'Stay',
				cancelButtonText: 'Leave',
			}).then((result) => {
				//console.log("result");
				//console.log(result);
				if (result.value) {
					return;
				}else if( result.dismiss == "cancel"){
					$("#wfAddWrapper").hide();
					menuWorking( $this );
				}
			})
		}else{
			menuWorking( $this );
		}
	}); 
	
	
	
	randomScalingFactor = function() {
		return Math.floor(Math.random() * 100) + 1;
	};
	
	/* invcVrfyByGraph();
	var labelArr = ["Vendor 1", "Vendor 2", "Vendor 3", "Vendor 4", "Vendor 5", "Vendor 6"];
	var dataArr = [randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor(), randomScalingFactor()];
	invcPndVndrGraph(labelArr, dataArr);
	invcPndByApprGraph();
	lstSixMthInvcGraph();
	avgCrdtdDay();
	acctPyblAge();
	apFnnlGraph(); */
	
	$(document).on("change", "#yearMonth", function(){
		invcVrfyByGraph();
	});
	$(document).on("change", "#vendors", function(){
		if( $("#vendors").val().length ){
			var dataArr = [];
			for( var i=0; i<$("#vendors").val().length; i++ ){
				if( $("#vendors").val()[i] == "Vendor 1" ){
					dataArr.push( 80 );
				}else if( $("#vendors").val()[i] == "Vendor 2" ){
					dataArr.push( 55 );
				}else if( $("#vendors").val()[i] == "Vendor 3" ){
					dataArr.push( 20 );
				}else if( $("#vendors").val()[i] == "Vendor 4" ){
					dataArr.push( 67 );
				}else if( $("#vendors").val()[i] == "Vendor 5" ){
					dataArr.push( 96 );
				}else if( $("#vendors").val()[i] == "Vendor 6" ){
					dataArr.push( 45 );
				}
			}
			invcPndVndrGraph($("#vendors").val(), dataArr);
		}
	});
	
	$(document).on("change", "#venCmpyId", function(){
		var cmpyId = $(this).val();
		getVendorList(cmpyId, true);
	});
	
	$(document).on("change", "#aipCmpyId", function(){
		var cmpyId = $(this).val();
		getVendorList(cmpyId);
	});
	
	$("#achVenId").change(function(){
		if( $("#achVenId").val() == undefined || $("#achVenId").val() == "" || $("#achVenId").val() == null ){
			return;
		}
		
		$("#mainLoader").show();
		
		var cmpyId = $("#venCmpyId").val();
		var venId = $("#achVenId").val();
		
		var arguments = "3|"+ cmpyId +"|"+ venId +"";
		var d = new Date();
		var n = d.getTime();
		var reportName = "cstm_ach_dtls_" + n + ".dat";

		$.ajax({
			data: {
				pgmPath: execPathCmnFld,
				Args: arguments,
				rptName: reportName
			},
			url: 'callPgmAndGetJSON.jsp',
			type: 'POST',
			dataType: 'text',
			success: function(data){
				$("#mainLoader").hide();
				
				if (IsJsonString(data.trim())) {
					var arrResponse = JSON.parse(data.trim());
					
					$("#achVenNm").val( arrResponse.venNm );
					$("#venLongNm").val( arrResponse.venLongNm );
					$("#vndrCry").val( arrResponse.cry );
				}
			},
			error: function(data){
				$("#mainLoader").hide();
				console.log(data.trim());
			}
		});
	});
	
		
	
		
	$("#aprvAutoImvcPymnt, #blockAutoImvcPymnt").click(function(){
		var invSts = "";
		
		if( $(this).attr("id") == "aprvAutoImvcPymnt" ){
			invSts = "A";
			
			aprvBlockPropInvc(invSts);
		} else if( $(this).attr("id") == "blockAutoImvcPymnt" ){
			$("#blockRemark").removeClass("border-danger");
			$("#blockRemark").val("");
			$("#invcBlockConfirmModal").modal("show");
		}
	});
	
	
	
	$("#receiverAIPSnglPropData").on("change", "table tbody input[type='checkbox'][name='select-invoices']", function(){
		if( $("#receiverAIPSnglPropData table tbody input[type='checkbox'][name='select-invoices']:checked").length == 0 ){
			$("#aprvAutoImvcPymnt, #blockAutoImvcPymnt").hide();
		}else{
			$("#aprvAutoImvcPymnt, #blockAutoImvcPymnt").show();
		}
	});
	
		
	
	
	$("#vldtBnkStmntTbl").on("click", ".edit-bank-trans", function(){
		var transId = $(this).closest("tr").attr("data-trans-id");
		var transRef = $(this).closest("tr").attr("data-trans-ref");
		var transType = $(this).closest("tr").attr("data-trans-type");
		
		var transDt = "", amount = "", accNo = "", bank = "", venCus = "";
		if( transId == "1" ){
			transDt = "06/09/2020";
			amount = "836.32";
		}else if( transId == "2" ){
			transDt = "06/08/2020";
			amount = "141.00";
		}else if( transId == "3" ){
			transDt = "06/07/2020";
			amount = "1306.88";
		}else if( transId == "4" ){
			transDt = "06/07/2020";
			amount = "5632.00";
		}else if( transId == "5" ){
			transDt = "06/07/2020";
			amount = "302.00";
		}else if( transId == "6" ){
			transDt = "06/07/2020";
			amount = "200.00";
		}else if( transId == "7" ){
			transDt = "06/07/2020";
			amount = "5522.00";
		}else if( transId == "8" ){
			transDt = "06/07/2020";
			amount = "35000.00";
		}
		
		if( transType == "VD" ){
			accNo = "4567891259456";
			bank = "Morgan Stanley";
			venCus = "1720";
		}else if( transType == "CC" ){
			accNo = "9421578523652";
			bank = "Bank of America";
			venCus = "1010";
		}
		
		$("#transAccNo").val(accNo);
		$("#transBnk").val(bank);
		
		$("#updtTrans").attr("data-trans-id", transId);
		$("#transVndrNm").attr("data-change-trigger", "0");
		
		$("#transVndrNm").val(venCus).trigger("change");
		
		$("#transDt").val(transDt);
		$("#transRef").val(transRef);
		$("#transAmt").val(amount);
		
		if( transRef != "" ){
			$("#vndrInvcTblWrapper").hide();
		}
		
		$("#editTransModal").modal("show");
	});
	
	$("#transVndrNm").change(function(){
		$("#vndrInvcTblWrapper").hide();
		
		if( $(this).val() == "" || $(this).val() == null ){
			return;
		}
		
		$("#mainLoader").show();
		
		var cmpyId = "AP3";
		var invDt = "";
		var venIdFrm = $(this).val();
		var venIdTo = $(this).val();
		
		var inptObj = {
			"cmpyId": cmpyId, 
			"invDt": invDt,
			"venIdFrm": venIdFrm,
			"venIdTo": venIdTo
		}
		var arguments = "10|" + JSON.stringify( inptObj );
		var d = new Date();
		var n = d.getTime();
		var reportName = "cstm_get_ach_data_" + n + ".dat";

		$.ajax({
			data: {
				pgmPath: execPathCmnFld,
				Args: arguments,
				rptName: reportName
			},
			url: 'callPgmAndGetJSON.jsp',
			type: 'POST',
			dataType: 'text',
			success: function(data){
				var changeTrigger = parseInt($("#transVndrNm").attr("data-change-trigger"));
				$("#transVndrNm").attr("data-change-trigger", changeTrigger+1);
				
				if (IsJsonString(data.trim())) {
					var arrResponse = JSON.parse(data.trim());
					var recordCount = arrResponse.pendingList.length;
					var poStr = "", logStatus = "", checkboxStr = "";
					var tbl = '<table class="table table-hover table-bordered mb-4">'+
								'<thead>'+
									'<tr>'+
										'<th>Vendor ID</th>'+
										'<th>Invoice No.</th>'+
										'<th>Invoice Date</th>'+
										'<th>PO</th>'+
										'<th>Voucher No.</th>'+
										'<th>Branch</th>'+
										'<th class="text-right">Amount</th>'+
										'<th>Entered Date</th>'+
									'</tr>'+
								'</thead>'+
								'<tbody>';
					
					for( var i=0; i<recordCount; i++ ){
						if( arrResponse.pendingList[i].logSts == "Y" ){
							if( arrResponse.pendingList[i].poNo == 0 ){
								poStr = "";
							}else{
								poStr = arrResponse.pendingList[i].poPfx +'-'+ arrResponse.pendingList[i].poNo +'-'+ arrResponse.pendingList[i].poItm;
							}
							
							tbl += '<tr data-invoice-no="'+ arrResponse.pendingList[i].vendInvNo +'" data-vchr-no="'+ arrResponse.pendingList[i].opaPfx +'-'+ arrResponse.pendingList[i].opaNo +'">'+
										'<td>'+ arrResponse.pendingList[i].vendId +'</td>'+
										'<td>'+ arrResponse.pendingList[i].vendInvNo +'</td>'+
										'<td>'+ arrResponse.pendingList[i].vendInvDt +'</td>'+
										'<td>'+ poStr +'</td>'+
										'<td>'+ arrResponse.pendingList[i].opaPfx +'-'+ arrResponse.pendingList[i].opaNo +'</td>'+
										'<td>'+ arrResponse.pendingList[i].apBrh +'</td>'+
										'<td class="text-right">'+ arrResponse.pendingList[i].amount +' '+ arrResponse.pendingList[i].cry +'</td>'+
										'<td>'+ arrResponse.pendingList[i].entDt +'</td>'+
									'</tr>';
						}
					}
					tbl += '</tbody></table>';
					$("#vndrInvcTblWrapper").html(tbl);
					
					if( parseInt($("#transVndrNm").attr("data-change-trigger")) >= 2){
						$("#vndrInvcTblWrapper").show();
					}else{
						$("#vndrInvcTblWrapper").hide();
					}
					
					if( $("#transRef").val() == "" ){
						$("#vndrInvcTblWrapper").show();
					}
					
					$("#mainLoader").hide();
				}
			},
			error: function(data){
				console.log(data);
			}
		});
	});
	
	$("#vndrInvcTblWrapper").on("click", "table tbody tr", function(){
		$("#vndrInvcTblWrapper table tbody tr").removeClass("active-tr");
		$(this).addClass("active-tr");
		
		$("#transRef").val( $(this).attr("data-invoice-no") );
	});
	
	$("#uploadBnkStmntFile").change(function() {
		var fileType = "";
		
		for(i = 0; i < this.files.length ; i++){
			if (this.files && this.files[i]) {
				var fileNm = this.files[i].name;
				var fileExt = fileNm.split(".").pop().toLowerCase();

				var uploadFile = document.getElementById("uploadBnkStmntFile").files[i];
				uploadFileUrl = URL.createObjectURL(uploadFile);
				
				$("#uploadBnkStmntFile").next().html("<b>"+ fileNm +"</b>");
			}
		}
	});
	
	$("#upldBankFile").click(function(){
		var formElement = document.forms.namedItem("bankFileImportForm");
		var formData = new FormData(formElement);
		var upload = false;
		var fileName = "";
		var file = [];
		
		$.each(formData.getAll("file"), function(index, value) {
			if( value.name != "" && value.size > 0 ){
				upload = true;
				fileName = value.name;
			}
		});
		
		formData.append("fileFormat", $("#bnkStmntFlFrmt").val());
		formData.append("postParam", $("input[type='radio'][name='postParam']:checked").val());
		
		if( upload === true ){
			$("#mainLoader").show();
			$.ajax({
				data: formData,
				url: 'uploadToServer.jsp',
				type: 'POST',
				cache: false,
				processData: false,
				contentType: false,
				success: function(out) {
					if(out.trim() == "0") {
						swal({
							text: "Maximum File Size Exceeded.",
							type: 'error',
						});
					}else{
						swal({
							text: "File successfully imported.",
							type: 'success',
						});
					}
					
					$("#uploadBnkStmntFile").val("");
					$("#uploadBnkStmntFile").next().html("Choose file...");
					
					$("#mainLoader").hide();
				},
				error: function() {
					swal({
							text: "File Uploading Failed.",
							type: 'error',
						});
					$("#mainLoader").hide();
				}
			});
		}else{
			swal({
				text: "Please select a File.",
				type: 'warning',
			});
		}
	});
	
	$("#fltrTransRow").click(function(){
		$("#vldtBnkStmntTbl tbody tr").hide();
		
		var stmntStartDt = stmntEndDt = transStartDt = transEndDt = "";
		var stsFltr = stmntDtFltr = transDtFltr = false;
		var status = $("#transStsFltr").val();
		var stmntDt = $("#stmntDtFltr").val();
		var transDt = $("#transDtFltr").val();
		
		if( status != "" && status != "AL" ){
			stsFltr = true;
		} 
		
		if( stmntDt != "" ){
			stmntDtFltr = true;
			
			stmntStartDt = stmntDt.split(" - ")[0];
			stmntEndDt = stmntDt.split(" - ")[1];
			stmntStartDt = stmntStartDt.split('/').join('-');
			stmntEndDt = stmntEndDt.split('/').join('-');
		}
		
		if( transDt != "" ){
			transDtFltr = true;
			transStartDt = transDt.split(" - ")[0];
			transEndDt = transDt.split(" - ")[1];
			transStartDt = transStartDt.split('/').join('-');
			transEndDt = transEndDt.split('/').join('-');
		}
		
		$("#vldtBnkStmntTbl tbody tr").each(function(index, val){
			var stsShw = stmntDtShw = transDtShw = true;
			var rowSts = $(val).attr("data-sts");
			var rowStmntDt = $(val).attr("data-stmnt-dt");
			var rowTransDt = $(val).attr("data-trans-dt");
			
			//Status Filtering
			if( stsFltr ){
				if( rowSts.toLowerCase() != status.toLowerCase() ){
					stsShw = false;
				}
			}
			
			//Statement Date Filtering
			if( stmntDtFltr ){
				if( rowStmntDt != "" ){
					if( (new Date(rowStmntDt) < new Date(stmntStartDt)) || (new Date(rowStmntDt) > new Date(stmntEndDt)) ){
						stmntDtShw = false;
					}
				}else{
					stmntDtShw = false;
				}
			}
			
			//Transaction Date Filtering
			if( transDtFltr ){
				if( rowTransDt != "" ){
					if( (new Date(rowTransDt) < new Date(transStartDt)) || (new Date(rowTransDt) > new Date(transEndDt)) ){
						transDtShw = false;
					}
				}else{
					transDtShw = false;
				}
			}
			
			if( stsShw && stmntDtShw && transDtShw ){
				$(val).show();
			}
			
		});
	});
	
	$("#senderAIPSnglPropAprvdData, #aipCreatedTbl").on("click", ".view-ach-file", function(){
		$("#viewACHFileModal").modal("show");
	});
	
	
	
	
	$("#updtTrans").click(function(){
		$("#mainLoader").show();
		
		var transId = $(this).attr("data-trans-id");
		
		setTimeout(function(){
			$("#mainLoader").hide();
		
			$("#vldtBnkStmntTbl tr[data-trans-id='"+ transId +"']").find("span.badge").removeClass("badge-warning").addClass("badge-success").html("Posted");
			$("#vldtBnkStmntTbl tr[data-trans-id='"+ transId +"']").find(".edit-bank-trans").remove();
			
			$("#editTransModal").modal("hide");
		}, 1000);
		
	});
	
	$('#viewAutoInvcPymntModal').on('hidden.bs.modal', function () {
		getAprvInvcPymntTblData();
	});
	
	
})















/* END */

$(function(){
	
		
	$(document).on('click',".img-thumbnail", function(){
		var imgURL = $(this).attr("data-url");
		var fileType = $(this).attr("data-flTyp");
		
		$('#docPreview, #preview-img img').attr('src', imgURL);
			
		if( fileType == "pdf" ){
			$('#preview-iframe, #docPreview').slideDown("slow");
			$("#preview-img").hide();
		}else if( fileType = "image" ){
			$('#docPreview').hide();
			$("#preview-img").slideDown("slow");
		}
	});
	
	$(document).on('click',"#startBtn", function(){
		var newBox = '<div class="col-1"></div><div class="col-2"><div class="wf-box">ABCD</div></div>';
		$("#createWorkflowArea .row").append( newBox );
	});
	
	    
    /* $(document).on("click", ".pdf-icon, .image-icon, .view-icon", function(){
		if( $(this).attr("data-file-type") == "image" ){
			$("#viewPdf").hide();
			$("#viewImage").show();
		}else if( $(this).attr("data-file-type") == "pdf" ){
			$("#viewPdf").show();
			$("#viewImage").hide();
		}
        $("#viewFileModal").modal("toggle");
    }); */
	
	$(document).on("click", "#updateApprove", function(){
		
		$(".gl-form-wrapper").hide();
		
		var invDt = $("#invDt").val().split("/");
		var invFmtDt = invDt[2] + invDt[0] + invDt[1];
		
		var entDt = $("#entDt").val().split("/");
		var entFmtDt = entDt[2] + entDt[0] + entDt[1];
		
		var dueDt = $("#dueDt").val().split("/");
		var dueFmtDt = dueDt[2] + dueDt[0] + dueDt[1];
		
		var extlRef = ($("#extlref").val().trim() == "") ? "-" : $("#extlref").val().trim();
		var poNo = ($("#poNo").val().trim() == "") ? "0" : $("#poNo").val().trim();

		$("#editDataModal p.text-danger").remove();
		
		var error = false;
		
		if( $("#invNo").val().trim() == "" ){
			error = true;
			$( "<p class='text-danger'>Please enter Invoice No.</p>" ).insertAfter( "#invNo" );
		}
		/*if( $("#extlref").val().trim() == "" ){
			error = true;
			$( "<p class='text-danger'>Please enter Ext Ref.</p>" ).insertAfter( "#extlref" );
		}*/
		if( $("#apBrh").val().trim() == "" ){
			error = true;
			$( "<p class='text-danger'>Please enter Branch.</p>" ).insertAfter( "#apBrh" );
		}
		if( $("#amt").val().trim() == "" ){
			error = true;
			$( "<p class='text-danger'>Please enter Amount.</p>" ).insertAfter( "#amt" );
		}else if( $.isNumeric($("#amt").val().trim()) == false ){
			error = true;
			$( "<p class='text-danger'>Please enter valid Amount.</p>" ).insertAfter( "#amt" );
		}
		if( $("#cry").val().trim() == "" ){
			error = true;
			$( "<p class='text-danger'>Please enter Currency.</p>" ).insertAfter( "#cry" );
		}
		if( $("#invDt").val().trim() == "" ){
			error = true;
			$( "<p class='text-danger'>Please enter Invoice Date.</p>" ).insertAfter( "#invDt" );
		}else if( parseInt(invFmtDt).toString().length < 8 ){
			error = true;
			$( "<p class='text-danger'>Invalid Invoice Date.</p>" ).insertAfter( "#invDt" );
		}
		if( $("#entDt").val().trim() == "" ){
			error = true;
			$( "<p class='text-danger'>Please enter Entered Date.</p>" ).insertAfter( "#entDt" );
		}
		
		var glDataArray = [];
		
		$("#glTbody tr").each(function(index, val){
			var eachGlData = {};
			eachGlData.glAcc = $(val).attr("data-glacc");
			eachGlData.glSubAcc = $(val).attr("data-glSubAcc");
			eachGlData.drAmt = $(val).attr("data-drAmt");
			eachGlData.crAmt = $(val).attr("data-crAmt");
			eachGlData.remark = $(val).attr("data-remark");
			
			glDataArray.push(eachGlData);
		});
		
		var glDataJson = {
			glData : glDataArray,
		};

		if( error == false ){
			var arguments = "Q|"+
							$("#ctlNo").val() + "|" +
							$("#invNo").val() + "|" +
							extlRef + "|" +
							$("#apBrh").val() + "|" +
							$("#amt").val() + "|" +
							$("#cry").val() + "|" +
							invFmtDt + "|" +
							entFmtDt + "|" +
							poNo + "|" +
							$("#venId").val();
			approveInvoice(arguments, $("#venId").val(), $("#invNo").val());
		}
	});
	
	$(document).on("click", ".approve", function(){
		var glDataArray = [];
		var glDataJson = {
			glData : glDataArray,
		};
		
		var arguments = "Q|"+
						$(this).parent().find(".edit-row").attr("data-ctlno") + "|" +
						$(this).parent().find(".edit-row").attr("data-invno") + "|" +
						" " + "|" +
						$(this).parent().find(".edit-row").attr("data-apbrh") + "|" +
						$(this).parent().find(".edit-row").attr("data-amt") + "|" +
						$(this).parent().find(".edit-row").attr("data-cry") + "|" +
						$(this).parent().find(".edit-row").attr("data-invDtSrvc") + "|" +
						$(this).parent().find(".edit-row").attr("data-entDtSrvc") + "|" +
						$(this).parent().find(".edit-row").attr("data-dueDtSrvc") + "|" +
						$(this).parent().find(".edit-row").attr("data-pono") + "|" +
						$(this).parent().find(".edit-row").attr("data-venId") + "|" +
						JSON.stringify(glDataJson);
		approveInvoice(arguments, $(this).parent().find(".edit-row").attr("data-venId"), $(this).parent().find(".edit-row").attr("data-invno"));
	});
	
	$(document).on("change", "#glAccount", function(){
		getGlSubAccount();
	});
	
	$(document).on("click", ".edit-row", function(){
		$(".gl-form-wrapper").hide();
		$("#glTbody").html("");
		
		var date = moment();
		var newDate = moment(date).add(-2, 'minutes');
		var datStr = "[" + newDate.format("DD/MM/YYYY HH:mm A") + "]";
		$("span.date").html( datStr );
		
		var fileNm = $(this).attr("data-flNm");
		var ctlNo = $(this).attr("data-ctlNo");
		var flTyp = $(this).attr("data-file-type");
		var source = $(this).attr("data-source");
		var amount = $(this).attr("data-amt");
		var trnsts = $(this).attr("data-trnsts");
		
		var rowVal = this;
		
		if( trnsts == "R" ){
			$("#reject").hide();
			$("#rejectedHeading").show();
		}else{
			$("#reject").show();
			$("#rejectedHeading").hide();
		}
		
		if( source == "Email" ){
			$(".log-box p:eq(0)").html("<span class='date'>"+datStr+"</span>From email");
			$(".log-box p:eq(1)").hide();
		}else{
			$(".log-box p:eq(0)").html("<span class='date'>"+datStr+"</span>Uploaded usign application");
			
			if( parseFloat(amount) > 500 && parseFloat(amount) < 1000 ){
				$(".log-box p:eq(1)").show();
			}else{
				$(".log-box p:eq(1)").hide();
			}
		}
		
		$("#mainLoader").show();
		
		$.ajax({
			data: {
				file: fileNm,
				atchmtId: ctlNo,
				dwnld: "V",
				appName: $("#appName").val(),
				execPath: execPath,
				redirect: false
			},
			url: 'downloadFromServer.jsp',
			type: 'GET',
			success: function(data){
				
				$("#invNo, #extlref, #apBrh, #amt, #cry, #invDt, #entDt, #dueDt, #venNm, #remark").val( '' );
				$("#drAmt, #crAmt").val(0);
				$("#glAccount").val('-').trigger('change');	
				//$("#subAccount").html('<option values="-">-</option>');
				$("#subAccount").val($("#subAccount option:first").val()).trigger('change');
	
				if( flTyp == "image" ){
					$("#editViewPdf").hide();
					$("#editViewImage").show();
					
					$("#editViewImage").attr("src","uploads/"+fileNm);
					
				}else if( flTyp == "pdf" ){
					$("#editViewPdf").show();
					$("#editViewImage").hide();
					
					$("#editViewPdf").attr("src","uploads/"+fileNm);
				}
				
				$("#invNo").val( $(rowVal).attr("data-invNo") );
				$("#extlref").val( $(rowVal).attr("data-extlref") );
				$("#apBrh").val( $(rowVal).attr("data-apBrh") );
				$("#amt").val( $(rowVal).attr("data-amt") );
				$("#cry").val( $(rowVal).attr("data-cry") );
				$("#invDt").val( $(rowVal).attr("data-invDt") );
				$("#entDt").val( $(rowVal).attr("data-entDt") );
				$("#dueDt").val( $(rowVal).attr("data-duedt") );
				$("#venNm").val( $(rowVal).attr("data-vennm") );
				$("#ctlNo").val( ctlNo );
				
				if( $(rowVal).attr("data-pono") != "0" ){
					$("#poNo").val( $(rowVal).attr("data-pono") );
				}
				
				$('#invDt, #entDt, #dueDt').daterangepicker({
					singleDatePicker: true,
					showDropdowns: true,
					opens: 'left',
					locale: {
						format: 'MM/DD/YYYY'
					}
				});
				
				if( $(rowVal).attr("data-payTerms") != undefined ){
					$("#payTerms").val( decodeURIComponent(escape($(rowVal).attr("data-payTerms"))) );
				}
				
				$("#tab-content-text").attr("data-srv-call", "1");
				$("#nav-tab-pdf").trigger("click");
				
				getGlData(rowVal);
				
			},
			error: function(data){
				console.log(data);
			}
		});
		
    });
	
	
	$(document).on("click", "#nav-tab-text", function(){
		
		if( $("#tab-content-text").attr("data-srv-call") == "1" ){
			$("#tab-content-text pre").html("")
			$("#mainLoader").show();
			$.ajax({
				data: {
					ctlNo: $("#ctlNo").val(),
					redirect: false
				},
				url: 'readTxtFile.jsp',
				type: 'GET',
				success: function(data){
					//console.log(data.trim());
					$("#mainLoader").hide();
					//$("#tab-content-text pre").html( decodeURIComponent(encodeURIComponent(data.trim())) );
					$("#tab-content-text pre").html( decodeURIComponent(escape(data.trim())) );
					$("#tab-content-text").attr("data-srv-call", "0");
				},
				error: function(data){
					console.log(data);
				}
			});
		}
		
	});
	
	$(document).on("click", ".download-icon", function(){
		
		var fileNm = $(this).attr("data-flNm");
		var ctlNo = $(this).attr("data-ctlNo");
		$("#mainLoader").show();
		$.ajax({
			data: {
			file: fileNm,
			atchmtId: ctlNo,
			dwnld: "N",
			execPath: execPath,
			appName: $("#appName").val(),
			redirect: false
			},
			url: 'downloadFromServer.jsp',
			type: 'GET',
			success: function(data){
				$("#mainLoader").hide();
				window.location="downloadFromServer.jsp?file=" + fileNm.replaceAll("&","%26") + "&dwnld=Y&appName=" + $("#appName").val() + "&execPath=" + execPath;
			},
			error: function(data){
				console.log(data);
			}
		});		
    });
	
	$(document).on("click", ".view-icon", function(){
		
		$("#viewImage, #viewPdf").attr("src","");
		$("#viewPdf").hide();
		$("#viewImage").hide();
		
		var fileNm = $(this).attr("data-flNm");
		var ctlNo = $(this).attr("data-ctlNo");
		var flTyp = $(this).attr("data-file-type");
		
		$("#mainLoader").show();
		
		$.ajax({
			data: {
				file: fileNm,
				atchmtId: ctlNo,
				dwnld: "V",
				appName: $("#appName").val(),
				execPath: execPath,
				redirect: false
			},
			url: 'downloadFromServer.jsp',
			type: 'GET',
			success: function(data){
				
				$("#closeViewModal").attr("data-flNm",fileNm);
				
				if( flTyp == "image" ){
					$("#viewPdf").hide();
					$("#viewImage").show();
					
					$("#viewImage").attr("src","uploads/"+fileNm);
				}else if( flTyp == "pdf" ){
					$("#viewPdf").show();
					$("#viewImage").hide();
					
					$("#viewPdf").attr("src","uploads/"+fileNm);
				}
				
				$("#viewFileModal").modal("toggle");
				$("#mainLoader").hide();
			},
			error: function(data){
				console.log(data);
			}
		});
    });
	
	
	$(document).on("click", "#closeViewModal", function(){
		deleteUploadedFile($(this).attr("data-flNm"));
	});
	
	var deleteUploadedFile = function(filename){
		$.ajax({
			data: {
				filename: filename
			},
			url: 'deleteFromServer.jsp',
			type: 'POST',
			success: function(out) {
				//console.log( out.trim() );
			},
			error: function(error) {
				console.log( error );
			}
		});
	}
	
	//deleteUploadedFile("");

});


/* +
	'<img src="images/view_icon.png" class="img-responsive view-icon" alt="File" title="View" data-ctlNo="'+ arrResponse[i].ctlNo + '" data-flNm="'+ arrResponse[i].flName+ '" data-file-type="'+ fileType +'"/> '+ 
	'<img src="images/download_icon.png" class="img-responsive download-icon" alt="File" title="Download" data-ctlNo="'+ arrResponse[i].ctlNo + '" data-flNm="'+ arrResponse[i].flName+ '"/>' */


String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

function getCountryList(){
	var arguments = "14";
	var d = new Date();
	var n = d.getTime();
	var reportName = "cstm_ach_dtls_" + n + ".dat";

	$.ajax({
		data: {
			pgmPath: execPathCmnFld,
			Args: arguments,
			rptName: reportName
		},
		url: 'callPgmAndGetJSON.jsp',
		type: 'POST',
		dataType: 'text',
		success: function(data){
			if (IsJsonString(data.trim())) {
				var arrResponse = JSON.parse(data.trim());
				globalCountryList = "<option value=''>&nbsp;&nbsp;&nbsp;&nbsp;</option>" + arrResponse.ctyList;
				$("#bankCountry, #bnkCty").html("<option value=''>&nbsp;&nbsp;&nbsp;&nbsp;</option>" + arrResponse.ctyList);
			}
		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data);
		}
	});
}

function aprvBlockPropInvc(invSts, remark = ""){
	$("#mainLoader").show();
	
	var propId = $("#viewAutoInvcPymntModal").attr("data-prop-id");
	var reqId = $("#viewAutoInvcPymntModal").attr("data-req-id");
	var vchrNoArr = [];
	
	$("#receiverAIPSnglPropData table tbody input[type='checkbox'][name='select-invoices']:checked").each(function(index, checkbox){
		vchrNoArr.push( $(checkbox).closest("tr").attr("data-vchr-no") );
	});
	
	var inptObj = {
		"propId": propId, 
		"reqId": reqId, 
		"invSts": invSts,
		"invNo" : vchrNoArr.join(","),
		"rmk" : remark,
	};
	
	var arguments = "9|" + JSON.stringify(inptObj);
	var d = new Date();
	var n = d.getTime();
	var reportName = "cstm_ach_dtls_" + n + ".dat";

	$.ajax({
		data: {
			pgmPath: execPathCmnFld,
			Args: arguments,
			rptName: reportName
		},
		url: 'callPgmAndGetJSON.jsp',
		type: 'POST',
		dataType: 'text',
		success: function(data){
			if (IsJsonString(data.trim())) {
				var arrResponse = JSON.parse(data.trim());
				
				if( arrResponse.rtnSts == "0" ){
					getReceiverSnglPropData(inptObj.propId, inptObj.reqId);
					
					if( $("#receiverAIPSnglPropData table tbody tr input[type='checkbox'][name='select-invoices']").length == $("#receiverAIPSnglPropData table tbody tr input[type='checkbox'][name='select-invoices']:checked").length){
						$("#aprvAutoImvcPymnt, #blockAutoImvcPymnt").hide();
					}
					
					$("#blockRemark").val("");
					$("#invcBlockConfirmModal").modal("hide");
				}else if( arrResponse.rtnSts == "1" ){
					swal({
						text: arrResponse.rtnMsg,
						type: 'error',
					});
					$("#mainLoader").hide();
				}
			}
		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data.trim());
		}
	});
}


function getReceiverSnglPropData(propId, reqId){
	$("#aprvAutoImvcPymnt, #blockAutoImvcPymnt").hide();

	$("#viewAutoInvcPymntModal").attr("data-prop-id", propId);
	$("#viewAutoInvcPymntModal").attr("data-req-id", reqId);
	
	var arguments = "8|" + propId;
	var d = new Date();
	var n = d.getTime();
	var reportName = "cstm_ach_dtls_" + n + ".dat";

	$.ajax({
		data: {
			pgmPath: execPathCmnFld,
			Args: arguments,
			rptName: reportName
		},
		url: 'callPgmAndGetJSON.jsp',
		type: 'POST',
		dataType: 'text',
		success: function(data){
			if (IsJsonString(data.trim())) {
				var arrResponse = JSON.parse(data.trim());
				var recordCount = arrResponse.length;
				var poStr = "", checkboxStr = "", rowClass = "";
				var tbl = '<table class="table table-hover">'+
							'<thead>'+
								'<tr class="thead-clr">'+
									'<th>#</th>'+
									'<th>Vendor ID</th>'+
									'<th>Invoice No.</th>'+
									'<th>Invoice Date</th>'+
									'<th>PO</th>'+
									'<th>Voucher No.</th>'+
									'<th>Branch</th>'+
									'<th class="text-right">Amount</th>'+
									'<th>Entered Date</th>'+
								'</tr>'+
							'</thead>'+
							'<tbody>';
				for( var i=0; i<recordCount; i++ ){
					checkboxStr = "", rowClass = "";
					if( arrResponse[i].poNo == 0 ){
						poStr = "";
					}else{
						poStr = arrResponse[i].poPfx +'-'+ arrResponse[i].poNo +'-'+ arrResponse[i].poItm;
					}
					
					if( arrResponse[i].achSts == "P" ){
						checkboxStr = '<div class="form-check form-check-flat m-0">'+
									'<label class="form-check-label">'+
									  '<input type="checkbox" class="form-check-input" name="select-invoices" checked>'+
									'<i class="input-helper"></i></label>'+
								 '</div>';
					}else if( arrResponse[i].achSts == "B" ){
						rowClass = "row-bg-danger";
					}else{
						rowClass = "row-bg-primary";
					}
					//console.log(poStr);
					tbl += '<tr class="'+ rowClass +'" data-invoice-no="'+ arrResponse[i].vendInvNo +'" data-vchr-no="'+ arrResponse[i].opaPfx +'-'+ arrResponse[i].opaNo +'">'+
								'<td>'+ checkboxStr +'</td>'+
								'<td>'+ arrResponse[i].vendId +'</td>'+
								'<td>'+ arrResponse[i].vendInvNo +'</td>'+
								'<td>'+ arrResponse[i].vendInvDt +'</td>'+
								'<td>'+ poStr +'</td>'+
								'<td>'+ arrResponse[i].opaPfx +'-'+ arrResponse[i].opaNo +'</td>'+
								'<td>'+ arrResponse[i].apBrh +'</td>'+
								'<td class="text-right">'+ arrResponse[i].amount +' '+ arrResponse[i].cry +'</td>'+
								'<td>'+ arrResponse[i].entDt +'</td>'+
							'</tr>';
				}
				tbl += '</tbody></table>';
				$("#receiverAIPSnglPropData").html(tbl);
				
				if( recordCount ){
					$("#aprvAutoImvcPymnt, #blockAutoImvcPymnt").show();
				}
		
				$("#viewAutoInvcPymntModal").modal("show");
				
			}
			$("#mainLoader").hide();
		},
		error: function(data){
			console.log(data);
		}
	});
}


function menuWorking( $this ){
	$('.nav-item').removeClass("active");
	$this.parent().addClass("active");
	
	$(".section").hide();
	
	var cardId = $this.attr('data-card-id');
	$("#"+cardId).show();
	
	if( cardId == 'ocrHistory' ){
		getOcrHistory();
	}else if( cardId == 'retrieve' ){
		//getRetrieve();
		$("#tabToReviewInvc").trigger("click");
	}else if( cardId == 'favourite' ){
		getFavourite($this.attr('data-title'), $this.attr('data-from'));
	}else if( cardId == 'stxEntry' ){
		getStratixEntries();
	}else if( cardId == 'workFlow' ){
		getWorkFlows();
	}else if(cardId == 'company' || cardId == 'vendor' || cardId == 'autoInvcPymnt'){
		getCmpyList(cardId);
		if( cardId == 'company' ){
			//getCmpyAchTblData();
		}else if( cardId == 'vendor' ){
			getVndrAchTblData();
			
			$("#addUpdateVendorDiv").hide();
			$("#addVndrBtn").show();
			$("#addVndrBtn, #achVndrSavedArea").slideDown();
		}else if( cardId == 'autoInvcPymnt' ){
			getAutoInvcPymntTblData();
			
			$("#aipRqstId").val("");
			$("#stxEntryContent, #blockedVoucherTblWrapper").html("");
			$("#filterVoucherResults").hide();
			$("#aipCreateGroupArea, #invcDrpActns").hide();
			$("#aipCreatedGroupArea").slideDown();
		}
	}else if( cardId == "aprvInvcPymnt" ){
		getAprvInvcPymntTblData();
	}else if( cardId == "vldtBnkStmnt" ){
		$("#mainLoader").show();
		$("#vldtBnkStmntTbl").hide();
		getVendorList("AP3");
		
		setTimeout(function(){
			$("#mainLoader").hide();
			$("#vldtBnkStmntTbl").slideDown();
		}, 1500);
	}else if( cardId == "bankSection" ){
		$("#mainLoader").show();
		$("#bankSetupTbl, #addUpdateBankSetup").hide();
		$("#bankSetupSavedArea").show();
		
		setTimeout(function(){
			$("#mainLoader").hide();
			$("#bankSetupTbl").slideDown();
		}, 1000);
	}
}


function getAutoInvcPymntTblData(){
	$("#mainLoader").show();
	
	var arguments = "7";
	var d = new Date();
	var n = d.getTime();
	var reportName = "cstm_ach_dtls_" + n + ".dat";

	$.ajax({
		data: {
			pgmPath: execPathCmnFld,
			Args: arguments,
			rptName: reportName
		},
		url: 'callPgmAndGetJSON.jsp',
		type: 'POST',
		dataType: 'text',
		success: function(data){
			console.log(data);
			if (IsJsonString(data.trim())) {
				var arrResponse = JSON.parse(data.trim());
				var max = arrResponse.length;
				var tbody = '', reqSts = "", actnBtn = "";
				for( var i=0; i<max; i++ ){
					reqSts = "", actnBtn = "";
					
					actnBtn = '<i class="icon-menu icon-info display-5 text-primary view-auto-invc-pymnt c-pointer font-weight-bold" title="View"></i>';
					
					if( arrResponse[i].propSts == "P" ){
						reqSts = '<span class="badge badge-warning">Pending</span>';
						actnBtn = '<i class="icon-menu icon-note text-warning edit-auto-invc-pymnt c-pointer font-weight-bold" title="Edit"></i> <span class="badge view-ach-file c-pointer ml-1" title="View ACH File">View ACH</span>';
					}else if( arrResponse[i].propSts == "A" ){
						reqSts = '<span class="badge badge-success">Approved</span>';
					}else if( arrResponse[i].propSts == "PA" ){
						reqSts = '<span class="badge badge-info">Partially Approved</span>';
					}else if( arrResponse[i].propSts == "S" ){
						reqSts = '<span class="badge badge-primary">Sent For Approval</span>';
					}
					
					tbody +='<tr data-prop-id="' + arrResponse[i].propId + '" data-req-id="' + arrResponse[i].reqId + '" data-req-dt="' + arrResponse[i].reqDt + '" data-cmpy-id="' + arrResponse[i].cmpyId + '">'+
								'<td>'+ (i+1) +'</td>'+
								'<td>'+ arrResponse[i].reqId +'</td>'+
								'<td>'+ arrResponse[i].reqDt +'</td>'+
								'<td>'+ arrResponse[i].cmpyId +'</td>'+
								'<td>'+ arrResponse[i].amt +'</td>'+
								'<td>'+ arrResponse[i].crtdBy +'</td>'+
								'<td>'+ reqSts +'</td>'+
								'<td>'+ actnBtn +'</td>'+
							'<\tr>';
				}
				$("#aipCreatedTbl tbody").html(tbody);
				console.log(arrResponse);
			}
			$("#mainLoader").hide();
		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data.trim());
		}
	});
}


function getAprvInvcPymntTblData(){
	$("#mainLoader").show();
	
	var arguments = "13|" + $("#reqStsFltr").val();
	var d = new Date();
	var n = d.getTime();
	var reportName = "cstm_ach_dtls_" + n + ".dat";

	$.ajax({
		data: {
			pgmPath: execPathCmnFld,
			Args: arguments,
			rptName: reportName
		},
		url: 'callPgmAndGetJSON.jsp',
		type: 'POST',
		dataType: 'text',
		success: function(data){
			console.log(data);
			if (IsJsonString(data.trim())) {
				var arrResponse = JSON.parse(data.trim());
				var max = arrResponse.length;
				var tbody = '', reqSts = "";
				for( var i=0; i<max; i++ ){
					reqSts = "";
					
					if( arrResponse[i].propSts == "S" ){
						reqSts = '<span class="badge badge-warning">Pending</span>';
					}else if( arrResponse[i].propSts == "A" ){
						reqSts = '<span class="badge badge-success">Approved</span>';
					}else if( arrResponse[i].propSts == "PA" ){
						reqSts = '<span class="badge badge-info">Partially Approved</span>';
					}
					tbody +='<tr data-prop-id="' + arrResponse[i].propId + '" data-req-id="' + arrResponse[i].reqId + '">'+
								'<td>'+ (i+1) +'</td>'+
								'<td>'+ arrResponse[i].reqId +'</td>'+
								'<td>'+ arrResponse[i].reqDt +'</td>'+
								'<td>'+ arrResponse[i].cmpyId +'</td>'+
								'<td>'+ arrResponse[i].amt +'</td>'+
								'<td>'+ arrResponse[i].crtdBy +'</td>'+
								'<td>'+ reqSts +'</td>'+
								'<td><i class="icon-menu icon-info display-5 text-primary view-auto-invc-pymnt c-pointer"></i></td>'+
							'<\tr>';
				}
				$("#receiveAutoInvcPymntTbl tbody").html(tbody);
			}
			$("#mainLoader").hide();
		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data.trim());
		}
	});
}

/*
 * Validate Condition
 */
function validateCondition(boxData){
	$("#mainLoader").show();
	
	var amtFlg = 0, brhFlg = 0, venFlg = 0, dayFlg = 0, amtFrm = 0, amtTo = 0, days = 0, dayBef = 0, brhVndrSize = 0, costRecon = 0;
	var brh = '', venId = '', errTxt = '', keyNm = '';
	var rtnSts = false;
	
	var wkfId = ( $("#saveWfBtn").attr("data-wkf-id") != undefined ) ? $("#saveWfBtn").attr("data-wkf-id") : "";

	if( boxData.amountRow ){
		amtFlg = 1;
		amtFrm = boxData.amount;
		amtTo = boxData.amountTo;
	}
	
	if( boxData.branchRow ){
		brhFlg = 1;
		brh = boxData.brh.toString();
		brhVndrSize = boxData.brh.length;
	}
	
	if( boxData.vendorRow ){
		venFlg = 1;
		venId = boxData.venId.toString();
		brhVndrSize = boxData.venId.length;
		
		if( boxData.costRecon ){
			costRecon = 1;
		}
	}
	
	if( boxData.daysRow ){
		dayFlg = 1;
		days = boxData.nbrDays;
	}
	
	if( boxData.bfrInvDueDt ){
		dayBef = 1
	}

	var inptJson = {
		wkfId    : wkfId,
		wkfName  : boxData.wkfName,
		crtdBy   : boxData.crtdBy,
		stepName : boxData.stepName,
		amtFlg   : amtFlg,
		brhFlg   : brhFlg,
		venFlg   : venFlg,
		dayFlg   : dayFlg,
		amtFrm   : amtFrm,
		amtTo    : amtTo,
		brh      : brh,
		venId    : venId,
		days     : days,
		dayBef   : dayBef,
		apvr     : boxData.approvalTo,
		brhVndrSize : brhVndrSize,
		costRecon : costRecon,
	};
	
	if( inptJson.amtFlg ){
		errTxt += "<span class='text-primary'>If amount between " + amtFrm + " - " + amtTo + "</span>";
	}
	if( inptJson.brhFlg ){
		errTxt += (errTxt == "") ? '' : ' AND ';
		errTxt += "<span class='text-primary'>If branch in (" + brh + ")</span>";
	}
	if( inptJson.venFlg ){
		errTxt += (errTxt == "") ? '' : ' AND ';
		errTxt += "<span class='text-primary'>If vendor in (" + venId + ") </span>";
		errTxt += (inptJson.costRecon) ? "<span class='text-info'>Cost Reconciliation</span>" : '';
	}
	if( inptJson.dayFlg ){
		errTxt += (errTxt == "") ? '' : ' AND ';
		if( dayBef == 1 ){
			errTxt += "<span class='text-primary'>" + days + " days before invoice due date</span>";
		}else{
			errTxt += "<span class='text-primary'>" + days + " days from the invoice date</span>";
		}
	}

	var d = new Date();
	var n = d.getTime();
	var reportName = "cstm_vldt_wkf_cond_" + n + ".dat";
	var arguments = "VLDT|" + JSON.stringify(inptJson);
	
	$.ajax({
		async: false,
		data: {
			Args: arguments,
			pgmPath: execPathWkf,
			rptName: reportName,
		},
		url: 'callPgmAndGetJSON.jsp',
		type: 'POST',
		dataType: 'text',
		success: function(data){
			if (IsJsonString(data.trim())) {
				var arrResponse = JSON.parse(data.trim());
				if( arrResponse.errArr != undefined && arrResponse.errArr.length > 0 ){
					var table = '', swal_html = ''; 
					
					table = '<table class="table table-border"><thead class="thead-clr"><tr><th>Workflow</th><th>Condition</th></tr></thead><tbody>';
					for( var i=0; i<arrResponse.errArr.length; i++ ){
						var cond = "";
						if( inptJson.amtFlg ){
							cond += "<span class='text-primary'>If amount between " + arrResponse.errArr[i].amtFrm + " - " + arrResponse.errArr[i].amtTo + "</span>";
						}
						if( inptJson.brhFlg ){
							cond += (cond == "") ? '' : ' AND ';
							cond += "<span class='text-primary'>If branch in (" + arrResponse.errArr[i].brh + ")</span>";
						}
						if( inptJson.venFlg ){
							cond += (cond == "") ? '' : ' AND ';
							cond += "<span class='text-primary'>If vendor in (" + arrResponse.errArr[i].venId + ") </span>";
							cond += (arrResponse.errArr[i].costRecon) ? "<span class='text-info'>Cost Reconciliation</span>" : '';
						}
						if( inptJson.dayFlg ){
							cond += (cond == "") ? '' : ' AND ';
							if( arrResponse.errArr[i].dayBef == 1 ){
								cond += "<span class='text-primary'>" + arrResponse.errArr[i].days + " days before invoice due date</span>";
							}else{
								cond += "<span class='text-primary'>" + arrResponse.errArr[i].days + " days from the invoice date</span>";
							}
						}
						
						table += '<tr><td>'+ arrResponse.errArr[i].wkfNm +'</td><td>'+ cond +'</td></tr>';
					}
					table += '</tbody></table>';
					
					
					swal_html = '<div><p>This condition <b>'+ errTxt +'</b> is conflicting with below existing workflows.</p> <div class="table-responsive" style="max-height: 500px; overflow:auto;">'+ table +'</div></div>';
					
					swal({
						type: 'error',
						position: 'top',
						/* title: "Error...", */
						allowOutsideClick: false,
						allowEscapeKey: false,
						html: swal_html
					});
					rtnSts = false;
				}else{
					rtnSts = true;
				}
			}
		},
		error: function(data){
			console.log(data);
			$("#mainLoader").hide();
		}
	});
	
	$("#mainLoader").hide();
	return rtnSts;
}


function getUsrBrhVndrList(usr="", brh="", vndr=""){
	$("#mainLoader").show();
	
	var arguments = "LIST";
	var d = new Date();
	var n = d.getTime();
	var reportName = "cstm_get_ocr_data_" + n + ".dat";

	$.ajax({
		data: {
			pgmPath: execPath,
			Args: arguments,
			rptName: reportName
		},
		url: 'callPgmAndGetJSON.jsp',
		type: 'POST',
		dataType: 'text',
		success: function(data){
			if (IsJsonString(data.trim())) {
				var arrResponse = JSON.parse(data.trim());
				$("#approvalTo, #emailTo").html(arrResponse.usrList);
				$("#brh").html(arrResponse.brhList);
				$("#vndr").html(arrResponse.venList);
				
				if(usr != ""){
					$("#approvalTo").val(usr).trigger('change');
				}
				if(brh != ""){
					if( typeof brh == "string" ){
						$("#brh").val(brh.split(',')).trigger('change');
					}else{
						$("#brh").val(brh).trigger('change');
					}
				}
				if(vndr != ""){
					if( typeof vndr == "string" ){
						$("#vndr").val(vndr.split(',')).trigger('change');
					}else{
						$("#vndr").val(vndr).trigger('change');
					}
				}
				
				$("#mainLoader").hide();
			}
		},
		error: function(data){
			console.log(data);
			$("#mainLoader").hide();
		}
	});
}


function getAdminList(){
	$("#mainLoader").show();
	
	var arguments = "ADML";
	var d = new Date();
	var n = d.getTime();
	var reportName = "cstm_get_ocr_admin_data_" + n + ".dat";

	$.ajax({
		data: {
			pgmPath: execPath,
			Args: arguments,
			rptName: reportName
		},
		url: 'callPgmAndGetJSON.jsp',
		type: 'POST',
		dataType: 'text',
		success: function(data){
			if (IsJsonString(data.trim())) {
				var arrResponse = JSON.parse(data.trim());
				
				$("#admUsr").html( arrResponse.usrList );
				
				var admArr = arrResponse.adminList;
				var max = admArr.length;
				
				var table = '<table class="table table-border">'+
								'<thead class="thead-clr">'+
									'<tr>'+
										'<th width="60px">S. No.</th>'+
										'<th width="90px">Login Id</th>'+
										'<th>Name</th>'+
										'<th width="60px">Delete</th>'+
									'</tr>'+
								'</thead>'+
								'<tbody>';
				for( var i=0; i < max; i++ ){
					var delBtn = '';
					
					if( admArr[i].usrLgnId != $("#loginId").val() ){
						delBtn = '<i class="icon-trash menu-icon icon-pointer delete-adm icon-red"></i>';
					}
							table += '<tr data-lgn-id="' + admArr[i].usrLgnId + '">'+
										'<td>' + (i+1) + '</td>'+
										'<td>' + admArr[i].usrLgnId + '</td>'+
										'<td>' + admArr[i].usrNm + '</td>'+
										'<td class="text-center">' + delBtn + '</td>'+
									'</tr>';
				}
					table += '</tbody>'+
							'</table>';
						
				$("#admListDiv").html(table);
				$("#adminListWrapper").show();
				$("#mainLoader").hide();
			}
		},
		error: function(data){
			console.log(data);
			$("#mainLoader").hide();
		}
	});
}

function addAdmin(lgnId){
	$("#mainLoader").show();
	
	var arguments = "ADMA|" + lgnId;
	var d = new Date();
	var n = d.getTime();
	var reportName = "cstm_ocr_add_admin_data_" + n + ".dat";

	$.ajax({
		data: {
			pgmPath: execPath,
			Args: arguments,
			rptName: reportName
		},
		url: 'callPgmAndGetJSON.jsp',
		type: 'POST',
		dataType: 'text',
		success: function(data){
			if (IsJsonString(data.trim())) {
				var arrResponse = JSON.parse(data.trim());
				
				if( arrResponse.insertSts == 1 ){
					getAdminList();
					swal({
						text: "Admin added successfully.",
						type: 'success',
					});
				}else{
					swal({
						text: "Something went wrong, please try again later.",
						type: 'error',
					});
				}
				
				$("#mainLoader").hide();
			}
		},
		error: function(data){
			console.log(data);
			$("#mainLoader").hide();
		}
	});
}

function deleteAdmin(lgnId){
	$("#mainLoader").show();
	
	var arguments = "ADMD|" + lgnId;
	var d = new Date();
	var n = d.getTime();
	var reportName = "cstm_ocr_delete_admin_data_" + n + ".dat";

	$.ajax({
		data: {
			pgmPath: execPath,
			Args: arguments,
			rptName: reportName
		},
		url: 'callPgmAndGetJSON.jsp',
		type: 'POST',
		dataType: 'text',
		success: function(data){
			if (IsJsonString(data.trim())) {
				var arrResponse = JSON.parse(data.trim());
				
				if( arrResponse.delSts == 1 ){
					getAdminList();
					swal({
						text: "Admin deleted successfully.",
						type: 'success',
					});
				}else{
					swal({
						text: "Something went wrong, please try again later.",
						type: 'error',
					});
				}
				
				$("#mainLoader").hide();
			}
		},
		error: function(data){
			console.log(data);
			$("#mainLoader").hide();
		}
	});
}

function getStratixEntries()
{
	$("#mainLoader").show();
	
	var cmpyId = ( $("#aipCmpyId").val() != undefined && $("#aipCmpyId").val().trim() != "" ) ? $("#aipCmpyId").val().trim() : "";
	var invDt = ( $("#aipInvcDt").val() != undefined && $("#aipInvcDt").val().trim() != "" ) ? $("#aipInvcDt").val().trim() : "";
	var venIdFrm = ( $("#aipVndrIdFrm").val() != undefined && $("#aipVndrIdFrm").val().trim() != "" ) ? $("#aipVndrIdFrm").val().trim() : "";
	var venIdTo = ( $("#aipVndrIdTo").val() != undefined && $("#aipVndrIdTo").val().trim() != "" ) ? $("#aipVndrIdTo").val().trim() : "";
	
	var inptObj = {
		"cmpyId": cmpyId, 
		"invDt": mmddyyyyToDbFormat( invDt ),
		"venIdFrm": venIdFrm,
		"venIdTo": venIdTo
	}
	var arguments = "10|" + JSON.stringify( inptObj );
	var d = new Date();
	var n = d.getTime();
	var reportName = "cstm_get_ach_data_" + n + ".dat";

	$.ajax({
		data: {
			pgmPath: execPathCmnFld,
			Args: arguments,
			rptName: reportName
		},
		url: 'callPgmAndGetJSON.jsp',
		type: 'POST',
		dataType: 'text',
		success: function(data){
			if (IsJsonString(data.trim())) {
				var arrResponse = JSON.parse(data.trim());
				var recordCount = arrResponse.pendingList.length;
				var poStr = "", logStatus = "", checkboxStr = "";
				var tbl = '<table class="table table-hover">'+
							'<thead>'+
								'<tr class="thead-clr">'+
									'<th><div class="form-check form-check-flat m-0">'+
									'<label class="form-check-label">'+
									  '<input type="checkbox" class="form-check-input" id="selectAllInvc" checked>'+
									'<i class="input-helper"></i></label>'+
								 '</div></th>'+
									'<th>Status</th>'+
									'<th>Vendor ID</th>'+
									'<th>Invoice No.</th>'+
									'<th>Invoice Date</th>'+
									'<th>PO</th>'+
									'<th>Voucher No.</th>'+
									'<th>Branch</th>'+
									'<th class="text-right">Amount</th>'+
									'<th>Entered Date</th>'+
								'</tr>'+
							'</thead>'+
							'<tbody>';
				
				for( var i=0; i<recordCount; i++ ){
					logStatus = '<i class="icon-check text-success font-weight-bold display-5"></i>';
					checkboxStr = '<div class="form-check form-check-flat m-0">'+
									'<label class="form-check-label">'+
									  '<input type="checkbox" class="form-check-input" name="select-invoices" checked>'+
									'<i class="input-helper"></i></label>'+
								 '</div>';
					if( arrResponse.pendingList[i].poNo == 0 ){
						poStr = "";
					}else{
						poStr = arrResponse.pendingList[i].poPfx +'-'+ arrResponse.pendingList[i].poNo +'-'+ arrResponse.pendingList[i].poItm;
					}
					
					if( arrResponse.pendingList[i].logSts == "N" ){
						logStatus = '<i class="icon-ban c-pointer text-danger font-weight-bold display-5" title="Vendor bank information not available." data-toggle="tooltip"></i>';
						checkboxStr = '';
					}
					
					//console.log(poStr);
					tbl += '<tr data-invoice-no="'+ arrResponse.pendingList[i].vendInvNo +'" data-vchr-no="'+ arrResponse.pendingList[i].opaPfx +'-'+ arrResponse.pendingList[i].opaNo +'">'+
								'<td>'+ checkboxStr +'</td>'+
								'<td>'+ logStatus +'</td>'+
								'<td>'+ arrResponse.pendingList[i].vendId +'</td>'+
								'<td>'+ arrResponse.pendingList[i].vendInvNo +'</td>'+
								'<td>'+ arrResponse.pendingList[i].vendInvDt +'</td>'+
								'<td>'+ poStr +'</td>'+
								'<td>'+ arrResponse.pendingList[i].opaPfx +'-'+ arrResponse.pendingList[i].opaNo +'</td>'+
								'<td>'+ arrResponse.pendingList[i].apBrh +'</td>'+
								'<td class="text-right">'+ arrResponse.pendingList[i].amount +' '+ arrResponse.pendingList[i].cry +'</td>'+
								'<td>'+ arrResponse.pendingList[i].entDt +'</td>'+
							'</tr>';
				}
				tbl += '</tbody></table>';
				$("#stxEntryContent").html(tbl);
				
				if( recordCount ){
					$("#invcDrpActns").show();
				}else{
					$("#invcDrpActns").hide();
				}
				
				
				/**
				  * Getting Hold Invoices 
				  */
				recordCount = arrResponse.holdList.length;
				poStr = "", logStatus = "";
				tbl = '<table class="table table-hover">'+
							'<thead>'+
								'<tr>'+
									'<th>Vendor ID</th>'+
									'<th>Invoice No.</th>'+
									'<th>Invoice Date</th>'+
									'<th>PO</th>'+
									'<th>Voucher No.</th>'+
									'<th>Branch</th>'+
									'<th class="text-right">Amount</th>'+
									'<th>Entered Date</th>'+
								'</tr>'+
							'</thead>'+
							'<tbody>';
				
				for( var i=0; i<recordCount; i++ ){
					
					if( arrResponse.holdList[i].poNo == 0 ){
						poStr = "";
					}else{
						poStr = arrResponse.holdList[i].poPfx +'-'+ arrResponse.holdList[i].poNo +'-'+ arrResponse.holdList[i].poItm;
					}
					
					tbl += '<tr>'+
								'<td>'+ arrResponse.holdList[i].vendId +'</td>'+
								'<td>'+ arrResponse.holdList[i].vendInvNo +'</td>'+
								'<td>'+ arrResponse.holdList[i].vendInvDt +'</td>'+
								'<td>'+ poStr +'</td>'+
								'<td>'+ arrResponse.holdList[i].opaPfx +'-'+ arrResponse.holdList[i].opaNo +'</td>'+
								'<td>'+ arrResponse.holdList[i].apBrh +'</td>'+
								'<td class="text-right">'+ arrResponse.holdList[i].amount +' '+ arrResponse.holdList[i].cry +'</td>'+
								'<td>'+ arrResponse.holdList[i].entDt +'</td>'+
							'</tr>';
				}
				tbl += '</tbody></table>';
				$("#blockedVoucherTblWrapper").html(tbl);
				$("#filterVoucherResults").slideDown(1000);
				$("body").tooltip({ selector: '[data-toggle=tooltip]', placement: 'top' });
				
				if( recordCount ){
					$("#holdInvcWrapper").show();
				}else{
					$("#holdInvcWrapper").hide();
				}
				
				$("#mainLoader").hide();
			}
		},
		error: function(data){
			console.log(data);
		}
	});
}



function getCmpyList(cardId)
{
	$("#mainLoader").show();
	
	var arguments = "1";
	var d = new Date();
	var n = d.getTime();
	var reportName = "cstm_ach_dtls_" + n + ".dat";

	$.ajax({
		data: {
			pgmPath: execPathCmnFld,
			Args: arguments,
			rptName: reportName
		},
		url: 'callPgmAndGetJSON.jsp',
		type: 'POST',
		dataType: 'text',
		success: function(data){
			if (IsJsonString(data.trim())) {
				var arrResponse = JSON.parse(data.trim());
				$("#cmpyId, #venCmpyId, #achCmpyId, #aipCmpyId").html( arrResponse.companyList );
				
				$("#mainLoader").hide();
				
				if(cardId == "vendor"){
					$("#venCmpyId").trigger("change");
				} else if(cardId == "createAchModal"){
					$("#achCmpyId").trigger("change");
				} else if(cardId == "autoInvcPymnt"){
					$("#aipCmpyId").trigger("change");
				}
			}
		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data.trim());
		}
	});
}

function getVendorList(cmpyId, getVndrData=false){
	$("#mainLoader").show();

	var arguments = "2|"+cmpyId;
	var d = new Date();
	var n = d.getTime();
	var reportName = "cstm_ach_dtls_" + n + ".dat";

	$.ajax({
		data: {
			pgmPath: execPathCmnFld,
			Args: arguments,
			rptName: reportName
		},
		url: 'callPgmAndGetJSON.jsp',
		type: 'POST',
		dataType: 'text',
		success: function(data){
			if (IsJsonString(data.trim())) {
				var arrResponse = JSON.parse(data.trim());
				$("#achVenId, #aipVenI").html( arrResponse.vendorList );
				$("#aipVndrIdFrm, #aipVndrIdTo, #transVndrNm").html( "<option value=''>&nbsp;&nbsp;&nbsp;&nbsp;</option>" + arrResponse.vendorList );
				if( getVndrData==true ){
					$("#achVenId").trigger("change");
				}
				
				$("#mainLoader").hide();
			}
		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data.trim());
		}
	});
}



function getVndrAchTblData(){
	$("#mainLoader").show();
	
	var arguments = "5";
	var d = new Date();
	var n = d.getTime();
	var reportName = "cstm_cmmn_flds_" + n + ".dat";

	$.ajax({
		data: {
			pgmPath: execPathCmnFld,
			Args: arguments,
			rptName: reportName
		},
		url: 'callPgmAndGetJSON.jsp',
		type: 'POST',
		dataType: 'text',
		success: function(data){
			console.log(data);
			if (IsJsonString(data.trim())) {
				var arrResponse = JSON.parse(data.trim());
				var max = arrResponse.length;
				var tbody = '';
				for( var i=0; i<max; i++ ){
					tbody +='<tr>'+
								'<td>'+ (i+1) +'</td>'+
								'<td>'+ arrResponse[i].cmpyId +'</td>'+
								'<td>'+ arrResponse[i].venId +'</td>'+
								'<td>'+ arrResponse[i].bnkNm +'</td>'+
								'<td>'+ arrResponse[i].venBnkAcctNo +'</td>'+
								'<td>'+ arrResponse[i].bnkKey +'</td>'+
								'<td>'+ arrResponse[i].extRef +'</td>'+
								'<td>'+ arrResponse[i].crtdBy +'</td>'+
								'<td>'+ arrResponse[i].crtdDt +'</td>'+
								//'<td><i title="Edit" class="icon-note text-primary mr-1 font-weight-bold"></i><i title="Delete" class="icon-trash text-danger font-weight-bold"></i></td>'+
							'<\tr>';
				}
				$("#vndrAchTbl tbody").html(tbody);
				console.log(arrResponse);
			}
			$("#mainLoader").hide();
		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data.trim());
		}
	});
}

function getOcrHistory()
{
	$("#mainLoader").show();
	
	var arguments = "RC|-";
	var d = new Date();
	var n = d.getTime();
	var reportName = "cstm_get_ocr_data"+n+".dat";

	$.ajax({
		data: {
			pgmPath: "/u/starsoft/bin/cstm_get_ocr_data",
			Args: arguments,
			rptName: reportName
		},
		url: 'callPgmAndGetJSON.jsp',
		type: 'POST',
		dataType: 'text',
		success: function(data){
			if (IsJsonString(data.trim())) {
				var arrResponse = JSON.parse(data.trim());
				
				//console.log(arrResponse);
				var recordCount = arrResponse.length;
				var fileName , fileExt, fileType, infoTd, statusTd, voucherNo, source = '';
				var tbl = '<table class="table table-hover">'+
							'<thead>'+
								'<tr class="thead-clr">'+
									'<th width="70">File <i class="fa fa-filter icon-equalizer filterData" data-placement="top" aria-hidden="true" data-toggle="tooltip" title="Filter"></i></th>'+
									'<th>Invoice No</th>'+
									'<th width="200px">Vendor</th>'+
									'<th width="100px">Ext Ref</th>'+
									'<th>BRH</th>'+
									'<th>Amount</th>'+
									'<th>CRY</th>'+
									'<th>INV Date</th>'+
									'<th>ENT Date</th>'+
									'<th width="165px">Created On</th>'+
									'<th width="165px">Processed On</th>'+
									'<th>Voucher</th>'+
									'<th>Source</th>'+
									'<th width="110px">Status/Action</th>'+
									
								'</tr>'+
								'<tr class="filter-action-row hide-filter">'+
								  '<th></th>'+
								  '<th><input class="custom-text-filter form-control fieldsFilter" type="text" value=""></th>'+
								  '<th><input class="custom-text-filter form-control vendorFilter" type="text" value=""></th>'+
								  '<th></th>'+
								  '<th></th>'+
								  '<th></th>'+
								  '<th></th>'+
								  '<th></th>'+
								  '<th></th>'+
								  '<th><input type="text" name="daterange" class="form-control createdOnFilter"/></th>'+
								  '<th><input type="text" name="daterange" class="form-control processedOnFilter" /></th>'+
								  '<th><input class="custom-text-filter form-control voucherFilter" type="text" value=""></th>'+
								  '<th></th>'+
								  '<th>'+
									/* '<select class="custom-select-filter form-control statusFilter">'+
									  '<option value="">All</option>'+
									  '<option value="Pending">Pending</option>'+
									  '<option value="Completed">Completed</option>'+
									'</select>'+ */
								  '</th>'+
								'</tr>'+
							'</thead>'+
							'<tbody>';
				for( var i=0; i<recordCount; i++ ){
					fileName = arrResponse[i].flName;
					fileExt = fileName.split(".").pop();
					fileType = (fileExt == "pdf") ? "pdf" : "image";
					
					if( arrResponse[i].info != undefined ){
						
						infoTd = '<td>'+ arrResponse[i].info.invNo +'</td>' + '<td>'+ arrResponse[i].info.venNm +'</td>'+'<td>'+ arrResponse[i].info.extlref +'</td>' +
									'<td>'+ arrResponse[i].info.apBrh +'</td>'+
									'<td>'+ arrResponse[i].info.amt +'</td>'+
									'<td>'+ arrResponse[i].info.cry +'</td>'+
									'<td>'+ arrResponse[i].info.invDt +'</td>'+
									'<td>'+ arrResponse[i].info.entDt +'</td>';
						voucherNo = arrResponse[i].info.voucher;
					}else{
						voucherNo = '';
						infoTd = '<td></td><td></td><td></td><td></td><td></td><td></td><td></td>' ;
					}
					
					source = "Email";
					if( arrResponse[i].srcFlg.toLowerCase() == "s" ){
						source = "System";
					}
					
					if( arrResponse[i].isPrs == true ){
						statusTd = '<span ><i class="icon-check menu-icon text-success" data-toggle="tooltip" aria-hidden="true" title="Completed"></i></span><div class="d-none">Completed</div>';
					}else{
						statusTd = '<span ><i class="icon-clock menu-icon" data-toggle="tooltip" title="Pending"></i></span><div class="d-none">Pending</div>';
					}
					
					tbl += '<tr class="main-row">'+
								'<td><i class="icon-eye menu-icon view-icon" aria-hidden="true" alt="File" data-toggle="tooltip" title ="View - '+ arrResponse[i].flName+ '" data-ctlNo="'+ arrResponse[i].ctlNo + '" data-flNm="'+ arrResponse[i].flName+ '" data-file-type="'+ fileType + '"></i>&nbsp;&nbsp;'+ '<i class="icon-cloud-download menu-icon download-icon" aria-hidden="true" alt="File" data-toggle="tooltip" title="Download - '+ arrResponse[i].flName+ '" data-ctlNo="'+ arrResponse[i].ctlNo + '" data-flNm="'+ arrResponse[i].flName+ '" data-file-type="'+ fileType + '"></i>' +
								'</td>'+
								 infoTd +
								'<td>'+ arrResponse[i].crtdDt +'</td>'+
								'<td>'+ arrResponse[i].prsDt +'</td>'+
								'<td><span class="voucher-no">'+ voucherNo +'</span></td>'+
								'<td>'+ source +'</td>'+
								'<td class="td-center">'+ statusTd +'</td>'+
							'</tr>';
				}
				tbl += '</tbody></table>';
				$("#historyContent").hide();
				$("#historyContent").html(tbl);
				$("#historyContent").slideDown(1300);
				$("#compInvoice").html(recordCount);
				$("#mainLoader").hide();
				
				$("body").tooltip({ selector: '[data-toggle=tooltip]', placement: 'top' });
				
				$('body').on('focus',"input[name='daterange']", function(){
					$('input[name="daterange"]').daterangepicker({
						autoUpdateInput: false,
						opens: 'left',
						locale: {
							format: 'YYYY/MM/DD'
						}
					}, function(start, end, label) {
						console.log("A new date selection was made: " + start.format('YYYY/MM/DD') + ' to ' + end.format('YYYY/MM/DD'));
					});
					
					$('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
						$(this).val(picker.startDate.format('YYYY/MM/DD') + ' - ' + picker.endDate.format('YYYY/MM/DD'));
						$(this).trigger("change");
					});

					  $('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
						  $(this).val('');
						  $(this).trigger("change");
					});

				});
			}
		},
		error: function(data){
			console.log(data);
		}
	});
}


function approveInvoice(arguments, venId="", invcNo=""){
	$("#mainLoader").show();
	
	//var arguments = "P|"+ctlNo;
	var d = new Date();
	var n = d.getTime();
	var reportName = "cstm_get_ocr_data"+n+".dat";

	$.ajax({
		data: {
			pgmPath: "/u/starsoft/bin/cstm_get_ocr_data",
			Args: arguments,
			rptName: reportName
		},
		url: 'callPgmAndGetJSON.jsp',
		type: 'POST',
		dataType: 'text',
		success: function(data){
			if (IsJsonString(data.trim())) {
				var arrResponse = JSON.parse(data.trim());
				if( arrResponse.appr_sts == "Y" ){
					bootbox.alert('Voucher VR-'+arrResponse.voucher+' has been successfully created in STRATIX.');
					getRetrieve();
					$("#editDataModal").modal("hide");
				}else{
					
					if( arrResponse.msg != undefined && arrResponse.msg == "INV-VEN-EXST" ){
						bootbox.alert('Voucher <b class="text-primary">(VR-'+arrResponse.vchrNo+')</b> has already created for <b>Vendor ('+venId+')</b> with Invoice number <b>('+invcNo+')</b> in STRATIX.');
					}else{
						bootbox.alert('Error while creating Voucher in STRATIX, please verify data and try again.');
					}
					
					$("#mainLoader").hide();
				}
			}
		},
		error: function(data){
			console.log(data);
		}
	});
}

function pendInvcNo(){
	$("#mainLoader").show();
	
	var d = new Date();
	var n = d.getTime();
	var reportName = "cstm_get_ocr_data"+n+".dat";

	$.ajax({
		data: {
			pgmPath: execPath,
			Args: "CR|"+ $("#loginId").val(),
			rptName: reportName
		},
		url: 'callPgmAndGetJSON.jsp',
		type: 'POST',
		dataType: 'text',
		success: function(data){
			if (IsJsonString(data.trim())) {
				var arrResponse = JSON.parse(data.trim());
				$("#pendingInvoice").html( arrResponse.unPrsRcdCount );
				$("#loggedInUser").html( arrResponse.usrNm );
				$("#usrRole").val( arrResponse.usrRole );
				
				if( arrResponse.usrRole != "ADM" ){
					$("#dashboardNav, #newDocNav, #workflowNav, #adminListBtn").remove();
					if( initialCall ){
						$("#retrieveAnchor").trigger("click");
						initialCall = false;
					}
				}
			}
			$("#mainLoader").hide();
		},
		error: function(data){
			console.log(data);
			$("#mainLoader").hide();
		}
	});
}



function getWorkFlows()
{
	$("#mainLoader").show();
	
	$("#wfAddWrapper").hide();
	$("#wfGridWrapper").show();
	
	var arguments = "WKF|"+ $("#loginId").val();
	var d = new Date();
	var n = d.getTime();
	var reportName = "cstm_get_wkf_" + n + ".dat";

	$.ajax({
		data: {
			pgmPath: execPathWkf,
			Args: arguments,
			rptName: reportName
		},
		url: 'callPgmAndGetJSON.jsp',
		type: 'POST',
		dataType: 'text',
		success: function(data){
			if (IsJsonString(data.trim())) {

				var arrResponse = JSON.parse(data.trim());
				
				//console.log(arrResponse);
				var workFlows = arrResponse.workFlows;
				var recordCount = workFlows.length;
				
				var dltBtn = '';

				var tbl = '<table class="table table-hover table-border">'+
							'<thead class="thead-clr">'+
								'<tr>'+
									'<th width="80px">S No</th>'+
									'<th>Name</th>'+
									'<th># of Assign Invoices</th>'+
									'<th>Condition</th>'+
									'<th>Created By</th>'+
									'<th>Created On</th>'+
									'<th>Action</th>'+
								'</tr>'+
							'</thead>'+
						  '<tbody>';

				for( var i=0; i<recordCount; i++ ){
					
					dltBtn = (workFlows[i].pndgDoc) ? '' : '<button class="btn btn-xs btn-danger delete-wf">Delete</button>';
					
					var cndtn = "";
					if( workFlows[i].amtFlg == 1 ){
						cndtn += (cndtn == "") ? '' : ' AND ';
						cndtn += "<span class='text-primary'>If amount between " + workFlows[i].amtFrm + " - " + workFlows[i].amtTo + "</span>";
					}
					if( workFlows[i].brhFlg == 1 ){
						cndtn += (cndtn == "") ? '' : ' AND ';
						cndtn += "<span class='text-primary'>If branch in (" + workFlows[i].brh + ")</span>";
					}
					if( workFlows[i].venFlg == 1 ){
						cndtn += (cndtn == "") ? '' : ' AND ';
						cndtn += "<span class='text-primary'>If vendor in (" + workFlows[i].venId + ") </span>";
						cndtn += (workFlows[i].costRecon == 1) ? " <span class='text-info'>(Cost Reconciliation)</span>" : '';
					}
					
					if( workFlows[i].dayFlg == 1 ){
						cndtn += (cndtn == "") ? '' : ' AND ';
						if( workFlows[i].dayBef == 1 ){
							cndtn = "<span class='text-primary'>" + workFlows[i].days + " days before invoice due date</span>";
						}else{
							cndtn = "<span class='text-primary'>" + workFlows[i].days + " days from the invoice date</span>";
						}
					}
					
					tbl += '<tr class="" data-wkf-id="' + workFlows[i].wkfId + '">'+
								'<td>'+ (i+1) +'</td>'+
								'<td>'+ workFlows[i].wkfName +'</td>'+
								'<td>'+ workFlows[i].pndgDoc +'</td>'+
								'<td>'+ cndtn +'</td>'+
								'<td>'+ workFlows[i].crtdBy +'</td>'+
								'<td>'+ workFlows[i].crtdDtts +'</td>'+
								'<td>'+
									'<button class="btn btn-xs btn-primary mr-2 edit-wf" data-pndg-doc="'+ workFlows[i].pndgDoc +'">Edit</button>'+	dltBtn +
								'</td>'+
							'</tr>'; 
				}
				
				if(recordCount == 0)
				{
					tbl += '<tr class=""><td colspan="5" class="tbl-no-records">No Records Found</td></tr>';
				}
				
				tbl += '</tbody></table>';
				
				$("#savedWfContent").hide();
				$("#savedWfContent").html(tbl);
				
				$("#mainLoader").hide();
				
				$("#savedWfContent").slideDown(1300);
			}
		},
		error: function(data){
			$("#mainLoader").hide();
			console.log(data);
		}
	});
}

function getFavourite(title, startNo)
{
	$("#mainLoader").show();
	
	var arguments = "RP|"+ $("#loginId").val();
	var d = new Date();
	var n = d.getTime();
	var reportName = "cstm_get_ocr_data"+n+".dat";
	
	$("#favourite .card-title").html(title);

	$.ajax({
		data: {
			pgmPath: execPath,
			Args: arguments,
			rptName: reportName
		},
		url: 'callPgmAndGetJSON.jsp',
		type: 'POST',
		dataType: 'text',
		success: function(data){
			if (IsJsonString(data.trim())) {
				var arrResponse = JSON.parse(data.trim());
				
				//console.log(arrResponse);
				var recordCount = arrResponse.length;
				var fileName, fileExt, fileType, infoTd, statusTd, editBtn, approveBtn, voucherNo, invcNo = '';
				var tbl = '<table class="table table-hover">'+
							'<thead>'+
								'<tr>'+
									'<th width="90">File <i class="fa fa-filter icon-equalizer filterData" data-placement="top" aria-hidden="true" data-toggle="tooltip" title="Filter"></i></th>'+
									'<th>File Name</th>'+
									'<th>Invoice No</th>'+
									'<th>Voucher No.</th>'+
								'</tr>'+
								'<tr class="filter-action-row hide-filter">'+
								  '<th></th>'+
								  '<th><input class="custom-text-filter form-control fileFilter" type="text" value=""></th>'+
								  '<th><input class="custom-text-filter form-control invcFilter" type="text" value=""></th>'+
								  '<th><input class="custom-text-filter form-control voucherFilter" type="text" value=""></th>'+
								'</tr>'+
							'</thead>'+
							'<tbody>';
				for( var i=0; i<recordCount; i++ ){
					
					if( i < startNo ){
						continue;
					}
					
					fileName = arrResponse[i].flName;
					fileExt = fileName.split(".").pop();
					fileType = (fileExt == "pdf") ? "pdf" : "image";
					
					if( arrResponse[i].info != undefined ){
						invcNo = arrResponse[i].info.invNo;
						
						/*infoTd = '<td>'+ arrResponse[i].info.invNo +'</td>' + '<td>'+ arrResponse[i].info.extlref +'</td>'+
									'<td>'+ arrResponse[i].info.apBrh +'</td>'+
									'<td>'+ arrResponse[i].info.amt +'</td>'+
									'<td>'+ arrResponse[i].info.cry +'</td>'+
									'<td>'+ arrResponse[i].info.invDt +'</td>'+
									'<td>'+ arrResponse[i].info.entDt +'</td>';*/
						voucherNo = arrResponse[i].info.voucher;
						/*editBtn = '<span class="edit-row" data-file-type="'+ fileType +'" data-invNo="'+ arrResponse[i].info.invNo +'" data-extlref="'+ arrResponse[i].info.extlref +'" data-apBrh="'+ arrResponse[i].info.apBrh +'" data-amt="'+ arrResponse[i].info.amt +'" data-cry="'+ arrResponse[i].info.cry +'" data-invDt="'+ arrResponse[i].info.invDt +'" data-entDt="'+ arrResponse[i].info.entDt +'" data-ctlNo="'+ arrResponse[i].ctlNo + '" data-flNm="'+ arrResponse[i].flName +'"><i class="icon-note menu-icon" aria-hidden="true" title="Edit" data-toggle="tooltip"></i></span>';
						
						approveBtn = '<button type="button" class="btn btn-success approve" data-ctlNo="'+ arrResponse[i].ctlNo + '">Approve</button>';*/
					}else{
						/*infoTd = '<td></td><td></td><td></td><td></td><td></td><td></td><td></td>' ;
						editBtn = '';
						approveBtn = '';*/
						voucherNo, invcNo = '';
					}
					
					/*if( arrResponse[i].isPrs == true ){
						statusTd = '<span ><i class="icon-check menu-icon text-success" data-toggle="tooltip" aria-hidden="true" title="Completed"></i></span><div class="d-none">Completed</div>';
					}else{
						statusTd = '<span ><i class="icon-clock menu-icon" data-toggle="tooltip" title="Pending"></i></span> &nbsp;&nbsp;' + editBtn + ' &nbsp;&nbsp; '+ approveBtn +'<div class="d-none">Pending</div>';
					}*/
					
					tbl += '<tr class="main-row">'+
								'<td><i class="icon-heart menu-icon text-warning" data-toggle="tooltip" title="Favourite"></i>&nbsp;&nbsp;<i class="icon-eye menu-icon view-icon" aria-hidden="true" alt="File" data-toggle="tooltip" title ="View - '+ arrResponse[i].flName+ '" data-ctlNo="'+ arrResponse[i].ctlNo + '" data-flNm="'+ arrResponse[i].flName+ '" data-file-type="'+ fileType + '"></i>&nbsp;&nbsp;'+ '<i class="icon-cloud-download menu-icon download-icon" aria-hidden="true" alt="File" data-toggle="tooltip" title="Download - '+ arrResponse[i].flName+ '" data-ctlNo="'+ arrResponse[i].ctlNo + '" data-flNm="'+ arrResponse[i].flName+ '" data-file-type="'+ fileType + '"></i>' +
								'</td>'+
								'<td>'+ arrResponse[i].flName +'</td>'+
								'<td>'+ invcNo +'</td>'+
								'<td><span class="voucher-no">'+ voucherNo +'</span></td>'+
								/*'<td>'+ statusTd +'</td>'+*/
							'</tr>';
				}
				tbl += '</tbody></table>';
				$("#favouriteContent").hide();
				$("#favouriteContent").html(tbl);
				
				if( title.toLowerCase() == "rejected" ){
					$("#favouriteContent table tr td:last-child, #favouriteContent table tr th:last-child").hide();
				}
				
				$("#mainLoader").hide();
				
				$("body").tooltip({ selector: '[data-toggle=tooltip]', placement: 'top' });
				
				$('body').on('focus',"input[name='daterange']", function(){
					$('input[name="daterange"]').daterangepicker({
						autoUpdateInput: false,
						opens: 'left',
						locale: {
							format: 'YYYY/MM/DD'
						}
					}, function(start, end, label) {
						console.log("A new date selection was made: " + start.format('YYYY/MM/DD') + ' to ' + end.format('YYYY/MM/DD'));
					});
					
					$('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
						$(this).val(picker.startDate.format('YYYY/MM/DD') + ' - ' + picker.endDate.format('YYYY/MM/DD'));
						$(this).trigger("change");
					});

					$('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
						$(this).val('');
						$(this).trigger("change");
					});

				});
				
				$("#favouriteContent select.statusFilter").val("Pending");
				$("#favouriteContent select.statusFilter").trigger("change");
				$("#favouriteContent").slideDown(1300);
			}
		},
		error: function(data){
			console.log(data);
		}
	});
}



function fileSavetoDB(fileNm)
{
	$("#mainLoader").show();
	
	var arguments = "I|" + fileNm + "|" + $("#loginId").val();
	var d = new Date();
	var n = d.getTime();
	var reportName = "cstm_get_ocr_data"+n+".dat";
	var tbleOut = "";
	var txtMsg = "";
	var iSuccessFlg = 0 ;
		
 	$.ajax({
		data: {
			pgmPath: execPath,
			Args: arguments,
			rptName: reportName
		},
		url: 'callPgmAndGetJSON.jsp',
		type: 'POST',
		dataType: 'text',
		success: function(data)
		{
			$("#mainLoader").hide();
			//console.log(data);
			if (IsJsonString(data.trim())) 
			{
				var arrResponse = JSON.parse(data.trim());
				
				tbleOut = '<table class="table table-border"><thead><tr class="thead-clr"><th>File Name</th><th>Upload Status</th></tr></thead><tbody>';
				
				for(i = 0; i < arrResponse.length; i++)
				{
					tbleOut += '<tr><td>' + arrResponse[i].fl_nm + '</td>';
						
					if(arrResponse[i].ins_sts == 0)
					{
						tbleOut += '<td><i class="icon-check menu-icon text-success"></i></td>';
					}
					else
					{
						tbleOut += '<td><i class="icon-close menu-icon icon-pointer reject text-danger"></i></td>';
						iSuccessFlg = 1;
					}
						
					tbleOut += '</tr></tbody>';
				}
				
				if(iSuccessFlg == 0)
				{
					if(arrResponse.length == 1)
					{
						txtMsg = "File has been uploaded successfully";
					}
					else
					{
						txtMsg = "All the files have been uploaded successfully";
					}
					
					tbleOut = "<div class='text-success'>" + txtMsg +"</div><hr>" + tbleOut;
				}
				else
				{
					tbleOut = "<div class='text-danger'>Some of the files have been uploaded successfully</div><hr>" + tbleOut;
				}
				
				tbleOut += "</table>";
				
				$('#docPreview, #preview-img img').attr('src', "");
				$('#preview-iframe, #preview-img').slideUp();
				$('#doc-thmbnl').html("");
				pendInvcNo();
				
				bootbox.alert(tbleOut);
			}
		},
		error: function(data)
		{
			console.log(data.trim());
			$("#mainLoader").hide();
		}
	}); 
}

/**
 * Reject Invoice Function
 
function rejectInvoice(ctlNo){
	
	$("#mainLoader").show();
	
	var arguments = "R|" + ctlNo;
	var d = new Date();
	var n = d.getTime();
	var reportName = "cstm_OCR_reject_invc" + n + ".dat";
	
	$.ajax({
		data: {
			pgmPath: execPath,
			Args: arguments,
			rptName: reportName
		},
		url: 'callPgmAndGetJSON.jsp',
		type: 'POST',
		dataType: 'text',
		success: function(data){
			//console.log(data);
			if (IsJsonString(data.trim())) {
				var arrResponse = JSON.parse(data.trim());
				
				if( arrResponse.rtnSts == 0 ){
					Swal.fire({
						title: 'Invoice rejected successfully.',
						width: 400,
						padding: '1em 3em',
						timer: 5000,
					});
					
					getRetrieve();
				}else if ( arrResponse.rtnSts == 1 ){
					Swal.fire({
						title: 'Something went wrong, please try again.',
						width: 400,
						padding: '1em 3em',
						timer: 5000,
						type: 'danger',
					});
				}
			}
			$("#editDataModal").modal("hide");
		},
		error: function(data){
			console.log(data.trim());
			$("#mainLoader").hide();
		}
	});
}**/


/**
 * Reject/Delete Invoice Function
 **/
function rejectDeleteInvoice(reqType, ctlNo){
	
	$("#mainLoader").show();
	
	var arguments = reqType + "|" + ctlNo;
	var d = new Date();
	var n = d.getTime();
	var reportName = "cstm_OCR_reject_invc" + n + ".dat";
	
	var action = "";
	if( reqType == "R" ){
		action = "rejected";
	}else if( reqType == "DEL" ){
		action = "deleted";
	}
	
	$.ajax({
		data: {
			pgmPath: execPath,
			Args: arguments,
			rptName: reportName
		},
		url: 'callPgmAndGetJSON.jsp',
		type: 'POST',
		dataType: 'text',
		success: function(data){
			//console.log(data);
			if (IsJsonString(data.trim())) {
				var arrResponse = JSON.parse(data.trim());
				
				if( arrResponse.rtnSts == 0 ){
					Swal.fire({
						title: 'Invoice '+ action +' successfully.',
						width: 400,
						padding: '1em 3em',
						timer: 5000,
					});
					
					getRetrieve();
				}else if ( arrResponse.rtnSts == 1 ){
					Swal.fire({
						title: 'Something went wrong, please try again.',
						width: 400,
						padding: '1em 3em',
						timer: 5000,
						type: 'danger',
					});
				}
				$("#editDataModal").modal("hide");
			}
		},
		error: function(data){
			console.log(data.trim());
			$("#mainLoader").hide();
		}
	});
}

/**
 * Delete Workflow Function
 **/
function deleteWorkFlow(wkfId){
	
	$("#mainLoader").show();
	
	var arguments = "DEL|" + wkfId + "|" + $("#loginId").val();
	var d = new Date();
	var n = d.getTime();
	var reportName = "cstm_del_wkf_" + n + ".dat";
	
	$.ajax({
		data: {
			pgmPath: execPathWkf,
			Args: arguments,
			rptName: reportName
		},
		url: 'callPgmAndGetJSON.jsp',
		type: 'POST',
		dataType: 'text',
		success: function(data){
			//console.log(data);
			if (IsJsonString(data.trim())) {
				var arrResponse = JSON.parse(data.trim());
				
				if( arrResponse.delSts == 1 ){
					Swal.fire({
						title: 'Workflow deleted successfully.',
						width: 400,
						padding: '1em 3em',
						timer: 5000,
						type: 'success',
					});
					
					getWorkFlows();
				}else if ( arrResponse.delSts == 0 ){
					Swal.fire({
						title: 'Something went wrong, please try again.',
						width: 400,
						padding: '1em 3em',
						timer: 5000,
						type: 'danger',
					});
					
					$("#mainLoader").hide();
				}
			}
		},
		error: function(data){
			console.log(data.trim());
			$("#mainLoader").hide();
		}
	});
}

/**
 *	Get GL account and PO from this function.
 */
function getGlData(rowVal){
	$("#mainLoader").show();
	
	var arguments = "E|" + $(rowVal).attr("data-vennm") ;
	var d = new Date();
	var n = d.getTime();
	var reportName = "cstm_OCR_gl_data" + n + ".dat";
	
	$.ajax({
		data: {
			pgmPath: execPath,
			Args: arguments,
			rptName: reportName
		},
		url: 'callPgmAndGetJSON.jsp',
		type: 'POST',
		dataType: 'text',
		success: function(data){
			//console.log(data);
			if (IsJsonString(data.trim())) {
				var arrResponse = JSON.parse(data.trim());
				
				var glAcc = "<option value='-'>-</option>" + arrResponse.glAccId;

				$("#glAccount").html(glAcc);
				$("#venId").html(arrResponse.venList);
				
				/*$("#poNo").html("<option value='0'>--Blank-- </option>" + arrResponse.poNoList);
				
				var poNo = $(rowVal).attr("data-poNo");
				if( poNo != "" && poNo != "0" && poNo.length > 1 ){
					var found = false;
					$("#poNo option").each(function(index, val){
						if( $(val).val() == poNo ){
							found = true;
						}
					});
					if( found == false ){
						$("#poNo").append('<option value="' + poNo + '">' + poNo + '</option>');
					}
					$("#poNo").val(poNo);
				}else{
					$("#poNo option:first").attr("selected", "selected");
				}*/
				
				$("#glAccount").trigger("change");
			}
			$("#mainLoader").hide();
			$("#editDataModal").modal("toggle");
		},
		error: function(data){
			console.log(data.trim());
			$("#mainLoader").hide();
		}
	});
}

/**
 *	Get GL subaccount from this function.
 */
function getGlSubAccount(){

	var glAcc = $("#glAccount").val();

	if( glAcc != null && glAcc != "" && glAcc != "-" && glAcc.length > 0 ){
		$("#mainLoader").show();
		
		var arguments = "F|STO|" + glAcc;
		var d = new Date();
		var n = d.getTime();
		var reportName = "cstm_OCR_gl_subacc_data" + n + ".dat";

		$.ajax({
			data: {
				pgmPath: execPath,
				Args: arguments,
				rptName: reportName
			},
			url: 'callPgmAndGetJSON.jsp',
			type: 'POST',
			dataType: 'text',
			success: function(data){
				//console.log(data);
				if (IsJsonString(data.trim())) {
					var arrResponse = JSON.parse(data.trim());
					
					var glSubAcc = "<option value='-'>-</option>" + arrResponse.glSubAcc;

					$("#subAccount").html(glSubAcc);
					
					if( globalGlSubAcc != undefined && globalGlSubAcc != null && globalGlSubAcc != "" && globalGlSubAcc != "-" ){
						$("#subAccount").val( globalGlSubAcc ).trigger('change');
						globalGlSubAcc = "";
					}
				}
				$("#mainLoader").hide();
			},
			error: function(data){
				console.log(data.trim());
				$("#mainLoader").hide();
			}
		});
	}
}

/**
 *	Get Cost Reconciliation from this function.
 */
function getCostReconData(venId=""){

	if( venId != "" && venId.length > 0 ){
		$("#mainLoader").show();
		
		var arguments = "G|" + venId;
		var d = new Date();
		var n = d.getTime();
		var reportName = "cstm_OCR_cost_recon_data" + n + ".dat";

		$.ajax({
			data: {
				pgmPath: execPath,
				Args: arguments,
				rptName: reportName
			},
			url: 'callPgmAndGetJSON.jsp',
			type: 'POST',
			dataType: 'text',
			success: function(data){
				//console.log(data);
				if (IsJsonString(data.trim())) {
					var arrResponse = JSON.parse(data.trim());
					$("#costReconTable tbody").html(arrResponse.costReconTbody);
					$("#costReconVenId").html(arrResponse.venList);
				}
				
				if( arrResponse.costReconTbody.length && venId == $("#invoiceVndrId").val() ){
					$("#setPoNo").removeClass("hide-imp");
				}else{
					$("#setPoNo").addClass("hide-imp");
				}
				
				$("#costReconVenId").val( venId );
				$("#costReconModal").modal("show");
				$("#mainLoader").hide();
			},
			error: function(data){
				console.log(data.trim());
				$("#mainLoader").hide();
			}
		});
	}
}

/**
 *	This function is validating for valid JSON String
 */
function IsJsonString(str) {
	try {
		JSON.parse(str);
	} catch (e) {
		return false;
	}
	return true;
}

/*
 *	This function is used to filter Folder table in table
 */
function filterFolderTableData(tblDivId) {
	$("#"+tblDivId+" table tbody tr.main-row").hide();
	
	var fileFltr = invcFltr = vchrFltr = false;
	
	var file = $("#"+tblDivId+" .fileFilter").val().trim();
	var invc = $("#"+tblDivId+" .invcFilter").val().trim();
	var voucher = $("#"+tblDivId+" .voucherFilter").val().trim();
	
	if( file != "" ){
		fileFltr = true;
	}
	
	if( invc != "" ){
		invcFltr = true;
	}
	
	if( voucher != "" ){
		vchrFltr = true;
	}
	
	var fileIndx = $("#"+tblDivId+" .fileFilter").parent().parent().find("th").index( $("#"+tblDivId+" .fileFilter").parent() );
	var invcIndx = $("#"+tblDivId+" .invcFilter").parent().parent().find("th").index( $("#"+tblDivId+" .invcFilter").parent() );
	var vchrIndx = $("#"+tblDivId+" .voucherFilter").parent().parent().find("th").index( $("#"+tblDivId+" .voucherFilter").parent() );
	
	$("#"+tblDivId+" table tbody tr.main-row").each(function(index, val){
		var fileShw = invcShw = vchrShw = true;
		
		//Fields Filtering
		if( fileFltr ){
			var allStr = $(val).children()[fileIndx].textContent;
			if(allStr.toLowerCase().indexOf(file.toLowerCase()) < 0){
				fileShw = false;
			}
		}
		
		//Invoice No Filtering
		if( invcFltr ){
			var allStr = $(val).children()[invcIndx].textContent;
			if(allStr.toLowerCase().indexOf(invc.toLowerCase()) < 0){
				invcShw = false;
			}
		}
		
		//Voucher Filtering
		if( vchrFltr ){
			var allStr = $(val).children()[vchrIndx].textContent;
			if(allStr.toLowerCase().indexOf(voucher.toLowerCase()) < 0){
				vchrShw = false;
			}
		}
		
		if( fileShw && invcShw && vchrShw ){
			$(val).show();
		}
	});
}


function invcVrfyByGraph(){
	
	$('#canvas1').replaceWith('<canvas id="canvas1" style="width: 100%;"></canvas>');
	
	var ctx = document.getElementById("canvas1").getContext('2d');
	var canvas1 = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: ["Kaylin", "Sammy", "Meghan", "Spencer", "Pierre", "Veronica"],
			datasets: [{
				label: 'Non PO',
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
				],
				backgroundColor: "rgba(254, 114, 144, 0.69)",
				borderColor: "#fe7290",
				borderWidth: 1,
			},
			{	
				label: 'PO Basis',
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
				],
				backgroundColor: "#a1d2f4",
				borderColor: "#68b9f0",
				borderWidth: 1,
			}]
		},
		options: {
			/* title: {
				display: true,
				text: 'Chart 1'
			}, */
			legend: {
				position: 'bottom',
			},
			tooltips: {
				mode: 'index',
				intersect: false
			},
			responsive: true,
			scales: {
				xAxes: [{
					stacked: true,
					barThickness: 40,
				}],
				yAxes: [{
					stacked: true
				}]
			},
		}
	});
}


function invcPndVndrGraph(labelArr, dataArr){
	$('#canvas2').replaceWith('<canvas id="canvas2" style="width: 100%;"></canvas>');
	
	var ctx = document.getElementById("canvas2").getContext('2d');
	var canvas2 = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: labelArr,
			datasets: [{
				label: 'Pending Invoices',
				data: dataArr,
				backgroundColor: '#a1d2f4',
				borderColor: '#68b9f0',
				borderWidth: 1
			},
			/* {	
				label: 'Dataset 2',
				data: [5, 25, 10, 15, 22],
				backgroundColor: 'rgba(254, 114, 144, 0.69)',
				borderColor: '#fe7290',
				borderWidth: 1
			},
			{	
				label: 'Dataset 3',
				data: [10, 22, 5, 15, 25],
				backgroundColor: '#88d2d1',
				borderColor: '#72b7b7',
				borderWidth: 1
			} */]
		},
		options: {
			/* title: {
				display: true,
				text: 'Chart 2'
			}, */
			legend: {
				position: 'bottom',
			},
			tooltips: {
				mode: 'index',
				intersect: false
			},
			responsive: true,
			scales: {
				xAxes: [{
					stacked: true,
					barThickness: 40,
				}],
				yAxes: [{
					stacked: true
				}]
			}
		}
	});
}


function invcPndByApprGraph(){
	var ctx = document.getElementById('canvas3').getContext('2d');
	var canvas3 = new Chart(ctx, {
		type: 'pie',
		data: {
			datasets: [{
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
				],
				backgroundColor: [
					'rgb(255, 99, 132)',
					'rgb(255, 159, 64)',
					'rgb(255, 205, 86)',
					'rgb(75, 192, 192)',
					'rgb(54, 162, 235)',
				],
				label: 'Dataset 1'
			}],
			labels: [
				'Veronica',
				'Jaylen',
				'Spencer',
				'Raelynn',
				'Celeste'
			]
		},
		options: {
			/* title: {
				display: true,
				text: 'Chart 3'
			}, */
			legend: {
				position: 'right',
			},
			responsive: true
		}
	});
}


function lstSixMthInvcGraph(){
	var ctx = document.getElementById("canvas4").getContext('2d');
	var canvas4 = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: getMonthsName(),
			datasets: [{
				label: 'Invoices Amount',
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
				],
				backgroundColor: "rgba(254, 114, 144, 0.69)",
				borderColor: "#fe7290",
				borderWidth: 1
			}]
		},
		options: {
			/* title: {
				display: true,
				text: 'Chart 2'
			}, */
			legend: {
				position: 'bottom',
			},
			tooltips: {
				mode: 'index',
				intersect: false
			},
			responsive: true,
			scales: {
				xAxes: [{
					stacked: true,
					barThickness: 40,
				}],
				yAxes: [{
					scaleLabel: {
						display: true,
						labelString: 'Amount in thousands dollars'
					},
					stacked: true
				}]
			}
		}
	});
}


function avgCrdtdDay(){
	var opts = {
	  angle: 0, // The span of the gauge arc
	  lineWidth: 0.44, // The line thickness
	  radiusScale: 1, // Relative radius
	  pointer: {
		length: 0.64, // // Relative to gauge radius
		strokeWidth: 0.035, // The thickness
		color: '#0A0001' // Fill color
	  },
	  limitMax: false,     // If false, max value increases automatically if value > maxValue
	  limitMin: false,     // If true, the min value of the gauge will be fixed
	  colorStart: '#6FADCF',   // Colors
	  colorStop: '#8FC0DA',    // just experiment with them
	  strokeColor: '#E0E0E0',  // to see which ones work best for you
	  generateGradient: true,
	  highDpiSupport: true,     // High resolution support
	  
	};
	var target = document.getElementById('canvas5'); // your canvas element
	var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
	gauge.maxValue = 100; // set max gauge value
	gauge.setMinValue(0);  // Prefer setter over gauge.minValue = 0
	gauge.animationSpeed = 37; // set animation speed (32 is default value)
	gauge.set(30); // set actual value
}


function acctPyblAge(){
	var ctx = document.getElementById('canvas6').getContext('2d');
	var canvas6 = new Chart(ctx, {
		type: 'horizontalBar',
		data: {
			labels: ['1-30 days', '31-60 days', '61-90 days', '> 90days'],
			datasets: [{
				label: 'Account Payable Age',
				backgroundColor: 'rgb(54, 162, 235)',
				borderColor: 'rgb(54, 162, 235)',
				borderWidth: 1,
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
				],
			}]

		},
		options: {
			// Elements options apply to all of the options unless overridden in a dataset
			// In this case, we are setting the border of each horizontal bar to be 2px wide
			elements: {
				rectangle: {
					borderWidth: 1,
				}
			},
			scales: {
				xAxes: [{
					ticks: {
						beginAtZero: true
					}
				}]
			},
			responsive: true,
			legend: {
				position: 'bottom',
			},
			/* title: {
				display: true,
				text: 'Chart 4'
			} */
		}
	});
}

	/*var ctx = document.getElementById("canvas6").getContext('2d');
	var canvas6 = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: ["Mar 2018", "Feb 2018", "Jan 2018", "Dec 2017", "Nov 2017"],
			datasets: [{
				label: 'Quotes',
				data: [12, 19, 15, 10, 29],
				backgroundColor: [
					'rgba(254, 114, 144, 0.69)',
					'rgba(254, 114, 144, 0.69)',
					'rgba(254, 114, 144, 0.69)',
					'rgba(254, 114, 144, 0.69)',
					'rgba(254, 114, 144, 0.69)'
				],
				borderColor: [
					'#fe7290',
					'#fe7290',
					'#fe7290',
					'#fe7290',
					'#fe7290'
				],
				borderWidth: 1
			},
			{	
				label: 'Work Order',
				data: [5, 25, 10, 15, 22],
				backgroundColor: [
					'#a1d2f4',
					'#a1d2f4',
					'#a1d2f4',
					'#a1d2f4',
					'#a1d2f4'
				],
				borderColor: [
					'#68b9f0',
					'#68b9f0',
					'#68b9f0',
					'#68b9f0',
					'#68b9f0'
				],
				borderWidth: 1
			}]
		},
		options: {
			title: {
				display: true,
				text: 'Chart 6'
			},
			legend: {
				position: 'bottom',
			},
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero:true
					}
				}]
			},
			onHover: function(e, items) {
				//$("#canvas5").css("cursor", items[0] ? "pointer" : "default");
				
				//without jquery it can be like this:
				//  var el = document.getElementById("canvas5");
				//  el.style.cursor = e[0] ? "pointer" : "default";
				
			}
		}
	});*/


function apFnnlGraph(){
	var ctx = document.getElementById("canvas7").getContext('2d');
	var canvas7 = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: ["Total Purchase", "Payable Account", "Overdue"],
			datasets: [{
				label: 'Amount',
				data: [10252, 2818, 2017],
				backgroundColor: '#a1d2f4',
				borderColor: '#68b9f0',
				borderWidth: 1
			}]
		},
		options: {
			/* title: {
				display: true,
				text: 'Chart 2'
			}, */
			legend: {
				position: 'bottom',
			},
			tooltips: {
				mode: 'index',
				intersect: false
			},
			responsive: true,
			scales: {
				xAxes: [{
					stacked: true,
					barThickness: 40,
				}],
				yAxes: [{
					/* scaleLabel: {
						display: true,
						labelString: 'Amount in thousands dollars'
					}, */
					stacked: true
				}]
			}
		}
	});
}

function showDashboard(){
	$(".hide-content").hide();
	$("#dashboardNav").addClass("active");

	$("#dashboard").slideDown(1000, function(){
		randomScalingFactor = function() {
			return Math.floor(Math.random() * 100) + 1;
		};
		
		invcVrfyByGraph();
		var labelArr = ["Vendor 1", "Vendor 2", "Vendor 3", "Vendor 4", "Vendor 5", "Vendor 6"];
		var dataArr = [80, 55, 20, 67, 96, 45];
		invcPndVndrGraph(labelArr, dataArr);
		invcPndByApprGraph();
		lstSixMthInvcGraph();
		avgCrdtdDay();
		acctPyblAge();
		apFnnlGraph();
		
		/* ACH Graphs */
		totalPymntMthGraph();
		venPayMtnGraph();
		voucherVsAch();
		voucherCreatedGraph();
    });
}

function clearBlockModal(){
	$("#stepName, #message, #assigneeMode, #approvalTo, #nbrDays, #amount, #amountTo").val('');
	//$("#amountType").val('');
	$("#assigneeMode").val('All');
	$("#emailTo, #brh, #vndr").val(['']).trigger('change');
	$("input[type='radio'][name='daysCndt'][value='frmInvDt']").prop('checked', true);
	$("input[type='checkbox'].blockCndt").prop('checked', false);
	$("input[type='checkbox'][name='costRecon']").prop('checked', false);
	$(".condition-row").css({"transform": "scale(0)","height": "0"});
}

function manageWfWidth(){
	if( $("#wfBlock1>.row>.col").length > 2 ){
		$("#wfBlock1").addClass("col-8");
	}else{
		$("#wfBlock1").removeClass("col-8");
	}
}

function totalPymntMthGraph(){
	if( $('#achCanvas1').length <= 0 ){
		return;
	}
	
	$('#achCanvas1').replaceWith('<canvas id="achCanvas1" style="width: 100%;"></canvas>');
	
	var ctx = document.getElementById("achCanvas1").getContext('2d');
	var canvas1 = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: getMonthsName(),
			datasets: [{
				label: 'With ACH',
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
				],
				backgroundColor: "rgba(254, 114, 144, 0.69)",
				borderColor: "#fe7290",
				borderWidth: 1,
			},
			{	
				label: 'Without ACH',
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
				],
				backgroundColor: "#a1d2f4",
				borderColor: "#68b9f0",
				borderWidth: 1,
			}]
		},
		options: {
			/* title: {
				display: true,
				text: 'Chart 1'
			}, */
			legend: {
				position: 'bottom',
			},
			tooltips: {
				mode: 'index',
				intersect: false
			},
			responsive: true,
			scales: {
				xAxes: [{
					stacked: true,
					barThickness: 40,
				}],
				yAxes: [{
					stacked: true
				}]
			},
		}
	});
}

function venPayMtnGraph(){
	if( $('#achCanvas2').length <= 0 ){
		return;
	}
	
	$('#achCanvas2').replaceWith('<canvas id="achCanvas2" style="width: 100%;"></canvas>');
	
	var ctx = document.getElementById("achCanvas2").getContext('2d');
	var canvas1 = new Chart(ctx, {
		type: 'line',
		data: {
			labels: getMonthsName(),
			datasets: [{
					label: 'Vendor 1010',
					backgroundColor: "rgba(254, 114, 144, 0.7)",
					borderColor: "#fe7290",
					borderWidth: 1,
					data: [
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor()
					],
					fill: true,
				},
				{
					label: 'Vendor 1020',
					backgroundColor: "#a1d2f4",
					borderColor: "#68b9f0",
					borderWidth: 1,
					data: [
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor()
					],
					fill: true,
				},
				{
					label: 'Vendor 1030',
					backgroundColor: "rgba(159, 150, 197, 6)",
					borderColor: "rgb(159, 150, 197)",
					borderWidth: 1,
					data: [
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor()
					],
					fill: true,
				},
				{
					label: 'Vendor 1040',
					backgroundColor: "rgba(75, 192, 192, 0.6)",
					borderColor: "rgb(75, 192, 192)",
					borderWidth: 1,
					data: [
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor()
					],
					fill: true,
				}]
		},
		options: {
			/* title: {
				display: true,
				text: 'Chart 1'
			}, */
			legend: {
				position: 'bottom',
			},
			hover: {
				mode: 'nearest',
				intersect: true
			},
			tooltips: {
				mode: 'index',
				intersect: false
			},
			responsive: true,
			scales: {
				xAxes: [{
					stacked: true,
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Month'
					}
				}],
				yAxes: [{
					stacked: true,
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Amount'
					}
				}]
			},
		}
	});
}


function voucherVsAch(){
	if( $('#achCanvas3').length <= 0 ){
		return;
	}
	
	$('#achCanvas3').replaceWith('<canvas id="achCanvas3" style="width: 100%;"></canvas>');
	
	var ctx = document.getElementById("achCanvas3").getContext('2d');
	var canvas1 = new Chart(ctx, {
		type: 'line',
		data: {
			labels: getMonthsName(),
			datasets: [{
				label: 'Vouchers Created',
				data: [65, 89, 115, 97, 64, 136],
				backgroundColor: "rgba(255, 159, 64, 0.8)",
				borderColor: "rgb(255, 159, 64)",
				borderWidth: 2,
				fill: false,
			},
			{	
				label: 'ACH generated',
				data: [45, 59, 89, 71, 45, 99],
				backgroundColor: "rgba(75, 192, 192, 0.8)",
				borderColor: "rgb(75, 192, 192)",
				borderWidth: 2,
				fill: false,
			}]
		},
		options: {
			/* title: {
				display: true,
				text: 'Chart 1'
			}, */
			legend: {
				position: 'bottom',
			},
			tooltips: {
				mode: 'index',
				intersect: false
			},
			responsive: true,
			scales: {
				xAxes: [{
					display: true,
				}],
				yAxes: [{
					display: true,
				}]
			},
		}
	});
}

function voucherCreatedGraph(){
	if( $('#achCanvas4').length <= 0 ){
		return;
	}
	
	$('#achCanvas4').replaceWith('<canvas id="achCanvas4" style="width: 100%;"></canvas>');
	
	var ctx = document.getElementById("achCanvas4").getContext('2d');
	var canvas1 = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: getMonthsName(),
			datasets: [{
				label: 'Vouchers Created',
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
				],
				backgroundColor: "rgba(254, 114, 144, 0.69)",
				borderColor: "#fe7290",
				borderWidth: 1,
			}]
		},
		options: {
			/* title: {
				display: true,
				text: 'Chart 1'
			}, */
			legend: {
				position: 'bottom',
			},
			tooltips: {
				mode: 'index',
				intersect: false
			},
			responsive: true,
			scales: {
				xAxes: [{
					stacked: true,
					barThickness: 40,
				}],
				yAxes: [{
					stacked: true
				}]
			},
		}
	});
}


function getMonthsName(){
	//var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
	var monthNames = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
	var monthYear = [];
	
	var today = new Date();
	var d;
	var year;
	var month;
	var mthYrStr;
	
	var options = '';
	
	for(var i = 0; i < 6; i++) {
		d = new Date(today.getFullYear(), today.getMonth() - i, 1);
		month = monthNames[d.getMonth()];
		mthYrStr = month + " " + d.getFullYear();
		monthYear.push(mthYrStr);
		
		options += '<option value="'+d.getFullYear()+''+d.getMonth()+'">'+mthYrStr+'</option>';
	}
	
	$("#yearMonth").html(options);
	
	return monthYear;
}