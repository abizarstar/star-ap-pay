$(function(){
	
	//getSidebarMenus();
	
	randomScalingFactor = function() {
		return Math.floor(Math.random() * 100) + 1;
	};
	
	$('#vendors').val(['Vendor 1','Vendor 2','Vendor 3','Vendor 4','Vendor 5','Vendor 6']).trigger('change');
	getMonthsName();
	invcVrfyByGraph();
	var labelArr = ["Vendor 1", "Vendor 2", "Vendor 3", "Vendor 4", "Vendor 5", "Vendor 6"];
	var dataArr = [80, 55, 20, 67, 96, 45];
	invcPndVndrGraph(labelArr, dataArr);
	invcPndByApprGraph();
	lstSixMthInvcGraph();
	avgCrdtdDay();
	acctPyblAge();
	apFnnlGraph();
	
	/* ACH Graphs */
	totalPymntMthGraph();
	venPayMtnGraph();
	voucherVsAch();
	voucherCreatedGraph();
	
	$(document).on("change", "#yearMonth", function(){
		invcVrfyByGraph();
	});
	$(document).on("change", "#vendors", function(){
		if( $("#vendors").val().length ){
			var dataArr = [];
			for( var i=0; i<$("#vendors").val().length; i++ ){
				if( $("#vendors").val()[i] == "Vendor 1" ){
					dataArr.push( 80 );
				}else if( $("#vendors").val()[i] == "Vendor 2" ){
					dataArr.push( 55 );
				}else if( $("#vendors").val()[i] == "Vendor 3" ){
					dataArr.push( 20 );
				}else if( $("#vendors").val()[i] == "Vendor 4" ){
					dataArr.push( 67 );
				}else if( $("#vendors").val()[i] == "Vendor 5" ){
					dataArr.push( 96 );
				}else if( $("#vendors").val()[i] == "Vendor 6" ){
					dataArr.push( 45 );
				}
			}
			invcPndVndrGraph($("#vendors").val(), dataArr);
		}
	});
	
});





function invcVrfyByGraph(){
	
	$('#canvas1').replaceWith('<canvas id="canvas1" style="width: 100%;"></canvas>');
	
	var ctx = document.getElementById("canvas1").getContext('2d');
	var canvas1 = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: ["Kaylin", "Sammy", "Meghan", "Spencer", "Pierre", "Veronica"],
			datasets: [{
				label: 'Non PO',
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
				],
				backgroundColor: "rgba(254, 114, 144, 0.69)",
				borderColor: "#fe7290",
				borderWidth: 1,
			},
			{	
				label: 'PO Basis',
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
				],
				backgroundColor: "#a1d2f4",
				borderColor: "#68b9f0",
				borderWidth: 1,
			}]
		},
		options: {
			/* title: {
				display: true,
				text: 'Chart 1'
			}, */
			legend: {
				position: 'bottom',
			},
			tooltips: {
				mode: 'index',
				intersect: false
			},
			responsive: true,
			scales: {
				xAxes: [{
					stacked: true,
					barThickness: 40,
				}],
				yAxes: [{
					stacked: true
				}]
			},
		}
	});
}


function invcPndVndrGraph(labelArr, dataArr){
	$('#canvas2').replaceWith('<canvas id="canvas2" style="width: 100%;"></canvas>');
	
	var ctx = document.getElementById("canvas2").getContext('2d');
	var canvas2 = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: labelArr,
			datasets: [{
				label: 'Pending Invoices',
				data: dataArr,
				backgroundColor: '#a1d2f4',
				borderColor: '#68b9f0',
				borderWidth: 1
			},
			/* {	
				label: 'Dataset 2',
				data: [5, 25, 10, 15, 22],
				backgroundColor: 'rgba(254, 114, 144, 0.69)',
				borderColor: '#fe7290',
				borderWidth: 1
			},
			{	
				label: 'Dataset 3',
				data: [10, 22, 5, 15, 25],
				backgroundColor: '#88d2d1',
				borderColor: '#72b7b7',
				borderWidth: 1
			} */]
		},
		options: {
			/* title: {
				display: true,
				text: 'Chart 2'
			}, */
			legend: {
				position: 'bottom',
			},
			tooltips: {
				mode: 'index',
				intersect: false
			},
			responsive: true,
			scales: {
				xAxes: [{
					stacked: true,
					barThickness: 40,
				}],
				yAxes: [{
					stacked: true
				}]
			}
		}
	});
}


function invcPndByApprGraph(){
	var ctx = document.getElementById('canvas3').getContext('2d');
	var canvas3 = new Chart(ctx, {
		type: 'pie',
		data: {
			datasets: [{
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
				],
				backgroundColor: [
					'rgb(255, 99, 132)',
					'rgb(255, 159, 64)',
					'rgb(255, 205, 86)',
					'rgb(75, 192, 192)',
					'rgb(54, 162, 235)',
				],
				label: 'Dataset 1'
			}],
			labels: [
				'Veronica',
				'Jaylen',
				'Spencer',
				'Raelynn',
				'Celeste'
			]
		},
		options: {
			/* title: {
				display: true,
				text: 'Chart 3'
			}, */
			legend: {
				position: 'right',
			},
			responsive: true
		}
	});
}


function lstSixMthInvcGraph(){
	var ctx = document.getElementById("canvas4").getContext('2d');
	var canvas4 = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: getMonthsName(),
			datasets: [{
				label: 'Invoices Amount',
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
				],
				backgroundColor: "rgba(254, 114, 144, 0.69)",
				borderColor: "#fe7290",
				borderWidth: 1
			}]
		},
		options: {
			/* title: {
				display: true,
				text: 'Chart 2'
			}, */
			legend: {
				position: 'bottom',
			},
			tooltips: {
				mode: 'index',
				intersect: false
			},
			responsive: true,
			scales: {
				xAxes: [{
					stacked: true,
					barThickness: 40,
				}],
				yAxes: [{
					scaleLabel: {
						display: true,
						labelString: 'Amount in thousands dollars'
					},
					stacked: true
				}]
			}
		}
	});
}


function avgCrdtdDay(){
	var opts = {
	  angle: 0, // The span of the gauge arc
	  lineWidth: 0.44, // The line thickness
	  radiusScale: 1, // Relative radius
	  pointer: {
		length: 0.64, // // Relative to gauge radius
		strokeWidth: 0.035, // The thickness
		color: '#0A0001' // Fill color
	  },
	  limitMax: false,     // If false, max value increases automatically if value > maxValue
	  limitMin: false,     // If true, the min value of the gauge will be fixed
	  colorStart: '#6FADCF',   // Colors
	  colorStop: '#8FC0DA',    // just experiment with them
	  strokeColor: '#E0E0E0',  // to see which ones work best for you
	  generateGradient: true,
	  highDpiSupport: true,     // High resolution support
	  
	};
	var target = document.getElementById('canvas5'); // your canvas element
	var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
	gauge.maxValue = 100; // set max gauge value
	gauge.setMinValue(0);  // Prefer setter over gauge.minValue = 0
	gauge.animationSpeed = 37; // set animation speed (32 is default value)
	gauge.set(30); // set actual value
}


function acctPyblAge(){
	var ctx = document.getElementById('canvas6').getContext('2d');
	var canvas6 = new Chart(ctx, {
		type: 'horizontalBar',
		data: {
			labels: ['1-30 days', '31-60 days', '61-90 days', '> 90days'],
			datasets: [{
				label: 'Account Payable Age',
				backgroundColor: 'rgb(54, 162, 235)',
				borderColor: 'rgb(54, 162, 235)',
				borderWidth: 1,
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
				],
			}]

		},
		options: {
			// Elements options apply to all of the options unless overridden in a dataset
			// In this case, we are setting the border of each horizontal bar to be 2px wide
			elements: {
				rectangle: {
					borderWidth: 1,
				}
			},
			scales: {
				xAxes: [{
					ticks: {
						beginAtZero: true
					}
				}]
			},
			responsive: true,
			legend: {
				position: 'bottom',
			},
			/* title: {
				display: true,
				text: 'Chart 4'
			} */
		}
	});
}

	/*var ctx = document.getElementById("canvas6").getContext('2d');
	var canvas6 = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: ["Mar 2018", "Feb 2018", "Jan 2018", "Dec 2017", "Nov 2017"],
			datasets: [{
				label: 'Quotes',
				data: [12, 19, 15, 10, 29],
				backgroundColor: [
					'rgba(254, 114, 144, 0.69)',
					'rgba(254, 114, 144, 0.69)',
					'rgba(254, 114, 144, 0.69)',
					'rgba(254, 114, 144, 0.69)',
					'rgba(254, 114, 144, 0.69)'
				],
				borderColor: [
					'#fe7290',
					'#fe7290',
					'#fe7290',
					'#fe7290',
					'#fe7290'
				],
				borderWidth: 1
			},
			{	
				label: 'Work Order',
				data: [5, 25, 10, 15, 22],
				backgroundColor: [
					'#a1d2f4',
					'#a1d2f4',
					'#a1d2f4',
					'#a1d2f4',
					'#a1d2f4'
				],
				borderColor: [
					'#68b9f0',
					'#68b9f0',
					'#68b9f0',
					'#68b9f0',
					'#68b9f0'
				],
				borderWidth: 1
			}]
		},
		options: {
			title: {
				display: true,
				text: 'Chart 6'
			},
			legend: {
				position: 'bottom',
			},
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero:true
					}
				}]
			},
			onHover: function(e, items) {
				//$("#canvas5").css("cursor", items[0] ? "pointer" : "default");
				
				//without jquery it can be like this:
				//  var el = document.getElementById("canvas5");
				//  el.style.cursor = e[0] ? "pointer" : "default";
				
			}
		}
	});*/


function apFnnlGraph(){
	var ctx = document.getElementById("canvas7").getContext('2d');
	var canvas7 = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: ["Total Purchase", "Payable Account", "Overdue"],
			datasets: [{
				label: 'Amount',
				data: [10252, 2818, 2017],
				backgroundColor: '#a1d2f4',
				borderColor: '#68b9f0',
				borderWidth: 1
			}]
		},
		options: {
			/* title: {
				display: true,
				text: 'Chart 2'
			}, */
			legend: {
				position: 'bottom',
			},
			tooltips: {
				mode: 'index',
				intersect: false
			},
			responsive: true,
			scales: {
				xAxes: [{
					stacked: true,
					barThickness: 40,
				}],
				yAxes: [{
					/* scaleLabel: {
						display: true,
						labelString: 'Amount in thousands dollars'
					}, */
					stacked: true
				}]
			}
		}
	});
}



function totalPymntMthGraph(){
	if( $('#achCanvas1').length <= 0 ){
		return;
	}
	
	$('#achCanvas1').replaceWith('<canvas id="achCanvas1" style="width: 100%;"></canvas>');
	
	var ctx = document.getElementById("achCanvas1").getContext('2d');
	var canvas1 = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: getMonthsName(),
			datasets: [{
				label: 'With ACH',
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
				],
				backgroundColor: "rgba(254, 114, 144, 0.69)",
				borderColor: "#fe7290",
				borderWidth: 1,
			},
			{	
				label: 'Without ACH',
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
				],
				backgroundColor: "#a1d2f4",
				borderColor: "#68b9f0",
				borderWidth: 1,
			}]
		},
		options: {
			/* title: {
				display: true,
				text: 'Chart 1'
			}, */
			legend: {
				position: 'bottom',
			},
			tooltips: {
				mode: 'index',
				intersect: false
			},
			responsive: true,
			scales: {
				xAxes: [{
					stacked: true,
					barThickness: 40,
				}],
				yAxes: [{
					stacked: true
				}]
			},
		}
	});
}

function venPayMtnGraph(){
	if( $('#achCanvas2').length <= 0 ){
		return;
	}
	
	$('#achCanvas2').replaceWith('<canvas id="achCanvas2" style="width: 100%;"></canvas>');
	
	var ctx = document.getElementById("achCanvas2").getContext('2d');
	var canvas1 = new Chart(ctx, {
		type: 'line',
		data: {
			labels: getMonthsName(),
			datasets: [{
					label: 'Vendor 1010',
					backgroundColor: "rgba(254, 114, 144, 0.7)",
					borderColor: "#fe7290",
					borderWidth: 1,
					data: [
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor()
					],
					fill: true,
				},
				{
					label: 'Vendor 1020',
					backgroundColor: "#a1d2f4",
					borderColor: "#68b9f0",
					borderWidth: 1,
					data: [
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor()
					],
					fill: true,
				},
				{
					label: 'Vendor 1030',
					backgroundColor: "rgba(159, 150, 197, 6)",
					borderColor: "rgb(159, 150, 197)",
					borderWidth: 1,
					data: [
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor()
					],
					fill: true,
				},
				{
					label: 'Vendor 1040',
					backgroundColor: "rgba(75, 192, 192, 0.6)",
					borderColor: "rgb(75, 192, 192)",
					borderWidth: 1,
					data: [
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor()
					],
					fill: true,
				}]
		},
		options: {
			/* title: {
				display: true,
				text: 'Chart 1'
			}, */
			legend: {
				position: 'bottom',
			},
			hover: {
				mode: 'nearest',
				intersect: true
			},
			tooltips: {
				mode: 'index',
				intersect: false
			},
			responsive: true,
			scales: {
				xAxes: [{
					stacked: true,
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Month'
					}
				}],
				yAxes: [{
					stacked: true,
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Amount'
					}
				}]
			},
		}
	});
}


function voucherVsAch(){
	if( $('#achCanvas3').length <= 0 ){
		return;
	}
	
	$('#achCanvas3').replaceWith('<canvas id="achCanvas3" style="width: 100%;"></canvas>');
	
	var ctx = document.getElementById("achCanvas3").getContext('2d');
	var canvas1 = new Chart(ctx, {
		type: 'line',
		data: {
			labels: getMonthsName(),
			datasets: [{
				label: 'Vouchers Created',
				data: [65, 89, 115, 97, 64, 136],
				backgroundColor: "rgba(255, 159, 64, 0.8)",
				borderColor: "rgb(255, 159, 64)",
				borderWidth: 2,
				fill: false,
			},
			{	
				label: 'ACH generated',
				data: [45, 59, 89, 71, 45, 99],
				backgroundColor: "rgba(75, 192, 192, 0.8)",
				borderColor: "rgb(75, 192, 192)",
				borderWidth: 2,
				fill: false,
			}]
		},
		options: {
			/* title: {
				display: true,
				text: 'Chart 1'
			}, */
			legend: {
				position: 'bottom',
			},
			tooltips: {
				mode: 'index',
				intersect: false
			},
			responsive: true,
			scales: {
				xAxes: [{
					display: true,
				}],
				yAxes: [{
					display: true,
				}]
			},
		}
	});
}

function voucherCreatedGraph(){
	if( $('#achCanvas4').length <= 0 ){
		return;
	}
	
	$('#achCanvas4').replaceWith('<canvas id="achCanvas4" style="width: 100%;"></canvas>');
	
	var ctx = document.getElementById("achCanvas4").getContext('2d');
	var canvas1 = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: getMonthsName(),
			datasets: [{
				label: 'Vouchers Created',
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
				],
				backgroundColor: "rgba(254, 114, 144, 0.69)",
				borderColor: "#fe7290",
				borderWidth: 1,
			}]
		},
		options: {
			/* title: {
				display: true,
				text: 'Chart 1'
			}, */
			legend: {
				position: 'bottom',
			},
			tooltips: {
				mode: 'index',
				intersect: false
			},
			responsive: true,
			scales: {
				xAxes: [{
					stacked: true,
					barThickness: 40,
				}],
				yAxes: [{
					stacked: true
				}]
			},
		}
	});
}


function getMonthsName(){
	//var monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
	var monthNames = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
	var monthYear = [];
	
	var today = new Date();
	var d;
	var year;
	var month;
	var mthYrStr;
	
	var options = '';
	
	for(var i = 0; i < 6; i++) {
		d = new Date(today.getFullYear(), today.getMonth() - i, 1);
		month = monthNames[d.getMonth()];
		mthYrStr = month + " " + d.getFullYear();
		monthYear.push(mthYrStr);
		
		options += '<option value="'+d.getFullYear()+''+d.getMonth()+'">'+mthYrStr+'</option>';
	}
	
	$("#yearMonth").html(options);
	
	return monthYear;
}