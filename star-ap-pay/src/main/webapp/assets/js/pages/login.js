$(function(){
	
	var urlParams = new URLSearchParams(window.location.search);
	
	console.log(urlParams.has('param'))
	
	if(urlParams.has('param'))
	{
		var inpParam = urlParams.get('param');
		callLogin(inpParam);
	}
	
	
	if( localStorage.getItem("starOcrAuthToken") != null && localStorage.getItem("starOcrAuthToken") != "" ){
		//window.location.replace("upld-invc.html");
		
		getSidebarMenus();
	}else{
		$("#mainAppWrapper").hide();
	}
    
    $("#password").keyup(function(event){
    	if (event.keyCode === 13) {
    		$("#btnLogin").trigger("click");
    	}
    });
    
    
	$("#btnLogin").click(function(){
		$("#btnLogin").hide();
		$("#loadingLoginWrapper").show();
		
		$("#formLogin .form-control").removeClass("border-danger");
		$("#loginError").html("");
		
		var error = false;
		
		var userId = $("#userId").val();
		var password = $("#password").val();
		
		if( userId == undefined || userId == "" || userId.trim().length == 0 ){
			error = true;
			$("#userId").addClass("border-danger");
		}
		
		if( password == undefined || password == "" || password.trim().length == 0 ){
			error = true;
			$("#password").addClass("border-danger");
		}
		
		if( error == false ){
			var inputData = {
				usrId: userId, 
				usrNm: "", 
				password: password,
				param : "",
			};
			
			$.ajax({
				headers: { 
					'accept': 'application/json',
				    'content-type': 'application/json' 
				},
				type: 'POST',
				dataType: 'json',
		        url: rootAppName + '/rest/auth/validate',
		        data: JSON.stringify( inputData ), // serializes the form's elements.
		        success: function(response, textStatus, jqXHR) {
		            console.log(response);
		            console.log(textStatus);
		            console.log(jqXHR.status);
		            
		            if( response.output.rtnSts == 1)
		            {
		            	if( response.output != undefined && response.output.messages != undefined ){
		            		var loopLimit = response.output.messages.length;
			            	if( loopLimit > 0 ){
			            		errList = '<ul class="m-0 pl-1">';
								for( var i=0; i<loopLimit; i++ ){
									errList += '<li>'+ response.output.messages[i] +'</li>';
								}
								errList += '</ul>';
			            	}
		            	}
		            	
		            	var error = '<div class="alert alert-danger alert-dismissible mb-0" role="alert">'+
										'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
											'<span aria-hidden="true">×</span>'+
										'</button>'+ errList
									'</div>';
		            	$("#loginError").html( error );
		            	
						$("#loadingLoginWrapper").hide();
		            	$("#btnLogin").show();
		            }
		            else
		            {		            
		            	localStorage.setItem("starOcrAuthToken", response.output.authToken);
		            	localStorage.setItem("starOcrUserId", response.output.loginUser);
		            	localStorage.setItem("starOcrUserNm", response.output.usrNm);
		            	localStorage.setItem("starOcrEmailId", response.output.usrEmail);
		            	localStorage.setItem("starOcrUsrGrp", response.output.usrGrp);
		            	localStorage.setItem("starOcrAppHost",response.output.appHost);
		            	localStorage.setItem("starOcrAppPort", response.output.appPort);
		            	localStorage.setItem("starOcrAppToken", response.output.appToken);
		            	localStorage.setItem("starOcrCmpyId", response.output.usrTyp);
		            	
		            	$.cookie('auth-token', localStorage.getItem("starOcrAuthToken"));
		            	$.cookie('app-token', localStorage.getItem("starOcrAppToken"));
						$.cookie('user-id', localStorage.getItem("starOcrUserId"));
		            	
		            	//redirectToDashboard();
		            	
		            	//window.location.replace("upld-invc.html");
		            	//window.location.replace("index.html");
		            	
		            	setAjaxHeader();
		            	getSidebarMenus();
		            }
		            
		            /*if( response.authToken != "" ){
		            	localStorage.setItem("starOcrAuthToken", response.authToken);
		            	localStorage.setItem("starOcrUserId", response["login-user"]);
		            	localStorage.setItem("starOcrUserNm", response.user_name);
		            	localStorage.setItem("starOcrEmailId", response.user_email);
		            	localStorage.setItem("starOcrUsrGrp", response.user_group);
		            	localStorage.setItem("starOcrAppHost", response.appHost);
		            	localStorage.setItem("starOcrAppPort", response.appPort);
		            	localStorage.setItem("starOcrAppToken", response.applicationToken);
		            	localStorage.setItem("starOcrCmpyId", response.user_type);
		            	
		            	$.cookie('auth-token', localStorage.getItem("starOcrAuthToken"));
		            	$.cookie('app-token', localStorage.getItem("starOcrAppToken"));
						$.cookie('user-id', localStorage.getItem("starOcrUserId"));
		            	
		            	//redirectToDashboard();
		            	
		            	//window.location.replace("upld-invc.html");
		            	window.location.replace("index.html");
		            }else{
		            	var errList = "Invalid Credentials";
		            	if( response.output != undefined && response.output.messages != undefined ){
		            		var loopLimit = response.output.messages.length;
			            	if( loopLimit > 0 ){
			            		errList = '<ul class="m-0 pl-1">';
								for( var i=0; i<loopLimit; i++ ){
									errList += '<li>'+ response.output.messages[i] +'</li>';
								}
								errList += '</ul>';
			            	}
		            	}
		            	
		            	var error = '<div class="alert alert-danger alert-dismissible mb-0" role="alert">'+
										'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
											'<span aria-hidden="true">×</span>'+
										'</button>'+ errList
									'</div>';
		            	$("#loginError").html( error );
		            	
						$("#loadingLoginWrapper").hide();
		            	$("#btnLogin").show();
		            }*/
		        }
		    });
		}else{
			$("#loadingLoginWrapper").hide();
		    $("#btnLogin").show();
		}
	});
	
});

function callLogin(inpParam)
{
	$("#mainLoader").show();

	var inputData = {
				usrId: "", 
				usrNm: "", 
				password: "",
				param : inpParam,
			};

	$.ajax({
				headers: { 
					'accept': 'application/json',
				    'content-type': 'application/json' 
				},
				type: 'POST',
				dataType: 'json',
		        url: rootAppName + '/rest/auth/validate',
		        data: JSON.stringify( inputData ), // serializes the form's elements.
		        success: function(response, textStatus, jqXHR) {
		            console.log(response);
		            console.log(textStatus);
		            console.log(jqXHR.status);
		            $("#mainLoader").hide();
		            if( response.output.rtnSts == 1)
		            {
		            	if( response.output != undefined && response.output.messages != undefined ){
		            		var loopLimit = response.output.messages.length;
			            	if( loopLimit > 0 ){
			            		errList = '<ul class="m-0 pl-1">';
								for( var i=0; i<loopLimit; i++ ){
									errList += '<li>'+ response.output.messages[i] +'</li>';
								}
								errList += '</ul>';
			            	}
		            	}
		            	
		            	var error = '<div class="alert alert-danger alert-dismissible mb-0" role="alert">'+
										'<button type="button" class="close" data-dismiss="alert" aria-label="Close">'+
											'<span aria-hidden="true">×</span>'+
										'</button>'+ errList
									'</div>';
		            	$("#loginError").html( error );
		            	
						$("#loadingLoginWrapper").hide();
		            	$("#btnLogin").show();
		            }
		            else
		            {		            
		            	localStorage.setItem("starOcrAuthToken", response.output.authToken);
		            	localStorage.setItem("starOcrUserId", response.output.loginUser);
		            	localStorage.setItem("starOcrUserNm", response.output.usrNm);
		            	localStorage.setItem("starOcrEmailId", response.output.usrEmail);
		            	localStorage.setItem("starOcrUsrGrp", response.output.usrGrp);
		            	localStorage.setItem("starOcrAppHost",response.output.appHost);
		            	localStorage.setItem("starOcrAppPort", response.output.appPort);
		            	localStorage.setItem("starOcrAppToken", response.output.appToken);
		            	localStorage.setItem("starOcrCmpyId", response.output.usrTyp);
		            	
		            	$.cookie('auth-token', localStorage.getItem("starOcrAuthToken"));
		            	$.cookie('app-token', localStorage.getItem("starOcrAppToken"));
						$.cookie('user-id', localStorage.getItem("starOcrUserId"));
		            	
		            	setAjaxHeader();
		            	getSidebarMenus();
		            }
		            
		        }
		    });
}


function redirectToDashboard(){
	var inputData = {
		grpId: localStorage.getItem("starOcrUsrGrp"),
	};
	
	$.ajax({
		headers: {
			'accept': 'application/json',
		    'content-type': 'application/json',
		    'user-id': localStorage.getItem("starOcrUserId"),
		    'auth-token': localStorage.getItem("starOcrAuthToken"),
		    'email-id': localStorage.getItem("starOcrEmailId"),
		    'usr-grp': localStorage.getItem("starOcrUsrGrp"),
		    'req-typ': localStorage.getItem("starOcrReqTyp"),
		},
		data: JSON.stringify( inputData ),
		url: '/doc-mgmt/rest/menu/read',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){		            	
				var loopLimit = response.output.fldTblMenu.length;
				var innerLoopLimit = 0;
				for( var i=0; i<loopLimit; i++){
					innerLoopLimit = response.output.fldTblMenu[i].subMenuList.length;
					if( innerLoopLimit ){												
						for( var j=0; j<innerLoopLimit; j++){
							window.location.replace( response.output.fldTblMenu[i].subMenuList[j].menuHtml );
							break;
						}
					}else{
						window.location.replace( response.output.fldTblMenu[i].menuHtml );
						break;
					}
					break;
				}
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				$("#loginError").html( errList );
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}