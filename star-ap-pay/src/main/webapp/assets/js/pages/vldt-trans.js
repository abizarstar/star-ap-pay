var glblGlAcctCnt = 0, glblGlSubAcctCnt = 0;
var cashRcptProofGlb = 0;

$(function(){
	
	var date = moment().format('MM/DD/YYYY');
	
	var yesterday = moment().subtract(3, "days").format('MM/DD/YYYY');
	
	//$("#upldDtFltr").val(date + " - " + date);
	
	//$("#stmntDtFltr").val(yesterday + " - " + yesterday);
	
	getTransactions();
	
	getEbsList( $("#bnkStmntFlFrmt"), false );
	
	getCustomerList( $("#fltrCusId, #pymntFltrCusId, #crCusId") );
	
	getVendorList($("#crVenId"), "");
	
	getGlList( $("#glAccountAP, #glAccountAddtn") );
	
	getBranchList( $("#pymntFltrBrh, #addSsnBrh, #crBrhId") );
	
	getStxBankList( $("#addSsnBnk, #bnkInfo, #bnkInfoAP, #bnkInfoAddtn") );
	
	getSessionList( $("#savedSsnId") );
	
	getCurrencyList( $("#addSsnArCry1, #addSsnArCry2, #addSsnArCry3") );
	
	getGlList( $("#glAccount") );
	
	$("#glAccount").change(function(){
		var glAcct = $(this).val();
		
		if( glAcct != "" && glAcct != null && glAcct != undefined ){
			getGlSubAccList( glAcct, $("#subAccount") );
		}else{
			$("#subAccount").val("").trigger("change");
			$("#subAccount").attr("disabled", "disabled");
		}
	});
	
	$("#glAccountAP").change(function(){
		var glAcct = $(this).val();
		
		if( glAcct != "" && glAcct != null && glAcct != undefined ){
			getGlSubAccList( glAcct, $("#subAccountAP") );
		}else{
			$("#subAccountAP").val("").trigger("change");
			$("#subAccountAP").attr("disabled", "disabled");
		}
	});
	
	$("#glAccountAddtn").change(function(){
		var glAcct = $(this).val();
		
		if( glAcct != "" && glAcct != null && glAcct != undefined ){
			getGlSubAccList( glAcct, $("#subAccountAddtn") );
		}else{
			$("#subAccountAddtn").val("").trigger("change");
			$("#subAccountAddtn").attr("disabled", "disabled");
		}
	});
	
	
	/*$("#vldtBnkStmntTbl").on("click", ".view-trans-data", function(){
		$(this).closest("tr").next().find(".inner-trans-data-wrapper").slideToggle();
	});*/
	
	$("#vldtBnkStmntTbl").on("click", ".accordian-icon", function(){
		
				
		$(this).closest("tr").next().find(".inner-trans-data-wrapper").slideToggle();			
		
		
		$(this).toggleClass("rotate-180");
	});
	
	$("#searchTransRow").click(function(){
		getTransactions();
	});
	
	$("#loadMoreTrans").click(function(){
	
		var pageNo = $(this).attr("data-page-no");
	
		getTransactions(pageNo);
	});
	
	$("#resetTransFltr").click(function(){
		$("#transStsFltr").val("AL").trigger("change");
		$("#bnkStmntFlFrmt").val("").trigger("change");
		$("#stmntDtFltr, #upldDtFltr").val("");
	});
	
	$("#updtCreditTrans").click(function(){
		updateCreditTransaction();
	});
	
	$("#postCreditTrans").click(function(){
		
		var errFlg = false;
		
		$("#crCusId, #crBrhId").next().find(".select2-selection").removeClass("border-danger");
		
		if($("#arOpenItemsTblSel tbody tr").length == 0 || ($("#arOpenItemsTblSel tbody tr > td:contains(No items selected):visible").length > 0))
		{
			var cusId = $("#crCusId").val();
			var brhId = $("#crBrhId").val();
			
			if(cusId == "" || cusId == undefined || cusId == null)
			{
				$("#crCusId").next().find(".select2-selection").addClass("border-danger");
				errFlg= true;
			}
			
			if(brhId == "" || brhId == undefined || brhId == null)
			{
				$("#crBrhId").next().find(".select2-selection").addClass("border-danger");
				errFlg= true;
			}
			
			if( errFlg == true ){
				return;
			}
		
			$("#postConfirmModal").modal("show");
		}
		else
		{
			postCreditTransaction();
		}
	
		
	});
	
	$("#postConfirm").click(function(){
	
		postCreditTransaction();
	
	});
	
	
	$("#searchCusId").click(function(){
		
		if($("#crCusId").val() == 0)
		{
			return;
		}
		
		getAREnquiry();
	});
	
	
	/*$("#addNwGL").click(function(){

		var tblRow = '';
		tblRow += '<tr data-trn-typ="D">'+
			'<td><select class="form-control select2 gl-acct gl-acct-add" style="width:100%" data-dflt-val=""></select></td>'+
			'<td><select class="form-control select2 gl-sub-acct " style="width:100%" ></select></td>'+
			'<td><select class="form-control select2 gl-acct-typ-add" style="width:100%"> <option value="D">Debit</option><option value="C">Credit</option></select></td>'+
			'<td><input type="text" class="form-control amount decimal debit-amt"/></td>'+
		'</tr>';
										
		$("#glMappingTbody").append(tblRow);
		
		getGlListForMultiple( $("#glMappingTbody tr:last-child .gl-acct-add") );
		
		$("#glMappingTbody tr:last-child .select2").select2();
	
	});*/
	
	$(document).on("change", ".gl-acct-typ-add", function(){
	
		
	
	});
	
	
	
	$("#crCusId").change(function(){
		
		if($("#crCusId").val() == 0){
		
			$('#arOpenItemsTbl').floatThead('destroy');
			$("#arOpenItemsTbl tbody").html("<tr><td colspan='11' style='text-align:center;'>No items available</td></tr>");
			var tblRows = '<tr class="no-rcrds"><td colspan="6" style="text-align:center;">No items selected<br> <br>Add Items that you want to clear from left.</td></tr>';
			$("#arOpenItemsTblSel tbody").html( tblRows );
			$("#arSelCount").html(0); 
			
			
			
			
			/*var payAmt = $("#payAmt").val();
			if( payAmt != "" && !isNaN(payAmt)){
				$("#cashRcptProof").html($("#payAmt").val() + " " + $("#bnkCry").val());
			}*/
			
			//$("#postCreditTrans").attr("disabled", "disabled");
			
			/*var payAmt = $("#payAmt").val();
			if( payAmt != "" && !isNaN(payAmt)){
				calculateProofBefCashRc( payAmt );
			}*/
			return;
		}
		
		getAREnquiry();
	});
	
	
	$("#crVenId").change(function(){
		
		/*if($("#crVenId").val() == 0)
		{
			return;
		}*/
		
		if($("#modalJENo").html() != "")
		{
			return;
		}
		
		getPaymentEntries();
	});
	
	$("#postDt").focusout(function(){
		
		$("#arOpenItemsTblSel .rmv-trn").trigger("click");
		
	});
	
	$("#srcOpnItms").keyup(function(){
	
		var srchStr = $(this).val();
		
		if(srchStr != "")
		{
			$("#arOpenItemsTbl tbody tr").hide();
			
			$("#arOpenItemsTbl tbody tr td").each(function(index, val){
		
				var allStr = $(val).text();
				if(allStr.toLowerCase().indexOf(srchStr.toLowerCase()) >= 0)
				{	
					$(val).parent().show();	
				}
			
			});
		}
		else
		{
			$("#arOpenItemsTbl tbody tr").show();
		}
		
	})
	
	$(document).on("click", ".rmv-trn", function(){
	
	var tblRows = '';
	
	$(this).closest('tr').remove();
	
	$("#arOpenItemsTbl tbody tr[data-inv-ar='" + $(this).attr("data-inv-ar") +"']").find(".clear-trn").removeAttr("disabled");
	$("#arOpenItemsTbl tbody tr[data-inv-ar='" + $(this).attr("data-inv-ar") +"']").find(".clear-trn").closest("tr").removeClass("sel-inv-dsble-row");
 
	
	if($("#arOpenItemsTblSel tbody tr.sel-inv-rcrds").length == 0){
	
		tblRows = '<tr class="no-rcrds"><td colspan="6" style="text-align:center;">No items selected<br> <br>Add Items that you want to clear from left.</td></tr>';
		$("#arOpenItemsTblSel tbody").append( tblRows );
		$("#postCreditTrans").attr("disabled", "disabled");
	}
	
	$("#arSelCount").html($("#arOpenItemsTblSel tbody tr.sel-inv-rcrds").length);
	
	var trnAmt = $("#payAmt").val();
	calculateProofBefCashRc(trnAmt);
	
	});
	
	$(document).on("click", ".clear-trn", function(){
		
		if($("#arOpenItemsTblSel tbody tr.no-rcrds").length){
			$("#arOpenItemsTblSel tbody tr.no-rcrds").hide();
		}
		
		$("#postCreditTrans").removeAttr("disabled");
		
		$(this).attr("disabled", "disabled");
		$(this).closest("tr").addClass("sel-inv-dsble-row");
		
		var arNo = $(this).attr('data-inv-ar');
		var cus = $(this).attr('data-inv-cus');
		var brh = $(this).attr('data-inv-brh');
		var balAmt = $(this).attr('data-inv-bal-amt');
		var dueDt = $(this).attr('data-inv-due');
		var invRef = $(this).attr('data-inv-ref');
		var discAmt = $(this).attr('data-inv-disc');
		var ipAmt = $(this).attr('data-inv-ip-amt');
		var discAmtEditSts = $(this).attr('data-disc-amt-edit-sts');
		var discDt = $(this).attr('data-disc-dt');
		var tblRows = '', discAmtReadOnly = '';
		var prsAmt = balAmt - ipAmt;
		
		if( discDt == undefined || discDt.trim() == "" ){
			if( discAmtEditSts != undefined && discAmtEditSts == "N" ){
				discAmtReadOnly = 'readonly';
			}
			
			tblRows += '<tr class="sel-inv-rcrds" data-inv-ar="'+ arNo + '"  data-inv-cus="'+ cus + '" data-inv-brh="'+ brh + '"  data-inv-amt="'+ prsAmt + '" data-inv-ref="'+ invRef + '" data-inv-disc="'+ discAmt + '"  data-inv-due="'+ dueDt + '"   data-disc-flg="'+ discAmtEditSts + '">'+
					'<td>'+ '<button type="button" class="btn btn-sm btn-inverse-danger rmv-trn" data-inv-ar="' + arNo + '">Remove<i class="icon-trash m-0"></button>' +'</td>'+
					'<td>'+ '<span class="text-primary fs-18 d-block mb-1">' + arNo + '</span>'+ '<span class="b-1">' + cus + ' (' + brh + ')' + '</span></td>'+
					'<td><input type="text" class="form-control disc-amt decimal" value="'+ discAmt +'" '+ discAmtReadOnly +' style="width: 100px"/></td>'+
					'<td>'+ prsAmt +'</td>'+
					'<td>'+ dueDt +'</td>'+	
					'<td>'+ invRef+'</td>'+
					
				'</tr>';
			
			$("#arOpenItemsTblSel tbody").append( tblRows );
			
			$("#arSelCount").html($("#arOpenItemsTblSel tbody tr.sel-inv-rcrds").length);
			
			var trnAmt = $("#payAmt").val();
			calculateProofBefCashRc(trnAmt);
		}else{
			$("#mainLoader").show();
			
			discAmtReadOnly = "readonly";
			
			var inputData = {
				postDt: $("#postDt").val(),
				discDt: discDt,
			};
			
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/ar/vldt-disc',
				type: 'POST',
				dataType : 'json',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						if( response.output.allowFlg == "Y" ){
							discAmtReadOnly = '';
						}
						
						tblRows += '<tr class="sel-inv-rcrds" data-inv-ar="'+ arNo + '"  data-inv-cus="'+ cus + '" data-inv-brh="'+ brh + '"  data-inv-amt="'+ prsAmt + '" data-inv-ref="'+ invRef + '" data-inv-disc="'+ discAmt + '"  data-inv-due="'+ dueDt + '"   data-disc-flg="'+ response.output.allowFlg + '">'+
								'<td>'+ '<button type="button" class="btn btn-sm btn-inverse-danger rmv-trn" data-inv-ar="' + arNo + '">Remove<i class="icon-trash m-0"></button>' +'</td>'+
								'<td>'+ '<span class="text-primary fs-18 d-block mb-1">' + arNo + '</span>'+ '<span class="b-1">' + cus + ' (' + brh + ')' + '</span></td>'+
								'<td><input type="text" class="form-control disc-amt decimal" value="'+ discAmt +'" '+ discAmtReadOnly +' style="width: 100px"/></td>'+
								'<td>'+ prsAmt +'</td>'+
								'<td>'+ dueDt +'</td>'+	
								'<td>'+ invRef+'</td>'+
								
							'</tr>';
						
						$("#arOpenItemsTblSel tbody").append( tblRows );
						
						$("#arSelCount").html($("#arOpenItemsTblSel tbody tr.sel-inv-rcrds").length);
						
						var trnAmt = $("#payAmt").val();
						calculateProofBefCashRc(trnAmt);
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#popupNotify", "alert-danger", errList);
					}
					
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					console.log(jqXHR);
					console.log(textStatus);
					console.log(errorThrown);
					$("#mainLoader").hide();
				}
			});
		}
	});
	
	$("#arOpenItemsTblSel").on("focusout", ".disc-amt", function(){
		var discAmt = $(this).val();
		var maxDiscAmt = $(this).closest("tr").attr("data-inv-disc");
		
		if(discAmt == "")
		{
			discAmt = 0;
			$(this).val("0.00");
		}
		
		if( parseFloat(discAmt) > parseFloat(maxDiscAmt) ){
			$(this).addClass("border-danger");
		}else{
			$(this).removeClass("border-danger");
		}
		
		var trnAmt = $("#payAmt").val();
		calculateProofBefCashRc(trnAmt);
	});
	
	/*  DEBIT for AP OPEN ITEMS TRANSACTIONS */
	
	$(document).on("click", ".rmv-trn-debit", function(){
	
	var tblRows = '';
	
	$(this).closest('tr').remove();
	
	$("#glDebitVchrList tbody tr[data-inv-vchr='" + $(this).attr("data-inv-vchr") +"']").find(".clear-trn-debit").removeAttr("disabled");
	$("#glDebitVchrList tbody tr[data-inv-vchr='" + $(this).attr("data-inv-vchr") +"']").find(".clear-trn-debit").closest("tr").removeClass("sel-inv-dsble-row");
 
	
	if($("#apOpenItemsTblSel tbody tr.sel-inv-rcrds").length == 0){
	
		tblRows = '<tr class="no-rcrds"><td colspan="6" style="text-align:center;">No items selected<br> <br>Add Items that you want to clear from left.</td></tr>';
		$("#apOpenItemsTblSel tbody").append( tblRows );
		$("#saveTypecodeGlPostingDebit").attr("disabled", "disabled");
	}
	
	$("#apSelCount").html($("#apOpenItemsTblSel tbody tr.sel-inv-rcrds").length);
	
	calculateProof();
	
	});
	
	$(document).on("click", ".clear-trn-debit", function(){
		
		if($("#apOpenItemsTblSel tbody tr.no-rcrds").length){
		
			$("#apOpenItemsTblSel tbody tr.no-rcrds").remove();
		}
		
		$("#saveTypecodeGlPostingDebit").removeAttr("disabled");
		
		
		var $this = $(this); 
		
		var vchrNo = $(this).attr('data-inv-vchr');
		var ven = $(this).attr('data-inv-ven');
		var venNm = $(this).attr('data-inv-ven-nm');
		var brh = $(this).attr('data-inv-brh');
		var amt = $(this).attr('data-inv-amt');
		var totVal = $(this).attr('data-tot-inv-amt');
		var dueDt = $(this).attr('data-inv-due');
		var invRef = $(this).attr('data-inv-ref');
		var discAmt = $(this).attr('data-inv-disc');
		var discFlg = $(this).attr('data-disc-flg');
		var clrAllTrn = $(this).attr('data-clr-all');
		
		var tblRows = '';
		
		var iCheckError = 0;
		
		/*if($("#apOpenItemsTblSel tbody tr.sel-inv-rcrds").length > 0)
		{
			if(ven != $("#apOpenItemsTblSel tbody tr.sel-inv-rcrds:first-child").attr('data-inv-ven'))
			{
				iCheckError = iCheckError + 1;
			}
		}*/
		
		if(discFlg == 'N')
		{
			discAmt = '0.00';
		}
		
		if(iCheckError == 0)
			{
			
			//'<td>'+ '<button type="button" class="btn btn-sm btn-inverse-danger rmv-trn-debit" data-inv-vchr="' + vchrNo + '"  data-clr-all="'+ clrAllTrn + '">Remove<i class="icon-trash m-0"></button>' +'</td>'+
			
				tblRows += '<tr class="sel-inv-rcrds" data-inv-vchr="'+ vchrNo + '"  data-inv-ven="'+ ven + '" data-inv-brh="'+ brh + '"  data-inv-amt="'+ totVal + '" data-inv-ref="'+ invRef + '" data-inv-disc="'+ discAmt + '"  data-inv-due="'+ dueDt + '">'+
				'<td>'+ '<i class="icon-trash m-0 text-danger c-pointer rmv-trn-debit" data-inv-vchr="' + vchrNo + '"  data-clr-all="'+ clrAllTrn + '"></i>' +'</td>'+
				'<td>'+ '<span class="text-primary fs-18 d-block mb-1">' + vchrNo + '</span>'+ '<span class="b-1">' + ven + '-' + venNm + ' (' + brh + ')' + '</span></td>'+
				'<td>'+ amt +'</td>'+
				'<td>'+ discAmt +'</td>'+
				'<td>'+ totVal +'</td>'+
				'<td>'+ dueDt +'</td>'+	
				'<td>'+ invRef+'</td>'+
				'</tr>';
		
				$("#apOpenItemsTblSel tbody").append( tblRows );
		
				calculateProof();
		
				$("#apSelCount").html($("#apOpenItemsTblSel tbody tr.sel-inv-rcrds").length);
				
				$this.attr("disabled", "disabled");
				$this.closest("tr").addClass("sel-inv-dsble-row");
				
			}
			else
			{
				customAlert("#popupNotify", "alert-danger", "The selected Invoice " + vchrNo + " belongs to different Vendor");
			}
		
		
		/*$("#apOpenItemsTblSel tbody tr.sel-inv-rcrds").each(function(index, val){
		
			if(ven != $(val).closest('tr').attr('data-inv-ven'))
			{
				iCheckError = iCheckError + 1;
			}
			
			if(iCheckError == 0)
			{
				tblRows += '<tr class="sel-inv-rcrds" data-inv-vchr="'+ vchrNo + '"  data-inv-ven="'+ ven + '" data-inv-brh="'+ brh + '"  data-inv-amt="'+ amt + '" data-inv-ref="'+ invRef + '" data-inv-disc="'+ discAmt + '"  data-inv-due="'+ dueDt + '">'+
				'<td>'+ '<button type="button" class="btn btn-sm btn-inverse-danger rmv-trn-debit" data-inv-vchr="' + vchrNo + '">Remove<i class="icon-trash m-0"></button>' +'</td>'+
				'<td>'+ '<span class="text-primary fs-18 d-block mb-1">' + vchrNo + '</span>'+ '<span class="b-1">' + ven + '-' + venNm + ' (' + brh + ')' + '</span></td>'+
				'<td>'+ discAmt +'</td>'+
				'<td>'+ amt +'</td>'+
				'<td>'+ dueDt +'</td>'+	
				'<td>'+ invRef+'</td>'+
				'</tr>';
		
				$("#apOpenItemsTblSel tbody").append( tblRows );
		
				calculateProof();
		
				$("#apSelCount").html($("#apOpenItemsTblSel tbody tr.sel-inv-rcrds").length);
				
				$this.attr("disabled", "disabled");
				$this.closest("tr").addClass("sel-inv-dsble-row");
			}
			else
			{
				customAlert("#popupNotify", "alert-danger", "The selected Invoice " + vchrNo + " belongs to different Vendor");
			}
		
		});*/
		
		/*if($("#apOpenItemsTblSel tbody tr.sel-inv-rcrds").length == 0)
		{
			tblRows += '<tr class="sel-inv-rcrds" data-inv-vchr="'+ vchrNo + '"  data-inv-ven="'+ ven + '" data-inv-brh="'+ brh + '"  data-inv-amt="'+ amt + '" data-inv-ref="'+ invRef + '" data-inv-disc="'+ discAmt + '"  data-inv-due="'+ dueDt + '">'+
				'<td>'+ '<button type="button" class="btn btn-sm btn-inverse-danger rmv-trn-debit" data-inv-vchr="' + vchrNo + '">Remove<i class="icon-trash m-0"></button>' +'</td>'+
				'<td>'+ '<span class="text-primary fs-18 d-block mb-1">' + vchrNo + '</span>'+ '<span class="b-1">' + ven + '-' + venNm + ' (' + brh + ')' + '</span></td>'+
				'<td>'+ discAmt +'</td>'+
				'<td>'+ amt +'</td>'+
				'<td>'+ dueDt +'</td>'+	
				'<td>'+ invRef+'</td>'+
				'</tr>';
		
				$("#apOpenItemsTblSel tbody").append( tblRows );
		
				calculateProof();
		
				$("#apSelCount").html($("#apOpenItemsTblSel tbody tr.sel-inv-rcrds").length);
				
				$this.attr("disabled", "disabled");
				$this.closest("tr").addClass("sel-inv-dsble-row");
		}*/
	});
	
	
	$(document).on("click", ".clear-trn-debit-total", function(){
		
		if($("#apOpenItemsTblSel tbody tr.no-rcrds").length){
		
			$("#apOpenItemsTblSel tbody tr.no-rcrds").remove();
		}
		
		$("#saveTypecodeGlPostingDebit").removeAttr("disabled");
		
		
		var $this = $(this); 
		
		var clrAllTrn = $(this).attr('data-clr-all');
		
		$("#glDebitVchrListTbody .clear-trn-debit[data-clr-all='" + clrAllTrn+ "']").trigger("click");
		
		var tblRows = '<tr class="vndr-total font-weight-bold text-dark">'+
				'<td>'+ '<button type="button" class="btn btn-sm btn-inverse-danger rmv-trn-debit-total"  data-clr-all="'+ clrAllTrn + '">Remove<i class="icon-trash m-0"></button>' +'</td>'+
				'<td colspan="2">'+ $this.closest('tr').find('td:eq(0)').html() +'</td>'+
				'<td>'+ $this.closest('tr').find('td:eq(3)').html() +'</td>'+
				'<td colspan="3">'+ $this.closest('tr').find('td:eq(6)').html() +'</td>'+
				'</tr>';
		
		$("#apOpenItemsTblSel tbody").append( tblRows );
		
		$this.attr("disabled", "disabled");
		
		return;
		
	});
	
	
	$(document).on("click", ".rmv-trn-debit-total", function(){
	
		var tblRows = '';
		
		var $this = $(this); 
		
		var clrAllTrn = $this.attr('data-clr-all');
		
		$("#apOpenItemsTblSel .rmv-trn-debit[data-clr-all='" + clrAllTrn+ "']").trigger("click");
		
		$this.closest('tr').remove();
		
		$("#glDebitVchrList tbody tr .pay-total-row .clear-trn-debit-total[data-clr-all='" + clrAllTrn +"']").removeAttr("disabled");	 
	
	});
	
	$(document).on("click", ".trn-debit-info", function(){
		
		$("#glDebitVchrListTbody tr td .clear-trn-debit").closest('tr').hide();
		
		var $this = $(this);
		
		if($this.hasClass("text-success"))
		{
			$this.removeClass("text-success");
			$this.addClass("text-primary");
			return;
		}
		
		$this.removeClass("text-primary");
		$this.addClass("text-success"); 
		
		var clrAllTrn = $this.attr('data-clr-all');
		
		$("#glDebitVchrListTbody tr td .clear-trn-debit[data-clr-all='" + clrAllTrn+ "']").closest('tr').show();	 
	
	});
	
	
	
	$("#addNewGLDebit").click(function(){
		
		$("#editTransGlWrapper #remarkAP").val("");
		$("#editTransGlWrapper #drAmtAP").val(0);
		$("#editTransGlWrapper #crAmtAP").val(0);
		$("#editTransGlWrapper #glAccountAP").val('-').trigger('change');	
		//$("#subAccount").html('<option values="-">-</option>');
		$("#editTransGlWrapper #subAccountAP").val($("#editTransGlWrapper #subAccountAP option:first").val()).trigger('change');
		$("#editTransGlWrapper #addGlData").removeClass("update").text("Add");
		
		$(".gl-form-wrapper-ap").toggle();
	
	});
	
	$("#closeGlForm").click(function(){
		$(".gl-form-wrapper-ap").hide();
	});
	
	$(document).on("click", "#addGlData", function(){
		
		var mainParentId = $(this).parents(".modal").attr("id");
		mainParentId = "#"+mainParentId;
		
		$(".gl-form-wrapper-ap .form-control, .gl-form-wrapper-ap .select2-selection").removeClass("border-danger");
		
		var glAcc = $(mainParentId +" #glAccountAP").val();
		var glSubAcc = $(mainParentId +" #subAccountAP").val();
		var glSubAccTxt = $(mainParentId +" #subAccountAP option:selected").text();
		var drAmt = $(mainParentId +" #drAmtAP").val().trim();
		var crAmt = $(mainParentId +" #crAmtAP").val().trim();
		var remark = $(mainParentId +" #remarkAP").val().trim();
		var error = false;
		
		if(crAmt == '')
		{
			crAmt = 0;
		}
		
		if(drAmt == '')
		{
			drAmt = 0;
		}
		
		
		if( glAcc == "" || glAcc == "-" || glAcc == null || glAcc == undefined ){
			error = true;
			$( mainParentId +" #glAccountAP" ).next().find(".select2-selection").addClass("border-danger");
		}
		
		if( $(mainParentId +" #subAccountAP option").length > 1 ){
			if( glSubAcc == "" || glSubAcc == "-" ){
				error = true;
				$( mainParentId +" #subAccountAP" ).next().find(".select2-selection").addClass("border-danger");
			}
		}
		
		if( (drAmt == "" || $.isNumeric(drAmt) == false || drAmt == 0) && (crAmt == "" || $.isNumeric(crAmt) == false || crAmt == 0) ){
			error = true;
			$( mainParentId +" #drAmtAP" ).addClass("border-danger");
			$( mainParentId +" #crAmtAP" ).addClass("border-danger");
		}
		
		if( remark == "" || remark.length == 0 ){
			remark = " ";
		}
		
		if( error == false ){
		
			if($('#glTbody tr > td:contains(No GL mapping found.)').length)
			{
				$('#glTbody tr > td:contains(No GL mapping found.)').parent().remove();
			}
		
			if( $(mainParentId +" #addGlData").hasClass("update") ){
				var uniqueId = $(mainParentId +" #addGlData").attr("data-gl-id");
				$("#"+uniqueId).attr("data-glAcc", glAcc);
				$("#"+uniqueId).attr("data-glSubAcc", glSubAcc);
				$("#"+uniqueId).attr("data-drAmt", drAmt);
				$("#"+uniqueId).attr("data-crAmt", crAmt);
				$("#"+uniqueId).attr("data-remark", remark);
				
				var acctDesc = $("#glAccountAP option:selected").attr("data-acct-desc");
				
				var newData = '<td><span class="text-primary fs-18 mb-1 d-block">'+ glAcc +'</span><span class="b-1">'+ acctDesc + '</span></td>'+
							'<td>'+ glSubAccTxt +'</td>'+
							'<td>'+ drAmt +'</td>'+
							'<td>'+ crAmt +'</td>'+
							'<td>'+ remark +'</td>'+
							'<td> <i class="icon-note menu-icon icon-pointer edit-gl-data-ap"></i> &nbsp; <i class="icon-close menu-icon icon-pointer delete-gl-data-ap"></i> </td>';
							//'<td><i class="icon-close menu-icon icon-pointer delete-gl-data"></i> </td>';
				$("#"+uniqueId).html(newData);
				
			}else{
				var d = new Date();
				var uniqueId = d.valueOf();
				
				var acctDesc = $("#glAccountAP option:selected").attr("data-acct-desc");
				
				var tr = '<tr id="'+ uniqueId +'" data-glAcc="'+ glAcc +'" data-glSubAcc="'+ glSubAcc +'" data-drAmt="'+ drAmt +'" data-crAmt="'+ crAmt +'" data-remark="'+ remark +'">'+
							'<td><span class="text-primary fs-18 mb-1 d-block">'+ glAcc +'</span><span class="b-1">'+ acctDesc + '</span></td>'+
							'<td>'+ glSubAccTxt +'</td>'+
							'<td>'+ drAmt +'</td>'+
							'<td>'+ crAmt +'</td>'+
							'<td>'+ remark +'</td>'+
							'<td> <i class="icon-note menu-icon icon-pointer edit-gl-data-ap"></i> &nbsp; <i class="icon-close menu-icon icon-pointer delete-gl-data-ap"></i> </td>'+
							//'<td><i class="icon-close menu-icon icon-pointer delete-gl-data"></i> </td>'+
						'</tr>';
				$(mainParentId +" #glTbody").append(tr);
			}
			
			$(mainParentId +" #remarkAP").val("");
			$(mainParentId +" #drAmtAP").val(0);
			$(mainParentId +" #crAmtAP").val(0);
			$(mainParentId +" #glAccountAP").val($("#glAccountAP option:first").val()).trigger('change');	
			//$("#subAccount").html('<option values="-">-</option>');
			$(mainParentId +" #subAccountAP").val($("#subAccountAP option:first").val()).trigger('change');
			
			$(mainParentId +" #addGlData").removeClass("update").text("Add");
		}
		
		calculateProof();
	});
	
	
	$(document).on("click", ".edit-gl-data-ap", function(){
		var mainParentId = $(this).parents(".modal").attr("id");
		mainParentId = "#"+mainParentId;
		
		var $tr = $(this).closest( "tr" );
		$(".gl-form-wrapper-ap .form-control, .gl-form-wrapper-ap .select2-selection").removeClass("border-danger");
		$(mainParentId+ " #glAccountAP").val( $tr.attr("data-glAcc") ).trigger('change');
		//globalGlSubAcc = $tr.attr("data-glSubAcc");
		$("#subAccountAP").attr("data-selected", $tr.attr("data-glSubAcc"));
		$(mainParentId+ " #drAmtAP").val( $tr.attr("data-drAmt") );
		$(mainParentId+ " #crAmtAP").val( $tr.attr("data-crAmt") );
		$(mainParentId+ " #remarkAP").val( $tr.attr("data-remark") );
		
		$(mainParentId+ " #addGlData").addClass("update").text("Update");
		$(mainParentId+ " #addGlData").attr("data-gl-id", $tr.attr("id"));
		
		$(mainParentId+ " .gl-form-wrapper-ap").show();
		
		calculateProof();
		
	});
	
	$(document).on("click", ".delete-gl-data-ap", function(){
		$(this).closest( "tr" ).remove();
		
		calculateProof();
	});
	
	
	$("#addNwGL").click(function(){
	
		$(".gl-form-wrapper-addtn").toggle();
	
	});
	
	$("#closeGlFormAddtn").click(function(){
		$(".gl-form-wrapper-addtn").hide();
	});
	
	$(document).on("click", "#addGlDataAddtn", function(){
		
		var mainParentId = $(this).parents(".modal").attr("id");
		mainParentId = "#"+mainParentId;
		
		$(".gl-form-wrapper-addtn .form-control, .gl-form-wrapper-addtn .select2-selection").removeClass("border-danger");
		
		var glAcc = $(mainParentId +" #glAccountAddtn").val();
		var glSubAcc = $(mainParentId +" #subAccountAddtn").val();
		var glSubAccTxt = $(mainParentId +" #subAccountAddtn option:selected").text();
		var drAmt = $(mainParentId +" #drAmtAddtn").val().trim();
		var crAmt = $(mainParentId +" #crAmtAddtn").val().trim();
		var remark = $(mainParentId +" #remarkAddtn").val().trim();
		var error = false;
		
		if(crAmt == "")
		{
			crAmt = 0;
		}
		
		if(drAmt == "")
		{
			drAmt = 0;
		}
		
		if( glAcc == "" || glAcc == "-" || glAcc == null || glAcc == undefined ){
			error = true;
			$( mainParentId +" #glAccountAddtn" ).next().find(".select2-selection").addClass("border-danger");
		}
		
		if( $(mainParentId +" #subAccountAddtn option").length > 1 ){
			if( glSubAcc == "" || glSubAcc == "-" ){
				error = true;
				$( mainParentId +" #subAccountAddtn" ).next().find(".select2-selection").addClass("border-danger");
			}
		}
		
		if( (drAmt == "" || $.isNumeric(drAmt) == false || drAmt == 0) && (crAmt == "" || $.isNumeric(crAmt) == false || crAmt == 0) ){
			error = true;
			$( mainParentId +" #drAmtAddtn" ).addClass("border-danger");
			$( mainParentId +" #crAmtAddtn" ).addClass("border-danger");
		}
		
		if( remark == "" || remark.length == 0 ){
			remark = " ";
		}
		
		if( error == false ){
		
			if($('#glMappingTbody tr > td:contains(No GL mapping found.)').length)
			{
				$('#glMappingTbody tr > td:contains(No GL mapping found.)').parent().remove();
			}
		
			if( $(mainParentId +" #addGlDataAddtn").hasClass("update") ){
				var uniqueId = $(mainParentId +" #addGlDataAddtn").attr("data-gl-id");
				$("#"+uniqueId).attr("data-glAcc", glAcc);
				$("#"+uniqueId).attr("data-glSubAcc", glSubAcc);
				$("#"+uniqueId).attr("data-drAmt", drAmt);
				$("#"+uniqueId).attr("data-crAmt", crAmt);
				$("#"+uniqueId).attr("data-remark", remark);
				
				var acctDesc = $("#glAccountAddtn option:selected").attr("data-acct-desc");
				
				var newData = '<td><span class="text-primary fs-18 mb-1 d-block">'+ glAcc + '</span><span class="b-1">' + acctDesc + '</span></td>'+
							'<td>'+ glSubAccTxt +'</td>'+
							'<td>'+ crAmt +'</td>'+
							'<td>'+ drAmt +'</td>'+
							'<td>'+ remark +'</td>'+
							'<td> <i class="icon-note menu-icon icon-pointer edit-gl-data-addtn"></i> &nbsp; <i class="icon-close menu-icon icon-pointer delete-gl-data-addtn"></i> </td>';
							//'<td><i class="icon-close menu-icon icon-pointer delete-gl-data"></i> </td>';
				$("#"+uniqueId).html(newData);
				
			}else{
				var d = new Date();
				var uniqueId = d.valueOf();
				var dataTrnTyp = "";
				
				if(crAmt > 0 )
				{
					dataTrnTyp = "C";
				}
				
				if(drAmt > 0 )
				{
					dataTrnTyp = "D";
				}
				
				
				var acctDesc = $("#glAccountAddtn option:selected").attr("data-acct-desc");
				
				var tr = '<tr id="'+ uniqueId +'" data-trn-typ="'+ dataTrnTyp +'" data-glAcc="'+ glAcc +'" data-glSubAcc="'+ glSubAcc +'" data-drAmt="'+ drAmt +'" data-crAmt="'+ crAmt +'" data-remark="'+ remark +'">'+
							'<td><span class="text-primary fs-18 mb-1 d-block">'+ glAcc + '</span><span class="b-1">' + acctDesc + '</span></td>'+
							'<td>'+ glSubAccTxt +'</td>'+
							'<td>'+ crAmt +'</td>'+
							'<td>'+ drAmt +'</td>'+
							'<td>'+ remark +'</td>'+
							'<td> <i class="icon-note menu-icon icon-pointer edit-gl-data-addtn"></i> &nbsp; <i class="icon-close menu-icon icon-pointer delete-gl-data-addtn"></i> </td>'+
							//'<td><i class="icon-close menu-icon icon-pointer delete-gl-data"></i> </td>'+
						'</tr>';
				$(mainParentId +" #glMappingTbody").append(tr);
			}
			
			$(mainParentId +" #remarkAddtn").val("");
			$(mainParentId +" #drAmtAddtn").val(0);
			$(mainParentId +" #crAmtAddtn").val(0);
			$(mainParentId +" #glAccountAddtn").val($("#glAccountAddtn option:first").val()).trigger('change');	
			//$("#subAccount").html('<option values="-">-</option>');
			$(mainParentId +" #subAccountAddtn").val($("#subAccountAddtn option:first").val()).trigger('change');
			
			$(mainParentId +" #addGlDataAddtn").removeClass("update").text("Add");
		}
		
		calculateProofAddtn();
	});
	
	
	$(document).on("click", ".edit-gl-data-addtn", function(){
		var mainParentId = $(this).parents(".modal").attr("id");
		mainParentId = "#"+mainParentId;
		
		var $tr = $(this).closest( "tr" );
		$(".gl-form-wrapper-addtn .form-control, .gl-form-wrapper-addtn .select2-selection").removeClass("border-danger");
		$(mainParentId+ " #glAccountAddtn").val( $tr.attr("data-glAcc") ).trigger('change');
		//globalGlSubAcc = $tr.attr("data-glSubAcc");
		$("#subAccountAddtn").attr("data-selected", $tr.attr("data-glsubacc"));
		$(mainParentId+ " #drAmtAddtn").val( $tr.attr("data-drAmt") );
		$(mainParentId+ " #crAmtAddtn").val( $tr.attr("data-crAmt") );
		$(mainParentId+ " #remarkAddtn").val( $tr.attr("data-remark") );
		
		$(mainParentId+ " #addGlDataAddtn").addClass("update").text("Update");
		$(mainParentId+ " #addGlDataAddtn").attr("data-gl-id", $tr.attr("id"));
		
		$(mainParentId+ " .gl-form-wrapper-addtn").show();
		
		calculateProofAddtn();
		
	});
	
	$(document).on("click", ".delete-gl-data-addtn", function(){
		$(this).closest( "tr" ).remove();
		
		if($('#glMappingTbody tr').length == 0)
		{
			tblRow = '<tr class="no-gl-row"><td colspan="4">No GL mapping found.</td></tr>';
			$("#glMappingTbody").html(tblRow);
		}
		
		calculateProofAddtn();
	});

	$("#vldtBnkStmntTbl").on("click", ".edit-bank-trans", function(){
		$("thead th input[type='text']").val('');
		$("#postCreditTrans").attr("data-trans-hdr-id", $(this).attr("data-trans-hdr-id"));
		$("#rcptCrtdDiv").hide();
		$("#postDt").removeAttr("disabled");
		
		var transType = $(this).closest("tr").attr("data-trans-type");
		var trnId = $(this).closest("tr").attr("data-trn-id");
		var hdrId = $(this).closest("tr").attr("data-hdr-id");
		var upldId = $(this).closest("tr").attr("data-upld-id");
		var typCd = $(this).closest("tr").attr("data-typ-cd");	
		var ebsId = $(this).closest("tr").attr("data-ebs-id");	
		var trnAmt = $(this).closest("tr").attr("data-trn-amt");
		var ssnId = $(this).closest("tr").attr("data-trans-ssn");
		var bnkCode = $(this).closest("tr").attr("data-trans-bnk");
		var cashRcpt = $(this).closest("tr").attr("data-trans-rcpt");
		var appRef = $(this).closest("tr").attr("data-app-ref");
		var trnSts = $(this).closest("tr").attr("data-trn-sts");
		var bnkStmtDt = $(this).closest("tr").attr("data-post-dt");
		var bnkRef = $($(this).closest("tr").find("td")[1]).text();
		var addtnlInfo = $($(this).closest("tr").find("td")[3]).text();
		var erpRef = $($(this).closest("tr").find("td")[4]).text();
		
		var date = moment().format('MM/DD/YYYY');
		$("#srcOpnItms").val("");
		
		$("#postDt").val(bnkStmtDt).trigger("change");
		
		/*$("#glPostingOpenModal").hide();*/
		
		if(ssnId.length > 0 )
		{
			$(".ssn-cash-rcpt-title").show();		
			$("#modalSsnId").html(ssnId);
			$("#modalCashRcptNo").html(cashRcpt);
			$(".cash-rcpt-actn-btns button").removeAttr("disabled");
		}
		else
		{
			$(".ssn-cash-rcpt-title").hide();		
			$("#modalSsnId").html("");
			$("#modalCashRcptNo").html("");
			$(".cash-rcpt-actn-btns button").attr("disabled", "disabled");
		}
		
		if( erpRef.length > 0 ){
			$("#glPostingOpenModal").hide();
		}else{
			$("#glPostingOpenModal").show();
			$("#glPostingOpenModal").attr("data-typ-cd-gl", typCd);
		}
		
		if( erpRef.split("-")[0] == "OE" && (transType == "C" || transType == "E") ){
			$("#errMsgVal").html("");
			$("#errMsgVal").hide();
			$("#bnkRefOth").val( bnkRef );
			$("#memoLineOth").val( addtnlInfo );
			$("#bnkInfoAddtn").val( bnkCode ).trigger("change");
			$("#bnkCryAddtn").val( $("#bnkInfoAddtn option:selected").attr("data-cry") );
			trnAmt = trnAmt.replace(/,/g, '');
			$("#trnAmt").val( trnAmt );
			
			$("#typCdGlPostingModal .form-control").attr('disabled','disabled');
			$("#addNwGL, #saveTypecodeGlPosting").hide();
			$("#trnGlRefernce").html("<strong class='text-primary'> Journal Entry : " + erpRef + "</strong>");
			getExistLedgerEntry(erpRef);

			return;
		}else{
			$("#saveTypecodeGlPosting").removeAttr("disabled");
			$("#addNwGL").removeAttr("disabled");
			$("#typCdGlPostingModal .form-control").attr('disabled','disabled');
			$("#editTransGlWrapperAddtn .form-control").removeAttr('disabled');
			$("#glPostingRmk").removeAttr('disabled');
			//getTypCdGlMapping(ebsId, typCd);
			$("#addNwGL").show();
			
			$("#trnGlRefernce").html("");
			$("#saveTypecodeGlPosting").show();
		}
		
		if( transType == "C" ){
			
			$("#modalCashRcptSts").html("<span class='badge badge-outline-warning'>OPEN</span>");
			$("#cashRcptProof").html("");
			cashRcptProofGlb = 0;
			$("#crCusId").val("").trigger("change");
			$("#crBrhId").val("").trigger("change");
			$("#crCusId, #crBrhId").next().find(".select2-selection").removeClass("border-danger");
			$("#postCreditTrans").removeAttr("disabled");
			var trnAmount = $(this).closest("tr").attr("data-trn-amt");
			trnAmount = trnAmount.replace(/,/g, '');
			$("#crCusChkAmt").val(trnAmount);
			
			$("#updtCreditTrans").attr("data-trn-id", trnId);
			$("#updtCreditTrans").attr("data-hdr-id", hdrId);
			$("#updtCreditTrans").attr("data-upld-id", upldId);
			$("#updtCreditTrans").attr("data-trn-typ", "S");
			
			$("#postCreditTrans").attr("data-trn-id", trnId);
			$("#postCreditTrans").attr("data-hdr-id", hdrId);
			$("#postCreditTrans").attr("data-upld-id", upldId);
			$("#postCreditTrans").attr("data-trn-typ", "S");
			$("#postCreditTrans").attr("data-trn-ssn", ssnId);
			$("#postCreditTrans").attr("data-trn-rcpt", cashRcpt);
			$("#postCreditTrans").attr("data-trn-bnk", bnkCode);
			
			if( cashRcpt != undefined && cashRcpt != "" && cashRcpt.length > 0 ){
				$("#postCreditTrans").attr("disabled", "disabled");
			}
			
			$("#bnkInfo").val(bnkCode).trigger("change");
			$("#bnkCry").val($("#bnkInfo option:selected").attr("data-cry"));
			
			$("#arOpnInvCount").html(0);
			$("#arOpnInvCountInPrs").html(0);
			
			
			if(cashRcpt.length > 0)
			{
				getPayments(cashRcpt);
			}
			else
			{
				$("#arPayCount").html(0);
				$("#crCusId").removeAttr("disabled");
				$("#crBrhId").removeAttr("disabled");
				$("#arOpenItemsTblARPayment tbody").html( "" );
				
				
			}
			
			var addtnlInfo = $($(this).closest("tr").find("td")[3]).text();
			var bnkRef = $($(this).closest("tr").find("td")[1]).text();
			
			$("#memoLine").val(addtnlInfo);
			$("#bnkRef").val(bnkRef);
			$("#payAmt").val(trnAmount);
			
			$("#arOpenItemsTbl tbody").html("<tr><td colspan='11' style='text-align:center;'>No items available</td></tr>");
			$("#arOpenItemsTblSel tbody").html(" <tr class='no-rcrds'><td colspan='6' style='text-align:center;'>No items selected<br> <br>Add Items that you want to clear from left.</td></tr>");
			$("#arSelCount").html(0);
			
			if(cashRcpt.length <= 0)
			{
				calculateProofBefCashRc(trnAmount);
			}
			
			$("#crtSsnCashRcptModalNw").modal("show");
			//$("#cashRcptProof").html("");
			$("#postCreditTrans").attr("cash-rcpt-sts", "");
			
		}else if(transType == "D")
		{
			$("#postDtOthDebit").val(bnkStmtDt);
			$("#crVenId").removeAttr("disabled");
			$("#narDesc").removeAttr("disabled");
			$("#narDesc").val("");
			$("#saveTypecodeGlPostingDebit").attr("data-trn-id", trnId);
			$("#saveTypecodeGlPostingDebit").attr("data-hdr-id", hdrId);
			$("#saveTypecodeGlPostingDebit").attr("data-upld-id", upldId);
			$("#saveTypecodeGlPostingDebit").attr("data-trn-typ", "S");
			$("#saveTypecodeGlPostingDebit").attr("data-typ-cd", typCd);
			$("#bnkInfoAP").val(bnkCode).trigger("change");
			$("#bnkCryAP").val($("#bnkInfoAP option:selected").attr("data-cry"));
			
			
			var addtnlInfo = $($(this).closest("tr").find("td")[3]).text();
			var bnkRef = $($(this).closest("tr").find("td")[1]).text();
			var erpRef = $($(this).closest("tr").find("td")[4]).text();
			var errMsg = $($(this).closest("tr").find("td")[5]).text();
			
			var trnAmount = $(this).closest("tr").attr("data-trn-amt");
			trnAmount = trnAmount.replace(/,/g, '');
			
			if(erpRef.length > 0 )
			{
				$(".je-title").show();		
				$("#modalJENo").html(erpRef);
				$("#addNewGLDebit").hide();
				getExistingAPEntries(erpRef);
				$("#tabSelInvTabListDebit").hide();
				$("#saveTypecodeGlPostingDebit").attr("disabled","disabled");
				$("#tabSelInvTabListAP").trigger("click");
			}
			else
			{
				$("#crVenId").val("").trigger("change");
				$(".je-title").hide();		
				$("#modalJENo").html("");
				$("#addNewGLDebit").show();
				getTypCdGlMappingAP(ebsId, typCd);
				$("#tabSelInvTabListDebit").show();
				$("#saveTypecodeGlPostingDebit").removeAttr("disabled");
			}
			
			$("#glDebitVchrListTbody").html( "" );
			$("#apOpenItemsTblSel tbody").html( "" );	
			$("#apSelCount").html(0);
			$(".gl-form-wrapper-ap").hide();
			$("#payAmtDebit").val(trnAmount);
			$("#memoLineOthDebit").val(addtnlInfo);
			$("#bnkRefOthDebit").val(bnkRef);
		
			$("#glPostingStsDebit").hide();
			$("#errMsgValDebit").hide();
			
			
			
			$("#typCdGlPostingModalDebit").modal("show");
		}
		else if( transType == "E"){
			
			$("#postDtOth").val(bnkStmtDt);
			$("#errMsgVal").hide();
			$("#errMsgVal").html("");
			$("#glPostingRmk").val("");
			
			var addtnlInfo = $($(this).closest("tr").find("td")[3]).text();
			var bnkRef = $($(this).closest("tr").find("td")[1]).text();
			var erpRef = $($(this).closest("tr").find("td")[4]).text();
			var errMsg = $($(this).closest("tr").find("td")[5]).text();
			
			$("#bnkInfoAddtn").val(bnkCode).trigger("change");
			$("#bnkCryAddtn").val($("#bnkInfoAddtn option:selected").attr("data-cry"));
			
			trnAmt = trnAmt.replace(/,/g, '');
			
			$("#trnAmt").val(trnAmt);
			$("#memoLineOth").val(addtnlInfo);
			$("#bnkRefOth").val(bnkRef);
			
			if(erpRef.length > 0)
			{
				$("#trnGlRefernce").html("<strong class='text-primary'> Journal Entry : " + erpRef + "</strong>");
			}
			else
			{
				$("#trnGlRefernce").html("");
			}
			
			/*if(appRef.length > 0 && erpRef.length == 0)
			{
				$("#glPostingSts").html('<span class="badge badge-outline-warning">In Process</span>');
			}
			else if(appRef.length > 0 && erpRef.length > 0)
			{
				$("#glPostingSts").html('<span class="badge badge-outline-success">Success</span>');
			}
			else
			{
				$("#glPostingSts").html('');
			}*/
			
			if(trnSts == 'E')
			{	
				$("#errMsgVal").html(errMsg);
				$("#errMsgVal").show();
				$("#glPostingSts").html('<span class="badge badge-outline-danger">Error</span>');
			}
			
			
			$("#saveTypecodeGlPosting").attr("data-ebs-id", ebsId);
			$("#saveTypecodeGlPosting").attr("data-typ-cd", typCd);
			$("#saveTypecodeGlPosting").attr("data-trn-id", trnId);
			$("#saveTypecodeGlPosting").attr("data-hdr-id", hdrId);
			$("#saveTypecodeGlPosting").attr("data-upld-id", upldId);
			$("#saveTypecodeGlPosting").attr("data-err-msg", errMsg);
			
			$("#glPostingProofAmt").html("0");
			$(".gl-form-wrapper-addtn").hide();
			
			if(erpRef.length > 0)
			{
				$("#saveTypecodeGlPosting").attr("disabled", "disabled");
				$("#addNwGL").attr("disabled", "disabled");
				$("#typCdGlPostingModal .form-control").attr('disabled','disabled');
				getExistLedgerEntry(erpRef);
				$("#addNwGL").hide();
			}
			else
			{
				$("#saveTypecodeGlPosting").removeAttr("disabled");
				$("#addNwGL").removeAttr("disabled");
				$("#typCdGlPostingModal .form-control").attr('disabled','disabled');
				$("#editTransGlWrapperAddtn .form-control").removeAttr('disabled');
				$("#glPostingRmk").removeAttr('disabled');
				getTypCdGlMapping(ebsId, typCd);
				$("#addNwGL").show();
				$("#postDtOth").removeAttr("disabled");
			}
			
			/*$("#transAccNo").val(accNo);
			$("#transBnk").val(bank);
			
			$("#updtTrans").attr("data-trans-id", transId);
			$("#transVndrNm").attr("data-change-trigger", "0");
			
			$("#transVndrNm").val(venCus).trigger("change");
			
			$("#transDt").val(transDt);
			$("#transRef").val(transRef);
			$("#transAmt").val(amount);
			
			$("#creditAmt, #debitAmt, #balanceAmt").html("");
			
			if( transRef != "" ){
				$("#vndrInvcTblWrapper").hide();
				$("#creditAmt, #debitAmt").html(amount);
				$("#balanceAmt").html("0");
			}else{
				$("#creditAmt, #balanceAmt").html(amount);
			}
			
			$("#editTransModal").modal("show");*/
		}
		
	});

	$("#vldtBnkStmntTbl").on("click", ".edit-lockbox-trans", function(){
		$("thead th input[type='text']").val('');
		$("#postCreditTrans").attr("data-trans-hdr-id", $(this).attr("data-trans-hdr-id"));
		$("#rcptCrtdDiv").hide();
		$("#postDt").removeAttr("disabled");
		var transType = $(this).closest("tr").attr("data-trans-type");
		var trnId = $(this).closest("tr").attr("data-trn-id");
		var hdrId = $(this).closest("tr").attr("data-hdr-id");
		var upldId = $(this).closest("tr").attr("data-upld-id");
		var typCd = $(this).closest("tr").attr("data-typ-cd");	
		var ebsId = $(this).closest("tr").attr("data-ebs-id");	
		var trnAmt = $(this).closest("tr").attr("data-trn-amt");
		var ssnId = $(this).closest("tr").attr("data-trans-ssn");
		var bnkCode = $(this).closest("tr").attr("data-trans-bnk");
		var cashRcpt = $(this).closest("tr").attr("data-trans-rcpt");
		var appRef = $(this).closest("tr").attr("data-app-ref");
		var trnSts = $(this).closest("tr").attr("data-trn-sts");
		var cusId = $(this).closest("tr").attr("data-cus-id");
		var dpstDtLckbx = $(this).closest("tr").attr("data-post-dt");
		var bnkRef = $($(this).closest("tr").find("td")[1]).text();
		var addtnlInfo = $($(this).closest("tr").find("td")[3]).text();
		var erpRef = $($(this).closest("tr").find("td")[4]).text();
		var date = moment().format('MM/DD/YYYY');
		$("#postDt").val(dpstDtLckbx).trigger("change");
		
		$("#srcOpnItms").val("");
		$("#cashRcptProof").html("");
		$("#modalCashRcptSts").html("<span class='badge badge-outline-warning'>OPEN</span>");
		$("#postCreditTrans").removeAttr("disabled");
		cashRcptProofGlb = 0;
		if(ssnId.length > 0 )
		{
			$(".ssn-cash-rcpt-title").show();		
			$("#modalSsnId").html(ssnId);
			$("#modalCashRcptNo").html(cashRcpt);
			$(".cash-rcpt-actn-btns button").removeAttr("disabled");
		}
		else
		{
			$(".ssn-cash-rcpt-title").hide();		
			$("#modalSsnId").html("");
			$("#modalCashRcptNo").html("");
			$(".cash-rcpt-actn-btns button").attr("disabled", "disabled");
		}
		
		//$("#postCreditTrans").attr("disabled", "disabled");
		var trnAmount = $(this).closest("tr").attr("data-trn-amt");
		trnAmount = trnAmount.replace(/,/g, '');
		$("#crCusChkAmt").val(trnAmount);
		
		if( erpRef.length > 0 ){
			$("#glPostingOpenModal").hide();
		}else{
			$("#glPostingOpenModal").show();
			$("#glPostingOpenModal").attr("data-typ-cd-gl", "LCK");
		}
		
		if( erpRef.split("-")[0] == "OE" ){
			$("#errMsgVal").html("");
			$("#errMsgVal").hide();
			$("#bnkRefOth").val( bnkRef );
			$("#memoLineOth").val( addtnlInfo );
			$("#bnkInfoAddtn").val( bnkCode ).trigger("change");
			$("#bnkCryAddtn").val( $("#bnkInfoAddtn option:selected").attr("data-cry") );
			$("#trnAmt").val( trnAmount );
			$("#trnGlRefernce").html("<strong class='text-primary'> Journal Entry : " + erpRef + "</strong>");
			$("#typCdGlPostingModal .form-control").attr('disabled','disabled');
			$("#addNwGL, #saveTypecodeGlPosting").hide();
			getExistLedgerEntry(erpRef);

			return;
		}else{
			$("#saveTypecodeGlPosting").removeAttr("disabled");
			$("#addNwGL").removeAttr("disabled");
			$("#typCdGlPostingModal .form-control").attr('disabled','disabled');
			$("#editTransGlWrapperAddtn .form-control").removeAttr('disabled');
			$("#glPostingRmk").removeAttr('disabled');
			//getTypCdGlMapping(ebsId, typCd);
			$("#addNwGL").show();
			
			$("#trnGlRefernce").html("");
			$("#saveTypecodeGlPosting").show();
		}
		
	/*	$("#updtCreditTrans").attr("data-trn-id", trnId);
		$("#updtCreditTrans").attr("data-hdr-id", hdrId);
		$("#updtCreditTrans").attr("data-upld-id", upldId);
		$("#updtCreditTrans").attr("data-trn-typ", "L");*/
		
		/*if(cusId.length > 0)
		{
			$("#crCusId").val(cusId).trigger("change");
		}
		else
		{
			$("#crCusId").val("").trigger("change");
		}*/
		
		$("#postCreditTrans").attr("data-trn-id", trnId);
		$("#postCreditTrans").attr("data-hdr-id", hdrId);
		$("#postCreditTrans").attr("data-upld-id", upldId);
		$("#postCreditTrans").attr("data-trn-typ", "L");
		$("#postCreditTrans").attr("data-trn-ssn", ssnId);
		$("#postCreditTrans").attr("data-trn-rcpt", cashRcpt);
		$("#postCreditTrans").attr("data-trn-bnk", bnkCode);
		
		$("#bnkInfo").val(bnkCode).trigger("change");
		$("#bnkCry").val($("#bnkInfo option:selected").attr("data-cry"));
		$("#arSelCount").html($("#arOpenItemsTblSel tbody tr.sel-inv-rcrds").length);
		
		$("#arOpnInvCount").html(0);
		$("#arOpnInvCountInPrs").html(0);
		$("#postCreditTrans").attr("cash-rcpt-sts", "");
		
		if(cashRcpt.length > 0)
		{
			getPayments(cashRcpt);
		}
		else
		{
			$("#arPayCount").html(0);
			$("#crCusId").removeAttr("disabled");
			$("#arOpenItemsTblARPayment tbody").html( "" );
			$("#crBrhId").val("").trigger("change");
			$("#crBrhId").removeAttr("disabled");
			
			if(cusId.length > 0)
			{
				$("#crCusId").val(cusId).trigger("change");
			}
			else
			{
				$("#crCusId").val("").trigger("change");
			}
		}
		
		$("#memoLine").val(addtnlInfo);
		$("#bnkRef").val(bnkRef);
		$("#payAmt").val(trnAmount);
		
		$("#arOpenItemsTbl tbody").html("<tr><td colspan='11' style='text-align:center;'>No items available</td></tr>");
		$("#arOpenItemsTblSel tbody").html(" <tr class='no-rcrds'><td colspan='6' style='text-align:center;'>No items selected<br> <br>Add Items that you want to clear from left.</td></tr>");
		
		if(cashRcpt.length <= 0)
		{
			calculateProofBefCashRc(trnAmt);
		}
		
		if( cashRcpt != undefined && cashRcpt != "" && cashRcpt.length > 0 ){
			$("#postCreditTrans").attr("disabled", "disabled");
		}
		
		$("#crtSsnCashRcptModalNw").modal("show");
		
	});
	
	
	
	$("#showArPymnts").click(function(){
		var refNo = $("#modalCashRcptNo").text();
		$(".cash-rcpt-title").text( refNo );
		getPayments(refNo);
	});
	
	$("#showAdvncPymnt").click(function(){
		$("#apDesc, #apAdvAmt").removeClass("border-danger");
	
		var refNo = $("#modalCashRcptNo").text();
		$(".cash-rcpt-title").text( refNo );
		$("#apDesc, #apAdvAmt").val("");  
		
		getAdvancePayment(refNo);
	});
	
	$("#showUnplydDbt").click(function(){
	
		$("#udDesc, #udDrAmt").removeClass("border-danger");
	
		var refNo = $("#modalCashRcptNo").text();
		$(".cash-rcpt-title").text( refNo );
		$("#udDesc, #udDrAmt").val("");  
		
		getUnappliedDebit(refNo);
	});
	
	$("#showGlDist").click(function(){
	
		$("#glDistModal .form-control, #glDistModal .select2").removeClass("border-danger");
	
		var refNo = $("#modalCashRcptNo").text();
		$(".cash-rcpt-title").text( refNo );
		
		$("#remark").val("");
		$("#drAmt").val(0);
		$("#crAmt").val(0);
		$("#glAccount").val($("#glAccount option:first").val()).trigger('change');
		$("#subAccount").val($("#subAccount option:first").val()).trigger('change');
		
		getGLDistribution(refNo);
	});
	

	$("#addAdvncPymnt").click(function(){
		$("#apDesc, #apAdvAmt").removeClass("border-danger");
		
		var errFlg = false;
		var apDesc = $("#apDesc").val().trim();
		var apAdvAmt = $("#apAdvAmt").val().trim();
		
		if( apDesc == "" ){
			errFlg = true;
			$("#apDesc").addClass("border-danger");
		}
		
		if( apAdvAmt == "" ){
			errFlg = true;
			$("#apAdvAmt").addClass("border-danger");
		}
		
		if( errFlg ){
			return;
		}
		
		$("#mainLoader").show();
	
		var inputData = {
			advAmt: apAdvAmt,
			desc: apDesc,
			cashRcptNo: $(".cash-rcpt-title").html(),
		};
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/rcpt/advPmt',
			type: 'POST',
			dataType : 'json',
			success: function(response){
				if( response.output.authToken == undefined || response.output.authToken == "" ){
					$("#logout").trigger("click");
				}
				
				localStorage.setItem("starOcrAuthToken", response.output.authToken);
				setAjaxHeader();
					
				if( response.output.rtnSts == 0 ){
					customAlert("#popupNotify", "alert-success", "Advance payment saved successfully.");
					$("#apDesc, #apAdvAmt").val("");
					
					getAdvancePayment($(".cash-rcpt-title").html());
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#popupNotify", "alert-danger", errList);
				
					$("#mainLoader").hide();
				}
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				console.log(jqXHR);
				console.log(textStatus);
				console.log(errorThrown);
				$("#mainLoader").hide();
			}
		});
		
	});
	

	$("#addUnapldDbt").click(function(){
		$("#udDesc, #udDrAmt").removeClass("border-danger");
		
		var errFlg = false;
		var udDesc = $("#udDesc").val().trim();
		var udDrAmt = $("#udDrAmt").val().trim();
		
		if( udDesc == "" ){
			errFlg = true;
			$("#udDesc").addClass("border-danger");
		}
		
		if( udDrAmt == "" ){
			errFlg = true;
			$("#udDrAmt").addClass("border-danger");
		}
		
		if( errFlg ){
			return;
		}
		
		$("#mainLoader").show();
	
		var inputData = {
			drAmt: udDrAmt,
			desc: udDesc,
			cashRcptNo: $(".cash-rcpt-title").html(),
		};
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/rcpt/unDebit',
			type: 'POST',
			dataType : 'json',
			success: function(response){
				if( response.output.authToken == undefined || response.output.authToken == "" ){
					$("#logout").trigger("click");
				}
				
				localStorage.setItem("starOcrAuthToken", response.output.authToken);
				setAjaxHeader();
					
				if( response.output.rtnSts == 0 ){
					customAlert("#popupNotify", "alert-success", "Unapplied Debit saved successfully.");
					$("#udDesc, #udDrAmt").val("");
					
					getUnappliedDebit($(".cash-rcpt-title").html());
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#popupNotify", "alert-danger", errList);
				
					$("#mainLoader").hide();
				}
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				console.log(jqXHR);
				console.log(textStatus);
				console.log(errorThrown);
				$("#mainLoader").hide();
			}
		});
		
	});
	

	$("#addGlDist").click(function(){
		$("#glDistModal .form-control, #glDistModal .select2-selection").removeClass("border-danger");
		
		var glAcc = $("#glAccount").val();
		var glSubAcc = $("#subAccount").val();
		var drAmt = $("#drAmt").val().trim();
		var crAmt = $("#crAmt").val().trim();
		var remark = $("#remark").val().trim();
		var error = false;
		
		if( glAcc == "" || glAcc == "-" || glAcc == null || glAcc == undefined ){
			error = true;
			$("#glAccount").next().find(".select2-selection").addClass("border-danger");
		}
		
		if( $("#subAccount option").length > 1 ){
			if( glSubAcc == "" || glSubAcc == "-" ){
				error = true;
				$("#subAccount" ).next().find(".select2-selection").addClass("border-danger");
			}
		}
		
		if( (drAmt == "" || $.isNumeric(drAmt) == false || drAmt == 0) && (crAmt == "" || $.isNumeric(crAmt) == false || crAmt == 0) ){
			error = true;
			$("#drAmt" ).addClass("border-danger");
			$("#crAmt" ).addClass("border-danger");
		}
		
		if( remark == "" || remark.length == 0 ){
			remark = " ";
		}
		
		if( error ){
			return;
		}
		
		$("#mainLoader").show();
	
		var inputData = {
			glAcc: glAcc,
			glSubAcc: glSubAcc,
			drAmt: drAmt,
			crAmt: crAmt,
			remark: remark,
			cashRcptNo: $(".cash-rcpt-title").html(),
		};
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/rcpt/glDist',
			type: 'POST',
			dataType : 'json',
			success: function(response){
				if( response.output.authToken == undefined || response.output.authToken == "" ){
					$("#logout").trigger("click");
				}
				
				localStorage.setItem("starOcrAuthToken", response.output.authToken);
				setAjaxHeader();
					
				if( response.output.rtnSts == 0 ){
					customAlert("#popupNotify", "alert-success", "GL Distribution saved successfully.");
					
					$("#remark").val("");
					$("#drAmt").val(0);
					$("#crAmt").val(0);
					$("#glAccount").val($("#glAccount option:first").val()).trigger('change');
					$("#subAccount").val($("#subAccount option:first").val()).trigger('change');
			
					getGLDistribution($(".cash-rcpt-title").html());
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#popupNotify", "alert-danger", errList);
				
					$("#mainLoader").hide();
				}
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				console.log(jqXHR);
				console.log(textStatus);
				console.log(errorThrown);
				$("#mainLoader").hide();
			}
		});
		
	});
	
	
	$("#savedSsnId").change(function(){
		var ssnId = $(this).val();
		
		if( ssnId != "" && ssnId != null && ssnId != undefined){
			getSpecificSessionData(ssnId);
		}else{
			$("#addSsnBrh, #addSsnBnk, #addSsnBnkExchngRt, #addSsnJrnlDt, #addSsnArCry1, #addSsnArCry2, #addSsnArCry3, #addSsnDpstDt, #addSsnCrsExchngRt1, #addSsnCrsExchngRt2, #addSsnCrsExchngRt3, #addSsnDpstAmt, #addSsnNbrChk").removeAttr("disabled");
			$("#addSsnBrh, #addSsnBnk, #addSsnArCry1, #addSsnArCry2, #addSsnArCry3").val("").trigger("change");
			$("#addSsnBnkCry, #addSsnBnkExchngRt, #addSsnJrnlDt, #addSsnDpstDt, #addSsnCrsExchngRt1, #addSsnCrsExchngRt2, #addSsnCrsExchngRt3, #addSsnDpstAmt, #addSsnNbrChk").val("");
		}
	});
	
	
	$("#transStsFltr, #bnkStmntFlFrmt").change(function(){
		$("#loadMoreTrans").attr("disabled", "disabled");
		$("#loadMoreTrans").addClass("disabled");
	});
	
	$("#stmntDtFltr, #upldDtFltr").focusout(function(){
		$("#loadMoreTrans").attr("disabled", "disabled");
		$("#loadMoreTrans").addClass("disabled");
	});
	
	$("#addSsnBnk").change(function(){
		var bnk = $(this).val();
		
		$("#addSsnArCry1, #addSsnArCry2, #addSsnArCry3").val("").trigger("change");
		$("#addSsnCrsExchngRt1, #addSsnCrsExchngRt2, #addSsnCrsExchngRt3").val("");
		
		if( bnk != "" ){
			$("#addSsnBnkCry").val( $("#addSsnBnk option:selected").attr("data-cry") );
			$("#addSsnBnkExchngRt").val( $("#addSsnBnk option:selected").attr("data-exrt") );
		}else{
			$("#addSsnBnkCry, #addSsnBnkExchngRt").val("");
		}
	});
	
	
	$("#addSsnArCry1").change(function(){
		var origCry = $("#addSsnBnkCry").val();
		var eqvCry = $(this).val();
		
		if( origCry != "" && origCry != undefined && eqvCry != "" && eqvCry != undefined ){
			getExchangeRate(origCry, eqvCry, $("#addSsnCrsExchngRt1"));
		}else{
			$("#addSsnCrsExchngRt1").val("");
		}
	});
	
	$("#addSsnArCry2").change(function(){
		var origCry = $("#addSsnBnkCry").val();
		var eqvCry = $(this).val();
		
		if( origCry != "" && origCry != undefined && eqvCry != "" && eqvCry != undefined ){
			getExchangeRate(origCry, eqvCry, $("#addSsnCrsExchngRt2"));
		}else{
			$("#addSsnCrsExchngRt2").val("");
		}
	});
	
	$("#addSsnArCry3").change(function(){
		var origCry = $("#addSsnBnkCry").val();
		var eqvCry = $(this).val();
		
		if( origCry != "" && origCry != undefined && eqvCry != "" && eqvCry != undefined ){
			getExchangeRate(origCry, eqvCry, $("#addSsnCrsExchngRt3"));
		}else{
			$("#addSsnCrsExchngRt3").val("");
		}
	});
	
	$("#glMappingTbl").on("focusout", ".amount", function(){
		var glDebit = 0.0, glCredit = 0.0;
		
		$("#glMappingTbl .debit-amt").each(function(index, drAmt){
			if($(drAmt).val().trim() != "")
			{
				glDebit += parseFloat($(drAmt).val());
			}
			
		})
		
		$("#glMappingTbl .credit-amt").each(function(index, crAmt){
		
			if($(crAmt).val().trim() != "")
			{
				glCredit += parseFloat($(crAmt).val());
			}
		
			
		})
		
		$("#glPostingProofAmt").html( glDebit - glCredit );
	});
	
	$("#saveTypecodeGlPosting").click(function(){
		$("#typCdGlPostingModal .form-control, #typCdGlPostingModal .select2-selection").removeClass("border-danger");
		var hdrId = $(this).attr("data-hdr-id");
		var upldId = $(this).attr("data-upld-id");
		var trnId = $(this).attr("data-trn-id");
		var typCd = $(this).attr("data-typ-cd");
		var glMapArr = [];
		var rmk = $("#glPostingRmk").val();
		var errFlg = false;
		
		$("#glMappingTbody tr").each(function(index, row){
			var error = false;
			var $row = $(row);
			var trnTyp = $row.attr("data-trn-typ");
			var glAcct = $row.attr("data-glacc");
			var glSubAcct = $row.attr("data-glsubacc");
			var remark = $row.attr("data-remark");
			var drAmt = 0;
			var crAmt = 0;
			crAmt = $row.attr("data-cramt");
			drAmt = $row.attr("data-dramt");
			
			
		/*	if( glAcct == "" || glAcct == null || glAcct == undefined ){
				error = true;
				$row.find(".gl-acct").next().find(".select2-selection").addClass("border-danger");
			}
			
			if( $row.find(".gl-sub-acct option").length > 1 ){
				if( glSubAcct == "" || glSubAcct == null || glSubAcct == undefined ){
					error = true;
					$row.find(".gl-sub-acct").next().find(".select2-selection").addClass("border-danger");
				}
			}
			
			if( amt == "" || amt == null || amt == undefined ){
				error = true;
				$row.find(".amount").addClass("border-danger");
			}*/
			
			if( !error ){
				var glObj = {
					acct: glAcct,
					subAcct: glSubAcct,
					trnTyp: trnTyp,
					crAmt: crAmt,
					drAmt: drAmt,
					remark: remark,
				};
				
				glMapArr.push(glObj);
			}else{
				errFlg = true;
			}
		});
		
		if( rmk == "" ){
			errFlg = true;
			$("#glPostingRmk").addClass("border-danger");
		}
		
		if( glMapArr.length == 0 ){
			errFlg = true;
			customAlert("#popupNotify", "alert-danger", "Please select any GL Account.");
		}
		
		/*if( $("#glPostingProofAmt").text() != "0" ){
			errFlg = true;
			customAlert("#popupNotify", "alert-danger", "Proof must be 0");
		}*/
		
		if( !errFlg ){
			
			$("#mainLoader").show();
			
			var inputData = {
				hdrId: hdrId,
				upldId: upldId,
				trnId: trnId,
				typCd: typCd,
				rmk: rmk,
				glMap: glMapArr,
				postDt: $("#postDtOth").val(),
			};
			
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/bai/upd-gl-data',
				type: 'POST',
				dataType : 'json',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						//ban$("#typCdGlPostingModal").modal("hide");
						
						var erpRef = response.output.glEntry ;
						
						if(erpRef != '' && erpRef != undefined)
						{
							$("#glPostingSts").html('<span class="badge badge-outline-success">Completed</span>');
							getExistLedgerEntry(erpRef);
							$("#searchTransRow").trigger("click");
							$("#errMsgVal").hide();
							customAlert("#popupNotify", "alert-success", "Journal entry <b>"+ response.output.glEntry +"</b> posted successfully. ");
							$("#addNwGL").hide();
							
							$("#trnGlRefernce").html("<strong class='text-primary'> Journal Entry : " + erpRef + "</strong>");
						
							$("#saveTypecodeGlPosting").attr("disabled", "disabled");
							$("#addNwGL").attr("disabled", "disabled");
							$("#typCdGlPostingModal .form-control").attr('disabled','disabled');
						}
						
						
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#popupNotify", "alert-danger", errList);
					}
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					console.log(jqXHR);
					console.log(textStatus);
					console.log(errorThrown);
					$("#mainLoader").hide();
				}
			});
		}
		
	});
	
	
	$("#saveTypecodeGlPostingDebit").click(function(){
		$("#typCdGlPostingModal .form-control, #typCdGlPostingModal .select2-selection").removeClass("border-danger");
		var hdrId = $(this).attr("data-hdr-id");
		var upldId = $(this).attr("data-upld-id");
		var trnId = $(this).attr("data-trn-id");
		var typCd = $(this).attr("data-typ-cd");
		var glMapArr = [];
		var invTrnDataArray = [];
		var errFlg = false;
		var rmk = '';
		var narDesc = $("#narDesc").val();
		
		$("#glTbody tr").each(function(index, row){
			var error = false;
			var $row = $(row);
			var glAcct = $row.attr("data-glacc");
			var glSubAcct = $row.attr("data-glsubacc");
			var crAmt = $row.attr("data-cramt");
			var drAmt = $row.attr("data-dramt");
			var remark = $row.attr("data-remark");
			
			
			if( !error ){
				var glObj = {
					acct: glAcct,
					subAcct: glSubAcct,
					crAmt: crAmt,
					drAmt: drAmt,
					remark: remark,
				};
				
				glMapArr.push(glObj);
			}else{
				errFlg = true;
			}
		});
		
		$("#apOpenItemsTblSel tr").each(function(index, val){
		var eachInvTrnData = {};
		if($(val).attr("data-inv-vchr")!= undefined)
		{
		eachInvTrnData.vchrNo = $(val).attr("data-inv-vchr");
		eachInvTrnData.brh = $(val).attr("data-inv-brh");
		eachInvTrnData.ven = $(val).attr("data-inv-ven");
		
		var amtValue = $(val).attr("data-inv-amt"); 
		amtValue = amtValue.replace(/,/g, '');
		
		var discAmtValue = $(val).attr("data-inv-disc"); 
		discAmtValue = discAmtValue.replace(/,/g, '');
		
		eachInvTrnData.amt = amtValue;
		eachInvTrnData.ref = $(val).attr("data-inv-ref");
		eachInvTrnData.discAmt = discAmtValue;
		eachInvTrnData.due = $(val).attr("data-inv-due");
		
		invTrnDataArray.push(eachInvTrnData);
		}
	});
		
		if( !errFlg ){
			
			$("#mainLoader").show();
			
			var inputData = {
				hdrId: hdrId,
				upldId: upldId,
				trnId: trnId,
				typCd: typCd,
				rmk: rmk,
				narr: narDesc,
				glMap: glMapArr,
				invTrn : invTrnDataArray,
				postDt: $("#postDtOthDebit").val(),
			};
			
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/bai/upd-gl-data-ap',
				type: 'POST',
				dataType : 'json',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						
						if(response.output.glEntry != undefined)
						{	
							$("#saveTypecodeGlPostingDebit").attr("disabled", "disabled");
							$("#apOpenItemsTblSel tbody").html( "" );	
							customAlert("#popupNotify", "alert-success", "Journal entry <b>"+ response.output.glEntry +"</b> posted successfully. ");
							$(".je-title").show();		
							$("#modalJENo").html(response.output.glEntry);
							$("#addNewGLDebit").hide();
							getExistingAPEntries(response.output.glEntry);
							$("#tabSelInvTabListDebit").hide();
							$("#saveTypecodeGlPostingDebit").attr("disabled","disabled");
							$("#tabSelInvTabListAP").trigger("click");
							getTransactions();
						}
						else
						{
							customAlert("#popupNotify", "alert-danger", "Error while posting Journal Entry, Please try again.");
						}
					
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#popupNotify", "alert-danger", errList);
					}
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					console.log(jqXHR);
					console.log(textStatus);
					console.log(errorThrown);
					$("#mainLoader").hide();
				}
			});
		}
		
	});
	
	$("#glMappingTbl").on("change", ".gl-acct.custom-select2", function(){
		var glAcct = $(this).val();
		
		if( glAcct != "" && glAcct != null && glAcct != undefined ){
			getGlSubAccList( glAcct, $(this).closest("tr").find(".gl-sub-acct") );
		}else{
			$(this).closest("tr").find(".gl-sub-acct").html("");
		}
	});
	
	$("#advncPymntTbl").on("click", ".delete-advnc-pymnt", function(){
		$(this).closest("tr").addClass("delete-me");
		$("#deleteAdvncPymntModal").modal("show");
	});
	
	$("#deleteAdvncPymntModal").on('hide.bs.modal', function(){
		$("#advncPymntTbl tbody tr").removeClass("delete-me");
	});
	
	$("#deleteAdvncPymnt").click(function(){
		$("#mainLoader").show();
		
		var inputData = {
			refPfx: $("#advncPymntTbl tbody tr.delete-me").attr("data-ref-pfx"),
			refNo: $("#advncPymntTbl tbody tr.delete-me").attr("data-ref-no"),
			refItm: $("#advncPymntTbl tbody tr.delete-me").attr("data-ref-itm"),
		};
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/rcpt/delAdvPmt',
			type: 'POST',
			dataType : 'json',
			success: function(response){
				if( response.output.authToken == undefined || response.output.authToken == "" ){
					$("#logout").trigger("click");
				}
				
				localStorage.setItem("starOcrAuthToken", response.output.authToken);
				setAjaxHeader();
					
				if( response.output.rtnSts == 0 ){
					customAlert("#popupNotify", "alert-success", "Advance payment deleted successfully.");
					
					$("#deleteAdvncPymntModal").modal("hide");
					
					getAdvancePayment($(".cash-rcpt-title").html());
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#popupNotify", "alert-danger", errList);
				
					$("#mainLoader").hide();
				}
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				console.log(jqXHR);
				console.log(textStatus);
				console.log(errorThrown);
				$("#mainLoader").hide();
			}
		});
	});
	
	
	
	
	$("#unapldDbtTbl").on("click", ".delete-unapld-dbt", function(){
		$(this).closest("tr").addClass("delete-me");
		$("#deleteUnapldDbtModal").modal("show");
	});
	
	$("#deleteUnapldDbtModal").on('hide.bs.modal', function(){
		$("#unapldDbtTbl tbody tr").removeClass("delete-me");
	});
	
	$("#deleteUnapldDbt").click(function(){
		$("#mainLoader").show();
		
		var inputData = {
			refPfx: $("#unapldDbtTbl tbody tr.delete-me").attr("data-ref-pfx"),
			refNo: $("#unapldDbtTbl tbody tr.delete-me").attr("data-ref-no"),
			refItm: $("#unapldDbtTbl tbody tr.delete-me").attr("data-ref-itm"),
		};
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/rcpt/delUnDebit',
			type: 'POST',
			dataType : 'json',
			success: function(response){
				if( response.output.authToken == undefined || response.output.authToken == "" ){
					$("#logout").trigger("click");
				}
				
				localStorage.setItem("starOcrAuthToken", response.output.authToken);
				setAjaxHeader();
					
				if( response.output.rtnSts == 0 ){
					customAlert("#popupNotify", "alert-success", "Unapplied Debit deleted successfully.");
					
					$("#deleteUnapldDbtModal").modal("hide");
					
					getUnappliedDebit($(".cash-rcpt-title").html());
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#popupNotify", "alert-danger", errList);
				
					$("#mainLoader").hide();
				}
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				console.log(jqXHR);
				console.log(textStatus);
				console.log(errorThrown);
				$("#mainLoader").hide();
			}
		});
	});
	
	
	
	$("#glDistTbl").on("click", ".delete-gl-dist", function(){
		$(this).closest("tr").addClass("delete-me");
		$("#deleteGlDistModal").modal("show");
	});
	
	$("#deleteGlDistModal").on('hide.bs.modal', function(){
		$("#glDistTbl tbody tr").removeClass("delete-me");
	});
	
	$("#deleteGlDist").click(function(){
		$("#mainLoader").show();
		
		var inputData = {
			refPfx: $("#glDistTbl tbody tr.delete-me").attr("data-ref-pfx"),
			refNo: $("#glDistTbl tbody tr.delete-me").attr("data-ref-no"),
			refItm: $("#glDistTbl tbody tr.delete-me").attr("data-ref-itm"),
		};
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/rcpt/delGlDist',
			type: 'POST',
			dataType : 'json',
			success: function(response){
				if( response.output.authToken == undefined || response.output.authToken == "" ){
					$("#logout").trigger("click");
				}
				
				localStorage.setItem("starOcrAuthToken", response.output.authToken);
				setAjaxHeader();
					
				if( response.output.rtnSts == 0 ){
					customAlert("#popupNotify", "alert-success", "GL Distribution deleted successfully.");
					
					$("#deleteGlDistModal").modal("hide");
					
					getGLDistribution($(".cash-rcpt-title").html());
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#popupNotify", "alert-danger", errList);
				
					$("#mainLoader").hide();
				}
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				console.log(jqXHR);
				console.log(textStatus);
				console.log(errorThrown);
				$("#mainLoader").hide();
			}
		});
	});
	
	$("#glPostingOpenModal").click(function(){
		$("#crtSsnCashRcptModalNw").modal("hide");
		
		$("#errMsgVal, #glMappingTbody, #trnGlRefernce, #glPostingSts").html("");
		$("#errMsgVal").hide();
		$("#bnkRefOth").val( $("#bnkRef").val() );
		$("#memoLineOth").val( $("#memoLine").val() );
		$("#bnkInfoAddtn").val( $("#bnkInfo").val() ).trigger("change");
		$("#bnkCryAddtn").val( $("#bnkCry").val() );
		$("#trnAmt").val( $("#payAmt").val() );
		$("#postDtOth").val( $("#postDt").val() ).trigger("change");
		$("#glPostingRmk").val("");
		
		
		$("#saveTypecodeGlPosting").attr("data-hdr-id", $("#postCreditTrans").attr("data-hdr-id"));
		$("#saveTypecodeGlPosting").attr("data-upld-id", $("#postCreditTrans").attr("data-upld-id"));
		$("#saveTypecodeGlPosting").attr("data-trn-id", $("#postCreditTrans").attr("data-trn-id"));
				
		$("#saveTypecodeGlPosting").attr("data-typ-cd", $(this).attr("data-typ-cd-gl"));
		
		$("#addNwGL").removeAttr("disabled", "disabled");
		$("#addNwGL").show();
		$("#editTransGlWrapperAddtn .form-control").removeAttr('disabled');
		$("#glPostingRmk").removeClass('border-danger');
		$("#glPostingRmk").removeAttr('disabled');
		$("#postDtOth").removeAttr('disabled');
				
		$("#typCdGlPostingModal").modal("show");
	});
	
	$("#vldtBnkStmntTbl").on("click", ".delete-bank-lockbox-trans", function(){
		var $row = $(this).closest("tr");
		var ebsId = $row.attr("data-ebs-id");
		var hdrId = $row.attr("data-hdr-id");
		var trnId = $row.attr("data-trn-id");
		var upldId = $row.attr("data-upld-id");
		var typeCode = $row.attr("data-typ-cd");
		
		if(typeCode == undefined)
		{
			typeCode = 'L';
		}
		
		$("#deleteBankStatement").attr("data-ebs-id", ebsId);
		$("#deleteBankStatement").attr("data-hdr-id", hdrId);
		$("#deleteBankStatement").attr("data-trn-id", trnId);
		$("#deleteBankStatement").attr("data-upld-id", upldId);
		$("#deleteBankStatement").attr("data-typ-cd", typeCode);
		
		$("#deleteBankStatementModal").modal("show");
	});
	
	$("#deleteBankStatement").click(function(){
		$("#mainLoader").show();
		
		var $delBtn = $("#deleteBankStatement")
			
		var inputData = {
			ebsId: $delBtn.attr("data-ebs-id"),
			hdrId: $delBtn.attr("data-hdr-id"),
			trnId: $delBtn.attr("data-trn-id"),
			upldId: $delBtn.attr("data-upld-id"),
			typeCode: $delBtn.attr("data-typ-cd"),
		};
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData),
			url: rootAppName + '/rest/bai/del-trans',
			type: 'POST',
			dataType : 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					$("#deleteBankStatementModal").modal("hide");
					customAlert("#mainNotify", "alert-success", "Transaction deleted successfully.");
					$("#searchTransRow").trigger("click");
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
				}
				$("#mainLoader").hide();
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				console.log(jqXHR);
				console.log(textStatus);
				console.log(errorThrown);
				$("#mainLoader").hide();
			}
		});
	});
	
});

function getTransactions(pageNo=0){
	$("#mainLoader").show();
	
	var stmntDtFrm = '', stmntDtTo = '', upldDtFrm = '', upldDtTo = '';
	var status = $("#transStsFltr").val();
	var flFrmt = ($("#bnkStmntFlFrmt").val() != null) ? $("#bnkStmntFlFrmt").val() : "";
	var stmntDt = $("#stmntDtFltr").val();
	var upldDt = $("#upldDtFltr").val();
	
	if( stmntDt != "" ){		
		stmntDtFrm = stmntDt.split(" - ")[0];
		stmntDtTo = stmntDt.split(" - ")[1];
	}
	
	if( upldDt != "" ){		
		upldDtFrm = upldDt.split(" - ")[0];
		upldDtTo = upldDt.split(" - ")[1];
	}
		
	var inputData = {
		status : status,
		flFrmt: flFrmt,
		upldOnFrm : upldDtFrm,
		upldOnTo : upldDtTo,
		stmntDtFrm : stmntDtFrm,
		stmntDtTo : stmntDtTo,
		pageNo: pageNo,
	};
	
	if( flFrmt == 2 ){
		getLockBoxFileData(inputData);
	}else{
		getBaiFileData(inputData);
	}	
}


function getBaiFileData(inputData){

	$("#mainLoader").show();
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/bai/read',
		data : JSON.stringify( inputData ),
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopLimit = response.output.fldTblBnkTrn.length;
				var tblRows = '', transAmtTyp = '', transHdrId = '';
				
				for( var i=0; i<loopLimit; i++ ){
					transHdrId = "transHdr" + i;
					var innerTblHdr = '<div class="inner-trans-data-wrapper table-responsive" id="bnkStmt'+ i +'">'+
										'<table class="table m-0">'+
											'<thead>'+
												'<tr>'+
													'<th>Type Code</th>'+
													'<th>Bank Ref</th>'+
													'<th style="width:9%">Customer Ref</th>'+
													'<th style="width:55%">Additional</th>'+
													'<th style="width:250px">ERP Ref</th>'+
													'<th style="width:250px">GL Ref</th>'+
													'<th style="width:350px">Status</th>'+
													'<th>Action</th>'+
												'</tr>'+
												'<tr class="filter-action-row hide-filter">'+
													'<th><input class="custom-text-filter form-control bankRefFilter" type="text" /></th>'+
													'<th><input class="custom-text-filter form-control cusRefFilter" type="text" /></th>'+
													'<th><input class="custom-text-filter form-control addtnTxtFilter" type="text" /></th>'+
													'<th><input class="custom-text-filter form-control erpRefFilter" type="text"/></th>'+
													'<th></th>'+
													'<th></th>'+
													'<th></th>'+
													'<th></th>'+
												'</tr>'+
											'</thead>'+
											'<tbody>';
					var innerLoopLimit = response.output.fldTblBnkTrn[i].listTrn.length;
					if( innerLoopLimit ){
						for( var j=0; j<innerLoopLimit; j++ ){
							transAmtTyp = "";
							
							if( response.output.fldTblBnkTrn[i].listTrn[j].trnType == "C" ){	
								transAmtTyp = '<i class="icon-arrow-down-circle text-success m-0 font-weight-bold" title="Credit"></i>&nbsp;';
							}else if( response.output.fldTblBnkTrn[i].listTrn[j].trnType == "D" ){
								transAmtTyp = '<i class="icon-arrow-up-circle text-danger m-0 font-weight-bold" title="Debit"></i>&nbsp;';
							}
							
							/*if(response.output.fldTblBnkTrn[i].listTrn[j].erpRef.length > 0)
							{
								editIcon = '';	
							}
							else
							{*/
								editIcon = '<i data-trans-hdr-id="'+ transHdrId +'" class="icon-note text-primary edit-bank-trans menu-icon font-weight-bold c-pointer" title="Edit"></i>';
							//}
							
							var erpRef = response.output.fldTblBnkTrn[i].listTrn[j].erpRef;
							var ssnId = '';
							var cashRcpt = '';
							var erpRefVal = '';
							var sts = '', delBtn = '';
							
							if( erpRef.trim().length == 0 ){
								delBtn = '<i data-trans-hdr-id="'+ transHdrId +'" class="icon-trash text-danger delete-bank-lockbox-trans menu-icon font-weight-bold c-pointer ml-1" title="Delete"></i>';
							}
							
							if(erpRef.includes("CH-"))
							{
								var res = erpRef.split("-");
								ssnId = res[0];
								cashRcpt = res[1] + "-" + res[2];
								
								erpRefVal = "<span class='text-primary'>" + cashRcpt + "</span>" + " (" + ssnId +")";
								
							}
							else
							{
								erpRefVal = erpRef;
							}
							
							if(response.output.fldTblBnkTrn[i].listTrn[j].trnSts == 'E')
							{
								sts = '<i class="icon-info text-danger menu-icon font-weight-bold mr-1"></i>';
							}
							else if(response.output.fldTblBnkTrn[i].listTrn[j].trnSts == 'Y')
							{
								sts = '<i class="icon-check text-success menu-icon font-weight-bold mr-1"></i>';
							}
							
							transAmtTyp += response.output.fldTblBnkTrn[i].listTrn[j].trnAmount;
							
							var postDt = response.output.fldTblBnkTrn[i].crtdDttsStr;
							
							var postDtStr;
							if(postDt != "")
							{
								postDtStr = postDt.split(" ");
							}
							
							innerTblHdr += '<tr data-ebs-id="1" data-typ-cd="'+ response.output.fldTblBnkTrn[i].listTrn[j].typCode +'" data-upld-id="'+ response.output.fldTblBnkTrn[i].upldId +'" data-hdr-id="'+ response.output.fldTblBnkTrn[i].hdrId +'" data-trn-amt="'+ response.output.fldTblBnkTrn[i].listTrn[j].trnAmount +'"data-trn-id="'+ response.output.fldTblBnkTrn[i].listTrn[j].id +'" data-trans-type="'+ response.output.fldTblBnkTrn[i].listTrn[j].typCdTrnTyp +'"  data-trans-ssn="'+ ssnId +'"   data-trans-rcpt="'+ cashRcpt +'" data-trans-bnk="'+ response.output.fldTblBnkTrn[i].listTrn[j].bnkCode +'"  data-app-ref="'+ response.output.fldTblBnkTrn[i].listTrn[j].appRef +'"   data-trn-sts="'+ response.output.fldTblBnkTrn[i].listTrn[j].trnSts +'" data-post-dt="'+ postDtStr[0] +'">'+
												'<td><p class="mb-2">('+ response.output.fldTblBnkTrn[i].listTrn[j].typCode +') '+ response.output.fldTblBnkTrn[i].listTrn[j].typNm +'</p><p class="m-0">' + transAmtTyp +'</p></td>'+
												'<td>'+ response.output.fldTblBnkTrn[i].listTrn[j].bnkRef +'</td>'+
												'<td>'+ response.output.fldTblBnkTrn[i].listTrn[j].custRef +'</td>'+
												'<td>'+ response.output.fldTblBnkTrn[i].listTrn[j].adtnRef +'</td>'+
												'<td><b>'+ erpRefVal +'</b></td>'+
												'<td><b>'+ response.output.fldTblBnkTrn[i].listTrn[j].post1Ref +'</b></td>'+
												'<td>' + sts + '<strong>'+ response.output.fldTblBnkTrn[i].listTrn[j].errMsg +'</strong></td>'+
												'<td>'+ editIcon + delBtn + '</td>'+
											'</tr>';
						}
					}else{
						innerTblHdr += '<tr>'+
											'<td colspan="7">No transactions found.</td>'+
										'</tr>';
					}
					
					innerTblHdr += 			'</tbody>'+
										'</table>'+
									'</div>';

					tblRows += '<tr class="trans-header">'+
									'<td>'+
										'<div class="row">'+
											'<div class="col text-primary rcvr-id"><strong>Receiver:</strong> '+ response.output.fldTblBnkTrn[i].rcvrId +'</div>'+
											'<div class="col font-weight-bold"><i class="icon-arrow-up-circle text-danger m-0 font-weight-800" title="Debit"></i>&nbsp;'+ response.output.fldTblBnkTrn[i].totDebTrn +' ('+ response.output.fldTblBnkTrn[i].debTrnCount +')</div>'+
											'<div class="col font-weight-bold"><i class="icon-arrow-down-circle text-success m-0 font-weight-800" title="Credit"></i>&nbsp;'+ response.output.fldTblBnkTrn[i].totCrdTrn +' ('+ response.output.fldTblBnkTrn[i].crdTrnCount +')</div>'+
											'<div class="col stmt-dt"><b>Statement Date:</b> '+ response.output.fldTblBnkTrn[i].crtdDttsStr +'</div>'+
											'<div class="col stmt-dt"><b>Uploaded On:</b> '+ response.output.fldTblBnkTrn[i].upldOnStr +'</div>'+
											'<div class="col-auto text-center"><i class="icon-arrow-down accordian-icon"  id="'+ transHdrId +'"></i></div>'+
										'</div>'+
									'</td>'+
								'</tr>'+
							   '<tr class="m-0 p-0">'+
							   		'<td colspan="6" class="m-0 p-0">' + innerTblHdr + '</td>'+
							   	'</tr>';
				}
				
				if( inputData.pageNo == 0 ){
					$("#vldtBnkStmntTbl tbody").html( tblRows );
				}else{
					$("#vldtBnkStmntTbl tbody").append( tblRows );
				}
				
				if( response.output.eof ){
					$("#loadMoreTrans").attr("disabled", "disabled");
					$("#loadMoreTrans").addClass("disabled");
				}else{
					$("#loadMoreTrans").removeAttr("disabled");
					$("#loadMoreTrans").removeClass("disabled");
				}
				
				$("#loadMoreTrans").attr("data-page-no", parseInt(inputData.pageNo)+1);
				
				var transHdrId = $("#postCreditTrans").attr("data-trans-hdr-id");
				if( transHdrId != undefined && transHdrId != "" ){
					$("#"+transHdrId).trigger("click");
					$("#postCreditTrans").removeAttr("data-trans-hdr-id");
				}
				
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();	
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}

function getLockBoxFileData(inputData){
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/bai/read-lckbx',
		data : JSON.stringify( inputData ),
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopLimit = response.output.fldTblLckBxHdr.length;
				var tblRows = '', prsSts = '', transHdrId = "";
				
				for( var i=0; i<loopLimit; i++ ){
					transHdrId = "transHdr" + i;
					var innerTblHdr = '<div class="inner-trans-data-wrapper table-responsive" id="lckbxStmt'+ i +'">'+
										'<table class="table m-0">'+
											'<thead>'+
												'<tr>'+
													'<th>Amount</th>'+
													'<th>Check No.</th>'+
													'<th>Customer</th>'+
													'<th>Invoice No.</th>'+
													'<th style="width:250px">ERP Ref</th>'+
													'<th style="width:250px">GL Ref</th>'+
													'<th>Error Message</th>'+ 
													'<th>Action</th>'+
												'</tr>'+
												'<tr class="filter-action-row hide-filter">'+
													'<th></th>'+
													'<th><input class="custom-text-filter form-control cusRefFilter" type="text" /></th>'+
													'<th><input class="custom-text-filter form-control addtnTxtFilter" type="text" /></th>'+
													'<th><input class="custom-text-filter form-control erpRefFilter" type="text"/></th>'+
													'<th></th>'+
													'<th></th>'+
													'<th></th>'+
													'<th></th>'+
												'</tr>'+
											'</thead>'+
											'<tbody>';
					var innerLoopLimit = response.output.fldTblLckBxHdr[i].listTrn.length;
					if( innerLoopLimit ){
						for( var j=0; j<innerLoopLimit; j++ ){	
						
							var erpRef = response.output.fldTblLckBxHdr[i].listTrn[j].erpRef;
							var ssnId = '';
							var cashRcpt = '';
							var erpRefVal = '';
							var sts = '', delBtn = '';
							
							if( erpRef.trim().length == 0 ){
								delBtn = '<i data-trans-hdr-id="'+ transHdrId +'" class="icon-trash text-danger delete-bank-lockbox-trans menu-icon font-weight-bold c-pointer ml-1" title="Delete"></i>';
							}
							
							if(erpRef.includes("CH-"))
							{
								var res = erpRef.split("-");
								ssnId = res[0];
								cashRcpt = res[1] + "-" + res[2];
								erpRefVal = "<span class='text-primary'>" + cashRcpt + "</span>" + " (" + ssnId +")";
								
							}
							else
							{
								erpRefVal = erpRef;
							}
							
							var customerRef = response.output.fldTblLckBxHdr[i].listTrn[j].customer;
							var cusIdArr = '';
							if(customerRef.length > 0)
							{
								cusIdArr = customerRef.split("(");
								
								cusId = cusIdArr[0];
								
								cusId = cusId.trim();
							}
							else
							{
								cusId = '';
							}
							
							
							var postDtLckBx = response.output.fldTblLckBxHdr[i].dpstDt;
							
							var postDtStrLckBx;
							if(postDtLckBx != "")
							{
								postDtStrLckBx = postDtLckBx.split(" ");
							}
							
							
							prsSts = (response.output.fldTblLckBxHdr[i].listTrn[j].prs) ? "<span class='badge badge-success'>Yes</span>" : "<span class='badge badge-danger'>No</span>";						
							innerTblHdr += '<tr data-ebs-id="2" data-seq-no="'+ response.output.fldTblLckBxHdr[i].listTrn[j].seqNo +'" data-trn-amt="'+ response.output.fldTblLckBxHdr[i].listTrn[j].amount +'" data-batch-id="'+ response.output.fldTblLckBxHdr[i].batchId +'" data-upld-id="'+ response.output.fldTblLckBxHdr[i].ebsUpldId +'" data-hdr-id="'+ response.output.fldTblLckBxHdr[i].id +'" data-trn-id="'+ response.output.fldTblLckBxHdr[i].listTrn[j].id +'" data-trans-ssn="'+ ssnId +'" data-trans-rcpt="'+ cashRcpt +'" data-trans-bnk="'+ response.output.fldTblLckBxHdr[i].listTrn[j].bnkCode +'"  data-app-ref="" data-trn-sts="'+ response.output.fldTblLckBxHdr[i].listTrn[j].trnSts +'" data-cus-id="'+ cusId +'"  data-post-dt="'+ postDtStrLckBx[0] +'">'+
												'<td>'+ response.output.fldTblLckBxHdr[i].listTrn[j].amountStr +'</td>'+
												'<td>'+ response.output.fldTblLckBxHdr[i].listTrn[j].chkNo +'</td>'+
												'<td><strong>'+ customerRef +'</strong></td>'+
												'<td>'+ response.output.fldTblLckBxHdr[i].listTrn[j].invNo +'</td>'+
												'<td><b>'+ erpRefVal +'</b></td>'+
												'<td><b>'+ response.output.fldTblLckBxHdr[i].listTrn[j].post1Ref +'</b></td>'+
												'<td>'+ response.output.fldTblLckBxHdr[i].listTrn[j].errMsg +'</td>'+
												'<td><i data-trans-hdr-id="'+ transHdrId +'" class="icon-note text-primary edit-lockbox-trans menu-icon font-weight-bold c-pointer" title="Edit"></i>'+ delBtn +'</td>'+
											'</tr>';
						}
					}else{
						innerTblHdr += '<tr>'+
											'<td colspan="7">No transactions found.</td>'+
										'</tr>';
					}
					
					innerTblHdr += 			'</tbody>'+
										'</table>'+
									'</div>';
					tblRows += '<tr class="trans-header">'+
									'<td>'+
										'<div class="row">'+
											'<div class="col text-primary"><strong>Batch ID:</strong> '+ response.output.fldTblLckBxHdr[i].batchId +'</div>'+
											'<div class="col text-success"><strong>Total Amount:</strong> '+ response.output.fldTblLckBxHdr[i].batchTotStr +'</div>'+
											'<div class="col"><b>Deposite Date:</b> '+ response.output.fldTblLckBxHdr[i].dpstDt +'</div>'+
											'<div class="col stmt-dt"><b>Uploaded On: </b> '+ response.output.fldTblLckBxHdr[i].upldOnStr +'</div>'+
											'<div class="col stmt-dt"><b>Transactions: </b> '+ response.output.fldTblLckBxHdr[i].rcrdCount +'</div>'+
											'<div class="col-auto text-center"><i class="icon-arrow-down accordian-icon" id="'+ transHdrId +'"></i></div>'+
										'</div>'+
									'</td>'+
								'</tr>'+
							   '<tr class="m-0 p-0">'+
							   		'<td colspan="6" class="m-0 p-0">' + innerTblHdr + '</td>'+
							   	'</tr>';
				}
				
				if( inputData.pageNo == 0 ){
					$("#vldtBnkStmntTbl tbody").html( tblRows );
				}else{
					$("#vldtBnkStmntTbl tbody").append( tblRows );
				}
				
				if( response.output.eof ){
					$("#loadMoreTrans").attr("disabled", "disabled");
					$("#loadMoreTrans").addClass("disabled");
				}else{
					$("#loadMoreTrans").removeAttr("disabled");
					$("#loadMoreTrans").removeClass("disabled");
				}
				
				$("#loadMoreTrans").attr("data-page-no", parseInt(inputData.pageNo)+1);
				
				var transHdrId = $("#postCreditTrans").attr("data-trans-hdr-id");
				if( transHdrId != undefined && transHdrId != "" ){
					$("#"+transHdrId).trigger("click");
					$("#postCreditTrans").removeAttr("data-trans-hdr-id");
				}
				
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();	
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}

function updateCreditTransaction(){
	$("#crtSsnCashRcptModal .form-control, #crtSsnCashRcptModal .select2-selection").removeClass("border-danger");
	
	var trnId = $("#updtCreditTrans").attr("data-trn-id");
	var hdrId = $("#updtCreditTrans").attr("data-hdr-id");
	var upldId = $("#updtCreditTrans").attr("data-upld-id");
	var trnTypRef = $("#updtCreditTrans").attr("data-trn-typ");
	var errFlg = false;
	var ssnId = $("#savedSsnId").val();
	var brh = $("#addSsnBrh").val();
	var jrnlDt = $("#addSsnJrnlDt").val();
	var bnk = $("#addSsnBnk").val();
	var dpstExchngRt = $("#addSsnBnkExchngRt").val();
	var dpstDt = $("#addSsnDpstDt").val();
	var cry1 = $("#addSsnArCry1").val();
	var dpstExchngRt1 = $("#addSsnCrsExchngRt1").val();
	var cry2 = $("#addSsnArCry2").val();
	var dpstExchngRt2 = $("#addSsnCrsExchngRt2").val();
	var cry3 = $("#addSsnArCry3").val();
	var dpstExchngRt3 = $("#addSsnCrsExchngRt3").val();
	var dpstAmt = $("#addSsnDpstAmt").val();
	var nbrChk = $("#addSsnNbrChk").val();
	
	var invNo = $("#crInvcNo").val();
	var cusId = $("#crCusId").val();
	var cusChkAmt = $("#crCusChkAmt").val();
	var chkNo = $("#crChkNo").val();
	var desc = $("#crDesc").val();
	
	if( ssnId == "" || ssnId == null || ssnId == undefined ){
		ssnId = "";
		
		if( brh == "" ){
			errFlg = true;
			$("#addSsnBrh").next().find(".select2-selection").addClass("border-danger");
		}
		
		if( jrnlDt == "" ){
			errFlg = true;
			$("#addSsnJrnlDt").addClass("border-danger");
		}
		
		if( bnk == "" ){
			errFlg = true;
			$("#addSsnBnk").next().find(".select2-selection").addClass("border-danger");
		}
		
		if( dpstExchngRt == "" ){
			errFlg = true;
			$("#addSsnBnkExchngRt").addClass("border-danger");
		}
		
		if( dpstDt == "" ){
			errFlg = true;
			$("#addSsnDpstDt").addClass("border-danger");
		}
		
		if( cry1 == "" && cry2 == "" && cry3 == "" ){
			errFlg = true;
			$("#addSsnArCry1").next().find(".select2-selection").addClass("border-danger");
		}
		
		if( dpstExchngRt1 == "" && dpstExchngRt1 == "" && dpstExchngRt1 == "" ){
			errFlg = true;
			$("#addSsnCrsExchngRt1").addClass("border-danger");
		}
	}
	
	if( cusId == "" ){
		errFlg = true;
		$("#crCusId").next().find(".select2-selection").addClass("border-danger");
	}
	
	if( errFlg == true ){
		return;
	}
	
	$("#mainLoader").show();
			
	var inputData = {
		ssnId: ssnId,
		upld: upldId,
		hdr: hdrId,
		trn: trnId,
		brh: brh,
		jrnlDt: jrnlDt,
		bnk: bnk,
		dpstExchngRt: dpstExchngRt,
		dpstDt: dpstDt,
		cry1: cry1,
		dpstExchngRt1: dpstExchngRt1,
		cry2: cry2,
		dpstExchngRt2: dpstExchngRt2,
		cry3: cry3,
		dpstExchngRt3: dpstExchngRt3,
		dpstAmt: dpstAmt,
		nbrChk: nbrChk,
		invNo: invNo,
		cusId: cusId,
		cusChkAmt: cusChkAmt,
		chkNo: chkNo,
		desc: desc,
		trnTypRef: trnTypRef,
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/rcpt/crtssn',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.authToken == undefined || response.output.authToken == "" ){
				$("#logout").trigger("click");
			}
			
			localStorage.setItem("starOcrAuthToken", response.output.authToken);
			setAjaxHeader();
				
			if( response.output.rtnSts == 0 ){
				var msg = "Cash Receipt <strong>("+ response.output.rcptId +")</strong> has been created for session <strong>("+ response.output.ssnId +")</strong>.";
				customAlert("#popupNotify", "alert-success", msg);
				
				$("#modalSsnId").html(response.output.ssnId);
				$("#modalCashRcptNo").html(response.output.rcptId);
				$("#addSsnBrh, #addSsnBnk, #addSsnBnkExchngRt, #addSsnJrnlDt, #addSsnArCry1, #addSsnArCry2, #addSsnArCry3, #addSsnDpstDt, #addSsnCrsExchngRt1, #addSsnCrsExchngRt2, #addSsnCrsExchngRt3, #addSsnDpstAmt, #addSsnNbrChk").removeAttr("disabled");
				$("#addSsnBrh, #addSsnBnk, #addSsnArCry1, #addSsnArCry2, #addSsnArCry3").val("").trigger("change");
				$("#addSsnBnkCry, #addSsnBnkExchngRt, #addSsnJrnlDt, #addSsnDpstDt, #addSsnCrsExchngRt1, #addSsnCrsExchngRt2, #addSsnCrsExchngRt3, #addSsnDpstAmt, #addSsnNbrChk").val("");
				
				$("#crInvcNo, #crCusChkAmt, #crChkNo, #crDesc").val("");
				$("#crCusId").val("").trigger("change");
				
				$(".ssn-cash-rcpt-title").show();
				$(".cash-rcpt-actn-btns button").removeAttr("disabled");
			}else{
				$(".ssn-cash-rcpt-title").hide();
				$(".cash-rcpt-actn-btns button").attr("disabled", "disabled");
				
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#popupNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}


function postCreditTransaction(){

	/*var postSelDt = new Date($("#postDt").val());
	
	if(((new Date() - postSelDt)/ (1000 * 3600 * 24)) > 7)
	{
		customAlert("#popupNotify", "alert-danger", "The Posting date must be within 07 days of Today's date.");
		return;
	}*/

	$("#crtSsnCashRcptModal .form-control, #crtSsnCashRcptModal .select2-selection").removeClass("border-danger");
	
	var trnId = $("#postCreditTrans").attr("data-trn-id");
	var hdrId = $("#postCreditTrans").attr("data-hdr-id");
	var upldId = $("#postCreditTrans").attr("data-upld-id");
	var trnTypRef = $("#postCreditTrans").attr("data-trn-typ");
	var payAmt = $("#payAmt").val();
	var ssnId = $("#postCreditTrans").attr("data-trn-ssn");
	var rcpt = $("#postCreditTrans").attr("data-trn-rcpt");
	var bnkCode = $("#postCreditTrans").attr("data-trn-bnk");
	var postDt = $("#postDt").val();
	var jrnlDt = postDt;
	var errFlg = false;
	var invTrnDataArray = [];
	var cusId = "";
	
	if( $("#arOpenItemsTblSel .disc-amt.border-danger").length ){
		errFlg = true;
	}
	
	$("#arOpenItemsTblSel tr").each(function(index, val){
		var eachInvTrnData = {};
		if($(val).attr("data-inv-ar")!= undefined)
		{
		eachInvTrnData.arNo = $(val).attr("data-inv-ar");
		eachInvTrnData.brh = $(val).attr("data-inv-brh");
		eachInvTrnData.cus = $(val).attr("data-inv-cus");
		eachInvTrnData.amt = $(val).attr("data-inv-amt");
		eachInvTrnData.ref = $(val).attr("data-inv-ref");
		//eachInvTrnData.discAmt = $(val).attr("data-inv-disc");
		eachInvTrnData.discAmt = $(val).find(".disc-amt").val();
		eachInvTrnData.due = $(val).attr("data-inv-due");
		eachInvTrnData.discFlg = $(val).attr("data-disc-flg");
		
		invTrnDataArray.push(eachInvTrnData);
		
		cusId = $(val).attr("data-inv-cus");
		}
	});
	
	if($("#arOpenItemsTblSel tr").length == 0 || $("#arOpenItemsTblSel tbody tr > td:contains(No items selected):visible").length > 0)
	{
		var eachInvTrnData = {};
		
		eachInvTrnData.arNo = "";
		eachInvTrnData.brh = $("#crBrhId").val();
		eachInvTrnData.cus = $("#crCusId").val();
		eachInvTrnData.amt = "";
		eachInvTrnData.ref = "";
		eachInvTrnData.discAmt = ""; 
		eachInvTrnData.due = "";
		eachInvTrnData.discFlg = "N";
		invTrnDataArray.push(eachInvTrnData);
	}
	
	if( errFlg == true ){
		return;
	}
	
	$("#mainLoader").show();
			
	var inputData = {
		invTrn : invTrnDataArray,
		upld: upldId,
		hdr: hdrId,
		trn: trnId,
		payAmt: payAmt,
		ssnId: ssnId,
		rcpt: rcpt,
		bnk: bnkCode,
		post: postDt,
		jrnl: jrnlDt,
		trnTypRef: trnTypRef
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/rcpt/crtssn-mul',
		type: 'POST',
		dataType : 'json',
		success: function(response){
		
			if( response.output.rtnSts == 2 ){
				
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#popupNotify", "alert-danger", errList);
				$("#mainLoader").hide();
				return;
			}
		
			if( response.output.authToken == undefined || response.output.authToken == "" ){
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1" style="list-style: none">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				//customAlert("#popupNotify", "alert-danger", errList);
				
				stxAuthFailed(errList);
				
				$("#crtSsnCashRcptModalNw").modal("hide");
				$("#postConfirmModal").modal("hide");
				$("#mainLoader").hide();
				return;
			}
			
			localStorage.setItem("starOcrAuthToken", response.output.authToken);
			setAjaxHeader();
				
			if( response.output.rtnSts == 0 ){
				var cashRcpt = response.output.rcptId;
				var ssnId = response.output.ssnId;
				getPayments(cashRcpt);
				getTransactions();
				$(".ssn-cash-rcpt-title").show();		
				$("#modalSsnId").html(ssnId);
				$("#modalCashRcptNo").html(cashRcpt);
				$(".cash-rcpt-actn-btns button").removeAttr("disabled");
				$("#postCreditTrans").attr("data-trn-ssn", ssnId);
				$("#postCreditTrans").attr("data-trn-rcpt", cashRcpt);
				
				tblRows = '<tr class="no-rcrds"><td colspan="6" style="text-align:center;">No items selected<br> <br>Add Items that you want to clear from left.</td></tr>';
				$("#arOpenItemsTblSel tbody").html( tblRows );
				$("#postCreditTrans").attr("disabled", "disabled");
				$("#arSelCount").html(0);
				$("#crCusId").val(cusId).trigger("change");
				$("#searchCusId").trigger("click");
				$("#postConfirmModal").modal("hide");
				
				$("#glPostingOpenModal").hide();
			}else{
								
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#popupNotify", "alert-danger", errList);
			}
			
			cashRcptProofGlb = 0;
			
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}

function getPayments(refNo){
	$("#mainLoader").show();
			
	var inputData = {
		refPfx: refNo.split("-")[0],
		refNo: refNo.split("-")[1],
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData),
		url: rootAppName + '/rest/ar/read-arPymnt',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblARPymnt.length;
				var cusId = '';
				var brh = '';
				var tblRows = '';	
				for( var i=0; i<loopSize; i++ ){
					tblRows += '<tr>'+
									'<td>'+ '<span class="text-primary fs-18 d-block mb-1">' + response.output.fldTblARPymnt[i].arPfx + '-' + response.output.fldTblARPymnt[i].arNo + '</span>'+ '<span class="b-1">' + response.output.fldTblARPymnt[i].cusId + ' (' + response.output.fldTblARPymnt[i].brh + ')' + '</span></td>'+
									'<td>'+ response.output.fldTblARPymnt[i].discAmt +'</td>'+
									'<td>'+ response.output.fldTblARPymnt[i].discTknAmt +'</td>'+
									'<td>'+ response.output.fldTblARPymnt[i].dsAmt +'</td>'+
									'<td>'+ response.output.fldTblARPymnt[i].updRef +'</td>'+
								'</tr>';
								
								cusId = response.output.fldTblARPymnt[i].cusId;
				}
				$("#arPymntTbl tbody").html( tblRows );
				$("#arPayCount").html(loopSize);
				
				if(cusId == '' && response.output.customerId != undefined)
				{
					cusId = response.output.customerId;
				}
				
				brh = response.output.ssnBrh;
				
				if(loopSize > 0 || cusId.length > 0)
				{
					$("#crCusId").val(cusId).trigger("change");
					$("#crBrhId").val(brh).trigger("change");
					$("#searchCusId").trigger("click");
					$("#crCusId").attr("disabled", "disabled");
					$("#crBrhId").attr("disabled", "disabled");
					
					$("#rcptCrtd").html(response.output.crtdDt + " (" + response.output.crtdUsr + ")");
					$("#postDt").val(response.output.jrnlDt).trigger("change");
					$("#postDt").attr("disabled", "disabled");
					$("#rcptCrtdDiv").show();
				}
				else
				{
					$("#crCusId").removeAttr("disabled");
				}
				$("#cashRcptProof").html(response.output.chkProof + " " + $("#bnkCry").val());
				
				if(response.output.chkProof == 0)
				{		
					$("#cashRcptProof").removeClass("text-danger");
					$("#cashRcptProof").addClass("text-success");
				}
				else
				{		
					$("#cashRcptProof").addClass("text-danger");
					$("#cashRcptProof").removeClass("text-success");
				}
				
				$("#postCreditTrans").attr("cash-rcpt-sts", response.output.rcptSts);
				
				$("#arOpenItemsTblARPayment tbody").html( tblRows );
				
				//$("#arPymntsModal").modal("toggle");
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}


function getAdvancePayment(refNo){
	$("#mainLoader").show();
			
	var inputData = {
		refPfx: refNo.split("-")[0],
		refNo: refNo.split("-")[1],
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData),
		url: rootAppName + '/rest/ar/read-advPmt',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblAdvPmt.length;
				var tblRows = '';
				if( loopSize == 0 ){
					tblRows = '<tr><td colspan="3">No record found.</td></tr>';
				}
				for( var i=0; i<loopSize; i++ ){
					tblRows += '<tr data-ref-pfx="'+ response.output.fldTblAdvPmt[i].refPfx +'" data-ref-no="'+ response.output.fldTblAdvPmt[i].refNo +'" data-ref-itm="'+ response.output.fldTblAdvPmt[i].refItm +'">'+
									'<td>'+ response.output.fldTblAdvPmt[i].desc +'</td>'+
									'<td>'+ response.output.fldTblAdvPmt[i].advAmt +'</td>'+
									'<td>'+ response.output.fldTblAdvPmt[i].cry +'</td>'+
									'<td><i class="icon-trash menu-icon icon-pointer delete-advnc-pymnt icon-red font-weight-bold" title="Delete"></i></td>'+
								'</tr>';
				}
				$("#advncPymntTbl tbody").html( tblRows );
				$("#cashRcptProof").html(response.output.chkProof);
				if(response.output.chkProof == 0)
				{		
					$("#cashRcptProof").removeClass("text-danger");
					$("#cashRcptProof").addClass("text-success");
				}
				else
				{		
					$("#cashRcptProof").addClass("text-danger");
					$("#cashRcptProof").removeClass("text-success");
				}
				
				hideShowDelCol();
				
				$("#advncPymntModal").modal("show");
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}

function getUnappliedDebit(refNo){
	$("#mainLoader").show();
			
	var inputData = {
		refPfx: refNo.split("-")[0],
		refNo: refNo.split("-")[1],
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData),
		url: rootAppName + '/rest/ar/read-uaDebit',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblUnappliedDr.length;
				var tblRows = '';
				if( loopSize == 0 ){
					tblRows = '<tr><td colspan="3">No record found.</td></tr>';
				}
				for( var i=0; i<loopSize; i++ ){
					tblRows += '<tr data-ref-pfx="'+ response.output.fldTblUnappliedDr[i].refPfx +'" data-ref-no="'+ response.output.fldTblUnappliedDr[i].refNo +'" data-ref-itm="'+ response.output.fldTblUnappliedDr[i].refItm +'">'+
									'<td>'+ response.output.fldTblUnappliedDr[i].desc +'</td>'+
									'<td>'+ response.output.fldTblUnappliedDr[i].drAmt +'</td>'+
									'<td>'+ response.output.fldTblUnappliedDr[i].cry +'</td>'+
									'<td><i class="icon-trash menu-icon icon-pointer delete-unapld-dbt icon-red font-weight-bold" title="Delete"></i></td>'+
								'</tr>';
				}
				$("#unapldDbtTbl tbody").html( tblRows );
				$("#cashRcptProof").html(response.output.chkProof);
				if(response.output.chkProof == 0)
				{		
					$("#cashRcptProof").removeClass("text-danger");
					$("#cashRcptProof").addClass("text-success");
				}
				else
				{		
					$("#cashRcptProof").addClass("text-danger");
					$("#cashRcptProof").removeClass("text-success");
				}
				
				hideShowDelCol();
				
				$("#unapldDbtModal").modal("show");
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#popupNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}


function getGLDistribution(refNo){
	$("#mainLoader").show();
			
	var inputData = {
		refPfx: refNo.split("-")[0],
		refNo: refNo.split("-")[1],
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData),
		url: rootAppName + '/rest/ar/read-glDist',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblGLDist.length;
				var tblRows = '';
				if( loopSize == 0 ){
					tblRows = '<tr><td colspan="5">No record found.</td></tr>';
				}
				for( var i=0; i<loopSize; i++ ){
					tblRows += '<tr data-ref-pfx="'+ response.output.fldTblGLDist[i].refPfx +'" data-ref-no="'+ response.output.fldTblGLDist[i].refNo +'" data-ref-itm="'+ response.output.fldTblGLDist[i].refItm +'">'+
									'<td>'+ response.output.fldTblGLDist[i].glAcct +'</td>'+
									'<td>'+ response.output.fldTblGLDist[i].desc +'</td>'+
									'<td>'+ response.output.fldTblGLDist[i].sacct +'</td>'+
									'<td>'+ response.output.fldTblGLDist[i].rmk + '</td>'+
									'<td>'+ response.output.fldTblGLDist[i].drAmt +'</td>'+
									'<td>'+ response.output.fldTblGLDist[i].crAmt +'</td>'+
									'<td><i class="icon-trash menu-icon icon-pointer delete-gl-dist icon-red font-weight-bold" title="Delete"></i></td>'+
								'</tr>';
				}
				$("#glDistTbl tbody").html( tblRows );
				$("#cashRcptProof").html(response.output.chkProof);
				
				if(response.output.chkProof == 0)
				{		
					$("#cashRcptProof").removeClass("text-danger");
					$("#cashRcptProof").addClass("text-success");
				}
				else
				{		
					$("#cashRcptProof").addClass("text-danger");
					$("#cashRcptProof").removeClass("text-success");
				}
				
				hideShowDelCol();
				
				$("#glDistModal").modal("show");
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}


function getSessionList($select){
	$("#mainLoader").show();
	
	var inputData = {};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/common/read-ssn',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var optionList = '<option value="">New Session</option>';		
				var loopSize = response.output.fldTblSsn.length;
				if( loopSize ){			
					for( var i=0; i<loopSize; i++ ){
						optionList += '<option value="'+ response.output.fldTblSsn[i].ssnId +'">'+ response.output.fldTblSsn[i].ssnId +'</option>';
					}
				}
				
				$select.html(optionList);
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}


function getSpecificSessionData(ssnId){
	$("#mainLoader").show();
	
	var inputData = {
		ssnId: ssnId,
		crcpNo: "",
		cusId: "",
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/ar/read-ssn',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblSsn.length;
				
				if(loopSize){
					$("#addSsnBrh").val(response.output.fldTblSsn[0].brh).trigger("change");
					$("#addSsnBnk").val(response.output.fldTblSsn[0].bnk).trigger("change");
					$("#addSsnArCry1").val(response.output.fldTblSsn[0].arCry1).trigger("change");
					$("#addSsnArCry2").val(response.output.fldTblSsn[0].arCry2).trigger("change");
					$("#addSsnArCry3").val(response.output.fldTblSsn[0].arCry3).trigger("change");
					
					$("#addSsnBnkCry").val(response.output.fldTblSsn[0].depCry); 
					$("#addSsnBnkExchngRt").val(response.output.fldTblSsn[0].depExrt); 
					$("#addSsnJrnlDt").val(response.output.fldTblSsn[0].jrnlDt); 
					$("#addSsnDpstDt").val(response.output.fldTblSsn[0].depDt); 
					$("#addSsnCrsExchngRt1").val(response.output.fldTblSsn[0].arXexrt1);
					$("#addSsnCrsExchngRt2").val(response.output.fldTblSsn[0].arXexrt2);
					$("#addSsnCrsExchngRt3").val(response.output.fldTblSsn[0].arXexrt3);
					$("#addSsnDpstAmt").val(response.output.fldTblSsn[0].totDepAmt);
					$("#addSsnNbrChk").val(response.output.fldTblSsn[0].totNbrChk);
				}
				
				$("#addSsnBrh, #addSsnBnk, #addSsnBnkExchngRt, #addSsnJrnlDt, #addSsnArCry1, #addSsnArCry2, #addSsnArCry3, #addSsnDpstDt, #addSsnCrsExchngRt1, #addSsnCrsExchngRt2, #addSsnCrsExchngRt3, #addSsnDpstAmt, #addSsnNbrChk").attr("disabled", "disabled");
		
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#popupNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}

function getTypCdGlMapping(ebsId, typCd){
	$("#mainLoader").show();
	
	var inputData = {
		ebsId: ebsId,
		typCd: typCd,
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/ebsTypeCode/read-typ-cd-post',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopLimit = response.output.fldTblGlEbsMap.length;
				var tblRow = "", type = "", amtClass = "";
				var acctDesc = '';
				if( loopLimit ){
					for( var y=0; y<loopLimit; y++){
						type = ( response.output.fldTblGlEbsMap[y].trnTyp == "C" ) ? "<span class='badge badge-success'>Credit</span>" : "<span class='badge badge-warning'>Debit</span>";
						amtClass = ( response.output.fldTblGlEbsMap[y].trnTyp == "C" ) ? "credit-amt" : "debit-amt";
						
						if( response.output.fldTblGlEbsMap[y].glAcct != "" ){
						
						var crAmt = 0 ;
						var drAmt = 0;	
						var remark = '';
						var rowId = loopLimit;
						
						if(response.output.fldTblGlEbsMap[y].trnTyp == "C")
						{
							crAmt = $("#trnAmt").val();
							rowId = loopLimit  + 99;
							crAmt = crAmt.replace(/,/g, '');
						}
						else
						{
							drAmt = $("#trnAmt").val();
							rowId = loopLimit  + 100;
							drAmt = drAmt.replace(/,/g, '');
						}
						
						acctDesc = $("#glAccountAddtn option[value='" + response.output.fldTblGlEbsMap[y].glAcct + "']").attr("data-acct-desc");
						
						tblRow += '<tr id="'+ rowId +'" data-trn-typ="'+ response.output.fldTblGlEbsMap[y].trnTyp +'" data-glAcc="'+ response.output.fldTblGlEbsMap[y].glAcct +'" data-glSubAcc="'+ response.output.fldTblGlEbsMap[y].glSubAcct +'" data-drAmt="'+ drAmt +'" data-crAmt="'+ crAmt +'" data-remark="'+ remark +'">'+
						'<td><span class="text-primary fs-18 mb-1 d-block">'+ response.output.fldTblGlEbsMap[y].glAcct +'</span><span class="b-1">'+ acctDesc + '</span></td>'+
							'<td>'+  response.output.fldTblGlEbsMap[y].glSubAcct  +'</td>'+
							'<td>' + drAmt +'</td>'+
							'<td>' + crAmt+'</td>'+
							'<td>' + remark +'</td>'+
							'<td><i class="icon-note menu-icon icon-pointer edit-gl-data-addtn"></i> &nbsp; <i class="icon-close menu-icon icon-pointer delete-gl-data-addtn"></i> </td>';
						}
						if( response.output.fldTblGlEbsMap[y].glAcct1 != "" ){
							
							var rowId = loopLimit ;
							if(response.output.fldTblGlEbsMap[y].trnTyp == "C")
							{
								crAmt = $("#trnAmt").val();
								rowId = loopLimit  + 101;
								crAmt = crAmt.replace(/,/g, '');
							}
							else
							{
								drAmt = $("#trnAmt").val();
								rowId = loopLimit  + 102;
								drAmt = drAmt.replace(/,/g, '');
							}
							
							acctDesc = $("#glAccountAddtn option[value='" + response.output.fldTblGlEbsMap[y].glAcct1 + "']").attr("data-acct-desc");
							
							tblRow += '<tr id="'+ rowId +'" data-trn-typ="'+ response.output.fldTblGlEbsMap[y].trnTyp +'" data-glAcc="'+ response.output.fldTblGlEbsMap[y].glAcct1 +'" data-glSubAcc="'+ response.output.fldTblGlEbsMap[y].glSubAcct1 +'" data-drAmt="'+ drAmt +'" data-crAmt="'+ crAmt +'" data-remark="'+ remark +'">'+
							'<td><span class="text-primary fs-18 mb-1 d-block">'+ response.output.fldTblGlEbsMap[y].glAcct1 +'</span><span class="b-1">'+ acctDesc + '</span></td>'+
							'<td>'+  response.output.fldTblGlEbsMap[y].glSubAcct1 +'</td>'+
							'<td>' + drAmt +'</td>'+
							'<td>' + crAmt+'</td>'+
							'<td>' + remark +'</td>'+
							'<td><i class="icon-note menu-icon icon-pointer edit-gl-data-addtn"></i> &nbsp; <i class="icon-close menu-icon icon-pointer delete-gl-data-addtn"></i> </td>';
						}
						if( response.output.fldTblGlEbsMap[y].glAcct2 != "" ){
						
							var rowId = loopLimit ;
						
							if(response.output.fldTblGlEbsMap[y].trnTyp == "C")
							{
								crAmt = $("#trnAmt").val();
								rowId = rowId + 103;
							}
							else
							{
								drAmt = $("#trnAmt").val();
								rowId = rowId + 104;
								drAmt = drAmt.replace(/,/g, '');
							}
						
						acctDesc = $("#glAccountAddtn option[value='" + response.output.fldTblGlEbsMap[y].glAcct2 + "']").attr("data-acct-desc");
						
						tblRow += '<tr id="'+ rowId +'" data-trn-typ="'+ response.output.fldTblGlEbsMap[y].trnTyp +'" data-glAcc="'+ response.output.fldTblGlEbsMap[y].glAcct2 +'" data-glSubAcc="'+ response.output.fldTblGlEbsMap[y].glSubAcct2 +'" data-drAmt="'+ drAmt +'" data-crAmt="'+ crAmt +'" data-remark="'+ remark +'">'+
							'<td><span class="text-primary fs-18 mb-1 d-block">'+ response.output.fldTblGlEbsMap[y].glAcct2	 +'</span><span class="b-1">'+ acctDesc + '</span></td>'+
							'<td>'+  response.output.fldTblGlEbsMap[y].glSubAcct2 +'</td>'+
							'<td>' + drAmt +'</td>'+
							'<td>' + crAmt+'</td>'+
							'<td>' + remark +'</td>'+
							'<td><i class="icon-note menu-icon icon-pointer edit-gl-data-addtn"></i> &nbsp; <i class="icon-close menu-icon icon-pointer delete-gl-data-addtn"></i> </td>';
						}
					}
					$("#saveTypecodeGlPosting, .rmk-row").show();
				}else{
					tblRow += '<tr class="no-gl-row"><td colspan="4">No GL mapping found.</td></tr>';
					//$("#saveTypecodeGlPosting, .rmk-row").hide();
				}
				
				$("#saveTypecodeGlPosting, .rmk-row").show();
				$("#saveTypecodeGlPosting").attr("disabled", "disabled");
				
				$("#glMappingTbody").html(tblRow);
				$("#typCdGlPostingModal").modal("show");
				$("#mainLoader").hide();
				if( $("#glMappingTbody tr").length == 2 && $("#glMappingTbody tr[data-trn-typ='C']").length == 1 && $("#glMappingTbody tr[data-trn-typ='D']").length == 1 ){
					var amountStr = $("#trnAmt").val();
					var amount = amountStr.replace(/\,/g,"");
					$("#glMappingTbody .amount").val( amount );
					$("#glPostingProofAmt").text("0");
				}
				
				calculateProofAddtn();
			//	getGlListForMultiple( $("#glMappingTbody .gl-acct") );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#popupNotify", "alert-danger", errList);
				
				$("#mainLoader").hide();
			}
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}


function getTypCdGlMappingAP(ebsId, typCd){
	$("#mainLoader").show();
	
	var inputData = {
		ebsId: ebsId,
		typCd: typCd,
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/ebsTypeCode/read-typ-cd-post',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopLimit = response.output.fldTblGlEbsMap.length;
				var tblRow = "", type = "", amtClass = "";
				
				if( loopLimit ){
					for( var y=0; y<loopLimit; y++){
						type = ( response.output.fldTblGlEbsMap[y].trnTyp == "C" ) ? "<span class='badge badge-success'>Credit</span>" : "<span class='badge badge-warning'>Debit</span>";
						amtClass = ( response.output.fldTblGlEbsMap[y].trnTyp == "C" ) ? "credit-amt" : "debit-amt";
						
						if( response.output.fldTblGlEbsMap[y].glAcct != "" && response.output.fldTblGlEbsMap[y].trnTyp == "C" && response.output.fldTblGlEbsMap[y].posSeq == "P2"){
						
						var crAmt = $("#payAmtDebit").val() ;
						var drAmt = 0;	
						var remark = '';
							
						acctDesc = $("#glAccountAddtn option[value='" + response.output.fldTblGlEbsMap[y].glAcct + "']").attr("data-acct-desc");
							
						var tblRow = '<tr data-glAcc="'+ response.output.fldTblGlEbsMap[y].glAcct +'" data-glSubAcc="'+ response.output.fldTblGlEbsMap[y].glSubAcct +'" data-drAmt="'+ drAmt +'" data-crAmt="'+ crAmt +'" data-remark="'+ remark +'">'+
						'<td><span class="text-primary fs-18 mb-1 d-block">'+ response.output.fldTblGlEbsMap[y].glAcct +'</span><span class="b-1">'+ acctDesc + '</span></td>'+
							'<td>'+  response.output.fldTblGlEbsMap[y].glSubAcct  +'</td>'+
							'<td>' + drAmt +'</td>'+
							'<td>' + crAmt+'</td>'+
							'<td>' + remark +'</td>'+
							'<td></td>';
						}
					}
				}else{
					tblRow += '<tr class="no-gl-row"><td colspan="4" >No GL mapping found.</td></tr>';
					
				}
				
				$("#glTbody").html(tblRow);
				
				calculateProof();
				
				$("#mainLoader").hide();
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#popupNotify", "alert-danger", errList);
				
				$("#mainLoader").hide();
			}
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}

function getExistLedgerEntry(trnRef){
	$("#mainLoader").show();
	
	var inputData = {
		trnRef: trnRef,
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/bai/get-gl-data',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopLimit = response.output.fldTblGLEntry.length;
				var tblRow = "", type = "", amtClass = "", dataTrnTyp = "", amt = "";
				
				if( loopLimit ){
					for( var y=0; y<loopLimit; y++){
						
						
						var drAmt = response.output.fldTblGLEntry[y].drAmt;
						var crAmt = response.output.fldTblGLEntry[y].crAmt;
						var remark = response.output.fldTblGLEntry[y].rmk;
						
						tblRow += '<tr data-trn-typ="'+ dataTrnTyp +'" data-glAcc="'+ response.output.fldTblGLEntry[y].bscGLAcct +'" data-glSubAcc="'+ response.output.fldTblGLEntry[y].bscGLSubAcct +'" data-drAmt="'+ drAmt +'" data-crAmt="'+ crAmt +'" data-remark="'+ remark +'">'+
						'<td><span class="text-primary fs-18 mb-1 d-block">'+ response.output.fldTblGLEntry[y].bscGLAcct + '</span><span>' + response.output.fldTblGLEntry[y].bscGlAcctDesc +'</span></td>'+
							'<td>'+  response.output.fldTblGLEntry[y].bscGLSubAcct  +'</td>'+
							'<td>' + crAmt +'</td>'+
							'<td>' + drAmt +'</td>'+
							'<td>' + remark +'</td>'+
							'<td></td>';						
					}
				}
				
				$("#mainLoader").hide();
				$("#glMappingTbody").html(tblRow);
				
				$("#typCdGlPostingModal").modal("show");
				$("#typCdGlPostingModal .form-control").attr('disabled','disabled');
				
				$("#postDtOth").val(response.output.entryDt);
				$("#glPostingRmk").val(response.output.narrDesc);
				
				calculateProofAddtn();
				//getGlListForMultiple( $("#glMappingTbody .gl-acct") );
				
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#popupNotify", "alert-danger", errList);
				
				$("#mainLoader").hide();
			}
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}


function getGlListForMultiple( $select ){
	$("#mainLoader").show();
	
	var inputData = {
		//cmpyId : cmpyId,
	};
	
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/gl/read',
		type: 'POST',
		data : JSON.stringify( inputData ),
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblGL.length;
				var acctTyp = '', desc = '', glAcct = '', options = '<option value="">&nbsp;</option>';
				
				for( var i=0; i<loopSize; i++ ){
					glAcct = response.output.fldTblGL[i].glAcct.trim();
					desc = response.output.fldTblGL[i].desc.trim();
					acctTyp = response.output.fldTblGL[i].acctTyp.trim();
					
					options += '<option value="'+ glAcct +'" data-acct-typ="'+ acctTyp +'" data-acct-desc="'+ desc +'">('+ glAcct + ') ' + desc + ' (' + acctTyp +')</option>';
				}
				$select.html( options );
				$select.select2();
				
				//$("#mainLoader").hide();
				glblGlAcctCnt = 0, glblGlSubAcctCnt = 0;
				getGlSubAccts();
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
				$("#mainLoader").hide();
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}


function getExistingAPEntries(refNo){
	$("#glDebitVchrLoader").removeClass("hide-imp");
	
	var inputData = {
		trnRef : refNo,
	};
	
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/bai/read-je',
		type: 'POST',
		data : JSON.stringify( inputData ),
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopLimit = response.output.fldTblGlDist.length;
				var loopLimitVchr = response.output.fldTblVchrInfo.length;
				var tblRow = "", type = "", amtClass = "";
				var tblRowGL = "", tblRowVchr = "";
				var venId = '';
				
				if( loopLimit ){
					for( var y=0; y<loopLimit; y++){
						
						var drAmt = response.output.fldTblGlDist[y].drAmt;
						var crAmt = response.output.fldTblGlDist[y].crAmt;
						var remark = response.output.fldTblGlDist[y].remark;
								
						tblRowGL += '<tr data-glAcc="'+ response.output.fldTblGlDist[y].glAcct +'" data-glSubAcc="'+ response.output.fldTblGlDist[y].sacct +'" data-drAmt="'+ drAmt +'" data-crAmt="'+ crAmt +'" data-remark="'+ remark +'">'+
						'<td><span class="text-primary fs-18 mb-1 d-block">'+ response.output.fldTblGlDist[y].glAcct +'</span><span class="b-1">' + response.output.fldTblGlDist[y].desc + '</span></td>'+
							'<td>'+  response.output.fldTblGlDist[y].sacct  +'</td>'+
							'<td>' + drAmt +'</td>'+
							'<td>' + crAmt+'</td>'+
							'<td>' + remark +'</td>'+
							'<td></td> </tr>';
					}
				}else{
					tblRowGL += '<tr class="no-gl-row"><td colspan="4">No GL mapping found.</td></tr>';
					
				}
				
				$("#glTbody").html(tblRowGL);
				
				tblRows = '';
				if( loopLimitVchr ){
					for( var y=0; y<loopLimitVchr; y++){
						
					var vchrNo = response.output.fldTblVchrInfo[y].vchrNo;
					var ven = response.output.fldTblVchrInfo[y].vchrVenId;
					var venNm = response.output.fldTblVchrInfo[y].vchrVenId;
					var brh = response.output.fldTblVchrInfo[y].vchrBrh;
					var amt = response.output.fldTblVchrInfo[y].vchrAmtStr;
					var dueDt = response.output.fldTblVchrInfo[y].vchrDueDtStr;
					var invRef = response.output.fldTblVchrInfo[y].vchrInvNo;
					var discAmt = response.output.fldTblVchrInfo[y].discAmtStr;				
					venId = ven;
					tblRowVchr += '<tr class="sel-inv-rcrds" data-inv-vchr="'+ vchrNo + '"  data-inv-ven="'+ ven + '" data-inv-brh="'+ brh + '"  data-inv-amt="'+ amt + '" data-inv-ref="'+ invRef + '" data-inv-disc="'+ discAmt + '"  data-inv-due="'+ dueDt + '">'+
				'<td>'+ '<span class="text-primary fs-18 d-block mb-1">' + vchrNo + '</span>'+ '<span class="b-1">' + ven + '-' + venNm + ' (' + brh + ')' + '</span></td>'+
				'<td></td>'+
				'<td>'+ dueDt +'</td>'+
				'<td></td>'+
				'<td></td>'+
				'<td class="text-right">'+ amt +'</td>'+	
				'<td class="text-right">'+ discAmt +'</td>'+
				'<td class="text-right"></td>'+
				'<td>'+ response.output.fldTblVchrInfo[y].chkNo +'</td>'+
				'<td></td>'+
			'</tr>';
					}
				}else{
					tblRowVchr += '<tr><td colspan="4">No Records found.</td></tr>';
					
				}
				
				$("#crVenId").val(venId).trigger("change");
				$("#crVenId").attr("disabled", "disabled");
				$("#glDebitVchrListTbody").html(tblRowVchr);
				$("#postDtOthDebit").val(response.output.entryDt);
				$("#postDtOthDebit").attr("disabled", "disabled");
				$("#narDesc").val(response.output.narrDesc);
				$("#narDesc").attr("disabled", "disabled");
				calculateProof();
				$("#glDebitVchrLoader").addClass("hide-imp");
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
				$("#glDebitVchrLoader").addClass("hide-imp");
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#glDebitVchrLoader").addClass("hide-imp");
		}
	});
}

function getGlSubAccts(){
	$("#glMappingTbl tbody tr .gl-acct").each(function(index, glAcct){
		var $glAcct = $(glAcct);
		var dfltVal = $glAcct.attr("data-dflt-val");
		
		if( dfltVal != undefined && dfltVal != "" ){
			$glAcct.val(dfltVal).trigger("change");
			
			glblGlAcctCnt++;
			
			getGlSubAcc(dfltVal, $glAcct.closest("tr").find(".gl-sub-acct"));
			
			$glAcct.attr("data-dflt-val", "");
		}
	});
	
	calculateGlAcctAndSubAcct();
}


function getGlSubAcc( glAcct, $select ){	
	var inputData = {
		glAcct : glAcct,
	};
	
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/gl/sub-acct-read',
		type: 'POST',
		data : JSON.stringify( inputData ),
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblSubAcct.length;
				var subAcct = '', options = '<option value="">&nbsp;</option>';	
				
				var dfltOption = "", selectOption = "";
				if( $select.attr("data-selected") != undefined && $select.attr("data-selected") != "" ){
					dfltOption = $select.attr("data-selected");
					
					$select.attr("data-selected", "");
				}
				
				for( var i=0; i<loopSize; i++ ){
					selectOption = "";
				
					subAcct = response.output.fldTblSubAcct[i].subAcct.trim();
					
					if( dfltOption == subAcct ){
						selectOption = "selected";
					}
					
					options += '<option value="'+ subAcct +'" '+ selectOption +'>' + subAcct + '</option>';
				}
				$select.html( options );
				$select.removeAttr("disabled");
				$select.select2();
				
				glblGlSubAcctCnt++;
				
				calculateGlAcctAndSubAcct();
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
		}
	});
}

function getAREnquiry(){
	$("#mainLoader").show();
	
	$("thead th input[type='text']").val('');
	//$('#arOpenItemsTbl').floatThead('destroy');
	
	var refVal = $("#modalCashRcptNo").val();
	var rcptPfx = "";
	var rcptNo = "";
	
	if(refVal != "")
	{
		rcptPfx = refNo.split("-")[0];
		rcptNo = refNo.split("-")[1];
	}
	
	
	var inputData = {
		cusId: $("#crCusId").val(),
		brh: "",
		dueDt: "",
		invNo: "",
		refPfx: rcptPfx,
		refNo: rcptNo,
		cry: $("#bnkCry").val(),
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData),
		url: rootAppName + '/rest/ar/read-arEnqry',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblAREnqry.length;
				var tblRows = '', discAmt = '';	
				var inPrsCount = 0;
				for( var i=0; i<loopSize; i++ ){
				
				
					if(response.output.fldTblAREnqry[i].discAmt > 0)
					{
						discAmt = response.output.fldTblAREnqry[i].discAmtStr;
					}
					else
					{
						discAmt = '';
					}
					
					if(response.output.fldTblAREnqry[i].processFlg == true)
					{
						var clrBtn = '<button type="button" class="btn btn-sm btn-inverse-danger in-process" disabled>In Process<i class=" icon-cursor-move m-0"></button>';
						inPrsCount = inPrsCount + 1;
					}
					else
					{
						var clrBtn = '<button type="button" class="btn btn-sm btn-inverse-primary clear-trn" data-inv-ar="' + response.output.fldTblAREnqry[i].arPfx + '-' + response.output.fldTblAREnqry[i].arNo + '" data-inv-cus="' + response.output.fldTblAREnqry[i].cusId + '" data-inv-brh="' + response.output.fldTblAREnqry[i].brh + '"  data-inv-bal-amt="' + response.output.fldTblAREnqry[i].balamt + '" data-inv-due="' + response.output.fldTblAREnqry[i].dueDt + '" data-inv-ref="' + response.output.fldTblAREnqry[i].updRef + '" data-inv-disc="' + response.output.fldTblAREnqry[i].discAmt + '" data-inv-ip-amt="' + response.output.fldTblAREnqry[i].ipAmt + '"  data-disc-amt-edit-sts="' + response.output.fldTblAREnqry[i].discFlg + '" data-disc-dt="'+ response.output.fldTblAREnqry[i].discDt +'">Clear<i class="icon-arrow-right m-0"></button>';
					}
					 
				
					tblRows += '<tr data-inv-ar="' + response.output.fldTblAREnqry[i].arPfx + '-' + response.output.fldTblAREnqry[i].arNo + '"> '+
									'<td>'+ '<span class="text-primary fs-18 d-block mb-1">' + response.output.fldTblAREnqry[i].arPfx + '-' + response.output.fldTblAREnqry[i].arNo + ' </span>'+ '<span class="b-1">' + response.output.fldTblAREnqry[i].cusId + ' (' + response.output.fldTblAREnqry[i].brh + ')' + '</span></td>'+
									'<td>'+ discAmt +'</td>'+
									'<td>'+ response.output.fldTblAREnqry[i].balamtStr +'</td>'+
									'<td>'+ response.output.fldTblAREnqry[i].ipAmtStr +'</td>'+
									'<td>'+ response.output.fldTblAREnqry[i].origAmtStr +'</td>'+
									'<td>'+ response.output.fldTblAREnqry[i].desc +'</td>'+
									'<td>'+ response.output.fldTblAREnqry[i].dueDt +'</td>'+
									'<td>'+ response.output.fldTblAREnqry[i].invDt +'</td>'+	
									'<td>'+ response.output.fldTblAREnqry[i].updRef +'</td>'+
									'<td>'+ response.output.fldTblAREnqry[i].discDt +'</td>'+
									'<td>'+ clrBtn +'</td>'+
								'</tr>';
				}
				
				//$("#cashRcptProof").html(response.output.chkProof);
				
				if(loopSize == 0)
				{
					$("#arOpenItemsTbl").floatThead('destroy');
					$("#arOpenItemsTbl tbody").html("<tr><td colspan='11' style='text-align:center;'>No items available</td></tr>");
				
				}
				else
				{
					$("#arOpenItemsTbl tbody").html( tblRows );
					//fixedHeader( $('#arOpenItemsTbl') );
				}
				
				
				
				$("#arOpnInvCount").html(loopSize);
				$("#arOpnInvCountInPrs").html(inPrsCount);
				$("#srcOpnItms").val("");
				
				if($("#postCreditTrans").attr("cash-rcpt-sts") == 'C')
				{
					$("#postCreditTrans").attr("disabled", "disabled");
					$("#arOpenItemsTbl .clear-trn").attr("disabled", "disabled");
					$(".modal .add-cash-rcpt-data").hide();
					$("#modalCashRcptSts").html("<span class='badge badge-outline-success'>CLOSED</span>");
				}
				else
				{
					$(".modal .add-cash-rcpt-data").show();
					$("#arOpenItemsTbl .clear-trn").removeAttr("disabled");
					$("#modalCashRcptSts").html("<span class='badge badge-outline-warning'>OPEN</span>");
				}
				
				$("#arOpenItemsTblSel tbody").html("");
				tblRows = '<tr class="no-rcrds"><td colspan="6" style="text-align:center;">No items selected<br> <br>Add Items that you want to clear from left.</td></tr>';
				$("#arOpenItemsTblSel tbody").append( tblRows );
				$("#arSelCount").html(0); 
				
	//			$("#postCreditTrans").attr("disabled", "disabled");
				
				var payAmtValidate = parseFloat($("#payAmt").val());
				var checkValidate = parseFloat($("#memoLine").val());
				
				$("#arOpenItemsTbl tbody tr .clear-trn").each(function(index, btn){
					
					var balAmt = parseFloat($(btn).attr("data-inv-bal-amt"));
					var discAmt = parseFloat($(btn).attr("data-inv-disc"));
					var invRefVal = $(btn).attr("data-inv-ref");
					
					/*checkValidate = checkValidate.replace(/^0+/, '');*/
					
					var diff = balAmt - discAmt;
					
					diff = diff.toFixed(2);
					
					if(payAmtValidate == diff || payAmtValidate == balAmt)
					{
						if(invRefVal == checkValidate)
						{
							$(btn).trigger("click");
							return false;
							}
					}
					
				});
				
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}

function getPaymentEntries()
{
	$("#glDebitVchrListTbody").html("");
	
	$("#glDebitVchrLoader").removeClass("hide-imp");
	
	/*$("#mainLoader").show();*/
		var inputData = { 
			reqId : "",
			paySts: "A",
			venId: $("#crVenId").val(),
			vchrNo: "",
			typCd: $("#saveTypecodeGlPostingDebit").attr("data-typ-cd"),
		};
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/ebs/read-vch',
			type: 'POST',
			dataType : 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					var poStr = "", vchrNo = "", amtStr = '', vchrInvNo = '';
					var tbl = '', prevCry = '', prevVenId = '', prevVendor = '', prevPmntDt = '', prevReqId = '', venTotal = 0, prevChkNo = '';
					var tbl = '';
					var iChkFlg = 0;
					if( response.output.fldTblDoc.length ){
						var loopSize = response.output.fldTblDoc.length;
						for( var i=0; i<loopSize; i++ ){
							vchrNo = "";
							
							if(response.output.fldTblDoc[i].vchrInvNo != '')
							{
								vchrInvNo = '(' + response.output.fldTblDoc[i].vchrInvNo + ')';
							} 
							
							vchrNo = response.output.fldTblDoc[i].vchrPfx + "-" + response.output.fldTblDoc[i].vchrNo;
							
							var clrBtn = '';
							var clrBtnTotal = '';
							var clrAllBtn =  '';
							
							
							clrAllBtn = response.output.fldTblDoc[i].reqId.trim() + '-' + response.output.fldTblDoc[i].vchrVenId.trim() + '-' + response.output.fldTblDoc[i].pmntDtStr.trim();
							
							
							if(response.output.fldTblDoc[i].glEntrySts == 'N')
							{
								clrBtn = '<button type="button" class="btn btn-sm btn-inverse-primary clear-trn-debit " data-inv-vchr="' + response.output.fldTblDoc[i].vchrPfx + '-' + response.output.fldTblDoc[i].vchrNo + '" data-inv-ven="' + response.output.fldTblDoc[i].vchrVenId + '" data-inv-brh="' + response.output.fldTblDoc[i].vchrBrh + '"  data-inv-amt="' + response.output.fldTblDoc[i].vchrAmtStr + '" data-inv-due="' + response.output.fldTblDoc[i].vchrDueDtStr + '" data-inv-ref="' + response.output.fldTblDoc[i].vchrInvNo + '" data-inv-disc="' + response.output.fldTblDoc[i].discAmtStr + '"  data-inv-ven-nm="' + response.output.fldTblDoc[i].vchrVenNm + '" data-disc-flg="' + response.output.fldTblDoc[i].discFlg + '"  data-tot-inv-amt="' + response.output.fldTblDoc[i].totAmtStr + '" data-pmnt-dt="' + response.output.fldTblDoc[i].pmntDtStr + '" data-clr-all="' + clrAllBtn + '">Clear<i class="icon-arrow-right m-0"></button>';
							}
							else
							{
								clrBtn = '<button type="button" class="btn btn-sm btn-inverse-success disable-ok-w" disabled><i class=" icon-check m-0 fs-14"></button>';
							}
							
							
							if( (prevVenId != "" && prevVenId != response.output.fldTblDoc[i].vchrVenId.trim()) || (prevPmntDt != "" && prevPmntDt != response.output.fldTblDoc[i].pmntDtStr) || (prevReqId != "" && prevReqId != response.output.fldTblDoc[i].reqId.trim())){
								
								clrAllBtn = prevReqId + '-' + prevVenId + '-' + prevPmntDt;
																
								clrBtnTotal = '<button type="button" class="btn btn-sm btn-inverse-primary clear-trn-debit-total " data-clr-all="' + clrAllBtn + '">Clear<i class="icon-arrow-right m-0"></button>';
								
								if(venTotal > 0)
								{
									tbl += '<tr class="vndr-total font-weight-bold text-dark">'+
											'<td>'+ '<i class="icon-info text-primary font-weight-bold c-pointer trn-debit-info" data-clr-all="' + clrAllBtn + '"></i> ' + prevVendor +' (Total):</td>'+
											'<td>' + prevReqId + '</td>'+
											'<td></td>'+
											'<td>'+ prevPmntDt +'</td>'+
											'<td></td>'+
											'<td></td>'+
											'<td class="text-right" colspan="2">'+ venTotal.toFixed(2) +' '+ prevCry +'</td>';
											
											if(prevChkNo.length > 0)
											{
												tbl +='<td>'+ prevChkNo + '</td>';
											}
											
											tbl += '<td class="pay-total-row">' + clrBtnTotal + '</td>'+
										'</tr>';
									}
									venTotal = 0;
							}
							
							
							tbl += '<tr data-invc-no="'+ response.output.fldTblDoc[i].vchrInvNo +'" data-inv-vchr="'+ vchrNo +'" data-ctl-no="'+ response.output.fldTblDoc[i].ctlNo +'" data-chk-no="'+ response.output.fldTblDoc[i].chkNo +'">'+
										'<td><span class="text-primary fs-18 d-block mb-1">'+ vchrNo + vchrInvNo + '</span><span>' + response.output.fldTblDoc[i].vchrVenId.trim() + "-" + response.output.fldTblDoc[i].vchrVenNm.trim() + '</span>' + '(' + response.output.fldTblDoc[i].vchrBrh + ')' + '<span class="d-block mt-1"><strong>' + response.output.fldTblDoc[i].glEntry + '</strong></span></td>'+
										'<td>'+ response.output.fldTblDoc[i].vchrInvDtStr +'</td>'+
										'<td>'+ response.output.fldTblDoc[i].vchrDueDtStr +'</td>'+
										'<td>'+ response.output.fldTblDoc[i].pmntDtStr +'</td>'+
										'<td>'+ response.output.fldTblDoc[i].vchrDiscDtStr +'</td>'+
										'<td class="text-right">'+ response.output.fldTblDoc[i].vchrAmtStr +'</td>'+
										'<td class="text-right">'+ response.output.fldTblDoc[i].discAmtStr + '</td>'+
										'<td class="text-right">'+ response.output.fldTblDoc[i].totAmtStr + " " + response.output.fldTblDoc[i].vchrCry + '</td>';
										
										if(response.output.fldTblDoc[i].chkNo.length > 0)
										{
											tbl +='<td>'+ response.output.fldTblDoc[i].chkNo + '</td>';
											iChkFlg = iChkFlg + 1;
										}
										
										
										tbl +='<td>' + clrBtn +'</td>'+'</tr>';
										
							if(response.output.fldTblDoc[i].glEntrySts == 'N')
							{						
								venTotal += response.output.fldTblDoc[i].totAmt;
							}
							prevVenId = response.output.fldTblDoc[i].vchrVenId.trim();	
							prevCry = response.output.fldTblDoc[i].vchrCry;
							prevPmntDt = response.output.fldTblDoc[i].pmntDtStr.trim();
							prevVendor = response.output.fldTblDoc[i].vchrVenId.trim() + "-" + response.output.fldTblDoc[i].vchrVenNm.trim();
							prevChkNo = response.output.fldTblDoc[i].chkNo;
							prevReqId = response.output.fldTblDoc[i].reqId;
						}
						
						
						if( prevVenId != "" || prevPmntDt != "" || prevReqId != ""){
							
							clrAllBtn = prevReqId + '-' + prevVenId + '-' + prevPmntDt;
							clrBtnTotal = '<button type="button" class="btn btn-sm btn-inverse-primary clear-trn-debit-total " data-clr-all="' + clrAllBtn + '">Clear<i class="icon-arrow-right m-0"></button>';
						
							if(venTotal > 0)
								{
							tbl += '<tr class="vndr-total font-weight-bold text-dark">'+
											'<td>'+ '<i class="icon-info text-primary font-weight-bold c-pointer trn-debit-info" data-clr-all="' + clrAllBtn + '"></i> ' + prevVendor + '(Total):</td>'+
											'<td>'+ prevReqId +'</td>'+
											'<td></td>'+
											'<td>'+ prevPmntDt +'</td>'+
											'<td></td>'+
											'<td></td>'+
											'<td class="text-right" colspan="2">'+ venTotal.toFixed(2) +' '+ prevCry  +'</td>';
											if(prevChkNo.length > 0)
											{
												tbl +='<td>'+ prevChkNo + '</td>';
											}
											tbl +='<td class="pay-total-row">' + clrBtnTotal +'</td>'+
										'</tr>';
										}
									venTotal = 0;
						}
						
						if(iChkFlg > 0)
						{
							$(".show-chk-col").show();
						}
						else
						{
							$(".show-chk-col").hide();	
						}
						
						
						$("#glDebitVchrListTbody").html( tbl );
						
						$("#glDebitVchrListTbody .clear-trn-debit").hide();
						$("#glDebitVchrListTbody .pay-total-row .clear-trn-debit").show();
						
						$("#glDebitVchrListTbody tr").hide();
						$("#glDebitVchrListTbody tr.vndr-total").show();
						
						//fixedHeader( $('#glDebitVchrList') );
						
						$('#glDebitVchrList').tableSorter();
						
						autoClearSameCheckTrans();
					}
					
					
					
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
				}
				
				/*$("#mainLoader").hide();*/
				$("#glDebitVchrLoader").addClass("hide-imp");
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				/*$("#mainLoader").hide();*/
				$("#glDebitVchrLoader").addClass("hide-imp");
			}
		});
}


function calculateProofBefCashRc(trnAmt){

	var totCrAmt = 0, totDrAmt = 0, reconAmt = 0, invAmt = 0;
	var existAmt = 0;
	
	var totAmt = parseFloat(trnAmt);
	
	if($("#cashRcptProof").html().length > 0 && cashRcptProofGlb == 0)
	{
		var res = $("#cashRcptProof").html().split(" ");
				
		existAmt = parseFloat(res[0]);
		
		cashRcptProofGlb = existAmt;
	}
	
	$("#arOpenItemsTblSel tbody tr").each(function(index, val){
		
		var discVal = "0";
		
		if(!$(val).find(".disc-amt").prop('readonly'))
		{
			discVal = $(val).find(".disc-amt").val();
		}
		else
		{
			discVal = "0";
		}
		
		
		var amtValue = $(val).attr("data-inv-amt");
		
		if(amtValue != undefined)
		{
			amtValue = amtValue.replace(/,/g, '');
			discVal = discVal.replace(/,/g, '');
		
			var amt = parseFloat( amtValue - discVal );
			invAmt += parseFloat(amt);
		}
	});
	
	var proof = 0;
	
	if($("#cashRcptProof").html().length > 0)
	{
		proof = cashRcptProofGlb - invAmt;
	}
	else
	{
		proof = totAmt - invAmt;
	}
	
	$("#cashRcptProof").html(parseFloat(proof.toString()).toFixed(2) + " " + $("#bnkCry").val())
	
	if(proof == 0)
	{		
		$("#cashRcptProof").removeClass("text-danger");
		$("#cashRcptProof").addClass("text-success");
	}
	else
	{		
		$("#cashRcptProof").addClass("text-danger");
		$("#cashRcptProof").removeClass("text-success");
	}
	
}


function calculateProof(){
	
	var totCrAmt = 0, totDrAmt = 0, reconAmt = 0, invAmt = 0;
	
	$("#glTbody tr").each(function(index, val){
		
		var crAmtFld = 0, drAmtFld = 0;
		
		if(!$(val).hasClass("no-gl-row"))
		{
			if($(val).attr("data-crAmt") == '')
			{
				crAmtFld = 0;
			}
			else
			{
				crAmtFld = $(val).attr("data-crAmt");
			}
			
			if($(val).attr("data-drAmt") == '')
			{
				drAmtFld = 0;
			}
			else
			{
				drAmtFld = $(val).attr("data-drAmt");
			}
		
			var crAmt = parseFloat(crAmtFld);
			var drAmt = parseFloat(drAmtFld);
			
			totCrAmt += parseFloat(crAmt);
			totDrAmt += parseFloat(drAmt);
		}
		else
		{
			totCrAmt = parseFloat($("#payAmtDebit").val());
		}
	});
	
	$("#apOpenItemsTblSel tbody tr").each(function(index, val){
		
		var amtValue = $(val).attr("data-inv-amt");
		if(amtValue != undefined)
		{
			amtValue = amtValue.replace(/,/g, '');
		
			var amt = parseFloat( amtValue );
			invAmt += parseFloat(amt);
		}
	});
	
	totCrAmt = parseFloat(parseFloat(totCrAmt.toString()).toFixed(2));
	totDrAmt = parseFloat(parseFloat(totDrAmt.toString()).toFixed(2));
	invAmt = parseFloat(parseFloat(invAmt.toString()).toFixed(2));
	
	console.log(typeof(totCrAmt));
	console.log(typeof(totDrAmt));
	console.log(typeof(invAmt));
	console.log(totDrAmt + invAmt);
	console.log(totCrAmt - (totDrAmt + invAmt));
	
	var proof = totCrAmt - (totDrAmt + invAmt);
	proof = parseFloat(parseFloat(proof.toString()).toFixed(2));
	console.log(parseFloat(parseFloat(proof.toString()).toFixed(2)));
	
	console.log(proof);
	console.log(parseFloat(proof.toString()));
	$("#glPostingProofAmtAP").html( parseFloat(proof.toString()).toFixed(2) + " " + $("#bnkCryAP").val());
	
	if(proof == 0)
	{
		$("#saveTypecodeGlPostingDebit").removeAttr("disabled");
		
		$(".ap-bal-val").removeClass("text-danger");
		$(".ap-bal-val").addClass("text-success");
	}
	else
	{
		$("#saveTypecodeGlPostingDebit").attr("disabled", "disabled");
		
		$(".ap-bal-val").addClass("text-danger");
		$(".ap-bal-val").removeClass("text-success");
	}
	
	
	if($("#modalJENo").html() != '')
	{
		$("#saveTypecodeGlPostingDebit").attr("disabled", "disabled");
		$(".ap-bal-val").removeClass("text-danger");
		$(".ap-bal-val").addClass("text-success");
	}
	
}


function calculateProofAddtn(){
	
	var totCrAmt = 0, totDrAmt = 0, reconAmt = 0, invAmt = 0;
	
	$("#glMappingTbody tr").each(function(index, val){
		
		var crAmt  = 0;
		var drAmt = 0;
		
		if( $(val).attr("data-crAmt") != '' && $(val).attr("data-crAmt") != undefined)
		{
			crAmt = parseFloat( $(val).attr("data-crAmt") );
		}
		
		if($(val).attr("data-drAmt") != '' && $(val).attr("data-drAmt") != undefined)
		{
			drAmt = parseFloat( $(val).attr("data-drAmt"));
		}
		
		totCrAmt += parseFloat(crAmt);
		totDrAmt += parseFloat(drAmt);
	});
	
	totCrAmt = parseFloat(parseFloat(totCrAmt.toString()).toFixed(2));
	totDrAmt = parseFloat(parseFloat(totDrAmt.toString()).toFixed(2));
	

	
	var proof = totCrAmt - totDrAmt;
	proof = parseFloat(parseFloat(proof.toString()).toFixed(2));
	
	if(totCrAmt == 0 && totDrAmt == 0)
	{
		$("#glPostingProofAmt").html("");
	}
	else
	{
		$("#glPostingProofAmt").html( parseFloat(proof.toString()).toFixed(2) + " " + $("#bnkCryAddtn").val());
	}
	
	if(proof == 0)
	{
		$("#saveTypecodeGlPosting").removeAttr("disabled");
		
		$(".gl-proof").removeClass("text-danger");
		$(".gl-proof").addClass("text-success");
	}
	else
	{
		$("#saveTypecodeGlPosting").attr("disabled", "disabled");
		
		$(".gl-proof").addClass("text-danger");
		$(".gl-proof").removeClass("text-success");
	}
	
	if(totCrAmt == 0 && totDrAmt == 0)
	{
		$("#saveTypecodeGlPosting").attr("disabled", "disabled");
		
		$(".gl-proof").addClass("text-danger");
		$(".gl-proof").removeClass("text-success");
	}
	
	if($("#trnGlRefernce").html() != '')
	{
		$("#saveTypecodeGlPosting").attr("disabled", "disabled");
		$(".gl-proof").removeClass("text-danger");
		$(".gl-proof").addClass("text-success");
	}
}

function calculateGlAcctAndSubAcct(){
	if(glblGlAcctCnt == glblGlSubAcctCnt){
		$("#mainLoader").hide();
		
		$("#glMappingTbody .gl-acct").addClass("custom-select2");
	}
}

function hideShowDelCol(){
	if( $("#modalCashRcptSts .badge").text().toLowerCase() == "closed" ){
		$("#glDistTbl tr th:last-child, #glDistTbl tr td:last-child").hide();
		$("#unapldDbtTbl tr th:last-child, #unapldDbtTbl tr td:last-child").hide();
		$("#advncPymntTbl tr th:last-child, #advncPymntTbl tr td:last-child").hide();
	}else{
		$("#glDistTbl tr th:last-child, #glDistTbl tr td:last-child").show();
		$("#unapldDbtTbl tr th:last-child, #unapldDbtTbl tr td:last-child").show();
		$("#advncPymntTbl tr th:last-child, #advncPymntTbl tr td:last-child").show();
	}
}


function autoClearSameCheckTrans(){
	var checkNo = $("#bnkRefOthDebit").val();
	var transChkNo = "";
	var $row = null;
	
	$("#glDebitVchrListTbody tr").each(function(index, row){
		$row = $(row);
		transChkNo = $row.attr("data-chk-no");
		
		if(transChkNo != undefined && transChkNo != "")
		{
			if( checkNo == transChkNo ){
				$row.find(".clear-trn-debit").trigger("click");
			}
		}
	});
}