$(function(){
	
	getEbsList( $("#ebsId") );
	
	getPostingMapping();
	
	$("#ebsId").change(function(){
		$("#mainNotify").html("");
		
		var ebsId = $(this).val();

		if( ebsId != "" ){			
			getTypeCodes( ebsId );
		}else{
			$("#typCdMapTbl tbody").html( "" );
			$("#typCdMapSection").slideUp();
		}
	});
	
	$("#saveTypCdMap").click(function(){
		$("#mainLoader").show();
		
		var mapArr = [];
		
		$("#typCdMapTbl tbody tr").each(function(index, row){
			var $row = $(row);
			var typCd = $row.find(".type-code").val();
			var pstng1 = $row.find(".posting-1").val();
			var pstng2 = $row.find(".posting-2").val();
			var ebsId = $("#ebsId").val();
			
			var mapObj = {
				ebsId: ebsId,
				typCd: typCd,
				pstng1: pstng1,
				pstng2: pstng2,
			};
			
			mapArr.push(mapObj);
		});

		var inputData = {
			mapArr: mapArr,
		};
			
		$.ajax({
			headers: ajaxHeader,
			url: rootAppName + '/rest/ebsTypeCode/addGlEbsMap',
			type: 'POST',
			data : JSON.stringify( inputData ),
			dataType: 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					customAlert("#mainNotify", "alert-success", "Type code mapping saved succcessfully.");
					//$("#clearTypCdMap").trigger("click");
					//getGlEbsMapping();
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
				}			
				$("#mainLoader").hide();
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});
	});
	
	$("#clearTypCdMap").click(function(){
		$("#ebsId").val("").trigger("change");
		$("#typCdMapTbl tbody").html( "" );
		$("#typCdMapSection").slideUp();
	});
	
	$(".view-posting-info").click(function(){
		$("#postingInfoModal").modal("show");
	});
	
	$("#fltrPosting").keyup(function(){
		var fltrVal = $(this).val().toLowerCase();
		$("#glMapTbl tbody tr").hide();
		
		$("#glMapTbl tbody tr").each(function(index, row){
			var $row = $(row);
			
			if( $row.text().toLowerCase().indexOf(fltrVal) > -1 ){
				$row.show();
			}
		});
	});
	
	/*$("#glEbsMapTbl").on("click", ".delete-mapping", function(){
		$("#glEbsMapTbl tbody tr").removeClass("delete-this-mapping");
		$(this).closest("tr").addClass("delete-this-mapping");
		$("#deleteMapModal").modal("show");
	});
	
	$("#deleteMap").click(function(){
		$("#mainLoader").show();
		
		var $tr = $("#glEbsMapTbl tbody tr.delete-this-mapping");
		var ebsId = $tr.attr("data-ebs-id");
		var typCd = $tr.attr("data-typ-cd");
		var glAcct = $tr.attr("data-gl-acct");
		
		var inputData = {
			ebsId: ebsId,
			typCd: typCd,
			glAcct: glAcct
		};
		
		$.ajax({
			headers: ajaxHeader,
			url: rootAppName + '/rest/ebsTypeCode/deleteGlEbsMap',
			type: 'POST',
			data : JSON.stringify( inputData ),
			dataType: 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					customAlert("#mainNotify", "alert-success", "Type code mapping deleted succcessfully.");
					$("#deleteMapModal").modal("hide");
					getGlEbsMapping();
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
					$("#mainLoader").hide();
				}			
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});
	});*/

});


function getTypeCodes( ebsId ){
	$("#mainLoader").show();
	
	var inputData = {
		ebsId : ebsId,
	};
	
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/ebsTypeCode/readMap',
		type: 'POST',
		data : JSON.stringify( inputData ),
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblGlEbsMap.length;
				var tblRow = '';
				if(loopSize){
					for( var i=0; i<loopSize; i++ ){
						typCd = response.output.fldTblGlEbsMap[i].typCd.trim();
						typNm = response.output.fldTblGlEbsMap[i].typNm.trim();
						
						tblRow += '<tr>'+
										'<td class="font-weight-bold"><input type="hidden" class="type-code" value="'+ typCd +'" />'+ typCd + '-' + typNm +'</td>'+
										'<td><select class="form-control select2 posting-1 posting" style="width: 100%" data-selected="'+ response.output.fldTblGlEbsMap[i].post1.trim() +'"></select></td>'+
										'<td><select class="form-control select2 posting-2 posting" style="width: 100%" data-selected="'+ response.output.fldTblGlEbsMap[i].post2.trim() +'"></select></td>'+
									'</tr>';
					}
					$("#typCdMapTbl tbody").html( tblRow );
					$("#typCdMapSection").slideDown();
					
					getGlMapping();
				}else{
					$("#typCdMapTbl tbody").html( "" );
					$("#typCdMapSection").slideUp();
			
					customAlert("#mainNotify", "alert-warning", "No type code found for selected file format.");
					$("#mainLoader").hide();
				}
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
				$("#mainLoader").hide();
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
	
}

/*function getGlEbsMapping(){
	$("#mainLoader").show();
	
	var inputData = {};
	
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/ebsTypeCode/readMap',
		type: 'POST',
		data : JSON.stringify( inputData ),
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblGlEbsMap.length;
				var ebsId = '', ebsNm = '', typCd = '', typNm = '', glAcct = '', glAcctDesc = '', tblRows = '', glSubAcct = '', trnType = '';	
				for( var i=0; i<loopSize; i++ ){
					ebsId = response.output.fldTblGlEbsMap[i].ebsId.trim();
					ebsNm = response.output.fldTblGlEbsMap[i].ebsNm.trim();
					typCd = response.output.fldTblGlEbsMap[i].typCd.trim();
					typNm = response.output.fldTblGlEbsMap[i].typNm.trim();
					glAcct = response.output.fldTblGlEbsMap[i].glAcct.trim();
					glAcctDesc = response.output.fldTblGlEbsMap[i].glAcctDesc.trim();
					glSubAcct = response.output.fldTblGlEbsMap[i].subAcct.trim();
					trnType = response.output.fldTblGlEbsMap[i].trnType.trim();
					
					if(trnType == 'C')
					{
						trnType = '<span class="badge badge-success">Credit</span>';
					}
					
					if(trnType == 'D')
					{
						trnType = '<span class="badge badge-warning">Debit</span>';;
					}
					
					tblRows += '<tr data-ebs-id="'+ ebsId +'" data-typ-cd="'+ typCd +'" data-gl-acct="'+ glAcct +'">'+
									'<td>'+ (i+1) +'</td>'+
									'<td>'+ ebsNm +'</td>'+
									'<td>('+ typCd +') '+ typNm +'</td>'+
									'<td>('+ glAcct +') '+ glAcctDesc +'</td>'+
									'<td>'+ glSubAcct +'</td>'+
									'<td>'+ trnType +'</td>'+
									'<td><i class="icon-trash menu-icon icon-pointer delete-mapping icon-red font-weight-bold" title="Delete"></i></td>'+
								'</tr>';
				}
				$("#glEbsMapTbl tbody").html( tblRows );
				
				$("#mainLoader").hide();
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
				$("#mainLoader").hide();
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}*/



function getGlMapping(){
	$("#mainLoader").show();
	
	var inputData = {};
	
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/gl/get-acct-post',
		type: 'POST',
		data : JSON.stringify( inputData ),
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblGLPosting.length;
				var option = '<option value="">&nbsp;</option>';
				for( var i=0; i<loopSize; i++ ){
					posId = response.output.fldTblGLPosting[i].postId.trim();
					
					option += '<option value="'+ posId +'">'+ posId +'</option>';
				}
				$("select.posting").html( option );
				
				$(".select2").select2();
				
				$("select.posting").each(function(index, select){
					var $posting = $(select);
					var selected = $posting.attr("data-selected");
					
					if( selected != undefined && selected != "" ){
						$posting.val(selected).trigger("change");
					}
				});
				
				
				
				$("#mainLoader").hide();
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
				$("#mainLoader").hide();
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}


function getPostingMapping(){
	//$("#mainLoader").show();
	
	var inputData = {};
	
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/gl/get-acct-post',
		type: 'POST',
		data : JSON.stringify( inputData ),
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblGLPosting.length;
				var tblRows = '', posId = '', crAcct = '', crAcctDesc = '', crSubAcct = '';
				var drAcct = '', drAcctDesc = '', drSubAcct = '';
				for( var i=0; i<loopSize; i++ ){
					
					posId = response.output.fldTblGLPosting[i].postId.trim();
					crAcct1 = response.output.fldTblGLPosting[i].crAcct1.trim();
					crAcctDesc1 = response.output.fldTblGLPosting[i].crAcct1Desc.trim();
					crSubAcct1 = response.output.fldTblGLPosting[i].crSubAcct1.trim();
					
					crAcct2 = response.output.fldTblGLPosting[i].crAcct2.trim();
					crAcctDesc2 = response.output.fldTblGLPosting[i].crAcct2Desc.trim();
					crSubAcct2 = response.output.fldTblGLPosting[i].crSubAcct2.trim();
					
					crAcct3 = response.output.fldTblGLPosting[i].crAcct3.trim();
					crAcctDesc3 = response.output.fldTblGLPosting[i].crAcct3Desc.trim();
					crSubAcct3 = response.output.fldTblGLPosting[i].crSubAcct3.trim();
					
					drAcct1 = response.output.fldTblGLPosting[i].drAcct1.trim();
					drAcctDesc1 = response.output.fldTblGLPosting[i].drAcct1Desc.trim();
					drSubAcct1 = response.output.fldTblGLPosting[i].drSubAcct1.trim();
					
					drAcct2 = response.output.fldTblGLPosting[i].drAcct2.trim();
					drAcctDesc2 = response.output.fldTblGLPosting[i].drAcct2Desc.trim();
					drSubAcct2 = response.output.fldTblGLPosting[i].drSubAcct2.trim();
					
					drAcct3 = response.output.fldTblGLPosting[i].drAcct3.trim();
					drAcctDesc3 = response.output.fldTblGLPosting[i].drAcct3Desc.trim();
					drSubAcct3 = response.output.fldTblGLPosting[i].drSubAcct3.trim();
					
					if(crAcct1.length > 0)
					{
						crAcct1 = '('+ crAcct1 +') '+ crAcctDesc1;
					}
					
					if(crAcct2.length > 0)
					{
						crAcct2 = '('+ crAcct2 +') '+ crAcctDesc2;
					}
					
					if(crAcct3.length > 0)
					{
						crAcct3 = '('+ crAcct3 +') '+ crAcctDesc3;
					}
					
					if(drAcct1.length > 0)
					{
						drAcct1 = '('+ drAcct1 +') '+ drAcctDesc1;
					}
					
					if(drAcct2.length > 0)
					{
						drAcct2 = '('+ drAcct2 +') '+ drAcctDesc2;
					}
					
					if(drAcct3.length > 0)
					{
						drAcct3 = '('+ drAcct3 +') '+ drAcctDesc3;
					}
					
					tblRows += '<tr data-pos-id="' + posId + '">'+
									'<td>'+ posId +'</td>'+
									'<td>'+ crAcct1  +'</td>'+
									'<td>'+ crSubAcct1 +'</td>'+
									'<td>'+ crAcct2  +'</td>'+
									'<td>'+ crSubAcct2 +'</td>'+
									'<td>'+ crAcct3  +'</td>'+
									'<td>'+ crSubAcct3 +'</td>'+
									'<td>' + drAcct1 +'</td>'+
									'<td>'+ drSubAcct1 +'</td>'+
									'<td>' + drAcct2 +'</td>'+
									'<td>'+ drSubAcct2 +'</td>'+
									'<td>' + drAcct3 +'</td>'+
									'<td>'+ drSubAcct3 +'</td>'+
								'</tr>';
				}
				$("#glMapTbl tbody").html( tblRows );
				
				//$("#mainLoader").hide();
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
				//$("#mainLoader").hide();
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			//$("#mainLoader").hide();
		}
	});
}