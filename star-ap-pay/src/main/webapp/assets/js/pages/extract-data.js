var cropper;
var cropBoxDataJson;
var dataRowNum = 1;

$(function(){
	dataRowNum = 1;
	
	getVendorList( $("#vendorIdListFld"), glblCmpyId );

	$("#customFile").val("");
	$("#pdfCanvasColWrapper").hide();
	//$("#docFieldsRow").html("");
	
	//addExtractDataRow();
		
	$("#saveExtractData").click(function(){
		saveDoc();
		
	});
	
	function saveDoc(){
	$("#mainLoader").show();
	
	var ajaxUrl = rootAppName + "/rest/ocr/save";
	
	var inputData = {
		vendId : $("#vendorIdListFld").val(),
		vendNm : $("#docVendorNmFld").val(),
		vendNmStx : "",
		invNo : $("#invNoFld").val(),
		invDt : $("#invDtFld").val(),
		invAmt: $("#invAmtFld").val(),
		invCry: $("#invCryFld").val()
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: ajaxUrl,
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				$("#customFile").val("");
				$("#pdfCanvasColWrapper").hide();
				
				$("#extractData .form-control").val("");
				$("#vendorIdListFld").val("").trigger("change");
				
				customAlert("#mainNotify", "alert-success", "Data saved successfully.");
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				
				if( inputData.paySts == "S" ){
					customAlert("#sendToAprvErr", "alert-danger", errList);
				}else{
					customAlert("#mainNotify", "alert-danger", errList);
				}
			}
			
			
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}
	$("#getVenList").click(function(){
		getMappedVendorList("", "L");
		
	});
	
	$("#vendorIdListFld").change(function(){
	
		var venId = $(this).val();
		
		if(venId != '' && venId != null && venId != undefined)
		getMappedVendorList(venId, "F");
	
	});
	
	$("#cancelExtractData").click(function(){
		
		$("#customFile").val("");
		$("#pdfCanvasColWrapper").hide();
		$("#extractData .form-control").val("");
		$("#vendorIdListFld").val("").trigger("change");
		
		//$("#docFieldsRow").html("");
		
		//addExtractDataRow();
	});
	
	$("#customFile").change(function(){
		if( cropper != undefined ){
			$("#pdfCanvasWrapper .highlight-area").remove();
			cropper.destroy();
		}
		
		var uploadFile = this.files[0];
		uploadFileUrl = URL.createObjectURL(uploadFile);
		//$("#userImage").attr("src", uploadFileUrl);
		
		loadEditDocument(uploadFileUrl);
	});
	
	$("#addExtractDataRow").click(function(){
		addExtractDataRow();
	});
	
	$("#getImgData").click(function(){
		
		if( $("#docFieldsRow .extract-data-val.selected-field").length == 0 ){
			/*swal({
				title: "Warning",
				text: "Please select any field.",
				type: 'error',
			});*/
			
			customAlert("#mainNotify", "alert-warning", "Please select any field.");
			
			return;
		}
		
		document.getElementById('croppedImage').src = cropper.getCroppedCanvas().toDataURL('image/jpeg');
		
		var croppedArea = document.createElement('div'); // is a node
		croppedArea.className = "highlight-area";
		croppedArea.style.background = "rgba(33, 150, 243, 0.4)";
		croppedArea.style.position = "absolute";
		croppedArea.style.width = cropBoxDataJson.width + "px";
		croppedArea.style.height = cropBoxDataJson.height + "px";
		croppedArea.style.left = cropBoxDataJson.left + "px";
		croppedArea.style.top = cropBoxDataJson.top + "px";
		
		var imageWrapper = document.getElementById('pdfCanvasWrapper');
		
		imageWrapper.appendChild(croppedArea);
		
		callOCRSvc();
	});
	
	$("#docFieldsRow").on("click", "input.form-control", function(){
		$("#docFieldsRow input.form-control").removeClass("selected-field");
		$(this).addClass("selected-field");
	});
	
	
	$("#mappedVenTbl").on("click", ".delete-invc-map", function(){
		$("#mappedVenTbl tbody tr").removeClass("delete-me");
		$(this).closest("tr").addClass("delete-me");
		var venId = $(this).closest("tr").attr("data-ven-id");
		
		$("#deleteInvcMap").attr("data-ven-id", venId);
		
		$("#deleteInvcMapModal").modal("show");
	});
	
	$("#deleteInvcMapBtn").click(function(){
		var venId = $("#deleteInvcMap").attr("data-ven-id");
			
		$("#mainLoader").show();
		
		var inputData = { 
			venId : venId,
		};
		
		var ajaxUrl = '';
		ajaxUrl = rootAppName + '/rest/ocr/del';	
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: ajaxUrl,
			type: 'POST',
			dataType : 'json',
			success: function(response){
				
				if( response.output.rtnSts == 0 ){
					$("#deleteInvcMapModal").modal("hide");
					getMappedVendorList("", "L");
					customAlert("#popupNotify", "alert-success", "Mapping deleted successfully.");
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#popupNotify", "alert-danger", errList);
				}
				
				$("#mainLoader").hide();
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});
	});
	
});


function getMappedVendorList(venId, flg){
	$("#mainLoader").show();
	
	var ajaxUrl = rootAppName + "/rest/ocr/read-ven";
	
	var inputData = {
		vendId : venId
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: ajaxUrl,
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
			
				deleteBtn = '<i class="icon-trash menu-icon icon-pointer delete-invc-map icon-red" data-toggle="tooltip" title="Delete"></i>';
			
					var tbl = '';
					if( response.output.fldTblVenList.length ){
						var loopSize = response.output.fldTblVenList.length;
						if(flg == 'L')
						{
						for( var i=0; i<loopSize; i++ ){
						
							tbl += '<tr data-ven-id='+ response.output.fldTblVenList[i].venId.trim() +'>'+
										'<td>'+ (i+1) +'</td>'+
										'<td>'+ response.output.fldTblVenList[i].venId.trim() + '</td>'+
										'<td>'+ response.output.fldTblVenList[i].venNm +'</td>'+
										'<td>'+ response.output.fldTblVenList[i].venInvNoFld +'</td>'+
										'<td>'+ response.output.fldTblVenList[i].venInvDtFld +'</td>'+
										'<td>'+ response.output.fldTblVenList[i].venAmtFld +'</td>'+
										'<td>'+ response.output.fldTblVenList[i].venCryFld + '</td>'+
										'<td>'+ deleteBtn + '</td>'+
									'</tr>';
						}
						}
						if(flg == 'F' && loopSize > 0)
						{
							$("#docVendorNmFld").val(response.output.fldTblVenList[0].venNm),
							$("#invNoFld").val(response.output.fldTblVenList[0].venInvNoFld),
							$("#invDtFld").val(response.output.fldTblVenList[0].venInvDtFld),
							$("#invAmtFld").val(response.output.fldTblVenList[0].venAmtFld ),
							$("#invCryFld").val(response.output.fldTblVenList[0].venCryFld)
						}
					}
					
					if(flg == 'L')
					{
						if( tbl != "" ){
							$("#mappedVendList table tbody").html(tbl);
							$("#mappedVendList").show();
						}else{
							$("#mappedVendList table tbody").html("");
							$("#mappedVendList").hide();
						}
				
						$("#viewMapVend").modal("show");
					}
				
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}	

function loadEditDocument( fileUrl="" ){
	
	if( fileUrl.length <= 0 || fileUrl == "" ){
		return;
	}
	
	$("#mainLoader").show();
	// If absolute URL from the remote server is provided, configure the CORS
	// header on that server.
	//var url = 'https://raw.githubusercontent.com/mozilla/pdf.js/ba2edeae/examples/learning/helloworld.pdf';

	// Loaded via <script> tag, create shortcut to access PDF.js exports.
	var pdfjsLib = window['pdfjs-dist/build/pdf'];

	// The workerSrc property shall be specified.
	//pdfjsLib.GlobalWorkerOptions.workerSrc = 'https://mozilla.github.io/pdf.js/build/pdf.worker.js';
	pdfjsLib.GlobalWorkerOptions.workerSrc = 'js/pdf.worker.js';
	
	var url = fileUrl;
	
	console.log( url );

	// Asynchronous download of PDF
	var loadingTask = pdfjsLib.getDocument(url);
	loadingTask.promise.then(function(pdf) {
		console.log('PDF loaded');

		// Fetch the first page
		var pageNumber = 1;
		pdf.getPage(pageNumber).then(function(page) {
			console.log('Page loaded');
			
			var scale = 1.5;
			var viewport = page.getViewport({scale: scale});
			
			//var viewport = page.getViewport(1);

			// Prepare canvas using PDF page dimensions
			var canvas = document.getElementById('the-canvas');
			var context = canvas.getContext('2d');
			canvas.height = viewport.height;
			canvas.width = viewport.width;

			// Render PDF page into canvas context
			var renderContext = {
				canvasContext: context,
				viewport: viewport
			};
			var renderTask = page.render(renderContext);
			renderTask.promise.then(function () {
				console.log('Page rendered');
				$("#pdfCanvasColWrapper").slideDown();
				initializeCropper();
				
				$("#mainLoader").hide();
				
			});
		});
	}, function (reason) {
		// PDF loading error
		console.error(reason);
		$("#mainLoader").hide();
	});
}


function initializeCropper(){
	var image = document.querySelector('#the-canvas');
	var data = document.querySelector('#data');
	var cropBoxData = document.querySelector('#cropBoxData');
	cropper = new Cropper(image, {
		autoCrop: false,
		modal: false,
		ready: function (event) {
			// Zoom the image to its natural size
			cropper.zoomTo(1);
		},

		crop: function (event) {
			data.textContent = JSON.stringify(cropper.getData());
			cropBoxData.textContent = JSON.stringify(cropper.getCropBoxData());
			cropBoxDataJson = cropper.getCropBoxData();
		},

		zoom: function (event) {
			// Keep the image in its natural size
			if (event.detail.oldRatio === 1) {
				event.preventDefault();
			}
		},
	});
}


function callOCRSvc(){
	$("#mainLoader").show();
	console.log("Cropped Image Height: " + parseInt($("#croppedImage").css("width")));
	if( parseInt($("#croppedImage").css("width")) > 0 ){
		var myImage = document.getElementById('croppedImage');
		
		var d1 = new Date();
		var inputData = {
			imgValue : $("#croppedImage").attr("src")
		}
		
		$.ajax({
			headers : ajaxHeader,
			data : JSON.stringify( inputData ),
			url : rootAppName + '/rest/ocr/recognize',
			type : 'POST',
			dataType : 'json',
			success : function(data) {
				console.log(data);
				if( $("#docFieldsRow .extract-data-val.selected-field").length > 0 ){
					$("#docFieldsRow .extract-data-val.selected-field").val( data.responseText );
				}
				
				$("#croppedImage").attr("src", "");
				
				$("#mainLoader").hide();
			},
			error : function(data) {
				console.log(data);
				if( $("#docFieldsRow .extract-data-val.selected-field").length > 0 ){
					$("#docFieldsRow .extract-data-val.selected-field").val( data.responseText );
				}
				
				$("#croppedImage").attr("src", "");
				
				$("#mainLoader").hide();
			}
		});
		
	}else{
		setTimeout(function(){
			callOCRSvc();
		}, 500)
	}
}

function callOCR(){
	if( $("#docFieldsRow .extract-data-val.selected-field").attr("data-row-num") == 1 ){
		$("#docFieldsRow .extract-data-val.selected-field").val( "6877" );
		$("#croppedImage").attr("src", "");
		return;
	}else if( $("#docFieldsRow .extract-data-val.selected-field").attr("data-row-num") == 2 ){
		$("#docFieldsRow .extract-data-val.selected-field").val( "8/15/2019" );
		$("#croppedImage").attr("src", "");
		return;
	}else if( $("#docFieldsRow .extract-data-val.selected-field").attr("data-row-num") == 3 ){
		$("#docFieldsRow .extract-data-val.selected-field").val( "585" );
		$("#croppedImage").attr("src", "");
		return;
	}else{
		return;
	}
	
	
	$("#mainLoader").show();
	console.log("Cropped Image Height: " + parseInt($("#croppedImage").css("width")));
	if( parseInt($("#croppedImage").css("width")) > 0 ){
		var myImage = document.getElementById('croppedImage');

		/*Tesseract.recognize(myImage).then(function(result){
			//$("#scannedValues").append('<input type="text" class="form-control mb-3" value="'+ result.text +'"/>');
			
			if( $("#docFieldsRow .extract-data-val.selected-field").length > 0 ){
				$("#docFieldsRow .extract-data-val.selected-field").val( result.text );
			}
				
			$("#croppedImage").attr("src", "");

			$("#mainLoader").hide();
		});*/
		
		var d1 = new Date();
		var inputData = {
			imgValue : $("#croppedImage").attr("src")
		}
		
		var d = new Date();
		var n = d.getTime();
		var reportName = "cstm_get_text_from_bytes_" + n + ".dat";
		
		$.ajax({
			data : {
				imageBytes: inputData.imgValue,
				extn: (inputData.imgValue.split(";")[0]).split("/")[1],
				rptName: reportName
			},
			url : 'getTextFromBytes.jsp',
			type : 'POST',
			dataType : 'json',
			success : function(data) {
				console.log(data);
				var imageText = data.text.slice(3);
				if( $("#docFieldsRow .extract-data-val.selected-field").length > 0 ){
					$("#docFieldsRow .extract-data-val.selected-field").val( imageText );
				}
				
				$("#croppedImage").attr("src", "");
				
				$("#mainLoader").hide();
			},
			error : function(data) {
				console.log(data);
				if( $("#docFieldsRow .extract-data-val.selected-field").length > 0 ){
					$("#docFieldsRow .extract-data-val.selected-field").val( data.responseText );
				}
				
				$("#croppedImage").attr("src", "");
				
				$("#mainLoader").hide();
			}
		});
		
	}else{
		setTimeout(function(){
			callOCR();
		}, 500)
	}
}


function addExtractDataRow(){
	var newRow = '<div class="form-group">'+
					'<div class="row">'+
						'<div class="col-md-4">'+
							'<select class="form-control extract-data-name select2" width="style:100%">'+
								'<option value="">Select</option>'+
								'<option value="AMT">Amount</option>'+
								'<option value="BRH">Branch</option>'+
								'<option value="CRY">Currency</option>'+
								'<option value="DUE-DT">Due Date</option>'+
								'<option value="INVC-DT">Invoice Date</option>'+
								'<option value="INVC-NO">Invoice No</option>'+
								'<option value="PYMNT-TRM">Payment Terms</option>'+
								'<option value="PO-NO">PO Number</option>'+
								'<option value="VNDR-ID">Vendor ID</option>'+
								'<option value="VNDR-NM">Vendor Name</option>'+
							'</select>'+
						'</div>'+
						'<div class="col-md-8">'+
							'<input type="text" class="form-control extract-data-val" data-row-num="'+ dataRowNum +'"/>'+
						'</div>'+
					'</div>'+
				'</div>';

	$("#docFieldsRow").append( newRow );
	
	$(".select2").select2();
	
	dataRowNum++;
}