$(function(){
	
	getConfiguredEmails();
	
	$("#easSave").click(function(){
		$("#emailSection .select2-selection, #emailSection .form-control").removeClass("border-danger");
		
		var errFlg = false;
		var easEmailType = $("#easEmailType").val();
		var easEmailId = $("#easEmailId").val().trim();
		var easPassword = $("#easPassword").val().trim();
		var email_reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		
		if( easEmailType == "" ){
			errFlg = true;
			$("#easEmailType").next().find(".select2-selection").addClass("border-danger");
		}
		
		if( easEmailId == "" ){
			errFlg = true;
			$("#easEmailId").addClass("border-danger");
		}else if (!email_reg.test(easEmailId)) {
	        errFlg = true;
	        $('#easEmailId').addClass("border-danger");
	    }
	    
	    if( easPassword == "" ){
			errFlg = true;
			$("#easPassword").addClass("border-danger");
		}
		
		if( errFlg == false ){
			$("#mainLoader").show();
			
			var inputData = {
				eml: easEmailId,
				pass: easPassword,
				typ: easEmailType
			};
		
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/config-eml/add',
				type: 'POST',
				dataType: 'json',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						$("#easCancel").trigger("click");
						getConfiguredEmails();
						customAlert("#mainNotify", "alert-success", 'Email address configured successfully.');
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
					}
					
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					$("#mainLoader").hide();
				}
			});
		}
		
	});
	
	$("#easCancel").click(function(){
		$("#emailSection .select2").val("").trigger("change");
		$("#emailSection .form-control").val("");
	});
	
	$("#emailConfigTbl").on("click", ".onoffswitch-label", function(e){
		e.preventDefault();
		
		var confirmMsg = "Do you really want to activate this email ?";
		var emailId = $(this).closest("tr").attr("data-email-id");
		var chkBoxId = $(this).prev().attr("id");
		var curSts = $(this).prev().prop("checked");
		
		if( curSts ){
			confirmMsg = "Do you really want to deactivate this email ?";
		}
		
		$("#activeInactiveEmailBtn").attr("data-email-id", emailId);
		$("#activeInactiveEmailBtn").attr("data-chk-box-id", chkBoxId);
		$("#activeInactiveEmailBtn").attr("data-cur-sts", curSts);
		
		$("#activeInactiveEmailModal .info-msg").html(confirmMsg);
		$("#activeInactiveEmailModal").modal("show");
	});
	
	
	$("#activeInactiveEmailBtn").click(function(){
		$("#mainLoader").show();
		
		var emailId = $(this).attr("data-email-id");
		var status = ( $(this).attr("data-cur-sts") == "true" ) ? "false" : "true";
		
		var inputData = {
			usr_eml: emailId,
			sts: status
		};
	
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/usrgroup/actv-usr',
			type: 'POST',
			dataType: 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					if( $("#activeInactiveEmailBtn").attr("data-cur-sts") == "true" ){
						$("#"+ $("#activeInactiveEmailBtn").attr("data-chk-box-id") ).prop("checked", false);
						customAlert("#mainNotify", "alert-success", 'Email deactivated successfully.');
					}else{
						$("#"+ $("#activeInactiveEmailBtn").attr("data-chk-box-id") ).prop("checked", true);
						customAlert("#mainNotify", "alert-success", 'Email activated successfully.');
					}
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
				}
				
				$("#mainLoader").hide();
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});
	});

});


function getConfiguredEmails(){
	$("#mainLoader").show();

	var inputData = {};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/config-eml/get-list',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopLimit = response.output.fldTblEmlConfig.length;
				var tblRow = '', statusStr = '', statusId = '', statusChecked = '', lastRun = '', emailId = '', emlTyp = '';
				
				for( var y=0; y<loopLimit; y++ ){
					statusChecked = '';
					statusId = 'emailStsChkBox' + y;
					
					if(response.output.fldTblEmlConfig[y].sts){
						statusChecked = 'checked';
					}
					
					emailId = response.output.fldTblEmlConfig[y].usrEml;
					
					lastRun = (response.output.fldTblEmlConfig[y].lstRfshTm != 0) ? response.output.fldTblEmlConfig[y].lstRfshTm : '';
					
					if(response.output.fldTblEmlConfig[y].lgnTyp == 'GML')
					{
						emlTyp = 'Gmail';
					}
					else if(response.output.fldTblEmlConfig[y].lgnTyp == 'EXC')
					{
						emlTyp = 'Outlook';
					}
					
					statusStr = '<div class="onoffswitch display-inline-block">'+
										'<input type="checkbox" name="user-status" class="onoffswitch-checkbox" id="'+ statusId +'" tabindex="0" '+ statusChecked +'>'+
										'<label class="onoffswitch-label m-0" for="'+ statusId +'">'+
											'<span class="onoffswitch-inner user-status-type"></span>'+
											'<span class="onoffswitch-switch"></span>'+
										'</label>'+
									'</div>';
										
					tblRow += '<tr data-email-id="'+ emailId +'">'+
								'<td>'+ (y+1) +'</td>'+
								'<td>'+ emailId +'</td>'+
								'<td>'+ lastRun +'</td>'+
								'<td>'+ response.output.fldTblEmlConfig[y].crtdBy +'</td>'+
								'<td>'+ response.output.fldTblEmlConfig[y].crtdOnStr +'</td>'+
								'<td>'+ emlTyp +'</td>'+
								'<td>'+ statusStr +'</td>'+
							'</tr>';
				}
				
				$("#emailConfigTbl tbody").html(tblRow);
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}