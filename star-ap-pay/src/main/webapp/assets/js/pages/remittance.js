$(function(){

	getVendorList( $("#venId"), glblCmpyId );
	
	getCurrencyList( $("#cry") );
	
	$("#clearRemitDocsFltr").click(function(){
		$("#rmtncSection #cry, #rmtncSection #venId").val("").trigger("change");
		$("#rmtncSection #invcDt, #rmtncSection #pmntDt").val("");
	});
	
	$("#rmtncSection").on("change", "input[type='checkbox'][name='all-remit-doc']", function(){
		if( $(this).prop("checked") ){
			$("#remitDocTbl input[type='checkbox'][name='select-remit-doc']").prop("checked", true);
			$("#printRemitDocs").removeClass("disabled");
			$("#printRemitDocs").removeAttr("disabled", "disabled");
		}else{
			$("#remitDocTbl input[type='checkbox'][name='select-remit-doc']").prop("checked", false);
			$("#printRemitDocs").addClass("disabled");
			$("#printRemitDocs").attr("disabled", "disabled");
		}
		
		calculateTotal();
	});
	
	$("#remitDocTbl").on("change", "input[type='checkbox'][name='select-remit-doc']", function(){
		if( $("#remitDocTbl input[type='checkbox'][name='select-remit-doc']:checked").length ){
			$("#printRemitDocs").removeClass("disabled");
			$("#printRemitDocs").removeAttr("disabled", "disabled");
		}else{
			$("#printRemitDocs").addClass("disabled");
			$("#printRemitDocs").attr("disabled", "disabled");
		}
		
		if( $("#remitDocTbl input[type='checkbox'][name='select-remit-doc']").length == $("#remitDocTbl input[type='checkbox'][name='select-remit-doc']:checked").length ){
			$("#rmtncSection input[type='checkbox'][name='all-remit-doc']").prop("checked", true);
		}else{
			$("#rmtncSection input[type='checkbox'][name='all-remit-doc']").prop("checked", false);
		}
		
		calculateTotal();
	});
	
	
	$("#printRemitDocs").click(function(){
		$("#mainNotify").html("");
		var vchrNoStr = '', pmntDt = '';
		var pmntDtArr = [];
		$("#remitDocTbl input[type='checkbox'][name='select-remit-doc']:checked").each(function(index, checkbox){
			vchrNoStr += (vchrNoStr.length) ? "," : "";
			vchrNoStr += $(checkbox).closest("tr").attr("data-vchr-no");
			
			pmntDt = $(checkbox).closest("tr").attr("data-pmnt-dt");
			
			if( pmntDtArr.indexOf(pmntDt) < 0 && pmntDt != "" && pmntDt != undefined ){
				pmntDtArr.push(pmntDt);
			}
		});
		
		if( pmntDtArr.length > 1 ){
			customAlert("#mainNotify", "alert-danger", "Remittance document cannot be printed for records containing different payment dates.");
			return;
		}
		
		var docUrl = rootAppName + "/rest/remit/pdf?cmpyId="+glblCmpyId + "&vchrNo=" + vchrNoStr + "&pmntDt=" + pmntDtArr[0];
	
		window.open(docUrl, '_blank');
	});

	$("#searchRemitDocs").click(function(){
		$("#rmtncSection .select2-selection").removeClass("border-danger");
		
		var errFlg = false;
		var invcStrtDate = "", invcEndDate = "";
		var cry = $("#cry").val();
		var venId = $("#venId").val();
		var pmntDt = $("#pmntDt").val();
		var invcDt = $("#invcDt").val();
		if( invcDt != "" && invcDt != null && invcDt != undefined ){
			invcStrtDate = invcDt.split(" - ")[0];
			invcEndDate = invcDt.split(" - ")[1];
			
			invcStrtDate = invcStrtDate.split('/').join('-');
			invcEndDate = invcEndDate.split('/').join('-');
		}

		if( cry == "" || cry == null || cry == undefined ){
			errFlg = true;
			$("#cry").next().find(".select2-selection").addClass("border-danger");
		}

		//if( venId == "" || venId == null || venId == undefined ){
		//	errFlg = true;
		//	$("#venId").next().find(".select2-selection").addClass("border-danger");
		//}
		
		if( errFlg == false ){
			$("#mainLoader").show();
			
			$("#rmtncSection input[type='checkbox'][name='all-remit-doc']").prop("checked", false);
			$("#rmtncSection input[type='checkbox'][name='all-remit-doc']").attr("disabled");
		
			var inputData = {
				cry: cry,
				venId : venId,
				pmntDt : pmntDt,
				invcDt: invcStrtDate,
				invcDtTo: invcEndDate,
			};
			
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/remit/read',
				type: 'POST',
				dataType : 'json',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						var loopSize = response.output.fldTblDoc.length;
						var tbodyRows = '', vchrNoStr = '', poStr = '', chkNo = "", pmntDtStr = ""; 
						
						for( var i=0; i<loopSize; i++ ){
							vchrNoStr = "", chkNo = "";
							
							pmntDtStr = (response.output.fldTblDoc[i].pmntDtStr != undefined) ? response.output.fldTblDoc[i].pmntDtStr : "";
							
							if( response.output.fldTblDoc[i].poNo == 0 ){
								poStr = "";
							}else{
								if( response.output.fldTblDoc[i].poItm == 0 ){
									poStr = response.output.fldTblDoc[i].poPfx +'-'+ response.output.fldTblDoc[i].poNo;
								}else{
									poStr = response.output.fldTblDoc[i].poPfx +'-'+ response.output.fldTblDoc[i].poNo +'-'+ response.output.fldTblDoc[i].poItm;
								}
							}
							
							if( response.output.fldTblDoc[i].vchrPfx != undefined && response.output.fldTblDoc[i].vchrPfx.trim() != "" ){
								vchrNoStr = response.output.fldTblDoc[i].vchrPfx.trim() +'-'+ response.output.fldTblDoc[i].vchrNo.trim();
							}
							
							if( response.output.fldTblDoc[i].chkNo != undefined ){
								chkNo = response.output.fldTblDoc[i].chkNo;
							}
					
							tbodyRows += '<tr data-invc-no="'+ response.output.fldTblDoc[i].vchrInvNo +'" data-vchr-no="'+ vchrNoStr +'" data-ctl-no="'+ response.output.fldTblDoc[i].ctlNo +'" data-vchr-amt="'+ response.output.fldTblDoc[i].totAmt +'" data-vchr-cry="'+ response.output.fldTblDoc[i].vchrCry +'" data-pmnt-dt="'+ pmntDtStr +'">'+
										'<td>'+
											'<div class="form-check form-check-flat inline-checkbox m-0">'+
												'<label class="form-check-label m-0">'+
													'<input type="checkbox" class="form-check-input" name="select-remit-doc">'+
													'&nbsp;<i class="input-helper"></i>'+
												'</label>'+
											'</div>'+
										'</td>'+
										'<td>'+ response.output.fldTblDoc[i].vchrVenId.trim() + "-" + response.output.fldTblDoc[i].vchrVenNm.trim() + '</td>'+
										'<td>'+ response.output.fldTblDoc[i].vchrInvNo +'</td>'+
										'<td>'+ response.output.fldTblDoc[i].vchrInvDtStr +'</td>'+
										'<td>'+ response.output.fldTblDoc[i].vchrDueDtStr +'</td>'+
										'<td>'+ poStr +'</td>'+
										'<td>'+ vchrNoStr +'</td>'+
										'<td>'+ response.output.fldTblDoc[i].vchrBrh +'</td>'+
										'<td class="text-right">'+ response.output.fldTblDoc[i].vchrAmtStr +'</td>'+
										'<td class="text-right">'+ response.output.fldTblDoc[i].discAmtStr +'</td>'+
										'<td class="text-right">'+ response.output.fldTblDoc[i].totAmtStr +' '+ response.output.fldTblDoc[i].vchrCry +'</td>'+
										'<td class="check-col">'+ chkNo +'</td>'+
										'<td>'+ response.output.fldTblDoc[i].vchrDiscDtStr +'</td>'+
										'<td>'+ pmntDtStr +'</td>'+
										'<td>'+ response.output.fldTblDoc[i].crtDttsStr +'</td>'+
									'</tr>';
						}
						
						
												
						$("#remitDocTbl tbody").html(tbodyRows);
						
						calculateTotal();
						
						if( loopSize ){
							$("#rmtncSection input[type='checkbox'][name='all-remit-doc']").removeAttr("disabled");
						}else{
							$("#rmtncSection input[type='checkbox'][name='all-remit-doc']").attr("disabled", "disabled");
						}
						
						var $table = $('#remitDocTbl');
					
						$table.floatThead({
							useAbsolutePositioning: true,
							scrollContainer: function($table) {
								return $table.closest('.table-responsive');
							}
						});
						$table.floatThead('reflow');
						
						$("#printRemitDocs").addClass("disabled");
						$("#printRemitDocs").attr("disabled", "disabled");
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
					}
					
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					$("#mainLoader").hide();
				}
			});
		}
	});
	
	$("#remitDocTblSumm").on("click", ".dwnld-rmt-file", function(){
			$("#mainNotify").html("");
		var vchrNoStr = '', pmntDt = '';
		var pmntDtArr = [];
		
		vchrNoStr = $(this).closest("tr").attr("data-vchr-no");
		pmntDt = $(this).closest("tr").attr("data-pmnt-dt");				
		
		var docUrl = rootAppName + "/rest/remit/pdf?cmpyId="+glblCmpyId + "&vchrNo=" + vchrNoStr + "&pmntDt=" + pmntDt;
	
		window.open(docUrl, '_blank');

	});
	
	$("#remitDocTblSumm").on("click", ".view-rmit-row-info", function(){
    		var idOfParent = $(this).parents('tr').attr('id');
    		$('tr.child-'+idOfParent).toggle('slow');
	});
	
	
	$("#cancelSendMail").on("click", ".view-email-info", function(){
		$(".form-control").removeClass("border-danger");
	});
	
	var glblvchrNoStr = '', glblpmntDt = '';
	$("#remitDocTblSumm").on("click", ".view-email-info", function(){
		$(".form-control").removeClass("border-danger"); 
		$("#mailSubject").val(""); 
		$("#mailBody").val(""); 
		$("#mailTo").val("");
		$("#mailCc").val("");
		$("#sendMailModal").modal("show");
		var venId = $(this).closest("tr").attr("data-vnd-id");
		 		 		 
		$("#mainNotify").html("");
		var vchrNoStr = '', pmntDt = '';
		var pmntDtArr = [];
		
		vchrNoStr = $(this).closest("tr").attr("data-vchr-no");
		pmntDt = $(this).closest("tr").attr("data-pmnt-dt");	
		
		glblvchrNoStr = vchrNoStr;
		glblpmntDt = pmntDt;
		
		checkVndrEmailData(glblCmpyId, venId, pmntDt);			
		$('#mailBody').attr('rows', 17);
		var docUrl = rootAppName + "/rest/remit/pdf-view?cmpyId="+glblCmpyId + "&vchrNo=" + vchrNoStr + "&pmntDt=" + pmntDt;
		//window.open(docUrl, '_blank');
		
		$("#editViewPdf").attr("src", docUrl);
		 		
		var errFlg = false;
				
		if( errFlg == false ){
			$("#mainLoader").show();
			
			var inputData = {
				venId : venId,
			};
			
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/common/remit-email',
				type: 'POST',
				dataType : 'json',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						var loopSize = response.output.fldTblVendor.length;
						for( var i=0; i<loopSize; i++ ){																								
								 $("#mailTo").val(response.output.fldTblVendor[i].venEmail.trim());								 							
						}									
										
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
					}
					
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					$("#mainLoader").hide();
				}
			});
		}  
	});
	
		$("#sendMail").click(function(){
		$(".form-control").removeClass("border-danger");
		var errFlg = false;
		var emlFrm = $("#mailFrom").val().trim();
		var emlFrmNm = "";
		var emlTo = $("#mailTo").val().trim();
		var emlCc = $("#mailCc").val().trim();
		var emlBcc = "";
		var emlSub = $("#mailSubject").val().trim();
		var emlBody = $("#mailBody").val().trim();
		var emlDocId = "";
		var cmpyId,vchrNoStr, pmntDt;		
				
		if( emlFrm == "" || emlFrm == null || emlFrm == undefined ){
			errFlg = true;
			$("#mailFrom").addClass("border-danger");
		}
		if( emlSub == "" || emlSub == null || emlSub == undefined ){
			errFlg = true;
			$("#mailSubject").addClass("border-danger");
		}
		if( emlTo == "" || emlTo == null || emlTo == undefined ){
			errFlg = true;
			$("#mailTo").addClass("border-danger");
		}
		if( emlBody == "" || emlBody == null || emlBody == undefined ){
			errFlg = true;
			$("#mailBody").addClass("border-danger");
		}
		var lines = $("#mailBody").val().split('\n');
		var emlBody="";
		for(var i = 0;i < lines.length;i++){
    			emlBody+=lines[i]+"<br>";
		}
		if( errFlg == false ){
			$("#mainLoader").show();
			
			var inputData = {
				emlFrm : emlFrm,
				emlFrmNm : emlFrmNm,
				emlTo : emlTo,
				emlCc : emlCc,
				emlBcc : emlBcc,
				emlSub : emlSub,
				emlBody : emlBody,
				emlDocId : emlDocId,
				cmpyId : glblCmpyId,
				vchrNoStr : glblvchrNoStr, 
				pmntDt : glblpmntDt,
			};
			
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/sendmail/mail-attch',
				type: 'POST',
				dataType : 'json',
				success: function(response){					
					$("#mainLoader").show();
					$("#mainLoader").hide();
					$("#sendMailModal").modal("hide");
					customAlert("#mainNotify", "alert-success", "Mailed Successfully.");
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					$("#mainLoader").hide();
				}
			});
		}  
	});

		
	$("#searchRemitDocsSumm").click(function(){
		$(".select2-selection").removeClass("border-danger");
		$("#vendListTbody").html("");
		
		var errFlg = false;
		var invcStrtDate = "", invcEndDate = "";
		var cry = $("#cry").val();
		var venId = $("#venId").val();
		var pmntDt = $("#pmntDt").val();
		var invcDt = $("#invcDt").val();
		if( invcDt != "" && invcDt != null && invcDt != undefined ){
			invcStrtDate = invcDt.split(" - ")[0];
			invcEndDate = invcDt.split(" - ")[1];
			
			invcStrtDate = invcStrtDate.split('/').join('-');
			invcEndDate = invcEndDate.split('/').join('-');
		}

		if( cry == "" || cry == null || cry == undefined ){
			errFlg = true;
			$("#cry").next().find(".select2-selection").addClass("border-danger");
		}

		//if( venId == "" || venId == null || venId == undefined ){
		//	errFlg = true;
		//	$("#venId").next().find(".select2-selection").addClass("border-danger");
		//}
		
		if( errFlg == false ){
			$("#mainLoader").show();
			
			$("#rmtncSection input[type='checkbox'][name='all-remit-doc']").prop("checked", false);
			$("#rmtncSection input[type='checkbox'][name='all-remit-doc']").attr("disabled");
		
			var inputData = {
				cry: cry,
				venId : venId,
				pmntDt : pmntDt,
				invcDt: invcStrtDate,
				invcDtTo: invcEndDate,
			};
			
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/remit/read',
				type: 'POST',
				dataType : 'json',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						var loopSize = response.output.fldTblDoc.length;
						var tbodyRows = '',  poStr = '', chkNo = "", pmntDtStr = ""; 
						var actnBtn = "";
						var vid=[],vname=[],tamt=[];
						var vchrNoStr = [];
						for( var i=0; i<loopSize; i++ ){
							
							vid[i]=response.output.fldTblDoc[i].vchrVenId.trim();
							vname[i]=response.output.fldTblDoc[i].vchrVenNm.trim();
							tamt[i]=response.output.fldTblDoc[i].totAmt;
						}
						var vtamt,k=0,l=0;
						for( var i=0; i<loopSize; i++ ){
						
						vtamt=0;
						actnBtn = "";
						actnBtn = '<i class="icon-menu icon-magnifier display-5 text-primary view-rmit-row-info c-pointer font-weight-bold" title="View"></i>';
						actnBtn += '<i class="icon-menu icon-cloud-download c-pointer font-weight-bold display-5 text-info ml-1 dwnld-rmt-file" data-pay-sts="A" title="Download Remittance File"></i>';						
						actnBtn += '<i class="icon-menu icon-envelope display-5 text-primary view-email-info c-pointer ml-1 font-weight-bold" title="Email Remittance File"></i>';
						vchrNoStr = "";
						for( var j=i; j<loopSize; j++ ){								 
												
							chkNo = "";
						
							if(vid[i]==vid[j])
								{
									vtamt+=tamt[j];
																					
									pmntDtStr = (response.output.fldTblDoc[j].pmntDtStr != undefined) ? response.output.fldTblDoc[j].pmntDtStr : "";
							
									if( response.output.fldTblDoc[j].poNo == 0 ){
											poStr = "";
									}
									else{
										if( response.output.fldTblDoc[j].poItm == 0 ){
											poStr = response.output.fldTblDoc[j].poPfx +'-'+ response.output.fldTblDoc[j].poNo;
										}
										else{
											poStr = response.output.fldTblDoc[j].poPfx +'-'+ response.output.fldTblDoc[j].poNo +'-'+ response.output.fldTblDoc[j].poItm;
										}
									}
							
									if( response.output.fldTblDoc[j].vchrPfx != undefined && response.output.fldTblDoc[j].vchrPfx.trim() != "" ){
										vchrNoStr += (vchrNoStr.length) ? "," : "";
										vchrNoStr += response.output.fldTblDoc[j].vchrPfx.trim() +'-'+ response.output.fldTblDoc[j].vchrNo.trim();
									}
							
									if( response.output.fldTblDoc[j].chkNo != undefined ){
										chkNo = response.output.fldTblDoc[j].chkNo;
									}
							
									
									tbodyRows += '<tr class="child-'+i+'" data-invc-no="'+ response.output.fldTblDoc[j].vchrInvNo +'" data-vchr-noo="'+ vchrNoStr +'" data-ctl-no="'+ response.output.fldTblDoc[j].ctlNo +'" data-vchr-amt="'+ response.output.fldTblDoc[j].totAmt +'" data-vchr-cry="'+ response.output.fldTblDoc[j].vchrCry +'" data-pmnt-dt="'+ pmntDtStr +'">'+								
									//tbodyRows += '<tr> '+
										'<td>'+
											
										'</td>'+
										'<td>'+ response.output.fldTblDoc[j].vchrVenId.trim() + "-" + response.output.fldTblDoc[j].vchrVenNm.trim() + '</td>'+
										'<td>'+ response.output.fldTblDoc[j].vchrInvNo +'</td>'+
										'<td>'+ response.output.fldTblDoc[j].vchrInvDtStr +'</td>'+
										'<td>'+ response.output.fldTblDoc[j].vchrDueDtStr +'</td>'+
										'<td>'+ poStr +'</td>'+
										'<td>'+ response.output.fldTblDoc[j].vchrPfx.trim() +'-'+ response.output.fldTblDoc[j].vchrNo.trim() +'</td>'+
										'<td>'+ response.output.fldTblDoc[j].vchrBrh +'</td>'+
										'<td class="text-right">'+ response.output.fldTblDoc[j].vchrAmtStr +'</td>'+
										'<td class="text-right">'+ response.output.fldTblDoc[j].discAmtStr +'</td>'+
										'<td class="text-right">'+ response.output.fldTblDoc[j].totAmtStr +' '+ response.output.fldTblDoc[j].vchrCry +'</td>'+
										'<td class="check-col">'+ chkNo +'</td>'+
										'<td>'+ response.output.fldTblDoc[j].vchrDiscDtStr +'</td>'+
										'<td>'+ pmntDtStr +'</td>'+
										'<td>'+ response.output.fldTblDoc[j].crtDttsStr +'</td>'+
									'</tr>';
							}
							else
								break;
							
							}
														
							
							{
							k=k+1;
							//tbodyRows += '<tr data-invc-no="'+ response.output.fldTblDoc[i].vchrInvNo +'" data-vchr-no="'+ vchrNoStr +'" data-ctl-no="'+ response.output.fldTblDoc[i].ctlNo +'" data-vchr-amt="'+ response.output.fldTblDoc[i].totAmt +'" data-vchr-cry="'+ response.output.fldTblDoc[i].vchrCry +'" data-pmnt-dt="'+ pmntDtStr +'">'+
							tbodyRows += '<tr class="vndr-total font-weight-bold text-dark" id="'+i+'" data-vnd-id="'+ vid[i] +'" data-vnd-name="'+ vname[i] +'" data-vnd-tamt="'+ vtamt +'" data-vchr-no="'+ vchrNoStr +'" data-pmnt-dt="'+ pmntDtStr + '">'+
										'<td>'+
											k+
										'</td>'+
										'<td>'+ vid[i] + "-" + vname[i] + '</td>'+
										'<td></td>'+
										'<td></td>'+
										'<td></td>'+
										'<td></td>'+
										'<td></td>'+
										'<td></td>'+
										'<td></td>'+
										'<td></td>'+
										'<td class="text-right">'+ vtamt.toFixed(2) +' '+ cry +'</td>'+										
										'<td>'+ actnBtn +'</td>'+
										'<td></td>'+
										'<td></td>'+
										'<td></td>'+
										
									'</tr>';
									if(j==loopSize-1)
										i=loopSize;
									else
										i=j-1;
																								
									l=l+1;
								}
								
							
						}
						$("#remitDocTblSumm tbody").html(tbodyRows);
						$("#vendListTbody tr").hide();
						$("#vendListTbody tr.vndr-total").show();	
						
											
												
						//calculateTotal();
						if( loopSize ){
							$("#rmtncSection input[type='checkbox'][name='all-remit-doc']").removeAttr("disabled");
						}else{
							$("#rmtncSection input[type='checkbox'][name='all-remit-doc']").attr("disabled", "disabled");
						}
												
						var $table = $('#remitDocTblSumm');
					
						$table.floatThead({
							useAbsolutePositioning: true,
							scrollContainer: function($table) {
								return $table.closest('.table-responsive');
							}
						});
						$table.floatThead('reflow');
						
						//$("#printRemitDocs").addClass("disabled");
						//$("#printRemitDocs").attr("disabled", "disabled");
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
					}
					
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					$("#mainLoader").hide();
				}
			});
		}
	});
	
});

function checkVndrEmailData(cmpyId, venId, pmntDt) {
	$("#mainLoader").show();

	var inputData = {
		cmpyId: cmpyId,
		venId: venId,
	};

	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify(inputData),
		url: rootAppName + '/rest/vendor/read-email-setup',
		type: 'POST',
		dataType: 'json',
		success: function(response) {
			if (response.output.rtnSts == 0) {
				if (response.output.nbrRecRtn > 0) {
					if (response.output.fldTblVendorES[0].subject != undefined) {
						$("#mailSubject").val(response.output.fldTblVendorES[0].subject + pmntDt).trigger("change");
					}				
					if (response.output.fldTblVendorES[0].body != undefined) {
						$("#mailBody").val(response.output.fldTblVendorES[0].body).trigger("change");
					}													
				} 
			} else {
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for (var i = 0; i < loopLimit; i++) {
					errList += '<li>' + response.output.messages[i] + '</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}

			$("#mainLoader").hide();
		},
		error: function(jqXHR, textStatus, errorThrown) {
			if (jqXHR.status == 401) {
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}

function calculateTotal(){
	var total = 0, i = 0, vchrAmt = 0;
	var vchrCry = ""; 
	var totalCount = 0;
	
	$("#remitDocTbl input[type='checkbox'][name='select-remit-doc']:checked").each(function(index, checkbox){
		if( i==0 ){
			vchrCry = $(checkbox).closest("tr").attr("data-vchr-cry");
		}
		
		vchrAmt = parseFloat( $(checkbox).closest("tr").attr("data-vchr-amt") );
		
		if( !isNaN(vchrAmt) ){
			total += vchrAmt;
		}
		
		i++;
	});
	
	total = total.toFixed(2);
	
	totalCount = i ;
	$("#remitTotal").html(total + " " + vchrCry + "(" + totalCount + ")");
}