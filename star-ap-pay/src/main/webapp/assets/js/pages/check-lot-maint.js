$(function(){

	getBankList($("#bankId"));
	
	$("#bankId").change(function(){
		var bnkCd = $(this).val();
		if( bnkCd != "" ){			
			getBankAccountDetails($("#acctId"), bnkCd);
		}else{
			$("#acctId").html("");
		}
		
		$("#checkLotsTbl tbody tr.more-check-lot").remove();
		$("#checkLotsTbl tbody .form-control").val("");
		$("#checkLots").slideUp();
	});
	
	$("#acctId").change(function(){		
		$("#checkLotsTbl tbody tr.first-row .form-control").removeAttr("disabled");
		$("#checkLotsTbl tbody tr.more-check-lot").remove();
		$("#checkLotsTbl tbody .form-control").val("");
		$("#checkLotsTbl tbody tr.first-row").find(".in-use-col").html("");
		$("#checkLots").slideUp();
	});
	
	$("#searchCheckLots").click(function(){
		$("#bankId, #acctId").next().find(".select2-selection").removeClass("border-danger");
		
		var errFlg = false;
		var bnkCd = $("#bankId").val();
		var acctId = $("#acctId").val();
		
		if( bnkCd == "" || bnkCd == null || bnkCd == undefined ){
			errFlg = true;
			$("#bankId").next().find(".select2-selection").addClass("border-danger");
		}
		
		if( acctId == "" || acctId == null || acctId == undefined ){
			errFlg = true;
			$("#acctId").next().find(".select2-selection").addClass("border-danger");
		}
		
		if( !errFlg ){
			$("#mainLoader").show();
			$("#checkLotsTbl tbody tr.more-check-lot").remove();
			
			var inputData = {
				bnkCode : bnkCd,
				acctId : acctId,
			};
						
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/check/read',
				type: 'POST',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						var loopLimit = response.output.fldTblCheck.length;
						var checkLotRow = '', openSts = '', inUseSts = '', delBtn = '', disabledAttr = 'disabled';
						if( loopLimit ){
							for( var y=0; y<loopLimit; y++ ){
								delBtn = '', disabledAttr = 'disabled';
								openSts = (response.output.fldTblCheck[y].chkLotOpn) ? "<span class='badge badge-success'>Open</span>" : "<span class='badge badge-warning'>Closed</span>";
								inUseSts = (response.output.fldTblCheck[y].chkLotUse) ? "<i class='text-success fs-18 icon-check'></i>" : "";
								
								if( response.output.fldTblCheck[y].chkLotOpn ){
									delBtn = '<i class="icon-minus c-pointer text-danger delete-check-lot-row display-5" title="Delete"></i>';
									disabledAttr = '';
								}else{
									$("#checkLotsTbl tbody tr.first-row .form-control").attr("disabled", "disabled");
								}
								
								if( response.output.fldTblCheck[y].chkLotUse ){
									disabledAttr = 'disabled';
									delBtn = '';
									$("#checkLotsTbl tbody tr.first-row .form-control").attr("disabled", "disabled");
								}
								
								if( y==0 ){
									$("#checkLotsTbl tbody tr.first-row").find(".check-lot-no").html(response.output.fldTblCheck[y].lotNo);
									$("#checkLotsTbl tbody tr.first-row").find(".check-short-info").val(response.output.fldTblCheck[y].lotInfo);
									$("#checkLotsTbl tbody tr.first-row").find(".check-no-from").val(response.output.fldTblCheck[y].chkFrm);
									$("#checkLotsTbl tbody tr.first-row").find(".check-no-to").val(response.output.fldTblCheck[y].chkTo);
									$("#checkLotsTbl tbody tr.first-row").find(".open-close-col").html(openSts);
									$("#checkLotsTbl tbody tr.first-row").find(".in-use-col").html(inUseSts);
								}else{
									checkLotRow += '<tr class="more-check-lot" data-chk-sts="A">'+
										'<td>'+ delBtn +'</td>'+
										'<td class="text-center"><span class="check-lot-no">'+ response.output.fldTblCheck[y].lotNo +'</span></td>'+
										'<td><input type="text" class="form-control check-short-info" maxlength="20" value="'+ response.output.fldTblCheck[y].lotInfo +'" '+ disabledAttr +'></td>'+
										'<td><input type="text" class="form-control check-no-from number-only" maxlength="10" value="'+ response.output.fldTblCheck[y].chkFrm +'" '+ disabledAttr +'></td>'+
										'<td><input type="text" class="form-control check-no-to number-only" maxlength="10" value="'+ response.output.fldTblCheck[y].chkTo +'" '+ disabledAttr +'></td>'+
										'<td>'+ openSts +'</td>'+
										'<td>'+ inUseSts +'</td>'+
									'</tr>';
								}
							}
						}
						
						$("#lotInfo, #checkInfo").html("");
						if( response.output.curLotNo != undefined ){
							$("#lotInfo").html("Current Lot : " + response.output.curLotNo);
						}
						if( response.output.curChkNo != undefined ){
							$("#checkInfo").html("Current Check : " + response.output.curChkNo);
						}
						
						$("#checkLotsTbl tbody").append(checkLotRow);
						$("#checkLots").slideDown();
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
						$("#saveBankSetup").hide();
					}
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}else if( jqXHR.status == 500 ){
						customAlert("#mainNotify", "alert-danger", jqXHR.responseText);
						$("#saveFtpSetting").hide();
					}
					console.log(jqXHR);
					console.log(textStatus);
					console.log(errorThrown);
					$("#mainLoader").hide();
				}
			});
		}
	});
	
	$("#clearCheckLots").click(function(){
		$("#lotInfo, #checkInfo").html("");
		$("#cmpyId, #bankId, #acctId").val("").trigger("change");
		$("#checkLotsTbl tbody tr.more-check-lot").remove();
		$("#checkLotsTbl tbody .form-control").val("");
		$("#checkLots").slideUp();
	});
	
	$("#checkLotAddRow").click(function(){
		var checkLotRow = '<tr class="more-check-lot" data-chk-sts="N">'+
								'<td><i class="icon-minus c-pointer text-danger delete-check-lot-row display-5" title="Delete"></i></td>'+
								'<td class="text-center"><span class="check-lot-no">'+( $("#checkLotsTbl tbody tr").length + 1) +'</span></td>'+
								'<td><input type="text" class="form-control check-short-info" maxlength="20"></td>'+
								'<td><input type="text" class="form-control check-no-from number-only"></td>'+
								'<td><input type="text" class="form-control check-no-to number-only"></td>'+
								'<td></td>'+
								'<td></td>'+
							'</tr>';
		$("#checkLotsTbl tbody").append(checkLotRow);
	});
	
	$("#checkLotsTbl tbody").on("click", ".delete-check-lot-row", function(){
		$(this).closest("tr").remove();
		var count = 1
		$("#checkLotsTbl tbody tr").each(function(index, tr){
			$(tr).find(".check-lot-no").html(count);
			count++;
		});
	});
	
	
	$("#saveCheckLots").click(function(){
		$("#checkLotsTbl tbody .form-control").removeClass("border-danger");
		
		var errFlg = false;
		var bnkCd = $("#bankId").val();
		var acctId = $("#acctId").val();
		var lotArr = [];
		
		if( bnkCd == "" || bnkCd == null || bnkCd == undefined ){
			errFlg = true;
			$("#bankId").next().find(".select2-selection").addClass("border-danger");
		}
		
		if( acctId == "" || acctId == null || acctId == undefined ){
			errFlg = true;
			$("#acctId").next().find(".select2-selection").addClass("border-danger");
		}
		
		$("#checkLotsTbl tbody tr").each(function(index, row){			
			var lotNo = $(row).find(".check-lot-no").text();
			var shrtInfo = $(row).find(".check-short-info").val().trim();
			var chkFrm = $(row).find(".check-no-from").val().trim();
			var chkTo = $(row).find(".check-no-to").val().trim();
			var chkLotSts = $(row).attr("data-chk-sts");
			
			if( shrtInfo == "" ){
				errFlg = true;
				$(row).find(".check-short-info").addClass("border-danger");
			}
			
			if( chkFrm == "" ){
				errFlg = true;
				$(row).find(".check-no-from").addClass("border-danger");
			}
			
			if( chkTo == "" ){
				errFlg = true;
				$(row).find(".check-no-to").addClass("border-danger");
			}
			
			var chkLotObj = {
				lotNo : lotNo,
				shrtInfo : shrtInfo,
				chkFrm : chkFrm,
				chkTo: chkTo,
				sts: chkLotSts
			};
			
			lotArr.push( chkLotObj );
		});
		
		if( errFlg == false ){
			$("#mainLoader").show();
			
			var inputData = {
				bnkCode : bnkCd,
				acctId : acctId,				
				lotArr : lotArr,
			};
						
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/check/upd',
				type: 'POST',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						customAlert("#mainNotify", "alert-success", "Check lot saved successfully.");
						$("#clearCheckLots").trigger("click");
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
					}
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}else if( jqXHR.status == 500 ){
						customAlert("#mainNotify", "alert-danger", jqXHR.responseText);
						$("#saveFtpSetting").hide();
					}
					console.log(jqXHR);
					console.log(textStatus);
					console.log(errorThrown);
					$("#mainLoader").hide();
				}
			});
		}
	});
	
});