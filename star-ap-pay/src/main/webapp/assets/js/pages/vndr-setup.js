var dfltSubj = "",dfltBody="";

$(function() {

    getVendorData();

    getCountryList($("#venCty"));

    getPaymentMethodList($("#pymntType"));

    getVendorListESP($("#venIdSetting"), glblCmpyId);
       	
    getVendorListESP($("#emailVenId"), glblCmpyId);
	//checkVndrEmailData(glblCmpyId, "dflt");
    
    getBranchList($("#brhVenSetting"));

    //getGlList($("#vndrGlAcc"));

    //getVoucherCategoryList($("#categorySetting"));

    //getTrnStsActnList($("#trnsStsActn"));
    
    //getTrnStsList( $("#trnsSts") );

    getUserList($("#notifyUsr"));
    
    //getVendorDefaultSetting();

    getVendorListESP($("#achVenId"), glblCmpyId);
    
    $("#trnsStsActn").change(function(){
    	var trnsStsActn = $("#trnsStsActn").val();
    	    	
    	if( trnsStsActn != "" && trnsStsActn != null && trnsStsActn != undefined ){
    		if( trnsStsActn == "A" ){
    			$(".trn-sts-row").show();
    			$(".trn-sts-rsn-row").hide();
    		}else{
    			$(".trn-sts-rsn-row").show();
    			$(".trn-sts-row").hide();
    			
   				getTrnStsRsnList($("#trnsStsRsn"), trnsStsActn);
    		}
    	}else{
    		$(".trn-sts-row").show();
    		$(".trn-sts-rsn-row").hide();
    	}
    });

    $("#achVenId").change(function() {
        var vndrId = $("#achVenId").val();
        //var cmpyId = $("#venCmpyId").val();

        $("#updVndr").val("0");

        if (vndrId != "" && vndrId != null && vndrId != undefined) {
            var $selectedVndr = $("#achVenId option:selected");
            $("#achVenNm").val($selectedVndr.attr("data-vndr-nm"));
            $("#venLongNm").val($selectedVndr.attr("data-vndr-long-nm"));
            $("#vndrCry").val($selectedVndr.attr("data-vndr-cry"));

            //if( cmpyId != "" && cmpyId != null && cmpyId != undefined ){
            checkVndrData(glblCmpyId, vndrId);
            //}
        } else {
            $("#achVenNm, #venLongNm, #vndrCry").val("");

            $("#venCty, #pymntType").val("").trigger("change");
            $("#pymntMthdTbl tbody tr").remove();

            $("#bankDetails tbody tr.more-bank").remove();
            $("#bankDetails tbody .form-control").val("");
        }
    });

    $("#emailVenId").change(function() {
    	$("#mailSubject").removeClass("border-danger");
        $("#mailBody").removeClass("border-danger");
        var vndrId = $("#emailVenId").val();
        
        $("#updVndrEmailSetup").val("0");

        if (vndrId != "" && vndrId != null && vndrId != undefined) {
            checkVndrEmailData(glblCmpyId, vndrId);
        } else {
            $("#mailSubject, #mailBody").val("");            
        }
    });

    $("#addVndrBtn").click(function() {
        clearVndrBankArea();

        $("#achVndrSavedArea").hide();
        $("#addUpdateVendorDiv").slideDown();
    });

    $("#cancelAddVndr").click(function() {
        clearVndrBankArea();

        $("#addUpdateVendorDiv").hide();
        $("#achVndrSavedArea").slideDown();
    });

    $("#addMoreBank").click(function() {
        
        var error = false;
        $("#bankDetails tbody tr").each(function(index, row) {
            var $row = $(row);
            var bankKey = $row.find(".bank-key").val().trim();
            var bankAccNum = $row.find(".bank-acc-number").val().trim();
            var bankAccNm = $row.find(".bank-acc-name").val().trim();
            var bankRef = $row.find(".bank-reference").val().trim();

            if (bankKey == "") {
                error = true;
                $(row).find(".bank-key").addClass("border-danger");
            }

            if (bankAccNum == "") {
                error = true;
                $(row).find(".bank-acc-number").addClass("border-danger");
            }

            if (bankAccNm == "") {
                error = true;
                $(row).find(".bank-acc-name").addClass("border-danger");
            }

            /*if (bankRef == "") {
                error = true;
                $(row).find(".bank-reference").addClass("border-danger");
            }*/

        });
        
        if(error == false)
        {
	        var bankRow = '<tr class="more-bank">' +
	            '<td class="text-center"><i class="icon-minus c-pointer text-danger delete-bank-row display-5" title="Delete"></i></td>' +
	            '<td><input type="text" class="form-control bank-key" maxlength="50"></td>' +
	            '<td><input type="text" class="form-control bank-acc-number number-only" maxlength="10"></td>' +
	            '<td><input type="text" class="form-control bank-acc-name" maxlength="23"></td>' +
	            '<td><input type="text" class="form-control bank-reference" maxlength="100"></td>' +
	            '</tr>';
	        $("#bankDetails tbody").append(bankRow);
	
	        $(".select2").select2({});
        }
    });

    $("#bankDetails").on("click", ".delete-bank-row", function() {
        $(this).closest("tr").remove();
    });

    $("#vndrAchTbl").on("click", ".edit-vndr-info", function() {
        $("#mainLoader").show();

        var inputData = {
            cmpyId: glblCmpyId,
            venId: $(this).closest("tr").attr("data-vndr-id"),
        };

        $.ajax({
            headers: ajaxHeader,
            data: JSON.stringify(inputData),
            url: rootAppName + '/rest/vendor/read-ven',
            type: 'POST',
            dataType: 'json',
            success: function(response) {
                if (response.output.rtnSts == 0) {
                    //$("#venCmpyId").val( response.output.fldTblVendor[0].cmpyId ).trigger("change");

                    if (response.output.fldTblVendor[0].cty != undefined) {
                        $("#venCty").val(response.output.fldTblVendor[0].cty).trigger("change");
                    }

                    //$("#achVenId").attr("data-default-value", response.output.fldTblVendor[0].venId );
                    $("#achVenId").val(response.output.fldTblVendor[0].venId).trigger("change")

                    if (response.output.fldTblVendor[0].payMthd != undefined) {
                        $("#pymntType").val(response.output.fldTblVendor[0].payMthd).trigger("change");
                    }

                    /*var loopSize = 0;
					if( response.output.fldTblVendor[0].bnkList != undefined ){
						loopSize = response.output.fldTblVendor[0].bnkList.length;
					}

					for( var i=0; i<loopSize; i++ ){
						if( i == 0 ){
							$("#bankDetails tbody tr").find(".bank-key").val( response.output.fldTblVendor[0].bnkList[i].bnkKey );
							$("#bankDetails tbody tr").find(".bank-acc-number").val( response.output.fldTblVendor[0].bnkList[i].bnkAccNo );
							$("#bankDetails tbody tr").find(".bank-acc-name").val( response.output.fldTblVendor[0].bnkList[i].bnkNm );
							$("#bankDetails tbody tr").find(".bank-reference").val( response.output.fldTblVendor[0].bnkList[i].extRef );
						}else{
							var bankRow = '<tr class="more-bank">'+
											'<td class="text-center"><i class="icon-minus c-pointer text-danger delete-bank-row  menu-icon" title="Delete"></i></td>'+
											'<td><input type="text" class="form-control bank-key" value="'+ response.output.fldTblVendor[0].bnkList[i].bnkKey +'"></td>'+
											'<td><input type="text" class="form-control bank-acc-number number-only" value="'+ response.output.fldTblVendor[0].bnkList[i].bnkAccNo +'"></td>'+
											'<td><input type="text" class="form-control bank-acc-name" value="'+ response.output.fldTblVendor[0].bnkList[i].bnkNm +'"></td>'+
											'<td><input type="text" class="form-control bank-reference" value="'+ response.output.fldTblVendor[0].bnkList[i].extRef +'"></td>'+
										'</tr>';
							$("#bankDetails tbody").append(bankRow);
							
							$(".select2").select2({});
						}
					}
					
					loopSize = 0;
					if( response.output.fldTblVendor[0].payMthdList != undefined ){
						loopSize = response.output.fldTblVendor[0].payMthdList.length;
					}
					
					$("#pymntMthdTbl tbody").html("");
					for( var i=0; i<loopSize; i++ ){
						var checked = (response.output.fldTblVendor[0].payMthdList[i].mthdDflt) ? "checked" : "";
						var tblRow = '<tr data-pymnt-mthd="'+ response.output.fldTblVendor[0].payMthdList[i].mthdCd +'" data-mthd-id="'+ response.output.fldTblVendor[0].payMthdList[i].mthdId +'">'+
							'<td>'+
								'<div class="form-radio form-radio-flat m-0">'+
		                            '<label class="form-check-label">'+
		                              	'<input type="radio" class="form-check-input" name="default-pymnt-mthd" '+ checked +'>'+
		                            	'<i class="input-helper"></i>'+
		                            '</label>'+
		                    	'</div>'+
	                    	'</td>'+
							'<td>'+ response.output.fldTblVendor[0].payMthdList[i].mthdNm +'</td>'+
							'<td><i class="icon-trash text-danger font-weight-bold c-pointer delete-pymnt-mthd"></i></td>'+
						'</tr>';
						$("#pymntMthdTbl tbody").append(tblRow);
					}*/

                    $("#venCmpyId, #achVenId").attr("disabled", "disabled");

                    $("#achVndrSavedArea").hide();
                    $("#addUpdateVendorDiv").slideDown();

                    $("#mainLoader").hide();
                } else {
                    var loopLimit = response.output.messages.length;
                    var errList = '<ul class="m-0 pl-1">';
                    for (var i = 0; i < loopLimit; i++) {
                        errList += '<li>' + response.output.messages[i] + '</li>';
                    }
                    errList += '</ul>';
                    customAlert("#mainNotify", "alert-danger", errList);

                    $("#mainLoader").hide();
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                if (jqXHR.status == 401) {
                    validateAuthCode();
                }
                $("#mainLoader").hide();
            }
        });
    });

    $("#saveVndrSetup").click(function() {
        $(".form-control, .select2-selection").removeClass("border-danger");

        var error = false;
        var bankArr = [],
            pymntMthdArr = [];
        //var cmpyId = localStorage.getItem("starOcrCmpyId");
        var cmpyId = $("#headerCmpyId").val();
        var country = $("#venCty").val();
        var venId = $("#achVenId").val();
        var currency = $("#vndrCry").val();
        var achVenNm = $("#achVenNm").val();
        var venLongNm = $("#venLongNm").val();
        //var pymntMethod = $("#pymntType").val();

        if (cmpyId == null || cmpyId == undefined || cmpyId == "" || cmpyId.length <= 0) {
            error = true;
            $("#venCmpyId").next().find(".select2-selection").addClass("border-danger");
        }

        if (country == null || country == undefined || country == "" || country.length <= 0) {
            error = true;
            $("#venCty").next().find(".select2-selection").addClass("border-danger");
        }

        if (venId == null || venId == undefined || venId == "" || venId.length <= 0) {
            error = true;
            $("#achVenId").next().find(".select2-selection").addClass("border-danger");
        }

        if (currency == null || currency == undefined || currency == "" || currency.length <= 0) {
            error = true;
            $("#vndrCry").addClass("border-danger");
        }

        /*if( pymntMethod == null || pymntMethod == undefined || pymntMethod == "" || pymntMethod.length <= 0 ){
        	error = true;
        	$("#pymntType").next().find(".select2-selection").addClass("border-danger");
        }*/

        if ($("#pymntMthdTbl tbody tr").length == 0) {
            error = true;
            $("#pymntType").next().find(".select2-selection").addClass("border-danger");
        }

        $("#pymntMthdTbl tbody tr").each(function(index, row) {
            var $row = $(row);
            var mthdId = $row.attr("data-mthd-id");
            var mthdCd = $row.attr("data-pymnt-mthd");
            var mthdDflt = $row.find("input[type='radio'][name='default-pymnt-mthd']").prop("checked");

            var tmpMthdObj = {
                mthdId: mthdId,
                mthdCd: mthdCd,
                mthdDflt: mthdDflt,
            };

            pymntMthdArr.push(tmpMthdObj);
        });

        $("#bankDetails tbody tr").each(function(index, row) {
            var $row = $(row);
            var bankKey = $row.find(".bank-key").val().trim();
            var bankAccNum = $row.find(".bank-acc-number").val().trim();
            var bankAccNm = $row.find(".bank-acc-name").val().trim();
            var bankRef = $row.find(".bank-reference").val().trim();

            if (bankKey == "") {
                error = true;
                $(row).find(".bank-key").addClass("border-danger");
            }

            if (bankAccNum == "") {
                error = true;
                $(row).find(".bank-acc-number").addClass("border-danger");
            }

            if (bankAccNm == "") {
                error = true;
                $(row).find(".bank-acc-name").addClass("border-danger");
            }

            /*if (bankRef == "") {
                error = true;
                $(row).find(".bank-reference").addClass("border-danger");
            }*/

            var tmpBankObj = {
                acctNo: bankAccNum,
                bnkNm: bankAccNm,
                bnkKey: bankKey,
                extRef: bankRef,
            };

            bankArr.push(tmpBankObj);
        });
        
        //return;

        if (error == false) {
            $("#mainLoader").show();

            var ajaxUrl = rootAppName + "/rest/vendor/add";

            if ($("#updVndr").val() == "1") {
                ajaxUrl = rootAppName + "/rest/vendor/update";
            }

            var inputData = {
                cmpyId: cmpyId,
                cty: country,
                venId: venId,
                cry: currency,
                pymntMtd: "",
                venNm: achVenNm,
                venLongNm: venLongNm,
                bankList: bankArr,
                pymntMtdList: pymntMthdArr,
            };

            $.ajax({
                headers: ajaxHeader,
                data: JSON.stringify(inputData),
                url: ajaxUrl,
                type: 'POST',
                dataType: 'json',
                success: function(response) {
                    if (response.output.rtnSts == 0) {
                        getVendorData();

                        customAlert("#mainNotify", "alert-success", 'Vendor data saved successfully.');

                        clearVndrBankArea();
                    } else {
                        var loopLimit = response.output.messages.length;
                        var errList = '<ul class="m-0 pl-1">';
                        for (var i = 0; i < loopLimit; i++) {
                            errList += '<li>' + response.output.messages[i] + '</li>';
                        }
                        errList += '</ul>';
                        customAlert("#mainNotify", "alert-danger", errList);

                        $("#mainLoader").hide();
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    if (jqXHR.status == 401) {
                        validateAuthCode();
                    }
                    $("#mainLoader").hide();
                }
            });
        }
    });

	$("#saveEmailSetup").click(function() {
        $("#mailSubject").removeClass("border-danger");
        $("#mailBody").removeClass("border-danger");
        $("#emailVenId").next().find(".select2-selection").removeClass("border-danger");

        var error = false;
        var cmpyId = localStorage.getItem("starOcrCmpyId");
        var subj = $("#mailSubject").val();
        var body = $("#mailBody").val();
        var venId = $("#emailVenId").val();

        if (subj == null || subj == undefined || subj == "" || subj.length <= 0) {
            error = true;
            $("#mailSubject").addClass("border-danger");
        }
        
        if (body == null || body == undefined || body == "" || body.length <= 0) {
            error = true;
            $("#mailBody").addClass("border-danger");
        }

        if (venId == null || venId == undefined || venId == "" || venId.length <= 0) {
            error = true;
            $("#emailVenId").next().find(".select2-selection").addClass("border-danger");
        }
         
        if(venId=='dflt')
		{
			dfltSubj = subj;
			dfltBody = body;
		} 

        if (error == false) {
            $("#mainLoader").show();

            var ajaxUrl = rootAppName + "/rest/vendor/add-email-setup";

            if ($("#updVndrEmailSetup").val() == "1") {
                ajaxUrl = rootAppName + "/rest/vendor/update-email-setup";
            }

            var inputData = {
                //cmpyId: cmpyId,
                venId: venId,
                subj: subj,
                body: body,
            };

            $.ajax({
                headers: ajaxHeader,
                data: JSON.stringify(inputData),
                url: ajaxUrl,
                type: 'POST',
                dataType: 'json',
                success: function(response) {
                    if (response.output.rtnSts == 0) {
                        customAlert("#mainNotify", "alert-success", 'Vendor email data saved successfully.');
           				$("#mainLoader").hide();
                    } else {
                        var loopLimit = response.output.messages.length;
                        var errList = '<ul class="m-0 pl-1">';
                        for (var i = 0; i < loopLimit; i++) {
                            errList += '<li>' + response.output.messages[i] + '</li>';
                        }
                        errList += '</ul>';
                        customAlert("#mainNotify", "alert-danger", errList);

                        $("#mainLoader").hide();
                    }
                },
                error: function(jqXHR, textStatus, errorThrown) {
                    if (jqXHR.status == 401) {
                        validateAuthCode();
                    }
                    $("#mainLoader").hide();
                }
            });
        }
    });
	
    $("#addPymntMthd").click(function() {
        var pymntMthd = $("#pymntType").val();
        var checked = ($("#pymntMthdTbl tbody tr").length) ? "" : "checked";
        
        if (pymntMthd != "" && pymntMthd != null && pymntMthd != undefined && $("#pymntMthdTbl tbody tr[data-pymnt-mthd='" + pymntMthd + "']").length == 0) {
            var tblRow = '<tr data-pymnt-mthd="' + pymntMthd + '" data-mthd-id="' + $("#pymntType option:selected").attr("data-mthd-id") + '">' +
                '<td>' +
                '<div class="form-radio form-radio-flat m-0">' +
                '<label class="form-check-label">' +
                '<input type="radio" class="form-check-input" name="default-pymnt-mthd" '+ checked +'>' +
                '<i class="input-helper"></i>' +
                '</label>' +
                '</div>' +
                '</td>' +
                '<td>' + $("#pymntType option:selected").text() + '</td>' +
                '<td><i class="icon-trash text-danger font-weight-bold c-pointer delete-pymnt-mthd"></i></td>' +
                '</tr>';
            $("#pymntMthdTbl tbody").append(tblRow);
            $("#pymntType").val("").trigger("change");
        }
    });

    $("#addVndrGlRow").click(function() {
        $("#vendorGlForm .select2-selection, #vendorGlForm .form-control").removeClass("border-danger");
        var errFlg = false;
        var vndrGlAcc = $("#vndrGlAcc").val();
        var vndrSubAcc = $("#vndrSubAcc").val();
        var vndrGlRmk = $("#vndrGlRmk").val();

        if (vndrGlAcc == "" || vndrGlAcc == null || vndrGlAcc == undefined) {
            errFlg = true;
            $("#vndrGlAcc").next().find(".select2-selection").addClass("border-danger");
        }

        if ($("#vndrSubAcc option").length > 1) {
            if (vndrSubAcc == "" || vndrSubAcc == null || vndrSubAcc == undefined) {
                errFlg = true;
                $("#vndrSubAcc").next().find(".select2-selection").addClass("border-danger");
            }
        }

        if (vndrGlRmk == "" || vndrGlRmk == null || vndrGlRmk == undefined) {
            errFlg = true;
            $("#vndrGlRmk").addClass("border-danger");
        }

        if (errFlg == false) {
            $("#vndrGlTbody tr.no-record").remove();

            var tblRow = '<tr data-gl-acc="' + vndrGlAcc + '" data-gl-sub-acc="' + vndrSubAcc + '" data-gl-rmk="' + vndrGlRmk + '">' +
                '<td>' + vndrGlAcc + '</td>' +
                '<td>' + vndrSubAcc + '</td>' +
                '<td>' + vndrGlRmk + '</td>' +
                '<td><i class="icon-trash text-danger menu-icon icon-pointer delete-gl-data"></i></td>' +
                '</tr>';
            $("#vndrGlTbody").append(tblRow);

            $("#vndrGlAcc, #vndrSubAcc").val("").trigger("change");
            $("#vndrGlRmk").val("");
        }
    });

    $("#vndrGlTbody").on("click", ".delete-gl-data", function() {
        $(this).closest("tr").remove();

        if ($("#vndrGlTbody tr").length == 0) {
            $("#vndrGlTbody").html('<tr class="no-record"><td colspan="4">No GL record added.</td></tr>');
        }
    });

    $("#pymntMthdTbl").on("click", ".delete-pymnt-mthd", function() {
        $(this).closest("tr").remove();

        if ($("#pymntMthdTbl tbody input[type='radio'][name='default-pymnt-mthd']:checked").length == 0) {
            $("#pymntMthdTbl tbody tr").eq(0).find("input[type='radio'][name='default-pymnt-mthd']").prop("checked", true);
        }
    });

    $("#vndrGlAcc").change(function() {
        var glAcct = $(this).val();

        if (glAcct != "" && glAcct != null && glAcct != undefined) {
            getGlSubAccList(glAcct, $("#vndrSubAcc"));
        } else {
            $("#vndrSubAcc").val("").trigger("change");
            $("#vndrSubAcc").attr("disabled", "disabled");
        }
    });
    
    $("#addVndrSetting").click(function(){
    	$("#adtnlInfoSection .form-control, #adtnlInfoSection .select2-selection").removeClass("border-danger");
    	
    	var errFlg = false;
    	var venId = $("#venIdSetting").val();
    	var brh = $("#brhVenSetting").val();
    	var category = $("#categorySetting").val();
    	var trnsStsActn = $("#trnsStsActn").val();
    	var trnsStsRsn = $("#trnsStsRsn").val();
    	var trnsSts = $("#trnsSts").val();
    	var notifyUsr = $("#notifyUsr").val().toString();
    	
    	if( venId == undefined || venId == null || venId == "" ){
    		errFlg = true;
    		$("#venIdSetting").next().find(".select2-selection").addClass("border-danger");
    	}
    	
    	if( brh == "" && category == "" && trnsStsActn == "" && notifyUsr == "" ){
    		errFlg = true;
    		customAlert("#mainNotify", "alert-danger", "Please select any field to save.");
    	}
    	
    	if( trnsStsActn != null && trnsStsActn != "" && trnsStsActn != undefined ){
    		if( trnsStsActn == "A" ){
    			if( trnsSts == undefined || trnsSts == null || trnsSts == "" ){
		    		errFlg = true;
		    		$("#trnsSts").next().find(".select2-selection").addClass("border-danger");
		    	}else{
		    		trnsStsRsn = "";
		    	}
    		}else{
    			if( trnsStsRsn == undefined || trnsStsRsn == null || trnsStsRsn == "" ){
		    		errFlg = true;
		    		$("#trnsStsRsn").next().find(".select2-selection").addClass("border-danger");
		    	}else{
		    		trnsSts = "";
		    	}
    		}
    	}else{
    		trnsStsActn = "", trnsStsRsn = "", trnsSts = "";
    	}
    	
    	if( errFlg == false ){
    		$("#mainLoader").show();
    		
    		var glAcctListArr = [];
    		$("#vndrGlTbody tr").each(function(index, row) {
	            var $row = $(row);
	            var glAcc = $row.attr("data-gl-acc");
	            var glSubAcc = $row.attr("data-gl-sub-acc");
	            var glRmk = $row.attr("data-gl-rmk");
	
	            var glObj = {
	            	cmpyId: glblCmpyId,
	            	venId: venId,
	                bscGlAcct: glAcc,
	                sacct: glSubAcc,
	                sacctTxt: glSubAcc,
	                distRmk: glRmk
	            };
	
	            glAcctListArr.push(glObj);
	        });
    		
    		var inputData = {
    			cmpyId: glblCmpyId,
    			venId: venId,
    			vchrBrh: brh,
    			vchrCat: category,
    			trsStsActn: trnsStsActn,
    			trsSts: trnsSts,
    			trsRsn: trnsStsRsn,
    			ntfUsr: notifyUsr,
    			isActive: $("input[type='radio'][name='isActive']:checked").val(),
    			glAcctList: glAcctListArr,
    		};
    		
    		$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/vendor/add-dflt-setting',
				type: 'POST',
				dataType : 'json',
				success: function(response){						
					if( response.output.rtnSts == 0 ){						
						clearVendorDefaultSetting();
						
						getVendorDefaultSetting();
						
						customAlert("#mainNotify", "alert-success", "Vendor additional information saved successfully.");
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
					}
					
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					$("#mainLoader").hide();
				}
			});
    	}
    	
    });
    
    $("#venIdSetting").change(function(){
    	var venId = $(this).val();
    	
    	if( venId != undefined && venId != null && venId != "" ){
    		$("#mainLoader").show();
	
			var inputData = {
				cmpyId: glblCmpyId,
				venId: venId,
			};
			
			$.ajax({
				headers: ajaxHeader,
				url : rootAppName + '/rest/vendor/read-dflt-setting',
				data : JSON.stringify( inputData ),
				type: 'POST',
				dataType: 'json',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						var loopLimit = response.output.fldTblVndrDfltStng.length;
						var glTblRows = '', vndrGlAcc = '', vndrSubAcc = '', vndrGlRmk = '';
						
						if( loopLimit ){
							var innerLoopLimit = response.output.fldTblVndrDfltStng[0].glAcctList.length;
						
							$("#brhVenSetting").val(response.output.fldTblVndrDfltStng[0].vchrBrh).trigger("change");
							$("#categorySetting").val(response.output.fldTblVndrDfltStng[0].vchrCat).trigger("change");
							$("#trnsStsActn").val(response.output.fldTblVndrDfltStng[0].trsStsActn).trigger("change");
							if( response.output.fldTblVndrDfltStng[0].trsStsActn == "A" ){
								$("#trnsSts").val(response.output.fldTblVndrDfltStng[0].trsSts).trigger("change");
								$("#trnsStsRsn").attr("data-selected", "");
							}else{
								$("#trnsStsRsn").attr("data-selected", response.output.fldTblVndrDfltStng[0].trsRsn);
								$("#trnsSts").val("").trigger("change");
							}
							
							if( response.output.fldTblVndrDfltStng[0].ntfUsr != undefined ){
								$("#notifyUsr").val( response.output.fldTblVndrDfltStng[0].ntfUsr.trim().split(",") ).trigger("change");
							}
							
							$("input[type='radio'][name='isActive'][value='"+ response.output.fldTblVndrDfltStng[0].isActive +"']").prop("checked", true);
							
							for( var i=0; i<innerLoopLimit; i++ ){
								vndrGlAcc = response.output.fldTblVndrDfltStng[0].glAcctList[i].bscGlAcct;
								vndrSubAcc = response.output.fldTblVndrDfltStng[0].glAcctList[i].sacct;
								vndrGlRmk = response.output.fldTblVndrDfltStng[0].glAcctList[i].distRmk;
								
								glTblRows += '<tr data-gl-acc="' + vndrGlAcc + '" data-gl-sub-acc="' + vndrSubAcc + '" data-gl-rmk="' + vndrGlRmk + '">' +
								                '<td>' + vndrGlAcc + '</td>' +
								                '<td>' + vndrSubAcc + '</td>' +
								                '<td>' + vndrGlRmk + '</td>' +
								                '<td><i class="icon-trash text-danger menu-icon icon-pointer delete-gl-data"></i></td>' +
                							'</tr>';
							}
							$("#vndrGlTbody").html(glTblRows);
							$('#vendorGlForm').collapse("show")
						}else{
							$("#brhVenSetting, #categorySetting, #trnsStsActn, #trnsStsRsn, #notifyUsr, #vndrGlAcc, #vndrSubAcc").val("").trigger("change")
							$("#vndrGlRmk").val("");
							$("#vndrGlTbody").html("");
						}
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
					}
					$("#mainLoader").hide();		
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					$("#mainLoader").hide();
				}
			});
    	}else{
    		$("#brhVenSetting, #categorySetting, #trnsStsActn, #trnsStsRsn, #notifyUsr, #vndrGlAcc, #vndrSubAcc").val("").trigger("change")
			$("#vndrGlRmk").val("");
			$("#vndrGlTbody").html("");
    	}
    });
    
    $("#vendorSettingContent").on("click", ".edit-vndr-setg", function(){
		var $tr = $(this).closest( "tr" );
		
		$("#venIdSetting").val( $tr.attr("data-ven-id") ).trigger("change");
	});

});


function clearVendorDefaultSetting(){
	$("#venIdSetting, #brhVenSetting, #categorySetting, #trnsStsActn, #trnsStsRsn, #trnsSts, #notifyUsr, #vndrGlAcc, #vndrSubAcc").val("").trigger("change")
	$("#vndrGlRmk").val("");
	$("#vndrGlTbody").html("");
}


function clearVndrBankArea() {
	$("#venCmpyId, #achVenId").removeAttr("disabled");
	$("#updVndr").val("0");
	$("#achVenId").attr("data-default-value", "");

	$("#bankDetails tbody tr .bank-country").val("").trigger("change");
	$("#bankDetails tbody tr.more-bank, #pymntMthdTbl tbody tr").remove();
	$("#vndrStpFrm .form-control, #vndrStpFrm .select2-selection").removeClass("border-danger");
	$("#vndrStpFrm")[0].reset();
	$("#vndrStpFrm .form-control.select2").val("").trigger("change");
	$("#bankDetails tbody tr.more-bank").remove();
}

function getVendorData() {
	$("#mainLoader").show();

	var inputData = {
		cmpyId: "",
		venId: "",
	};

	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify(inputData),
		url: rootAppName + '/rest/vendor/read-ven',
		type: 'POST',
		dataType: 'json',
		success: function(response) {
			if (response.output.rtnSts == 0) {
				var loopSize = response.output.fldTblVendor.length;
				var innerLoopSize = 0;
				var vndrId = '',
					cmpyId = '',
					vndrInfo = '',
					bankAccNumbers = '';
				var tbodyHtml = '';
				for (var i = 0; i < loopSize; i++) {
					bankAccNumbers = '';
					cmpyId = response.output.fldTblVendor[i].cmpyId;
					vndrId = response.output.fldTblVendor[i].venId;

					innerLoopSize = response.output.fldTblVendor[i].bnkList.length;

					for (var j = 0; j < innerLoopSize; j++) {
					
						//(' + response.output.fldTblVendor[i].bnkList[j].extRef + ')
					
						bankAccNumbers += '<div class="mb-2">' + response.output.fldTblVendor[i].bnkList[j].bnkKey + ', ' + response.output.fldTblVendor[i].bnkList[j].bnkAccNo + ", " + response.output.fldTblVendor[i].bnkList[j].bnkNm + '</div>';
					}

					vndrInfo = '<h6 class="mb-2">' + cmpyId + '</h6>';
					vndrInfo += '<p class="mb-0">' + vndrId + ' - ' + response.output.fldTblVendor[i].venNm + '</p>';

					tbodyHtml += '<tr data-cmpy-id="' + cmpyId + '" data-vndr-id="' + vndrId + '">' +
						'<td>' + (i + 1) + '</td>' +
						'<td>' + vndrInfo + '</td>' +
						'<td>' + bankAccNumbers + '</td>' +
						'<td><i class="icon-menu icon-note  menu-icon text-warning edit-vndr-info c-pointer font-weight-bold" title="Edit"></i></td>' +
						'</tr>';
				}
				$("#vndrAchTbl tbody").html(tbodyHtml);
			} else {
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for (var i = 0; i < loopLimit; i++) {
					errList += '<li>' + response.output.messages[i] + '</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}

			$("#mainLoader").hide();
		},
		error: function(jqXHR, textStatus, errorThrown) {
			if (jqXHR.status == 401) {
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}


function checkVndrData(cmpyId, venId) {
	$("#mainLoader").show();

	var inputData = {
		cmpyId: cmpyId,
		venId: venId,
	};

	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify(inputData),
		url: rootAppName + '/rest/vendor/read-ven',
		type: 'POST',
		dataType: 'json',
		success: function(response) {
			if (response.output.rtnSts == 0) {
				if (response.output.nbrRecRtn > 0) {
					if (response.output.fldTblVendor[0].cty != undefined) {
						$("#venCty").val(response.output.fldTblVendor[0].cty).trigger("change");
					}

					if (response.output.fldTblVendor[0].payMthd != undefined) {
						$("#pymntType").val(response.output.fldTblVendor[0].payMthd).trigger("change");
					}

					var loopSize = 0;
					if (response.output.fldTblVendor[0].bnkList != undefined) {
						loopSize = response.output.fldTblVendor[0].bnkList.length;
					}

					for (var i = 0; i < loopSize; i++) {
						if (i == 0) {
							$("#bankDetails tbody tr").find(".bank-key").val(response.output.fldTblVendor[0].bnkList[i].bnkKey);
							$("#bankDetails tbody tr").find(".bank-acc-number").val(response.output.fldTblVendor[0].bnkList[i].bnkAccNo);
							$("#bankDetails tbody tr").find(".bank-acc-name").val(response.output.fldTblVendor[0].bnkList[i].bnkNm);
							$("#bankDetails tbody tr").find(".bank-reference").val(response.output.fldTblVendor[0].bnkList[i].extRef);
						} else {
							var bankRow = '<tr class="more-bank">' +
								'<td class="text-center"><i class="icon-minus c-pointer text-danger delete-bank-row  menu-icon" title="Delete"></i></td>' +
								'<td><input type="text" class="form-control bank-key" value="' + response.output.fldTblVendor[0].bnkList[i].bnkKey + '"></td>' +
								'<td><input type="text" class="form-control bank-acc-number number-only" value="' + response.output.fldTblVendor[0].bnkList[i].bnkAccNo + '"></td>' +
								'<td><input type="text" class="form-control bank-acc-name" value="' + response.output.fldTblVendor[0].bnkList[i].bnkNm + '"></td>' +
								'<td><input type="text" class="form-control bank-reference" value="' + response.output.fldTblVendor[0].bnkList[i].extRef + '"></td>' +
								'</tr>';
							$("#bankDetails tbody").append(bankRow);

							$(".select2").select2({});
						}
					}

					loopSize = 0;
					if (response.output.fldTblVendor[0].payMthdList != undefined) {
						loopSize = response.output.fldTblVendor[0].payMthdList.length;
					}

					$("#pymntMthdTbl tbody").html("");
					for (var i = 0; i < loopSize; i++) {
						var checked = (response.output.fldTblVendor[0].payMthdList[i].mthdDflt) ? "checked" : "";
						var tblRow = '<tr data-pymnt-mthd="' + response.output.fldTblVendor[0].payMthdList[i].mthdCd + '" data-mthd-id="' + response.output.fldTblVendor[0].payMthdList[i].mthdId + '">' +
							'<td>' +
							'<div class="form-radio form-radio-flat m-0">' +
							'<label class="form-check-label">' +
							'<input type="radio" class="form-check-input" name="default-pymnt-mthd" ' + checked + '>' +
							'<i class="input-helper"></i>' +
							'</label>' +
							'</div>' +
							'</td>' +
							'<td>' + response.output.fldTblVendor[0].payMthdList[i].mthdNm + '</td>' +
							'<td><i class="icon-trash text-danger font-weight-bold c-pointer delete-pymnt-mthd"></i></td>' +
							'</tr>';
						$("#pymntMthdTbl tbody").append(tblRow);
					}

					$("#updVndr").val("1");
				} else {
					$("#updVndr").val("0");

					$("#venCty, #pymntType").val("").trigger("change");
					$("#pymntMthdTbl tbody tr").remove();

					$("#bankDetails tbody tr.more-bank").remove();
					$("#bankDetails tbody .form-control").val("");
				}
			} else {
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for (var i = 0; i < loopLimit; i++) {
					errList += '<li>' + response.output.messages[i] + '</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}

			$("#mainLoader").hide();
		},
		error: function(jqXHR, textStatus, errorThrown) {
			if (jqXHR.status == 401) {
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}

function checkVndrEmailData(cmpyId, venId) {
	$("#mainLoader").show();

	var inputData = {
		cmpyId: cmpyId,
		venId: venId,
	};

	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify(inputData),
		url: rootAppName + '/rest/vendor/read-email-setup',
		type: 'POST',
		dataType: 'json',
		success: function(response) {
			if (response.output.rtnSts == 0) {
				if (response.output.nbrRecRtn > 0) {
					if (response.output.fldTblVendorES[0].subject != undefined) {
						$("#mailSubject").val(response.output.fldTblVendorES[0].subject).trigger("change");
					}				
					if (response.output.fldTblVendorES[0].body != undefined) {
						$("#mailBody").val(response.output.fldTblVendorES[0].body).trigger("change");
					}		
					
					if(venId=='dflt')
					{
						dfltSubj = response.output.fldTblVendorES[0].subject;
						dfltBody=response.output.fldTblVendorES[0].body;
					}
											
					$("#updVndrEmailSetup").val("1");
				} 
				
				else {
					$("#updVndrEmailSetup").val("0");
					$("#mailSubject").val(dfltSubj).trigger("change");
					$("#mailBody").val(dfltBody).trigger("change");	
				}
				
			} else {
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for (var i = 0; i < loopLimit; i++) {
					errList += '<li>' + response.output.messages[i] + '</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}

			$("#mainLoader").hide();
		},
		error: function(jqXHR, textStatus, errorThrown) {
			if (jqXHR.status == 401) {
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}

function getVendorListES( $select, cmpyId, allowDefault=true ){
	if( cmpyId != "" && cmpyId != null && cmpyId != undefined ){
		var inputData = {
			cmpyId : cmpyId,
		};
		
		$.ajax({
			headers: ajaxHeader,
			url: rootAppName + '/rest/vendor/read',
			type: 'POST',
			data : JSON.stringify( inputData ),
			dataType: 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					var loopSize = response.output.fldTblVendor.length;
					var venId = '', venNm = '', venLongNm = '', venCry = '', options = '';	
					
					if( allowDefault ){						
						options = '<option value="dflt">Default</option>';						
					}
				
					for( var i=0; i<loopSize; i++ ){
						venId = response.output.fldTblVendor[i].venId.trim();
						venNm = response.output.fldTblVendor[i].venNm.trim();
						venLongNm = response.output.fldTblVendor[i].venLongNm.trim();
						venCry = response.output.fldTblVendor[i].venCry.trim();
						
						options += '<option value="'+ venId +'" data-vndr-nm="'+ venNm +'" data-vndr-long-nm="'+ venLongNm +'" data-vndr-cry="'+ venCry +'">'+ venId + ' - ' + venNm + '</option>';
					}
					$select.html( options );
					
					if( $select.attr("data-default-value") != undefined && $select.attr("data-default-value") != "" ){
						$select.val( $select.attr("data-default-value") ).trigger("change");
					}else{
						//$("#mainLoader").hide();
					}
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
					//$("#mainLoader").hide();
				}			
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				//$("#mainLoader").hide();
			}
		});
	}else{
		setTimeout(function(){
			getVendorListES( $select, glblCmpyId );
		}, 500)
	}
}

function getUserList($select){	
	$.ajax({
		headers: ajaxHeader,
		url : rootAppName + '/rest/user/read-app',
		data : JSON.stringify({ usrId: "" }),
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblUsers.length;
				var options = '', userNm = '', userId = '', emailId = '';	
				for( var i=0; i<loopSize; i++ ){
					userId = response.output.fldTblUsers[i].userId;
					userNm = response.output.fldTblUsers[i].userNm;
					emailId = response.output.fldTblUsers[i].emailId;

					if(response.output.fldTblUsers[i].actvSts.trim() == "true"){
						options += '<option value="'+ emailId +'">'+ userNm +'</option>';
					}
				}
				
				$select.html( options );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}		
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
		}
	});
}


function getVendorDefaultSetting(){
	$("#mainLoader").show();
	
	var inputData = {
		cmpyId: "",
		venId: "",
	};
	
	$.ajax({
		headers: ajaxHeader,
		url : rootAppName + '/rest/vendor/read-dflt-setting',
		data : JSON.stringify( inputData ),
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopLimit = response.output.fldTblVndrDfltStng.length;
				var trnStsActn = '', actvSts = '', glSts = '';
				
				var vndrSetgTable = '<table class="table table-hover table-border">' +
										'<thead>' +
											'<tr class="thead-clr">' + 
												'<th>Vendor</th>' +
												'<th>Branch</th>' +
												'<th>Category</th>' +
												'<th>Trn Status Action</th>' +
												'<th>Trn Status</th>' +
												'<th>Trn Reason</th>' +
												'<th>Notify Emails</th>' +
												'<th>GL Setup</th>' +
												'<th>Active Status</th>' +
												'<th>Edit</th>' +
											'<tr>' + 
										'</thead>' +
										'<tbody>';
				
				for( var i=0; i<loopLimit; i++ ){
					trnStsActn = '', actvSts = '';
					
					if( response.output.fldTblVndrDfltStng[i].trsStsActn == "H" ){
						trnStsActn = "<div class='badge badge-danger'>Held</div>";
					}else if( response.output.fldTblVndrDfltStng[i].trsStsActn == "C" ){
						trnStsActn = "<div class='badge badge-warning'>Close</div>";
					}else if( response.output.fldTblVndrDfltStng[i].trsStsActn == "A" ){
						trnStsActn = "<div class='badge badge-success'>Approved</div>";
					}
					
					actvSts = (response.output.fldTblVndrDfltStng[i].isActive == "1") ? "<div class='badge badge-success'>Active</div>" : "<div class='badge badge-danger'>Inactive</div>";
					glSts = (response.output.fldTblVndrDfltStng[i].glAcctList.length) ? "<span class='badge badge-pill badge-success'>&nbsp;&nbsp;&nbsp;</span>" : "<span class='badge badge-pill badge-danger'>&nbsp;&nbsp;&nbsp;</span>";
						
					vndrSetgTable += '<tr data-ven-id="'+ response.output.fldTblVndrDfltStng[i].venId +'">'+
									'<td>'+ response.output.fldTblVndrDfltStng[i].venId +'</td>'+
									'<td>'+ response.output.fldTblVndrDfltStng[i].vchrBrh +'</td>'+
									'<td>'+ response.output.fldTblVndrDfltStng[i].vchrCat +'</td>'+
									'<td>'+ trnStsActn +'</td>'+
									'<td>'+ response.output.fldTblVndrDfltStng[i].trsSts +'</td>'+
									'<td>'+ response.output.fldTblVndrDfltStng[i].trsRsn +'</td>'+
									'<td>'+ response.output.fldTblVndrDfltStng[i].ntfUsr +'</td>'+
									'<td>'+ glSts +'</td>'+
									'<td>'+ actvSts +'</td>'+
									'<td><i class="icon-note menu-icon icon-pointer edit-vndr-setg"></i></td>' +
								'</tr>';
				}
				
				vndrSetgTable += '</tbody>' +
									'</table>';
				
				$("#vendorSettingContent").html(vndrSetgTable);
				
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();		
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}