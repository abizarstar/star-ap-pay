$(function(){
	
	getEbsList( $("#bnkStmntFlFrmt") );
	
	$("#uploadBnkStmntFile").change(function(){
		for(i = 0; i < this.files.length ; i++){
			if (this.files && this.files[i]) {
				$("#uploadBnkStmntFile").next().html(this.files[i].name);
			}
		}
	});
	
	$("#upldBankFile").click(function(){
		$("#bnkStmntFlFrmt").next().find(".select2-selection").removeClass("border-danger");
		var formElement = document.forms.namedItem("bankFileImportForm");
		var formData = new FormData(formElement);
		var upload = false;
		var fileName = "";
		var file = [];
		var stmtTyp = $("#bnkStmntFlFrmt").val();
		
		$.each(formData.getAll("file"), function(index, value) {
			if( value.name != "" && value.size > 0 ){
				upload = true;
				fileName = value.name;
			}
		});
		
		if( stmtTyp == "" || stmtTyp == undefined || stmtTyp == null ){
			upload = false;
			$("#bnkStmntFlFrmt").next().find(".select2-selection").addClass("border-danger");
		}
		
		
		formData.append('stmtTyp', stmtTyp);
		formData.append("fileFormat", stmtTyp);
		formData.append("postParam", $("input[type='radio'][name='postParam']:checked").val());
		
		if( upload === true ){
			$("#mainLoader").show();
			$.ajax({
				headers : {
					'user-id' : localStorage.getItem("starOcrUserId"),
					'auth-token' : localStorage.getItem("starOcrAuthToken"),
					'cmpy-id' : localStorage.getItem("starOcrCmpyId"),
					'app-token' : localStorage.getItem("starOcrAppToken"),
					'auth-token': localStorage.getItem("starOcrAuthToken"),
					'app-host' : localStorage.getItem("starOcrAppHost"),
					'app-port' : localStorage.getItem("starOcrAppPort"),
				},
				type: 'POST',
				url: rootAppName + '/rest/bai/pdf',
				data: formData,
				enctype : 'multipart/form-data',
				cache: false,
				processData: false,
				contentType: false,
				success: function(response) {
					if( response.output.rtnSts == 0 ){
						customAlert("#mainNotify", "alert-success", 'File imported successfully.');
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
					}
					
					$("#uploadBnkStmntFile").val("");
					$("#uploadBnkStmntFile").next().html("Choose file...");
						
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					$("#mainLoader").hide();
				}
			});
		}else{
			customAlert("#mainNotify", "alert-danger", 'Please select any valid file.');
		}
	});
	
	
	$(document).on("click", ".get-ftp", function(){
	
		var flNm = $(this).closest('tr').attr('data-fl-nm');
		var flTyp = $(this).closest('tr').attr('data-fl-typ');
		
		processFTPStmt(flNm, flTyp);
	
	});
	
	$("#getFTPBankFile").click(function(){
		
		$("#modalTitle").html("Bank Statement Files");
		getFTPStatement("EBS");
	
	});
	
	$("#getFTPLckBxFile").click(function(){
	
		$("#modalTitle").html("Lockbox Files");
		getFTPStatement("LCK");
	
	});
	
});
	
function getFTPStatement(type)
{
	$("#mainLoader").show();

	var inputData = { 
		typ: type
		};

	$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/bai/get-bnk-stmt-ftp',
			type: 'POST',
			dataType : 'json',
			success: function(response){
				
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblBnkStmt.length;
				var tblRows = '';
				if( loopSize == 0 ){
					tblRows = '<tr><td colspan="2">No record found.</td></tr>';
				}
				for( var i=0; i<loopSize; i++ ){
					
					var disableVal = '';
					var btnNm = '';
					
					if(response.output.fldTblBnkStmt[i].flg == 1)
					{
						disableVal = 'disabled';
						btnNm = 'Processed';
					}
					else
					{
						disableVal = '';
						btnNm = 'Import';
					}
				
					tblRows += '<tr data-fl-nm="' + response.output.fldTblBnkStmt[i].flNm + '" data-fl-typ="' + response.output.fldTblBnkStmt[i].flType + '">'+
									'<td>'+ response.output.fldTblBnkStmt[i].flNm +'</td>'+
									'<td>'+ response.output.fldTblBnkStmt[i].dtTm +'</td>'+
									'<td><button class="btn btn-sm btn-inverse-primary get-ftp" ' + disableVal + ' >' + btnNm +'</button></td>'+
								'</tr>';
				}
				$("#bnkStmtList tbody").html( tblRows );
				
				$("#bnkStmtModal").modal("show");
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
				
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});
		
}


function processFTPStmt(flNm, flTyp)
{
	$("#mainLoader").show();

	var inputData = { 
		flNm: flNm,
		flTyp: flTyp,
		};

	$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/bai/process-bnk-ftp',
			type: 'POST',
			dataType : 'json',
			success: function(response){
				
			if( response.output.rtnSts == 0 ){
			
				getFTPStatement(flTyp);
				}
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});
		
}
	
function tmpSendMail(){
	var inputData = {
		emlFrm: "abizarh@starsoftware.co", 
		emlFrmNm: "Abizar Hasan", 
		emlTo: "yogeshb@starsoftware.co", 
		emlCc: "yogeshb@starsoftware.co", 
		emlBcc: "", 
		emlSub: "Testing mail service from ocr-ap application", 
		emlBody: "Mail body for OCR-AP application.", 
		emlDocId: "1",
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/sendmail/mail',
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				/*$("#attachFiles ul").html("");
				$("#mailFrom, #mailTo, #mailCc, #mailSubject, #mailBody, #attachDocIds").val("");
				$("#transListAction, #listAction").val("").trigger("change");
				
				$("#sendMailModal").modal("hide");*/
				
				customAlert("#popupNotify", "alert-success", '<strong>Mail sent successfully.</strong>');	
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#popupNotify", "alert-danger", errList);
			}
			//$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			//$("#mainLoader").hide();
		}
	});
}