$(function(){

	getVendorList( $("#venId"), glblCmpyId );
	
	getCurrencyList( $("#cry") );
	
	$("#clearRemitDocsFltr").click(function(){
		$("#rmtncSection #cry, #rmtncSection #venId").val("").trigger("change");
		$("#rmtncSection #invcDt, #rmtncSection #pmntDt").val("");
	});
	
	$("#rmtncSection").on("change", "input[type='checkbox'][name='all-remit-doc']", function(){
		if( $(this).prop("checked") ){
			$("#remitDocTbl input[type='checkbox'][name='select-remit-doc']").prop("checked", true);
			$("#printRemitDocs").removeClass("disabled");
			$("#printRemitDocs").removeAttr("disabled", "disabled");
		}else{
			$("#remitDocTbl input[type='checkbox'][name='select-remit-doc']").prop("checked", false);
			$("#printRemitDocs").addClass("disabled");
			$("#printRemitDocs").attr("disabled", "disabled");
		}
		  
		calculateTotal();
	});
	
	$("#remitDocTbl").on("change", "input[type='checkbox'][name='select-remit-doc']", function(){
		if( $("#remitDocTbl input[type='checkbox'][name='select-remit-doc']:checked").length ){
			$("#printRemitDocs").removeClass("disabled");
			$("#printRemitDocs").removeAttr("disabled", "disabled");
		}else{
			$("#printRemitDocs").addClass("disabled");
			$("#printRemitDocs").attr("disabled", "disabled");
		}
		
		if( $("#remitDocTbl input[type='checkbox'][name='select-remit-doc']").length == $("#remitDocTbl input[type='checkbox'][name='select-remit-doc']:checked").length ){
			$("#rmtncSection input[type='checkbox'][name='all-remit-doc']").prop("checked", true);
		}else{
			$("#rmtncSection input[type='checkbox'][name='all-remit-doc']").prop("checked", false);
		}
		
		calculateTotal();
	});
	
	
	$("#printRemitDocs").click(function(){
		$("#mainNotify").html("");
		var vchrNoStr = '', pmntDt = '';
		var pmntDtArr = [];
		$("#remitDocTbl input[type='checkbox'][name='select-remit-doc']:checked").each(function(index, checkbox){
			vchrNoStr += (vchrNoStr.length) ? "," : "";
			vchrNoStr += $(checkbox).closest("tr").attr("data-vchr-no");
			
			pmntDt = $(checkbox).closest("tr").attr("data-pmnt-dt");
			
			if( pmntDtArr.indexOf(pmntDt) < 0 && pmntDt != "" && pmntDt != undefined ){
				pmntDtArr.push(pmntDt);
			}
		});
		
		if( pmntDtArr.length > 1 ){
			customAlert("#mainNotify", "alert-danger", "Two different payment dates are not allowed in remittance.");
			return;
		}
		
		var docUrl = rootAppName + "/rest/print-check/pdf?cmpyId="+glblCmpyId + "&vchrNo=" + vchrNoStr + "&pmntDt=" + pmntDtArr[0];
	
		window.open(docUrl, '_blank');
	});

	$("#searchRemitDocs").click(function(){
		$("#rmtncSection .select2-selection").removeClass("border-danger");
		
		var errFlg = false;
		var invcStrtDate = "", invcEndDate = "";
		var cry = $("#cry").val();
		var venId = $("#venId").val();
		var pmntDt = $("#pmntDt").val();
		var invcDt = $("#invcDt").val();
		if( invcDt != "" && invcDt != null && invcDt != undefined ){
			invcStrtDate = invcDt.split(" - ")[0];
			invcEndDate = invcDt.split(" - ")[1];
			
			invcStrtDate = invcStrtDate.split('/').join('-');
			invcEndDate = invcEndDate.split('/').join('-');
		}

		if( cry == "" || cry == null || cry == undefined ){
			errFlg = true;
			$("#cry").next().find(".select2-selection").addClass("border-danger");
		}

		if( venId == "" || venId == null || venId == undefined ){
			errFlg = true;
			$("#venId").next().find(".select2-selection").addClass("border-danger");
		}
		
		if( errFlg == false ){
			$("#mainLoader").show();
			
			$("#rmtncSection input[type='checkbox'][name='all-remit-doc']").prop("checked", false);
			$("#rmtncSection input[type='checkbox'][name='all-remit-doc']").attr("disabled");
		
			var inputData = {
				cry: cry,
				venId : venId,
				pmntDt : pmntDt,
				invcDt: invcStrtDate,
				invcDtTo: invcEndDate,
			};
			
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/print-check/read',
				type: 'POST',
				dataType : 'json',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						var loopSize = response.output.fldTblDoc.length;
						var tbodyRows = '', vchrNoStr = '', poStr = '', chkNo = "", pmntDtStr = ""; 
						for( var i=0; i<loopSize; i++ ){
							vchrNoStr = "", chkNo = "";
							
							pmntDtStr = (response.output.fldTblDoc[i].pmntDtStr != undefined) ? response.output.fldTblDoc[i].pmntDtStr : "";
							
							if( response.output.fldTblDoc[i].poNo == 0 ){
								poStr = "";
							}else{
								if( response.output.fldTblDoc[i].poItm == 0 ){
									poStr = response.output.fldTblDoc[i].poPfx +'-'+ response.output.fldTblDoc[i].poNo;
								}else{
									poStr = response.output.fldTblDoc[i].poPfx +'-'+ response.output.fldTblDoc[i].poNo +'-'+ response.output.fldTblDoc[i].poItm;
								}
							}
							
							if( response.output.fldTblDoc[i].vchrPfx != undefined && response.output.fldTblDoc[i].vchrPfx.trim() != "" ){
								vchrNoStr = response.output.fldTblDoc[i].vchrPfx.trim() +'-'+ response.output.fldTblDoc[i].vchrNo.trim();
							}
							
							if( response.output.fldTblDoc[i].chkNo != undefined ){
								chkNo = response.output.fldTblDoc[i].chkNo;
							}
							
							tbodyRows += '<tr data-invc-no="'+ response.output.fldTblDoc[i].vchrInvNo +'" data-vchr-no="'+ vchrNoStr +'" data-ctl-no="'+ response.output.fldTblDoc[i].ctlNo +'" data-vchr-amt="'+ response.output.fldTblDoc[i].vchrAmt +'" data-vchr-cry="'+ response.output.fldTblDoc[i].vchrCry +'" data-pmnt-dt="'+ pmntDtStr +'">'+
										'<td>'+
											'<div class="form-check form-check-flat inline-checkbox m-0">'+
												'<label class="form-check-label m-0">'+
													'<input type="checkbox" class="form-check-input" name="select-remit-doc">'+
													'&nbsp;<i class="input-helper"></i>'+
												'</label>'+
											'</div>'+
										'</td>'+
										'<td>'+ response.output.fldTblDoc[i].vchrVenId.trim() + "-" + response.output.fldTblDoc[i].vchrVenNm.trim() + '</td>'+
										'<td>'+ response.output.fldTblDoc[i].vchrInvNo +'</td>'+
										'<td>'+ response.output.fldTblDoc[i].vchrInvDtStr +'</td>'+
										'<td>'+ response.output.fldTblDoc[i].vchrDueDtStr +'</td>'+
										'<td>'+ poStr +'</td>'+
										'<td>'+ vchrNoStr +'</td>'+
										'<td>'+ response.output.fldTblDoc[i].vchrBrh +'</td>'+
										'<td class="text-right">'+ response.output.fldTblDoc[i].vchrAmtStr +' '+ response.output.fldTblDoc[i].vchrCry +'</td>'+
										'<td class="text-right">'+ response.output.fldTblDoc[i].discAmtStr +' '+ response.output.fldTblDoc[i].vchrCry +'</td>'+
										'<td class="check-col">'+ chkNo +'</td>'+
										'<td>'+ pmntDtStr +'</td>'+
										'<td>'+ response.output.fldTblDoc[i].crtDttsStr +'</td>'+
									'</tr>';
						}
												
						$("#remitDocTbl tbody").html(tbodyRows);
						
						calculateTotal();
						
						if( loopSize ){
							$("#rmtncSection input[type='checkbox'][name='all-remit-doc']").removeAttr("disabled");
						}else{
							$("#rmtncSection input[type='checkbox'][name='all-remit-doc']").attr("disabled", "disabled");
						}
						
						var $table = $('#remitDocTbl');
					
						$table.floatThead({
							useAbsolutePositioning: true,
							scrollContainer: function($table) {
								return $table.closest('.table-responsive');
							}
						});
						$table.floatThead('reflow');
						
						$("#printRemitDocs").addClass("disabled");
						$("#printRemitDocs").attr("disabled", "disabled");
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
					}
					
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					$("#mainLoader").hide();
				}
			});
		}
	});
	
});


function calculateTotal(){
	var total = 0, i = 0, vchrAmt = 0;
	var vchrCry = ""; 
	var totalCount = 0;
	
	$("#remitDocTbl input[type='checkbox'][name='select-remit-doc']:checked").each(function(index, checkbox){
		if( i==0 ){
			vchrCry = $(checkbox).closest("tr").attr("data-vchr-cry");
		}
		
		vchrAmt = parseFloat( $(checkbox).closest("tr").attr("data-vchr-amt") );
		
		if( !isNaN(vchrAmt) ){
			total += vchrAmt;
		}
		
		i++;
	});
	
	total = total.toFixed(2);
	
	totalCount = i ;
	$("#remitTotal").html(total + " " + vchrCry + "(" + totalCount + ")");
}