$(function(){
	
	getBankData();
	
	//getStxBankList( $("#bankCode") );
	
	getCountryList( $("#bankCountry") );
	
	getCurrencyList( $("#bankCurrency") );
	
	$("#addBankSetupBtn").click(function(){
		clearAddBankArea();

		$("#bankSetupSavedArea").hide();
		$("#addUpdateBankSetup").slideDown();
	});
	
	/*$("#bankCode").change(function(){
		var bnkCd = $(this).val();
		if( bnkCd != "" ){
			$("#bankName").val( $("#bankCode option:selected").text().split(" - ")[1] );
			$("#bankCurrency").val( $("#bankCode option:selected").attr("data-cry") );
			
			getSpecificBankSetupData(bnkCd);
		}
	});*/
	
	$("#bankCode").focusout(function(){
		var bnkCd = $(this).val();
		if( bnkCd != "" ){		
			bnkCd = bnkCd.toUpperCase();	
			getSpecificBankSetupData(bnkCd);
		}
	});
	
	$("#bankSetupAddRow").click(function(){
		var bankRow = '<tr class="more-bank">'+
						'<td><i class="icon-minus c-pointer text-danger delete-bank-row display-5" title="Delete"></i></td>'+
						'<td><input type="text" class="form-control bank-setup-acc-id"></td>'+
						'<td><input type="text" class="form-control bank-setup-desc"></td>'+
						'<td><input type="text" class="form-control bank-setup-acc-number number-only"></td>'+
						'<td><input type="text" class="form-control bank-setup-ach-id number-only"></td>'+
						'<td><input type="text" class="form-control bank-setup-mmb-id"></td>'+
						'<td><div class="form-radio form-radio-flat m-0">'+
	                            '<label class="form-check-label">'+
	                              '<input type="radio" class="form-check-input" name="default-account">'+
	                            '<i class="input-helper"></i></label>'+
	                          '</div>'+
	                    '</td>'+
					'</tr>';
		$("#bankSetupDetails tbody").append(bankRow);
	});
	
	$("#cancelBankSetup").click(function(){
		clearAddBankArea();
		
		getBankData();
		
		$("#addUpdateBankSetup").hide();
		$("#bankSetupSavedArea").slideDown();
	});
	
	$("#ftpConfig input[type='text'].form-control").keyup(function(){
		var bankRemoteIpAddr = $("#bankRemoteIpAddr").val().trim();
		var bankSftpPort = $("#bankSftpPort").val().trim();
		var bankSftpUserId = $("#bankSftpUserId").val().trim();
		var bankSftpPassword = $("#bankSftpPassword").val().trim(); 
		
		if( bankRemoteIpAddr == "" && bankSftpPort == "" && bankSftpUserId == "" && bankSftpPassword == "" ){
			$("#saveBankSetup").show();
		}else{
			$("#saveBankSetup").hide();
		}
	});
	
	
	$("#textSftpConn").click(function(){
		$("#ftpConfig input.form-control, #ftpConfig .select2-selection").removeClass("border-danger");
		
		var bankRemoteIpAddr = $("#bankRemoteIpAddr").val().trim();
		var bankSftpPort = $("#bankSftpPort").val().trim();
		var bankSftpUserId = $("#bankSftpUserId").val().trim();
		var bankSftpPassword = $("#bankSftpPassword").val().trim();
		var bankSftpTyp = $("#bankSftpTyp").val();
		var bankSftpFilePath = $("#bankSftpFilePath").val().trim();
		var errFlg = false;
		
		if( bankRemoteIpAddr == "" ){
			errFlg = true;
			$("#bankRemoteIpAddr").addClass("border-danger");
		}
		
		if( bankSftpPort == "" ){
			errFlg = true;
			$("#bankSftpPort").addClass("border-danger");
		}
		
		if( bankSftpUserId == "" ){
			errFlg = true;
			$("#bankSftpUserId").addClass("border-danger");
		}
		
		if( bankSftpPassword == "" ){
			errFlg = true;
			$("#bankSftpPassword").addClass("border-danger");
		}
		
		if( bankSftpTyp == "" || bankSftpTyp == null || bankSftpTyp == undefined ){
			errFlg = true;
			$("#bankSftpTyp").next().find(".select2-selection").addClass("border-danger");
		}
		
		/*if( bankSftpFilePath == "" ){
			errFlg = true;
			$("#bankSftpFilePath").addClass("border-danger");
		}*/
		
		if( errFlg == false ){
			$("#mainLoader").show();
			
			var inputData = {
				bnkCode : "",
				bnkNm : "",
				bnkCty : "",
				bnkRoutNo : "",
				bnkCity : "",
				bnkBrh : "",
				bnkCry : "",
				bnkSwiftCode : "",
				bnkHost : bankRemoteIpAddr,
				bnkPort : bankSftpPort,
				bnkFtpUser : bankSftpUserId,
				bnkFtpPass : bankSftpPassword,
				bnkKeyPath : "",
				bnkSftpTyp: bankSftpTyp,
				bnkSftpFilePath: bankSftpFilePath,
				bankAcctList : [],
			};
						
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/bank/vldt-ftp',
				type: 'POST',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						customAlert("#mainNotify", "alert-info", "Connection successfull.");
						$("#saveBankSetup").show();
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
						$("#saveBankSetup").hide();
					}
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}else if( jqXHR.status == 500 ){
						customAlert("#mainNotify", "alert-danger", jqXHR.responseText);
						$("#saveFtpSetting").hide();
					}
					console.log(jqXHR);
					console.log(textStatus);
					console.log(errorThrown);
					$("#mainLoader").hide();
				}
			});
		}
	});
	
	
	$("#saveBankSetup").click(function(){
		$(".form-control, .select2-selection").removeClass("border-danger");
		
		var errFlg = false;
		
		var bankCode = $("#bankCode").val().trim();
		var bankName = $("#bankName").val().trim();
		var bankCountry = $("#bankCountry").val();
		var bankRoutingNo = $("#bankRoutingNo").val().trim();
		var bankCity = $("#bankCity").val().trim();
		var bankBranch = $("#bankBranch").val().trim();
		var bankCurrency = $("#bankCurrency").val().trim();
		var bankSwiftCode = $("#bankSwiftCode").val().trim();
		var bankRemoteIpAddr = $("#bankRemoteIpAddr").val().trim();
		var bankSftpPort = $("#bankSftpPort").val().trim();
		var bankSftpUserId = $("#bankSftpUserId").val().trim();
		var bankSftpPassword = $("#bankSftpPassword").val().trim();
		var bankSftpTyp = $("#bankSftpTyp").val();
		var bankSftpFilePath = $("#bankSftpFilePath").val().trim();
		var bankAccArr = [];
		
		if( bankCode == "" ){
			errFlg = true;
			$("#bankCode").addClass("border-danger");
		}
		
		if( bankName == "" ){
			errFlg = true;
			$("#bankName").addClass("border-danger");
		}
		
		if( bankCountry == "" ){
			errFlg = true;
			$("#bankCountry").next().find(".select2-selection").addClass("border-danger");
		}
		
		if( bankRoutingNo == "" ){
			errFlg = true;
			$("#bankRoutingNo").addClass("border-danger");
		}
		
		if( bankCity == "" ){
			errFlg = true;
			$("#bankCity").addClass("border-danger");
		}
		
		if( bankBranch == "" ){
			errFlg = true;
			$("#bankBranch").addClass("border-danger");
		}
		
		if( bankCurrency == "" ){
			errFlg = true;
			$("#bankCurrency").addClass("border-danger");
		}
		
		$("#bankSetupDetails tbody tr").each(function(index, row){
			var accId = $(row).find(".bank-setup-acc-id").val().trim();
			var accDesc = $(row).find(".bank-setup-desc").val().trim();
			var accNmbr = $(row).find(".bank-setup-acc-number").val().trim();
			var achId = $(row).find(".bank-setup-ach-id").val().trim();
			var mmbId = $(row).find(".bank-setup-mmb-id").val().trim();
			
			if( accId == "" ){
				errFlg = true;
				$(row).find(".bank-setup-acc-id").addClass("border-danger");
			}
			
			if( accDesc == "" ){
				errFlg = true;
				$(row).find(".bank-setup-desc").addClass("border-danger");
			}
			
			if( accNmbr == "" ){
				errFlg = true;
				$(row).find(".bank-setup-acc-number").addClass("border-danger");
			}
			
			/*if( achId == "" ){
				errFlg = true;
				$(row).find(".bank-setup-ach-id").addClass("border-danger");
			}
			
			if( mmbId == "" ){
				errFlg = true;
				$(row).find(".bank-setup-mmb-id").addClass("border-danger");
			}*/
			
			var bankAccObj = {
				bnkAcct : accId,
				bnkAcctDesc : accDesc,
				bnkAcctNo : accNmbr,
				bnkAchId: achId,
				bnkMmbId: mmbId,
				isDefault: $(row).find("input[type='radio'][name='default-account']").prop("checked"),
			};
			
			bankAccArr.push( bankAccObj );
		});
		
		if( errFlg == false ){
			$("#mainLoader").show();
			
			var inputData = { 
				bnkCode : bankCode,
				bnkNm : bankName,
				bnkCty : bankCountry,
				bnkRoutNo : bankRoutingNo,
				bnkCity : bankCity,
				bnkBrh : bankBranch,
				bnkCry : bankCurrency,
				bnkSwiftCode : bankSwiftCode,
				bnkHost : bankRemoteIpAddr,
				bnkPort : bankSftpPort,
				bnkFtpUser : bankSftpUserId,
				bnkFtpPass : bankSftpPassword,
				bnkKeyPath : "",		
				bnkSftpTyp: bankSftpTyp,
				bnkSftpFilePath: bankSftpFilePath,		
				bankAcctList : bankAccArr,
			};
			
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/bank/upd',
				type: 'POST',
				dataType : 'json',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						clearAddBankArea();
						
						customAlert("#mainNotify", "alert-success", 'Bank data saved successfully.');
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
					}
					
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					$("#mainLoader").hide();
				}
			});
		}
	});
	
	$("#bankSetupTbl").on("click", ".edit-bank-info", function(){
		$("#bankCode").attr("disabled", "disabled");
		$("#bankCode").val( $(this).closest("tr").attr("data-bank-code") ).trigger("focusout");		
	});
	
	$("#bankSetupDetails").on("click", ".delete-bank-row", function(){
		$(this).closest("tr").remove();
	});
	
});

function clearAddBankArea(){
	$("#bankCode, #bankCountry, #bankRoutingNo").removeAttr("disabled");
	
	$("#bankSetupForm .form-control").removeClass("border-danger");
	$("#bankSetupForm")[0].reset();
	$("#bankSetupForm .form-control.select2").val("").trigger("change");
	$("#bankSetupDetails tbody tr.more-bank").remove();
}


function getBankData(){
	$("#mainLoader").show();
		
	var inputData = { 
		bnkCode : "",
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/bank/read',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblBank.length;
				var innerLoopSize = 0;
				var bnkCode = '', bankAccNumbers = '', bankInfo = '';
				var tbodyHtml = '';
				for( var i=0; i<loopSize; i++ ){
					bankAccNumbers = '';
					bnkCode = response.output.fldTblBank[i].bnkCode;
					innerLoopSize = response.output.fldTblBank[i].bankAcctList.length;
					
					for( var j=0; j<innerLoopSize; j++ ){
						bankAccNumbers += '<div class="mb-2">' + response.output.fldTblBank[i].bankAcctList[j].bnkAcct + ' - ' + response.output.fldTblBank[i].bankAcctList[j].bnkAcctNo + ' (' + response.output.fldTblBank[i].bankAcctList[j].bnkAcctDesc + ')';
						if( response.output.fldTblBank[i].bankAcctList[j].default ){
							bankAccNumbers += ' <i class="icon-check text-success"></i>';
						}
						bankAccNumbers += '</div>';
					}
					
					bankInfo = '<h6 class="mb-2">'+ response.output.fldTblBank[i].bnkCode +' - ' + response.output.fldTblBank[i].bnkNm +'</h6>';
					bankInfo += '<p class="mb-2">Routing No. ' + response.output.fldTblBank[i].bnkRoutNo +'</p>';
					bankInfo += '<p class="mb-0">' + response.output.fldTblBank[i].bnkBrh + ', ' + response.output.fldTblBank[i].bnkCity + ', ' + response.output.fldTblBank[i].bnkCty +'</p>';
					
					tbodyHtml += '<tr data-bank-code="'+ bnkCode +'">'+
									'<td>'+ (i+1) +'</td>'+
									'<td>'+ bankInfo +'</td>'+
									'<td>'+ bankAccNumbers +'</td>'+
									'<td><i class="icon-menu icon-note menu-icon text-warning edit-bank-info c-pointer font-weight-bold" title="Edit"></i></td>'+
								'</tr>';
				}
				$("#bankSetupTbl tbody").html(tbodyHtml);
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}


function getSpecificBankSetupData(bnkCd){
	$("#mainLoader").show();
	
	var inputData = { 
		bnkCode : bnkCd,
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/bank/read',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				if( response.output.fldTblBank.length ){
				 	$("#bankName").val( response.output.fldTblBank[0].bnkNm );
				 	$("#bankCurrency").val( response.output.fldTblBank[0].bnkCry ).trigger("change");
					$("#bankCountry").val( response.output.fldTblBank[0].bnkCty ).trigger("change");
					$("#bankRoutingNo").val( response.output.fldTblBank[0].bnkRoutNo );
					$("#bankCity").val( response.output.fldTblBank[0].bnkCity );
					$("#bankBranch").val( response.output.fldTblBank[0].bnkBrh );
					$("#bankSwiftCode").val( response.output.fldTblBank[0].bnkSwiftCode );
					$("#bankRemoteIpAddr").val( response.output.fldTblBank[0].bnkHost );
					$("#bankSftpPort").val( response.output.fldTblBank[0].bnkPort );
					$("#bankSftpUserId").val( response.output.fldTblBank[0].bnkFtpUser );
					$("#bankSftpPassword").val( response.output.fldTblBank[0].bnkFtpPass );
					$("#bankSftpTyp").val( response.output.fldTblBank[0].bnkSftpTyp ).trigger("change");
					$("#bankSftpFilePath").val( response.output.fldTblBank[0].bnkSftpFilePath );
					
					var loopSize = response.output.fldTblBank[0].bankAcctList.length;
		
					for( var i=0; i<loopSize; i++ ){
						var checked = (response.output.fldTblBank[0].bankAcctList[i].default) ? "checked" : "";
						if( i == 0 ){
							$("#bankSetupDetails tbody tr").find(".bank-setup-acc-id").val( response.output.fldTblBank[0].bankAcctList[i].bnkAcct );
							$("#bankSetupDetails tbody tr").find(".bank-setup-desc").val( response.output.fldTblBank[0].bankAcctList[i].bnkAcctDesc );
							$("#bankSetupDetails tbody tr").find(".bank-setup-acc-number").val( response.output.fldTblBank[0].bankAcctList[i].bnkAcctNo );
							$("#bankSetupDetails tbody tr").find(".bank-setup-ach-id").val( response.output.fldTblBank[0].bankAcctList[i].bnkAchId );
							$("#bankSetupDetails tbody tr").find(".bank-setup-mmb-id").val( response.output.fldTblBank[0].bankAcctList[i].bnkMmbId );
						}else{
							var bankRow = '<tr class="more-bank">'+
											'<td><i class="icon-minus c-pointer text-danger delete-bank-row display-5" title="Delete"></i></td>'+
											'<td><input type="text" class="form-control bank-setup-acc-id" value="'+ response.output.fldTblBank[0].bankAcctList[i].bnkAcct +'"></td>'+
											'<td><input type="text" class="form-control bank-setup-desc" value="'+ response.output.fldTblBank[0].bankAcctList[i].bnkAcctDesc +'"></td>'+
											'<td><input type="text" class="form-control bank-setup-acc-number number-only" value="'+ response.output.fldTblBank[0].bankAcctList[i].bnkAcctNo +'"></td>'+
											'<td><input type="text" class="form-control bank-setup-ach-id number-only" value="'+ response.output.fldTblBank[0].bankAcctList[i].bnkAchId +'"></td>'+
											'<td><input type="text" class="form-control bank-setup-mmb-id" value="'+ response.output.fldTblBank[0].bankAcctList[i].bnkMmbId +'"></td>'+
											'<td><div class="form-radio form-radio-flat m-0">'+
						                            '<label class="form-check-label">'+
						                              '<input type="radio" class="form-check-input" name="default-account" '+ checked +'>'+
						                            '<i class="input-helper"></i></label>'+
						                          '</div>'+
						                    '</td>'+
										'</tr>';
							$("#bankSetupDetails tbody").append(bankRow);
						}
					}
				}else{
					$("#bankCurrency, #bankCountry").val("").trigger("change");
					$("#bankName, #bankCity, #bankBranch, #bankRoutingNo, #bankSwiftCode, #bankRemoteIpAddr, #bankSftpUserId, #bankSftpPassword, #bankSetupDetails tbody .form-control").val("");
					$("#bankSetupDetails tbody tr.more-bank").remove();
				}
				$("#saveBankSetup").show();
				$("#bankSetupSavedArea").hide();		
				$("#addUpdateBankSetup").slideDown();
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}