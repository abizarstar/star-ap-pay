$(function(){
	
	getVendorList($("#crFltrVenId"), glblCmpyId);
	
	getBranchList( $("#crFltrBrh") );
	
	$(".report-tabs .card-body").click(function(){
		$(".report-tabs .card-body").removeClass("active");
		$(this).addClass("active");
		
		$(".report-section").hide();
		$("#reportSection").slideDown();
		$("#"+ $(this).attr("data-report-type")).slideDown();
	});
	
	$("#fltrChkDbstRpt").click(function(){
		getCheckDisbursementReport();
	});
	
	$("#loadMoreChkDbstRpt").click(function(){
		var pageNo = $(this).attr("data-page-no");
		getCheckDisbursementReport(pageNo);
	});
	
	$("#resetChkDbstRpt").click(function(){
		$("#chkDbstRptSection .form-control").val("").trigger("change");
	});
	
	$("#crFltrVenId").change(function(){
		$("#loadMoreChkDbstRpt").attr("disabled", "disabled");
		$("#loadMoreChkDbstRpt").addClass("disabled");
	});
	
	$("#crFltrInvNo, #crFltrInvDt").focusout(function(){
		$("#loadMoreChkDbstRpt").attr("disabled", "disabled");
		$("#loadMoreChkDbstRpt").addClass("disabled");
	});
	
	
	$("#exportChkDisb").click(function(){
		var invcStrtDate = "";
		var invcEndDate = "";
		var venId = $("#crFltrVenId").val();
		var invNo = $("#crFltrInvNo").val();
		var invcDt = $("#crFltrInvDt").val();
		var brh = $("#crFltrBrh").val();
		
		invcDt = (invcDt == null || invcDt == undefined ) ? "" : invcDt;
		
		if( invcDt != "" ){
			invcStrtDate = invcDt.split(" - ")[0];
			invcEndDate = invcDt.split(" - ")[1];
		}
	
		
		var docUrl = rootAppName + "/rest/export/chk-disb?cmpyId="+ glblCmpyId +"&venId=" + venId + "&brh=" + brh + "&invcNo=" + invNo + "&frmInvDt=" + invcStrtDate+ "&toInvcDt=" + invcEndDate;

		window.open(docUrl, '_blank');
	});
	
});


function getCheckDisbursementReport(pageNo=0){
	$("#mainLoader").show();
	
	var invcStrtDate = "";
	var invcEndDate = "";
	var venId = $("#crFltrVenId").val();
	var invNo = $("#crFltrInvNo").val();
	var invcDt = $("#crFltrInvDt").val();
	var brh = $("#crFltrBrh").val();
	
	invcDt = (invcDt == null || invcDt == undefined ) ? "" : invcDt;
	
	if( invcDt != "" ){
		invcStrtDate = invcDt.split(" - ")[0];
		invcEndDate = invcDt.split(" - ")[1];
	}
	
	var inputData = {
		invNo: invNo,
		venId: (venId == null || venId == undefined) ? "" : venId,
		frmInvDt: invcStrtDate,
		toInvcDt: invcEndDate,
		brh : brh,
		pageNo: pageNo,
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/ap/read-check-dsb',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblAPDsbChk.length;
				var tblRows = '';	
				for( var i=0; i<loopSize; i++ ){
					tblRows += '<tr>'+
									'<td>'+ response.output.fldTblAPDsbChk[i].brh +'</td>'+
									'<td>'+ response.output.fldTblAPDsbChk[i].vndrId + "-" + response.output.fldTblAPDsbChk[i].vndrNm +'</td>'+
									'<td>'+ response.output.fldTblAPDsbChk[i].vchrPfx + "-" + response.output.fldTblAPDsbChk[i].vchrNo +'</td>'+
									'<td>'+ response.output.fldTblAPDsbChk[i].invcNo +'</td>'+
									'<td>'+ response.output.fldTblAPDsbChk[i].po +'</td>'+
									'<td>'+ response.output.fldTblAPDsbChk[i].checkNo +'</td>'+
									'<td>'+ response.output.fldTblAPDsbChk[i].amtStr +'</td>'+
									'<td>'+ response.output.fldTblAPDsbChk[i].disAmtStr +'</td>'+
									'<td>'+ response.output.fldTblAPDsbChk[i].invcDt +'</td>'+
									'<td>'+ response.output.fldTblAPDsbChk[i].dueDt +'</td>'+
									'<td>'+ response.output.fldTblAPDsbChk[i].entrDt +'</td>'+
								'</tr>';
				}
				
				if( inputData.pageNo == 0 ){
					$("#chkDbstRptTbl tbody").html( tblRows );
				}else{
					$("#chkDbstRptTbl tbody").append( tblRows );
				}
				
				if( loopSize == 0 ){
					$("#exportChkDisb").addClass("disabled");
					$("#exportChkDisb").attr("disabled", "disabled");
				}else{
					$("#exportChkDisb").removeClass("disabled");
					$("#exportChkDisb").removeAttr("disabled");
				}
				
				if( response.output.eof ){
					$("#loadMoreChkDbstRpt").attr("disabled", "disabled");
					$("#loadMoreChkDbstRpt").addClass("disabled");
				}else{
					$("#loadMoreChkDbstRpt").removeAttr("disabled");
					$("#loadMoreChkDbstRpt").removeClass("disabled");
				}
				
				$("#loadMoreChkDbstRpt").attr("data-page-no", parseInt(inputData.pageNo)+1);
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}