$(function(){

	getProposals();
	
	$("#reqStsFltr").change(function(){
		$("#loadMoreReceiveAutoInvc").attr("disabled", "disabled");
		$("#loadMoreReceiveAutoInvc").addClass("disabled");
					
		getProposals();
	});
	
	$("#propBankCode").change(function(){
	
		if($("#propBankCode").attr("disabled") == undefined)
		{
			var bnkCd = $(this).val();
			if( bnkCd != "" ){
				$("#propBankRouting").val( $("#propBankCode option:selected").attr("data-rout-no") );
				getBankAccountDetails($("#propBankAccNo"), bnkCd);
			}else{
				$("#propBankAccNo").html("");
			}
		}
	});
	
	$("#receiveAutoInvcPymntTbl").on("click", ".view-auto-invc-pymnt", function(){
		$("#mainLoader").show();
		
		$("#previewCol").slideUp();
		$("#previewCol #previewIframe").attr("src", "");
		
		var reqId = $(this).closest("tr").attr("data-req-id");
		var payMthd = $(this).closest("tr").attr("data-pay-mthd");
		var totAmt = $(this).closest("tr").attr("data-tot-amt");
		
		$("#viewAutoInvcPymntModal .request-no").html(reqId);
		$("#viewAutoInvcPymntModal .pymnt-mthd").html(payMthd);
		$("#viewAutoInvcPymntModal .total-amt").html(totAmt);
		
		$("thead th input[type='text']").val('');
		$('#receiverAIPSnglPropData table').floatThead('destroy');
		
		$("#submitImvcPymnt").attr( "data-pay-mthd", $(this).closest("tr").attr("data-pay-mthd") );
		
		$("#viewAutoInvcPymntModal").attr("data-req-id", reqId);
		
		var inputData = { 
			reqId : reqId,
			paySts: "",
			venId: "",
			vchrNo: "",
		};
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/payment/read',
			type: 'POST',
			dataType : 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					var poStr = "", vchrNo = "", amtStr = '', crtDttsStr = '', rowPymntMthd = '';
					var tbl = '', prevCry = '', prevVenId = '', prevVendor = '', venTotal = 0;
					if( response.output.fldTblPayment.length ){
						var loopSize = response.output.fldTblPayment[0].paramInvList.length;
						for( var i=0; i<loopSize; i++ ){
							vchrNo = "", rowPymntMthd = "";
							crtDttsStr = (response.output.fldTblPayment[0].paramInvList[i].crtDttsStr != undefined) ? response.output.fldTblPayment[0].paramInvList[i].crtDttsStr : "";
							
							if( response.output.fldTblPayment[0].paramInvList[i].poNo == 0 ){
								poStr = "";
							}else{
								
								if( response.output.fldTblPayment[0].paramInvList[i].poItm == 0 ){
									//poStr = response.output.fldTblPayment[0].paramInvList[i].poPfx +'-'+ response.output.fldTblPayment[0].paramInvList[i].poNo;
									poStr = '';
								}
								else
								{
									//poStr = response.output.fldTblPayment[0].paramInvList[i].poPfx +'-'+ response.output.fldTblPayment[0].paramInvList[i].poNo +'-'+ response.output.fldTblPayment[0].paramInvList[i].poItm;
									poStr = '';
								}
							}
							
							if( response.output.fldTblPayment[0].paramInvList[i].vchrPfx != undefined && response.output.fldTblPayment[0].paramInvList[i].vchrPfx.trim() != "" ){
								vchrNo = response.output.fldTblPayment[0].paramInvList[i].vchrPfx.trim() + "-" + response.output.fldTblPayment[0].paramInvList[i].vchrNo.trim();
							}
							
							amtStr = response.output.fldTblPayment[0].paramInvList[i].vchrAmtStr;
							if( response.output.fldTblPayment[0].paramInvList[i].vchrCry != undefined ){
								//amtStr += " " + response.output.fldTblPayment[0].paramInvList[i].vchrCry;
							}
							
							var glVal = '';
							if(response.output.fldTblPayment[0].paramInvList[i].glEntry.length > 0)
							{
								glVal = '<strong>' + response.output.fldTblPayment[0].paramInvList[i].glEntry + ' (' + response.output.fldTblPayment[0].paramInvList[i].glEntryDt + ')' + '</strong>';
							}
							else
							{
								glVal = '';
							}
							
							var discStr = "";
							if(response.output.fldTblPayment[0].paramInvList[i].discAmtStr != 0)
							{
								discStr = response.output.fldTblPayment[0].paramInvList[i].discAmtStr +' '+ response.output.fldTblPayment[0].paramInvList[i].vchrCry;
								discStr = response.output.fldTblPayment[0].paramInvList[i].discAmtStr;
							}
							
							if( response.output.fldTblPayment[0].paramInvList[i].payMthds.length ){	
								var innerLoopSize = response.output.fldTblPayment[0].paramInvList[i].payMthds.length;
								for( var j=0; j<innerLoopSize; j++ ){
									if( response.output.fldTblPayment[0].paramInvList[i].payMthds[j].mthdDflt){	
										rowPymntMthd = response.output.fldTblPayment[0].paramInvList[i].payMthds[j].mthdNm;
										break;
									}		
								}
							}
							
							if( prevVenId != "" && prevVenId != response.output.fldTblPayment[0].paramInvList[i].vchrVenId.trim() ){
								tbl += '<tr class="vndr-total font-weight-bold text-dark">'+
										'<td></td>'+
										'<td></td>'+
										'<td>'+ prevVendor +' (Total):</td>'+
										'<td></td>'+
										'<td></td>'+
										'<td></td>'+
										'<td></td>'+
										'<td></td>'+
										'<td class="text-right" colspan="2">'+ venTotal.toFixed(2) +' '+ prevCry +'</td>'+
										'<td class="text-right"></td>'+
										//'<td class="check-col"></td>'+
										'<td></td>'+
										'<td class="pymnt-mthd-col"></td>'+
										'<td></td>'+
										'<td></td>'+
										/*'<td class="check-gl">'+ glVal +'</td>'+ */
									'</tr>';
								venTotal = 0;
							}
							
							var failedRowClass = '';
							
							if(response.output.fldTblPayment[0].paramInvList[i].vchrPrsSts == '' || response.output.fldTblPayment[0].paramInvList[i].vchrPrsSts == 'false')
							{
								failedRowClass = 'failed-payment-row';
							}
							
							tbl += '<tr class="'+ failedRowClass +'" data-invc-no="'+ response.output.fldTblPayment[0].paramInvList[i].vchrInvNo +'" data-vchr-no="'+ vchrNo +'" data-ctl-no="'+ response.output.fldTblPayment[0].paramInvList[i].ctlNo +'">'+
										'<td>'+ (i+1) +'</td>'+
										'<td><i class="icon-eye menu-icon view-doc text-info" aria-hidden="true"></i></td>'+
										'<td>'+ response.output.fldTblPayment[0].paramInvList[i].vchrVenId.trim() + "-" + response.output.fldTblPayment[0].paramInvList[i].vchrVenNm.trim() + '</td>'+
										'<td>'+ response.output.fldTblPayment[0].paramInvList[i].vchrInvNo +'</td>'+
										'<td>'+ response.output.fldTblPayment[0].paramInvList[i].vchrInvDtStr +'</td>'+
										'<td>'+ response.output.fldTblPayment[0].paramInvList[i].vchrDueDtStr +'</td>'+
										'<td>'+ poStr +'</td>'+
										'<td>'+ response.output.fldTblPayment[0].paramInvList[i].vchrPfx + "-" + response.output.fldTblPayment[0].paramInvList[i].vchrNo +'</td>'+
										'<td>'+ response.output.fldTblPayment[0].paramInvList[i].vchrBrh +'</td>'+
										'<td class="text-right">'+ amtStr +'</td>'+
										'<td class="text-right">'+ discStr + '</td>'+
										//'<td class="check-col">'+ response.output.fldTblPayment[0].paramInvList[i].chkNo +'</td>'+
										'<td >'+ response.output.fldTblPayment[0].paramInvList[i].chkNo +'</td>'+
										'<td class="pymnt-mthd-col">'+ rowPymntMthd +'</td>'+
										'<td>'+ crtDttsStr +'</td>'+
										'<td>'+ response.output.fldTblPayment[0].paramInvList[i].vchrDiscDtStr +'</td>'
										/*'<td class="check-gl">'+ glVal +'</td>'+*/
									'</tr>';
									
							prevVenId = response.output.fldTblPayment[0].paramInvList[i].vchrVenId.trim();
							prevCry = response.output.fldTblPayment[0].paramInvList[i].vchrCry;
							venTotal += response.output.fldTblPayment[0].paramInvList[i].vchrAmt;
							prevVendor = response.output.fldTblPayment[0].paramInvList[i].vchrVenId.trim() + "-" + response.output.fldTblPayment[0].paramInvList[i].vchrVenNm.trim();
						}
						
						if( prevVenId != "" ){
							tbl += '<tr class="vndr-total font-weight-bold text-dark">'+
									'<td></td>'+
									'<td></td>'+
									'<td>'+ prevVendor +' (Total):</td>'+
									'<td></td>'+
									'<td></td>'+
									'<td></td>'+
									'<td></td>'+
									'<td></td>'+
									//'<td class="text-right" colspan="2">'+ venTotal.toFixed(2) +' '+ prevCry +'</td>'+
									'<td class="text-right" colspan="2">'+ venTotal.toFixed(2) +'</td>'+
									'<td class="text-right"></td>'+
									//'<td class="check-col"></td>'+
									'<td ></td>'+
									'<td class="pymnt-mthd-col"></td>'+
									'<td></td>'+
									'<td></td>'+
									/*'<td class="check-gl">'+ glVal +'</td>'+*/
								'</tr>';
							venTotal = 0;
						}
						
						/*if( response.output.fldTblPayment[0].companyBnkList[0] != undefined ){
							$("#propCmpyId").val( response.output.fldTblPayment[0].companyBnkList[0].cmpyId );
							$("#propBankCode").val( response.output.fldTblPayment[0].companyBnkList[0].bankCode );
							$("#propBankCty").val( response.output.fldTblPayment[0].companyBnkList[0].bankCty );
						}*/
						
						$("#propBankRouting").val("");
						
						if( response.output.fldTblPayment[0].companyBnkList.length ){						
							loopSize = response.output.fldTblPayment[0].companyBnkList.length;
							
							var bnkList = '';
							for( var i=0; i<loopSize; i++ ){
								bnkList += '<option value="'+ response.output.fldTblPayment[0].companyBnkList[i].bankCode +'" data-rout-no="'+ response.output.fldTblPayment[0].companyBnkList[i].bnkRoutNo +'">'+ response.output.fldTblPayment[0].companyBnkList[i].bankCode +'</option>';
							}
							$("#propBankCode").html( bnkList );
							
							loopSize = response.output.fldTblPayment[0].companyBnkList[0].accoutnList.length;
							$("#propBankRouting").val( response.output.fldTblPayment[0].companyBnkList[0].bnkRoutNo );
							
							var checked = '';
							var accNumbers = '';
							for( var i=0; i<loopSize; i++ ){
								checked = (response.output.fldTblPayment[0].companyBnkList[0].accoutnList[i].default) ? 'selected' : '';
								accNumbers += '<option '+ checked +' value="'+ response.output.fldTblPayment[0].companyBnkList[0].accoutnList[i].bnkAcctNo +'">'+ response.output.fldTblPayment[0].companyBnkList[0].accoutnList[i].bnkAcctNo +' ('+ response.output.fldTblPayment[0].companyBnkList[0].accoutnList[i].bnkAcctDesc +')</option>';
							}
							$("#propBankAccNo").html( accNumbers );
						}
						
					}
					
					$("#receiverAIPSnglPropData table tbody").html(tbl);
					
					if( response.output.fldTblPayment.length ){
						if( response.output.fldTblPayment[0].paySts != undefined && response.output.fldTblPayment[0].paySts == "A" ){
							$("#invcActnSts").val("A").trigger("change");
							$("#propBankAccNo, #propBankCode, #invcActnSts").attr("disabled", "disabled");
							$("#viewAutoInvcPymntModal .modal-footer").hide();
						}else{
							$("#invcActnSts").val("A").trigger("change");
							$("#propBankAccNo, #propBankCode, #invcActnSts").removeAttr("disabled");
							$("#viewAutoInvcPymntModal .modal-footer").show();
						}
					}
					
					if(response.output.fldTblPayment[0].accountNo != '' && response.output.fldTblPayment[0].accountNo != undefined)
					{
						$("#propBankAccNo").val(response.output.fldTblPayment[0].accountNo).trigger('change');
						$("#propBankCode").val(response.output.fldTblPayment[0].bnkCode).trigger('change');
					}
					
					if( payMthd.indexOf("CHK") > -1 || payMthd.indexOf("CHECK") > -1 ){
						$(".check-col").show();
					}else{
						$(".check-col").hide();
					}
					
					if( payMthd.indexOf(",") > -1 ){
						$(".pymnt-mthd-col").show();
					}else{
						$(".pymnt-mthd-col").hide();
					}	
					
					$("#viewAutoInvcPymntModal").modal("show");
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
				}
				
				$("#mainLoader").hide();
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});
	});
	
	$("#viewAutoInvcPymntModal").on("shown.bs.modal", function() {
		var $table = $('#receiverAIPSnglPropData table');
					
		$table.floatThead({
			useAbsolutePositioning: true,
			scrollContainer: function($table) {
				return $table.closest('.table-responsive');
			}
		});
		$table.floatThead('reflow');
	});

	$("#receiverAIPSnglPropData").on("click", ".view-doc", function(){
		var ctlNo = $(this).closest("tr").attr("data-ctl-no");
		ctlNo = (ctlNo != undefined) ? ctlNo : 0;
		
		var docUrl = rootAppName + "/rest/cstmdoc/downloadget?ctlNo=" + ctlNo;
		
		$("#previewIframe").attr("src", docUrl);
		$("#previewCol").slideDown();
		
		var $table = $('#receiverAIPSnglPropData table');
		$table.floatThead('reflow');
	});
	
	$("#closePreview").click(function(){
		$("#previewIframe").attr("src", "");
		$("#previewCol").slideUp(500, function(){
			var $table = $('#receiverAIPSnglPropData table');
			$table.floatThead('reflow');
		});
	});
	
	$("#submitImvcPymnt").click(function(){
		$("#viewAutoInvcPymntModal .select2-selection").removeClass("border-danger");
		
		var errFlg = false;
		var invcActnSts = $("#invcActnSts").val();
		var bankCd = $("#propBankCode").val();
		var acctNo = $("#propBankAccNo").val();
		
		if( bankCd == "" || bankCd == null || bankCd == undefined ){
			errFlg = true;
			$("#propBankCode").next().find(".select2-selection").addClass("border-danger");
		}
		
		if( acctNo == "" || acctNo == null || acctNo == undefined ){
			errFlg = true;
			$("#propBankAccNo").next().find(".select2-selection").addClass("border-danger");
		}
		
		if(errFlg){
			return;
		}else{
			if( invcActnSts == "A" ){
				aprvBlockPropInvc( invcActnSts);
			} else if( invcActnSts == "R" ){
				$("#blockRemark").removeClass("border-danger");
				$("#blockRemark").val("");
				$("#invcBlockConfirmModal").modal("show");
			}
		}
	});
	
	$("#finalBlockAIP").click(function(){
		$("#blockRemark").removeClass("border-danger");
		var remark = $("#blockRemark").val().trim();
		
		if( remark == "" ){
			$("#blockRemark").addClass("border-danger");
			return;
		}else{
			aprvBlockPropInvc("R", remark);
		}
	});
	
	
	$("#loadMoreReceiveAutoInvc").click(function(){
		getProposals( $(this).attr("data-page-no") );
	});
	
	$("#receiveAutoInvcPymntTbl").on("click", ".dwnld-ach-file", function(){
		$("#mainLoader").show();
		
		var flNm = $(this).closest("tr").attr("data-fl-nm");
		var reqId = $(this).closest("tr").attr("data-req-id");
		var paySts = $(this).closest("tr").attr("data-pay-sts");
		
		var inputData = { 
			reqId : reqId,
			paySts: paySts,
			venId: "",
			vchrNo: "",
		};
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/payment/read',
			type: 'POST',
			dataType : 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					if( response.output.fldTblPayment.length && response.output.fldTblPayment[0].flSentSts ){
						$("#dwnldAchFl").attr("data-file-nm", flNm);
						$("#dwnldAchFlModal").modal("show");
					}else{
						downloadAchFile(flNm);
					}
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
				}
				
				$("#mainLoader").hide();
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});
	});
	
	$("#receiveAutoInvcPymntTbl").on("click", ".sftp-ach-file", function(){		
		var flNm = $(this).closest("tr").attr("data-fl-nm");
		var reqId = $(this).closest("tr").attr("data-req-id");
		var paySts = $(this).closest("tr").attr("data-pay-sts");
		
		$("#sftpAchFl").attr("data-fl-nm", flNm);
		$("#sftpAchFl").attr("data-req-id", reqId);
		$("#sftpAchFl").attr("data-pay-sts", paySts);
		
		$("#sftpAchFlModal").modal("show");
	});
	
	$("#sftpAchFl").click(function(){
		$("#mainLoader").show();
		
		var flNm = $("#sftpAchFl").attr("data-fl-nm");
		var reqId = $("#sftpAchFl").attr("data-req-id");
		var paySts = $("#sftpAchFl").attr("data-pay-sts");
		
		var inputData = { 
			reqId : reqId,
			flNm: flNm,
		};
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/payment/sftp-ach',
			type: 'POST',
			dataType : 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					customAlert("#mainNotify", "alert-success", "ACH file scuccessfully uploaded to bank server.");
					
					getProposals();
					
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
				}
				
				$("#sftpAchFlModal").modal("hide");
				$("#mainLoader").hide();
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});
	});
	
});


function getProposals(pageNo=0){
	$("#mainLoader").show();
		
	var inputData = { 
		reqId : "",
		paySts: $("#reqStsFltr").val(),
		venId: "",
		vchrNo: "",
		pageNo: pageNo,
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/payment/read',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblPayment.length;
				var tbody = '', reqSts = "", actnBtn = "", actnOn = "", pymntMthdArr = [], pymntMthdStr = '', amtStr = '';
				for( var i=0; i<loopSize; i++ ){
					reqSts = "", actnOn = "", pymntMthdArr = [], pymntMthdStr = '';
					actnBtn = '<i class="icon-menu icon-paper-plane display-5 text-primary view-auto-invc-pymnt c-pointer"></i>';
										
					if( response.output.fldTblPayment[i].paySts == "S" ){
						reqSts = '<span class="badge badge-warning">Pending</span>';
					}else if( response.output.fldTblPayment[i].paySts == "A" ){
						reqSts = '<span class="badge badge-success">Approved</span>';
						actnBtn += '<i class="icon-menu icon-cloud-download c-pointer font-weight-bold display-5 text-info ml-1 dwnld-ach-file" data-pay-sts="A" title="Download APLink File"></i>';
						if( response.output.fldTblPayment[i].flSentSts ){
							actnBtn += '<i class="icon-menu icon-cloud-upload display-5 text-muted ml-1" data-pay-sts="A" title="Sent to Bank Server"></i>';
						}else{
							actnBtn += '<i class="icon-menu icon-cloud-upload c-pointer font-weight-bold display-5 text-success ml-1 sftp-ach-file" data-pay-sts="A" title="Send to Bank Server"></i>';
						}
					}
					
					if( response.output.fldTblPayment[i].reqActnOnStr != undefined ){
						actnOn = response.output.fldTblPayment[i].reqActnOnStr;
					}
					
					if( response.output.fldTblPayment[i].payMthd != undefined ){
						pymntMthdArr = response.output.fldTblPayment[i].payMthd.split(",");
					}
					
					if( pymntMthdArr.length ){
						for( var y=0; y<pymntMthdArr.length; y++ ){
							var reminder = y % 5;
							if( reminder == 0 ){	
								pymntMthdCls = "badge-primary";
							}else if( reminder == 1 ){
								pymntMthdCls = "badge-info";
							}else if( reminder == 2 ){
								pymntMthdCls = "badge-danger";
							}else if( reminder == 3 ){
								pymntMthdCls = "badge-success";
							}else if( reminder == 4 ){
								pymntMthdCls = "badge-warning";
							}
							
							pymntMthdStr += '<span class="badge '+ pymntMthdCls +'">'+ pymntMthdArr[y] +'</span> &nbsp;';
						}
					}
					
					amtStr = response.output.fldTblPayment[i].amtTotalStr;
					if( response.output.fldTblPayment[i].cry != undefined ){
						amtStr += " " + response.output.fldTblPayment[i].cry;
					}
					
					tbody +='<tr data-req-id="' + response.output.fldTblPayment[i].reqId + '" data-req-dt="' + response.output.fldTblPayment[i].crtdDtStr + '" data-pay-sts="'+ response.output.fldTblPayment[i].paySts +'" data-pay-mthd="'+ response.output.fldTblPayment[i].payMthd +'"  data-fl-nm="' + response.output.fldTblPayment[i].flNm + '" data-req-dt="' + response.output.fldTblPayment[i].crtdDtStr + '" data-tot-amt="'+ amtStr +'">'+
								'<td>'+ (i+1) +'</td>'+
								'<td>'+
									'<h6 class="text-primary mb-1">'+ response.output.fldTblPayment[i].reqId +'</h6>'+
									'<p class="mb-1">'+ response.output.fldTblPayment[i].reqDtStr +'</p>'+
									'<p class="m-0">'+ response.output.fldTblPayment[i].crtdBy +'</p>'+
								'</td>'+
								'<td>'+ response.output.fldTblPayment[i].cmpyId +'</td>'+
								'<td>'+ amtStr +'</td>'+
								'<td>'+ pymntMthdStr +'</td>'+
								'<td>'+ actnOn +'</td>'+
								'<td>'+ reqSts +'</td>'+
								'<td>'+ actnBtn +'</td>'+
							'<\tr>';
				}
				
				if( pageNo == 0 ){
					$("#receiveAutoInvcPymntTbl tbody").html( tbody );
				}else{
					$("#receiveAutoInvcPymntTbl tbody").append( tbody );
				}
				
				if( response.output.eof ){
					$("#loadMoreReceiveAutoInvc").attr("disabled", "disabled");
					$("#loadMoreReceiveAutoInvc").addClass("disabled");
				}else{
					$("#loadMoreReceiveAutoInvc").removeAttr("disabled");
					$("#loadMoreReceiveAutoInvc").removeClass("disabled");
				}
				
				$("#loadMoreReceiveAutoInvc").attr("data-page-no", parseInt(pageNo)+1);
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}

function aprvBlockPropInvc(invSts, remark = ""){	
	$("#mainLoader").show();
		
	var inputData = { 
		reqId : $("#viewAutoInvcPymntModal").attr("data-req-id"),
		reqUserBy : "",
		reqActnBy : "",
		paySts : invSts,
		rmk : remark,
		bankCd: $("#propBankCode").val(),
		acctNo: $("#propBankAccNo").val(),
		payMthd: $("#submitImvcPymnt").attr( "data-pay-mthd"),
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/payment/upd-sts',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				//sendNotificationMail("prop", inputData.reqId );
				
				$("#invcBlockConfirmModal, #viewAutoInvcPymntModal").modal("hide");
				getProposals();
				customAlert("#popupNotify", "alert-success", "Proposal status updated successfully.");
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#popupNotify", "alert-danger", errList);
			}
			
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}