$(function(){
	
	$("#clearFltr").click(function(){
		$("#recordSection #trnType").val("").trigger("change");
		$("#recordSection #stmntDt").val("");
		$("#recordSection input[type='checkbox']").prop("checked", false);
	});
	
	$("#recordSection").on("change", "input[type='checkbox'][name='all-record']", function(){
		if( $(this).prop("checked") ){
			$("#recordTbl input[type='checkbox'][name='select-record']").prop("checked", true);
			$("#updateTrn").removeClass("disabled");
			$("#updateTrn").removeAttr("disabled", "disabled");
		}else{
			$("#recordTbl input[type='checkbox'][name='select-record']").prop("checked", false);
			$("#updateTrn").addClass("disabled");
			$("#updateTrn").attr("disabled", "disabled");
		}
		
		calculateTotal();
	});
	
	$("#recordTbl").on("change", "input[type='checkbox'][name='select-record']", function(){
		if( $("#recordTbl input[type='checkbox'][name='select-record']:checked").length ){
			$("#updateTrn").removeClass("disabled");
			$("#updateTrn").removeAttr("disabled", "disabled");
		}else{
			$("#updateTrn").addClass("disabled");
			$("#updateTrn").attr("disabled", "disabled");
		}
		
		if( $("#recordTbl input[type='checkbox'][name='select-record']").length == $("#recordTbl input[type='checkbox'][name='select-record']:checked").length ){
			$("#recordSection input[type='checkbox'][name='all-record']").prop("checked", true);
		}else{
			$("#recordSection input[type='checkbox'][name='all-record']").prop("checked", false);
		}
		
		calculateTotal();
	});
	
	
	$("#updateTrn").click(function(){
	
		
		$("#mainNotify").html("");
		var post1 = ( $("#post1").prop("checked") == true ) ? "Y" : "N";
		var post2 = ( $("#post2").prop("checked") == true ) ? "Y" : "N";
		var baiArray = [];
		var baiObj = {};
		$("#recordTbl input[type='checkbox'][name='select-record']:checked").each(function(index, checkbox){
			baiObj = {
				id: $(checkbox).closest("tr").attr("data-id"),
				hdrId: $(checkbox).closest("tr").attr("data-hdr-id"),
				upldId: $(checkbox).closest("tr").attr("data-upld-id"),
				typCode: $(checkbox).closest("tr").attr("data-typ-code"),
				trnTyp: $(checkbox).closest("tr").attr("data-trn-typ"),
			};
			
			baiArray.push(baiObj);
		});
		
		if( baiArray.length == 0 ){
			customAlert("#mainNotify", "alert-danger", "Please select any transaction.");
			return;
		}
		
		var inputData = {
			baiArray: baiArray,
			post1: post1,
			post2: post2,
		};
		
		$("#mainLoader").show();
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/validate-bai-gl/prs-rcrd',
			type: 'POST',
			dataType : 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					customAlert("#mainNotify", "alert-success", "Transaction updated successfully.");
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
				}
				
				$("#mainLoader").hide();
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});
	});

	$("#searchRecords").click(function(){
		$("#recordSection .select2-selection").removeClass("border-danger");
		
		var errFlg = false;
		var stmtType = $("#stmtTyp").val();
		var trnType = $("#trnType").val();
		var stmntDt = $("#stmntDt").val();
		var post1 = ( $("#post1").prop("checked") == true ) ? "Y" : "N";
		var post2 = ( $("#post2").prop("checked") == true ) ? "Y" : "N";

		if( trnType == null || trnType == undefined || trnType == "" ){
			errFlg = true;
			$("#trnType").next().find(".select2-selection").addClass("border-danger");
		}
		
		if( errFlg == false ){
			$("#mainLoader").show();
			
			$("#recordSection input[type='checkbox'][name='all-record']").prop("checked", false);
			$("#recordSection input[type='checkbox'][name='all-record']").attr("disabled");
		
			var inputData = {
				stmtType: stmtType,
				trnType: trnType,
				stmntDt : stmntDt,
				post1: post1,
				post2: post2,
			};
			
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/validate-bai-gl/read',
				type: 'POST',
				dataType : 'json',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						var loopSize = response.output.fldTblBnkTrn.length;
						var tbodyRows = '';
						var chkBox = ''; 
						for( var i=0; i<loopSize; i++ ){
							
							if(response.output.fldTblBnkTrn[i].post1Ref.length == 0)
							{
								chkBox = '<div class="form-check form-check-flat inline-checkbox m-0">'+
												'<label class="form-check-label m-0">'+
													'<input type="checkbox" class="form-check-input" name="select-record">'+
													'&nbsp;<i class="input-helper"></i>'+
												'</label>'+
											'</div>';
							}
							else
							{
								chkBox = '';
							}
							
							tbodyRows += '<tr data-id="'+ response.output.fldTblBnkTrn[i].id +'" data-hdr-id="'+ response.output.fldTblBnkTrn[i].hdrId +'" data-upld-id="'+ response.output.fldTblBnkTrn[i].upldId +'" data-typ-code="'+ response.output.fldTblBnkTrn[i].typCode +'" data-trn-typ="'+ response.output.fldTblBnkTrn[i].typCdTrnTyp +'" >'+
										'<td>'+ chkBox + '</td>'+
										'<td>'+ response.output.fldTblBnkTrn[i].typCdTrnTyp +'</td>'+   
										'<td>'+ response.output.fldTblBnkTrn[i].typCode +'</td>'+
										'<td>'+ response.output.fldTblBnkTrn[i].typNm +'</td>'+
										'<td>'+ response.output.fldTblBnkTrn[i].bnkCode +'</td>'+
										'<td>'+ response.output.fldTblBnkTrn[i].bnkRef +'</td>'+
										'<td>'+ response.output.fldTblBnkTrn[i].custRef +'</td>'+
										'<td>'+ response.output.fldTblBnkTrn[i].erpRef +'</td>'+
										'<td class="text-right">'+ response.output.fldTblBnkTrn[i].trnAmount +'</td>'+
										'<td>'+ response.output.fldTblBnkTrn[i].crtdDttsStr +'</td>'+
										'<td>'+ response.output.fldTblBnkTrn[i].post1Ref +'</td>'+
										'<td>'+ response.output.fldTblBnkTrn[i].post2Ref +'</td>'+
									'</tr>';
						}
												
						$("#recordTbl tbody").html(tbodyRows);
						
						calculateTotal();
						
						if( loopSize ){
							$("#recordSection input[type='checkbox'][name='all-record']").removeAttr("disabled");
						}else{
							$("#recordSection input[type='checkbox'][name='all-record']").attr("disabled", "disabled");
						}
						
						var $table = $('#recordTbl');
					
						$table.floatThead({
							useAbsolutePositioning: true,
							scrollContainer: function($table) {
								return $table.closest('.table-responsive');
							}
						});
						$table.floatThead('reflow');
						
						$("#updateTrn").addClass("disabled");
						$("#updateTrn").attr("disabled", "disabled");
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
					}
					
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					$("#mainLoader").hide();
				}
			});
		}
	});
	
});


function calculateTotal(){
	var total = 0, i = 0, vchrAmt = 0;
	var vchrCry = ""; 
	var totalCount = 0;
	
	$("#recordTbl input[type='checkbox'][name='select-record']:checked").each(function(index, checkbox){
		if( i==0 ){
			vchrCry = $(checkbox).closest("tr").attr("data-vchr-cry");
		}
		
		vchrAmt = parseFloat( $(checkbox).closest("tr").attr("data-vchr-amt") );
		
		if( !isNaN(vchrAmt) ){
			total += vchrAmt;
		}
		
		i++;
	});
	
	total = total.toFixed(2);
	
	totalCount = i ;
	$("#totalAmt").html(total + " " + vchrCry + "(" + totalCount + ")");
}