$(function(){

	getBankList($("#vcBankId, #svcBankId"));
	
	getSavedVoidChecks();
	
	$("#vcBankId").change(function(){
		var bnkCd = $(this).val();
		if( bnkCd != "" ){			
			getBankAccountDetails($("#vcAcctId"), bnkCd);
		}else{
			$("#vcAcctId").html("");
		}

		$("#voidChecksTblWrapper").hide();
	});
	
	$("#svcBankId").change(function(){
		var bnkCd = $(this).val();
		if( bnkCd != "" ){			
			getBankAccountDetails($("#svcAcctId"), bnkCd);
		}else{
			$("#svcAcctId").html("");
		}
	});
	
	$("#clearVoidCheck").click(function(){
		$("#vcBankId").val("").trigger("change");
		$("#vcAcctId, #voidChecksTbl tbody").html("");
		$("#voidChecksTblWrapper").hide();
	});
	
	$("#clearAddVoidCheck").click(function(){
		$("#unusedChecksSection .select2").val("").trigger("change");
		$("#unusedChecksSection .form-control").val("");
	});

	$("#searchVoidCheck").click(function(){
		$("#vcBankId, #vcAcctId").next().find(".select2-selection").removeClass("border-danger");
		
		var errFlg = false;
		var bnkCd = $("#vcBankId").val();
		var acctId = $("#vcAcctId").val();
		
		if( bnkCd == "" || bnkCd == null || bnkCd == undefined ){
			errFlg = true;
			$("#vcBankId").next().find(".select2-selection").addClass("border-danger");
		}
		
		if( acctId == "" || acctId == null || acctId == undefined ){
			errFlg = true;
			$("#vcAcctId").next().find(".select2-selection").addClass("border-danger");
		}
		
		if( !errFlg ){
			$("#mainLoader").show();
			$("#checkLotsTbl tbody tr.more-check-lot").remove();
			
			var inputData = {
				bnkCode : bnkCd,
				acctId : acctId,
			};
						
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/check/read-chk',
				type: 'POST',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						var loopLimit = response.output.fldTblDoc.length;
						var voidCheckRow = '', checkbox = '', rowClass = '';
								
						if( loopLimit ){
							for( var y=0; y<loopLimit; y++ ){
								checkbox = '';
								rowClass = 'void-check-row';
								if(response.output.fldTblDoc[y].isPrs){
									rowClass = '';
									checkbox = '<div class="form-check form-check-flat m-0">' +
								                	'<label class="form-check-label">' +
								                		'<input type="checkbox" class="form-check-input" name="void-check" />' +
								                		'<i class="input-helper"></i>' +
								                	'</label>' +
								                '</div>';
								}
								voidCheckRow += '<tr class="'+ rowClass +'" data-chk-no="'+ response.output.fldTblDoc[y].chkNo +'" data-req-id="'+ response.output.fldTblDoc[y].reqId +'" data-vchr-no="'+ response.output.fldTblDoc[y].vchrPfx + '-' + response.output.fldTblDoc[y].vchrNo +'">'+
													'<td>'+ checkbox  +'</td>'+
													'<td>'+ response.output.fldTblDoc[y].chkNo +'</td>'+
													'<td>'+ response.output.fldTblDoc[y].reqId +'</td>'+
													'<td>'+ response.output.fldTblDoc[y].vchrVenId + '-' + response.output.fldTblDoc[y].vchrVenNm +'</td>'+
													'<td>'+ response.output.fldTblDoc[y].vchrPfx + '-' + response.output.fldTblDoc[y].vchrNo +'</td>'+
													'<td>'+ response.output.fldTblDoc[y].vchrInvNo +'</td>'+
													'<td>'+ response.output.fldTblDoc[y].vchrAmtStr + ' ' + response.output.fldTblDoc[y].vchrCry +'</td>'+
													'<td>'+ response.output.fldTblDoc[y].note +'</td>'+
													'<td>'+ response.output.fldTblDoc[y].crtDttsStr +'</td>'+
													'<td>'+ response.output.fldTblDoc[y].prsDttsStr +'</td>'+
													'<td>'+ response.output.fldTblDoc[y].prsBy +'</td>'+
												'</tr>';
							}
						}
						$("#voidChecksTbl tbody").html(voidCheckRow);
						$("#voidChecksTblWrapper").slideDown();
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
						$("#voidChecksTblWrapper").hide();
					}
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					console.log(jqXHR);
					console.log(textStatus);
					console.log(errorThrown);
					$("#mainLoader").hide();
				}
			});
		}
	});
	
	$("#voidChecksTbl").on("change", "input[type='checkbox'][name='void-check']", function(){
		if( $("#voidChecksTbl input[type='checkbox'][name='void-check']:checked").length ){
			$("#confirmVoidCheck").removeAttr("disabled");
			$("#confirmVoidCheck").removeClass("disabled");
		}else{
			$("#confirmVoidCheck").attr("disabled", "disabled");
			$("#confirmVoidCheck").addClass("disabled");
		}
	});
	
	$("#confirmVoidCheck").click(function(){
		$("#vcRmk").removeClass("border-danger");
		
		$("#confirmVoidCheckModal").modal("show");
	});
	
	$("#saveVoidCheck").click(function(){
		$("#vcRmk").removeClass("border-danger");
        
        var rmk = $("#vcRmk").val().trim();
        
        if( rmk == "" ){
        	$("#vcRmk").addClass("border-danger");
        	return;
        }
        
        $("#mainLoader").show();
        
		var voidCheckArr = [];
		$("#voidChecksTbl input[type='checkbox'][name='void-check']:checked").each(function(index, checkbox) {
            var $row = $(checkbox).closest("tr");
            var reqId = $row.attr("data-req-id");
            var chkNo = $row.attr("data-chk-no");
            var vchrNo = $row.attr("data-vchr-no");

            var vcObj = {
            	reqId: reqId,
            	chkNo: chkNo,
            	vchrNo: vchrNo,
            };

            voidCheckArr.push(vcObj);
        });
		
		var inputData = {
			voidChk : voidCheckArr,
			rmk: rmk,
		};
					
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/check/void-chk',
			type: 'POST',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					$("#confirmVoidCheckModal").modal("hide");
					
					customAlert("#mainNotify", "alert-success", "Void check saved successfully.");
					
					$("#clearVoidCheck").trigger("click");
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
					$("#voidChecksTblWrapper").hide();
				}
				$("#mainLoader").hide();
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				console.log(jqXHR);
				console.log(textStatus);
				console.log(errorThrown);
				$("#mainLoader").hide();
			}
		});
	});
	
	$("#createVoidCheck").click(function(){
		$("#unusedChecksSection .select2-selection, #unusedChecksSection .form-control").removeClass("border-danger");
		
        var errFlg = false;
        var bnkId = $("#svcBankId").val();
        var acctId = $("#svcAcctId").val();
        var chkNo = $("#svcChkNmbr").val();
        var rmk = $("#svcRmk").val();
        
        if( bnkId == "" || bnkId == null || bnkId == undefined ){
        	errFlg = true;
        	$("#svcBankId").next().find(".select2-selection").addClass("border-danger");
        }
        
        if( acctId == "" || acctId == null || acctId == undefined ){
        	errFlg = true;
        	$("#svcAcctId").next().find(".select2-selection").addClass("border-danger");
        }
        
        if( chkNo == "" ){
        	errFlg = true;
        	$("#svcChkNmbr").addClass("border-danger");
        }
        
        if( rmk == "" ){
        	errFlg = true;
        	$("#svcRmk").addClass("border-danger");
        }
        
        if( errFlg ){
        	return;
        }
        
        $("#mainLoader").show();
		
		var inputData = {
			bnkCode : bnkId,
			acctNo: acctId,
			chkNo: chkNo,
			rmk: rmk,
		};
					
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/check/void-chk-ind',
			type: 'POST',
			success: function(response){
				if( response.output.rtnSts == 0 ){					
					customAlert("#mainNotify", "alert-success", "Void check saved successfully.");
					
					$("#clearAddVoidCheck").trigger("click");
					
					getSavedVoidChecks();
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
					
					$("#mainLoader").hide();
				}
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				console.log(jqXHR);
				console.log(textStatus);
				console.log(errorThrown);
				$("#mainLoader").hide();
			}
		});
	});
	
});


function getSavedVoidChecks(){
	$("#mainLoader").show();
	
	var inputData = {};
				
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/check/void-chk-lst',
		type: 'POST',
		success: function(response){
			if( response.output.rtnSts == 0 ){		
				var loopLimit = response.output.fldTblCheckDetails.length;
				var tblRow = '';
				for( var y=0; y<loopLimit; y++ ){
					tblRow += '<tr>'+
									'<td>'+ (y+1) +'</td>'+
									'<td>'+ response.output.fldTblCheckDetails[y].bnk +'</td>'+
									'<td>'+ response.output.fldTblCheckDetails[y].acctNo +'</td>'+
									'<td>'+ response.output.fldTblCheckDetails[y].chkNo +'</td>'+
									'<td>'+ response.output.fldTblCheckDetails[y].chkVdRmk +'</td>'+
									'<td>'+ response.output.fldTblCheckDetails[y].chkVdOnStr +'</td>'+
								'</tr>';
				}	
				
				$("#savedVoidChecksTbl tbody").html(tblRow); 
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
				$("#voidChecksTblWrapper").hide();
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}