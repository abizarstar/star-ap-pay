$(function(){

	//getVendorList( $("#venId"), glblCmpyId );
	getVendorListESP( $("#venId"), glblCmpyId );
	
	getCurrencyList( $("#cry") );
	
	$("#clearPymntFltr").click(function(){
		$("#pymntSection #cry, #pymntSection #venId").val("").trigger("change");
		$("#pymntSection #invcDt, #pymntSection #pmntDt, #pymntSection #reqId").val("");
	});
	
	$("#pymntTbl").on("change", "input[type='checkbox'][name='select-pymnt']", function(){
		if( $("#pymntTbl input[type='checkbox'][name='select-pymnt']:checked").length ){
			$("#revertPymnt").removeClass("disabled");
			$("#revertPymnt").removeAttr("disabled", "disabled");
		}else{
			$("#revertPymnt").addClass("disabled");
			$("#revertPymnt").attr("disabled", "disabled");
		}
		
		if( $("#pymntTbl input[type='checkbox'][name='select-pymnt']").length == $("#pymntTbl input[type='checkbox'][name='select-pymnt']:checked").length ){
			$("#pymntSection input[type='checkbox'][name='all-rev-pay']").prop("checked", true);
		}else{
			$("#pymntSection input[type='checkbox'][name='all-rev-pay']").prop("checked", false);
		}
		
	});
	
	$("#pymntSection").on("change", "input[type='checkbox'][name='all-rev-pay']", function(){
		if( $(this).prop("checked") ){
			$("#pymntTbl input[type='checkbox'][name='select-pymnt']").prop("checked", true);
			$("#revertPymnt").removeClass("disabled");
			$("#revertPymnt").removeAttr("disabled", "disabled");
		}else{
			$("#pymntTbl input[type='checkbox'][name='select-pymnt']").prop("checked", false);
			$("#revertPymnt").addClass("disabled");
			$("#revertPymnt").attr("disabled", "disabled");
		}
		
		//calculateTotal();
	});
	
	
	$("#revertPymnt").click(function(){
		var tbody = "", vchrNo = "";
		var i = 1;
		
		var invArr = [];
		
		$("#pymntTbl input[type='checkbox'][name='select-pymnt']:checked").each(function(index, checkbox){
			var $row = $(checkbox).closest("tr");
			tbody += '<tr>'+
						'<td>'+ i +'</td>'+
						'<td>'+ $row.attr("data-req-id") +'</td>'+
						'<td>'+ $row.attr("data-vchr-no") +'</td>'+
						'<td class="text-right">'+ $row.attr("data-vchr-amt") + " " + $row.attr("data-vchr-cry") +'</td>'+
					'</tr>';
					
			var obj  = {reqId : $row.attr("data-req-id"), vchrNo : $row.attr("data-vchr-no") };
			
			console.log(obj);
			
			invArr.push(obj);
			
			console.log(invArr);
					
			i++;
		});
		
		$("#revertPymntBtn").attr("data-vchrs", JSON.stringify(invArr));
		$("#revertPymntTbl tbody").html(tbody);
		
		$("#revertPymntModal").modal("show");
	});

	$("#searchPymnt").click(function(){
		$("#pymntSection .select2-selection").removeClass("border-danger");
		
		var errFlg = false;
		//var invcStrtDate = "", invcEndDate = "";
		var cry = $("#cry").val();
		var venId = $("#venId").val();
		var pmntDt = $("#pmntDt").val();
		var reqId = $("#reqId").val();
		/*if( invcDt != "" && invcDt != null && invcDt != undefined ){
			invcStrtDate = invcDt.split(" - ")[0];
			invcEndDate = invcDt.split(" - ")[1];
			
			invcStrtDate = invcStrtDate.split('/').join('-');
			invcEndDate = invcEndDate.split('/').join('-');
		}*/

		/*if( cry == "" || cry == null || cry == undefined ){
			errFlg = true;
			$("#cry").next().find(".select2-selection").addClass("border-danger");
		}

		if( venId == "" || venId == null || venId == undefined ){
			errFlg = true;
			$("#venId").next().find(".select2-selection").addClass("border-danger");
		}
		*/
		if(cry == "" || cry == null || cry == undefined)
		{
			cry = "";
		}
		
		if(venId == "" || venId == null || venId == undefined)
		{
			venId = "";
		}
		if(pmntDt == "" || pmntDt == null || pmntDt == undefined)
		{
			pmntDt = "";
		}
		if(reqId == "" || reqId == null || reqId == undefined)
		{
			reqId = "";
		}
		if( errFlg == false ){
			$("#mainLoader").show();
		
			var inputData = {
				cry: cry,
				venId : venId,
				pmntDt : pmntDt,
				reqId: reqId,
			};
			
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/reverse/read',
				type: 'POST',
				dataType : 'json',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						var loopSize = response.output.fldTblDoc.length;
						var tbodyRows = '', vchrNoStr = '', poStr = '', chkNo = "", pmntDtStr = "", checkbox = ""; 
						for( var i=0; i<loopSize; i++ ){
							vchrNoStr = "", chkNo = "", checkbox = "";
							
							pmntDtStr = (response.output.fldTblDoc[i].pmntDtStr != undefined) ? response.output.fldTblDoc[i].pmntDtStr : "";
							
							if( response.output.fldTblDoc[i].poNo == 0 ){
								poStr = "";
							}else{
								if( response.output.fldTblDoc[i].poItm == 0 ){
									poStr = response.output.fldTblDoc[i].poPfx +'-'+ response.output.fldTblDoc[i].poNo;
								}else{
									poStr = response.output.fldTblDoc[i].poPfx +'-'+ response.output.fldTblDoc[i].poNo +'-'+ response.output.fldTblDoc[i].poItm;
								}
							}
							
							if( response.output.fldTblDoc[i].vchrPfx != undefined && response.output.fldTblDoc[i].vchrPfx.trim() != "" ){
								vchrNoStr = response.output.fldTblDoc[i].vchrPfx.trim() +'-'+ response.output.fldTblDoc[i].vchrNo.trim();
							}
							
							
							if( response.output.fldTblDoc[i].chkNo != undefined && response.output.fldTblDoc[i].chkNo.trim() != "" ){
								chkNo = response.output.fldTblDoc[i].chkNo;	
							}
							else
							
							{
								checkbox = '<div class="form-check form-check-flat inline-checkbox m-0">'+
												'<label class="form-check-label m-0">'+
													'<input type="checkbox" class="form-check-input" name="select-pymnt">'+
													'&nbsp;<i class="input-helper"></i>'+
												'</label>'+
											'</div>';
							}
							
							tbodyRows += '<tr data-invc-no="'+ response.output.fldTblDoc[i].vchrInvNo +'" data-vchr-no="'+ vchrNoStr +'" data-ctl-no="'+ response.output.fldTblDoc[i].ctlNo +'" data-vchr-amt="'+ response.output.fldTblDoc[i].totAmt +'" data-vchr-cry="'+ response.output.fldTblDoc[i].vchrCry +'" data-pmnt-dt="'+ pmntDtStr +'" data-req-id="'+ response.output.fldTblDoc[i].reqId +'">'+
										'<td>'+ checkbox + '</td>'+
										'<td>'+ response.output.fldTblDoc[i].reqId.trim() + '</td>'+
										'<td>'+ response.output.fldTblDoc[i].vchrVenId.trim() + "-" + response.output.fldTblDoc[i].vchrVenNm.trim() + '</td>'+
										'<td>'+ response.output.fldTblDoc[i].vchrInvNo +'</td>'+
										'<td>'+ response.output.fldTblDoc[i].vchrInvDtStr +'</td>'+
										'<td>'+ response.output.fldTblDoc[i].vchrDueDtStr +'</td>'+
										'<td>'+ poStr +'</td>'+
										'<td>'+ vchrNoStr +'</td>'+
										'<td>'+ response.output.fldTblDoc[i].vchrBrh +'</td>'+
										'<td class="text-right">'+ response.output.fldTblDoc[i].vchrAmtStr +'</td>'+
										'<td class="text-right">'+ response.output.fldTblDoc[i].discAmtStr +'</td>'+
										'<td class="text-right">'+ response.output.fldTblDoc[i].totAmtStr +' '+ response.output.fldTblDoc[i].vchrCry +'</td>'+
										'<td>'+ response.output.fldTblDoc[i].vchrDiscDtStr +'</td>'+
										'<td>'+ pmntDtStr +'</td>'+
										'<td>'+ response.output.fldTblDoc[i].crtDttsStr +'</td>'+
									'</tr>';
						}
												
						$("#pymntTbl tbody").html(tbodyRows);
						
						if( loopSize ){
							$("#pymntSection input[type='checkbox'][name='all-rev-pay']").removeAttr("disabled");
						}else{
							$("#pymntSection input[type='checkbox'][name='all-rev-pay']").attr("disabled", "disabled");
						}
						
						var $table = $('#pymntTbl');
					
						$table.floatThead({
							useAbsolutePositioning: true,
							scrollContainer: function($table) {
								return $table.closest('.table-responsive');
							}
						});
						$table.floatThead('reflow');
						
						$("#revertPymnt").addClass("disabled");
						$("#revertPymnt").attr("disabled", "disabled");
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
					}
					
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					$("#mainLoader").hide();
				}
			});
		}
	});
	
	$("#revertPymntBtn").click(function(){
		$("#mainLoader").show();
		var vchrStr = $(this).attr("data-vchrs");
		//var vchrArr = vchrStr.split(",");
		
		var inputData = {
			vchr: JSON.parse(vchrStr),  //If need array pass vchrArr
		};
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/reverse/upd-sts', //change here 
			type: 'POST',
			dataType : 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					$("#revertPymntBtn").attr("data-vchrs", "");
					$("#revertPymntTbl tbody").html("");
					
					$("#pymntTbl tbody").html("");
					$("#revertPymnt").addClass("disabled");
					$("#revertPymnt").attr("disabled", "disabled");
					
					$("#revertPymntModal").modal("hide");
					
					$("#searchPymnt").trigger("click");
					
					customAlert("#mainNotify", "alert-success", "Payments reverted successfully.");
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
				}
				
				$("#mainLoader").hide();
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});
	});
	
});