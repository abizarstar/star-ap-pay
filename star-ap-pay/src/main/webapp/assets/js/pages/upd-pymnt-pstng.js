$(function(){
	
	$("#clearFltr").click(function(){
		$("#recordSection #trnType").val("").trigger("change");
		$("#recordSection #stmntDt").val("");
		$("#recordSection input[type='checkbox']").prop("checked", false);
	});
	
	$("#recordSection").on("change", "input[type='checkbox'][name='all-record']", function(){
		if( $(this).prop("checked") ){
			$("#recordTbl input[type='checkbox'][name='select-record']").prop("checked", true);
			$("#updateTrn").removeClass("disabled");
			$("#updateTrn").removeAttr("disabled", "disabled");
		}else{
			$("#recordTbl input[type='checkbox'][name='select-record']").prop("checked", false);
			$("#updateTrn").addClass("disabled");
			$("#updateTrn").attr("disabled", "disabled");
		}
		
		calculateTotal();
	});
	
	$("#recordTbl").on("change", "input[type='checkbox'][name='select-record']", function(){
		if( $("#recordTbl input[type='checkbox'][name='select-record']:checked").length ){
			$("#updateTrn").removeClass("disabled");
			$("#updateTrn").removeAttr("disabled", "disabled");
		}else{
			$("#updateTrn").addClass("disabled");
			$("#updateTrn").attr("disabled", "disabled");
		}
		
		if( $("#recordTbl input[type='checkbox'][name='select-record']").length == $("#recordTbl input[type='checkbox'][name='select-record']:checked").length ){
			$("#recordSection input[type='checkbox'][name='all-record']").prop("checked", true);
		}else{
			$("#recordSection input[type='checkbox'][name='all-record']").prop("checked", false);
		}
		
		calculateTotal();
	});
	
	
	$("#updateTrn").click(function(){
		$("#mainNotify").html("");
		var post1 = ( $("#post1").prop("checked") == true ) ? "Y" : "N";
		var reqIdStr = "";
		$("#recordTbl input[type='checkbox'][name='select-record']:checked").each(function(index, checkbox){
			reqIdStr += (reqIdStr.length) ? "," : "";
			reqIdStr += $(checkbox).closest("tr").attr("data-req-id");
		});
		
		if( reqIdStr.length == 0 ){
			customAlert("#mainNotify", "alert-danger", "Please select any transaction.");
			return;
		}
		
		var inputData = {
			reqId: reqIdStr,
			post1: post1,
		};
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/validate-bai-gl/prs-pymnt-rcrd',
			type: 'POST',
			dataType : 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					customAlert("#mainNotify", "alert-success", "Payment posting updated successfully.");
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
				}
				
				$("#mainLoader").hide();
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});
	});

	$("#searchRecords").click(function(){
		$("#recordSection .select2-selection").removeClass("border-danger");
		
		var errFlg = false;
		var reqId = $("#reqId").val();
		var post1 = ( $("#post1").prop("checked") == true ) ? "Y" : "N";
		
		if( errFlg == false ){
			$("#mainLoader").show();
			
			$("#recordSection input[type='checkbox'][name='all-record']").prop("checked", false);
			$("#recordSection input[type='checkbox'][name='all-record']").attr("disabled");
		
			var inputData = {
				propId: reqId,
			};
			
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/validate-bai-gl/read-pay',
				type: 'POST',
				dataType : 'json',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						var loopSize = response.output.fldTblInvoice.length;
						var tbodyRows = ''; 
						for( var i=0; i<loopSize; i++ ){
									
							tbodyRows += '<tr data-req-id="'+ response.output.fldTblInvoice[i].reqId +'">'+
											'<td>'+
												'<div class="form-check form-check-flat inline-checkbox m-0">'+
													'<label class="form-check-label m-0">'+
														'<input type="checkbox" class="form-check-input" name="select-record">'+
														'&nbsp;<i class="input-helper"></i>'+
													'</label>'+
												'</div>'+
											'</td>'+
											'<td>'+ response.output.fldTblInvoice[i].reqId +'</td>'+
											'<td>'+ response.output.fldTblInvoice[i].vchrPfx + "-" + response.output.fldTblInvoice[i].vchrNo +'</td>'+   
											'<td>'+ response.output.fldTblInvoice[i].vchrInvNo +'</td>'+
											'<td>'+ response.output.fldTblInvoice[i].vchrAmt +'</td>'+
											'<td>'+ response.output.fldTblInvoice[i].vchrDiscAmt +'</td>'+
											'<td>'+ response.output.fldTblInvoice[i].vchrVendTotal +'</td>'+
											'<td>'+ response.output.fldTblInvoice[i].glEntry +'</td>'+
											'<td>'+ response.output.fldTblInvoice[i].glEntryDt +'</td>'+
										'</tr>';
						}
												
						$("#recordTbl tbody").html(tbodyRows);
						
						calculateTotal();
						
						if( loopSize ){
							$("#recordSection input[type='checkbox'][name='all-record']").removeAttr("disabled");
						}else{
							$("#recordSection input[type='checkbox'][name='all-record']").attr("disabled", "disabled");
						}
						
						var $table = $('#recordTbl');
					
						$table.floatThead({
							useAbsolutePositioning: true,
							scrollContainer: function($table) {
								return $table.closest('.table-responsive');
							}
						});
						$table.floatThead('reflow');
						
						$("#updateTrn").addClass("disabled");
						$("#updateTrn").attr("disabled", "disabled");
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
					}
					
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					$("#mainLoader").hide();
				}
			});
		}
	});
	
});


function calculateTotal(){
	var total = 0, i = 0, vchrAmt = 0;
	var vchrCry = ""; 
	var totalCount = 0;
	
	$("#recordTbl input[type='checkbox'][name='select-record']:checked").each(function(index, checkbox){
		if( i==0 ){
			vchrCry = $(checkbox).closest("tr").attr("data-vchr-cry");
		}
		
		vchrAmt = parseFloat( $(checkbox).closest("tr").attr("data-vchr-amt") );
		
		if( !isNaN(vchrAmt) ){
			total += vchrAmt;
		}
		
		i++;
	});
	
	total = total.toFixed(2);
	
	totalCount = i ;
	$("#totalAmt").html(total + " " + vchrCry + "(" + totalCount + ")");
}