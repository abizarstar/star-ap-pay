 $(function(){
		dbdetails();
		
		$("#saveconfig").click(function(){
		config();
	});	
	$("#getdb").click(function(){
		dbdetails();
	});	
	$("#deldb").click(function(){
		deletedb();
	});	
});
	
	
function config(){
	$("#mainLoader").show();
	
	var inputData = {
		driver: $("#driver").val().trim(),
		url: $("#url").val().trim(),
		host: $("#host").val().trim(),
		port: $("#port").val().trim(),
		db: $("#db").val().trim(),
		user: $("#user").val().trim(),
		password: $("#password").val().trim(),
				conType:$("#conType").val().trim(),
		};
	
		$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/db/config',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				customAlert("#mainNotify", "alert-success", "response Ok");
			
						}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}

function dbdetails(){
	$("#mainLoader").show();
	
	var inputData = {conType:$("#conType").val().trim(),};
	
		$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/db/read-db',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				customAlert("#mainNotify", "alert-success", "Database detail is listed below  "+inputData);
								var loopSize = response.output.fldTbldbcnfg.length;
				var driver = '', url = '', host = '', port = '',db= '', user = '', tblHtml = '', conType = '', srNo = 1;	
				for( var i=0; i<loopSize; i++ ){
					driver = response.output.fldTbldbcnfg[i].driver;
					url = response.output.fldTbldbcnfg[i].url;
					host = response.output.fldTbldbcnfg[i].host;
					port = response.output.fldTbldbcnfg[i].port;
					db = response.output.fldTbldbcnfg[i].db;
					user = response.output.fldTbldbcnfg[i].user;
					conType = response.output.fldTbldbcnfg[i].conType;
									tblHtml += 	'<td>'+ driver +'</td>'+
										'<td>'+ url +'</td>'+
										'<td>'+ host +'</td>'+
										'<td>'+ port +'</td>'+
										'<td>'+ db +'</td>'+
										'<td>'+ user +'</td>'+
										'<td>'+ conType +'</td>'+
										'</tr>';
						srNo++;
					
									}
									
				$("#dblist tbody").html( tblHtml );
				

			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}

function deletedb(){
	$("#mainLoader").show();
	
	var inputData = {driver: $("#driver").val().trim(),};
	
		$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/db/del-db',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				customAlert("#mainNotify", "alert-success", "Database deleted  "+inputData);

			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}
	
	
	
	
	
	