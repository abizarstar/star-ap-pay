$(function(){
	
	//getStxUserList( $("#usrId") );
	
	//getUserGroupList( $("#usrGrpId") );
	
	$("#amountType").change(function(){
		if( $("#amountType").val() == "range" ){
			$("#amountTo").removeAttr("readonly");
			$("#amount").attr("placeholder", "From");
			$("#amountTo").attr("placeholder", "To");
		}else{
			$("#amountTo").attr("readonly", true);
			$("#amount").attr("placeholder", "");
			$("#amountTo").attr("placeholder", "");
			$("#amountTo").val("");
			$("#amountTo").removeClass("border-danger");
		}
	});
	
	$("input[type='radio'][name='blockCndt']").change(function(){
		$(".condition-row").css({"transform": "scale(0)","height": "0"});
		var rowId = $("input[type='radio'][name='blockCndt']:checked").val();
		$("#"+rowId).css({"height": "initial","transform": "scale(1)"});
	});
		
	
	$("#saveUserSetup").click(function(){
		$("#userSetupForm .form-control, #userSetupForm .select2-selection").removeClass("border-danger");
	
		var errFlg = false;
		var grpId = $("#usrGrpId").val();
		var usrId = $("#usrId").val();
		var usrNm = $("#usrNm").val();
		var usrEmail = $("#usrEmail").val();
		
		if( grpId == "" ){
			errFlg = true;
			$("#usrGrpId").next().find(".select2-selection").addClass("border-danger");
		}
		
		if( usrId == "" ){
			errFlg = true;
			$("#usrId").next().find(".select2-selection").addClass("border-danger");
		}
		
		if( usrNm == "" ){
			errFlg = true;
			$("#usrNm").addClass("border-danger");
		}
		
		if( usrEmail == "" ){
			errFlg = true;
			$("#usrEmail").addClass("border-danger");
		}
		
		if( errFlg == false ){
			$("#mainLoader").show();
		
			var inputData = {
				usrId : usrId,
				usrNm : usrNm,
				eml : usrEmail,
				usrType : "STX",
				usrGrp : grpId
			}
			
			$.ajax({
				headers: ajaxHeader,
				url : rootAppName + "/rest/user/add",
				data : JSON.stringify( inputData ),
				type: 'POST',
				dataType: 'json',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						getUserTableData();
						$("#cancelUserSetup").trigger("click");
						customAlert("#mainNotify", "alert-success", 'User saved successfully.');
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
						$("#mainLoader").hide();
					}			
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					$("#mainLoader").hide();
				}
			});
		}
	});
});