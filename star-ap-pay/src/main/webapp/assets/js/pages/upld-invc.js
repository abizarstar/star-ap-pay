$(function(){

	getCategoryList( $("#category") );
	
	$("#uploadFile").change(function() {
		$("#mainNotify").html("");
		var fileType = "";
		var tblRow = "";
		
		for(i = 0; i < this.files.length ; i++){
			if (this.files && this.files[i]) {
				var fileExt = this.files[i].name.split(".").pop().toLowerCase();
				//if( fileExt != "pdf" && fileExt != "jpg" && fileExt != "jpeg" && fileExt != "png" ){
				if( fileExt != "pdf" ){
					$("#uploadFile").val("");
					customAlert("#mainNotify", "alert-danger", 'Only PDF files are allowed.');
					return;
				}
				
				if( fileExt == "pdf" ){
					fileType = "pdf";
				}else if( fileExt == "jpg" || fileExt == "jpeg" || fileExt == "png" ){
					fileType = "image";
				}
							
				var uploadFile = document.getElementById("uploadFile").files[i];
				uploadFileUrl = URL.createObjectURL(uploadFile);
				
				if(i == 0){
					 $('#docPreview, #preview-img img').attr('src', uploadFileUrl);
				
					if( fileType == "pdf" ){
						$('#preview-iframe, #docPreview').slideDown("slow");
						$("#preview-img").hide();
					}else if( fileType = "image" ){
						$('#docPreview').hide();
						$("#preview-img").slideDown("slow");
					}
					$("#preview").show();
				}
				
				tblRow += '<tr data-file-name="' + this.files[i].name + '" data-flTyp="'+ fileType +'" data-url="'+ uploadFileUrl +'">';
					tblRow += '<td> <a href="#" class="upld-doc-preview-icon display-5" title="Preview"><i class="icon-doc"></i></a></td>';
					tblRow += '<td class="break-word"> ' + this.files[i].name + ' </td>';
					tblRow += '<td> ' + this.files[i].type + ' </td>';
					tblRow += '<td> ' + Math.round( this.files[i].size * 0.0009765625 ) + ' KB</td>';
				tblRow += '</tr>';
			}
		}
		
		$("#upldDocTbl tbody").html(tblRow);
		if( tblRow != "" ){
			$("#upldDocTblWrapper").slideDown();
		}else{
			$("#upldDocTblWrapper").hide();
		}
	
	});
	
	$("#cancelUpload").click(function(){
		$("#upldDocTbl tbody").html("");
		$("#uploadFile").val("");
		$("#category").val("DFLT").trigger("change");
		$("#upldDocTblWrapper, #preview-iframe, #preview-img").hide();
	});
	
	$("#upldDocTbl").on('click',".upld-doc-preview-icon", function(){
		var imgURL = $(this).closest("tr").attr("data-url");
		var fileType = $(this).closest("tr").attr("data-flTyp");
		
		 $('#docPreview, #preview-img img').attr('src', imgURL);
			
		if( fileType == "pdf" ){
			$('#preview-iframe, #docPreview').slideDown("slow");
			$("#preview-img").hide();
		}else if( fileType = "image" ){
			$('#docPreview').hide();
			$("#preview-img").slideDown("slow");
		}
	});
	
	
});


function submitComment(formId){
	$("#category").next().find(".select2-selection").removeClass("border-danger")
	var formElement = document.forms.namedItem(formId);
	var formData = new FormData(formElement);
	var upload = false;
	var fileName = "";
	var file = [];
	var category = $("#category").val();
	
	if( category == "" || category == null || category == undefined ){
		$("#category").next().find(".select2-selection").addClass("border-danger");
		return;
	}
	
	$.each(formData.getAll("file"), function(index, value) {
		if( value.name != "" && value.size > 0 ){
			upload = true;
			fileName = value.name;
		}
	});
	
	//$("#mainLoader").hide();
	//return ;
	
	if( upload === true ){
		//$("#mainLoader").show();
		
		formData.append('category', category);
		
		$("#cancelUpload").trigger("click");
		
		customAlert("#rightNotify", "alert-primary", 'Uploading document(s).......');
		
		$.ajax({
			headers : {
				'user-id' : localStorage.getItem("starOcrUserId"),
				'auth-token' : localStorage.getItem("starOcrAuthToken"),
				'app-token' : localStorage.getItem("starOcrAppToken"),
				'cmpy-id' : localStorage.getItem("starOcrCmpyId"),
			},
			type : "POST",
			url : rootAppName + '/rest/cstmdoc/adddoc',
			data : formData, // serializes the form's elements.
			enctype : 'multipart/form-data',
			cache : false,
			processData : false,
			contentType : false,
			success: function(response){
				//if( response.output.rtnSts == 0 ){
					$("#cancelUpload").trigger("click");
					
					customAlert("#rightNotify", "alert-success", 'Invoice(s) uploaded successfully.');
				/*}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
				}*/
				
				//$("#mainLoader").hide();
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				//$("#mainLoader").hide();
			}
		});
	}
	else{
		customAlert("#rightNotify", "alert-error", 'Please select any PDF document.');
	}
}



function getCategoryList( $select ){
	$("#mainLoader").show();
	
	var inputData = {};
	
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/common/vchr-cat',
		type: 'POST',
		data : JSON.stringify( inputData ),
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblVouCategory.length;
				var vchrCat = '', desc = '', options = '<option value=" ">Default</option>';	
				
				var dfltOption = "", selectOption = "";
				if( $select.attr("data-selected") != undefined && $select.attr("data-selected") != "" ){
					dfltOption = $select.attr("data-selected");
					
					$select.attr("data-selected", "");
				}
				
				for( var i=0; i<loopSize; i++ ){
					selectOption = "";
				
					vchrCat = response.output.fldTblVouCategory[i].vchrCat.trim();
					desc = response.output.fldTblVouCategory[i].desc.trim();
					
					if( dfltOption == vchrCat ){
						selectOption = "selected";
					}
					
					options += '<option value="'+ vchrCat +'" '+ selectOption +'>'+ vchrCat + ' - ' + desc + '</option>';
				}
				$select.html( options );
				
				$("#mainLoader").hide();
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
				$("#mainLoader").hide();
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}
