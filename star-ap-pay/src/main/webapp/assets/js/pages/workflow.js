$(function() {

	var wfBlockCnt = 1;

	getWorkflow();

	getApproverList($("#approvalTo, #dfltAprv"));

	getVendorList($("#vndr"), glblCmpyId, false);

	getBranchList($("#brh"), false);
	
	getVoucherCategoryList( $("#vchrCat") );

	$(".form-check label,.form-radio label").append('<i class="input-helper"></i>');

	$("#aprvList, #dfltAprvList").sortable({
		placeholder: "ui-state-highlight",
		axis: "y",
		//handle: ".move-aprv",
		containment: "parent",
	});
	$("#aprvList").sortable({
		update: function(event, ui) {
			aproverSequence("aprvList");
		},
	});
	$("#dfltAprvList").sortable({
		update: function(event, ui) {
			aproverSequence("dfltAprvList");
		},
	});

	$(".aprv-sortable-list").on("click", ".remove-aprv", function() {
		var prntId = $(this).closest(".aprv-sortable-list").attr("id");
		$(this).closest(".card").remove();
		aproverSequence(prntId);
	});

	$("#approvalTo").change(function() {
		var aprvNm = $("#approvalTo option:selected").attr("data-usr-nm");
		var aprvId = $("#approvalTo").val();
		var aprvArr = [];

		if (aprvId != "" && aprvId != null && aprvId != undefined) {

			$("#aprvList .card").each(function(index, row) {
				aprvArr.push($(row).attr("data-aprv-id"));
			});

			if (aprvArr.indexOf(aprvId) <= -1) {
				var aprvHtml = '<div class="card rounded border" data-aprv-id="' + aprvId + '" data-aprv-nm="' + aprvNm + '">' +
					'<div class="card-body">' +
					'<div class="media">' +
					'<div class="media-body">' +
					'<h6 class="mb-0"><span class="seq-num">' + ($("#aprvList .card").length + 1) + '</span><span class="mr-1">.</span>' + aprvNm + '</h6>' +
					'</div>' +
					'<i class="icon-trash align-self-center ml-1 remove-aprv c-pointer text-danger"></i>' +
					'</div>' +
					'</div>' +
					'</div>';
				$("#aprvList").append(aprvHtml);
				$("#approvalTo").val("").trigger("change");
				$("#approvalTo").next().find(".select2-selection").removeClass("border-danger");
				$("#aprvList").show();
			}
		}
	});
	

	$("#dfltAprv").change(function() {
		var aprvNm = $("#dfltAprv option:selected").attr("data-usr-nm");
		var aprvId = $("#dfltAprv").val();
		var aprvArr = [];

		if (aprvId != "" && aprvId != null && aprvId != undefined) {

			$("#dfltAprvList .card").each(function(index, row) {
				aprvArr.push($(row).attr("data-aprv-id"));
			});

			if (aprvArr.indexOf(aprvId) <= -1) {
				var aprvHtml = '<div class="card rounded border" data-aprv-id="' + aprvId + '" data-aprv-nm="' + aprvNm + '">' +
					'<div class="card-body">' +
					'<div class="media">' +
					'<div class="media-body">' +
					'<h6 class="mb-0"><span class="seq-num">' + ($("#dfltAprvList .card").length + 1) + '</span><span class="mr-1">.</span>' + aprvNm + '</h6>' +
					'</div>' +
					'<i class="icon-trash align-self-center ml-1 remove-aprv c-pointer text-danger"></i>' +
					'</div>' +
					'</div>' +
					'</div>';
				$("#dfltAprvList").append(aprvHtml);
				$("#dfltAprv").val("").trigger("change");
				$("#dfltAprv").next().find(".select2-selection").removeClass("border-danger");
				$("#dfltAprvList").show();
			}
		}
	});

	$(document).on("click", ".add-block", function() {
		clearBlockModal();
		$("#editBlockMode").val(0);
		$("#editableWfblockId").val($(this).attr("data-wf-block"));
		$("#blockModal").modal("show");
	});

	$("#newWfBtn").click(function() {
		$("#wfBlock1 .add-block").removeClass("hide-imp");
		$("#saveWfBtn").show();
		$("#wfTitle").attr("data-step-data", "");
		$("#wfName").val("");
		$("#wfOwner").val("").trigger("change");
		$("#wfStrtModal").modal("show");
		$("#wfAlert").html("");
	});
	
	
	$("#dfltWfSts").change(function(){
		
		var wkfId = $("#saveDfltWk").attr("data-wkf-id");
		
		if ($(this).prop("checked"))
		{	
			deleteWorkFlow(wkfId , "D");
		
		}
		else
		{
			deleteWorkFlow(wkfId , "E");
		}
		
		$("#dfltWfModal").modal("hide");
	
	});

	$("#dfltWfBtn").click(function() {
	
		$("#dfltWfModal").modal("show");
	});

	$("#saveWfInfo").click(function() {
		$("#wfname-error").html("");

		if ($("#wfName").val().trim() == "") {
			$("#wfname-error").html("Please enter name for this workflow.");
			return;
		} else if ($("#wfName").val().trim().toLowerCase() == "default") {
			$("#wfname-error").html("Please enter diffrent name.");
			return;
		} else {

			$("#mainLoader").show();

			var inputData = {
				wfName: $("#wfName").val().trim(),
			};

			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify(inputData),
				url: rootAppName + '/rest/wkf/read-wkf-chk',
				type: 'POST',
				dataType: 'json',
				success: function(response) {
					if (response.output.rtnSts == 0) {
						if (response.output.nbrRecRtn == 0) {
							$("#wfBlock1").removeClass("col-8");
							$("#wfTitle").text($("#wfName").val());
							$("#workflowContent").removeClass("d-none");

							$("#wfGridWrapper").hide();
							$("#wfAddWrapper").show();

							$("#wfStrtModal").modal("hide");
						} else {
							$("#wfname-error").html("This name is already used, please enter different name.");
						}
					} else {
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for (var i = 0; i < loopLimit; i++) {
							errList += '<li>' + response.output.messages[i] + '</li>';
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
					}

					$("#mainLoader").hide();
				},
				error: function(jqXHR, textStatus, errorThrown) {
					if (jqXHR.status == 401) {
						validateAuthCode();
					}
					$("#mainLoader").hide();
				}
			});
		}
	});

	$(document).on("click", ".delete-wf", function() {
		var wkfId = $(this).parents("tr").attr("data-wkf-id");

		$("#deleteWkf").attr("data-wkf-id", wkfId);

		$("#deleteWkfModal").modal("show");
	});

	$("#deleteWkf").click(function() {
		var wkfId = $(this).attr("data-wkf-id");

		deleteWorkFlow(wkfId, "D");
	});

	$(document).on("change", "input[type='radio'][name='blockCndt']", function() {
		$(".condition-row").css({
			"transform": "scale(0)",
			"height": "0"
		});
		var rowId = $("input[type='radio'][name='blockCndt']:checked").val();
		$("#" + rowId).css({
			"height": "initial",
			"transform": "scale(1)"
		});
	});

	$(document).on("change", "input[type='checkbox'].blockCndt", function() {
		var rowId = $(this).attr("name");

		if ($(this).prop("checked")) {
			$("#" + rowId).css({
				"transform": "scale(1)",
				"height": "initial"
			});
			$("#" + rowId).addClass("mb-2");
		} else {
			$("#" + rowId).css({
				"transform": "scale(0)",
				"height": "0"
			});
			$("#" + rowId).removeClass("mb-2");
		}
	});

	$(document).on("change", "#amountType", function() {
		if ($("#amountType").val() == "range") {
			$("#amountTo").removeAttr("readonly");
			$("#amount").attr("placeholder", "From");
			$("#amountTo").attr("placeholder", "To");
		} else {
			$("#amountTo").attr("readonly", true);
			$("#amount").attr("placeholder", "");
			$("#amountTo").attr("placeholder", "");
			$("#amountTo").val("");
			$("#amountTo").removeClass("border-danger");
		}
	});

	$(document).on("click", "#saveWfBtn, #cancelWfBtn", function() {

		if ($(this).attr("id") == "saveWfBtn") {

			if ($("#wfTitle").attr("data-step-data") != "") {
				var boxData = JSON.parse(decodeURIComponent($("#wfTitle").attr("data-step-data")));
				console.log(boxData);

				var alertMode = "created";

				var amtFlg = 0,
					brhFlg = 0,
					catFlg = 0,
					venFlg = 0,
					dayFlg = 0,
					amtFrm = 0,
					amtTo = 0,
					days = 0,
					dayBef = 0,
					costRecon = 0;
				var brh = '',
					venId = '', 
					vchrCat = '';

				var wkfId = ($(this).attr("data-wkf-id") != undefined) ? $(this).attr("data-wkf-id") : "";

				if (wkfId != "" && wkfId > 0) {
					alertMode = "updated";
				}

				if (boxData.amountRow) {
					amtFlg = 1;
					amtFrm = boxData.amount;
					amtTo = boxData.amountTo;
				}

				if (boxData.branchRow) {
					brhFlg = 1;
					brh = boxData.brh.toString();
				}
				
				if (boxData.categoryRow) {
					catFlg = 1;
					vchrCat = boxData.vchrCat.toString();
				}

				if (boxData.vendorRow) {
					venFlg = 1;
					venId = boxData.venId.toString();

					if (boxData.costRecon) {
						costRecon = 1;
					}
				}

				if (boxData.daysRow) {
					dayFlg = 1;
					days = boxData.nbrDays;
				}

				if (boxData.daysCndt == "bfrInvDueDt") {
					dayBef = 1;
				}

				var inputData = {
					wkfId: wkfId,
					wkfName: boxData.wkfName,
					crtdBy: boxData.crtdBy,
					stepName: boxData.stepName,
					amtFlg: amtFlg,
					brhFlg: brhFlg,
					venFlg: venFlg,
					dayFlg: dayFlg,
					catFlg: catFlg,
					amtFrm: amtFrm,
					amtTo: amtTo,
					brh: brh,
					venId: venId,
					vchrCat: vchrCat,
					days: days,
					dayBef: dayBef,
					apvr: boxData.aprvArr,
					costRecon: costRecon,
				};

				$("#mainLoader").show();

				$.ajax({
					headers: ajaxHeader,
					data: JSON.stringify(inputData),
					url: rootAppName + '/rest/wkf/create-wkf',
					type: 'POST',
					dataType: 'json',
					success: function(response) {
						if (response.output.rtnSts == 0) {
							$("#wfBlock1 .row").remove();
							$("#wfAddWrapper").hide();
							$("#wfGridWrapper").show();

							swal({
								text: "Workflow " + alertMode + " successfully.",
								type: 'success',
							});

							getWorkflow();
						} else {
							var loopLimit = response.output.messages.length;
							var errList = '<ul class="m-0 pl-1">';
							for (var i = 0; i < loopLimit; i++) {
								errList += '<li>' + response.output.messages[i] + '</li>';
							}
							errList += '</ul>';
							customAlert("#popupNotify", "alert-danger", errList);
							rtnSts = false;
						}
						$("#mainLoader").hide();
					},
					error: function(jqXHR, textStatus, errorThrown) {
						if (jqXHR.status == 401) {
							validateAuthCode();
						}
						$("#mainLoader").hide();
					}
				});

			} else {
				swal({
					text: "Please add a step in this workflow.",
					type: 'error',
				});
			}
		} else {
			$("#saveWfBtn").attr("data-role", "save");
			$("#saveWfBtn").attr("data-wkf-id", "");

			$("#wfBlock1 .row").remove();
			$("#wfAddWrapper").hide();
			$("#wfGridWrapper").show();
		}
	});

	$(document).on("click", ".enable-wf", function() {
	
		var wkfId = $(this).parents("tr").attr("data-wkf-id");
		deleteWorkFlow(wkfId , "E");
	
	});

	$(document).on("click", ".edit-wf", function() {
		$("#mainLoader").show();

		var wkfId = $(this).parents("tr").attr("data-wkf-id");
		var pndgDoc = $(this).attr("data-pndg-doc");

		$("#saveWfBtn").attr("data-role", "update");
		$("#saveWfBtn").attr("data-wkf-id", wkfId);

		var inputData = {
			wkfId: wkfId,
		};

		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify(inputData),
			url: rootAppName + '/rest/workflow/read',
			type: 'POST',
			dataType: 'json',
			success: function(response) {
				if (response.output.rtnSts == 0) {
					var wfBlockCnt = 2;

					var amountRow = 0,
						branchRow = 0,
						vendorRow = 0,
						categoryRow = 0;
						daysRow = 0;
					var cndTyp = "",
						daysCndt = "",
						cndtn = "";
					if (response.output.fldTblWorkflow[0].amtFlg == 1) {
						cndTyp = "amountRow";
						amountRow = 1;
						cndtn += (cndtn == "") ? '' : ' AND ';
						cndtn += "<span class='text-primary'>If Amount between " + response.output.fldTblWorkflow[0].amtFrm + " - " + response.output.fldTblWorkflow[0].amtTo + "</span>";
					}
					if (response.output.fldTblWorkflow[0].brhFlg == 1) {
						cndTyp = "branchRow";
						branchRow = 1;
						cndtn += (cndtn == "") ? '' : ' AND ';
						cndtn += "<span class='text-primary'>If Branch is (" + response.output.fldTblWorkflow[0].brh + ")</span>";
					}
					if (response.output.fldTblWorkflow[0].vchrCatFlg == 1) {
						cndTyp = "categoryRow";
						categoryRow = 1;
						cndtn += (cndtn == "") ? '' : ' AND ';
						cndtn += "<span class='text-primary'>If Category is (" + response.output.fldTblWorkflow[0].vchrCat + ")</span>";
					}
					if (response.output.fldTblWorkflow[0].venFlg == 1) {
						cndTyp = "vendorRow";
						vendorRow = 1;
						cndtn += (cndtn == "") ? '' : ' AND ';
						cndtn += "<span class='text-primary'>If Vendor is (" + response.output.fldTblWorkflow[0].venId + ") </span>";
						cndtn += (response.output.fldTblWorkflow[0].costRecon == 1) ? " <span class='text-info'>Cost Reconciliation</span>" : '';
					}
					if (response.output.fldTblWorkflow[0].dayFlg == 1) {
						cndTyp = "daysRow";
						daysRow = 1;
						cndtn += (cndtn == "") ? '' : ' AND ';
						if (response.output.fldTblWorkflow[0].dayBef == 1) {
							daysCndt = "bfrInvDueDt";
							cndtn += "<span class='text-primary'>" + response.output.fldTblWorkflow[0].days + " days before invoice due date</span>";
						} else {
							daysCndt = "frmInvDt";
							cndtn += "<span class='text-primary'>" + response.output.fldTblWorkflow[0].days + " days from the invoice date</span>";
						}
					}

					var actionIcon = '',
						approvalStr = '';
					var aprvArr = [];
					$(response.output.fldTblWorkflow[0].wkfAprvrList).each(function(index, row) {
						var aprvObj = {
							order: row.order,
							aprvId: row.usrId,
							aprvNm: row.usrNm,
						};
						aprvArr.push(aprvObj);

						approvalStr += (approvalStr.length) ? " > " : "";
						approvalStr += '<span class="badge badge-pill badge-outline-primary">' + row.order + '</span>' + " " + row.usrNm;
					});

					var boxData = {};
					boxData.wkfId = response.output.fldTblWorkflow[0].wkfId;
					boxData.wkfName = response.output.fldTblWorkflow[0].wkfName;
					boxData.crtdBy = localStorage.getItem("starOcrUserId");
					boxData.stpId = response.output.fldTblWorkflow[0].stpId;
					boxData.stepName = response.output.fldTblWorkflow[0].stpNm;
					boxData.amount = response.output.fldTblWorkflow[0].amtFrm;
					boxData.amountTo = response.output.fldTblWorkflow[0].amtTo;
					boxData.brh = response.output.fldTblWorkflow[0].brh;
					boxData.vchrCat = response.output.fldTblWorkflow[0].vchrCat;
					boxData.venId = response.output.fldTblWorkflow[0].venId;
					boxData.days = response.output.fldTblWorkflow[0].days;
					boxData.dayBef = response.output.fldTblWorkflow[0].dayBef;
					boxData.approvalTo = response.output.fldTblWorkflow[0].apvr;
					boxData.cndTyp = cndTyp;
					boxData.daysCndt = daysCndt;
					boxData.amountRow = amountRow;
					boxData.branchRow = branchRow;
					boxData.vendorRow = vendorRow;
					boxData.categoryRow = categoryRow;
					boxData.daysRow = daysRow;
					boxData.costRecon = response.output.fldTblWorkflow[0].costRecon;
					boxData.aprvArr = aprvArr;

					var boxDataStr = encodeURIComponent(JSON.stringify(boxData));

					if (pndgDoc == 0) {
						actionIcon = '<i class="icon-note edit-box" id="editBox' + wfBlockCnt + '" data-box-data="' + boxDataStr + '"></i> <i class="icon-close remove-box" id="closeBox' + wfBlockCnt + '"></i>';
						$("#saveWfBtn").show();
						$("#wfAlert").html('');
					} else {
						$("#saveWfBtn").hide();
						$("#wfAlert").html('<div class="alert alert-warning"><p class="m-0">This workflow is assigned to ' + pndgDoc + ' Invoice(s), so it is not editable.</p></div>');
					}

					var boxHtml = '<div class="alert alert-success text-center" role="alert">' +
						'Start <i class="add-block icon-plus hide-imp" data-wf-block="wfBlock1"></i>' +
						'</div>' +
						'<div class="row">' +
						'<div class="col wkf-step-row">' +
						'<div class="wf-block" id="wfBlock' + wfBlockCnt + '">' +
						actionIcon +
						'<div class="text-center d-block">' +
						'<p class="condition-para">' +
						'<span class="condition-span">' + cndtn + '</span>' +
						'</p>' +
						'</div>' +
						'<div class="text-center d-block">' +
						'<div class="alert alert-primary text-center m-0" role="alert">' +
						'<h4 class="mb-0 wkf-step yogesh">' +
						'<span class="wkf-level text-warning">Level 1: </span>' +
						response.output.fldTblWorkflow[0].stpNm +
						'</h4>' +
						'<p class="mb-0"></p>' +
						'<p class="mb-0">' +
						'<span class="text-warning font-weight-bold">Approver: </span>' + approvalStr +
						'</p>' +
						'</div>' +
						'</div>' +
						'<div class="row">' +
						'<div class="col">' +
						'<div class="text-center d-block">' +
						'<img src="assets/images/down-arrow24x24.png" class="down-arrow-img">' +
						'</div>' +
						'<div class="text-center d-block">' +
						'<div class="alert alert-success text-center" role="alert">End</div>' +
						'</div>' +
						'</div>' +
						'</div>' +
						'</div>' +
						'</div>' +
						'</div>';
					
					$("#wfTitle").attr("data-step-data", boxDataStr);
					$("#workflowContent").removeClass("d-none");
					$("#wfTitle").text(response.output.fldTblWorkflow[0].wkfName);
					$("#wfBlock1").html(boxHtml);

					countStepLevel();

					$("#wfGridWrapper").hide();
					$("#wfAddWrapper").show();
				} else {
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for (var i = 0; i < loopLimit; i++) {
						errList += '<li>' + response.output.messages[i] + '</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
				}

				$("#mainLoader").hide();
			},
			error: function(jqXHR, textStatus, errorThrown) {
				if (jqXHR.status == 401) {
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});
	});

	$(document).on('click', '.wf-block .remove-box', function(e) {
		var box = $(this).closest(".wkf-step-row");
		console.log(box);
		swal({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Delete '
		}).then((result) => {
			if (result.value) {
				$(box).remove();
				$("#wfBlock1 .add-block").removeClass("hide-imp");
				$("#wfTitle").attr("data-step-data", "");
				manageWfWidth();
				countStepLevel();
			}
		});
	});

	$(document).on('click', '.wf-block .edit-box', function(e) {
		clearBlockModal();
		$("#editableWfblockId").val($(this).parent().attr("id"));

		$(".condition-row").css({
			"transform": "scale(0)",
			"height": "0"
		});

		$("input[type='checkbox'].blockCndt").prop("checked", false);

		var boxData = JSON.parse(decodeURIComponent($(this).attr("data-box-data")));
		console.log(boxData);
		$("#stepName").val(boxData.stepName);
		$("#assigneeMode").val(boxData.assigneeMode);
		$("#message").val(boxData.message);
		//$("#approvalTo").val( boxData.approvalTo ).trigger('change');
		$("#emailTo").val(boxData.emailTo).trigger('change');
		//$("input[type='radio'][name='blockCndt'][value='"+boxData.cndTyp+"']").prop('checked', true);
		var aprvHtml = "";

		if (boxData.aprvArr != undefined && boxData.aprvArr.length > 0) {
			for (var y = 0; y < boxData.aprvArr.length; y++) {
				aprvHtml += '<div class="card rounded border" data-aprv-id="' + boxData.aprvArr[y].aprvId + '"  data-aprv-nm="' + boxData.aprvArr[y].aprvNm + '">' +
					'<div class="card-body">' +
					'<div class="media">' +
					'<div class="media-body">' +
					'<h6 class="mb-0"><span class="seq-num">' + boxData.aprvArr[y].order + '</span><span class="mr-1">.</span>' + boxData.aprvArr[y].aprvNm + '</h6>' +
					'</div>' +
					//'<i class="icon-cursor-move align-self-center mr-1 move-aprv c-move"></i>'+
					'<i class="icon-trash align-self-center ml-1 remove-aprv c-pointer text-danger"></i>' +
					'</div>' +
					'</div>' +
					'</div>';
			}
		}
		$("#aprvList").html(aprvHtml);
		$("#aprvList").show();

		if (boxData.amountRow) {
			$("input[type='checkbox'][name='amountRow']").prop('checked', true);
			$("#amount").val(boxData.amount.replace(/\,/g, ""));
			$("#amountTo").val(boxData.amountTo.replace(/\,/g, ""));
			$("#amountTo").removeAttr("readonly");
			$("#amountRow").css({
				"height": "initial",
				"transform": "scale(1)"
			});
			$("#amountRow").addClass("mb-2");
		}
		if (boxData.branchRow) {
			$("input[type='checkbox'][name='branchRow']").prop('checked', true);
			$("#brh").val(boxData.brh).trigger('change');
			$("#branchRow").css({
				"height": "initial",
				"transform": "scale(1)"
			});
			$("#branchRow").addClass("mb-2");
		}
		if (boxData.vendorRow) {
			$("input[type='checkbox'][name='vendorRow']").prop('checked', true);
			$("#vndr").val(boxData.venId).trigger('change');
			$("#vendorRow").css({
				"height": "initial",
				"transform": "scale(1)"
			});
			$("#vendorRow").addClass("mb-2");

			if (boxData.costRecon) {
				$("input[type='checkbox'][name='costRecon']").prop('checked', true);
			} else {
				$("input[type='checkbox'][name='costRecon']").prop('checked', false);
			}
		}
		if (boxData.categoryRow) {
			$("input[type='checkbox'][name='categoryRow']").prop('checked', true);
			$("#vchrCat").val(boxData.vchrCat).trigger('change');
			$("#categoryRow").css({
				"height": "initial",
				"transform": "scale(1)"
			});
			$("#categoryRow").addClass("mb-2");
		}
		if (boxData.daysRow) {
			$("input[type='checkbox'][name='daysRow']").prop('checked', true);
			$("#nbrDays").val(boxData.days);
			$("input[type='radio'][name='daysCndt'][value='" + boxData.daysCndt + "']").prop('checked', true);
			$("#daysRow").css({
				"height": "initial",
				"transform": "scale(1)"
			});
			$("#daysRow").addClass("mb-2");
		}

		$("#editBlockMode").val(1);
		$("#blockModal").modal("show");

		//getUsrBrhVndrList(boxData.approvalTo, boxData.brh, boxData.venId);
	});

	$(document).on("click", "#saveBlock", function() {
		$("#mainLoader").show();

		$("#blockModal .form-control, #blockModal .select2-selection, .condition-checkbox-row").removeClass("border-danger");

		var editMode = false;
		var errorFlag = false;
		var destId = $("#editableWfblockId").val();

		if ($("#editBlockMode").val() == 0) {
			wfBlockCnt++;
		} else {
			wfBlockCnt = destId.substring(7);
			editMode = true;
		}

		var cndtn = "",
			approvalStr = "";
		var stepName = $("#stepName").val();
		var assigneeMode = $("#assigneeMode").val();
		var message = $("#message").val();
		var approvalTo = $("#approvalTo").val();
		var aprvArr = [];
		var emailTo = $("#emailTo").val();

		var amountRow = $("input[type='checkbox'][name='amountRow']").prop("checked");
		var branchRow = $("input[type='checkbox'][name='branchRow']").prop("checked");
		var vendorRow = $("input[type='checkbox'][name='vendorRow']").prop("checked");
		var vchrCatRow = $("input[type='checkbox'][name='categoryRow']").prop("checked");
		var daysRow = $("input[type='checkbox'][name='daysRow']").prop("checked");

		var costRecon = $("input[type='checkbox'][name='costRecon']").prop("checked");

		var amountTyp = $("#amountType").val();
		var amount = $("#amount").val();
		var amountTo = $("#amountTo").val();
		var brh = $("#brh").val();
		var venId = $("#vndr").val();
		var nbrDays = $("#nbrDays").val();
		var daysCndt = $("input[type='radio'][name='daysCndt']:checked").val();
		var vchrCat = $("#vchrCat").val();

		var aprvOrd = 1;
		$("#aprvList .card").each(function(index, row) {
			var aprvObj = {
				order: aprvOrd,
				aprvId: $(row).attr("data-aprv-id"),
				aprvNm: $(row).attr("data-aprv-nm"),
			};
			aprvArr.push(aprvObj);

			approvalStr += (approvalStr.length) ? " > " : "";
			approvalStr += '<span class="badge badge-pill badge-outline-primary">' + aprvOrd + '</span>' + " " + $(row).attr("data-aprv-nm");

			aprvOrd++;
		});

		if (!amountRow && !branchRow && !vendorRow && !daysRow && !vchrCatRow) {
			errorFlag = true;
			$(".condition-checkbox-row").addClass("border-danger");
		}

		if (amountRow) {
			//cndtn += "If amount " + amountTyp + " " + amount;
			if (amountTyp == "" || amountTyp == null) {
				errorFlag = true;
				$("#amountType").addClass("border-danger");
			}

			if (amount == "") {
				errorFlag = true;
				$("#amount").addClass("border-danger");
			}

			if (amountTyp == "range" && amountTo == "") {
				errorFlag = true;
				$("#amountTo").addClass("border-danger");
			} else if (amountTyp == "range" && parseFloat(amount) > parseFloat(amountTo)) {
				errorFlag = true;
				$("#amount, #amountTo").addClass("border-danger");
			}

			if (amountTyp == "range") {
				cndtn += "<span class='text-primary'>If amount between " + amount + " - " + amountTo + "</span>";
			}
		}

		if (branchRow) {
			cndtn += (cndtn == "") ? '' : ' AND ';
			cndtn += "<span class='text-primary'>If Branch is (" + brh + ")</span>";
			if (brh == "") {
				errorFlag = true;
				$("#brh").next().find(".select2-selection").addClass("border-danger");
			}
		}

		if (vendorRow) {
			cndtn += (cndtn == "") ? '' : ' AND ';
			cndtn += "<span class='text-primary'>If Vendor is (" + venId + ") </span>";
			cndtn += (costRecon) ? "<span class='text-info'>Cost Reconciliation</span>" : '';

			if (venId == "") {
				errorFlag = true;
				$("#vndr").next().find(".select2-selection").addClass("border-danger");
			}
		}
		
		if (vchrCatRow) {
			cndtn += (cndtn == "") ? '' : ' AND ';
			cndtn += "<span class='text-primary'>If Category is (" + vchrCat + ") </span>";

			if (vchrCat == "") {
				errorFlag = true;
				$("#vchrCat").next().find(".select2-selection").addClass("border-danger");
			}
		}

		if (daysRow) {
			cndtn += (cndtn == "") ? '' : ' AND ';

			if (daysCndt == "frmInvDt") {
				cndtn += "<span class='text-primary'>" + nbrDays + " days from the invoice date</span>";
			} else {
				cndtn += "<span class='text-primary'>" + nbrDays + " days before invoice due date</span>";
			}

			if (nbrDays == "") {
				errorFlag = true;
				$("#nbrDays").addClass("border-danger");
			}
		}

		/*if( approvalTo == "" || approvalTo == null ){
			errorFlag = true;
			$("#approvalTo").next().find(".select2-selection").addClass("border-danger");
		}*/

		if (aprvArr.length <= 0) {
			errorFlag = true;
			$("#approvalTo").next().find(".select2-selection").addClass("border-danger");
		}

		if (stepName == "" || stepName == null) {
			errorFlag = true;
			$("#stepName").addClass("border-danger");
		}

		if (errorFlag == true) {
			$("#mainLoader").hide();
			return;
		}

		var boxData = {};
		boxData.wkfName = $("#wfTitle").text();
		boxData.crtdBy = localStorage.getItem("starOcrUserId");
		boxData.stepName = stepName;
		boxData.assigneeMode = assigneeMode;
		boxData.message = message;
		boxData.approvalTo = approvalTo;
		boxData.aprvArr = aprvArr;
		boxData.emailTo = emailTo;
		boxData.amountRow = amountRow;
		boxData.branchRow = branchRow;
		boxData.vendorRow = vendorRow;
		boxData.categoryRow = vchrCatRow;
		boxData.daysRow = daysRow;
		boxData.amountTyp = amountTyp;
		boxData.amount = amount;
		boxData.amountTo = amountTo;
		boxData.brh = brh;
		boxData.venId = venId;
		boxData.vchrCat = vchrCat;
		boxData.nbrDays = nbrDays;
		boxData.daysCndt = daysCndt;
		boxData.costRecon = costRecon;

		var boxDataStr = encodeURIComponent(JSON.stringify(boxData));

		if (message != undefined) {
			message = message.replace(/\n/g, "<br>");
		} else {
			message = "";
		}

		var rtnSts = validateCondition(boxData);

		if (rtnSts == false) {
			return;
		}

		$("#wfBlock1 .wkf-end-row").hide();

		if (editMode == true) {
			var blockStr = '<i class="icon-note edit-box" id="editBox' + wfBlockCnt + '" data-box-data="' + boxDataStr + '"></i> <i class="icon-close remove-box" id="closeBox' + wfBlockCnt + '"></i> <div class="text-center d-block"> <p class="condition-para"><span class="condition-span">' + cndtn + '</span></p> </div> <div class="text-center d-block"> <div class="alert alert-primary text-center m-0" role="alert"> <h4 class="mb-1 wkf-step"><span class="wkf-level"></span>' + stepName + '</h4> <p class="mb-0">' + message + '</p> <p class="mb-0"><span class="text-warning font-weight-bold">Approver: </span>' + approvalStr + '</p> </div> </div> <div class="row"> <div class="col"> <div class="text-center d-block"><img src="assets/images/down-arrow24x24.png" class="down-arrow-img"></div> <div class="text-center d-block"> <div class="alert alert-success text-center" role="alert">End</div> </div> </div> </div>';
			$("#" + destId).html(blockStr);
		} else {
			if ($("#" + destId + " .row").length == 0) {
				var blockStr = '<div class="row"> <div class="col wkf-step-row"> <div class="wf-block" id="wfBlock' + wfBlockCnt + '"> <i class="icon-note edit-box" id="editBox' + wfBlockCnt + '" data-box-data="' + boxDataStr + '"></i> <i class="icon-close remove-box" id="closeBox' + wfBlockCnt + '"></i> <div class="text-center d-block"> <p class="condition-para"><span class="condition-span">' + cndtn + '</span></p> </div> <div class="text-center d-block"> <div class="alert alert-primary text-center m-0" role="alert"> <h4 class="mb-1 wkf-step"><span class="wkf-level"></span>' + stepName + '</h4> <p class="mb-0">' + message + '</p> <p class="mb-0"><span class="text-warning font-weight-bold">Approver: </span>' + approvalStr + '</p> </div> </div> <div class="row wkf-end-row"> <div class="col"> <div class="text-center d-block"><img src="assets/images/down-arrow24x24.png" class="down-arrow-img"></div> <div class="text-center d-block"> <div class="alert alert-success text-center" role="alert">End</div> </div> </div> </div> </div> </div> </div>';
				$("#" + destId).append(blockStr);
			} else {
				var blockStr = '<div class="col-12 wkf-step-row"> <div class="wf-block" id="wfBlock' + wfBlockCnt + '"> <i class="icon-note edit-box" id="editBox' + wfBlockCnt + '" data-box-data="' + boxDataStr + '"></i> <i class="icon-close remove-box" id="closeBox' + wfBlockCnt + '"></i> <div class="text-center d-block"> <p class="condition-para"><span class="condition-span">' + cndtn + '</span></p> </div> <div class="text-center d-block"> <div class="alert alert-primary text-center m-0" role="alert"> <h4 class="mb-1 wkf-step"><span class="wkf-level"></span>' + stepName + '</h4> <p class="mb-0">' + message + '</p> <p class="mb-0"><span class="text-warning font-weight-bold">Approver: </span>' + approvalStr + '</p> </div> </div> <div class="row wkf-end-row"> <div class="col"> <div class="text-center d-block"><img src="assets/images/down-arrow24x24.png" class="down-arrow-img"></div> <div class="text-center d-block"> <div class="alert alert-success text-center" role="alert">End</div> </div> </div> </div> </div> </div>';
				$("#" + destId + " .row:first").append(blockStr);
			}
		}

		//$("#wfBlock1 .add-block").addClass("hide-imp");
		$("#wfTitle").attr("data-step-data", boxDataStr);

		manageWfWidth();

		countStepLevel();

		$("#blockModal").modal("hide");
		$("#mainLoader").hide();
	});


	$("#saveDfltWk").click(function() {
		$("#mainLoader").show();

		var aprvArr = []
		$($("#dfltAprvList .card")).each(function(index, row) {
			var aprvObj = {
				order: $(row).find(".seq-num").html(),
				aprvId: $(row).attr("data-aprv-id"),
				aprvNm: $(row).attr("data-aprv-nm"),
			}
			aprvArr.push(aprvObj);
		});

		var inputData = {
			wkfId: $("#saveDfltWk").attr("data-wkf-id"),
			wkfName: "Default",
			crtdBy: localStorage.getItem("starOcrUserId"),
			stepName: "Default",
			amtFlg: 0,
			brhFlg: 0,
			venFlg: 0,
			dayFlg: 0,
			amtFrm: 0,
			amtTo: 0,
			brh: "",
			venId: "",
			days: 0,
			dayBef: 0,
			apvr: aprvArr,
			costRecon: 0,
		};

		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify(inputData),
			url: rootAppName + '/rest/wkf/create-wkf',
			type: 'POST',
			dataType: 'json',
			success: function(response) {
				if (response.output.rtnSts == 0) {
					$("#dfltWfModal").modal("hide");
					customAlert("#mainNotify", "alert-success", "Default workflow saved successfully.");
					getWorkflow();
				} else {
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for (var i = 0; i < loopLimit; i++) {
						errList += '<li>' + response.output.messages[i] + '</li>';
					}
					errList += '</ul>';
					customAlert("#popupNotify", "alert-danger", errList);
					rtnSts = false;
				}
				$("#mainLoader").hide();
			},
			error: function(jqXHR, textStatus, errorThrown) {
				if (jqXHR.status == 401) {
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});
	});

});


function countStepLevel() {
	var levelNo = 1;
	$("#wfBlock1 .wkf-level").each(function(index, level) {
		$(level).html("Level " + levelNo + ": ");
		$(level).addClass("text-warning");
		levelNo++;
	});
}

function clearBlockModal() {
	$("#stepName, #message, #assigneeMode, #nbrDays, #amount, #amountTo").val('');
	//$("#amountType").val('');
	$("#assigneeMode").val('All');
	$("#approvalTo").val("").trigger('change');
	$("#emailTo, #brh, #vndr").val(['']).trigger('change');
	$("input[type='radio'][name='daysCndt'][value='frmInvDt']").prop('checked', true);
	$("input[type='checkbox'].blockCndt").prop('checked', false);
	$("input[type='checkbox'][name='costRecon']").prop('checked', false);
	$(".condition-row").css({
		"transform": "scale(0)",
		"height": "0"
	});
	$("#aprvList").html("");
	$("#aprvList").hide();
}

function manageWfWidth() {
	if ($("#wfBlock1>.row>.col").length > 2) {
		$("#wfBlock1").addClass("col-8");
	} else {
		$("#wfBlock1").removeClass("col-8");
	}
}

/*
 * Validate Condition
 */
function validateCondition(boxData) {
	$("#mainLoader").show();

	var amtFlg = 0,
		brhFlg = 0,
		venFlg = 0,
		dayFlg = 0,
		catFlg = 0,
		amtFrm = 0,
		amtTo = 0,
		days = 0,
		dayBef = 0,
		brhVndrSize = 0,
		costRecon = 0;
		catSize = 0;
	var brh = '',
		venId = '',
		vchrCat = '',
		errTxt = '',
		keyNm = '';
	var rtnSts = false;

	var wkfId = ($("#saveWfBtn").attr("data-wkf-id") != undefined) ? $("#saveWfBtn").attr("data-wkf-id") : "";

	if (boxData.amountRow) {
		amtFlg = 1;
		amtFrm = boxData.amount;
		amtTo = boxData.amountTo;
	}

	if (boxData.branchRow) {
		brhFlg = 1;
		brh = boxData.brh.toString();
		brhVndrSize = boxData.brh.length;
	}
	
	if (boxData.categoryRow) {
		catFlg = 1;
		vchrCat = boxData.vchrCat.toString();
		catSize = boxData.vchrCat.length;
	}

	if (boxData.vendorRow) {
		venFlg = 1;
		venId = boxData.venId.toString();
		brhVndrSize = boxData.venId.length;

		if (boxData.costRecon) {
			costRecon = 1;
		}
	}

	if (boxData.daysRow) {
		dayFlg = 1;
		days = boxData.nbrDays;
	}

	if (boxData.bfrInvDueDt) {
		dayBef = 1
	}

	var inputData = {
		wkfId: wkfId,
		wkfName: boxData.wkfName,
		crtdBy: boxData.crtdBy,
		stepName: boxData.stepName,
		amtFlg: amtFlg,
		brhFlg: brhFlg,
		venFlg: venFlg,
		dayFlg: dayFlg,
		catFlg: catFlg,
		amtFrm: amtFrm,
		amtTo: amtTo,
		brh: brh,
		venId: venId,
		vchrCat: vchrCat,
		days: days,
		dayBef: dayBef,
		apvr: boxData.approvalTo,
		brhVndrSize: brhVndrSize,
		costRecon: costRecon,
	};

	if (inputData.amtFlg) {
		errTxt += "<span class='text-primary'>If Amount between " + amtFrm + " - " + amtTo + "</span>";
	}
	if (inputData.brhFlg) {
		errTxt += (errTxt == "") ? '' : ' AND ';
		errTxt += "<span class='text-primary'>If Branch is (" + brh + ")</span>";
	}
	
	if (inputData.catFlg) {
		errTxt += (errTxt == "") ? '' : ' AND ';
		errTxt += "<span class='text-primary'>If Category is (" + vchrCat + ")</span>";
	}
	
	if (inputData.venFlg) {
		errTxt += (errTxt == "") ? '' : ' AND ';
		errTxt += "<span class='text-primary'>If Vendor is (" + venId + ") </span>";
		errTxt += (inputData.costRecon) ? "<span class='text-info'>Cost Reconciliation</span>" : '';
	}
	if (inputData.dayFlg) {
		errTxt += (errTxt == "") ? '' : ' AND ';
		if (dayBef == 1) {
			errTxt += "<span class='text-primary'>" + days + " days before invoice due date</span>";
		} else {
			errTxt += "<span class='text-primary'>" + days + " days from the invoice date</span>";
		}
	}

	$.ajax({
		async: false,
		headers: ajaxHeader,
		data: JSON.stringify(inputData),
		url: rootAppName + '/rest/wkf/vldt-stp',
		type: 'POST',
		dataType: 'json',
		success: function(response) {
			if (response.output.rtnSts == 0) {
				var loopLimit = response.output.fldTblWkfChk.length;
				if (loopLimit) {
					var table = '',
						swal_html = '';

					table = '<table class="table table-border"><thead class="thead-clr"><tr><th>Workflow</th><th>Condition</th></tr></thead><tbody>';
					for (var i = 0; i < loopLimit; i++) {
						var cond = "";
						if (inputData.amtFlg) {
							cond += "<span class='text-primary'>If Amount between " + response.output.fldTblWkfChk[i].amtFrom + " - " + response.output.fldTblWkfChk[i].amtTo + "</span>";
						}
						if (inputData.brhFlg) {
							cond += (cond == "") ? '' : ' AND ';
							cond += "<span class='text-primary'>If Branch is (" + response.output.fldTblWkfChk[i].brh + ")</span>";
						}
						
						if (inputData.vchrCatFlg) {
							cond += (cond == "") ? '' : ' AND ';
							cond += "<span class='text-primary'>If Category is (" + response.output.fldTblWkfChk[i].vchrCat + ")</span>";
						}
						
						if (inputData.venFlg) {
							cond += (cond == "") ? '' : ' AND ';
							cond += "<span class='text-primary'>If Vendor is (" + response.output.fldTblWkfChk[i].venId + ") </span>";
							cond += (response.output.fldTblWkfChk[i].costRecon) ? "<span class='text-info'>Cost Reconciliation</span>" : '';
						}
						if (inputData.dayFlg) {
							cond += (cond == "") ? '' : ' AND ';
							if (response.output.fldTblWkfChk[i].dayBef == 1) {
								cond += "<span class='text-primary'>" + response.output.fldTblWkfChk[i].days + " days before invoice due date</span>";
							} else {
								cond += "<span class='text-primary'>" + response.output.fldTblWkfChk[i].days + " days from the invoice date</span>";
							}
						}

						table += '<tr><td>' + response.output.fldTblWkfChk[i].wfName + '</td><td>' + cond + '</td></tr>';
					}
					table += '</tbody></table>';

					swal_html = '<div><p>This condition <b>' + errTxt + '</b> is conflicting with below existing workflows.</p> <div class="table-responsive" style="max-height: 500px; overflow:auto;">' + table + '</div></div>';

					swal({
						type: 'error',
						position: 'top',
						/* title: "Error...", */
						allowOutsideClick: false,
						allowEscapeKey: false,
						html: swal_html
					});
					rtnSts = false;
				} else {
					rtnSts = true;
				}
			} else {
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for (var i = 0; i < loopLimit; i++) {
					errList += '<li>' + response.output.messages[i] + '</li>';
				}
				errList += '</ul>';
				customAlert("#popupNotify", "alert-danger", errList);
				rtnSts = false;
			}
			$("#mainLoader").hide();
		},
		error: function(jqXHR, textStatus, errorThrown) {
			if (jqXHR.status == 401) {
				validateAuthCode();
			}
			$("#mainLoader").hide();
			rtnSts = false;
		}
	});

	$("#mainLoader").hide();
	return rtnSts;
}

function getWorkflow() {
	$("#mainLoader").show();

	var inputData = {
		wkfId: "",
	};

	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify(inputData),
		url: rootAppName + '/rest/workflow/read',
		type: 'POST',
		dataType: 'json',
		success: function(response) {
			if (response.output.rtnSts == 0) {
				var loopLimit = response.output.fldTblWorkflow.length;
				var tblRow = '',
					dltBtn = '',
					srNo = 1;

				if (loopLimit == 0) {
					tblRow += '<tr class=""><td colspan="7" class="tbl-no-records">No Records Found</td></tr>';
				}

				for (var i = 0; i < loopLimit; i++) {
					if (response.output.fldTblWorkflow[i].wkfName.trim().toLowerCase() == "default") {
						var aprvHtml = "";
						var pndngDoc = response.output.fldTblWorkflow[i].pndgDoc;
						var deletedFlg = response.output.fldTblWorkflow[i].deletedByNm;
						
						var delBtn = "";
						
						$("#dfltAssignedInv").html(pndngDoc);
						
						if(pndngDoc == 0 )
						{
							delBtn = '<i class="icon-trash align-self-center ml-1 remove-aprv c-pointer text-danger"></i>';
							$("#dfltAprv").removeAttr("disabled");
							$("#saveDfltWk").removeAttr("disabled");
							$("#dfltWfSts").removeAttr("disabled");
							$("#dfltWfSts").next().removeClass("disabled");
							
						}
						else
						{
							delBtn = '';
							$("#dfltAprv").attr("disabled", "disabled");
							$("#saveDfltWk").attr("disabled", "disabled");
							$("#dfltWfSts").attr("disabled", "disabled");
							$("#dfltWfSts").next().addClass("disabled");
							
							
						}
						
						if(deletedFlg.length > 0)
						{
							delBtn = '';
							$("#saveDfltWk").attr("disabled", "disabled");
							$("#dfltAprv").attr("disabled", "disabled");
							$("#dfltWfSts").prop("checked", true);
						}
						else
						{
							$("#saveDfltWk").removeAttr("disabled");
							$("#dfltWfSts").prop("checked", false);
						}
						
						$(response.output.fldTblWorkflow[i].wkfAprvrList).each(function(index, row) {
							console.log(row);
							aprvHtml += '<div class="card rounded border" data-aprv-id="' + row.usrId + '"  data-aprv-nm="' + row.usrNm + '">' +
								'<div class="card-body">' +
								'<div class="media">' +
								'<div class="media-body">' +
								'<h6 class="mb-0"><span class="seq-num">' + row.order + '</span><span class="mr-1">.</span>' + row.usrNm + '</h6>' +
								'</div>' +
								delBtn +
								'</div>' +
								'</div>' +
								'</div>';
						});

						$("#dfltAprvList").html(aprvHtml);
						if (aprvHtml != "") {
							$("#dfltAprvList").show();
						} else {
							$("#dfltAprvList").hide();
						}
						$("#saveDfltWk").attr("data-wkf-id", response.output.fldTblWorkflow[i].wkfId);
						continue;
					}

					dltBtn = (response.output.fldTblWorkflow[i].pndgDoc != "0") ? '' : '<button class="btn btn-xs btn-danger delete-wf">Disable</button>';

					var cndtn = "";
					if (response.output.fldTblWorkflow[i].amtFlg == 1) {
						cndtn += (cndtn == "") ? '' : ' AND ';
						cndtn += "<span class='text-primary'>If Amount between " + response.output.fldTblWorkflow[i].amtFrm + " - " + response.output.fldTblWorkflow[i].amtTo + "</span>";
					}
					if (response.output.fldTblWorkflow[i].brhFlg == 1) {
						cndtn += (cndtn == "") ? '' : ' AND ';
						cndtn += "<span class='text-primary'>If Branch is (" + response.output.fldTblWorkflow[i].brh + ")</span>";
					}
					if (response.output.fldTblWorkflow[i].venFlg == 1) {
						cndtn += (cndtn == "") ? '' : ' AND ';
						cndtn += "<span class='text-primary'>If Vendor is (" + response.output.fldTblWorkflow[i].venId + ") </span>";
						cndtn += (response.output.fldTblWorkflow[i].costRecon == 1) ? " <span class='text-info'>(Cost Reconciliation)</span>" : '';
					}
					if (response.output.fldTblWorkflow[i].vchrCatFlg == 1) {
						cndtn += (cndtn == "") ? '' : ' AND ';
						cndtn += "<span class='text-primary'>If Category is (" + response.output.fldTblWorkflow[i].vchrCat + ")</span>";
					}

					if (response.output.fldTblWorkflow[i].dayFlg == 1) {
						cndtn += (cndtn == "") ? '' : ' AND ';
						if (response.output.fldTblWorkflow[i].dayBef == 1) {
							cndtn = "<span class='text-primary'>" + response.output.fldTblWorkflow[i].days + " days before invoice due date</span>";
						} else {
							cndtn = "<span class='text-primary'>" + response.output.fldTblWorkflow[i].days + " days from the invoice date</span>";
						}
					}
					
					var wkfBtn = '';
					
					if(response.output.fldTblWorkflow[i].deletedByNm.length > 0)
					{
						wkfBtn = '<button class="btn btn-xs btn-danger mr-2 enable-wf" data-pndg-doc="' + response.output.fldTblWorkflow[i].pndgDoc + '" data-del-flg="E">Enable</button>' ;
					}
					else
					{
						wkfBtn = '<button class="btn btn-xs btn-primary mr-2 edit-wf" data-pndg-doc="' + response.output.fldTblWorkflow[i].pndgDoc + '">Edit</button>' + dltBtn ;
					}
					
					tblRow += '<tr class="" data-wkf-id="' + response.output.fldTblWorkflow[i].wkfId + '">' +
						'<td>' + srNo + '</td>' +
						'<td>' + response.output.fldTblWorkflow[i].wkfName + '</td>' +
						'<td>' + response.output.fldTblWorkflow[i].pndgDoc + '</td>' +
						'<td>' + cndtn + '</td>' +
						'<td>' + response.output.fldTblWorkflow[i].crtdByNm + '</td>' +
						'<td>' + response.output.fldTblWorkflow[i].crtdDtts + '</td>' +
						'<td>' + response.output.fldTblWorkflow[i].deletedByNm + '</td>' +
						'<td>' + response.output.fldTblWorkflow[i].deletedDtts + '</td>' +
						'<td>' +
						wkfBtn +
						'</td>' +
						'</tr>';
					srNo++;
				}

				$("#savedWfTbl tbody").html(tblRow);

			} else {
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for (var i = 0; i < loopLimit; i++) {
					errList += '<li>' + response.output.messages[i] + '</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}

			$("#mainLoader").hide();
		},
		error: function(jqXHR, textStatus, errorThrown) {
			if (jqXHR.status == 401) {
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}


/**
 * Delete Workflow Function
 **/
function deleteWorkFlow(wkfId, flg) {
	$("#mainLoader").show();

	var inputData = {
		wkfId: wkfId,
		flg: flg
	};

	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify(inputData),
		url: rootAppName + '/rest/workflow/delete',
		type: 'POST',
		dataType: 'json',
		success: function(response) {
			if (response.output.rtnSts == 0) {
				$("#deleteWkfModal").modal("hide");
				
				if(flg == 'D')
				{
					customAlert("#mainNotify", "alert-success", "Workflow Disabled successfully.");
				}
				else if(flg == 'E')
				{
					customAlert("#mainNotify", "alert-success", "Workflow Enabled successfully.");
				}
					

				getWorkflow();
			} else {
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for (var i = 0; i < loopLimit; i++) {
					errList += '<li>' + response.output.messages[i] + '</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}

			$("#mainLoader").hide();
		},
		error: function(jqXHR, textStatus, errorThrown) {
			if (jqXHR.status == 401) {
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}

function aproverSequence(element) {
	var y = 1;
	$("#" + element + " .card").each(function(index, row) {
		$(row).find(".seq-num").html(y);
		y++;
	});

	if ($("#" + element + " .card").length) {
		$("#" + element + "").show();
	} else {
		$("#" + element + "").hide();
		$("#approvalTo, #dfltAprv").val("").trigger("change");
	}
}