$(function(){
	
	getCmpyIdList( $("#cmpyId") );
	
	getBankList($("#cmpyBankId"));
	
	getCmpySetupData();
	
	$("#cmpyBankId").change(function(){
		var bankCode = $(this).val();
		
		if( bankCode != "" ){
			var inputData = { 
				bnkCode : bankCode,
			};
			
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/bank/read',
				type: 'POST',
				dataType : 'json',
				success: function(response){
					if( response.output.rtnSts == 0 ){						
						var loopSize = response.output.fldTblBank[0].bankAcctList.length;
						var tblHtml = '', defaultAcc = '';
						for( var i=0; i<loopSize; i++ ){
							defaultAcc = '';
							if( response.output.fldTblBank[0].bankAcctList[i].default ){
								defaultAcc = ' <i class="icon-check text-success font-weight-bold display-5"></i>';
							}
						
							tblHtml += '<tr>'+
											'<td>'+ (i+1) +'</td>'+
											'<td>'+ response.output.fldTblBank[0].bankAcctList[i].bnkAcct +'</td>'+
											'<td>'+ response.output.fldTblBank[0].bankAcctList[i].bnkAcctDesc +'</td>'+
											'<td>'+ response.output.fldTblBank[0].bankAcctList[i].bnkAcctNo +'</td>'+
											'<td>'+ defaultAcc +'</td>'+
										'</tr>';
						}
						$("#bankAccDetailsWrapper table tbody").html(tblHtml);		
						$("#bankAccDetailsWrapper").slideDown();
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
					}
					
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					$("#mainLoader").hide();
				}
			});
		}else{
			$("#bankAccDetailsWrapper table tbody").html("");
			$("#bankAccDetailsWrapper").slideUp();
		}
	});
	
	$("#saveCmpySetup").click(function(){
		$("#cmpyStpFrm .form-control, #cmpyStpFrm .select2-selection").removeClass("border-danger");
		
		var errFlg = false;
		var cmpyId = $("#cmpyId").val();
		var cmpyBankId = $("#cmpyBankId").val();
		
		if( cmpyId == "" ){
			errFlg = true;
			$("#cmpyId").next().find(".select2-selection").addClass("border-danger");
		}
		
		if( cmpyBankId == "" ){
			errFlg = true;
			$("#cmpyBankId").next().find(".select2-selection").addClass("border-danger");
		}
		
		if( errFlg == false ){
			var inputData = { 
				cmpyId : cmpyId,
				bnkCode : cmpyBankId,
			};
			
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/company/add',
				type: 'POST',
				dataType : 'json',
				success: function(response){
					if( response.output.rtnSts == 0 ){						
						$("#clearCmpySetup").trigger("click");
						
						getCmpySetupData();
						
						customAlert("#mainNotify", "alert-success", "Company setup saved successfully.");
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
					}
					
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					$("#mainLoader").hide();
				}
			});
		}
	});
	
	$("#clearCmpySetup").click(function(){
		$("#cmpyId, #cmpyBankId").val("").trigger("change");
		$("#bankAccDetailsWrapper table tbody").html("");
		$("#bankAccDetailsWrapper").slideUp();
	});
	
});

function getCmpySetupData(){
	$("#mainLoader").show();
	
	var inputData = {
		cmpyId : "",
	};
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/company/read',
		data : JSON.stringify( inputData ),
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblCompany.length;
				var innerLoopSize = 0;
				var bnkCode = '', bankAccNumbers = '', bankInfo = '';
				var tbodyHtml = '';
				for( var i=0; i<loopSize; i++ ){
					bankAccNumbers = '';
					innerLoopSize = response.output.fldTblCompany[i].accoutnList.length;
					
					for( var j=0; j<innerLoopSize; j++ ){
						bankAccNumbers += '<div class="mb-2">' + response.output.fldTblCompany[i].accoutnList[j].bnkAcct + ' - ' + response.output.fldTblCompany[i].accoutnList[j].bnkAcctNo + ' (' + response.output.fldTblCompany[i].accoutnList[j].bnkAcctDesc + ')';
						if( response.output.fldTblCompany[i].accoutnList[j].default ){
							bankAccNumbers += ' <i class="icon-check text-success"></i>';
						}
						bankAccNumbers += '</div>';
					}
					
					tbodyHtml += '<tr>'+
									'<td>'+ (i+1) +'</td>'+
									'<td>'+ response.output.fldTblCompany[i].cmpyId +'</td>'+
									'<td>'+ response.output.fldTblCompany[i].bankCode +'</td>'+
									'<td>'+ bankAccNumbers +'</td>'+
									'<td><i class="icon-trash menu-icon icon-pointer delete icon-red font-weight-bold" title="Edit"></i></td>'+
								'</tr>';
				}
				$("#cmpyAchTbl tbody").html(tbodyHtml);
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}