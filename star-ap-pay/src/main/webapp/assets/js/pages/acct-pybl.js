$(function(){
	
	/*getVendorList( $("#vndrFltr"), glblCmpyId );
	
	getBranchList( $("#brhFltr") );
	
	getCurrencyList( $("#cryFltr") );
	
	$("#searchOpnItm").click(function(){
		$("#mainLoader").show();
		
		var venId = $("#vndrFltr").val();
		var brh = $("#brhFltr").val();
		var cry = $("#cryFltr").val();
		var typ = $("#typeFltr").val();
		
		venId = (venId==null || venId==undefined) ? "" : venId;
		brh = (brh==null || brh==undefined) ? "" : brh;
		cry = (cry==null || cry==undefined) ? "" : cry;
		typ = (typ==null || typ==undefined) ? "" : typ;
		
		var inputData = { 
			venId : venId,
			brh: brh,
			cry: cry,
			typ: typ,
		};
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/ap/opn-itm',
			type: 'POST',
			dataType : 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					var loopLimit = response.output.fldTblDoc.length;
					var tbodyRows = '', discDt = ''; 
					for( var y=0; y<loopLimit; y++){
						discDt = (response.output.fldTblDoc[y].vchrDiscDtStr != undefined) ? response.output.fldTblDoc[y].vchrDiscDtStr : "";
						tbodyRows += '<tr>'+
										'<td>'+ response.output.fldTblDoc[y].vchrDueDtStr +'</td>'+
										'<td>'+ response.output.fldTblDoc[y].vchrInvDtStr +'</td>'+
										'<td>'+ response.output.fldTblDoc[y].vchrPfx + '-' + response.output.fldTblDoc[y].vchrNo +'</td>'+
										'<td>'+ response.output.fldTblDoc[y].vchrInvNo +'</td>'+
										'<td>'+ response.output.fldTblDoc[y].vchrBrh +'</td>'+
										'<td>'+ discDt +'</td>'+
										'<td>'+ response.output.fldTblDoc[y].vchrCry +'</td>'+
										'<td>'+ response.output.fldTblDoc[y].discAmt +'</td>'+
										'<td>'+ response.output.fldTblDoc[y].balAmt +'</td>'+
										'<td>'+ response.output.fldTblDoc[y].inPrsAmt +'</td>'+
										'<td>'+ response.output.fldTblDoc[y].inPrsBal +'</td>'+
										'<td>'+ response.output.fldTblDoc[y].crtDttsStr +'</td>'+
										'<td>'+ response.output.fldTblDoc[y].vchrCat +'</td>'+
										'<td>'+ response.output.fldTblDoc[y].vchrVenId + '-' + response.output.fldTblDoc[y].vchrVenNm +'</td>'+
									'</tr>';
					}
					
					$("#opnItmTbl tbody").html(tbodyRows);
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
				}
				
				$("#mainLoader").hide();
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});
	});*/
	
	getAPSessions();
	
	$("#apSsnTbl").on("click", ".view-cash-dsb-trs", function(){
		var ssnId = $(this).text();
		var $row = $(this).closest("tr");
		
		$(".ssn-id").html( $row.attr("data-ssn-id") );
		$(".login-id").html( $row.attr("data-lgn-id") );
		$(".dsb-brh").html( $row.attr("data-dsb-brh") );
		$(".jrnl-dt, .dsb-dt").html( $row.attr("data-jrnl-dt") );
		$(".bank-name").html( $row.attr("data-bnk") );
		$(".bank-cry").html( $row.attr("data-dsb-cry") );
		$(".bank-exrt").html( $row.attr("data-dsb-exrt") );
		$(".ap-cry").html( $row.attr("data-ap-cry1") );
		$(".dsb-amt").html( $row.attr("data-dsb-amt") );
		$(".cross-rate").html( $row.attr("data-ap-xexrt1") );
		
		getCashDsbTrs(ssnId);
	});
	
	$("#apCashDsbTrsTbl").on("click", ".view-ap-open-itm", function(){
		var $row = $(this).closest("tr");
		var venId = $row.attr("data-ven-id");
		var dsbPfx = $row.attr("data-dsb-pfx");
		var dsbNo = $row.attr("data-dsb-no");
		
		$(".payee").html( $row.attr("data-ven-nm") );
		$(".pmt-typ").html( $row.attr("data-pmt-typ") );
		$(".chk-tot").html( $row.attr("data-chk-tot") + " " + $(".bank-cry").html() );
		$(".chk-dt").html( $row.attr("data-chk-dt") );
		$(".chk-no").html( ($row.attr("data-chk-no") != "0") ? $row.attr("data-chk-no") : "" );
		
		$(".view-ap-pymnt").attr("data-dsb-pfx", dsbPfx);
		$(".view-ap-pymnt").attr("data-dsb-no", dsbNo);
		
		getApOpenItems(venId);
	});
	
	$(".view-ap-pymnt").click(function(){
		var dsbPfx = $(this).attr("data-dsb-pfx");
		var dsbNo = $(this).attr("data-dsb-no");
		
		getApPayments(dsbPfx, dsbNo);
	});
	
	$(".back-to-session").click(function(){
		$("#cashDsbTrsSection").hide();
		$("#apSsnSection").slideDown();
	});
	
	$(".back-to-cash-dsb-trs").click(function(){
		$("#apOpenItmSection").hide();
		$("#cashDsbTrsSection").slideDown();
	});
	
	$(".back-to-open-itm").click(function(){
		$("#apPymntSection").hide();
		$("#apOpenItmSection").slideDown();
	});
	
	
});

function getAPSessions(){
	$("#mainLoader").show();
			
	var inputData = {};
														
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData),
		url: rootAppName + '/rest/ap/read-ap-ssn',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblAPEnqry.length;
				var tblRows = '';	
				for( var i=0; i<loopSize; i++ ){				
					tblRows += '<tr data-ssn-id="'+ response.output.fldTblAPEnqry[i].ssnId +'"'+
								   'data-lgn-id="'+ response.output.fldTblAPEnqry[i].lgnId +'"'+
								   'data-dsb-brh="'+ response.output.fldTblAPEnqry[i].dsbBrh +'"'+
								   'data-jrnl-dt="'+ response.output.fldTblAPEnqry[i].jrnlDt +'"'+
								   'data-bnk="'+ response.output.fldTblAPEnqry[i].dssBnk +'"'+
								   'data-dsb-cry="'+ response.output.fldTblAPEnqry[i].dsbCry +'"'+
								   'data-dsb-exrt="'+ response.output.fldTblAPEnqry[i].dsbExrt +'"'+
								   'data-ap-cry1="'+ response.output.fldTblAPEnqry[i].apCry1 +'"'+
								   'data-dsb-amt="'+ response.output.fldTblAPEnqry[i].disbAmt +'"'+
								   'data-ap-xexrt1="'+ response.output.fldTblAPEnqry[i].apXexrt1 +'"'+
								'>'+
									'<td><button class="btn btn-link p-0 view-cash-dsb-trs">'+ response.output.fldTblAPEnqry[i].ssnId +'</button></td>'+
									'<td>'+ response.output.fldTblAPEnqry[i].dssBnk +'</td>'+
									'<td>'+ response.output.fldTblAPEnqry[i].dsbCry +'</td>'+
									'<td>'+ response.output.fldTblAPEnqry[i].lgnId +'</td>'+
									'<td>'+ response.output.fldTblAPEnqry[i].dsbBrh +'</td>'+
									'<td>'+ response.output.fldTblAPEnqry[i].disbNo +'</td>'+
									//'<td></td>'+
									'<td>'+ response.output.fldTblAPEnqry[i].disbAmt +'</td>'+
									'<td>'+ response.output.fldTblAPEnqry[i].jrnlDt +'</td>'+
								'</tr>';
				}
				$("#apSsnTbl tbody").html( tblRows );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}


function getCashDsbTrs(ssnId){	
	$("#mainLoader").show();
			
	var inputData = {
		ssnId: ssnId,
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData),
		url: rootAppName + '/rest/ap/read-ap-disb',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblAPDisbursement.length;
				var tblRows = '', stsStr = '';	
				for( var i=0; i<loopSize; i++ ){
					stsStr = '';
					if( response.output.fldTblAPDisbursement[i].disbSts == "A" ){
						stsStr = '<span class="badge badge-primary">Active</span>';
					}else if( response.output.fldTblAPDisbursement[i].disbSts == "C" ){
						stsStr = '<span class="badge badge-danger">Close</span>';
					}						
					tblRows += '<tr data-dsb-pfx="'+ response.output.fldTblAPDisbursement[i].dsbPfx +'" '+
								   'data-dsb-no="'+ response.output.fldTblAPDisbursement[i].dsbNo +'" '+
								   'data-ven-id="'+ response.output.fldTblAPDisbursement[i].venId +'"'+
								   'data-ven-nm="'+ response.output.fldTblAPDisbursement[i].venNm +'"'+
								   'data-pmt-typ="'+ response.output.fldTblAPDisbursement[i].pmtType +'"'+
								   'data-chk-tot="'+ response.output.fldTblAPDisbursement[i].venChkAmt +'"'+
								   'data-chk-dt="'+ response.output.fldTblAPDisbursement[i].venChkDt +'"'+
								   'data-chk-no="'+ response.output.fldTblAPDisbursement[i].venChkNo +'"'+
								 '>'+
									'<td>'+ response.output.fldTblAPDisbursement[i].venId +'</td>'+
									'<td>'+ response.output.fldTblAPDisbursement[i].venNm +'</td>'+
									'<td><button class="btn btn-link p-0 view-ap-open-itm">'+ response.output.fldTblAPDisbursement[i].dsbNo +'</button></td>'+
									'<td>'+ response.output.fldTblAPDisbursement[i].pmtType +'</td>'+
									'<td>'+ response.output.fldTblAPDisbursement[i].venChkDt +'</td>'+
									'<td>'+ response.output.fldTblAPDisbursement[i].venChkAmt +'</td>'+
									'<td>'+ stsStr +'</td>'+
								'</tr>';
				}
				$("#apCashDsbTrsTbl tbody").html( tblRows );
				
				$("#apSsnSection").hide();
				$("#cashDsbTrsSection").slideDown();
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}


function getApOpenItems(venId){	
	$("#mainLoader").show();
			
	var inputData = {
		venId: venId,
		brh: $(".dsb-brh").html(),
		cry: $(".ap-cry").html(),
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData),
		url: rootAppName + '/rest/ap/opn-itm',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblOpnItm.length;
				var tblRows = '', stsStr = '', discAmtStr = '', balAmtStr = '', ipAmtStr = '', inPrsBalStr = '';	
				for( var i=0; i<loopSize; i++ ){
					discAmtStr = (response.output.fldTblOpnItm[i].discAmt != "0.00") ? response.output.fldTblOpnItm[i].discAmt : ""; 
					balAmtStr = (response.output.fldTblOpnItm[i].balAmt != "0.00") ? response.output.fldTblOpnItm[i].balAmt : ""; 
					ipAmtStr = (response.output.fldTblOpnItm[i].ipAmt != "0.00") ? response.output.fldTblOpnItm[i].ipAmt : ""; 
					inPrsBalStr = (response.output.fldTblOpnItm[i].inPrsBal != "0.00") ? response.output.fldTblOpnItm[i].inPrsBal : "";
					tblRows += '<tr>'+
									'<td>'+ response.output.fldTblOpnItm[i].dueDt +'</td>'+
									'<td>'+ response.output.fldTblOpnItm[i].invDt +'</td>'+
									'<td>'+ response.output.fldTblOpnItm[i].refPfx +'-'+ response.output.fldTblOpnItm[i].refNo +'</td>'+
									'<td>'+ response.output.fldTblOpnItm[i].invNo +'</td>'+
									'<td>'+ response.output.fldTblOpnItm[i].pmtTyp +'</td>'+
									'<td>'+ response.output.fldTblOpnItm[i].brh +'</td>'+
									'<td></td>'+
									'<td>'+ response.output.fldTblOpnItm[i].discDt +'</td>'+
									'<td>'+ response.output.fldTblOpnItm[i].cry +'</td>'+
									'<td>'+ discAmtStr +'</td>'+
									'<td>'+ balAmtStr +'</td>'+
									'<td>'+ ipAmtStr +'</td>'+
									'<td>'+ inPrsBalStr +'</td>'+
									'<td></td>'+
								'</tr>';
				}
				$("#apOpenItmTbl tbody").html( tblRows );
				
				$("#cashDsbTrsSection").hide();
				$("#apOpenItmSection").slideDown();
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}


function getApPayments(dsbPfx, dsbNo){	
	$("#mainLoader").show();
			
	var inputData = {
		dsbPfx: dsbPfx,
		dsbNo: dsbNo,
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData),
		url: rootAppName + '/rest/ap/read-check-view',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblChkVw.length;
				var tblRows = '', pmtAmtStr = '', dsbAmtStr = '', exrtStr = '';	
				for( var i=0; i<loopSize; i++ ){	
					pmtAmtStr = (response.output.fldTblChkVw[i].pmtAmt != "0.00") ? response.output.fldTblChkVw[i].pmtAmt : ""; 
					dsbAmtStr = (response.output.fldTblChkVw[i].dsbAmt != "0.00") ? response.output.fldTblChkVw[i].dsbAmt : ""; 
					exrtStr = (response.output.fldTblChkVw[i].exrt != "0.00") ? response.output.fldTblChkVw[i].exrt : ""; 
									
					tblRows += '<tr>'+
									'<td>'+ response.output.fldTblChkVw[i].refPfx + '-' + response.output.fldTblChkVw[i].refNo +'</button></td>'+
									'<td>'+ response.output.fldTblChkVw[i].invNo +'</td>'+
									'<td>'+ pmtAmtStr +'</td>'+
									'<td>'+ response.output.fldTblChkVw[i].cry +'</td>'+
									'<td>'+ dsbAmtStr +'</td>'+
									'<td>'+ exrtStr +'</td>'+
								'</tr>';
				}
				$("#apPymntTbl tbody").html( tblRows );
				
				$("#apOpenItmSection").hide();
				$("#apPymntSection").slideDown();
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}