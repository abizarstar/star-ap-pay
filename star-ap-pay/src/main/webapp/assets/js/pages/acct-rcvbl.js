$(function(){
	
	$("#pymntFltrDueDt").val("");
	
	getGlList( $("#glAccount") );
	
	getCustomerList( $("#fltrCusId, #pymntFltrCusId, #crCusId") );
	
	getBranchList( $("#pymntFltrBrh, #addSsnBrh") );
	
	getStxBankList( $("#addSsnBnk") );
	
	getCurrencyList( $("#addSsnArCry1, #addSsnArCry2, #addSsnArCry3") );
	
	getCashReceiptSessions();
	
	getAREnquiry();
	
	$("#glAccount").change(function(){
		var glAcct = $(this).val();
		
		if( glAcct != "" && glAcct != null && glAcct != undefined ){
			getGlSubAccList( glAcct, $("#subAccount") );
		}else{
			$("#subAccount").val("").trigger("change");
			$("#subAccount").attr("disabled", "disabled");
		}
	});
	
	$("#ssnDataTbl").on("click", ".view-cust-checks", function(){
		var ssnId = $(this).text();
		
		getCustChecks(ssnId);
	});
	
	$("#custChecksTbl").on("click", ".view-ar-open-items", function(){
		$("#custChecksTbl tbody tr").removeClass("current");
		$(this).closest("tr").addClass("current");
		
		var cusId = $(this).closest("tr").attr("data-cus-id");
		var sts = $(this).closest("tr").attr("data-sts");
		var chkAmt = $(this).closest("tr").attr("data-chk-amt");
		var chkProof = $(this).closest("tr").attr("data-chk-proof");
		var cashRcpt = $(this).text();
		
		if( sts == "A" ){
			$(".add-cash-rcpt-data").show();
		}else{
			$(".add-cash-rcpt-data").hide();
		}
		
		$(".add-cash-rcpt-data").attr("data-sts", sts);
		
		$("#cashRcptTitle").html( cashRcpt );
		$(".chk-amt").html(chkAmt);
		$(".chk-proof").html(chkProof);
	
		getCusAROpenItems(cusId, cashRcpt);
	});
	
	$(".back-to-session").click(function(){
		$("#custChecksSection").hide();
		$("#sessionSection").slideDown();
	});
	
	$(".back-to-cust-checks").click(function(){
		$("#arPymntSection").hide();
		$("#custChecksSection").slideDown();
	});
	
	$("#addSsnBtn").click(function(){
		$("#addSsnModal").modal("show")
	});
	
	$("#addSession").click(function(){
		addSession();
	});
	
	$("#addCashRcpt").click(function(){
		addCashReceipt();
	});

	$("#showCashRcptModal").click(function(){
		$("#crSsnId").val( $("#ssnIdTitle").text() );
		$("#modalSsnId").text( $("#ssnIdTitle").text() );
		$("#addCashRcptModal").modal("show");
	});
	
	$("#ssnFltr").click(function(){
		$(".ssnFltrWrapper").slideToggle();
	});
	
	$("#arPymntFltr").click(function(){
		$(".arPymntFltrWrapper").slideToggle();
	});
	
	$("#fltrSessions").click(function(){
		getCashReceiptSessions();
	});
	
	$("#resetSessions").click(function(){
		$("#fltrSsnId, #fltrCashRcptNo").val("");
		$("#fltrCusId").val("").trigger("change");
		
		getCashReceiptSessions();
	});
	
	$("#fltrArPymnt").click(function(){
		getAREnquiry();
	});
	
	$("#resetArPymnt").click(function(){
		$("#pymntFltrDueDt").val("");
		$("#pymntFltrCusId, #pymntFltrBrh").val("").trigger("change");
		
		getAREnquiry();
	});
	
	$("#addSsnBnk").change(function(){
		var bnk = $(this).val();
		
		$("#addSsnArCry1, #addSsnArCry2, #addSsnArCry3").val("").trigger("change");
		$("#addSsnCrsExchngRt1, #addSsnCrsExchngRt2, #addSsnCrsExchngRt3").val("");
		
		if( bnk != "" ){
			$("#addSsnBnkCry").val( $("#addSsnBnk option:selected").attr("data-cry") );
			$("#addSsnBnkExchngRt").val( $("#addSsnBnk option:selected").attr("data-exrt") );
		}else{
			$("#addSsnBnkCry, #addSsnBnkExchngRt").val("");
		}
	});
	
	$("#showArPymnts").click(function(){
		var refNo = $("#cashRcptTitle").text();
		$(".cash-rcpt-title").text( refNo );
		getPayments(refNo);
	});
	
	$("#showAdvncPymnt").click(function(){
		var refNo = $("#cashRcptTitle").text();
		$(".cash-rcpt-title").text( refNo );
		$("#apDesc, #apAdvAmt").val("");  
		
		getAdvancePayment(refNo);
	});
	
	$("#showUnplydDbt").click(function(){
		var refNo = $("#cashRcptTitle").text();
		$(".cash-rcpt-title").text( refNo );
		$("#udDesc, #udDrAmt").val("");  
		
		getUnappliedDebit(refNo);
	});
	
	$("#showGlDist").click(function(){
		var refNo = $("#cashRcptTitle").text();
		$(".cash-rcpt-title").text( refNo );
		
		$("#remark").val("");
		$("#drAmt").val(0);
		$("#crAmt").val(0);
		$("#glAccount").val($("#glAccount option:first").val()).trigger('change');
		$("#subAccount").val($("#subAccount option:first").val()).trigger('change');
		
		getGLDistribution(refNo);
	});
	
	$("#addAdvncPymnt").click(function(){
		$("#apDesc, #apAdvAmt").removeClass("border-danger");
		
		var errFlg = false;
		var apDesc = $("#apDesc").val().trim();
		var apAdvAmt = $("#apAdvAmt").val().trim();
		
		if( apDesc == "" ){
			errFlg = true;
			$("#apDesc").addClass("border-danger");
		}
		
		if( apAdvAmt == "" ){
			errFlg = true;
			$("#apAdvAmt").addClass("border-danger");
		}
		
		if( errFlg ){
			return;
		}
		
		$("#mainLoader").show();
	
		var inputData = {
			advAmt: apAdvAmt,
			desc: apDesc,
			cashRcptNo: $(".cash-rcpt-title").html(),
		};
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/rcpt/advPmt',
			type: 'POST',
			dataType : 'json',
			success: function(response){
				if( response.output.authToken == undefined || response.output.authToken == "" ){
					$("#logout").trigger("click");
				}
				
				localStorage.setItem("starOcrAuthToken", response.output.authToken);
				setAjaxHeader();
					
				if( response.output.rtnSts == 0 ){
					customAlert("#popupNotify", "alert-success", "Advance payment saved successfully.");
					$("#apDesc, #apAdvAmt").val("");
					
					getAdvancePayment($(".cash-rcpt-title").html());
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#popupNotify", "alert-danger", errList);
				
					$("#mainLoader").hide();
				}
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				console.log(jqXHR);
				console.log(textStatus);
				console.log(errorThrown);
				$("#mainLoader").hide();
			}
		});
		
	});
	

	$("#addUnapldDbt").click(function(){
		$("#udDesc, #udDrAmt").removeClass("border-danger");
		
		var errFlg = false;
		var udDesc = $("#udDesc").val().trim();
		var udDrAmt = $("#udDrAmt").val().trim();
		
		if( udDesc == "" ){
			errFlg = true;
			$("#udDesc").addClass("border-danger");
		}
		
		if( udDrAmt == "" ){
			errFlg = true;
			$("#udDrAmt").addClass("border-danger");
		}
		
		if( errFlg ){
			return;
		}
		
		$("#mainLoader").show();
	
		var inputData = {
			drAmt: udDrAmt,
			desc: udDesc,
			cashRcptNo: $(".cash-rcpt-title").html(),
		};
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/rcpt/unDebit',
			type: 'POST',
			dataType : 'json',
			success: function(response){
				if( response.output.authToken == undefined || response.output.authToken == "" ){
					$("#logout").trigger("click");
				}
				
				localStorage.setItem("starOcrAuthToken", response.output.authToken);
				setAjaxHeader();
					
				if( response.output.rtnSts == 0 ){
					customAlert("#popupNotify", "alert-success", "Unapplied debit saved successfully.");
					$("#udDesc, #udDrAmt").val("");
					
					getUnappliedDebit($(".cash-rcpt-title").html());
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#popupNotify", "alert-danger", errList);
				
					$("#mainLoader").hide();
				}
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				console.log(jqXHR);
				console.log(textStatus);
				console.log(errorThrown);
				$("#mainLoader").hide();
			}
		});
		
	});
	

	$("#addGlDist").click(function(){
		$("#glDistModal .form-control, #glDistModal .select2-selection").removeClass("border-danger");
		
		var errFlg = false;
		var glAcc = $("#glAccount").val();
		var glSubAcc = $("#subAccount").val();
		var drAmt = $("#drAmt").val().trim();
		var crAmt = $("#crAmt").val().trim();
		var remark = $("#remark").val().trim();
		var error = false;
		
		if( glAcc == "" || glAcc == "-" || glAcc == null || glAcc == undefined ){
			error = true;
			$("#glAccount").next().find(".select2-selection").addClass("border-danger");
		}
		
		if( $("#subAccount option").length > 1 ){
			if( glSubAcc == "" || glSubAcc == "-" ){
				error = true;
				$("#subAccount" ).next().find(".select2-selection").addClass("border-danger");
			}
		}
		
		if( (drAmt == "" || $.isNumeric(drAmt) == false || drAmt == 0) && (crAmt == "" || $.isNumeric(crAmt) == false || crAmt == 0) ){
			error = true;
			$("#drAmt" ).addClass("border-danger");
			$("#crAmt" ).addClass("border-danger");
		}
		
		if( remark == "" || remark.length == 0 ){
			remark = " ";
		}
		
		if( errFlg ){
			return;
		}
		
		$("#mainLoader").show();
	
		var inputData = {
			glAcc: glAcc,
			glSubAcc: glSubAcc,
			drAmt: drAmt,
			crAmt: crAmt,
			remark: remark,
			cashRcptNo: $(".cash-rcpt-title").html(),
		};
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/rcpt/glDist',
			type: 'POST',
			dataType : 'json',
			success: function(response){
				if( response.output.authToken == undefined || response.output.authToken == "" ){
					$("#logout").trigger("click");
				}
				
				localStorage.setItem("starOcrAuthToken", response.output.authToken);
				setAjaxHeader();
					
				if( response.output.rtnSts == 0 ){
					customAlert("#popupNotify", "alert-success", "Unapplied debit saved successfully.");
					
					$("#remark").val("");
					$("#drAmt").val(0);
					$("#crAmt").val(0);
					$("#glAccount").val($("#glAccount option:first").val()).trigger('change');
					$("#subAccount").val($("#subAccount option:first").val()).trigger('change');
			
					getGLDistribution($(".cash-rcpt-title").html());
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#popupNotify", "alert-danger", errList);
				
					$("#mainLoader").hide();
				}
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				console.log(jqXHR);
				console.log(textStatus);
				console.log(errorThrown);
				$("#mainLoader").hide();
			}
		});
		
	});
	
	$("#arOpenItemsTbl tbody").on("click", "tr", function(){
		$("#arOpenItemsTbl tbody tr").removeClass("selected-row");
		if( $(".add-cash-rcpt-data").attr("data-sts") == "A" ){
			$(this).addClass("selected-row");
			$("#pymntEntry").removeAttr("disabled");
		}
	});
	
	$("#pymntEntry").click(function(){
		$(".cash-rcpt-title").text( $("#cashRcptTitle").text() );
		$("#pymntEntryModal .form-control").val("");
		
		var $tr = $("#arOpenItemsTbl tbody tr.selected-row");
		
		$("#peInvc").val($tr.attr("data-inv-no") );
		/*$("#peInvc").val( $tr.attr("data-pfx") + " " + $tr.attr("data-inv-no") );*/
		$("#peBalAmt").val( $tr.attr("data-bal-amt") )
		$("#peDiscAmt").val( $tr.attr("data-disc-amt") )
		$("#peCry").val( $tr.attr("data-cry") )
		$("#peDiscDt").val( $tr.attr("data-disc-dt"))
		
		if($tr.attr("data-disc-flg") == 'N')
		{
			$("#peDiscTknAmt").attr("disabled", "disabled");
			$("#peWrtOffAmt").attr("disabled", "disabled");
		}
		else
		{
			$("#peDiscTknAmt").removeAttr("disabled");
			$("#peWrtOffAmt").removeAttr("disabled");
		}
		
		//$("#peWrtOffAmt").val( $tr.attr("data-invc-no") )
		//$("#peDiscTknAmt").val( $tr.attr("data-invc-no") )
		$("#pePmtAmt").val( $tr.attr("data-bal-amt") );
		
		$("#pymntEntryModal").modal("toggle");
	});
	
	$("#addPymntEntry").click(function(){
		$("#pymntEntryModal .form-control").removeClass("border-danger");
		
		var errFlg = false;
		var peDiscTknAmt = $("#peDiscTknAmt").val().trim();
		var peInvc = $("#peInvc").val().trim();
		var pePmtAmt = $("#pePmtAmt").val().trim();
		var peRefItmNo = "";
		var peWrtOffAmt = $("#peWrtOffAmt").val().trim();
		
		if( peInvc == "" ){
			errFlg = true;
			$("#peInvc").addClass("border-danger");
		}
		
		if( pePmtAmt == "" ){
			errFlg = true;
			$("#pePmtAmt").addClass("border-danger");
		}
		
		if( errFlg ){
			return;
		}
		
		$("#mainLoader").show();
	
		var inputData = {
			discTknAmt: peDiscTknAmt,
			invcNo: peInvc,
			pmtAmt: pePmtAmt,
			refItmNo: peRefItmNo,
			wrtOffAmt: peWrtOffAmt,
			cashRcptNo: $(".cash-rcpt-title").html(),
		};
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/rcpt/arDist',
			type: 'POST',
			dataType : 'json',
			success: function(response){
				if( response.output.authToken == undefined || response.output.authToken == "" ){
					$("#logout").trigger("click");
				}
				
				localStorage.setItem("starOcrAuthToken", response.output.authToken);
				setAjaxHeader();
					
				if( response.output.rtnSts == 0 ){
					customAlert("#popupNotify", "alert-success", "Payment entry saved successfully.");
					$("#pymntEntryModal .form-control").val("");
					
					$("#custChecksTbl tbody tr.current .view-ar-open-items").trigger("click");
							
					$("#pymntEntryModal").modal("hide");
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#popupNotify", "alert-danger", errList);
				
					$("#mainLoader").hide();
				}
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				console.log(jqXHR);
				console.log(textStatus);
				console.log(errorThrown);
				$("#mainLoader").hide();
			}
		});
		
	});
	
	
	$("#addSsnArCry1").change(function(){
		var origCry = $("#addSsnBnkCry").val();
		var eqvCry = $(this).val();
		
		if( origCry != "" && origCry != undefined && eqvCry != "" && eqvCry != undefined ){
			getExchangeRate(origCry, eqvCry, $("#addSsnCrsExchngRt1"));
		}else{
			$("#addSsnCrsExchngRt1").val("");
		}
	});
	
	$("#addSsnArCry2").change(function(){
		var origCry = $("#addSsnBnkCry").val();
		var eqvCry = $(this).val();
		
		if( origCry != "" && origCry != undefined && eqvCry != "" && eqvCry != undefined ){
			getExchangeRate(origCry, eqvCry, $("#addSsnCrsExchngRt2"));
		}else{
			$("#addSsnCrsExchngRt2").val("");
		}
	});
	
	$("#addSsnArCry3").change(function(){
		var origCry = $("#addSsnBnkCry").val();
		var eqvCry = $(this).val();
		
		if( origCry != "" && origCry != undefined && eqvCry != "" && eqvCry != undefined ){
			getExchangeRate(origCry, eqvCry, $("#addSsnCrsExchngRt3"));
		}else{
			$("#addSsnCrsExchngRt3").val("");
		}
	});
	
	
});


function getCashReceiptSessions(){
	$("#mainLoader").show();
	
	var inputData = {
		ssnId: $("#fltrSsnId").val().trim(),
		crcpNo: $("#fltrCashRcptNo").val().trim(),
		cusId: ($("#fltrCusId").val() != null ) ? $("#fltrCusId").val() : "",
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/ar/read-ssn',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblSsn.length;
				var tblRows = '', stsStr = '';	
				for( var i=0; i<loopSize; i++ ){
					stsStr = '';
					if( response.output.fldTblSsn[i].sts == "A" ){
						stsStr = '<span class="badge badge-primary">Active</span>';
					}else if( response.output.fldTblSsn[i].sts == "C" ){
						stsStr = '<span class="badge badge-danger">Close</span>';
					}
					tblRows += '<tr data-ssn-sts="'+ response.output.fldTblSsn[i].sts +'">'+
									'<td><button class="btn btn-link p-0 view-cust-checks">'+ response.output.fldTblSsn[i].ssnId +'</button></td>'+
									'<td>'+ response.output.fldTblSsn[i].cmpyId +'</td>'+
									'<td>'+ response.output.fldTblSsn[i].bnk +'</td>'+
									'<td></td>'+
									'<td>'+ response.output.fldTblSsn[i].totDepAmtStr +'</td>'+
									'<td>'+ response.output.fldTblSsn[i].totNbrChk +'</td>'+
									'<td></td>'+
									'<td>'+ response.output.fldTblSsn[i].brh +'</td>'+
									'<td>'+ response.output.fldTblSsn[i].lgnNm +'</td>'+
									'<td>'+ response.output.fldTblSsn[i].jrnlDt +'</td>'+
									'<td>'+ stsStr +'</td>'+
								'</tr>';
				}
				$("#ssnDataTbl tbody").html( tblRows );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}


function getCustChecks(ssnId){
	$("#mainLoader").show();
			
	var inputData = {
		ssnId: ssnId,
	};
	
	$("#ssnIdTitle").html(ssnId);
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData),
		url: rootAppName + '/rest/ar/read-cashRcpt',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblCashRcpt.length;
				var tblRows = '', stsStr = '';	
				for( var i=0; i<loopSize; i++ ){
					stsStr = '';
					if( response.output.fldTblCashRcpt[i].sts == "A" ){
						stsStr = '<span class="badge badge-primary">Active</span>';
					}else if( response.output.fldTblCashRcpt[i].sts == "C" ){
						stsStr = '<span class="badge badge-danger">Close</span>';
					}
					
					tblRows += '<tr data-chk-proof="'+ response.output.fldTblCashRcpt[i].chkProof +'" data-chk-amt="'+ response.output.fldTblCashRcpt[i].chkAmt +'" data-cus-id="'+ response.output.fldTblCashRcpt[i].cusId +'" data-sts="'+ response.output.fldTblCashRcpt[i].sts +'">'+
									'<td>'+ response.output.fldTblCashRcpt[i].cmpyId +'</td>'+
									'<td>'+ response.output.fldTblCashRcpt[i].cusId +'</td>'+
									'<td></td>'+
									'<td><button class="btn btn-link p-0 view-ar-open-items">'+ response.output.fldTblCashRcpt[i].crPfx + '-' + response.output.fldTblCashRcpt[i].crNo +'</button></td>'+
									'<td>'+ response.output.fldTblCashRcpt[i].chkNo +'</td>'+
									'<td>'+ response.output.fldTblCashRcpt[i].desc +'</td>'+
									'<td>'+ response.output.fldTblCashRcpt[i].chkAmtStr +'</td>'+
									'<td>'+ stsStr +'</td>'+
								'</tr>';
				}
				$("#custChecksTbl tbody").html( tblRows );
				
				$("#sessionSection").hide();
				$("#custChecksSection").slideDown();
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}


function getPayments(refNo){
	$("#mainLoader").show();
			
	var inputData = {
		refPfx: refNo.split("-")[0],
		refNo: refNo.split("-")[1],
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData),
		url: rootAppName + '/rest/ar/read-arPymnt',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblARPymnt.length;
				var tblRows = '';	
				for( var i=0; i<loopSize; i++ ){
					tblRows += '<tr>'+
									'<td>'+ response.output.fldTblARPymnt[i].arPfx +'</td>'+
									'<td>'+ response.output.fldTblARPymnt[i].updRef +'</td>'+
									'<td></td>'+
									'<td>'+ response.output.fldTblARPymnt[i].dsAmt +'</td>'+
									'<td></td>'+
									'<td></td>'+
									'<td>'+ response.output.fldTblARPymnt[i].rcvdAmt +'</td>'+
								'</tr>';
				}
				$("#arPymntTbl tbody").html( tblRows );
				
				$("#arPymntsModal").modal("toggle");
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}


function getAREnquiry(){
	$("#mainLoader").show();
	
	var inputData = {
		cusId: ($("#pymntFltrCusId").val() != null ) ? $("#pymntFltrCusId").val() : "",
		brh: ($("#pymntFltrBrh").val() != null ) ? $("#pymntFltrBrh").val() : "",
		dueDt: $("#pymntFltrDueDt").val(),
		invNo: "",
		refPfx: "",
		refNo: "",
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData),
		url: rootAppName + '/rest/ar/read-arEnqry',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblAREnqry.length;
				var tblRows = '';	
				for( var i=0; i<loopSize; i++ ){
					tblRows += '<tr>'+
									'<td>'+ response.output.fldTblAREnqry[i].cmpyId +'</td>'+
									'<td>'+ response.output.fldTblAREnqry[i].arPfx + '-' + response.output.fldTblAREnqry[i].arNo +'</td>'+
									'<td>'+ response.output.fldTblAREnqry[i].cusId + '</td>'+
									'<td>'+ response.output.fldTblAREnqry[i].brh +'</td>'+
									'<td>'+ response.output.fldTblAREnqry[i].cry +'</td>'+
									'<td>'+ response.output.fldTblAREnqry[i].balamtStr +'</td>'+
									'<td>'+ response.output.fldTblAREnqry[i].ipAmtStr +'</td>'+
									'<td>'+ response.output.fldTblAREnqry[i].origAmtStr +'</td>'+
									'<td>'+ response.output.fldTblAREnqry[i].desc +'</td>'+
									'<td>'+ response.output.fldTblAREnqry[i].dueDt +'</td>'+
									'<td>'+ response.output.fldTblAREnqry[i].invDt +'</td>'+
									'<td>'+ response.output.fldTblAREnqry[i].updRef +'</td>'+
								'</tr>';
				}
				$("#arEnqryTbl tbody").html( tblRows );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}


function getCusAROpenItems(cusId, cashRcpt){
	$("#mainLoader").show();
	
	var inputData = {
		cusId: cusId,
		brh: "",
		dueDt: "",
		invNo: "",
		refPfx: cashRcpt.split("-")[0],
		refNo: cashRcpt.split("-")[1],
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData),
		url: rootAppName + '/rest/ar/read-arEnqry',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblAREnqry.length;
				var tblRows = '', invcNo = '', cry = '', pfx = '', balAmt = '', discAmt = '', DiscDt = '', discFlg = '';	
				for( var i=0; i<loopSize; i++ ){					
					invcNo = response.output.fldTblAREnqry[i].updRef;
					cry = response.output.fldTblAREnqry[i].cry;
					pfx = response.output.fldTblAREnqry[i].arPfx;
					balAmt = response.output.fldTblAREnqry[i].balamt;
					discFlg = response.output.fldTblAREnqry[i].discFlg
					
					tblRows += '<tr data-inv-no="'+ invcNo +'" data-cry="'+ cry +'" data-pfx="'+ pfx +'" data-bal-amt="'+ balAmt +'" data-disc-amt="'+ response.output.fldTblAREnqry[i].discAmt +'" data-disc-dt="'+ response.output.fldTblAREnqry[i].discDt +'" data-disc-flg="' + discFlg +'">'+
									'<td>'+ response.output.fldTblAREnqry[i].cmpyId +'</td>'+
									'<td>'+ pfx + '-' + response.output.fldTblAREnqry[i].arNo +'</td>'+
									'<td>'+ response.output.fldTblAREnqry[i].cusId + '</td>'+
									'<td>'+ response.output.fldTblAREnqry[i].brh +'</td>'+
									'<td>'+ cry +'</td>'+
									'<td>'+ response.output.fldTblAREnqry[i].balamtStr +'</td>'+
									'<td>'+ response.output.fldTblAREnqry[i].ipAmtStr +'</td>'+
									'<td>'+ response.output.fldTblAREnqry[i].origAmtStr +'</td>'+
									'<td>'+ response.output.fldTblAREnqry[i].desc +'</td>'+
									'<td>'+ response.output.fldTblAREnqry[i].dueDt +'</td>'+
									'<td>'+ response.output.fldTblAREnqry[i].invDt +'</td>'+
									'<td>'+ response.output.fldTblAREnqry[i].discDt +'</td>'+
									'<td>'+ invcNo +'</td>'+
								'</tr>';
				}
				$("#arOpenItemsTbl tbody").html( tblRows );
				$("#pymntEntry").attr("disabled", "disabled");
				$(".chk-proof").html(response.output.chkProof);
				
				$("#custChecksSection").hide();
				$("#arPymntSection").slideDown();
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}


function addSession(){
	$("#addSsnModal .form-control, #addSsnModal .select2-selection").removeClass("border-danger");

	var errFlg = false;
	var brh = $("#addSsnBrh").val();
	var jrnlDt = $("#addSsnJrnlDt").val();
	var bnk = $("#addSsnBnk").val();
	var dpstExchngRt = $("#addSsnBnkExchngRt").val();
	var dpstDt = $("#addSsnDpstDt").val();
	var cry1 = $("#addSsnArCry1").val();
	var dpstExchngRt1 = $("#addSsnCrsExchngRt1").val();
	var cry2 = $("#addSsnArCry2").val();
	var dpstExchngRt2 = $("#addSsnCrsExchngRt2").val();
	var cry3 = $("#addSsnArCry3").val();
	var dpstExchngRt3 = $("#addSsnCrsExchngRt3").val();
	var dpstAmt = $("#addSsnDpstAmt").val();
	var nbrChk = $("#addSsnNbrChk").val();
	
	if( brh == "" ){
		errFlg = true;
		$("#addSsnBrh").next().find(".select2-selection").addClass("border-danger");
	}
	
	if( jrnlDt == "" ){
		errFlg = true;
		$("#addSsnJrnlDt").addClass("border-danger");
	}
	
	if( bnk == "" ){
		errFlg = true;
		$("#addSsnBnk").next().find(".select2-selection").addClass("border-danger");
	}
	
	if( dpstExchngRt == "" ){
		errFlg = true;
		$("#addSsnBnkExchngRt").addClass("border-danger");
	}
	
	if( dpstDt == "" ){
		errFlg = true;
		$("#addSsnDpstDt").addClass("border-danger");
	}
	
	if( cry1 == "" && cry2 == "" && cry3 == "" ){
		errFlg = true;
		$("#addSsnArCry1").next().find(".select2-selection").addClass("border-danger");
	}
	
	if( dpstExchngRt1 == "" && dpstExchngRt1 == "" && dpstExchngRt1 == "" ){
		errFlg = true;
		$("#addSsnCrsExchngRt1").addClass("border-danger");
	}
	
	if( errFlg == true ){
		return;
	}
	
	$("#mainLoader").show();
	
	var inputData = {
		ssnId:"",
		brh: brh,
		jrnlDt: "", //jrnlDt
		bnk: bnk,
		dpstExchngRt: dpstExchngRt,
		dpstDt: "", //dpstDt
		cry1: cry1,
		dpstExchngRt1: dpstExchngRt1,
		cry2: cry2,
		dpstExchngRt2: dpstExchngRt2,
		cry3: cry3,
		dpstExchngRt3: dpstExchngRt3,
		dpstAmt: dpstAmt,
		nbrChk: nbrChk,
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/rcpt/crtssn',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.authToken == undefined || response.output.authToken == "" ){
				$("#logout").trigger("click");
			}
			
			localStorage.setItem("starOcrAuthToken", response.output.authToken);
			setAjaxHeader();
				
			if( response.output.rtnSts == 0 ){
				$("#addSsnModal").modal("hide");
				
				$("#crSsnId").val( response.output.ssnId );
				$("#modalSsnId").text( response.output.ssnId );
				$("#addCashRcptModal").modal("show");
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#popupNotify", "alert-danger", errList);
			}
			
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}


function addCashReceipt(){
	$("#addCashRcptModal .form-control, #addCashRcptModal .select2-selection").removeClass("border-danger border-warning");

	var errFlg = false;
	var invNo = $("#crInvcNo").val();
	var cusId = $("#crCusId").val();
	var cusChkAmt = $("#crCusChkAmt").val();
	var chkNo = $("#crChkNo").val();
	var desc = $("#crDesc").val();
	var ssnId = $("#crSsnId").val();
	
	if( cusId == "" ){
		errFlg = true;
		$("#crCusId").next().find(".select2-selection").addClass("border-danger");
	}
	
	if( errFlg == true ){
		return;
	}
	
	$("#mainLoader").show();
			
	var inputData = {
		ssnId: ssnId,
		invNo: invNo,
		cusId: cusId,
		cusChkAmt: cusChkAmt,
		chkNo: chkNo,
		desc: desc,
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/rcpt/crtRcpt',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.authToken == undefined || response.output.authToken == "" ){
				$("#logout").trigger("click");
			}
			
			localStorage.setItem("starOcrAuthToken", response.output.authToken);
			setAjaxHeader();
				
			if( response.output.rtnSts == 0 ){
				$("#addCashRcptModal").modal("hide");
				
				var ssnId = $("#ssnIdTitle").text();
		
				getCustChecks(ssnId);
				
				customAlert("#mainNotify", "alert-success", "Cash receipt created successfully.");
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#popupNotify", "alert-danger", errList);
			}
			
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}


function getAdvancePayment(refNo){
	$("#mainLoader").show();
			
	var inputData = {
		refPfx: refNo.split("-")[0],
		refNo: refNo.split("-")[1],
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData),
		url: rootAppName + '/rest/ar/read-advPmt',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblAdvPmt.length;
				var tblRows = '';
				if( loopSize == 0 ){
					tblRows = '<tr><td colspan="3">No record found.</td></tr>';
				}
				for( var i=0; i<loopSize; i++ ){
					tblRows += '<tr>'+
									'<td>'+ response.output.fldTblAdvPmt[i].desc +'</td>'+
									'<td>'+ response.output.fldTblAdvPmt[i].advAmt +'</td>'+
									'<td>'+ response.output.fldTblAdvPmt[i].cry +'</td>'+
								'</tr>';
				}
				$("#advncPymntTbl tbody").html( tblRows );
				$(".chk-proof").html(response.output.chkProof);
				
				$("#advncPymntModal").modal("show");
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}

function getUnappliedDebit(refNo){
	$("#mainLoader").show();
			
	var inputData = {
		refPfx: refNo.split("-")[0],
		refNo: refNo.split("-")[1],
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData),
		url: rootAppName + '/rest/ar/read-uaDebit',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblUnappliedDr.length;
				var tblRows = '';
				if( loopSize == 0 ){
					tblRows = '<tr><td colspan="3">No record found.</td></tr>';
				}
				for( var i=0; i<loopSize; i++ ){
					tblRows += '<tr>'+
									'<td>'+ response.output.fldTblUnappliedDr[i].desc +'</td>'+
									'<td>'+ response.output.fldTblUnappliedDr[i].drAmt +'</td>'+
									'<td>'+ response.output.fldTblUnappliedDr[i].cry +'</td>'+
								'</tr>';
				}
				$("#unapldDbtTbl tbody").html( tblRows );
				$(".chk-proof").html(response.output.chkProof);
				
				$("#unapldDbtModal").modal("show");
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}


function getGLDistribution(refNo){
	$("#mainLoader").show();
			
	var inputData = {
		refPfx: refNo.split("-")[0],
		refNo: refNo.split("-")[1],
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData),
		url: rootAppName + '/rest/ar/read-glDist',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblGLDist.length;
				var tblRows = '';
				if( loopSize == 0 ){
					tblRows = '<tr><td colspan="5">No record found.</td></tr>';
				}
				for( var i=0; i<loopSize; i++ ){
					tblRows += '<tr>'+
									'<td>'+ response.output.fldTblGLDist[i].glAcct +'</td>'+
									'<td>'+ response.output.fldTblGLDist[i].desc +'</td>'+
									'<td>'+ response.output.fldTblGLDist[i].sacct +'</td>'+
									'<td></td>'+
									'<td>'+ response.output.fldTblGLDist[i].drAmt +'</td>'+
									'<td>'+ response.output.fldTblGLDist[i].crAmt +'</td>'+
								'</tr>';
				}
				$("#glDistTbl tbody").html( tblRows );
				$(".chk-proof").html(response.output.chkProof);
				
				$("#glDistModal").modal("show");
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}