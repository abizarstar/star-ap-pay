$(function(){

	getBranchList( $("#arAgingFltrBrh, #arPymntRcvdDtFltrBrh") );
	getCustomerList( $("#arAgingFltrCusId, #arPymntRcvdDtFltrCusId") );
	
	$(".report-tabs .card-body").click(function(){
		$(".report-tabs .card-body").removeClass("active");
		$(this).addClass("active");
		
		$(".report-section").hide();
		$("#reportSection").slideDown();
		$("#"+ $(this).attr("data-report-type")).slideDown();
	});
	
	$("#fltrArAgingRpt").click(function(){
		getArAgingReport();
	});
	
	$("#loadMoreArAgingRpt").click(function(){
		var pageNo = $(this).attr("data-page-no");
		getArAgingReport(pageNo);
	});
	
	$("#resetArAgingRpt").click(function(){
		$("#arAgingRptSection .form-control").val("").trigger("change");
	});
	
	$("#arAgingFltrCusId, #arAgingFltrBrh").change(function(){
		$("#loadMoreArAgingRpt").attr("disabled", "disabled");
		$("#loadMoreArAgingRpt").addClass("disabled");
	});
	
	$("#arAgingFltrAgng, #arAgingFltrInvDt").focusout(function(){
		$("#loadMoreArAgingRpt").attr("disabled", "disabled");
		$("#loadMoreArAgingRpt").addClass("disabled");
	});
	
	
	$("#fltrArPymntRcvdDtRpt").click(function(){
		getArPaymentRcvdByDateReport();
	});
	
	$("#loadMoreArPymntRcvdDtRpt").click(function(){
		var pageNo = $(this).attr("data-page-no");
		getArPaymentRcvdByDateReport(pageNo);
	});
	
	$("#resetArPymntRcvdDtRpt").click(function(){
		$("#arPymntRcvdDtRptSection .form-control").val("").trigger("change");
	});
	
	$("#arPymntRcvdDtFltrCusId, #arPymntRcvdDtFltrBrh").change(function(){
		$("#loadMoreArPymntRcvdDtRpt").attr("disabled", "disabled");
		$("#loadMoreArPymntRcvdDtRpt").addClass("disabled");
	});
	
	$("#arPymntRcvdDtFltrAgng, #arPymntRcvdDtFltrInvDt").focusout(function(){
		$("#loadMoreArPymntRcvdDtRpt").attr("disabled", "disabled");
		$("#loadMoreArPymntRcvdDtRpt").addClass("disabled");
	});
	
	
});


function getArAgingReport(pageNo=0){
	$("#mainLoader").show();
	
	var invcStrtDate = "";
	var invcEndDate = "";
	var cusId = $("#arAgingFltrCusId").val();
	var agng = $("#arAgingFltrAgng").val();
	var brh = $("#arAgingFltrBrh").val();
	var invcDt = $("#arAgingFltrInvDt").val();
	
	invcDt = (invcDt == null || invcDt == undefined ) ? "" : invcDt;
	
	if( invcDt != "" ){
		invcStrtDate = invcDt.split(" - ")[0];
		invcEndDate = invcDt.split(" - ")[1];
	}
	
	var inputData = {
		cusId: (cusId == null || cusId == undefined) ? "" : cusId,
		agng: agng,
		brh: (brh == null || brh == undefined) ? "" : brh,
		invcStrtDt: invcStrtDate,
		invcEndDt: invcEndDate,
		pageNo: pageNo,
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/ar/read-aragng',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblARdsbrd.length;
				var tblRows = '';	
				for( var i=0; i<loopSize; i++ ){
					tblRows += '<tr>'+ 
									'<td>'+ response.output.fldTblARdsbrd[i].arPfx + '-' + response.output.fldTblARdsbrd[i].arNo +'</td>'+
									'<td>'+ response.output.fldTblARdsbrd[i].brh +'</td>'+
									'<td>('+ response.output.fldTblARdsbrd[i].cusId +') '+ response.output.fldTblARdsbrd[i].cusNm +'</td>'+
									'<td>'+ response.output.fldTblARdsbrd[i].aging +'</td>'+
									'<td>'+ response.output.fldTblARdsbrd[i].desc30 +'</td>'+
									'<td>'+ response.output.fldTblARdsbrd[i].updRef +'</td>'+
									'<td>'+ response.output.fldTblARdsbrd[i].balamt +'</td>'+
									'<td>'+ response.output.fldTblARdsbrd[i].ipAmt +'</td>'+
									'<td>'+ response.output.fldTblARdsbrd[i].origAmt +'</td>'+
									'<td>'+ response.output.fldTblARdsbrd[i].dueDt +'</td>'+
									'<td>'+ response.output.fldTblARdsbrd[i].invDt +'</td>'+
								'</tr>';
				}
				
				if( inputData.pageNo == 0 ){
					$("#arAgingRptTbl tbody").html( tblRows );
				}else{
					$("#arAgingRptTbl tbody").append( tblRows );
				}
				
				if( response.output.eof ){
					$("#loadMoreArAgingRpt").attr("disabled", "disabled");
					$("#loadMoreArAgingRpt").addClass("disabled");
				}else{
					$("#loadMoreArAgingRpt").removeAttr("disabled");
					$("#loadMoreArAgingRpt").removeClass("disabled");
				}
				
				$("#loadMoreArAgingRpt").attr("data-page-no", parseInt(inputData.pageNo)+1);
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}


function getArPaymentRcvdByDateReport(pageNo=0){
	$("#mainLoader").show();
	
	var invcStrtDate = "";
	var invcEndDate = "";
	var cusId = $("#arPymntRcvdDtFltrCusId").val();
	var agng = $("#arPymntRcvdDtFltrAgng").val();
	var brh = $("#arPymntRcvdDtFltrBrh").val();
	var invcDt = $("#arPymntRcvdDtFltrInvDt").val();
	
	invcDt = (invcDt == null || invcDt == undefined ) ? "" : invcDt;
	
	if( invcDt != "" ){
		invcStrtDate = invcDt.split(" - ")[0];
		invcEndDate = invcDt.split(" - ")[1];
	}
	
	var inputData = {
		cusId: (cusId == null || cusId == undefined) ? "" : cusId,
		agng: agng,
		brh: (brh == null || brh == undefined) ? "" : brh,
		strtdt: invcStrtDate,
		enddt: invcEndDate,
		pageNo: pageNo,
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/ar/read-ar-rcv',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblARPmtRecvdata.length;
				var tblRows = '';	
				for( var i=0; i<loopSize; i++ ){
					tblRows += '<tr>'+ 
									'<td>'+ response.output.fldTblARPmtRecvdata[i].arPfx +'</td>'+
									'<td>'+ response.output.fldTblARPmtRecvdata[i].crcpBrh +'</td>'+
									'<td>('+ response.output.fldTblARPmtRecvdata[i].cusId +') '+ response.output.fldTblARPmtRecvdata[i].cusNm +'</td>'+
									'<td>'+ response.output.fldTblARPmtRecvdata[i].crcpNo +'</td>'+
									'<td>'+ response.output.fldTblARPmtRecvdata[i].chkNo +'</td>'+
									'<td>'+ response.output.fldTblARPmtRecvdata[i].chkAmt + ' ' + response.output.fldTblARPmtRecvdata[i].depCry +'</td>'+
									'<td>'+ response.output.fldTblARPmtRecvdata[i].desc30 +'</td>'+
									'<td>'+ response.output.fldTblARPmtRecvdata[i].depExrt +'</td>'+
									'<td>'+ response.output.fldTblARPmtRecvdata[i].depDt +'</td>'+
									'<td>'+ response.output.fldTblARPmtRecvdata[i].jrnlDt +'</td>'+
									'<td>'+ response.output.fldTblARPmtRecvdata[i].updtDt +'</td>'+
									'<td>'+ response.output.fldTblARPmtRecvdata[i].lgnId +'</td>'+
								'</tr>';
				}
				
				if( inputData.pageNo == 0 ){
					$("#arPymntRcvdDtRptTbl tbody").html( tblRows );
				}else{
					$("#arPymntRcvdDtRptTbl tbody").append( tblRows );
				}
				
				if( response.output.eof ){
					$("#loadMoreArPymntRcvdDtRpt").attr("disabled", "disabled");
					$("#loadMoreArPymntRcvdDtRpt").addClass("disabled");
				}else{
					$("#loadMoreArPymntRcvdDtRpt").removeAttr("disabled");
					$("#loadMoreArPymntRcvdDtRpt").removeClass("disabled");
				}
				
				$("#loadMoreArPymntRcvdDtRpt").attr("data-page-no", parseInt(inputData.pageNo)+1);
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}