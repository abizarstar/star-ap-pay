$(function(){
	
	getCustomerList( $("#crFltrCus") );
	
	$(".report-tabs .card-body").click(function(){
		$(".report-tabs .card-body").removeClass("active");
		$(this).addClass("active");
		
		$(".report-section").hide();
		$("#reportSection").slideDown();
		$("#"+ $(this).attr("data-report-type")).slideDown();
	});
	
	$("#fltrLockBoxRpt").click(function(){
		getLockBoxReport();
	});
	
	$("#loadMoreLockBoxRpt").click(function(){
		var pageNo = $(this).attr("data-page-no");
		getLockBoxReport(pageNo);
	});
	
	$("#resetLockBoxRpt").click(function(){
		$("#lockBoxRptSection .form-control").val("").trigger("change");
	});
		
	$("#crFltrDpstDt").focusout(function(){
		$("#loadMoreLockBoxRpt").attr("disabled", "disabled");
		$("#loadMoreLockBoxRpt").addClass("disabled");
	});
	
	$("#exportLockBox").click(function(){
		var frmDpstDt = "";
		var toDpstDt = "";
		var dpctDt = $("#crFltrDpstDt").val();
		var cusId = $("#crFltrCus").val();
		var sts = $("#crFltrSts").val();
		
		dpctDt = (dpctDt == null || dpctDt == undefined ) ? "" : dpctDt;
		cusId = (cusId == null || cusId == undefined ) ? "" : cusId;
		sts = (sts == null || sts == undefined ) ? "" : sts;
		
		if( dpctDt != "" ){
			frmDpstDt = dpctDt.split(" - ")[0];
			toDpstDt = dpctDt.split(" - ")[1];
		}
		
		var docUrl = rootAppName + "/rest/export/lck-rpt?cmpyId="+ glblCmpyId +"&cusId=" + cusId + "&sts=" + sts + "&frmDpstDt=" + frmDpstDt + "&toDpstDt=" + toDpstDt;

		window.open(docUrl, '_blank');
	});
});


function getLockBoxReport(pageNo=0){
	$("#mainLoader").show();
	
	var frmDpstDt = "";
	var toDpstDt = "";
	var dpctDt = $("#crFltrDpstDt").val();
	var cusId = $("#crFltrCus").val();
	var sts = $("#crFltrSts").val();
	
	dpctDt = (dpctDt == null || dpctDt == undefined ) ? "" : dpctDt;
	cusId = (cusId == null || cusId == undefined ) ? "" : cusId;
	sts = (sts == null || sts == undefined ) ? "" : sts;
	
	if( dpctDt != "" ){
		frmDpstDt = dpctDt.split(" - ")[0];
		toDpstDt = dpctDt.split(" - ")[1];
	}
	
	var inputData = {
		frmDpstDt: frmDpstDt,
		toDpstDt: toDpstDt,
		cus:cusId,
		sts:sts,
		pageNo: pageNo,
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/report/read-lckbx-rpt',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblLockBox.length;
				var tblRows = '';
				var rcptStr = '', cusStr = '', ststIcon = '';	
				for( var i=0; i<loopSize; i++ ){
					
					if( response.output.fldTblLockBox[i].ssnId == "" ){
						rcptStr = "";
					}
					else
					{
						rcptStr = response.output.fldTblLockBox[i].cashRcpt + ' (' + response.output.fldTblLockBox[i].ssnId + ')';
					}
					
					if( response.output.fldTblLockBox[i].cusId == "" ){
						cusStr = "";
					}
					else
					{
						cusStr = '(' + response.output.fldTblLockBox[i].cusId + ') ' + response.output.fldTblLockBox[i].cusNm ;
					}
					
					if(response.output.fldTblLockBox[i].trnSts == 'C')
					{
						ststIcon = '<span class="badge badge-success">Close</span>';
					}
					else if(response.output.fldTblLockBox[i].trnSts == 'A')
					{
						ststIcon = '<span class="badge badge-primary">Pending</span>';
					}
					else 
					{
						ststIcon = '<span class="badge badge-danger">Open</span>';
					}
				
					tblRows += '<tr>'+
									'<td><strong>'+ cusStr +'</strong></td>'+ 
									'<td>'+ response.output.fldTblLockBox[i].chkNo +'</td>'+
									'<td>'+ response.output.fldTblLockBox[i].invNo +'</td>'+
									'<td class="text-right">'+ response.output.fldTblLockBox[i].amountStr + '</td>'+
									'<td>'+ response.output.fldTblLockBox[i].dpstDt +'</td>'+
									'<td><strong>'+ rcptStr +'</strong></td>'+
									'<td>'+ response.output.fldTblLockBox[i].jeRef +'</td>'+
									'<td>'+ response.output.fldTblLockBox[i].actvyDt +'</td>'+
									'<td>'+ response.output.fldTblLockBox[i].lgnUsr +'</td>'+
									'<td>'+ response.output.fldTblLockBox[i].frmAcctngPr +'</td>'+
									'<td>'+ response.output.fldTblLockBox[i].toAcctngPr +'</td>'+
									'<td>'+ response.output.fldTblLockBox[i].extDesc +'</td>'+
									'<td>'+ response.output.fldTblLockBox[i].extDesc1 +'</td>'+
									'<td><strong>'+ ststIcon +'</strong></td>'+
								'</tr>';
				}
				
				if( inputData.pageNo == 0 ){
					$("#lockBoxRptTbl tbody").html( tblRows );
				}else{
					$("#lockBoxRptTbl tbody").append( tblRows );
				}
				
				if( response.output.eof ){
					$("#loadMoreLockBoxRpt").attr("disabled", "disabled");
					$("#loadMoreLockBoxRpt").addClass("disabled");
				}else{
					$("#loadMoreLockBoxRpt").removeAttr("disabled");
					$("#loadMoreLockBoxRpt").removeClass("disabled");
				}
				
				if( loopSize == 0 ){
					$("#exportLockBox").addClass("disabled");
					$("#exportLockBox").attr("disabled", "disabled");
				}else{
					$("#exportLockBox").removeClass("disabled");
					$("#exportLockBox").removeAttr("disabled");
				}
				
				var $table = $('#lockBoxRptTbl');
					
				$table.floatThead({
					useAbsolutePositioning: true,
					scrollContainer: function($table) {
						return $table.closest('.table-responsive');
					}
				});
				$table.floatThead('reflow');
				
				$("#loadMoreLockBoxRpt").attr("data-page-no", parseInt(inputData.pageNo)+1);
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}


function getArPaymentRcvdByDateReport(pageNo=0){
	$("#mainLoader").show();
	
	var invcStrtDate = "";
	var invcEndDate = "";
	var cusId = $("#arPymntRcvdDtFltrCusId").val();
	var agng = $("#arPymntRcvdDtFltrAgng").val();
	var brh = $("#arPymntRcvdDtFltrBrh").val();
	var invcDt = $("#arPymntRcvdDtFltrInvDt").val();
	
	invcDt = (invcDt == null || invcDt == undefined ) ? "" : invcDt;
	
	if( invcDt != "" ){
		invcStrtDate = invcDt.split(" - ")[0];
		invcEndDate = invcDt.split(" - ")[1];
	}
	
	var inputData = {
		cusId: (cusId == null || cusId == undefined) ? "" : cusId,
		agng: agng,
		brh: (brh == null || brh == undefined) ? "" : brh,
		strtdt: invcStrtDate,
		enddt: invcEndDate,
		pageNo: pageNo,
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/ar/read-ar-rcv',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblARPmtRecvdata.length;
				var tblRows = '';	
				for( var i=0; i<loopSize; i++ ){
					tblRows += '<tr>'+ 
									'<td>'+ response.output.fldTblARPmtRecvdata[i].arPfx +'</td>'+
									'<td>'+ response.output.fldTblARPmtRecvdata[i].crcpBrh +'</td>'+
									'<td>('+ response.output.fldTblARPmtRecvdata[i].cusId +') '+ response.output.fldTblARPmtRecvdata[i].cusNm +'</td>'+
									'<td>'+ response.output.fldTblARPmtRecvdata[i].crcpNo +'</td>'+
									'<td>'+ response.output.fldTblARPmtRecvdata[i].chkNo +'</td>'+
									'<td>'+ response.output.fldTblARPmtRecvdata[i].chkAmt + ' ' + response.output.fldTblARPmtRecvdata[i].depCry +'</td>'+
									'<td>'+ response.output.fldTblARPmtRecvdata[i].desc30 +'</td>'+
									'<td>'+ response.output.fldTblARPmtRecvdata[i].depExrt +'</td>'+
									'<td>'+ response.output.fldTblARPmtRecvdata[i].depDt +'</td>'+
									'<td>'+ response.output.fldTblARPmtRecvdata[i].jrnlDt +'</td>'+
									'<td>'+ response.output.fldTblARPmtRecvdata[i].updtDt +'</td>'+
									'<td>'+ response.output.fldTblARPmtRecvdata[i].lgnId +'</td>'+
								'</tr>';
				}
				
				if( inputData.pageNo == 0 ){
					$("#arPymntRcvdDtRptTbl tbody").html( tblRows );
				}else{
					$("#arPymntRcvdDtRptTbl tbody").append( tblRows );
				}
				
				if( response.output.eof ){
					$("#loadMoreArPymntRcvdDtRpt").attr("disabled", "disabled");
					$("#loadMoreArPymntRcvdDtRpt").addClass("disabled");
				}else{
					$("#loadMoreArPymntRcvdDtRpt").removeAttr("disabled");
					$("#loadMoreArPymntRcvdDtRpt").removeClass("disabled");
				}
				
				$("#loadMoreArPymntRcvdDtRpt").attr("data-page-no", parseInt(inputData.pageNo)+1);
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}