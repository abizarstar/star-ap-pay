$(function(){

	getGlList( $("#crAcct1,#crAcct2,#crAcct3,#drAcct1,#drAcct2,#drAcct3") );
	
	getGlMapping();
	
	$("#crAcct1").change(function(){
		var glAcct = $(this).val();
		
		if( glAcct != "" && glAcct != null && glAcct != undefined ){
			getGlSubAccList( glAcct, $("#crSubAccount1") );
		}else{
			$("#crSubAccount1").val("").trigger("change");
			$("#crSubAccount1").attr("disabled", "disabled");
		}
	});
	
	$("#crAcct2").change(function(){
		var glAcct = $(this).val();
		
		if( glAcct != "" && glAcct != null && glAcct != undefined ){
			getGlSubAccList( glAcct, $("#crSubAccount2") );
		}else{
			$("#crSubAccount2").val("").trigger("change");
			$("#crSubAccount2").attr("disabled", "disabled");
		}
	});
	
	$("#crAcct3").change(function(){
		var glAcct = $(this).val();
		
		if( glAcct != "" && glAcct != null && glAcct != undefined ){
			getGlSubAccList( glAcct, $("#crSubAccount3") );
		}else{
			$("#crSubAccount3").val("").trigger("change");
			$("#crSubAccount3").attr("disabled", "disabled");
		}
	});
	
	$("#drAcct3").change(function(){
		var glAcct = $(this).val();
		
		if( glAcct != "" && glAcct != null && glAcct != undefined ){
			getGlSubAccList( glAcct, $("#drSubAccount3") );
		}else{
			$("#drSubAccount3").val("").trigger("change");
			$("#drSubAccount3").attr("disabled", "disabled");
		}
	});
	
	$("#drAcct2").change(function(){
		var glAcct = $(this).val();
		
		if( glAcct != "" && glAcct != null && glAcct != undefined ){
			getGlSubAccList( glAcct, $("#drSubAccount2") );
		}else{
			$("#drSubAccount2").val("").trigger("change");
			$("#drSubAccount2").attr("disabled", "disabled");
		}
	});
	
	$("#drAcct1").change(function(){
		var glAcct = $(this).val();
		
		if( glAcct != "" && glAcct != null && glAcct != undefined ){
			getGlSubAccList( glAcct, $("#drSubAccount1") );
		}else{
			$("#drSubAccount1").val("").trigger("change");
			$("#drSubAccount1").attr("disabled", "disabled");
		}
	});
	
	
	$("#saveGLMap").click(function(){
		$("#glMapFrm .select2-selection").removeClass("border-danger");
		
		var errFlg = false;
		var crAcct1 = $("#crAcct1").val();
		var crSubAcct1 = $("#crSubAccount1").val();
		var crAcct2 = $("#crAcct2").val();
		var crSubAcct2 = $("#crSubAccount2").val();
		var crAcct3 = $("#crAcct3").val();
		var crSubAcct3 = $("#crSubAccount3").val();
		var drAcct1 = $("#drAcct1").val();
		var drSubAcct1 = $("#drSubAccount1").val();
		var drAcct2 = $("#drAcct2").val();
		var drSubAcct2 = $("#drSubAccount2").val();
		var drAcct3 = $("#drAcct3").val();
		var drSubAcct3 = $("#drSubAccount3").val();
		
		if( crAcct1 == "" && crAcct2 == "" && crAcct3 == ""){
			errFlg = true;
			$("#crAcct1").next().find(".select2-selection").addClass("border-danger");
		}
		
		if( drAcct1 == "" && drAcct2 == "" && drAcct3 == ""){
			errFlg = true;
			$("#drAcct1").next().find(".select2-selection").addClass("border-danger");
		}
		
		if( $("#crSubAccount1 option").length > 1 ){
			if( crSubAcct1 == "" ){
				errFlg = true;
				$("#crSubAccount1").next().find(".select2-selection").addClass("border-danger");
			}
		}
		
		if( $("#crSubAccount2 option").length > 1 ){
			if( crSubAcct2 == "" ){
				errFlg = true;
				$("#crSubAccount2").next().find(".select2-selection").addClass("border-danger");
			}
		}
		
		if( $("#crSubAccount3 option").length > 1 ){
			if( crSubAcct3 == "" ){
				errFlg = true;
				$("#crSubAccount3").next().find(".select2-selection").addClass("border-danger");
			}
		}
		
		if( $("#drSubAccount1 option").length > 1 ){
			if( drSubAcct1 == "" ){
				errFlg = true;
				$("#drSubAccount1").next().find(".select2-selection").addClass("border-danger");
			}
		}
		
		if( $("#drSubAccount2 option").length > 1 ){
			if( drSubAcct2 == "" ){
				errFlg = true;
				$("#drSubAccount2").next().find(".select2-selection").addClass("border-danger");
			}
		}
		
		if( $("#drSubAccount3 option").length > 1 ){
			if( drSubAcct3 == "" ){
				errFlg = true;
				$("#drSubAccount3").next().find(".select2-selection").addClass("border-danger");
			}
		}
		
		if( !errFlg ){
			$("#mainLoader").show();
	
			var inputData = {
				crAcct1: crAcct1,
				crAcct1Desc: $("#crAcct1 option:selected").attr("data-acct-desc"),
				crSubAcct1: crSubAcct1,
				crAcct2: crAcct2,
				crAcct2Desc: $("#crAcct2 option:selected").attr("data-acct-desc"),
				crSubAcct2: crSubAcct2,
				crAcct3: crAcct3,
				crAcct3Desc: $("#crAcct3 option:selected").attr("data-acct-desc"),
				crSubAcct3: crSubAcct3,
				drAcct1: drAcct1,
				drAcct1Desc: $("#drAcct1 option:selected").attr("data-acct-desc"),
				drSubAcct1: drSubAcct1,
				drAcct2: drAcct2,
				drAcct2Desc: $("#drAcct2 option:selected").attr("data-acct-desc"),
				drSubAcct2: drSubAcct2,
				drAcct3: drAcct3,
				drAcct3Desc: $("#drAcct3 option:selected").attr("data-acct-desc"),
				drSubAcct3: drSubAcct3,
			};
			
			$.ajax({
				headers: ajaxHeader,
				url: rootAppName + '/rest/gl/add-acct-post',
				type: 'POST',
				data : JSON.stringify( inputData ),
				dataType: 'json',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						customAlert("#mainNotify", "alert-success", "GL Mapping saved succcessfully.");
						$("#clearGLMap").trigger("click");
						getGlMapping();
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
						$("#mainLoader").hide();
					}			
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					$("#mainLoader").hide();
				}
			});
		}
	});
	
	$("#clearGLMap").click(function(){
		$("#glMapFrm .select2").val("").trigger("change");
	});
	
	$("#glMapTbl").on("click", ".delete-mapping", function(){
		var postId = $(this).closest("tr").attr("data-pos-id");
		$("#delPostId").val(postId);
		
		$("#deleteMapModal").modal("show");
	});
	
	$("#deleteMap").click(function(){
		$("#mainLoader").show();
	
		var postId = $("#delPostId").val();
		
		var inputData = {
			postId: postId,
		};
		
		$.ajax({
			headers: ajaxHeader,
			url: rootAppName + '/rest/gl/del-acct-post',
			type: 'POST',
			data : JSON.stringify( inputData ),
			dataType: 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					getGlMapping();
					
					$("#deleteMapModal").modal("hide");
					
					customAlert("#mainNotify", "alert-success", "Posting (" + postId + ") deleted successfully.");
					
					$("#mainLoader").hide();
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#popupNotify", "alert-danger", errList);
					$("#mainLoader").hide();
				}			
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});
	});
	
});


function getGlMapping(){
	$("#mainLoader").show();
	
	/*var inputData = {
		ebsId: ebsId,
		typCd: typCd
	};*/
	
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/gl/get-acct-post',
		type: 'POST',
		//data : JSON.stringify( inputData ),
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblGLPosting.length;
				var tblRows = '', posId = '', crAcct = '', crAcctDesc = '', crSubAcct = '';
				var drAcct = '', drAcctDesc = '', drSubAcct = '';
				for( var i=0; i<loopSize; i++ ){
					
					posId = response.output.fldTblGLPosting[i].postId.trim();
					crAcct1 = response.output.fldTblGLPosting[i].crAcct1.trim();
					crAcctDesc1 = response.output.fldTblGLPosting[i].crAcct1Desc.trim();
					crSubAcct1 = response.output.fldTblGLPosting[i].crSubAcct1.trim();
					
					crAcct2 = response.output.fldTblGLPosting[i].crAcct2.trim();
					crAcctDesc2 = response.output.fldTblGLPosting[i].crAcct2Desc.trim();
					crSubAcct2 = response.output.fldTblGLPosting[i].crSubAcct2.trim();
					
					crAcct3 = response.output.fldTblGLPosting[i].crAcct3.trim();
					crAcctDesc3 = response.output.fldTblGLPosting[i].crAcct3Desc.trim();
					crSubAcct3 = response.output.fldTblGLPosting[i].crSubAcct3.trim();
					
					drAcct1 = response.output.fldTblGLPosting[i].drAcct1.trim();
					drAcctDesc1 = response.output.fldTblGLPosting[i].drAcct1Desc.trim();
					drSubAcct1 = response.output.fldTblGLPosting[i].drSubAcct1.trim();
					
					drAcct2 = response.output.fldTblGLPosting[i].drAcct2.trim();
					drAcctDesc2 = response.output.fldTblGLPosting[i].drAcct2Desc.trim();
					drSubAcct2 = response.output.fldTblGLPosting[i].drSubAcct2.trim();
					
					drAcct3 = response.output.fldTblGLPosting[i].drAcct3.trim();
					drAcctDesc3 = response.output.fldTblGLPosting[i].drAcct3Desc.trim();
					drSubAcct3 = response.output.fldTblGLPosting[i].drSubAcct3.trim();
					
					if(crAcct1.length > 0)
					{
						crAcct1 = '('+ crAcct1 +') '+ crAcctDesc1;
					}
					
					if(crAcct2.length > 0)
					{
						crAcct2 = '('+ crAcct2 +') '+ crAcctDesc2;
					}
					
					if(crAcct3.length > 0)
					{
						crAcct3 = '('+ crAcct3 +') '+ crAcctDesc3;
					}
					
					if(drAcct1.length > 0)
					{
						drAcct1 = '('+ drAcct1 +') '+ drAcctDesc1;
					}
					
					if(drAcct2.length > 0)
					{
						drAcct2 = '('+ drAcct2 +') '+ drAcctDesc2;
					}
					
					if(drAcct3.length > 0)
					{
						drAcct3 = '('+ drAcct3 +') '+ drAcctDesc3;
					}
					
					tblRows += '<tr data-pos-id="' + posId + '">'+
									'<td>'+ posId +'</td>'+
									'<td>'+ crAcct1  +'</td>'+
									'<td>'+ crSubAcct1 +'</td>'+
									'<td>'+ crAcct2  +'</td>'+
									'<td>'+ crSubAcct2 +'</td>'+
									'<td>'+ crAcct3  +'</td>'+
									'<td>'+ crSubAcct3 +'</td>'+
									'<td>' + drAcct1 +'</td>'+
									'<td>'+ drSubAcct1 +'</td>'+
									'<td>' + drAcct2 +'</td>'+
									'<td>'+ drSubAcct2 +'</td>'+
									'<td>' + drAcct3 +'</td>'+
									'<td>'+ drSubAcct3 +'</td>'+
									'<td><i class="icon-trash menu-icon icon-pointer delete-mapping icon-red font-weight-bold" title="Delete"></i></td>'+
								'</tr>';
				}
				$("#glMapTbl tbody").html( tblRows );
				
				$("#mainLoader").hide();
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
				$("#mainLoader").hide();
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}