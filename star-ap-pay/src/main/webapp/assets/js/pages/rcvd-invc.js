var vndrChngCnt = 0;
var vndrReconCnt = 0 ;
var costReconClick  = 0;

$(function(){
	
	$(document).ready(function(){
  		$('[data-toggle="tooltip"]').tooltip({ html:true});
	});
	
	$("#tabOverviewInvc").prop('title', '');
	
	//$("#tabToReviewInvc").prop('title', 'Shows all the pending Invoices in the system <br> <b>Blue - <b> All the Invoices pending for Approval <br><b>Green - </b> Voucher created in the System');
	$("#tabToReviewInvc").prop('title', 'Voucher created in the System');
	
	$(document).ready(function(){
  		$('[data-toggle="tooltip"]').tooltip({ html:true});
	});
	
	getOverviewData();
	
	//getVendorList( $("#venId, #venIdRecon"), glblCmpyId );
	
	getGlList( $("#glAccount") );
	
	getApproverList( $("#assignAprvList") );
	
	getBranchList( $("#apBrh, #brhFltr") );
	
	getCurrencyList( $("#cry") );
	
	//getVoucherCategoryList( $("#vchrCat") );
	
	getInPrcInvcPymtSts();
	getPaidcInvcPymtSts();
	
	$(".select2-multiline").select2({
  		templateResult: formatState
	});
	
	function formatState (state) {
	  if (!state.id) {
	    return state.text;
	  }
	  var $state = $(
	    '<span>' + state.id + '<br>' + state.text + '</span>'
	  );
	  return $state;
	};
	
	var glblCmpyId = $("#headerCmpyId").val();
	
	if( glblCmpyId == "" || glblCmpyId == null || glblCmpyId == undefined ){
		glblCmpyId = localStorage.getItem("starOcrCmpyId");
	}
	
	//$('#retrieveContent table').fixedHeaderTable({ footer: false, cloneHeadToFoot: false, fixedColumn: true });
	
	$(document).mouseup(function(e){
		var container = $("#invcHstry");

	    // if the target of the click isn't the container nor a descendant of the container
	    if (!container.is(e.target) && container.has(e.target).length === 0) {
	        container.hide();
	    }
	    
	    var container = $("#innerInvcHstry");

	    // if the target of the click isn't the container nor a descendant of the container
	    if (!container.is(e.target) && container.has(e.target).length === 0) {
	        container.hide();
	    }
	    
	    var container = $("#sendForReviewPopup");

	    // if the target of the click isn't the container nor a descendant of the container
	    if( e.target.attributes.length && e.target.attributes[0].nodeValue.indexOf("select2-results__option") < 0 && e.target.id != "assignAprv" ){
		    if (!container.is(e.target) && container.has(e.target).length === 0) {
		        container.hide();
		    }
	    }
	    
	});
	
	var curYear = parseInt(moment().format('YYYY'));
	var yearOptions = '';
	for( var y=0; y<10; y++ ){
		yearOptions += '<option>'+ (curYear - y) +'</option>';
	}
	$(".year-fltr").html(yearOptions);
	
	/*$("#glAccount").change(function(){
		var glAcct = $(this).val();
		
		if( glAcct != "" && glAcct != null && glAcct != undefined ){
			getGlSubAccList( glAcct, $("#subAccount") );
		}else{
			$("#subAccount").val("").trigger("change");
			$("#subAccount").attr("disabled", "disabled");
		}
	});*/	
	
	$("section#retrieve").on("click", ".view-doc", function(){
		$(".load-more-invc").hide();
		$(".load-more-invc").removeClass("d-inline-block");	

		var ctlNo = $(this).closest("tr").attr("data-ctl-no");
		var docUrl = rootAppName + "/rest/cstmdoc/downloadget?ctlNo=" + ctlNo;
		
		$("#previewIframe").attr("src", docUrl);
		$("#previewCol").slideDown();
	});
	
	$("section#retrieve").on("click", ".download-doc", function(){
		var ctlNo = $(this).closest("tr").attr("data-ctl-no");
		
		downloadDocument(ctlNo);
	});
	
	$(document).on('click',".filterData", function(){
		var tblDivId = $(this).parents(".table-responsive").attr("id");
		$("#"+tblDivId+" table thead tr.filter-action-row").toggleClass("d-none");
	});
	
	$("body").tooltip({ selector: '[data-toggle=tooltip]', placement: 'top' });
				
	$('body').on('focus',"input[name='daterange']", function(){
		$('input[name="daterange"]').daterangepicker({
			autoUpdateInput: false,
			opens: 'left',
			locale: {
				format: 'MM/DD/YYYY'
			}
		}, function(start, end, label) {
			console.log("A new date selection was made: " + start.format('MM/DD/YYYY') + ' to ' + end.format('MM/DD/YYYY'));
		});
		
		$('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
			$(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
			$(this).trigger("change");
		});

		$('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
			$(this).val('');
			$(this).trigger("change");
		});

	});
	
	$("#tabOverviewInvc").click(function(){
		getOverviewData();
		getInPrcInvcPymtSts();
		getPaidcInvcPymtSts();
	});
	
	$("#tabToReviewInvc").click(function(){
		getRetrieve(0);
	});
	
	$("#loadMorePndgInvc").click(function(){
		getRetrieve( $(this).attr("data-page-no") );
	});
	
	$("#tabConfirmInvc").click(function(){
		getConfirmInvc();
	});
	
	$("#loadMoreConfirmInvc").click(function(){
		getConfirmInvc( $(this).attr("data-page-no") );
	});
	
	$("#tabPrcPymntInvc").click(function(){
		getPrcPymntInvc();
	});
	
	$("#loadMorePrcPymntInvc").click(function(){
		getPrcPymntInvc( $(this).attr("data-page-no") );
	});
	
	$("#tabCompInvc").click(function(){
		getCompletedInvc();
	});
	
	$("#loadMoreCompInvc").click(function(){
		getCompletedInvc( $(this).attr("data-page-no") );
	});
	
	$("#tabInProcess").click(function(){
	});
	
	$("#tabRejectedInvc").click(function(){
		getRejectedInvoices();
	});
	
	$("#tabHoldInvc").click(function(){
		getHoldInvoices();
	});
	
	$("#loadMoreRejectedInvc").click(function(){
		getRejectedInvoices( $(this).attr("data-page-no") );
	});
	
	$("#reject").click(function(){
		var ctlNo = $("#editCtlNo").val();
		$("#rejectInvcCtlNo").val( ctlNo );
		$("#rejectRemark").val("");
		$("#rejectRemark").removeClass("border-danger");
		
		$("#rejectInvcModal").modal("show");
	});
	
	$("#retrieveContent").on("click", ".reject-invc", function(){
		var ctlNo = $(this).closest("tr").attr("data-ctl-no");
		$("#rejectInvcCtlNo").val( ctlNo );
		$("#rejectRemark").val("");
		$("#rejectRemark").removeClass("border-danger");
		
		$("#rejectInvcModal").modal("show");
	});
	
	$("#rejectInvc").click(function(){
		$("#rejectRemark").removeClass("border-danger");
		
		var rejRmk = $("#rejectRemark").val().trim();
		var ctlNo = $("#rejectInvcCtlNo").val();
		
		if( rejRmk == "" ){
			$("#rejectRemark").addClass("border-danger");
		}else{			
			$("#mainLoader").show();
		
			var inputData = { 
				ctlNo : ctlNo,
				trnRmk : rejRmk,
			};
			
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/vendor/read-ven',
				type: 'POST',
				dataType : 'json',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						getRetrieve();
						$("#rejectInvcModal, #editDataModal").modal("hide");
						customAlert("#mainNotify", "alert-success", "Invoice successfully rejected.");
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
					}
					
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					$("#mainLoader").hide();
				}
			});
		}
	});
	
	
	$("#retrieveContent, #holdInvcWrapper").on("click", ".delete-invc", function(){
		$("#retrieveContent tbody tr").removeClass("delete-me");
		$(this).closest("tr").addClass("delete-me");
		$("#closePreview").trigger("click");
		var ctlNo = $(this).closest("tr").attr("data-ctl-no");
		var vchrNo = $(this).closest("tr").attr("data-vchr-no");
		$("#deleteInvcCtlNo").val( ctlNo );
		$("#deleteInvcCtlNo").attr("data-vchr-no", vchrNo);
		
		$("#deleteInvcModal").modal("show");
	});
	
	$("#deleteInvc").click(function(){
		var ctlNo = $("#deleteInvcCtlNo").val();
		var vchrNo = $("#deleteInvcCtlNo").attr("data-vchr-no");
			
		$("#mainLoader").show();
		
		var inputData = { 
			ctlNo : ctlNo,
		};
		
		var ajaxUrl = '';
		
		
		if( vchrNo == "0" || isNaN(vchrNo) ){
			
		 ajaxUrl = rootAppName + '/rest/cstmdoc/deldoc';	
		}
		else
		{
			inputData = {
				vchrPfx: $("#retrieveContent tbody tr.delete-me").attr("data-vchr-pfx"),
				vchrNo: $("#retrieveContent tbody tr.delete-me").attr("data-vchr-no"),
			}
			ajaxUrl = rootAppName + '/rest/vour/delvour';
		}
	
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: ajaxUrl,
			type: 'POST',
			dataType : 'json',
			success: function(response){
				
				
				if( vchrNo == "0" || isNaN(vchrNo) ){
					
				}
				else
				{
					if( response.output.authToken == undefined || response.output.authToken == "" ){
						$("#logout").trigger("click");
					}
				
					localStorage.setItem("starOcrAuthToken", response.output.authToken);
					setAjaxHeader();
				}
					
				if( response.output.rtnSts == 0 ){
					getRetrieve();
					$("#deleteInvcModal").modal("hide");
					customAlert("#mainNotify", "alert-success", "Invoice deleted successfully.");
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
				}
				
				$("#mainLoader").hide();
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});
	});
	
	$("#venId").change(function(){
		$(".vchr-exist").html("");
		
		var venId = $("#venId").val();
		var invNo = $("#invNo").val();
		var pfx = $("#trnPfx").val();
		
		if( venId != "" ){
			$("#venNm").val( $("#venId option:selected").attr("data-vndr-nm") );
			
			$("#venId").parent().next().remove();
			$("#venNm").next().remove();
			
			getVndrDfltVal(venId);
		}else{
			$("#venNm, #payTerms").val("");
			$("#cry").val("").trigger("click");
		}
		
		if( venId != "" && venId != null && venId != undefined && invNo != "" && invNo != null && invNo != undefined ){
			if(pfx == 'VR')
			{
				validateVendorInvcNo(venId, invNo);
			}
		}
	});
	
	$("#trnPfx").change(function(){
		
		$("#editDataModal p.text-danger").remove();
		$(".vchr-exist").html("");
		
		var venId = $("#venId").val();
		var invNo = $("#invNo").val();
		var pfx = $("#trnPfx").val();
		
		if(pfx == 'VR')
		{
			$(".vr-mandatory").show();
			$(".crt-btn-nm").html('Voucher');
		}
		else
		{
			if(pfx == 'CM')
			{
				$(".crt-btn-nm").html('Credit Memo');
			}
			else if(pfx == 'DM')
			{
				$(".crt-btn-nm").html('Debit Memo');
			}
		
			$(".vr-mandatory").hide();
			$("#createVou").removeAttr("disabled");
		}
		
		if( venId != "" ){
			$("#venNm").val( $("#venId option:selected").attr("data-vndr-nm") );
			
			$("#venId").parent().next().remove();
			$("#venNm").next().remove();
			
			getVndrDfltVal(venId);
		}else{
			$("#venNm").val("");
		}
		
		if( venId != "" && venId != null && venId != undefined && invNo != "" && invNo != null && invNo != undefined ){
			if(pfx == 'VR')
			{
				validateVendorInvcNo(venId, invNo);
			}
		}
	});
	
	
	$("#apBrh").change(function(){
		var apBrh = $(this).val();
		if( apBrh != "" && apBrh != null && apBrh != undefined ){
			$("#apBrh").parent().next().remove();
		}
	});
	
	$("#cry").change(function(){
		var cry = $(this).val();
		if( cry != "" && cry != null && cry != undefined ){
			$("#cry").parent().next().remove();
		}
	});
					
	$("#invNo").focusout(function(){
		$(".vchr-exist").html("");
		
		var venId = $("#venId").val();
		var invNo = $("#invNo").val();
		var pfx = $("#trnPfx").val();
		
		if( venId != "" && venId != null && venId != undefined && invNo != "" && invNo != null && invNo != undefined ){
		
			if(pfx == 'VR')
			{
				validateVendorInvcNo(venId, invNo);
			}
		}
	});
	
	$("#invDt").focusout(function(){
		$("#dueDt").val($(this).val());
	
	});
	
	$("#invDt").click(function(){
		$("#dueDt").val($(this).val());
	
	});
	
	$("#poNo, #poItem").focusout(function(){
		$(".vchr-exist").html("");
		
		var poNo = $("#poNo").val();
		var poItem = $("#poItem").val();
		
		if( poNo != "" ){
			validatePoNo(poNo, poItem);
		}
	});
	
	$("#venId, #cry, #apBrh").change(function(){
		$('#trsNo').val("");
		$('#trsNo').removeAttr("data-trs-no");
		$('#trsNo').removeAttr("data-po-no");
		$('#trsNo').removeAttr("data-crcn-no");
		$('#trsNo').removeAttr("data-cost-no");
		$('#trsNo').removeAttr("data-cost-recon");
		
		calculateProof();
					
		getReconData();
	});
	
	$("#extlref").focusout(function(){
		var savedExtlRef = $("#extlref").attr("data-saved");
		var extlRef = $("#extlref").val();
		
		if( savedExtlRef != "" ){
			if( savedExtlRef != extlRef ){
				$('#trsNo').val("");
				$('#trsNo').removeAttr("data-trs-no");
				$('#trsNo').removeAttr("data-po-no");
				$('#trsNo').removeAttr("data-crcn-no");
				$('#trsNo').removeAttr("data-cost-no");
				$('#trsNo').removeAttr("data-cost-recon");
				
				$("#extlref").removeAttr("data-saved");
			}
		}
		
		calculateProof();
		
		getReconData();
	});
	
	$("#retrieve").on("click", ".edit-invc", function(){
		$("#mainLoader").show();
		
		$("#closePreview").trigger("click");
		editInvoice($(this));
	});
	
	$("#closePreview").click(function(){
		$("#previewIframe").attr("src", "");
		$("#previewCol").slideUp("slow", function(){
			$(".load-more-invc").show();
			$(".load-more-invc").addClass("d-inline-block");
		});
	});
	
	
	$("#updateApprove").click(function(){
//	alert('1');
		$("#editDataModal p.text-danger").remove();
		
		var errFlg = false;
		var ctlNo = $("#editCtlNo").val();
		var invcNo = $("#invNo").val().trim();
		var venId = $("#venId").val();
		var venNm = $("#venNm").val().trim();
		var brh = $("#apBrh").val();
		var amt = $("#amt").val().trim();
		var extRef ="";// $("#extlref").val().trim();
		var invDt = $("#invDt").val().trim();
		var dueDt = $("#dueDt").val().trim();
		var payTerm ='4';// $("#payTerms").val().trim();
		var cry = '';// $("#cry").val();
		var cat = 'test';//$("#vchrCat").val().trim();
		var pfx = $("#trnPfx").val().trim();;
		var poNo = $("#poNo").val().trim();
		var poItem = $("#poItem").val().trim();
		var discDt ='0';// $("#discDt").val().trim();
		var discAmt ='0';// $("#discAmount").val().trim();
		var voyageNo ="";// $("#voyageNo").val().trim();
		var vchrRmk = $("#vchrRmk").val().trim();
		var poNoStr = "";
	
		var jrnlDt ="";// $("#jrnlDt").val();
		var acctngPer = $("#acctngPer").val();
		var noOfDy = '0';//$("#noOfDy").val();
		var discDy = '0';//$("#discDy").val();
		var discRt ='0';// $("#discRt").val();
		var discbChg ='0';// $("#discbChg").val();
		
		if( invcNo == "" ){
			errFlg = true;
			$( "<p class='text-danger'>Please enter Invoice No</p>" ).insertAfter( "#invNo" );
		}
		
		if( venId == "" || venId == null || venId == undefined ){
			errFlg = true;
			$( "<p class='text-danger'>Please select Vendor ID</p>" ).insertAfter( $("#venId").parent() );
		}
		
		/*if( venNm == "" ){
			errFlg = true;
			$( "<p class='text-danger'>Please enter Vendor Name</p>" ).insertAfter( "#venNm" );
		}*/
		
		if( brh == "" || brh == null || brh == undefined ){
			errFlg = true;
			$( "<p class='text-danger'>Please select Branch</p>" ).insertAfter( $("#apBrh").parent() );
		}
		
		if( amt == "" ){
			errFlg = true;
			$( "<p class='text-danger'>Please enter Amount</p>" ).insertAfter( "#amt" );
		}
		
		if( invDt == "" ){
			errFlg = true;
			$( "<p class='text-danger'>Please select Invoice Date</p>" ).insertAfter( "#invDt" );
		}
		
		/*if( dueDt == "" ){
			errFlg = true;
			$( "<p class='text-danger'>Please select Due Date</p>" ).insertAfter( "#dueDt" );
		}*/
		
		if( invDt != "" && dueDt != "" ){
			var startDate = moment(invDt, "MM/DD/YYYY");
			var endDate = moment(dueDt, "MM/DD/YYYY");
			var dateDiff = endDate.diff(startDate, 'days');
			
			if( dateDiff > 0 ){
			 noOfDy= dateDiff;
			}
			if( dateDiff < 0 ){
				errFlg = true;
				$( "<p class='text-danger'>Due Date can't be less than Invoice Date</p>" ).insertAfter( "#dueDt" );
			}
		}
		
				
		if( poItem != "" ){
			if( poNo == "" ){
				errFlg = true;
				$( "<p class='text-danger'>Please enter PO Number</p>" ).insertAfter( $("#poNo").parent() );
			}
		}
		
		if( poNo != "" ){
			poNoStr = poNo;
			
			if( poItem != "" ){
				poNoStr += "-" + poItem;
			}
		}
	
		
		if( amt != "" && discAmt != "" ){
			if( parseFloat(discAmt) > parseFloat(amt) ){
				errFlg = true;
				$( "<p class='text-danger'>Disc Amount can't be greater than Invoice Amount.</p>" ).insertAfter( "#discAmount" );
			}
		}
		
		$(".gl-error").html("");
		$("#glTbody tr").each(function(index, val){
			if( $(val).attr("data-drAmt") == "0" && $(val).attr("data-crAmt") == "0" ) {
				errFlg = true;
				$(".gl-error").html("<p class='text-danger text-left mb-1'>GL debit and credit both amount can't be 0.</p>");
			}
		});
		
		if( errFlg == false ){
			$("#mainLoader").show();
			
			var glDataArray = [];
		
			$("#glTbody tr").each(function(index, val){
				var eachGlData = {};
				eachGlData.glAcc = $(val).attr("data-glacc");
				eachGlData.glSubAcc = $(val).attr("data-glSubAcc");
				eachGlData.drAmt = $(val).attr("data-drAmt");
				eachGlData.crAmt = $(val).attr("data-crAmt");
				eachGlData.remark = $(val).attr("data-remark");
				
				glDataArray.push(eachGlData);
			});
			
			var costReconArr = [];
			if( $("#trsNo").attr("data-cost-recon") != undefined ){
				costReconArr = JSON.parse($("#trsNo").attr("data-cost-recon"));
			}
		
			var inputData = { 
				vchrCmpyId : glblCmpyId,
				vchrPfx : pfx,
				vchrCtlNo  : ctlNo,
				vchrInvNo  : invcNo,
				vchrVenId : venId,
				vchrVenNm : venNm,
				vchrBrh : brh,
				vchrAmt : amt,
				vchrExtRef : extRef,
				vchrInvDt : invDt,
				vchrInvDtStr : "",
				vchrDueDt : dueDt,
				vchrDueDtStr : "",
				vchrPayTerm : payTerm,
				vchrCry : cry,
				vchrCat : cat,
				glData : glDataArray,
				poNo : poNoStr,
				costRecon : costReconArr,
				discDt: discDt,
				discAmt: discAmt,
				voyage: voyageNo,
				vchrRmk: vchrRmk,
				acctngPer: acctngPer,
				jrnlDt: jrnlDt,
				noOfDy: noOfDy,
				discDy: discDy,
				discRt: discRt,
				discbChg:discbChg,
			};
			
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/cstmdoc/upddoc',
				type: 'POST',
				dataType : 'json',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						customAlert("#popupNotify", "alert-success", "Invoice data successfully updated.");
						
						//$("#editDataModal").modal("hide");
						getRetrieve();
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#popupNotify", "alert-danger", errList);
					}
					
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					$("#mainLoader").hide();
				}
			});
		}
	});
	
	$("#createVou").click(function(){
		$("#editDataModal p.text-danger").remove();
		
		var errFlg = false;
		var ctlNo = $("#editCtlNo").val();
		var invcNo = $("#invNo").val().trim();
		var venId = $("#venId").val();
		var venNm = $("#venNm").val().trim();
		var brh = $("#apBrh").val();
		var amt = $("#amt").val().trim();
		var extRef ="";// $("#extlref").val().trim();
		var invDt = $("#invDt").val().trim();
		var dueDt = $("#dueDt").val().trim();
		var payTerm ='2';// $("#payTerms").val().trim();
		var cry = '';// $("#cry").val();
		var cat = 'test';//$("#vchrCat").val().trim();
		var pfx = $("#trnPfx").val().trim();;
		var poNo = $("#poNo").val().trim();
		var poItem = $("#poItem").val().trim();
		var discDt = $("#discDt").val().trim();// $("#discDt").val().trim();
		var discAmt = $("#discAmount").val().trim();;// $("#discAmount").val().trim();
		var voyageNo ="";// $("#voyageNo").val().trim();
		var vchrRmk = $("#vchrRmk").val().trim();
		var itemDesc = $("#itmDesc").val().trim();
		var poNoStr = "";
	
		var jrnlDt =$("#jrnlDt").val();// ;
		var acctngPer = $("#acctngPer").val();
		var noOfDy = '0';//$("#noOfDy").val();
		var discDy = '0';//$("#discDy").val();
		var discRt ='0';// $("#discRt").val();
		var discbChg ='0';// $("#discbChg").val();
							
		if( invcNo == "" && pfx == 'VR'){
			errFlg = true;
			$( "<p class='text-danger'>Please enter Invoice No</p>" ).insertAfter( "#invNo" );
		}
		
		if( venId == "" || venId == null || venId == undefined ){
			errFlg = true;
			$( "<p class='text-danger'>Please select Vendor ID</p>" ).insertAfter( $("#venId").parent() );
		}
		
		/*if( venNm == "" ){
			errFlg = true;
			$( "<p class='text-danger'>Please enter Vendor Name</p>" ).insertAfter( "#venNm" );
		}*/
		
		if( brh == "" || brh == null || brh == undefined ){
			errFlg = true;
			$( "<p class='text-danger'>Please select Branch</p>" ).insertAfter( $("#apBrh").parent() );
		}
		
		if( amt == "" ){
			errFlg = true;
			$( "<p class='text-danger'>Please enter Amount</p>" ).insertAfter( "#amt" );
		}
		
		if( invDt == "" ){
			errFlg = true;
			$( "<p class='text-danger'>Please select Invoice Date</p>" ).insertAfter( "#invDt" );
		}
		
		/*if( dueDt == "" ){
			errFlg = true;
			$( "<p class='text-danger'>Please select Due Date</p>" ).insertAfter( "#dueDt" );
		}*/
		
		if( invDt != "" && dueDt != "" ){
			var startDate = moment(invDt, "MM/DD/YYYY");
			var endDate = moment(dueDt, "MM/DD/YYYY");
			var dateDiff = endDate.diff(startDate, 'days');
			
			if( dateDiff > 0 ){
			 noOfDy= dateDiff;
			}
			if( dateDiff < 0 ){
				errFlg = true;
				$( "<p class='text-danger'>Due Date can't be less than Invoice Date</p>" ).insertAfter( "#dueDt" );
			}
		}
		
	
		if( poItem != "" ){
			if( poNo == "" ){
				errFlg = true;
				$( "<p class='text-danger'>Please enter PO Number</p>" ).insertAfter( $("#poNo").parent() );
			}
		}
		
		if( poNo != "" ){
			poNoStr = poNo;
			
			if( poItem != "" ){
				poNoStr += "-" + poItem;
			}
		}
		
		
		if( amt != "" && discAmt != "" ){
			if( parseFloat(discAmt) > parseFloat(amt) ){
				errFlg = true;
				$( "<p class='text-danger'>Disc amount can't be greater than Invoice Amount.</p>" ).insertAfter( "#discAmount" );
			}
		}
		
		if(amt != "" && pfx=='DM')
		{
			if(parseFloat(amt) > 0)
			{
				errFlg = true;
				$( "<p class='text-danger'>Amount should be negative for Debit Memo</p>" ).insertAfter( "#amt" );
			}
		}
		
		if(amt != "" && (pfx=='VR' || pfx=='CM'))
		{
			if(parseFloat(amt) < 0)
			{
				errFlg = true;
				$( "<p class='text-danger'>Amount should be positive value</p>" ).insertAfter( "#amt" );
			}
		}
		
		$(".gl-error").html("");
		$("#glTbody tr").each(function(index, val){
			if( $(val).attr("data-drAmt") == "0" && $(val).attr("data-crAmt") == "0" ) {
				errFlg = true;
				$(".gl-error").html("<p class='text-danger text-left mb-1'>GL debit and credit both amount can't be 0.</p>");
			}
		});
		
		if( errFlg == false ){
			$("#mainLoader").show();
			
			var glDataArray = [];
		
			$("#glTbody tr").each(function(index, val){
				var eachGlData = {};
				eachGlData.glAcc = $(val).attr("data-glacc");
				eachGlData.glSubAcc = $(val).attr("data-glSubAcc");
				eachGlData.drAmt = $(val).attr("data-drAmt");
				eachGlData.crAmt = $(val).attr("data-crAmt");
				eachGlData.remark = $(val).attr("data-remark");
				
				glDataArray.push(eachGlData);
			});
			
			var costReconArr = [];
			if( $("#trsNo").attr("data-cost-recon") != undefined ){
				costReconArr = JSON.parse($("#trsNo").attr("data-cost-recon"));
			}
		
			var inputData = { 
				vchrCmpyId : glblCmpyId,
				vchrPfx : pfx,
				vchrCtlNo  : ctlNo,
				vchrInvNo  : invcNo,
				vchrVenId : venId,
				vchrVenNm : venNm,
				vchrBrh : brh,
				vchrAmt : amt,
				vchrExtRef : extRef,
				vchrInvDt : invDt,
				vchrInvDtStr : "",
				vchrDueDt : dueDt,
				vchrDueDtStr : "",
				vchrPayTerm : payTerm,
				vchrCry : cry,
				vchrCat : cat,
				glData : glDataArray,
				poNo : poNoStr,
				costRecon : costReconArr,
				discDt: discDt,
				discAmt: discAmt,
				voyage: voyageNo,
				vchrRmk: vchrRmk,
				acctngPer: acctngPer,
				jrnlDt: jrnlDt,
				noOfDy: noOfDy,
				discDy: discDy,
				discRt: discRt,
				discbChg:discbChg,
				itemDesc:itemDesc
			};
			
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/vour/crtvour',
				type: 'POST',
				dataType : 'json',
				success: function(response){
					/*if( response.output.authToken == undefined || response.output.authToken == "" ){
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1" style="list-style: none">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						//customAlert("#popupNotify", "alert-danger", errList);
						
						stxAuthFailed(errList);
						
						$("#editDataModal").modal("hide");
						$("#mainLoader").hide();
						return;
					}*/
					
					localStorage.setItem("starOcrAuthToken", response.output.authToken);
					setAjaxHeader();
					
					if( response.output.rtnSts == 0 ){
						$("#editDataModal").modal("hide");
						
						getRetrieve(0);
						
						var vchrNo = response.output.vchrPfx + "-" + response.output.vchrNo;
						
						customAlert("#mainNotify", "alert-success", "Voucher ("+ vchrNo +") created successfully.");
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#popupNotify", "alert-danger", errList);
					}
					
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					$("#mainLoader").hide();
				}
			});
		}
	});
	
	
	$("#tabInProcess").click(function(){
		$("#tabInProcess").removeClass("btn-default").addClass("btn-primary");
		$("#tabPaid").removeClass("btn-primary").addClass("btn-default");
		
		$("#tabPanePaid").addClass("d-none");
		$("#tabPaneInProcess").removeClass("d-none");
	});
	
	$("#tabPaid").click(function(){
		$("#tabPaid").removeClass("btn-default").addClass("btn-primary");
		$("#tabInProcess").removeClass("btn-primary").addClass("btn-default");
		
		$("#tabPaneInProcess").addClass("d-none");
		$("#tabPanePaid").removeClass("d-none");
	});
	
	
	$("#glFormBtn").click(function(){
		$("#editDataModal #remark").val("");
		$("#editDataModal #drAmt").val(0);
		$("#editDataModal #crAmt").val(0);
		$("#editDataModal #glAccount").val('-').trigger('change');	
		//$("#subAccount").html('<option values="-">-</option>');
		$("#editDataModal #subAccount").val($("#editDataModal #subAccount option:first").val()).trigger('change');
		$("#editDataModal #addGlData").removeClass("update").text("Add");
		
		$("#editTransModal #remark").val("");
		$("#editTransModal #drAmt").val(0);
		$("#editTransModal #crAmt").val(0);
		$("#editTransModal #glAccount").val('-').trigger('change');	
		//$("#subAccount").html('<option values="-">-</option>');
		$("#editTransModal #subAccount").val($("#editTransModal #subAccount option:first").val()).trigger('change');
		$("#editTransModal #addGlData").removeClass("update").text("Add");
			
		$(".gl-form-wrapper").toggle();
	});
	
	$("#closeGlForm").click(function(){
		$(".gl-form-wrapper").hide();
	});
	
	$(document).on("click", "#addGlData", function(){
		
		var mainParentId = $(this).parents(".modal").attr("id");
		mainParentId = "#"+mainParentId;
		
		$(".gl-form-wrapper .form-control, .gl-form-wrapper .select2-selection").removeClass("border-danger");
		
		var glAcc = $(mainParentId +" #glAccount").val();
		var glSubAcc = $(mainParentId +" #subAccount").val();
		var glSubAccTxt = $(mainParentId +" #subAccount option:selected").text();
		var drAmt = $(mainParentId +" #drAmt").val().trim();
		var crAmt = $(mainParentId +" #crAmt").val().trim();
		var remark = $(mainParentId +" #remark").val().trim();
		var error = false;
		
		if( glAcc == "" || glAcc == "-" || glAcc == null || glAcc == undefined ){
			error = true;
			$( mainParentId +" #glAccount" ).next().find(".select2-selection").addClass("border-danger");
		}
		
		if( $(mainParentId +" #subAccount option").length > 1 ){
			if( glSubAcc == "" || glSubAcc == "-" ){
				error = true;
				$( mainParentId +" #subAccount" ).next().find(".select2-selection").addClass("border-danger");
			}
		}
		
		if( (drAmt == "" || $.isNumeric(drAmt) == false || drAmt == 0) && (crAmt == "" || $.isNumeric(crAmt) == false || crAmt == 0) ){
			error = true;
		//	alert('1');
			$( mainParentId +" #drAmt" ).addClass("border-danger");
			$( mainParentId +" #crAmt" ).addClass("border-danger");
		}
		
		if( remark == "" || remark.length == 0 ){
			remark = " ";
		}
		
		if( error == false ){
			if( $(mainParentId +" #addGlData").hasClass("update") ){
				var uniqueId = $(mainParentId +" #addGlData").attr("data-gl-id");
				$("#"+uniqueId).attr("data-glAcc", glAcc);
				$("#"+uniqueId).attr("data-glSubAcc", glSubAcc);
				$("#"+uniqueId).attr("data-drAmt", drAmt);
				$("#"+uniqueId).attr("data-crAmt", crAmt);
				$("#"+uniqueId).attr("data-remark", remark);
				
				var newData = '<td>'+ glAcc +'</td>'+
							//'<td>'+ glSubAccTxt +'</td>'+
							'<td>'+ drAmt +'</td>'+
							'<td>'+ crAmt +'</td>'+
							//'<td>'+ remark +'</td>'+
							'<td> <i class="icon-note menu-icon icon-pointer edit-gl-data"></i> &nbsp; <i class="icon-close menu-icon icon-pointer delete-gl-data"></i> </td>';
							//'<td><i class="icon-close menu-icon icon-pointer delete-gl-data"></i> </td>';
				$("#"+uniqueId).html(newData);
				
			}else{
				var d = new Date();
				var uniqueId = d.valueOf();
				
				var tr = '<tr id="'+ uniqueId +'" data-glAcc="'+ glAcc +'" data-glSubAcc="'+ glSubAcc +'" data-drAmt="'+ drAmt +'" data-crAmt="'+ crAmt +'" data-remark="'+ remark +'">'+
							'<td>'+ glAcc +'</td>'+
							//'<td>'+ glSubAccTxt +'</td>'+
							'<td>'+ drAmt +'</td>'+
							'<td>'+ crAmt +'</td>'+
							//'<td>'+ remark +'</td>'+
							'<td> <i class="icon-note menu-icon icon-pointer edit-gl-data"></i> &nbsp; <i class="icon-close menu-icon icon-pointer delete-gl-data"></i> </td>'+
							//'<td><i class="icon-close menu-icon icon-pointer delete-gl-data"></i> </td>'+
						'</tr>';
				$(mainParentId +" #glTbody").append(tr);
			}
			
			$(mainParentId +" #remark").val("");
			$(mainParentId +" #drAmt").val(0);
			$(mainParentId +" #crAmt").val(0);
			$(mainParentId +" #glAccount").val($("#glAccount option:first").val()).trigger('change');	
			//$("#subAccount").html('<option values="-">-</option>');
			$(mainParentId +" #subAccount").val($("#subAccount option:first").val()).trigger('change');
			
			$(mainParentId +" #addGlData").removeClass("update").text("Add");
			
			calculateProof();
		}
	});
	
	$(document).on("click", ".edit-gl-data", function(){
		var mainParentId = $(this).parents(".modal").attr("id");
		mainParentId = "#"+mainParentId;
		
		var $tr = $(this).closest( "tr" );
		
		$(mainParentId+ " #glAccount").val( $tr.attr("data-glAcc") ).trigger('change');
		//globalGlSubAcc = $tr.attr("data-glSubAcc");
		$("#subAccount").attr("data-selected", $tr.attr("data-glSubAcc"));
		$(mainParentId+ " #drAmt").val( $tr.attr("data-drAmt") );
		$(mainParentId+ " #crAmt").val( $tr.attr("data-crAmt") );
		$(mainParentId+ " #remark").val( $tr.attr("data-remark") );
		
		$(mainParentId+ " #addGlData").addClass("update").text("Update");
		$(mainParentId+ " #addGlData").attr("data-gl-id", $tr.attr("id"));
		
		$(mainParentId+ " .gl-form-wrapper").show();
		
		calculateProof();
		
	});
	
	$(document).on("click", ".delete-gl-data", function(){
		$(this).closest( "tr" ).remove();
		
		calculateProof();
	});
	
	$(document).on("click", ".cost-recon", function(){
		$("#editDataModal p.text-danger").remove();
		
		costReconClick++;
		var errFlg = false;
		var venId = $("#venId").val();
		var brh = $("#apBrh").val();
		var cry ='';// $("#cry").val();
		//var exRef = $("#extlref").val();
		var exRef = $("#invNo").val();
		
		if( venId == "" || venId == null || venId == undefined ){
			errFlg = true;
			$( "<p class='text-danger'>Please select vendor.</p>" ).insertAfter( $("#venId").parent() );
		}
		
		if( brh == "" || brh == null || brh == undefined ){
			errFlg = true;
			$( "<p class='text-danger'>Please select branch.</p>" ).insertAfter( $("#apBrh").parent() );
		}
		
		/*if( cry == "" || cry == null || cry == undefined ){
			errFlg = true;
			$( "<p class='text-danger'>Please select currency.</p>" ).insertAfter( $("#cry").parent() );
		} */
		
		if( errFlg == false ){
			
			/*if($("#trsNo").attr("data-recon-ven-id") != undefined && $("#trsNo").attr("data-recon-ven-id") != '' )
			{
				venId = $("#trsNo").attr("data-recon-ven-id");
			}*/
			
			$("#venIdRecon").val(venId).trigger('change');
			
			//getCostReconData(venId, brh, cry, exRef);
		}
	});
	
	$("#venIdRecon").change(function(){
		
		var venId = $(this).val();
		var brh = $("#apBrh").val();
		var cry ='';// $("#cry").val();
		//var exRef = $("#extlref").val();
		var exRef = $("#invNo").val();
		
		if( venId != "" && venId != null && venId != undefined ){
			
			getCostReconData(venId, brh, cry, exRef);
		}
		
		
	
	});
	
	$("#selCostReconTable").on("change", "input[type='checkbox'][name='cost-recon-row']", function(){
		if( !$(this).prop("checked") ){
			var ircNo = $(this).closest("tr").attr("data-irc-no");
			$("#costReconTable tbody tr[data-irc-no='"+ ircNo +"']").find("input[type='checkbox'][name='cost-recon-row']").prop("checked", false);
			$(this).closest("tr").addClass("display-none");
		}
		
		calculateProof();
	});
	
	$("#selCostReconTable").on("keyup", ".recon-bal-val", function(){
		calculateProof();
	});
	
	$("#costReconTable").on("change", "input[type='checkbox'][name='cost-recon-row']", function(){
		var ircNo = $(this).closest("tr").attr("data-irc-no");
		
		if( $(this).prop("checked") ){
			var costNo = $(this).closest("tr").attr("data-cost-no");
			var brh = $(this).closest("tr").attr("data-brh");
			var trsNo = $(this).closest("tr").attr("data-trs-no");
			var poNo = $(this).closest("tr").attr("data-po-no");
			var balAmt = $(this).closest("tr").attr("data-bal-amt");
			var balQty = $(this).closest("tr").attr("data-bal-qty");
			var reconVen = $(this).closest("tr").attr("data-recon-ven");
			
			var tblRow = "<tr data-cost-no='"+ costNo +"' data-irc-no='"+ ircNo +"' data-brh='"+ brh +"' data-trs-no='"+ trsNo +"' data-po-no='"+ poNo +"' data-bal-amt='"+ balAmt +"' data-recon-ven='"+ reconVen +"' data-bal-qty='"+ balQty +"'>" + $(this).closest("tr").html() + "</tr>";
			
			
			
			if( $("#selCostReconTable tbody tr[data-irc-no='"+ ircNo +"']").length == 0 ){
				$("#selCostReconTable tbody").prepend( tblRow );
				
				var reconBalAmt = $("#selCostReconTable tbody tr:first-child .bal-amt-recon").html();
				//$("#selCostReconTable tbody tr:first-child .bal-amt-recon").html("<input type='text' class='form-control' value='"+ reconBalAmt +"'/>" + " <b>(" + reconBalAmt + ")</b>")
				reconBalAmt = reconBalAmt.replace(/,/g, "");
				
				$("#selCostReconTable tbody tr:first-child .bal-amt-recon").html("<div class='input-group'>	 <input type='text' class='form-control decimal recon-bal-val' value='"+ reconBalAmt +"'><div class='input-group-prepend'> <span class='input-group-text recon-bal-max'>"+ reconBalAmt +"</span></div></div>");
				
				manageReconBalWidth();
				
			}else{
				$("#selCostReconTable tbody tr[data-irc-no='"+ ircNo +"']").removeClass("display-none");
			}
			$("#selCostReconTable tbody input[type='checkbox']").prop("checked", true);
			
			
		}else{
			if( $("#selCostReconTable tbody tr[data-irc-no='"+ ircNo +"']").length ){
				$("#selCostReconTable tbody tr[data-irc-no='"+ ircNo +"']").addClass("display-none");
			}
		}
		
		calculateProof();
	});
	
	/*$("#saveCostRecon").click(function(){
		var trsNo = "", poNo = "", crcnNo = "", costNo = '';
		var costReconArr = [];
		//alert('1');
		$("#costReconTable tbody tr").each(function(index, row){
			if( $(row).find("input[name='cost-recon-row']").prop("checked") ){
				trsNo += (trsNo.length) ? ", " : "";
				trsNo += $(row).attr("data-trs-no");
				
				poNo += (poNo.length) ? ", " : "";
				poNo += $(row).attr("data-po-no");
				
				//crcnNo += (crcnNo.length) ? ", " : "";
				//crcnNo += $(row).attr("data-crcn-no");
				
				//costNo += (costNo.length) ? ", " : "";
				//costNo += $(row).attr("data-cost-no");
				//alert(poNo);
				var costReconObj = {
					trsNo: $(row).attr("data-trs-no"),
					ircNo: $(row).attr("data-ircNo"),
					poNo: $(row).attr("data-po-no"),
					//crcnNo: $(row).attr("data-crcn-no"),
					//costNo: $(row).attr("data-cost-no"),
					balAmt: $(row).attr("data-bal-amt"),
					balQty: $(row).attr("data-bal-qty"),
					reconVenId: $("#venIdRecon").val(),
				};
				costReconArr.push(costReconObj);
			}
		});
		
		$("#trsNo").val(trsNo);
		//$("#trsNo").attr("data-trs-no", trsNo);
		$("#trsNo").attr("data-po-no", poNo);
		//$("#trsNo").attr("data-crcn-no", crcnNo);
		//$("#trsNo").attr("data-cost-no", costNo);
		$("#trsNo").attr("data-recon-ven-id", $("#venIdRecon").val());
		$("#trsNo").attr("data-cost-recon", JSON.stringify(costReconArr));
		
		$("#costReconModal").modal("hide");
		
		calculateProof();
	});*/
	
	$("#saveCostRecon").click(function(){
	
		$("#selCostReconTable tbody tr input[type='text']").removeClass("text-danger");
		
		var trsNo = "", poNo = "", ircNo = "", costNo = '', venId = '';
		var errFlg = false;
		var costReconArr = [];
		$("#selCostReconTable tbody tr.display-none").remove();
		$("#selCostReconTable tbody tr").each(function(index, row){
			if( $(row).find("input[name='cost-recon-row']").prop("checked") ){
				trsNo += (trsNo.length) ? ", " : "";
				trsNo += $(row).attr("data-trs-no");
				
				poNo += (poNo.length) ? ", " : "";
				poNo += $(row).attr("data-po-no");
				
				ircNo += (ircNo.length) ? ", " : "";
				ircNo += $(row).attr("data-irc-no");
				
				venId += (venId.length) ? ", " : "";
				venId += $(row).attr("data-recon-ven");
				
				var maxBalValue  = parseFloat($(row).find(".recon-bal-max").html());
				var enteredBalValue  = parseFloat($(row).find("input[type='text']").val());
				
				if(enteredBalValue > maxBalValue || isNaN(enteredBalValue) )
				{
					$(row).find("input[type='text']").addClass("border-danger");
					errFlg = true;
				}
				
				
				var costReconObj = {
					trsNo: $(row).attr("data-trs-no"),
					poNo: $(row).attr("data-po-no"),
					ircNo: $(row).attr("data-irc-no"),
					/*balAmt: $(row).attr("data-bal-amt"),*/
					balAmt: $(row).find("input[type='text']").val(),
					balQty: $(row).attr("data-bal-qty"),
					reconVenId: $(row).attr("data-recon-ven"),
				};
				costReconArr.push(costReconObj);
			}
		});
		
		if(errFlg == true)
		{
			
			return;
		}
		
		$("#trsNo").val(trsNo);
		$("#trsNo").attr("data-trs-no", trsNo);
		$("#trsNo").attr("data-po-no", poNo);
		$("#trsNo").attr("data-irc-no", ircNo);
		$("#trsNo").attr("data-cost-no", costNo);
		$("#trsNo").attr("data-recon-ven-id",venId);
		$("#trsNo").attr("data-cost-recon", JSON.stringify(costReconArr));
		
		$("#costReconModal").modal("hide");
		
		calculateProof();
	});
	
	$("#stxAppInvcFltr").change(function(){
		//if($(this).prop("checked")){
		//	$("#loadMorePndgInvc").hide();
		//	$("#retrieveContent table tbody tr.stx-invc, #retrieveContent table .trans-pymnt-col").addClass("d-none");
		//	$("#retrieveContent table tbody tr.app-invc").removeClass("d-none");
		//}else
		{
			$("#loadMorePndgInvc").show();
			$("#retrieveContent table tbody tr.app-invc").addClass("d-none");
			$("#retrieveContent table tbody tr.stx-invc, #retrieveContent table .trans-pymnt-col").removeClass("d-none");
		}
	});
	
	$("#amt").focusout(function(){
		calculateProof();
	});
	

	$("#discDt").keyup(function(){
		if( $(this).val().trim() != "" ){
			$("#clearDiscDt").show();
		}else{
			$("#clearDiscDt").hide();
		}
	});
	$("#discDt").blur(function(){
		if( $(this).val().trim() != "" ){
			$("#clearDiscDt").show();
		}else{
			$("#clearDiscDt").hide();
		}
	});
	$("#discDt").focusout(function(){
		if( $(this).val().trim() != "" ){
			$("#clearDiscDt").show();
		}else{
			$("#clearDiscDt").hide();
		}
	});
	$("#discDt").change(function(){
		if( $(this).val().trim() != "" ){
			$("#clearDiscDt").show();
		}else{
			$("#clearDiscDt").hide();
		}
	});
	$("#clearDiscDt").click(function(){
		$("#discDt").val("");
		$("#clearDiscDt").hide();
	}); 
	

	$("#dueDt").keyup(function(){
		if( $(this).val().trim() != "" ){
			$("#clearDueDt").show();
		}else{
			$("#clearDueDt").hide();
		}
	});
	$("#dueDt").blur(function(){
		if( $(this).val().trim() != "" ){
			$("#clearDueDt").show();
		}else{
			$("#clearDueDt").hide();
		}
	});
	$("#dueDt").focusout(function(){
		if( $(this).val().trim() != "" ){
			$("#clearDueDt").show();
		}else{
			$("#clearDueDt").hide();
		}
	});
	$("#dueDt").change(function(){
		if( $(this).val().trim() != "" ){
			$("#clearDueDt").show();
		}else{
			$("#clearDueDt").hide();
		}
	});
	$("#clearDueDt").click(function(){
		$("#dueDt").val("");
		$("#clearDueDt").hide();
	});
	
	$('#costReconModal').on('show.bs.modal', function (e) {
		$("#editDataModal").modal("hide");
	});
	
	$('#costReconModal').on('hidden.bs.modal', function (e) {
		$("#costReconTable thead th .form-control").val("");
		$("#editDataModal").modal("show");
	});
	
	
	$("#reviewWkf").click(function(){
		
		var wkfId = $("#wkfInfo").attr("data-wkf-id");
		
		getApproverListWithoutWkfUsr($("#aprv"), wkfId);
	});
	
	$("#sendForReview").click(function(){
		
		var errFlg = 0;
		var ctlNo = $("#wkfInfo").attr("data-ctl-no");
		var wkfId = $("#wkfInfo").attr("data-wkf-id");
		
		
		$("#blockRemark").removeClass("border-danger");
		$("#aprv").next().find(".select2-selection").removeClass("border-danger");
		
		var remark = $("#blockRemark").val().trim();
		var aprv = $("#aprv").val().trim();
		
		if(aprv == "" || aprv == null)
		{
			$("#aprv").next().find(".select2-selection").addClass("border-danger");
			errFlg = 1;
		}
		
		if( remark == "" ){
			$("#blockRemark").addClass("border-danger");
			errFlg = 1;
		}
		
		if(errFlg == 1)
		{
			return;
		}
		
		if(!errFlg) 
		{
			$("#mainLoader").show();
			
			var inputData = {
				asgnTo : aprv,
				sts: "S",
				ctlNo: ctlNo, 
				wkfId: wkfId,
				rmk: remark,
			};
			
			$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/wkf-map/update',
			type: 'POST',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					$("#sendToAprvModal, #sendForReviewModal, #editDataModal").modal("hide");
					
					customAlert("#mainNotify", "alert-success", "Invoice has been sent for further review to User - " + response.output.usrNm);
					
					getRetrieve();
				
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
				}
				
				
				$("#mainLoader").hide();
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				console.log(jqXHR);
				console.log(textStatus);
				console.log(errorThrown);
				$("#mainLoader").hide();
			}
		});
		}
	});
	
	$("#approveWkf").click(function(){
		$("#wkfInfo").attr("data-wkf-sts", "A");
		$("#approveRejectModal .apr-rej").html("approve");
		$("#sendForwModalBtn, #sendBackModalBtn").hide();
		$("#approveRejectBtn").html("Approve");
		$("#rmkApproveReject").val("");
		$("#approveRejectModal").modal("show");
	});
	
	$("#rejectWkf").click(function(){
		$("#wkfInfo").attr("data-wkf-sts", "R");
		$("#approveRejectModal .apr-rej").html("reject");
		$("#approveRejectBtn").html("Reject");
		
		
		var wkfBackSts = $("#wkfInfo").attr("data-wkf-back");
		var wkfTotSts = $("#wkfInfo").attr("data-wkf-forw");
		var wkfExtSts = $("#wkfInfo").attr("data-wkf-ext");
		
		/* CONDITION TO SHOW */
		$("#sendForwModalBtn, #sendBackModalBtn").show();
		if(wkfBackSts == 0)
		{
			$("#sendBackModalBtn").hide();
		}
		
		if(wkfTotSts == wkfBackSts)
		{
			$("#sendForwModalBtn").hide();
		}
		
		if(wkfExtSts == 0)
		{
			$("#sendBackModalBtn,#sendForwModalBtn").hide();
		}
		
		$("#rmkApproveReject").val("");
		$("#approveRejectModal").modal("show");
		
	});
	
	
	$("#sendForwModalBtn").click(function(){
	
		var errFlg = 0;
		var statusMsg = '';
		var ctlNo = $("#wkfInfo").attr("data-ctl-no");
		var wkfId = $("#wkfInfo").attr("data-wkf-id");
		var usrId = localStorage.getItem("starOcrUserId");
		var wkfSts = $("#wkfInfo").attr("data-wkf-sts");
		
		$("#rmkApproveReject").removeClass("border-danger");
		
		var remark = $("#rmkApproveReject").val().trim();
		
		if( remark == "" ){
			$("#blockRemark").addClass("border-danger");
			errFlg = 1;
		}
		
		if(errFlg == 1)
		{
			return;
		}
		
		if(!errFlg)
		{
			$("#mainLoader").show();
			
			var inputData = {
				asgnTo : usrId,
				sts: "F",
				ctlNo: ctlNo, 
				wkfId: wkfId,
				rmk: remark,
			};
			
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/wkf-map/update',
				type: 'POST',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						$("#approveRejectModal").modal("hide");
						$("#editDataModal").modal("hide");
						customAlert("#mainNotify", "alert-success", "Invoice has been successfully forwarded to next Approver.");
						
						getRetrieve();
					
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
					}
					
					
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					console.log(jqXHR);
					console.log(textStatus);
					console.log(errorThrown);
					$("#mainLoader").hide();
				}
			});
		}
	
	});
	
	$("#sendBackModalBtn").click(function(){
	
		var errFlg = 0;
		var statusMsg = '';
		var ctlNo = $("#wkfInfo").attr("data-ctl-no");
		var wkfId = $("#wkfInfo").attr("data-wkf-id");
		var usrId = localStorage.getItem("starOcrUserId");
		var wkfSts = $("#wkfInfo").attr("data-wkf-sts");
		
		$("#rmkApproveReject").removeClass("border-danger");
		
		var remark = $("#rmkApproveReject").val().trim();
		
		if( remark == "" ){
			$("#blockRemark").addClass("border-danger");
			errFlg = 1;
		}
		
		if(errFlg == 1)
		{
			return;
		}
		
		if(!errFlg)
		{
			$("#mainLoader").show();
			
			var inputData = {
				asgnTo : usrId,
				sts: "B",
				ctlNo: ctlNo, 
				wkfId: wkfId,
				rmk: remark,
			};
			
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/wkf-map/update',
				type: 'POST',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						$("#approveRejectModal").modal("hide");
						$("#editDataModal").modal("hide");
						customAlert("#mainNotify", "alert-success", "Invoice has been successfully sent Back to last Approver");
						
						getRetrieve();
					
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
					}
					
					
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					console.log(jqXHR);
					console.log(textStatus);
					console.log(errorThrown);
					$("#mainLoader").hide();
				}
			});
		}
	
	});
	
	
	$("#approveRejectBtn").click(function(){
	
		var errFlg = 0;
		var statusMsg = '';
		var ctlNo = $("#wkfInfo").attr("data-ctl-no");
		var wkfId = $("#wkfInfo").attr("data-wkf-id");
		var usrId = localStorage.getItem("starOcrUserId");
		var wkfSts = $("#wkfInfo").attr("data-wkf-sts");
		
		if( wkfSts == "A" ){
			statusMsg = "approved";
		}else if( wkfSts == "R" ){
			statusMsg = "rejected";
		}
		
		$("#rmkApproveReject").removeClass("border-danger");
		
		var remark = $("#rmkApproveReject").val().trim();
		
		if( remark == "" ){
			$("#blockRemark").addClass("border-danger");
			errFlg = 1;
		}
		
		if(errFlg == 1)
		{
			return;
		}
		
		if(!errFlg)
		{
			$("#mainLoader").show();
			
			var inputData = {
				asgnTo : usrId,
				sts: wkfSts,
				ctlNo: ctlNo, 
				wkfId: wkfId,
				rmk: remark,
			};
			
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/wkf-map/update',
				type: 'POST',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						$("#approveRejectModal").modal("hide");
						$("#editDataModal").modal("hide");
						customAlert("#mainNotify", "alert-success", "Invoice has been successfully "+ statusMsg +".");
						
						getRetrieve();
					
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
					}
					
					
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					console.log(jqXHR);
					console.log(textStatus);
					console.log(errorThrown);
					$("#mainLoader").hide();
				}
			});
		}
	
	});
	
	
	$("#retrieve").on("click", ".wkf-his", function(e){
		var ctlNo = $(this).closest("tr").attr("data-ctl-no");
		var wkfId = $(this).closest("tr").attr("data-wkf-id");
		
		getWorkflowHistory(ctlNo, wkfId, e, "O");
	});
	
	$("#retrieve").on("click", ".assign-aprv-badge", function(e){
		var ctlNo = $(this).closest("tr").attr("data-ctl-no");
		var wkfId = $(this).closest("tr").attr("data-wkf-id");
		var aprvr = $(this).closest("tr").attr("data-wkf-aprvr");
		
		$("#assignAprv").attr("data-ctl-no", ctlNo);
		$("#assignAprv").attr("data-wkf-id", wkfId);
		
		if( (e.pageY - 70 - $("#sendForReviewPopup").innerHeight()) <= 20){
			$("#sendForReviewPopup").css({"top": "0px", "left": e.pageX - 270 + "px"});
		}else{
			$("#sendForReviewPopup").css({"top": e.pageY - 70 - $("#sendForReviewPopup").innerHeight() + "px", "left": e.pageX - 270 + "px"});
		}
			
		$("#assignAprvList").val("").trigger("change");	
		$("#assignRmk").val('');	
		$("#assignAprvList").next().find(".select2-selection").removeClass("border-danger");
		$("#assignRmk").removeClass("border-danger");
		
		if(aprvr.length == 0)
		{
			$("#sendForReviewPopup").show();
		}
		else
		{
			getWorkflowHistory(ctlNo, wkfId, e, "");
		}
			
	});
	
	$(".close-invc-hstry").click(function(){
		$(this).parent().hide();
	});
	
	$("#wkf-status").on("click", ".view-invc-hstry", function(e){
		var ctlNo = $("#wkfInfo").attr("data-ctl-no");
		var wkfId = $("#wkfInfo").attr("data-wkf-id");
		
		getWorkflowHistory(ctlNo, wkfId, "", "I");
	});
	
	$("#retrieveContent").on("click", ".hold-invc", function(){
		$("#retrieveContent tbody tr").removeClass("delete-me");
		
		$("#closePreview").trigger("click");
		$(this).closest("tr").addClass("delete-me");
		
		var ctlNo = $(this).closest("tr").attr("data-ctl-no");
		$("#holdInvcBtn").attr("data-ctl-no", ctlNo);
		
		$("#holdModal").modal("show");
	});
	
	$("#holdInvcBtn").click(function(){
		$("#holdRmk").removeClass("border-danger");
		
		var errFlg = false;
		var holdRmk = $("#holdRmk").val().trim();
		var ctlNo = $("#holdInvcBtn").attr("data-ctl-no");
		
		if( holdRmk == "" ){
			$("#holdRmk").addClass("border-danger");
			errFlg = true;
		}
		
		if(errFlg == false){
			$("#mainLoader").show();
			
			var inputData = {
				trnSts : "H",
				ctlNo: ctlNo,
				rmk: holdRmk,
			};
			
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/cstmdoc/updSts',
				type: 'POST',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						$("#holdRmk").val("");
						$("#holdModal").modal("hide");
						
						customAlert("#mainNotify", "alert-success", "Invoice has been moved to hold status.");
						
						getRetrieve();
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#popup", "alert-danger", errList);
					}
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					console.log(jqXHR);
					console.log(textStatus);
					console.log(errorThrown);
					$("#mainLoader").hide();
				}
			});
		}
	});
	
	$("#holdInvcWrapper").on("click", ".unhold-invc", function(){
		$("#retrieveContent tbody tr").removeClass("delete-me");
		$(this).closest("tr").addClass("delete-me");
		
		$("#closePreview").trigger("click");
		var ctlNo = $(this).closest("tr").attr("data-ctl-no");
		$("#unholdInvcBtn").attr("data-ctl-no", ctlNo);
		
		$("#unholdModal").modal("show");
	});
	
	$("#unholdInvcBtn").click(function(){
		$("#unholdRmk").removeClass("border-danger");
		
		var errFlg = false;
		var unholdRmk = $("#unholdRmk").val().trim();
		var ctlNo = $("#unholdInvcBtn").attr("data-ctl-no");
		
		if( unholdRmk == "" ){
			$("#unholdRmk").addClass("border-danger");
			errFlg = true;
		}
		
		if(errFlg == false){
			$("#mainLoader").show();
			
			var inputData = {
				trnSts : "",
				ctlNo: ctlNo,
				rmk: unholdRmk,
			};
			
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/cstmdoc/updSts',
				type: 'POST',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						$("#unholdRmk").val("");
						$("#unholdModal").modal("hide");
						
						customAlert("#mainNotify", "alert-success", "Invoice has been removed from hold status.");
						
						getHoldInvoices();
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#popup", "alert-danger", errList);
					}
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					console.log(jqXHR);
					console.log(textStatus);
					console.log(errorThrown);
					$("#mainLoader").hide();
				}
			});
		}
	});
	
	$("#rejectedInvcWrapper").on("click", ".unreject-invc", function(){
		$("#rejectedInvcWrappe tbody tr").removeClass("delete-me");
		$(this).closest("tr").addClass("delete-me");
		
		$("#closePreview").trigger("click");
		var ctlNo = $(this).closest("tr").attr("data-ctl-no");
		var wkfId = $(this).closest("tr").attr("data-wkf-id");
		var wkfSts = $(this).closest("tr").attr("data-wkf-sts");
		var wkfAprvr = $(this).closest("tr").attr("data-wkf-aprvr");
		
		$("#unrejectInvcBtn").attr("data-ctl-no", ctlNo);
		$("#unrejectInvcBtn").attr("data-wkf-id", wkfId);
		$("#unrejectInvcBtn").attr("data-wkf-sts", wkfSts);
		$("#unrejectInvcBtn").attr("data-wkf-aprvr", wkfAprvr);
		
		$("#unrejectModal").modal("show");
	});
	
	$("#unrejectInvcBtn").click(function(){
		$("#unrejectRmk").removeClass("border-danger");
		
		var errFlg = false;
		var unrejectRmk = $("#unrejectRmk").val().trim();
		var ctlNo = $("#unrejectInvcBtn").attr("data-ctl-no");
		var wkfId = $("#unrejectInvcBtn").attr("data-wkf-id");
		var wkfSts = $("#unrejectInvcBtn").attr("data-wkf-sts");
		var wkfAprvr = $("#unrejectInvcBtn").attr("data-wkf-aprvr");
		
		if( unrejectRmk == "" ){
			$("#unrejectRmk").addClass("border-danger");
			errFlg = true;
		}
		
		if(errFlg == false){
			$("#mainLoader").show();
			
			var inputData = {
				sts : "RV",
				ctlNo: ctlNo,
				asgnTo: wkfAprvr,
				wkfId:wkfId,
				rmk: unrejectRmk,
			};
			
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/wkf-map/update',
				type: 'POST',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						$("#unrejectRmk").val("");
						$("#unrejectModal").modal("hide");
						
						customAlert("#mainNotify", "alert-success", "Invoice has been moved to Review");
						
						getRejectedInvoices();
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#popup", "alert-danger", errList);
					}
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					console.log(jqXHR);
					console.log(textStatus);
					console.log(errorThrown);
					$("#mainLoader").hide();
				}
			});
		}
	});
	
	$(".modal").on("hidden.bs.modal", function () {
		$("tr").removeClass("delete-me");
	});
	
	$("#retrieveContent, #holdInvcWrapper").scroll(function() {
	  	$( "#invcHstry, #sendForReviewPopup" ).hide();
	});
	
	$("#retrieve").on("click", ".invc-comments", function(){
		var ctlNo = $(this).closest("tr").attr("data-ctl-no");
		$("#closePreview").trigger("click");
		getInvoiceComments(ctlNo);
	});
	
	$("#invcComments").click(function(){
		getInvoiceComments($("#editCtlNo").val());
	});
	
	$("#saveCommentBtn").click(function(){
		$("#commentText").removeClass("border-danger");
	
		var errFlg = false;
		var ctlNo = $(this).attr("data-ctl-no");
		var comment = $("#commentText").val().trim();
		
		if( comment == "" || comment.length == 0 ){
			errFlg = true;
			$("#commentText").addClass("border-danger");
		}
		
		if( errFlg == false ){
			$("#mainLoader").show();
		
			var inputData = { 
				ctlNo: ctlNo,
				note: comment,
			};
			
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/vchr-note/add',
				type: 'POST',
				dataType : 'json',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						$("#commentText").val("");
						customAlert("#popupNotify", "alert-success", "Comment successfully added.");
						getInvoiceComments(ctlNo);
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#popupNotify", "alert-danger", errList);
						$("#mainLoader").hide();
					}
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					$("#mainLoader").hide();
				}
			});
		}
	});
	
	$("#commentList").on("click", ".delete-comment", function(){
		var notesId = $(this).closest(".invc-note-row").attr("data-note-id");
		
		$("#deleteCommentBtn").attr("data-notes-id", notesId);
		$("#deleteCommentModal").modal("show");
	});
	
	$("#deleteCommentBtn").click(function(){
		var notesId = $(this).attr("data-notes-id");
		
		$("#mainLoader").show();
		
		var inputData = { 
			notesId: notesId,
		};
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/vchr-note/del',
			type: 'POST',
			dataType : 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					customAlert("#popupNotify", "alert-success", "Comment successfully deleted.");
					
					$("#deleteCommentModal").modal("hide");
					
					getInvoiceComments( $("#saveCommentBtn").attr("data-ctl-no") );
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#popupNotify", "alert-danger", errList);
					$("#mainLoader").hide();
				}
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});
	});
	
	$("#exprtRecon").click(function(){
		var venId = $("#venId").val();
		var brh = $("#apBrh").val();
		var cry = $("#cry").val();
		var exRef = $("#extlref").val();
		
		var docUrl = rootAppName + "/rest/export/recon?cmpyId="+ glblCmpyId +"&venId=" + venId + "&brh=" + brh + "&cry=" + cry + "&exRef=" + exRef;

		window.open(docUrl, '_blank');
	});
	
	$(".year-fltr").change(function(){
		var year = $(this).val();
		
		$("#retrieve .tab-content .filter-action-row .form-control").val("");
		$("#retrieve .tab-content .filter-action-row .form-control").val("").trigger("change");
		
		if( year != "" && year != null && year != undefined ){
			$(".year-fltr").val(year);
			
			$("#invcTabList .nav-link.active").trigger("click")
		}
	});
	
	$("#closeAprv").click(function(){
		$("#sendForReviewPopup").hide();
	});
	
	$("#assignAprv").click(function(){
		
		var errFlg = 0;
		var remark = $("#assignRmk").val();
		
		$("#assignAprvList").next().find(".select2-selection").removeClass("border-danger");
		var aprv = $("#assignAprvList").val();
		
		if(aprv == '' || aprv == null || aprv == undefined)
		{
			$("#assignAprvList").next().find(".select2-selection").addClass("border-danger");
			errFlg = 1;
		}
		
		if(remark == '' || remark == null || remark == undefined)
		{
			$("#assignRmk").addClass("border-danger");
			errFlg = 1;
		}
		
		if(errFlg == 1)
		{
			return;
		}
		
		var ctlNo  = $("#assignAprv").attr("data-ctl-no");
		var wkfId  = $("#assignAprv").attr("data-wkf-id");
		
		$("#sendForReviewPopup").hide();
		
			$("#mainLoader").show();
			
			var inputData = {
				asgnTo : aprv,
				sts: "S",
				ctlNo: ctlNo, 
				wkfId: wkfId,
				rmk: remark,
			};
			
			$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/wkf-map/add',
			type: 'POST',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					$("#sendToAprvModal, #sendForReviewModal, #editDataModal").modal("hide");
					
					customAlert("#mainNotify", "alert-success", "Invoice has been sent for review to User - " + response.output.usrNm);
					
					getRetrieve();
				
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
				}
				
				
				$("#mainLoader").hide();
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				console.log(jqXHR);
				console.log(textStatus);
				console.log(errorThrown);
				$("#mainLoader").hide();
			}
		});
		
		
	});
	
});


function validateVendorInvcNo(venId, invNo){
	$("#mainLoader").show();
		
	var inputData = { 
		venId : venId,
		invNo : invNo,
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/cstmdoc/validate-inv',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				$(".vchr-exist").html("");
				$("#createVou").removeAttr("disabled");
			}else{
				if( response.output.vchrRefNo != undefined && response.output.vchrRefNo != "" ){
					var msg = "Voucher " + response.output.vchrRefNo.trim() + " is already created for Vendor ("+ venId +") and Invoice Number("+ invNo +")";
					$(".vchr-exist").html( msg );
					$("#createVou").attr("disabled", "disabled");
				}
				
				/*var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#popupNotify", "alert-danger", errList);*/
			}
			
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}


function validatePoNo(poNo, poItem){
	$("#mainLoader").show();
		
	var inputData = { 
		poNo : poNo,
		poItm : poItem,
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/cstmdoc/validate-po',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				$(".vchr-exist").html("");	
				$("#poNo, #poItem").removeClass("border-danger");			
				$("#venId").val( response.output.venId ).trigger("change");
			}else{
				var poNoStr = poNo;
				$("#poNo").addClass("border-danger");
				if( poItem != "" ){
					poNoStr += "-" + poItem;
					$("#poItem").addClass("border-danger");
				}
				$(".vchr-exist").html( "PO (" + poNoStr + ") is not available in the system." );
				
				/*var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#popupNotify", "alert-danger", errList);*/
			}
			
			$("#mainLoader").hide();
			
			getReconData();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}


function getReconData(){
	var errFlg = false; 
	var venId = $("#venId").val();
	var cry = $("#cry").val();
	var poNo = $("#poNo").val().trim();
	var exRef = "";//$("#extlref").val().trim();
	
	if( venId == "" || venId == null || venId == undefined ){
		errFlg = true;
	}
	
	if( cry == "" || cry == null || cry == undefined ){
		errFlg = true;
	}
	
	/*if( poNo == "" || poNo == null || poNo == undefined ){
		errFlg = true;
	}*/
	
	if(errFlg){
		return;
	}
	
	$("#mainLoader").show();
		
	var inputData = {
		venId: venId,
		poNo : poNo,
		cry : cry,
		exRef: exRef,
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/cstmdoc/get-recon-data',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){			
				$(".recon-amt").html( response.output.reconAmtStr ); 
				$(".recon-cnt").html( response.output.reconCount );
			}else{				
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#popupNotify", "alert-danger", errList);
			}
			
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}
	
function getRetrieve(pageNo=0){
	$("#mainLoader").show();
	
	var year = $(".year-fltr").val();
	if( year == null || year == undefined || year == "" ){
		year = moment().format('YYYY');
	}
	
	var inputData = {
		trnSts: "",
		pageNo: pageNo,
		year: year,
	}
	
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/cstmdoc/readdoc',
		data : JSON.stringify( inputData ),
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				setInvcTabCounts(response);
				var loopSize = response.output.fldTblDoc.length;
				var tblHtml = '', rowClass = '';	
				
				//$("#pendingInvoice").html( loopSize );
				var invInfo = '', vchrExtRef = "", viewDwnld = '', vchrNoStr = '', vchrPoStr = '', vchrCatStr = '', transStsStr = '', 
				pymntStsStr = '', invFrm = '', editBtn = '', amtStr = '', deleteBtn = '', holdBtn = '', invcDtStr = '', dueDtStr = '', 
				srcStr = '', commentIcon = '';
									
				for( var i=0; i<loopSize; i++ ){
	
					invcDtStr = (response.output.fldTblDoc[i].vchrInvDtStr!=undefined) ? response.output.fldTblDoc[i].vchrInvDtStr : '';
					dueDtStr = (response.output.fldTblDoc[i].vchrDueDtStr!=undefined) ? response.output.fldTblDoc[i].vchrDueDtStr : '', 
					invInfo = '', viewDwnld = '', vchrNoStr = '', vchrPoStr = '', vchrCatStr = '', transStsStr = '', pymntStsStr = '', srcStr = '';
					invFrm = '<span class="fill-circle bg-primary"></span>';
					rowClass = 'app-invc';
					editBtn = '<i class="icon-note menu-icon icon-pointer edit-invc text-primary" title="Edit" data-toggle="tooltip"></i>';
					deleteBtn = '<i class="icon-trash menu-icon icon-pointer delete-invc icon-red" data-toggle="tooltip" title="Delete"></i>';
					holdBtn = '<i class="icon-clock menu-icon icon-pointer hold-invc icon-red" data-toggle="tooltip" title="Hold"></i>';
					commentIcon = '';
					
					if( response.output.fldTblDoc[i].srcFlg != undefined && response.output.fldTblDoc[i].srcFlg == "M" ){
						srcStr = '<i class="icon-envelope"></i>';
					}
					if( response.output.fldTblDoc[i].vchrPfx != undefined && response.output.fldTblDoc[i].vchrNo != undefined ){
						vchrNoStr = response.output.fldTblDoc[i].vchrPfx.trim() + "-" + response.output.fldTblDoc[i].vchrNo.trim();
						invFrm = '<span class="fill-circle bg-success"></span>';
						rowClass = 'stx-invc';
						editBtn = '', holdBtn = '', deleteBtn='';
					}
					if( response.output.fldTblDoc[i].poPfx != undefined && response.output.fldTblDoc[i].poPfx.trim() != "null" ){
						vchrPoStr = response.output.fldTblDoc[i].poPfx.trim() + "-" + response.output.fldTblDoc[i].poNo.trim();
						if( response.output.fldTblDoc[i].poItm.trim() != 0 ){
							vchrPoStr += "-" + response.output.fldTblDoc[i].poItm.trim();
						} 
					}
					if( response.output.fldTblDoc[i].vchrCat != undefined ){
						vchrCatStr = response.output.fldTblDoc[i].vchrCat.trim();
					}
					if( response.output.fldTblDoc[i].transSts != undefined ){
						if(response.output.fldTblDoc[i].transSts.trim() == 'H'){
							transStsStr = '<label class="badge badge-danger">Held</label>';
						}else if(response.output.fldTblDoc[i].transSts.trim() == 'C'){
							transStsStr = '<label class="badge badge-success">Completed</label>';
						}else if(response.output.fldTblDoc[i].transSts.trim() == 'A'){
							transStsStr = '<label class="badge badge-primary">Active</label>';
						}else{
							transStsStr = '<label class="badge badge-info">Pending</label>';
						}		
					}
					if( response.output.fldTblDoc[i].pymntSts != undefined ){
						if(response.output.fldTblDoc[i].pymntSts.trim() == 'A' && response.output.fldTblDoc[i].transSts.trim() == 'C'){
							pymntStsStr = '<label class="badge badge-success">Completed</label>';
						}else if(response.output.fldTblDoc[i].pymntSts.trim() == 'H'){
							pymntStsStr = '<label class="badge badge-danger">Held</label>';
						}else if(response.output.fldTblDoc[i].pymntSts.trim() == 'C'){
							pymntStsStr = '<label class="badge badge-success">Completed</label>';
						}else if(response.output.fldTblDoc[i].pymntSts.trim() == 'A'){
							pymntStsStr = '<label class="badge badge-primary">Active</label>';
						}else{
							pymntStsStr = '<label class="badge badge-info">Pending</label>';
						}
					}
					
					if( response.output.fldTblDoc[i].transSts != undefined && response.output.fldTblDoc[i].pymntSts != undefined ){
						/*if(response.output.fldTblDoc[i].transSts.trim() == 'C' && response.output.fldTblDoc[i].pymntSts.trim() == 'C'){
							deleteBtn = '';
						}*/
						
						if(response.output.fldTblDoc[i].transSts.trim() == 'C'){
							deleteBtn = '';
						}
					}
					
					if( response.output.fldTblDoc[i].vchrInvNo != undefined || response.output.fldTblDoc[i].vchrVenId != undefined ){
						if( response.output.fldTblDoc[i].vchrInvNo.trim() != "" || response.output.fldTblDoc[i].vchrVenId.trim() != "" ){
							invInfo += '<h6 class="text-primary mb-1">';
							if( response.output.fldTblDoc[i].vchrInvNo != undefined && response.output.fldTblDoc[i].vchrInvNo.trim() != "" ){
								invInfo += '<span title="Invoice Number">'+ response.output.fldTblDoc[i].vchrInvNo +' </span>';
							}
							if( response.output.fldTblDoc[i].vchrInvNo != undefined && response.output.fldTblDoc[i].vchrInvNo.trim() != "" && response.output.fldTblDoc[i].vchrVenId != undefined && response.output.fldTblDoc[i].vchrVenId.trim() != "" ){
								invInfo += ', ';
							}
							if( response.output.fldTblDoc[i].vchrVenId != undefined && response.output.fldTblDoc[i].vchrVenId.trim() != "" ){
								invInfo += '<span title="Vendor ID">'+ response.output.fldTblDoc[i].vchrVenId.trim() +'</span>-'+
											'<span title="Vendor Name">'+ response.output.fldTblDoc[i].vchrVenNm +' </span>';
							}
							invInfo += '</h6>';
						}
					}
					
					if( response.output.fldTblDoc[i].crtDttsStr != undefined || response.output.fldTblDoc[i].upldBy != undefined ){
						if( response.output.fldTblDoc[i].crtDttsStr.trim() != "" || response.output.fldTblDoc[i].upldBy.trim() != "" ){
							invInfo += '<p class="mb-1">';
							if( response.output.fldTblDoc[i].crtDttsStr != undefined && response.output.fldTblDoc[i].crtDttsStr.trim() != "" ){
								invInfo += '<span title="Uploaded On">'+ response.output.fldTblDoc[i].crtDttsStr +' </span>';
							}
							if( response.output.fldTblDoc[i].crtDttsStr != undefined && response.output.fldTblDoc[i].crtDttsStr.trim() != "" && response.output.fldTblDoc[i].upldBy != undefined && response.output.fldTblDoc[i].upldBy.trim() != "" ){
								invInfo += ', ';
							}
							if( response.output.fldTblDoc[i].upldBy != undefined && response.output.fldTblDoc[i].upldBy.trim() != "" ){
								
								if( response.output.fldTblDoc[i].vchrPfx != undefined && response.output.fldTblDoc[i].vchrNo != undefined ){
								
									invInfo += '<span title="Uploaded By">'+ response.output.fldTblDoc[i].upldBy +' </span>';
								}
								else
								{
									invInfo += '<span title="Uploaded By">'+ response.output.fldTblDoc[i].usrNm +' </span>';
								}
							}
							invInfo += '</p>';
						}
					}
					
					if( response.output.fldTblDoc[i].flPdfName != undefined && response.output.fldTblDoc[i].flPdfName.trim() != "" ){
						invInfo += '<p class="mb-0">'+
										'<span title="View File" class="view-doc text-success c-pointer">'+ response.output.fldTblDoc[i].flPdfName +' </span>'+ srcStr +
									'</p>';
						
						commentIcon = '<i class="icon-speech menu-icon icon-pointer invc-comments text-success" data-toggle="tooltip" title="Comments"></i>';
					
						//viewDwnld = '<i class="icon-eye menu-icon view-doc text-primary" aria-hidden="true" alt="File" data-toggle="tooltip" title ="View - '+ response.output.fldTblDoc[i].flPdfName + '""></i>'+ 
										//'&nbsp;<i class="icon-cloud-download menu-icon download-doc text-success" aria-hidden="true" alt="File" data-toggle="tooltip" title="Download - '+ response.output.fldTblDoc[i].flPdfName + '"></i>';
					}
					
					// Added by ABIZAR
					if( response.output.fldTblDoc[i].wrkFlwId != 0)
					{
						invInfo += '<p class="mt-1">'+
										'<span title="Workflow" class="text-primary c-pointer">'+ response.output.fldTblDoc[i].wrkFlw +'</span>'+
										'<span title="Approver" class="text-dark c-pointer">'+ ' (' + response.output.fldTblDoc[i].wrkFlwAprvrNm +') </span>';
						
						if(response.output.fldTblDoc[i].wrkFlwSts == 'S')
						{
							invInfo += '<span class="badge badge-outline-warning wkf-his c-pointer">In Review</span>';
						}
						else if(response.output.fldTblDoc[i].wrkFlwSts == 'R')
						{
							invInfo += '<span class="badge badge-outline-danger wkf-his c-pointer">Rejected</span>';
						}
						else if(response.output.fldTblDoc[i].wrkFlwSts == 'A')
						{
							invInfo += '<span class="badge badge-outline-success wkf-his c-pointer">Approved</span>';
						}
									
						invInfo +='</p>';
					}
					
					if( response.output.fldTblDoc[i].wrkFlwId == 0)
					{
						/*if( response.output.fldTblDoc[i].vchrPfx != undefined && response.output.fldTblDoc[i].vchrNo != undefined ){
						
						invInfo += '<p class="mt-1">Workflow <span class="badge badge-outline-primary">NA</span>';
						}
						else
						{*/
							if(response.output.fldTblDoc[i].wrkFlwAprvr.length == 0)
							{
								if( response.output.fldTblDoc[i].vchrPfx != undefined && response.output.fldTblDoc[i].vchrNo != undefined ){
								
									invInfo += '<p class="mt-1">Workflow <span class="badge badge-outline-primary">NA</span>';
								}
								else
								{
									invInfo += '<p class="mt-1">Workflow <span class="badge badge-outline-primary assign-aprv-badge c-pointer">NA</span>';
								}
									
							}
							else
							{
								invInfo += '<p class="mt-1">'+
										'<span title="Approver" class="text-dark c-pointer">'+ ' (' + response.output.fldTblDoc[i].wrkFlwAprvrNm +') </span>';
							
								if(response.output.fldTblDoc[i].wrkFlwSts == 'S')
								{
									invInfo += '<span class="badge badge-outline-warning wkf-his c-pointer">In Review</span>';
								}
								else if(response.output.fldTblDoc[i].wrkFlwSts == 'R')
								{
									invInfo += '<span class="badge badge-outline-danger wkf-his c-pointer">Rejected</span>';
								}
								else if(response.output.fldTblDoc[i].wrkFlwSts == 'A')
								{
									invInfo += '<span class="badge badge-outline-success wkf-his c-pointer">Approved</span>';
								}
							}
							invInfo +='</p>';
						/*}*/
					}
					
					vchrExtRef = ( response.output.fldTblDoc[i].vchrExtRef != undefined && response.output.fldTblDoc[i].vchrExtRef.trim() != "" ) ? response.output.fldTblDoc[i].vchrExtRef : "";
					
					amtStr = response.output.fldTblDoc[i].vchrAmtStr;
					if( response.output.fldTblDoc[i].vchrCry != undefined && response.output.fldTblDoc[i].vchrCry != null && response.output.fldTblDoc[i].vchrCry != "null" ){
						amtStr += " " + response.output.fldTblDoc[i].vchrCry;
					}
					
					
					tblHtml += '<tr class="'+rowClass+'" data-ctl-no="'+ response.output.fldTblDoc[i].ctlNo +'" data-vchr-pfx="'+ response.output.fldTblDoc[i].vchrPfx +'" data-vchr-no="'+ response.output.fldTblDoc[i].vchrNo +'" data-wkf-id="'+ response.output.fldTblDoc[i].wrkFlwId +'" data-wkf-sts="'+ response.output.fldTblDoc[i].wrkFlwSts +'" data-wkf-aprvr="'+ response.output.fldTblDoc[i].wrkFlwAprvr +'"  data-wkf-back="'+ response.output.fldTblDoc[i].wkfBackSts +'" data-wkf-forw="'+ response.output.fldTblDoc[i].wkfForwSts +'" data-wkf-ext="'+ response.output.fldTblDoc[i].wkfExtSts +'" data-upld-by="'+ response.output.fldTblDoc[i].upldBy +'">'+
									//'<td>'+ viewDwnld +'</td>'+
									//'<td class="text-center">'+ invFrm +'</td>'+
									'<td>'+ invInfo +'</td>'+
									'<td>'+ response.output.fldTblDoc[i].vchrBrh +'</td>'+
									'<td class="text-right">'+  amtStr  +'</td>'+
									'<td>'+ invcDtStr +'</td>'+
									'<td>'+ dueDtStr +'</td>'+
									'<td>'+ vchrNoStr +'</td>'+
									'<td>'+ vchrPoStr +'</td>'+
									/*'<td>'+ vchrCatStr +'</td>'+
									'<td class="trans-pymnt-col">'+ transStsStr +'</td>'+
									'<td class="trans-pymnt-col">'+ pymntStsStr +'</td>'+
									'<td>'+ vchrExtRef +'</td>'+*/
									'<td>'+ editBtn + '&nbsp;'+ commentIcon + '&nbsp;' + holdBtn + '&nbsp;' + deleteBtn +'</td>'+
								'</tr>';
				}
				
				if( pageNo == 0 ){
					$("#retrieveContent table tbody").html( tblHtml );
				}else{
					$("#retrieveContent table tbody").append( tblHtml );
				}
				
				/*$("#retrieveContent table").tablesorter();*/
				
				if( response.output.eof ){
					$("#loadMorePndgInvc").attr("disabled", "disabled");
					$("#loadMorePndgInvc").addClass("disabled");
				}else{
					$("#loadMorePndgInvc").removeAttr("disabled");
					$("#loadMorePndgInvc").removeClass("disabled");
				}
				
				$("#loadMorePndgInvc").attr("data-page-no", parseInt(pageNo)+1);
				if( pageNo == 0 ){
					$("#stxAppInvcFltr").prop("checked", true);
					$("#stxAppInvcFltr").trigger("change");
					{
						$("#loadMorePndgInvc").show();
						$("#retrieveContent table tbody tr.app-invc").addClass("d-none");
						$("#retrieveContent table tbody tr.stx-invc, #retrieveContent table .trans-pymnt-col").removeClass("d-none");
					}
				}
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}


function getConfirmInvc(pageNo=0){
	$("#mainLoader").show();
	
	var year = $(".year-fltr").val();
	if( year == null || year == undefined || year == "" ){
		year = moment().format('YYYY');
	}
	
	var inputData = {
		trnSts : "S",
		pageNo: pageNo,
		year: year,
	}
	
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/cstmdoc/readdoc',
		data : JSON.stringify( inputData ),
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				setInvcTabCounts(response);
				var loopSize = response.output.fldTblDoc.length;
				var tblHtml = '';	
				
				//$("#confirmedInvoice").html( loopSize );
				var invInfo = '', vchrExtRef = "", viewDwnld = '', vchrNoStr = '', vchrPoStr = '', vchrCatStr = '', transStsStr = '', 
				pymntStsStr = '', invFrm = '';
									
				for( var i=0; i<loopSize; i++ ){
					invInfo = '', viewDwnld = '', vchrNoStr = '', vchrPoStr = '', vchrCatStr = '', transStsStr = '', pymntStsStr = '';
					invFrm = '<span class="fill-circle bg-primary"></span>';
					
					if( response.output.fldTblDoc[i].vchrPfx != undefined && response.output.fldTblDoc[i].vchrNo != undefined ){
						vchrNoStr = response.output.fldTblDoc[i].vchrPfx.trim() + "-" + response.output.fldTblDoc[i].vchrNo.trim();
						invFrm = '<span class="fill-circle bg-success"></span>';
					}
					if( response.output.fldTblDoc[i].poPfx != undefined && response.output.fldTblDoc[i].poPfx.trim() != "null" ){
						vchrPoStr = response.output.fldTblDoc[i].poPfx.trim() + "-" + response.output.fldTblDoc[i].poNo.trim() + "-" + response.output.fldTblDoc[i].poItm.trim();
					}
					if( response.output.fldTblDoc[i].vchrCat != undefined ){
						vchrCatStr = response.output.fldTblDoc[i].vchrCat.trim();
					}
					if( response.output.fldTblDoc[i].transSts != undefined ){
						if(response.output.fldTblDoc[i].transSts.trim() == 'H'){
							transStsStr = '<label class="badge badge-danger">Held</label>';
						}else if(response.output.fldTblDoc[i].transSts.trim() == 'C'){
							transStsStr = '<label class="badge badge-success">Completed</label>';
						}else if(response.output.fldTblDoc[i].transSts.trim() == 'A'){
							transStsStr = '<label class="badge badge-primary">Active</label>';
						}else{
							transStsStr = '<label class="badge badge-info">Pending</label>';
						}		
					}
					if( response.output.fldTblDoc[i].pymntSts != undefined ){
						if(response.output.fldTblDoc[i].pymntSts.trim() == 'A' && response.output.fldTblDoc[i].transSts.trim() == 'C'){
							pymntStsStr = '<label class="badge badge-success">Completed</label>';
						}else if(response.output.fldTblDoc[i].pymntSts.trim() == 'H'){
							pymntStsStr = '<label class="badge badge-danger">Held</label>';
						}else if(response.output.fldTblDoc[i].pymntSts.trim() == 'C'){
							pymntStsStr = '<label class="badge badge-success">Completed</label>';
						}else if(response.output.fldTblDoc[i].pymntSts.trim() == 'A'){
							pymntStsStr = '<label class="badge badge-primary">Active</label>';
						}else{
							pymntStsStr = '<label class="badge badge-info">Pending</label>';
						}
					}
					
					if( response.output.fldTblDoc[i].vchrInvNo != undefined || response.output.fldTblDoc[i].vchrVenId != undefined ){
						if( response.output.fldTblDoc[i].vchrInvNo.trim() != "" || response.output.fldTblDoc[i].vchrVenId.trim() != "" ){
							invInfo += '<h6 class="text-primary mb-1">';
							if( response.output.fldTblDoc[i].vchrInvNo != undefined && response.output.fldTblDoc[i].vchrInvNo.trim() != "" ){
								invInfo += '<span title="Invoice Number">'+ response.output.fldTblDoc[i].vchrInvNo +' </span>';
							}
							if( response.output.fldTblDoc[i].vchrInvNo != undefined && response.output.fldTblDoc[i].vchrInvNo.trim() != "" && response.output.fldTblDoc[i].vchrVenId != undefined && response.output.fldTblDoc[i].vchrVenId.trim() != "" ){
								invInfo += ', ';
							}
							if( response.output.fldTblDoc[i].vchrVenId != undefined && response.output.fldTblDoc[i].vchrVenId.trim() != "" ){
								invInfo += '<span title="Vendor ID">'+ response.output.fldTblDoc[i].vchrVenId.trim() +'</span>-'+
											'<span title="Vendor Name">'+ response.output.fldTblDoc[i].vchrVenNm +'</span> ';
							}
							invInfo += '</h6>';
						}
					}
					
					if( response.output.fldTblDoc[i].crtDttsStr != undefined || response.output.fldTblDoc[i].upldBy != undefined ){
						if( response.output.fldTblDoc[i].crtDttsStr.trim() != "" || response.output.fldTblDoc[i].upldBy.trim() != "" ){
							invInfo += '<p class="mb-1">';
							if( response.output.fldTblDoc[i].crtDttsStr != undefined && response.output.fldTblDoc[i].crtDttsStr.trim() != "" ){
								invInfo += '<span title="Uploaded On">'+ response.output.fldTblDoc[i].crtDttsStr +'</span>';
							}
							if( response.output.fldTblDoc[i].crtDttsStr != undefined && response.output.fldTblDoc[i].crtDttsStr.trim() != "" && response.output.fldTblDoc[i].upldBy != undefined && response.output.fldTblDoc[i].upldBy.trim() != "" ){
								invInfo += ', ';
							}
							if( response.output.fldTblDoc[i].upldBy != undefined && response.output.fldTblDoc[i].upldBy.trim() != "" ){
									invInfo += '<span title="Uploaded By">'+ response.output.fldTblDoc[i].upldBy +' </span>';
								
							}
							invInfo += '</p>';
						}
					}
					
					if( response.output.fldTblDoc[i].flPdfName != undefined && response.output.fldTblDoc[i].flPdfName.trim() != "" ){
						invInfo += '<p class="mb-0">'+
										'<span title="View File" class="view-doc text-success c-pointer">'+ response.output.fldTblDoc[i].flPdfName +' </span>'+
									'</p>';
						//viewDwnld = '<i class="icon-eye menu-icon view-doc text-primary" aria-hidden="true" alt="File" data-toggle="tooltip" title ="View - '+ response.output.fldTblDoc[i].flPdfName + '""></i>'+ 
										//'&nbsp;<i class="icon-cloud-download menu-icon download-doc text-success" aria-hidden="true" alt="File" data-toggle="tooltip" title="Download - '+ response.output.fldTblDoc[i].flPdfName + '"></i>';
					}
					
					vchrExtRef = ( response.output.fldTblDoc[i].vchrExtRef != undefined && response.output.fldTblDoc[i].vchrExtRef.trim() != "" ) ? response.output.fldTblDoc[i].vchrExtRef : "";
					
					tblHtml += '<tr data-ctl-no="'+ response.output.fldTblDoc[i].ctlNo +'" data-vchr-pfx="'+ response.output.fldTblDoc[i].vchrPfx +'" data-vchr-no="'+ response.output.fldTblDoc[i].vchrNo +'">'+
									/*'<td>'+ viewDwnld +'</td>'+
									'<td class="text-center">'+ invFrm +'</td>'+*/
									'<td>'+ invInfo +'</td>'+
									'<td>'+ response.output.fldTblDoc[i].vchrBrh +'</td>'+
									'<td class="text-right">'+ response.output.fldTblDoc[i].vchrAmtStr + " " + response.output.fldTblDoc[i].vchrCry +'</td>'+
									'<td>'+ response.output.fldTblDoc[i].vchrInvDtStr +'</td>'+
									'<td>'+ response.output.fldTblDoc[i].vchrDueDtStr +'</td>'+
									'<td>'+ vchrNoStr +'</td>'+
									'<td>'+ vchrPoStr +'</td>'+
									'<td>'+ vchrCatStr +'</td>'+
									'<td>'+ transStsStr +'</td>'+
									'<td>'+ pymntStsStr +'</td>'+
									'<td>'+ vchrExtRef +'</td>'+
									'<td>'+ response.output.fldTblDoc[i].reqId +'</td>'+
								'</tr>';
				}

				if( pageNo == 0 ){
					$("#confirmInvcContent table tbody").html( tblHtml );
				}else{
					$("#confirmInvcContent table tbody").append( tblHtml );
				}
				
				if( response.output.eof ){
					$("#loadMoreConfirmInvc").attr("disabled", "disabled");
					$("#loadMoreConfirmInvc").addClass("disabled");
				}else{
					$("#loadMoreConfirmInvc").removeAttr("disabled");
					$("#loadMoreConfirmInvc").removeClass("disabled");
				}
				
				$("#loadMoreConfirmInvc").attr("data-page-no", parseInt(pageNo)+1);
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}


function getPrcPymntInvc(pageNo=0){
	$("#mainLoader").show();
	
	var year = $(".year-fltr").val();
	if( year == null || year == undefined || year == "" ){
		year = moment().format('YYYY');
	}
	
	var inputData = {
		trnSts : "A",
		pageNo: pageNo,
		year: year,
	}
	
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/cstmdoc/readdoc',
		data : JSON.stringify( inputData ),
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				setInvcTabCounts(response);
				var loopSize = response.output.fldTblDoc.length;
				var tblHtml = '';	
				
				//$("#prcPymntInvoice").html( loopSize );
				var invInfo = '', vchrExtRef = "", viewDwnld = '', vchrNoStr = '', vchrPoStr = '', vchrCatStr = '', transStsStr = '', 
				pymntStsStr = '', invFrm = '', reqId = '', chkNo = '';
									
				for( var i=0; i<loopSize; i++ ){
					invInfo = '', viewDwnld = '', vchrNoStr = '', vchrPoStr = '', vchrCatStr = '', transStsStr = '', pymntStsStr = '';
					invFrm = '<span class="fill-circle bg-primary"></span>';
					
					if( response.output.fldTblDoc[i].vchrPfx != undefined && response.output.fldTblDoc[i].vchrNo != undefined ){
						vchrNoStr = response.output.fldTblDoc[i].vchrPfx.trim() + "-" + response.output.fldTblDoc[i].vchrNo.trim();
						invFrm = '<span class="fill-circle bg-success"></span>';
					}
					if( response.output.fldTblDoc[i].poPfx != undefined && response.output.fldTblDoc[i].poPfx.trim() != "null" ){
						vchrPoStr = response.output.fldTblDoc[i].poPfx.trim() + "-" + response.output.fldTblDoc[i].poNo.trim() + "-" + response.output.fldTblDoc[i].poItm.trim();
					}
					if( response.output.fldTblDoc[i].vchrCat != undefined ){
						vchrCatStr = response.output.fldTblDoc[i].vchrCat.trim();
					}
					if( response.output.fldTblDoc[i].transSts != undefined ){
						if(response.output.fldTblDoc[i].transSts.trim() == 'H'){
							transStsStr = '<label class="badge badge-danger">Held</label>';
						}else if(response.output.fldTblDoc[i].transSts.trim() == 'C'){
							transStsStr = '<label class="badge badge-success">Completed</label>';
						}else if(response.output.fldTblDoc[i].transSts.trim() == 'A'){
							transStsStr = '<label class="badge badge-primary">Active</label>';
						}else{
							transStsStr = '<label class="badge badge-info">Pending</label>';
						}		
					}
					if( response.output.fldTblDoc[i].pymntSts != undefined ){
						if(response.output.fldTblDoc[i].pymntSts.trim() == 'A' && response.output.fldTblDoc[i].transSts.trim() == 'C'){
							pymntStsStr = '<label class="badge badge-success">Completed</label>';
						}else if(response.output.fldTblDoc[i].pymntSts.trim() == 'H'){
							pymntStsStr = '<label class="badge badge-danger">Held</label>';
						}else if(response.output.fldTblDoc[i].pymntSts.trim() == 'C'){
							pymntStsStr = '<label class="badge badge-success">Completed</label>';
						}else if(response.output.fldTblDoc[i].pymntSts.trim() == 'A'){
							pymntStsStr = '<label class="badge badge-primary">Active</label>';
						}else{
							pymntStsStr = '<label class="badge badge-info">Pending</label>';
						}
					}
					
					if( response.output.fldTblDoc[i].vchrInvNo != undefined || response.output.fldTblDoc[i].vchrVenId != undefined ){
						if( response.output.fldTblDoc[i].vchrInvNo.trim() != "" || response.output.fldTblDoc[i].vchrVenId.trim() != "" ){
							invInfo += '<h6 class="text-primary mb-1">';
							if( response.output.fldTblDoc[i].vchrInvNo != undefined && response.output.fldTblDoc[i].vchrInvNo.trim() != "" ){
								invInfo += '<span title="Invoice Number">'+ response.output.fldTblDoc[i].vchrInvNo +' </span>';
							}
							if( response.output.fldTblDoc[i].vchrInvNo != undefined && response.output.fldTblDoc[i].vchrInvNo.trim() != "" && response.output.fldTblDoc[i].vchrVenId != undefined && response.output.fldTblDoc[i].vchrVenId.trim() != "" ){
								invInfo += ', ';
							}
							if( response.output.fldTblDoc[i].vchrVenId != undefined && response.output.fldTblDoc[i].vchrVenId.trim() != "" ){
								invInfo += '<span title="Vendor ID">'+ response.output.fldTblDoc[i].vchrVenId.trim() +' </span>-'+
											'<span title="Vendor Name">'+ response.output.fldTblDoc[i].vchrVenNm +' </span>';
							}
							invInfo += '</h6>';
						}
					}
					
					if( response.output.fldTblDoc[i].crtDttsStr != undefined || response.output.fldTblDoc[i].upldBy != undefined ){
						if( response.output.fldTblDoc[i].crtDttsStr.trim() != "" || response.output.fldTblDoc[i].upldBy.trim() != "" ){
							invInfo += '<p class="mb-1">';
							if( response.output.fldTblDoc[i].crtDttsStr != undefined && response.output.fldTblDoc[i].crtDttsStr.trim() != "" ){
								invInfo += '<span title="Uploaded On">'+ response.output.fldTblDoc[i].crtDttsStr +' </span>';
							}
							if( response.output.fldTblDoc[i].crtDttsStr != undefined && response.output.fldTblDoc[i].crtDttsStr.trim() != "" && response.output.fldTblDoc[i].upldBy != undefined && response.output.fldTblDoc[i].upldBy.trim() != "" ){
								invInfo += ', ';
							}
							if( response.output.fldTblDoc[i].upldBy != undefined && response.output.fldTblDoc[i].upldBy.trim() != "" ){
								invInfo += '<span title="Uploaded By">'+ response.output.fldTblDoc[i].upldBy +' </span>';
							}
							invInfo += '</p>';
						}
					}
					
					if( response.output.fldTblDoc[i].flPdfName != undefined && response.output.fldTblDoc[i].flPdfName.trim() != "" ){
						invInfo += '<p class="mb-0">'+
										'<span title="View File" class="view-doc text-success c-pointer">'+ response.output.fldTblDoc[i].flPdfName +' </span>'+
									'</p>';
						//viewDwnld = '<i class="icon-eye menu-icon view-doc text-primary" aria-hidden="true" alt="File" data-toggle="tooltip" title ="View - '+ response.output.fldTblDoc[i].flPdfName + '""></i>'+ 
										//'&nbsp;<i class="icon-cloud-download menu-icon download-doc text-success" aria-hidden="true" alt="File" data-toggle="tooltip" title="Download - '+ response.output.fldTblDoc[i].flPdfName + '"></i>';
					}
					
					vchrExtRef = ( response.output.fldTblDoc[i].vchrExtRef != undefined && response.output.fldTblDoc[i].vchrExtRef.trim() != "" ) ? response.output.fldTblDoc[i].vchrExtRef : "";
					
					reqId = (response.output.fldTblDoc[i].reqId != undefined) ? response.output.fldTblDoc[i].reqId : "";
					chkNo = (response.output.fldTblDoc[i].chkNo != undefined) ? response.output.fldTblDoc[i].chkNo : "";
					
					tblHtml += '<tr data-ctl-no="'+ response.output.fldTblDoc[i].ctlNo +'" data-vchr-pfx="'+ response.output.fldTblDoc[i].vchrPfx +'" data-vchr-no="'+ response.output.fldTblDoc[i].vchrNo +'">'+
									/*'<td>'+ viewDwnld +'</td>'+
									'<td class="text-center">'+ invFrm +'</td>'+*/
									'<td>'+ invInfo +'</td>'+
									'<td>'+ response.output.fldTblDoc[i].vchrBrh +'</td>'+
									'<td class="text-right">'+ response.output.fldTblDoc[i].vchrAmtStr + " " + response.output.fldTblDoc[i].vchrCry +'</td>'+
									'<td>'+ response.output.fldTblDoc[i].vchrInvDtStr +'</td>'+
									'<td>'+ response.output.fldTblDoc[i].vchrDueDtStr +'</td>'+
									'<td>'+ vchrNoStr +'</td>'+
									'<td>'+ vchrPoStr +'</td>'+
									'<td>'+ vchrCatStr +'</td>'+
									'<td>'+ transStsStr +'</td>'+
									'<td>'+ pymntStsStr +'</td>'+
									'<td>'+ vchrExtRef +'</td>'+
									'<td>'+ reqId +'</td>'+
									'<td>'+ chkNo +'</td>'+
									/*'<td>'+
										'<i class="icon-note menu-icon icon-pointer edit-invc text-primary" title="Edit" data-toggle="tooltip"></i>'+
										//'&nbsp;<i class="icon-ban menu-icon icon-pointer reject-invc text-warning" data-toggle="tooltip" title="Reject"></i>'+
										'&nbsp;<i class="icon-trash menu-icon icon-pointer delete-invc icon-red" data-toggle="tooltip" title="Delete"></i>'+
									'</td>'+*/
								'</tr>';
				}

				if( pageNo == 0 ){
					$("#prcPymntInvcContent table tbody").html( tblHtml );
				}else{
					$("#prcPymntInvcContent table tbody").append( tblHtml );
				}
				
				if( response.output.eof ){
					$("#loadMorePrcPymntInvc").attr("disabled", "disabled");
					$("#loadMorePrcPymntInvc").addClass("disabled");
				}else{
					$("#loadMorePrcPymntInvc").removeAttr("disabled");
					$("#loadMorePrcPymntInvc").removeClass("disabled");
				}
				
				$("#loadMorePrcPymntInvc").attr("data-page-no", parseInt(pageNo)+1);
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}


function getRejectedInvoices(pageNo=0){
	$("#mainLoader").show();
	
	var year = $(".year-fltr").val();
	if( year == null || year == undefined || year == "" ){
		year = moment().format('YYYY');
	}
	
	var inputData = {
		trnSts : "R",
		pageNo: pageNo,
		year: year,
	}
	
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/cstmdoc/readdoc',
		data : JSON.stringify( inputData ),
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
			
				setInvcTabCounts(response);
				var loopSize = response.output.fldTblDoc.length;
				var tblHtml = '', rowClass = '';	
				
				//$("#pendingInvoice").html( loopSize );
				var invInfo = '', vchrExtRef = "", viewDwnld = '', vchrNoStr = '', vchrPoStr = '', vchrCatStr = '', transStsStr = '', 
				pymntStsStr = '', invFrm = '', editBtn = '', amtStr = '', deleteBtn = '', activeBtn = '', invcDtStr = '', dueDtStr = '', 
				srcStr = '', commentIcon = '';
			
				/*setInvcTabCounts(response);
				var loopSize = response.output.fldTblDoc.length;
				var tblHtml = '';*/
				
				//$("#rejectedInvoice").html( loopSize );	
				for( var i=0; i<loopSize; i++ ){
				/*	tblHtml += '<tr data-ctl-no="'+ response.output.fldTblDoc[i].ctlNo +'">'+
									'<td>'+
										'<i class="icon-eye menu-icon view-doc text-primary" aria-hidden="true" alt="File" data-toggle="tooltip" title ="View - '+ response.output.fldTblDoc[i].flPdfName + '""></i>'+ 
										'&nbsp;<i class="icon-cloud-download menu-icon download-doc text-success" aria-hidden="true" alt="File" data-toggle="tooltip" title="Download - '+ response.output.fldTblDoc[i].flPdfName + '"></i>' +
									'</td>'+
									'<td>'+ 
										'<h6 class="text-primary mb-1">'+
											'<span title="Invoice Number">'+ response.output.fldTblDoc[i].vchrInvNo +'</span>, '+
											'<span title="Vendor ID">'+ response.output.fldTblDoc[i].vchrVenId.trim() +'</span>-'+
											'<span title="Vendor Name">'+ response.output.fldTblDoc[i].vchrVenNm +'</span> '+
										'</h6>'+
										'<p class="mb-1">'+
											'<span title="Uploaded On">'+ response.output.fldTblDoc[i].crtDttsStr +'</span>, '+
											'<span title="Uploaded By">'+ response.output.fldTblDoc[i].upldBy +' </span>'+
										'</p>'+
										'<p class="mb-0">'+
											'<span title="File Name">'+ response.output.fldTblDoc[i].flPdfName +' </span>'+
										'</p>'+
									 '</td>'+
									'<td>'+ response.output.fldTblDoc[i].vchrBrh +'</td>'+
									'<td class="text-right">'+ response.output.fldTblDoc[i].vchrAmtStr + " " + response.output.fldTblDoc[i].vchrCry +'</td>'+
									'<td>'+ response.output.fldTblDoc[i].vchrInvDtStr +'</td>'+
									'<td>'+ response.output.fldTblDoc[i].vchrDueDtStr +'</td>'+
									'<td>'+ response.output.fldTblDoc[i].vchrExtRef +'</td>'+
									'<td>'+ response.output.fldTblDoc[i].trnRmk +'</td>'+
								'</tr>';*/
								
								
	
					invcDtStr = (response.output.fldTblDoc[i].vchrInvDtStr!=undefined) ? response.output.fldTblDoc[i].vchrInvDtStr : '';
					dueDtStr = (response.output.fldTblDoc[i].vchrDueDtStr!=undefined) ? response.output.fldTblDoc[i].vchrDueDtStr : '', 
					invInfo = '', viewDwnld = '', vchrNoStr = '', vchrPoStr = '', vchrCatStr = '', transStsStr = '', pymntStsStr = '', srcStr = '';
					invFrm = '<span class="fill-circle bg-primary"></span>';
					rowClass = 'app-invc';
					editBtn = '<i class="icon-note menu-icon icon-pointer edit-invc text-primary" title="Edit" data-toggle="tooltip"></i>';
					deleteBtn = '<i class="icon-trash menu-icon icon-pointer delete-invc icon-red" data-toggle="tooltip" title="Delete"></i>';
					activeBtn = '<i class="icon-lock-open menu-icon icon-pointer unreject-invc text-info" data-toggle="tooltip" title="Active"></i>';
					commentIcon = '';
					
					if( response.output.fldTblDoc[i].srcFlg != undefined && response.output.fldTblDoc[i].srcFlg == "M" ){
						srcStr = '<i class="icon-envelope"></i>';
					}
					if( response.output.fldTblDoc[i].vchrPfx != undefined && response.output.fldTblDoc[i].vchrNo != undefined ){
						vchrNoStr = response.output.fldTblDoc[i].vchrPfx.trim() + "-" + response.output.fldTblDoc[i].vchrNo.trim();
						invFrm = '<span class="fill-circle bg-success"></span>';
						rowClass = 'stx-invc';
						editBtn = '', activeBtn = '';
					}
					if( response.output.fldTblDoc[i].poPfx != undefined && response.output.fldTblDoc[i].poPfx.trim() != "null" ){
						vchrPoStr = response.output.fldTblDoc[i].poPfx.trim() + "-" + response.output.fldTblDoc[i].poNo.trim();
						if( response.output.fldTblDoc[i].poItm.trim() != 0 ){
							vchrPoStr += "-" + response.output.fldTblDoc[i].poItm.trim();
						} 
					}
					if( response.output.fldTblDoc[i].vchrCat != undefined ){
						vchrCatStr = response.output.fldTblDoc[i].vchrCat.trim();
					}
					if( response.output.fldTblDoc[i].transSts != undefined ){
						if(response.output.fldTblDoc[i].transSts.trim() == 'H'){
							transStsStr = '<label class="badge badge-danger">Held</label>';
						}else if(response.output.fldTblDoc[i].transSts.trim() == 'C'){
							transStsStr = '<label class="badge badge-success">Completed</label>';
						}else if(response.output.fldTblDoc[i].transSts.trim() == 'A'){
							transStsStr = '<label class="badge badge-primary">Active</label>';
						}else{
							transStsStr = '<label class="badge badge-info">Pending</label>';
						}		
					}
					if( response.output.fldTblDoc[i].pymntSts != undefined ){
						if(response.output.fldTblDoc[i].pymntSts.trim() == 'A' && response.output.fldTblDoc[i].transSts.trim() == 'C'){
							pymntStsStr = '<label class="badge badge-success">Completed</label>';
						}else if(response.output.fldTblDoc[i].pymntSts.trim() == 'H'){
							pymntStsStr = '<label class="badge badge-danger">Held</label>';
						}else if(response.output.fldTblDoc[i].pymntSts.trim() == 'C'){
							pymntStsStr = '<label class="badge badge-success">Completed</label>';
						}else if(response.output.fldTblDoc[i].pymntSts.trim() == 'A'){
							pymntStsStr = '<label class="badge badge-primary">Active</label>';
						}else{
							pymntStsStr = '<label class="badge badge-info">Pending</label>';
						}
					}
					
					if( response.output.fldTblDoc[i].transSts != undefined && response.output.fldTblDoc[i].pymntSts != undefined ){
						/*if(response.output.fldTblDoc[i].transSts.trim() == 'C' && response.output.fldTblDoc[i].pymntSts.trim() == 'C'){
							deleteBtn = '';
						}*/
						
						if(response.output.fldTblDoc[i].transSts.trim() == 'C'){
							deleteBtn = '';
						}
					}
					
					if( response.output.fldTblDoc[i].vchrInvNo != undefined || response.output.fldTblDoc[i].vchrVenId != undefined ){
						if( response.output.fldTblDoc[i].vchrInvNo.trim() != "" || response.output.fldTblDoc[i].vchrVenId.trim() != "" ){
							invInfo += '<h6 class="text-primary mb-1">';
							if( response.output.fldTblDoc[i].vchrInvNo != undefined && response.output.fldTblDoc[i].vchrInvNo.trim() != "" ){
								invInfo += '<span title="Invoice Number">'+ response.output.fldTblDoc[i].vchrInvNo +' </span>';
							}
							if( response.output.fldTblDoc[i].vchrInvNo != undefined && response.output.fldTblDoc[i].vchrInvNo.trim() != "" && response.output.fldTblDoc[i].vchrVenId != undefined && response.output.fldTblDoc[i].vchrVenId.trim() != "" ){
								invInfo += ', ';
							}
							if( response.output.fldTblDoc[i].vchrVenId != undefined && response.output.fldTblDoc[i].vchrVenId.trim() != "" ){
								invInfo += '<span title="Vendor ID">'+ response.output.fldTblDoc[i].vchrVenId.trim() +'</span>-'+
											'<span title="Vendor Name">'+ response.output.fldTblDoc[i].vchrVenNm +' </span>';
							}
							invInfo += '</h6>';
						}
					}
					
					if( response.output.fldTblDoc[i].crtDttsStr != undefined || response.output.fldTblDoc[i].upldBy != undefined ){
						if( response.output.fldTblDoc[i].crtDttsStr.trim() != "" || response.output.fldTblDoc[i].upldBy.trim() != "" ){
							invInfo += '<p class="mb-1">';
							if( response.output.fldTblDoc[i].crtDttsStr != undefined && response.output.fldTblDoc[i].crtDttsStr.trim() != "" ){
								invInfo += '<span title="Uploaded On">'+ response.output.fldTblDoc[i].crtDttsStr +' </span>';
							}
							if( response.output.fldTblDoc[i].crtDttsStr != undefined && response.output.fldTblDoc[i].crtDttsStr.trim() != "" && response.output.fldTblDoc[i].upldBy != undefined && response.output.fldTblDoc[i].upldBy.trim() != "" ){
								invInfo += ', ';
							}
							if( response.output.fldTblDoc[i].upldBy != undefined && response.output.fldTblDoc[i].upldBy.trim() != "" ){
								
								if( response.output.fldTblDoc[i].vchrPfx != undefined && response.output.fldTblDoc[i].vchrNo != undefined ){
								
									invInfo += '<span title="Uploaded By">'+ response.output.fldTblDoc[i].upldBy +' </span>';
								}
								else
								{
									invInfo += '<span title="Uploaded By">'+ response.output.fldTblDoc[i].usrNm +' </span>';
								}
							}
							invInfo += '</p>';
						}
					}
					
					if( response.output.fldTblDoc[i].flPdfName != undefined && response.output.fldTblDoc[i].flPdfName.trim() != "" ){
						invInfo += '<p class="mb-0">'+
										'<span title="View File" class="view-doc text-success c-pointer">'+ response.output.fldTblDoc[i].flPdfName +' </span>'+ srcStr +
									'</p>';
						
						commentIcon = '<i class="icon-speech menu-icon icon-pointer invc-comments text-success" data-toggle="tooltip" title="Comments"></i>';
					
						//viewDwnld = '<i class="icon-eye menu-icon view-doc text-primary" aria-hidden="true" alt="File" data-toggle="tooltip" title ="View - '+ response.output.fldTblDoc[i].flPdfName + '""></i>'+ 
										//'&nbsp;<i class="icon-cloud-download menu-icon download-doc text-success" aria-hidden="true" alt="File" data-toggle="tooltip" title="Download - '+ response.output.fldTblDoc[i].flPdfName + '"></i>';
					}
					
					// Added by ABIZAR
					if( response.output.fldTblDoc[i].wrkFlwId != 0)
					{
						invInfo += '<p class="mt-1">'+
										'<span title="Workflow" class="text-primary c-pointer">'+ response.output.fldTblDoc[i].wrkFlw +'</span>'+
										'<span title="Approver" class="text-dark c-pointer">'+ ' (' + response.output.fldTblDoc[i].wrkFlwAprvrNm +') </span>';
						
						if(response.output.fldTblDoc[i].wrkFlwSts == 'S')
						{
							invInfo += '<span class="badge badge-outline-warning wkf-his c-pointer">In Review</span>';
						}
						else if(response.output.fldTblDoc[i].wrkFlwSts == 'R')
						{
							invInfo += '<span class="badge badge-outline-danger wkf-his c-pointer">Rejected</span>';
						}
						else if(response.output.fldTblDoc[i].wrkFlwSts == 'A')
						{
							invInfo += '<span class="badge badge-outline-success wkf-his c-pointer">Approved</span>';
						}
									
						invInfo +='</p>';
					}
					
					if( response.output.fldTblDoc[i].wrkFlwId == 0)
					{
						if( response.output.fldTblDoc[i].vchrPfx != undefined && response.output.fldTblDoc[i].vchrNo != undefined ){
						
						invInfo += '<p class="mt-1">Workflow <span class="badge badge-outline-primary">NA</span>';
						}
						else
						{
							if(response.output.fldTblDoc[i].wrkFlwAprvr.length == 0)
							{
								invInfo += '<p class="mt-1">Workflow <span class="badge badge-outline-primary assign-aprv-badge c-pointer">NA</span>';
							}
							else
							{
								invInfo += '<p class="mt-1">'+
										'<span title="Approver" class="text-dark c-pointer">'+ ' (' + response.output.fldTblDoc[i].wrkFlwAprvrNm +') </span>';
							
								if(response.output.fldTblDoc[i].wrkFlwSts == 'S')
								{
									invInfo += '<span class="badge badge-outline-warning wkf-his c-pointer">In Review</span>';
								}
								else if(response.output.fldTblDoc[i].wrkFlwSts == 'R')
								{
									invInfo += '<span class="badge badge-outline-danger wkf-his c-pointer">Rejected</span>';
								}
								else if(response.output.fldTblDoc[i].wrkFlwSts == 'A')
								{
									invInfo += '<span class="badge badge-outline-success wkf-his c-pointer">Approved</span>';
								}
							}
							invInfo +='</p>';
						}
					}
					
					vchrExtRef = ( response.output.fldTblDoc[i].vchrExtRef != undefined && response.output.fldTblDoc[i].vchrExtRef.trim() != "" ) ? response.output.fldTblDoc[i].vchrExtRef : "";
					
					amtStr = response.output.fldTblDoc[i].vchrAmtStr;
					if( response.output.fldTblDoc[i].vchrCry != undefined && response.output.fldTblDoc[i].vchrCry != null && response.output.fldTblDoc[i].vchrCry != "null" ){
						amtStr += " " + response.output.fldTblDoc[i].vchrCry;
					}
					
					
					tblHtml += '<tr class="'+rowClass+'" data-ctl-no="'+ response.output.fldTblDoc[i].ctlNo +'" data-vchr-pfx="'+ response.output.fldTblDoc[i].vchrPfx +'" data-vchr-no="'+ response.output.fldTblDoc[i].vchrNo +'" data-wkf-id="'+ response.output.fldTblDoc[i].wrkFlwId +'" data-wkf-sts="'+ response.output.fldTblDoc[i].wrkFlwSts +'" data-wkf-aprvr="'+ response.output.fldTblDoc[i].wrkFlwAprvr +'"  data-wkf-back="'+ response.output.fldTblDoc[i].wkfBackSts +'" data-wkf-forw="'+ response.output.fldTblDoc[i].wkfForwSts +'" data-wkf-ext="'+ response.output.fldTblDoc[i].wkfExtSts +'" data-upld-by="'+ response.output.fldTblDoc[i].upldBy +'">'+
									//'<td>'+ viewDwnld +'</td>'+
									'<td class="text-center">'+ invFrm +'</td>'+
									'<td>'+ invInfo +'</td>'+
									'<td>'+ response.output.fldTblDoc[i].vchrBrh +'</td>'+
									'<td class="text-right">'+  amtStr  +'</td>'+
									'<td>'+ invcDtStr +'</td>'+
									'<td>'+ dueDtStr +'</td>'+
									'<td>'+ vchrNoStr +'</td>'+
									'<td>'+ vchrPoStr +'</td>'+
									'<td>'+ vchrCatStr +'</td>'+
									'<td>'+ transStsStr +'</td>'+
									'<td>'+ pymntStsStr +'</td>'+
									'<td>'+ vchrExtRef +'</td>'+
									'<td>'+ editBtn + '&nbsp;'+ commentIcon + '&nbsp;' + activeBtn + '&nbsp;' + deleteBtn +'</td>'+
								'</tr>';
				}

				if( pageNo == 0 ){
					$("#rejectedInvcWrapper table tbody").html( tblHtml );
				}else{
					$("#rejectedInvcWrapper table tbody").append( tblHtml );
				}
				
				if( response.output.eof ){
					$("#loadMoreRejectedInvc").attr("disabled", "disabled");
					$("#loadMoreRejectedInvc").addClass("disabled");
				}else{
					$("#loadMoreRejectedInvc").removeAttr("disabled");
					$("#loadMoreRejectedInvc").removeClass("disabled");
				}
				
				$("#loadMoreRejectedInvc").attr("data-page-no", parseInt(pageNo)+1);
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}


function getHoldInvoices(pageNo=0){
	$("#mainLoader").show();
	
	var year = $(".year-fltr").val();
	if( year == null || year == undefined || year == "" ){
		year = moment().format('YYYY');
	}
	
	var inputData = {
		trnSts : "H",
		pageNo: pageNo,
		year: year,
	}
	
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/cstmdoc/readdoc',
		data : JSON.stringify( inputData ),
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				setInvcTabCounts(response);
				var loopSize = response.output.fldTblDoc.length;
				var tblHtml = '', rowClass = '';
				
				var invInfo = '', vchrExtRef = "", viewDwnld = '', vchrNoStr = '', vchrPoStr = '', vchrCatStr = '', transStsStr = '', 
				pymntStsStr = '', invFrm = '', editBtn = '', amtStr = '', deleteBtn = '', invcDtStr = '', dueDtStr = '', commentIcon = '';
				
				for( var i=0; i<loopSize; i++ ){
	
					invcDtStr = (response.output.fldTblDoc[i].vchrInvDtStr!=undefined) ? response.output.fldTblDoc[i].vchrInvDtStr : '';
					dueDtStr = (response.output.fldTblDoc[i].vchrDueDtStr!=undefined) ? response.output.fldTblDoc[i].vchrDueDtStr : '', 
					invInfo = '', viewDwnld = '', vchrNoStr = '', vchrPoStr = '', vchrCatStr = '', transStsStr = '', pymntStsStr = '';
					invFrm = '<span class="fill-circle bg-primary"></span>';
					rowClass = 'app-invc';
					editBtn = '<i class="icon-eye menu-icon icon-pointer edit-invc edit-hold-invc text-primary" title="View" data-toggle="tooltip"></i>';
					holdBtn = '<i class="icon-clock menu-icon icon-pointer unhold-invc icon-red" title="Remove from Hold" data-toggle="tooltip"></i>';
					deleteBtn = '<i class="icon-trash menu-icon icon-pointer delete-invc icon-red" data-toggle="tooltip" title="Delete"></i>';
					commentIcon = '<i class="icon-speech menu-icon icon-pointer invc-comments text-success" data-toggle="tooltip" title="Comments"></i>';
					
					if( response.output.fldTblDoc[i].vchrPfx != undefined && response.output.fldTblDoc[i].vchrNo != undefined ){
						vchrNoStr = response.output.fldTblDoc[i].vchrPfx.trim() + "-" + response.output.fldTblDoc[i].vchrNo.trim();
						invFrm = '<span class="fill-circle bg-success"></span>';
						rowClass = 'stx-invc';
						editBtn = '';
					}
					if( response.output.fldTblDoc[i].poPfx != undefined && response.output.fldTblDoc[i].poPfx.trim() != "null" ){
						vchrPoStr = response.output.fldTblDoc[i].poPfx.trim() + "-" + response.output.fldTblDoc[i].poNo.trim();
						if( response.output.fldTblDoc[i].poItm.trim() != 0 ){
							vchrPoStr += "-" + response.output.fldTblDoc[i].poItm.trim();
						} 
					}
					if( response.output.fldTblDoc[i].vchrCat != undefined ){
						vchrCatStr = response.output.fldTblDoc[i].vchrCat.trim();
					}
					if( response.output.fldTblDoc[i].transSts != undefined ){
						if(response.output.fldTblDoc[i].transSts.trim() == 'H'){
							transStsStr = '<label class="badge badge-danger">Held</label>';
						}else if(response.output.fldTblDoc[i].transSts.trim() == 'C'){
							transStsStr = '<label class="badge badge-success">Completed</label>';
						}else if(response.output.fldTblDoc[i].transSts.trim() == 'A'){
							transStsStr = '<label class="badge badge-primary">Active</label>';
						}else{
							transStsStr = '<label class="badge badge-info">Pending</label>';
						}		
					}
					if( response.output.fldTblDoc[i].pymntSts != undefined ){
						if(response.output.fldTblDoc[i].pymntSts.trim() == 'A' && response.output.fldTblDoc[i].transSts.trim() == 'C'){
							pymntStsStr = '<label class="badge badge-success">Completed</label>';
						}else if(response.output.fldTblDoc[i].pymntSts.trim() == 'H'){
							pymntStsStr = '<label class="badge badge-danger">Held</label>';
						}else if(response.output.fldTblDoc[i].pymntSts.trim() == 'C'){
							pymntStsStr = '<label class="badge badge-success">Completed</label>';
						}else if(response.output.fldTblDoc[i].pymntSts.trim() == 'A'){
							pymntStsStr = '<label class="badge badge-primary">Active</label>';
						}else{
							pymntStsStr = '<label class="badge badge-info">Pending</label>';
						}
					}
					
					if( response.output.fldTblDoc[i].transSts != undefined && response.output.fldTblDoc[i].pymntSts != undefined ){
						if(response.output.fldTblDoc[i].transSts.trim() == 'C' && response.output.fldTblDoc[i].pymntSts.trim() == 'C'){
							deleteBtn = '';
						}
					}
					
					if( response.output.fldTblDoc[i].vchrInvNo != undefined || response.output.fldTblDoc[i].vchrVenId != undefined ){
						if( response.output.fldTblDoc[i].vchrInvNo.trim() != "" || response.output.fldTblDoc[i].vchrVenId.trim() != "" ){
							invInfo += '<h6 class="text-primary mb-1">';
							if( response.output.fldTblDoc[i].vchrInvNo != undefined && response.output.fldTblDoc[i].vchrInvNo.trim() != "" ){
								invInfo += '<span title="Invoice Number">'+ response.output.fldTblDoc[i].vchrInvNo +' </span>';
							}
							if( response.output.fldTblDoc[i].vchrInvNo != undefined && response.output.fldTblDoc[i].vchrInvNo.trim() != "" && response.output.fldTblDoc[i].vchrVenId != undefined && response.output.fldTblDoc[i].vchrVenId.trim() != "" ){
								invInfo += ', ';
							}
							if( response.output.fldTblDoc[i].vchrVenId != undefined && response.output.fldTblDoc[i].vchrVenId.trim() != "" ){
								invInfo += '<span title="Vendor ID">'+ response.output.fldTblDoc[i].vchrVenId.trim() +'</span>-'+
											'<span title="Vendor Name">'+ response.output.fldTblDoc[i].vchrVenNm +' </span>';
							}
							invInfo += '</h6>';
						}
					}
					
					if( response.output.fldTblDoc[i].crtDttsStr != undefined || response.output.fldTblDoc[i].upldBy != undefined ){
						if( response.output.fldTblDoc[i].crtDttsStr.trim() != "" || response.output.fldTblDoc[i].upldBy.trim() != "" ){
							invInfo += '<p class="mb-1">';
							if( response.output.fldTblDoc[i].crtDttsStr != undefined && response.output.fldTblDoc[i].crtDttsStr.trim() != "" ){
								invInfo += '<span title="Uploaded On">'+ response.output.fldTblDoc[i].crtDttsStr +' </span>';
							}
							if( response.output.fldTblDoc[i].crtDttsStr != undefined && response.output.fldTblDoc[i].crtDttsStr.trim() != "" && response.output.fldTblDoc[i].upldBy != undefined && response.output.fldTblDoc[i].upldBy.trim() != "" ){
								invInfo += ', ';
							}
							if( response.output.fldTblDoc[i].upldBy != undefined && response.output.fldTblDoc[i].upldBy.trim() != "" ){
								
								if( response.output.fldTblDoc[i].vchrPfx != undefined && response.output.fldTblDoc[i].vchrNo != undefined ){
								
									invInfo += '<span title="Uploaded By">'+ response.output.fldTblDoc[i].upldBy +' </span>';
								}
								else
								{
									invInfo += '<span title="Uploaded By">'+ response.output.fldTblDoc[i].usrNm +' </span>';
								}
							}
							invInfo += '</p>';
						}
					}
					
					if( response.output.fldTblDoc[i].flPdfName != undefined && response.output.fldTblDoc[i].flPdfName.trim() != "" ){
						invInfo += '<p class="mb-0">'+
										'<span title="View File" class="view-doc text-success c-pointer">'+ response.output.fldTblDoc[i].flPdfName +' </span>'+
									'</p>';
						//viewDwnld = '<i class="icon-eye menu-icon view-doc text-primary" aria-hidden="true" alt="File" data-toggle="tooltip" title ="View - '+ response.output.fldTblDoc[i].flPdfName + '""></i>'+ 
										//'&nbsp;<i class="icon-cloud-download menu-icon download-doc text-success" aria-hidden="true" alt="File" data-toggle="tooltip" title="Download - '+ response.output.fldTblDoc[i].flPdfName + '"></i>';
					}
					
					// Added by ABIZAR
					if( response.output.fldTblDoc[i].wrkFlwId != 0)
					{
						invInfo += '<p class="mt-1">'+
										'<span title="Workflow" class="text-primary c-pointer">'+ response.output.fldTblDoc[i].wrkFlw +'</span>'+
										'<span title="Approver" class="text-dark c-pointer">'+ '(' + response.output.fldTblDoc[i].wrkFlwAprvrNm +') </span>';
						
						if(response.output.fldTblDoc[i].wrkFlwSts == 'S')
						{
							invInfo += '<span class="badge badge-outline-warning wkf-his c-pointer">In Review</span>';
						}
						else if(response.output.fldTblDoc[i].wrkFlwSts == 'R')
						{
							invInfo += '<span class="badge badge-outline-danger wkf-his c-pointer">Rejected</span>';
						}
						else if(response.output.fldTblDoc[i].wrkFlwSts == 'A')
						{
							invInfo += '<span class="badge badge-outline-success wkf-his c-pointer">Approved</span>';
						}
									
						invInfo +='</p>';
					}
					
					if( response.output.fldTblDoc[i].wrkFlwId == 0)
					{
						invInfo += '<p class="mt-1">Workflow <span class="badge badge-outline-primary">NA</span>';
					}
					
					vchrExtRef = ( response.output.fldTblDoc[i].vchrExtRef != undefined && response.output.fldTblDoc[i].vchrExtRef.trim() != "" ) ? response.output.fldTblDoc[i].vchrExtRef : "";
					
					amtStr = response.output.fldTblDoc[i].vchrAmtStr;
					if( response.output.fldTblDoc[i].vchrCry != undefined && response.output.fldTblDoc[i].vchrCry != null && response.output.fldTblDoc[i].vchrCry != "null" ){
						amtStr += " " + response.output.fldTblDoc[i].vchrCry;
					}
					
					
					tblHtml += '<tr class="'+rowClass+'" data-ctl-no="'+ response.output.fldTblDoc[i].ctlNo +'" data-vchr-pfx="'+ response.output.fldTblDoc[i].vchrPfx +'" data-vchr-no="'+ response.output.fldTblDoc[i].vchrNo +'" data-wkf-id="'+ response.output.fldTblDoc[i].wrkFlwId +'" data-wkf-sts="'+ response.output.fldTblDoc[i].wrkFlwSts +'" data-wkf-aprvr="'+ response.output.fldTblDoc[i].wrkFlwAprvr +'" data-upld-by="'+ response.output.fldTblDoc[i].upldBy +'">'+
									//'<td>'+ viewDwnld +'</td>'+
									'<td class="text-center">'+ invFrm +'</td>'+
									'<td>'+ invInfo +'</td>'+
									'<td>'+ response.output.fldTblDoc[i].vchrBrh +'</td>'+
									'<td class="text-right">'+  amtStr  +'</td>'+
									'<td>'+ invcDtStr +'</td>'+
									'<td>'+ dueDtStr +'</td>'+
									'<td>'+ vchrNoStr +'</td>'+
									'<td>'+ vchrPoStr +'</td>'+
									'<td>'+ vchrCatStr +'</td>'+
									'<td>'+ vchrExtRef +'</td>'+
									'<td style="max-width:250px">'+ response.output.fldTblDoc[i].trnRmk +'</td>'+
									'<td>'+ editBtn  + '&nbsp;'+ commentIcon + '&nbsp;'+ holdBtn + '&nbsp;'+ deleteBtn +'</td>'+
								'</tr>';
				}

				if( pageNo == 0 ){
					$("#holdInvcWrapper table tbody").html( tblHtml );
				}else{
					$("#holdInvcWrapper table tbody").append( tblHtml );
				}
				
				if( response.output.eof ){
					$("#loadMoreHoldInvc").attr("disabled", "disabled");
					$("#loadMoreHoldInvc").addClass("disabled");
				}else{
					$("#loadMoreHoldInvc").removeAttr("disabled");
					$("#loadMoreHoldInvc").removeClass("disabled");
				}
				
				$("#loadMoreHoldInvc").attr("data-page-no", parseInt(pageNo)+1);
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}


function getCompletedInvc(pageNo=0){
	$("#mainLoader").show();
	
	var year = $(".year-fltr").val();
	if( year == null || year == undefined || year == "" ){
		year = moment().format('YYYY');
	}
	
	var inputData = {
		trnSts : "C",
		pageNo: pageNo,
		year: year,
	}
	
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/cstmdoc/readdoc',
		data : JSON.stringify( inputData ),
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				setInvcTabCounts(response);
				var loopSize = response.output.fldTblDoc.length;
				var tblHtml = '';	
				
				//$("#prcPymntInvoice").html( loopSize );
				var invInfo = '', vchrExtRef = "", viewDwnld = '', vchrNoStr = '', vchrPoStr = '', vchrCatStr = '', transStsStr = '', 
				pymntStsStr = '', invFrm = '', reqId = '', chkNo = '';
									
				for( var i=0; i<loopSize; i++ ){
					invInfo = '', viewDwnld = '', vchrNoStr = '', vchrPoStr = '', vchrCatStr = '', transStsStr = '', pymntStsStr = '';
					invFrm = '<span class="fill-circle bg-primary"></span>';
					
					if( response.output.fldTblDoc[i].vchrPfx != undefined && response.output.fldTblDoc[i].vchrNo != undefined ){
						vchrNoStr = response.output.fldTblDoc[i].vchrPfx.trim() + "-" + response.output.fldTblDoc[i].vchrNo.trim();
						invFrm = '<span class="fill-circle bg-success"></span>';
					}
					if( response.output.fldTblDoc[i].poPfx != undefined && response.output.fldTblDoc[i].poPfx.trim() != "null" ){
						vchrPoStr = response.output.fldTblDoc[i].poPfx.trim() + "-" + response.output.fldTblDoc[i].poNo.trim() + "-" + response.output.fldTblDoc[i].poItm.trim();
					}
					if( response.output.fldTblDoc[i].vchrCat != undefined ){
						vchrCatStr = response.output.fldTblDoc[i].vchrCat.trim();
					}
					if( response.output.fldTblDoc[i].transSts != undefined ){
						if(response.output.fldTblDoc[i].transSts.trim() == 'H'){
							transStsStr = '<label class="badge badge-danger">Held</label>';
						}else if(response.output.fldTblDoc[i].transSts.trim() == 'C'){
							transStsStr = '<label class="badge badge-success">Completed</label>';
						}else if(response.output.fldTblDoc[i].transSts.trim() == 'A'){
							transStsStr = '<label class="badge badge-primary">Active</label>';
						}else{
							transStsStr = '<label class="badge badge-info">Pending</label>';
						}		
					}
					if( response.output.fldTblDoc[i].pymntSts != undefined ){
						if(response.output.fldTblDoc[i].pymntSts.trim() == 'A' && response.output.fldTblDoc[i].transSts.trim() == 'C'){
							pymntStsStr = '<label class="badge badge-success">Completed</label>';
						}else if(response.output.fldTblDoc[i].pymntSts.trim() == 'H'){
							pymntStsStr = '<label class="badge badge-danger">Held</label>';
						}else if(response.output.fldTblDoc[i].pymntSts.trim() == 'C'){
							pymntStsStr = '<label class="badge badge-success">Completed</label>';
						}else if(response.output.fldTblDoc[i].pymntSts.trim() == 'A'){
							pymntStsStr = '<label class="badge badge-primary">Active</label>';
						}else{
							pymntStsStr = '<label class="badge badge-info">Pending</label>';
						}
					}
					
					if( response.output.fldTblDoc[i].vchrInvNo != undefined || response.output.fldTblDoc[i].vchrVenId != undefined ){
						if( response.output.fldTblDoc[i].vchrInvNo.trim() != "" || response.output.fldTblDoc[i].vchrVenId.trim() != "" ){
							invInfo += '<h6 class="text-primary mb-1">';
							if( response.output.fldTblDoc[i].vchrInvNo != undefined && response.output.fldTblDoc[i].vchrInvNo.trim() != "" ){
								invInfo += '<span title="Invoice Number">'+ response.output.fldTblDoc[i].vchrInvNo +'</span>';
							}
							if( response.output.fldTblDoc[i].vchrInvNo != undefined && response.output.fldTblDoc[i].vchrInvNo.trim() != "" && response.output.fldTblDoc[i].vchrVenId != undefined && response.output.fldTblDoc[i].vchrVenId.trim() != "" ){
								invInfo += ', ';
							}
							if( response.output.fldTblDoc[i].vchrVenId != undefined && response.output.fldTblDoc[i].vchrVenId.trim() != "" ){
								invInfo += '<span title="Vendor ID">'+ response.output.fldTblDoc[i].vchrVenId.trim() +'</span>-'+
											'<span title="Vendor Name">'+ response.output.fldTblDoc[i].vchrVenNm +' </span>';
							}
							invInfo += '</h6>';
						}
					}
					
					if( response.output.fldTblDoc[i].crtDttsStr != undefined || response.output.fldTblDoc[i].upldBy != undefined ){
						if( response.output.fldTblDoc[i].crtDttsStr.trim() != "" || response.output.fldTblDoc[i].upldBy.trim() != "" ){
							invInfo += '<p class="mb-1">';
							if( response.output.fldTblDoc[i].crtDttsStr != undefined && response.output.fldTblDoc[i].crtDttsStr.trim() != "" ){
								invInfo += '<span title="Uploaded On">'+ response.output.fldTblDoc[i].crtDttsStr +'</span>';
							}
							if( response.output.fldTblDoc[i].crtDttsStr != undefined && response.output.fldTblDoc[i].crtDttsStr.trim() != "" && response.output.fldTblDoc[i].upldBy != undefined && response.output.fldTblDoc[i].upldBy.trim() != "" ){
								invInfo += ', ';
							}
							if( response.output.fldTblDoc[i].upldBy != undefined && response.output.fldTblDoc[i].upldBy.trim() != "" ){
								invInfo += '<span title="Uploaded By">'+ response.output.fldTblDoc[i].upldBy +' </span>';
							}
							invInfo += '</p>';
						}
					}
					
					if( response.output.fldTblDoc[i].flPdfName != undefined && response.output.fldTblDoc[i].flPdfName.trim() != "" ){
						invInfo += '<p class="mb-0">'+
										'<span title="View File" class="view-doc text-success c-pointer">'+ response.output.fldTblDoc[i].flPdfName +' </span>'+
									'</p>';
						//viewDwnld = '<i class="icon-eye menu-icon view-doc text-primary" aria-hidden="true" alt="File" data-toggle="tooltip" title ="View - '+ response.output.fldTblDoc[i].flPdfName + '""></i>'+ 
										//'&nbsp;<i class="icon-cloud-download menu-icon download-doc text-success" aria-hidden="true" alt="File" data-toggle="tooltip" title="Download - '+ response.output.fldTblDoc[i].flPdfName + '"></i>';
					}
					
					vchrExtRef = ( response.output.fldTblDoc[i].vchrExtRef != undefined && response.output.fldTblDoc[i].vchrExtRef.trim() != "" ) ? response.output.fldTblDoc[i].vchrExtRef : "";
					
					reqId = (response.output.fldTblDoc[i].reqId != undefined) ? response.output.fldTblDoc[i].reqId : "";
					chkNo = (response.output.fldTblDoc[i].chkNo != undefined) ? response.output.fldTblDoc[i].chkNo : "";
					
					tblHtml += '<tr data-ctl-no="'+ response.output.fldTblDoc[i].ctlNo +'" data-vchr-pfx="'+ response.output.fldTblDoc[i].vchrPfx +'" data-vchr-no="'+ response.output.fldTblDoc[i].vchrNo +'">'+
									/*'<td>'+ viewDwnld +'</td>'+
									'<td class="text-center">'+ invFrm +'</td>'+*/
									'<td>'+ invInfo +'</td>'+
									'<td>'+ response.output.fldTblDoc[i].vchrBrh +'</td>'+
									'<td class="text-right">'+ response.output.fldTblDoc[i].vchrAmtStr + " " + response.output.fldTblDoc[i].vchrCry +'</td>'+
									'<td>'+ response.output.fldTblDoc[i].vchrInvDtStr +'</td>'+
									'<td>'+ response.output.fldTblDoc[i].vchrDueDtStr +'</td>'+
									'<td>'+ vchrNoStr +'</td>'+
									/*'<td>'+ vchrPoStr +'</td>'+*/
									/*'<td>'+ vchrCatStr +'</td>'+*/
								/*	'<td>'+ transStsStr +'</td>'+
									'<td>'+ pymntStsStr +'</td>'+*/
									/*'<td>'+ vchrExtRef +'</td>'+
									'<td>'+ reqId +'</td>'+
									'<td>'+ chkNo +'</td>'+*/
									/*'<td>'+
										'<i class="icon-note menu-icon icon-pointer edit-invc text-primary" title="Edit" data-toggle="tooltip"></i>'+
										//'&nbsp;<i class="icon-ban menu-icon icon-pointer reject-invc text-warning" data-toggle="tooltip" title="Reject"></i>'+
										'&nbsp;<i class="icon-trash menu-icon icon-pointer delete-invc icon-red" data-toggle="tooltip" title="Delete"></i>'+
									'</td>'+*/
								'</tr>';
				}

				if( pageNo == 0 ){
					$("#compInvcContent table tbody").html( tblHtml );
				}else{
					$("#compInvcContent table tbody").append( tblHtml );
				}
				
				if( response.output.eof ){
					$("#loadMoreCompInvc").attr("disabled", "disabled");
					$("#loadMoreCompInvc").addClass("disabled");
				}else{
					$("#loadMoreCompInvc").removeAttr("disabled");
					$("#loadMoreCompInvc").removeClass("disabled");
				}
				
				$("#loadMoreCompInvc").attr("data-page-no", parseInt(pageNo)+1);
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}


function getInPrcInvcPymtSts(){
	var loader = '<div class="box-loader d-flex align-items-center" id="inprocessLoader">'+
					'<div class="circle-loader"></div>'+
				'</div>';
	$("#tabPaneInProcess").html(loader);								
																		
	var year = $(".year-fltr").val();
	if( year == null || year == undefined || year == "" ){
		year = moment().format('YYYY');
	}
	
	var inputData = { 
		trnSts: "",
		year: year,
		pageNo: 0,
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/cstmdoc/readdoc',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				setInvcTabCounts(response);
				var loopLimit = response.output.fldTblDoc.length;
				var htmlStr = '', borderClass = "";
				
				for( var i=0; i<loopLimit; i++){
					borderClass = ( i != (loopLimit-1)) ? "border-bottom" : "";
					
					if( response.output.fldTblDoc[i].vchrPfx != undefined && response.output.fldTblDoc[i].vchrNo != undefined ){
						htmlStr += '<div class="list d-flex align-items-center '+ borderClass +' py-2">'+
								'<div class="wrapper w-100">'+
								  '<div class="d-flex justify-content-between align-items-center">'+
									'<div class="d-flex align-items-center">'+
									  '<p class="mb-0">'+ response.output.fldTblDoc[i].vchrVenId.trim() + "-" + response.output.fldTblDoc[i].vchrVenNm +'</p>'+
									'</div>'+
									'<small class="text-muted ml-auto">'+ response.output.fldTblDoc[i].vchrInvDtStr +'</small>'+
								  '</div><h3 class="mb-0">$ '+ response.output.fldTblDoc[i].vchrAmtStr +'</h3>'+
								'</div>'+
							'</div>';
					}
				}
				$("#tabPaneInProcess").html(htmlStr);
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
		}
	});
}

function getPaidcInvcPymtSts(){
	var loader = '<div class="box-loader d-flex align-items-center" id="inprocessLoader">'+
					'<div class="circle-loader"></div>'+
				'</div>';
	$("#paidLoader").html(loader);
	
	var year = $(".year-fltr").val();
	if( year == null || year == undefined || year == "" ){
		year = moment().format('YYYY');
	}
	
	var inputData = { 
		trnSts: "A",
		year: year,
		pageNo: 0,
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/cstmdoc/readdoc',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				setInvcTabCounts(response);
				var loopLimit = response.output.fldTblDoc.length;
				var htmlStr = '', borderClass = "";
				
				for( var i=0; i<loopLimit; i++){
					borderClass = ( i != (loopLimit-1)) ? "border-bottom" : "";
					htmlStr += '<div class="list d-flex align-items-center '+ borderClass +' py-2">'+
								'<div class="wrapper w-100">'+
								  '<div class="d-flex justify-content-between align-items-center">'+
									'<div class="d-flex align-items-center">'+
									  '<p class="mb-0">'+ response.output.fldTblDoc[i].vchrVenId.trim() + "-" + response.output.fldTblDoc[i].vchrVenNm +'</p>'+
									'</div>'+
									'<small class="text-muted ml-auto">'+ response.output.fldTblDoc[i].vchrInvDtStr +'</small>'+
								  '</div><h3 class="mb-0">$ '+ response.output.fldTblDoc[i].vchrAmtStr +'</h3>'+
								'</div>'+
							'</div>';
				}
				$("#tabPanePaid").html(htmlStr);
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
		}
	});
}


function getDiffInvcCount(){

	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/cstmdoc/prsdoc',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				$("#pendingInvoice").html( response.output.fldTblDoc[0].a );
				$("#confirmedInvoice").html( response.output.fldTblDoc[0].b );
				$("#prcPymntInvoice").html( response.output.fldTblDoc[0].c );
				$("#compInvoice").html( response.output.fldTblDoc[0].d );
				$("#rejectedInvoice").html( response.output.fldTblDoc[0].e );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
		}
	});
}


function createVoucher()
	{	
				
		$.ajax({
			headers : {
				'Accept' : 'application/json',
				'Content-Type' : 'application/json'
			},
			url : rootAppName + '/rest/vour/crtvour',
			type : 'POST',
			dataType : 'json',
		    data: JSON.stringify({
		    	companyId:$("#companyId").val(),
		    	voucherPrefix:$("#voucherPrefix").val(),
		    	voucherNumber:$("#voucherNumber").val(),
		    	sessionId:$("#sessionId").val(),
		    	entryDate:$("#entryDate").val(),
		    	vendorId:$("#vendorId").val(),
		    	vendorInvoiceNumber:$("#vendorInvoiceNumber").val(),
		    	extenralReference:$("#extenralReference").val(),
		    	materialTransferNumber:$("#materialTransferNumber").val(),
		    	voyageNumber:$("#voyageNumber").val(),
		    	vendorInvoiceDate:$("#vendorInvoiceDate").val(),
		    	purchaseOrderPrefix:$("#purchaseOrderPrefix").val(),
		    	purchaseOrderNumber:$("#purchaseOrderNumber").val(),
		    	purchaseOrderItem:$("#purchaseOrderItem").val(),
		    	voucherBranch:$("#voucherBranch").val(),
		    	pretaxVoucherAmount:$("#pretaxVoucherAmount").val(),
		    	voucherAmount:$("#voucherAmount").val(),
		    	discountableAmount:$("#discountableAmount").val(),
		    	voucherDescription:$("#voucherDescription").val(),
		    	voucherCurrency:$("#voucherCurrency").val(),
		    	exchangeRate:$("#exchangeRate").val(),
		    	paymentTerm:$("#paymentTerm").val(),
		    	discountTerm:$("#discountTerm").val(),
		    	dueDate:$("#dueDate").val(),
		    	discountDate:$("#discountDate").val(),
		    	discountAmount:$("#discountAmount").val(),
		    	checkItemRemarks:$("#checkItemRemarks").val(),
		    	paymentType:$("#paymentType").val(),
		    	voucherCrossRefNo:$("#voucherCrossRefNo").val(),
		    	authorizationReference:$("#authorizationReference").val(),
		    	voucherCategory:$("#voucherCategory").val(),
		    	serviceFulfillmentDate:$("#serviceFulfillmentDate").val(),
		    	prepaymentEligibility:$("#prepaymentEligibility").val(),
		    	transactionStatusAction:$("#transactionStatusAction").val(),
		    	transactionReason:$("#transactionReason").val(),
		    	transactionStatus:$("#transactionStatus").val(),
		    	transactionStatusRemarks:$("#transactionStatusRemarks").val(),
		    	paymentStatusAction:$("#paymentStatusAction").val(),
		    	paymentReason:$("#paymentReason").val(),
		    	paymentStatus:$("#paymentStatus").val(),
		    	paymentStatusRemarks:$("#paymentStatusRemarks").val(),
		    	userId:$("#userId").val(),
		    	authToken:$("#authToken").val(),
				host:$("#host").val(),
		    	port:$("#port").val()
			}),
		    success: function(data) {
		    	
		    	var arrResponse = JSON.parse(data.trim());
		    	
		    	tokenValue = arrResponse.authToken;
		    	usr = $("#user").val();
		    	
		    	
		    	console.log(data);
		    	alert(data);
		    }
		});
		
	}

	

function getCostReconData(venId="", brh="", cry="", exRef=""){	
	$("#mainLoader").show();
	
	vndrReconCnt++;
	
	var vendor = $("#venIdRecon option:selected").html();
	var inputData = {
		cmpyId: glblCmpyId, 
		brhId: brh, 
		venId: venId,
		cry: cry,
		exRef: exRef,
		ctlNo: $("#editCtlNo").val(),
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/vchr/recon',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopLimit = response.output.fldTblRecon.length;
				var loopLimitSel = response.output.fldTblReconSel.length;
				var tblRows = '', tblRowsSel = '', poStr = '', trnNoStr = '', selectStr = '', checkbox = '', rowClass = '', refVchrStr = '', crcnNo = '';
				var selected = $("#trsNo").attr("data-irc-no");
				var selectedArr  = [];
				
				if(selected == "" || selected == undefined  || selected == null)
				{
					 selectedArr = [];
				}
				else
				{
					selectedArr = selected.split(", ");
				}
					 
				var selReconArr = [];
				
				if(costReconClick == 0)
				{
					$("#selCostReconTable tbody tr").each(function(index, row){
					if(!$(row).hasClass("display-none")){
						selReconArr.push( $(row).attr("data-irc-no") );
					}
					});
				
					if( selReconArr.length ){
						selectedArr = selReconArr;
					}
					
				}
				
				costReconClick = 0;
				
				$("#selCostReconTable tbody tr").each(function(index, row){
						if(selectedArr.indexOf($(row).attr("data-irc-no").toString()) <= -1){
							$(this).remove();
						}else{
							$(row).removeClass("display-none");
							selReconArr.push( $(row).attr("data-irc-no") );
						}
					});
				
				$("#selCostReconTable tbody input[type='checkbox']").prop("checked", true);
				
				
				
				if(loopLimit){
					$("#exprtRecon").show();
				}else{
					$("#exprtRecon").hide();
				}
				
				for( var i=0; i<loopLimit; i++ ){
					poStr = '', checkbox = '', rowClass = 'vchr-row', refVchrStr = '';
					if( response.output.fldTblRecon[i].poNo != "0" ){
						poStr = 'PO-'+ response.output.fldTblRecon[i].poNo + '-' + response.output.fldTblRecon[i].poItm;
					}
					
					trnNoStr = response.output.fldTblRecon[i].trsPfx + '-' + response.output.fldTblRecon[i].trsNo;
					if( response.output.fldTblRecon[i].trsItm > 0 ){
						trnNoStr += '-' + response.output.fldTblRecon[i].trsItm;
					}
					if( response.output.fldTblRecon[i].trnsbItm > 0 ){
						trnNoStr += '-' + response.output.fldTblRecon[i].trnsbItm;
					}
					
					crcnNo = response.output.fldTblRecon[i].crcnNo;
					
					selectStr = (selectedArr.indexOf(crcnNo.toString()) > -1) ? "checked" : "";
					
					if( response.output.fldTblRecon[i].flg == "C" || response.output.fldTblRecon[i].flg == ""){
						rowClass = '';
						checkbox = '<div class="form-check form-check-flat inline-checkbox m-0">'+
										'<label class="form-check-label m-0">'+
											'<input type="checkbox" class="form-check-input" name="cost-recon-row" '+ selectStr +'>'+
											'&nbsp;<i class="input-helper"></i>'+
										'</label>'+
									'</div>';
					}
					else
					{
						checkbox = '';
					}
					
					
					var refVchrArr = response.output.fldTblRecon[i].refVoucher.split(",");
						for( var j=0; j<refVchrArr.length; j++){
							refVchrStr += '<span class="badge badge-dark mb-1">'+ refVchrArr[j] +'</span>&nbsp';
						}
					
					tblRows += '<tr class="'+ rowClass +'" data-cost-no="'+ response.output.fldTblRecon[i].costNo +'" data-irc-no="'+ response.output.fldTblRecon[i].ircNo +'" data-brh="'+ response.output.fldTblRecon[i].brh +'" data-trs-no="'+ trnNoStr +'" data-po-no="'+ poStr +'" data-bal-amt="'+ response.output.fldTblRecon[i].balAmt +'"  data-bal-qty="'+ response.output.fldTblRecon[i].balQty +'" data-recon-ven="'+ $("#venIdRecon").val() +'">'+
									'<td>'+ checkbox +'</td>'+
									'<td>'+ response.output.fldTblRecon[i].ircNo +'</td>'+
									'<td>'+ response.output.fldTblRecon[i].irhDesc +'</td>'+
									'<td>'+ poStr +'</td>'+
									'<td>'+ response.output.fldTblRecon[i].brh +'</td>'+
									'<td>'+ trnNoStr +'</td>'+
									'<td class="text-right">'+ response.output.fldTblRecon[i].origAmtStr +'</td>'+
									'<td class="text-right">'+ response.output.fldTblRecon[i].ipAmtStr +'</td>'+
									'<td class="text-right bal-amt-recon">'+ response.output.fldTblRecon[i].balAmtStr +'</td>'+
									'<td class="text-right">'+ response.output.fldTblRecon[i].balQty +'</td>'+
									'<td>'+ response.output.fldTblRecon[i].extRef +'</td>'+
									'<td>'+ vendor + '</td>'+
								'</tr>';
				}
				
				if(vndrReconCnt < 2)
				{
					for( var i=0; i<loopLimitSel; i++ ){
						poStr = "";
						if( response.output.fldTblReconSel[i].poNo != "0" ){
							poStr = 'PO-'+ response.output.fldTblReconSel[i].poNo + '-' + response.output.fldTblReconSel[i].poItm;
						}
					
						trnNoStr = response.output.fldTblReconSel[i].trsPfx + '-' + response.output.fldTblReconSel[i].trsNo;
						if( response.output.fldTblReconSel[i].trsItm > 0 ){
							trnNoStr += '-' + response.output.fldTblReconSel[i].trsItm;
						}
						if( response.output.fldTblReconSel[i].trnsbItm > 0 ){
							trnNoStr += '-' + response.output.fldTblReconSel[i].trnsbItm;
						}
					
						var crcnNo = response.output.fldTblReconSel[i].crcnNo;
						var costNo = response.output.fldTblReconSel[i].costNo;
						var brh =response.output.fldTblReconSel[i].brh;
						var trsNo = trnNoStr;
						var poNo = poStr;
						var balAmt = response.output.fldTblReconSel[i].balAmtStr;
						
						checkbox = '<div class="form-check form-check-flat inline-checkbox m-0">'+
												'<label class="form-check-label m-0">'+
													'<input type="checkbox" class="form-check-input" name="cost-recon-row" checked>'+
													'&nbsp;<i class="input-helper"></i>'+
												'</label>'+
											'</div>';
											
						var balAmtCol = '<div class="input-group">'+
											'<input type="text" class="form-control decimal recon-bal-val" value="'+ response.output.fldTblReconSel[i].ipAmt +'">'+
											'<div class="input-group-prepend">'+
												'<span class="input-group-text recon-bal-max">'+ response.output.fldTblReconSel[i].balAmt +'</span>'+
											'</div>'+
										'</div>';
						
						tblRowsSel += '<tr class="'+ rowClass +'" data-cost-no="'+ response.output.fldTblReconSel[i].costNo +'" data-irc-no="'+ response.output.fldTblReconSel[i].ircNo +'" data-brh="'+ response.output.fldTblReconSel[i].brh +'" data-trs-no="'+ trnNoStr +'" data-po-no="'+ poStr +'" data-bal-amt="'+ response.output.fldTblReconSel[i].balAmt +'"   data-bal-qty="'+ response.output.fldTblReconSel[i].balQty + '" data-recon-ven="'+ response.output.fldTblReconSel[i].venId +'">'+
										'<td>'+ checkbox +'</td>'+
										'<td>'+ response.output.fldTblRecon[i].ircNo +'</td>'+
										'<td>'+ response.output.fldTblRecon[i].irhDesc +'</td>'+
										'<td>'+ poStr +'</td>'+
										'<td>'+ response.output.fldTblReconSel[i].brh +'</td>'+
										'<td>'+ trnNoStr +'</td>'+
										'<td class="text-right">'+ response.output.fldTblReconSel[i].origAmt +'</td>'+
										'<td class="text-right">'+ response.output.fldTblReconSel[i].ipAmt +'</td>'+
										'<td class="text-right">'+ balAmtCol +'</td>'+
										'<td class="text-right">'+ response.output.fldTblReconSel[i].balQty +'</td>'+
										'<td>'+ response.output.fldTblRecon[i].extRef +'</td>'+
										'<td>'+ response.output.fldTblReconSel[i].venId + "-" + response.output.fldTblReconSel[i].venNm + '</td>'+
									'</tr>';
					}
					
					
					$("#selCostReconTable tbody").html(tblRowsSel);
					
					manageReconBalWidth();
				}
				
				calculateProof();
				$("#costReconTable tbody").html(tblRows);
				$("#costReconModal").modal("show");
				$("#mainLoader").hide();	
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
		}
	});
}

	
/**
 *	Get Cost Reconciliation from this function.
 */
/*function getCostReconData(venId="", brh="", cry="", exRef=""){	
	$("#mainLoader").show();
	
	var inputData = {
		cmpyId: glblCmpyId, 
		brhId: brh, 
		venId: venId,
		cry: cry,
		exRef: exRef,
	};
	
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/vchr/recon',
		type: 'POST',
		dataType : 'json',
		success: function(response){
		
			if( response.output.rtnSts == 0 ){
				var loopLimit = response.output.fldTblRecon.length;
				var tblRows = '', poStr = '', trnNoStr = '', selectStr = '', checkbox = '', rowClass = '', refVchrStr = '';
			
				var selected = $("#trsNo").val();
				var selectedArr = selected.split(", ");
				
				if(loopLimit){
					$("#exprtRecon").show();
				}else{
					$("#exprtRecon").hide();
				}
				
				for( var i=0; i<loopLimit; i++ ){
					poStr = '', checkbox = '', rowClass = 'vchr-row', refVchrStr = '';
					if( response.output.fldTblRecon[i].poNo != "0" ){
						poStr = 'PO-'+ response.output.fldTblRecon[i].poNo + '-' + response.output.fldTblRecon[i].poItm;
					}
					
					trnNoStr = response.output.fldTblRecon[i].trsPfx + '-' + response.output.fldTblRecon[i].trsNo;
					if( response.output.fldTblRecon[i].trsItm > 0 ){
						trnNoStr += '-' + response.output.fldTblRecon[i].trsItm;
					}
					if( response.output.fldTblRecon[i].trnsbItm > 0 ){
						trnNoStr += '-' + response.output.fldTblRecon[i].trnsbItm;
					}
					
					//selectStr = (selectedArr.indexOf(trnNoStr) > -1) ? "checked" : "";
					selectStr = (1 > -1) ? "" : "";
					response.output.fldTblRecon[i].refVoucher="";
					
					if( response.output.fldTblRecon[i].refVoucher == "" ){
						rowClass = '';
						checkbox = '<div class="form-check form-check-flat inline-checkbox m-0">'+
										'<label class="form-check-label m-0">'+
											'<input type="checkbox" class="form-check-input" name="cost-recon-row" '+ selectStr +'>'+
											'&nbsp;<i class="input-helper"></i>'+
										'</label>'+
									'</div>';
					}else{
						var refVchrArr = response.output.fldTblRecon[i].refVoucher.split(",");
						for( var j=0; j<refVchrArr.length; j++){
							refVchrStr += '<span class="badge badge-dark mb-1">'+ refVchrArr[j] +'</span>&nbsp';
						}
						
					}
					
					var balAmtCol = '<div class="input-group">'+
											'<input type="text" class="form-control decimal recon-bal-val" value="'+ response.output.fldTblReconSel[i].balAmt +'">'+
											'<div class="input-group-prepend">'+
												'<span class="input-group-text recon-bal-max">'+ response.output.fldTblReconSel[i].balAmt +'</span>'+
											'</div>'+
										'</div>';
										
					tblRows += '<tr class="'+ rowClass +'" data-brh="'+ response.output.fldTblRecon[i].brh +'" data-po-no="'+ poStr +'" data-bal-amt="'+ response.output.fldTblRecon[i].balAmt +'" data-bal-qty="'+ response.output.fldTblRecon[i].balQty +'" data-ircNo="'+ response.output.fldTblRecon[i].ircNo +'" data-trs-no="'+ trnNoStr +'">'+
									'<td>'+ checkbox +'</td>'+
									'<td>'+ poStr +'</td>'+
									'<td>'+ response.output.fldTblRecon[i].brh +'</td>'+
									'<td class="text-right">'+ response.output.fldTblRecon[i].balQty +'</td>'+
									'<td class="text-right">'+ response.output.fldTblRecon[i].balAmt +'</td>'+
									'<td class="text-right">'+ balAmtCol +'</td>'+
									'<td>'+ response.output.fldTblRecon[i].ircNo + '</td>'+
									//'<td>'+ refVchrStr +'</td>'+
									'<td>'+ trnNoStr +'</td>'+
								'</tr>';
					
					
					
				/*	tblRows += '<tr class="'+ rowClass +'" data-cost-no="'+ response.output.fldTblRecon[i].costNo +'" data-crcn-no="'+ response.output.fldTblRecon[i].crcnNo +'" data-brh="'+ response.output.fldTblRecon[i].brh +'" data-trs-no="'+ trnNoStr +'" data-po-no="'+ poStr +'" data-bal-amt="'+ response.output.fldTblRecon[i].balAmt +'">'+
									'<td>'+ checkbox +'</td>'+
									'<td>'+ poStr +'</td>'+
									'<td>'+ response.output.fldTblRecon[i].brh +'</td>'+
									'<td>'+ response.output.fldTblRecon[i].crcnNo +'</td>'+
									'<td>'+ trnNoStr +'</td>'+
									'<td class="text-right">'+ response.output.fldTblRecon[i].origAmtStr +'</td>'+
									'<td class="text-right">'+ response.output.fldTblRecon[i].balAmtStr +'</td>'+
									'<td>'+ response.output.fldTblRecon[i].cry +'</td>'+
									'<td>'+ response.output.fldTblRecon[i].balQty +'</td>'+
									'<td>'+ response.output.fldTblRecon[i].costNo + '-' + response.output.fldTblRecon[i].costDesc +'</td>'+
									'<td>'+ response.output.fldTblRecon[i].whs + '</td>'+
									'<td>'+ refVchrStr +'</td>'+
								'</tr>'; 
				}
				$("#costReconTable tbody").html(tblRows);
				$("#costReconModal").modal("show");
				$("#mainLoader").hide();
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
		}
	});
}*/

function manageReconBalWidth(){
	var maxWidth = 0;
	$("#selCostReconTable tbody tr .recon-bal-max").each(function(index, val){
		if(parseInt($(val).css("width")) > maxWidth){
			maxWidth = parseInt($(val).css("width")) ;
		}
	});
	
	if( maxWidth > 0 ){
		$("#selCostReconTable tbody tr .recon-bal-max").css("width", maxWidth + "px");
	}else{
		setTimeout(function(){
			manageReconBalWidth()
		}, 500)
	}
}

function calculateProof(){
	var mainAmt = "0"; 
	
	if($("#amt").val().length > 0)
	{
		mainAmt = $("#amt").val();
	}
	
	var totCrAmt = 0, totDrAmt = 0, reconAmt = 0, costReconArr = [];
	
	$("#glTbody tr").each(function(index, val){
		var crAmt = parseFloat( $(val).attr("data-crAmt") );
		var drAmt = parseFloat( $(val).attr("data-drAmt") );
		
		totCrAmt += parseFloat(crAmt);
		totDrAmt += parseFloat(drAmt);
	});
	
	if( $("#trsNo").attr("data-cost-recon") != undefined ){
		costReconArr = JSON.parse($("#trsNo").attr("data-cost-recon"));
	}
	
	
	if($("#costReconModal").hasClass("show"))
	{
		$("#selCostReconTable tbody tr").each(function(index, row){
				
				if(!$(row).hasClass("display-none") && $(row).find(".recon-bal-val").val().length > 0 && !isNaN($(row).find(".recon-bal-val").val()))
				{
					reconAmt +=	parseFloat($(row).find(".recon-bal-val").val());
				}
		});
	}
	else
	{
		for( var y=0; y<costReconArr.length; y++ ){
		reconAmt += parseFloat(costReconArr[y].balAmt);
		}
	}
	
	var proof = parseFloat(mainAmt) + totCrAmt - totDrAmt - reconAmt;
	$("span.proof").html( parseFloat(proof.toString()).toFixed(2) );
	$("#reconTotal").html( "Reconciliation Total: " + parseFloat(reconAmt.toString()).toFixed(2) );
	
}

/*function calculateProof(){
	var mainAmt = "0"; 
	
	if($("#amt").val().length > 0)
	{
		mainAmt = $("#amt").val();
	}
	
	var totCrAmt = 0, totDrAmt = 0, reconAmt = 0, costReconArr = [];
	
	$("#glTbody tr").each(function(index, val){
		var crAmt = parseFloat( $(val).attr("data-crAmt") );
		var drAmt = parseFloat( $(val).attr("data-drAmt") );
		
		totCrAmt += parseFloat(crAmt);
		totDrAmt += parseFloat(drAmt);
	});
	
	if( $("#trsNo").attr("data-cost-recon") != undefined ){
		costReconArr = JSON.parse($("#trsNo").attr("data-cost-recon"));
	}
	
	for( var y=0; y<costReconArr.length; y++ ){
		reconAmt += parseFloat(costReconArr[y].balAmt);
	}
	
	var proof = parseFloat(mainAmt) + totCrAmt - totDrAmt - reconAmt;
	$("span.proof").html( parseFloat(proof.toString()).toFixed(2) );
}*/


function getVndrDfltVal(venId){
	if( venId == "" || venId == undefined || venId == "null" || venId == null ){
		return;
	}
	
	vndrChngCnt++;
	
	var inputData = {
		cmpyId: glblCmpyId,
		venId: venId,
	};
	
	$.ajax({
		headers: ajaxHeader,
		url : rootAppName + '/rest/vendor/read-dflt-setting',
		data : JSON.stringify( inputData ),
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopLimit = response.output.fldTblVndrDfltStng.length;
				var glTblRows = '', vndrGlAcc = '', vndrSubAcc = '', vndrGlRmk = '', vndrDebAmt = 0, vndrCrdAmt = 0;
				
				if( loopLimit ){
					var innerLoopLimit = response.output.fldTblVndrDfltStng[0].glAcctList.length;
					if( vndrChngCnt == 1 ){
						if( $("#apBrh").val() == "" || $("#apBrh").val() == null ){
							$("#apBrh").val(response.output.fldTblVndrDfltStng[0].vchrBrh).trigger("change");
						}
						
						if( $("#vchrCat").val() == "" || $("#vchrCat").val() == null ){
							$("#vchrCat").val(response.output.fldTblVndrDfltStng[0].vchrCat).trigger("change");
						}
						
						$("#payTerms").val(response.output.fldTblVndrDfltStng[0].payTerm);
						$("#cry").val(response.output.fldTblVndrDfltStng[0].cry).trigger("change");
						$("#acctngPer").val(response.output.fldTblVndrDfltStng[0].curPeriod);
						
					}else{
						$("#apBrh").val(response.output.fldTblVndrDfltStng[0].vchrBrh).trigger("change");
						$("#vchrCat").val(response.output.fldTblVndrDfltStng[0].vchrCat).trigger("change");
						$("#payTerms").val(response.output.fldTblVndrDfltStng[0].payTerm);
						$("#acctngPer").val(response.output.fldTblVndrDfltStng[0].curPeriod);
						$("#cry").val(response.output.fldTblVndrDfltStng[0].cry).trigger("change");
					}
				
					/* $("#trnsStsActn").val(response.output.fldTblVndrDfltStng[0].trsStsActn).trigger("change");
					$("#trnsStsRsn").val(response.output.fldTblVndrDfltStng[0].trsRsn).trigger("change");
					$("#notifyUsr").val( response.output.fldTblVndrDfltStng[0].ntfUsr.trim().split(",") ).trigger("change");
					
					$("input[type='radio'][name='isActive'][value='"+ response.output.fldTblVndrDfltStng[0].isActive +"']").prop("checked", true);
					*/
					
					for( var i=0; i<innerLoopLimit; i++ ){
						vndrGlAcc = response.output.fldTblVndrDfltStng[0].glAcctList[i].bscGlAcct;
						vndrSubAcc = response.output.fldTblVndrDfltStng[0].glAcctList[i].sacct;
						vndrGlRmk = response.output.fldTblVndrDfltStng[0].glAcctList[i].distRmk;
						
						vndrDebAmt = $("#amt").val();
						
						var d = new Date();
						var uniqueId = d.valueOf() + "-" + i;
						
						glTblRows += '<tr id="'+ uniqueId +'" data-glacc="'+ vndrGlAcc +'" data-glSubAcc="' + vndrSubAcc + '" data-dramt="'+ vndrDebAmt +'" data-cramt = "'+ vndrCrdAmt +'" data-remark="' + vndrGlRmk + '">' +
						                '<td>' + vndrGlAcc + '</td>' +
						                /*'<td>' + vndrSubAcc + '</td>' +*/
						                '<td>' + vndrDebAmt + '</td>' +
						                '<td>' + vndrCrdAmt + '</td>' +
						                /*'<td>' + vndrGlRmk + '</td>' +*/
						                '<td> <i class="icon-note menu-icon icon-pointer edit-gl-data"></i> &nbsp; <i class="icon-close menu-icon icon-pointer delete-gl-data"></i> </td>' +
        							'</tr>';
					}
					
					if( vndrChngCnt > 1 ||  $("#glTbody").html() == ''){
						$("#glTbody").html(glTblRows);
					}
					
					if(!$("#venId").prop('disabled'))
					{
						$(".edit-gl-data").show();
						$(".delete-gl-data").show();
					}
					else
					{
						$(".edit-gl-data").hide();
						$(".delete-gl-data").hide();
					}
					
					calculateProof();
					//$('#vendorGlForm').collapse("show");
				}else{
					$("#apBrh, #vchrCat").val("").trigger("change");
				}
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}		
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
		}
	});

}

function setInvcTabCounts(response){
	if( response.output.toReviewCount != undefined ){
		$("#pendingInvoice").html(response.output.toReviewCount);
	}
	if( response.output.toReviewDocCount != undefined ){
		$("#pendingDocInvoice").html(response.output.toReviewDocCount);
	}
	if( response.output.confirmedCount != undefined ){
		$("#confirmedInvoice").html(response.output.confirmedCount);
	}
	if( response.output.payApprovedCount != undefined ){
		$("#prcPymntInvoice").html(response.output.payApprovedCount);
	}
	if( response.output.completeCount != undefined ){
		$("#compInvoice").html(response.output.completeCount);
	}
	
	if( response.output.toHoldDocCount != undefined ){
		$("#holdInvoices").html(response.output.toHoldDocCount);
	}
	
	if( response.output.rejectDocCount != undefined ){
		$("#rejectedInvoice").html(response.output.rejectDocCount);
	}
}

function getOverviewData(){
	$("#overviewLoader").removeClass("hide-imp");
	
	var year = $(".year-fltr").val();
	if( year == null || year == undefined || year == "" ){
		year = moment().format('YYYY');
	}
			
	var inputData = {
		year: year,
	};
														
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData),
		url: rootAppName + '/rest/dshbrd/ovrvw',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var dataArr = [];
				$("#totPayable").html( response.output.totPayable );
				$("#balance").html( response.output.balance );
				$("#overDueBalance").html( response.output.overDueBalance );
				$("#overDuePer").html( response.output.overDuePer );
				
				dataArr.push( response.output.dueInvoice30Days );
				dataArr.push( response.output.dueInvoice60Days );
				dataArr.push( response.output.dueInvoice90Days );
				dataArr.push( response.output.dueInvoice90PlusDays );
				
				$("#overviewLoader").addClass("hide-imp");
				
				overviewGraph(dataArr);
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			//$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			//$("#mainLoader").hide();
		}
	});
}


function overviewGraph(dataArr){
	var ctx = document.getElementById("overviewCanvas1").getContext('2d');
	var overviewCanvas1 = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: ["1-30", "31-60", "61-90", "90+"],
			datasets: [{
				label: 'Invoices',
				data: dataArr,
				backgroundColor: "rgba(254, 114, 144, 0.69)",
				borderColor: "#fe7290",
				borderWidth: 1
			}]
		},
		options: {
			title: {
				display: true,
				fontSize: 20,
				text: 'Unpaid Invoices (Due Date)'
			},
			legend: {
				position: 'bottom',
			},
			tooltips: {
				mode: 'index',
				intersect: false
			},
			responsive: true,
			scales: {
				xAxes: [{
					stacked: true,
					scaleLabel: {
						display: true,
						labelString: 'Days'
					},
					barThickness: 40,
				}],
				yAxes: [{
					scaleLabel: {
						display: false,
						//labelString: 'Amount in thousands dollars'
					},
					stacked: true
				}]
			}
		}
	});
}

function getApproverList( $select ){

	var inputData = {
			typ: "INV",
	};

	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/user/read-aprv',
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblERPUser.length;
				var usrId = '', usrNm = '', usrEmail = '', options = '<option value="">&nbsp;</option>';	
				for( var i=0; i<loopSize; i++ ){
					usrId = response.output.fldTblERPUser[i].usrId.trim();
					usrNm = response.output.fldTblERPUser[i].usrNm.trim();
					usrEmail = response.output.fldTblERPUser[i].usrEmail.trim();
					
					options += '<option value="'+ usrId +'">' + usrNm + '</option>';
				}
				$select.html( options );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			//$("#mainLoader").hide();
		}
	});
}

function getApproverListWithoutWkfUsr( $select, wkfId){
	
	$("#mainLoader").show();
	
	var inputData = {
			wkfId: wkfId,
			typ: "INV",
	};

	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/user/read-aprv',
		data: JSON.stringify( inputData ),
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblERPUser.length;
				var usrId = '', usrNm = '', usrEmail = '', options = '<option value="">&nbsp;</option>';	
				for( var i=0; i<loopSize; i++ ){
					usrId = response.output.fldTblERPUser[i].usrId.trim();
					usrNm = response.output.fldTblERPUser[i].usrNm.trim();
					usrEmail = response.output.fldTblERPUser[i].usrEmail.trim();
					
					options += '<option value="'+ usrId +'">('+ usrId + ') ' + usrNm + '</option>';
				}
				$select.html( options );
				
				$("#sendForReviewModal").modal("show");
				
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}		
			
			$("#mainLoader").hide();	
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}


function getWorkflowHistory(ctlNo, wkfId, e, place="O"){
	$("#mainLoader").show();
	
	var inputData = {
			ctlNo: ctlNo,
			wkfId: wkfId,
	};
				
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/wkf-map/hist',
		type: 'POST',
		success: function(response){
			if( response.output.rtnSts == 0 ){		
				var loopLimit = response.output.fldTblWkHistory.length;
				var tblRow = '', borderClass = "", statusClass = "", aprvrList = [], workflowQueue = '', activeAprvr = '';
				for( var y=0; y<loopLimit; y++){
					borderClass = ( y != (loopLimit-1)) ? "border-bottom" : "";
					statusClass = "badge-primary";
					if( response.output.fldTblWkHistory[y].sts == "In Review" ){
						statusClass = "badge-warning";
					}else if( response.output.fldTblWkHistory[y].sts == "Rejected" ){
						statusClass = "badge-danger";
					}else if( response.output.fldTblWkHistory[y].sts == "Approved" ){
						statusClass = "badge-success";
					}					
						
					tblRow += '<div class="list d-flex align-items-center '+ borderClass +' py-2">'+
									'<div class="wrapper w-100">'+
									  '<div class="d-flex justify-content-between align-items-center">'+
										'<div class="d-flex align-items-center">'+
										  '<h6 class="mb-1"><i class="icon-user text-primary mr-1"></i>'+ response.output.fldTblWkHistory[y].asgnToNm.trim() +'</h6>'+
										'</div>'+
										'<small class="text-muted ml-auto">'+ '<b>(' + response.output.fldTblWkHistory[y].rmkTyp +')</b>' + response.output.fldTblWkHistory[y].asgnOnDtStr +'</small>'+
									  '</div><p class="mb-0"><i class="icon-speech text-muted mr-1"></i> '+ response.output.fldTblWkHistory[y].rmk +'</p>'+
									  '<span class="badge '+ statusClass +' badge-pill">'+ response.output.fldTblWkHistory[y].sts +'</span>'+
									'</div>'+
								'</div>';
					
					//if(y==0){
						workflowQueue = '';
						aprvrList = response.output.fldTblWkHistory[y].wkfAprvrList;
						for(var z=0; z<aprvrList.length; z++){
							activeAprvr = (response.output.fldTblWkHistory[y].asgnToNm==aprvrList[z].usrNm) ? "text-primary font-weight-bold" : "";
							workflowQueue += '<li class="breadcrumb-item '+ activeAprvr +'">'+ aprvrList[z].usrNm +'</li>';
						}
					//}
				}
				
				$(".wkf-aprv-stage ol.breadcrumb").html(workflowQueue);
				
				if( place == "I" ){
					$("#innerInvcHstryWrapper").html(tblRow);
					
					$("#innerInvcHstry").slideDown();
				}else{
					$("#invcHstryWrapper").html(tblRow);
					
					var diff = e.pageY - 70 - $("#invcHstry").innerHeight();
					
					if( (e.pageY - 70 - $("#invcHstry").innerHeight()) <= 20){
						$("#invcHstry").css({"top": "0px", "left": e.pageX - 270 + "px"});
						$("#invcHstryWrapper").css({"max-height": "110px"});
						
						//if( diff < 0 ){
						//$("#invcHstryWrapper").css({"max-height": (250 + diff) + "px"});
					//}
					}else{
						$("#invcHstryWrapper").css({"max-height": "250px"});
						$("#invcHstry").css({"top": e.pageY - 70 - $("#invcHstry").innerHeight() + "px", "left": e.pageX - 270 + "px"});
					}
		
					$("#invcHstry").slideDown();
				}
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}


function getInvoiceComments(ctlNo){	
	$("#mainLoader").show();
	
	$("#saveCommentBtn").attr("data-ctl-no", ctlNo);
	
	var inputData = {
		ctlNo: ctlNo,
	};
				
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/vchr-note/read',
		type: 'POST',
		success: function(response){
			if( response.output.rtnSts == 0 ){		
				var loopLimit = response.output.fldTblVchrNotes.length;
				var htmlStr = '', borderClass = "", deleteIcon = "";
				
				for( var i=0; i<loopLimit; i++){
					deleteIcon = "";
					borderClass = ( i != (loopLimit-1)) ? "border-bottom" : "";
					
					if( localStorage.getItem("starOcrUserId") == response.output.fldTblVchrNotes[i].usrId ){
						deleteIcon = "<i class='icon-trash c-pointer font-weight-bold text-danger delete-comment mr-1'></i>";
					}
					
					htmlStr += '<div class="list d-flex align-items-center '+ borderClass +' py-2 invc-note-row" data-note-id="'+ response.output.fldTblVchrNotes[i].noteId +'">'+
								'<div class="wrapper w-100">'+
								  '<div class="d-flex justify-content-between align-items-center">'+
									'<div class="d-flex align-items-center">'+
									  '<h6 class="mb-1"><i class="icon-user text-primary mr-1"></i>'+ response.output.fldTblVchrNotes[i].usrNm.trim() +'</h6>'+
									'</div>'+
									'<small class="text-muted ml-auto">'+ response.output.fldTblVchrNotes[i].crtdOnStr +'</small>'+
								  '</div><p class="mb-0">'+ deleteIcon +'<i class="icon-speech text-muted mr-1"></i> '+ response.output.fldTblVchrNotes[i].note +'</p>'+
								'</div>'+
							'</div>';
				}
				
				$("#commentList").html(htmlStr);
				$("#commentsModal").modal("show");
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}


function editInvoice($this){
	if( $("#venId option").length ){
		vndrChngCnt = 0;
		
		$("#closeGlForm").trigger("click");
		//$("#venId").val("").trigger("change");
		$("#editDataModal .form-control").removeClass("border-danger");
		$("#editDataModal p.text-danger").remove();
		$("#editDataModal .form-control").val( '' );
		var ctlNo = $this.closest("tr").attr("data-ctl-no");
		var wkfId = $this.closest("tr").attr("data-wkf-id");
		var wkfSts = $this.closest("tr").attr("data-wkf-sts");
		var wkfAprvr = $this.closest("tr").attr("data-wkf-aprvr");
		var wkfBackSts = $this.closest("tr").attr("data-wkf-back");
		var wkfTotSts = $this.closest("tr").attr("data-wkf-forw");
		var wkfExtSts = $this.closest("tr").attr("data-wkf-ext");
		var usrId = localStorage.getItem("starOcrUserId");
		var upldBy = $this.closest("tr").attr("data-upld-by");
		
		$("#wkfInfo").attr("data-ctl-no", ctlNo);
		$("#wkfInfo").attr("data-wkf-id", wkfId);
		$("#wkfInfo").attr("data-wkf-back", wkfBackSts);
		$("#wkfInfo").attr("data-wkf-forw", wkfTotSts);
		$("#wkfInfo").attr("data-wkf-ext", wkfExtSts);
		
		
		$("#trnPfx").val("VR").trigger("change");
		$("#invcActnBtns, #glFormBtn").show();
		// TO DISABLE A FORM IF WORKFLOW IS APPLIED
		
		if(wkfId != 0 && wkfSts!= 'A')
		{
			if(wkfSts == 'S')
			{
				$("#wkf-status").html("Workflow Status <span class='badge badge-outline-warning c-pointer view-invc-hstry'>In Review</span>");
			}
			else if(wkfSts == 'R')
			{
				$("#wkf-status").html("Workflow Status <span class='badge badge-outline-danger c-pointer view-invc-hstry'>Rejected</span>");
			}
		
			if(usrId != wkfAprvr)
			{
				$("#editDataModal .form-control").prop("disabled", true);
				$("#editDataModal .cost-recon").prop("disabled", true);
				$("#glFormBtn").hide();
				$("#reviewWkf, #rejectWkf, #approveWkf, #sendBackWkf").hide();
				$("#createVou").hide();
				$("#updateApprove").hide();
			}
			else if(usrId == wkfAprvr && upldBy != usrId)
			{
				$("#editDataModal .form-control").prop("disabled", false);
				$("#editDataModal .cost-recon").prop("disabled", false);
				$("#glFormBtn").show();
				$("#reviewWkf, #rejectWkf, #approveWkf, #sendBackWkf").show();
				$("#createVou").hide();
				$("#updateApprove").show(); 
			}
			else if(usrId == wkfAprvr && upldBy == usrId)
			{
				$("#editDataModal .form-control").prop("disabled", false);
				$("#editDataModal .cost-recon").prop("disabled", false);
				$("#glFormBtn").show();
				$("#reviewWkf, #rejectWkf, #approveWkf, #sendBackWkf").hide();
				$("#createVou").show();
				$("#updateApprove").show();
			}
		}
		else if(wkfId != 0 && wkfSts== 'A')
		{
			$("#wkf-status").html("Workflow Status <span class='badge badge-outline-success c-pointer view-invc-hstry'>Approved</span>");
		
			$("#editDataModal .form-control").prop("disabled", false);
			$("#editDataModal .cost-recon").prop("disabled", false);
			$("#glFormBtn").show();
			$("#reviewWkf, #rejectWkf, #approveWkf, #sendBackWkf").hide();
			$("#createVou").show();
			$("#updateApprove").show();
		}
		
		/* CONDITION IF ASSIGNED TO SEPRATE USER OTHER THAN WORKFLOW */
		if(wkfId == 0 && wkfAprvr.length == 0)
		{
			$("#wkf-status").html("Workflow <span class='badge badge-outline-primary'>NA</span>");
		
			$("#editDataModal .form-control").prop("disabled", false);
			$("#editDataModal .cost-recon").prop("disabled", false);
			$("#glFormBtn").show();
			$("#reviewWkf, #rejectWkf, #approveWkf, #sendBackWkf").hide();
			
			$("#createVou").show();
			$("#updateApprove").show();
		}
		
		if(wkfId == 0 && wkfAprvr.length > 0 && wkfAprvr != usrId)
		{
			
				$("#wkf-status").html("Workflow <span class='badge badge-outline-primary'>NA</span>");
				$("#editDataModal .form-control").prop("disabled", true);
				$("#editDataModal .cost-recon").prop("disabled", true);
				$("#glFormBtn").hide();
				$("#reviewWkf, #rejectWkf, #approveWkf, #sendBackWkf").hide();
				
				$("#createVou").hide();
				$("#updateApprove").hide();
			
		}
		else if(wkfId == 0 && wkfAprvr.length > 0 && wkfAprvr == usrId)
		{
			if(wkfSts != 'R')
			{
				$("#wkf-status").html("Workflow <span class='badge badge-outline-primary'>NA</span>");
				$("#editDataModal .form-control").prop("disabled", false);
				$("#editDataModal .cost-recon").prop("disabled", false);
				$("#glFormBtn").show();
				$("#reviewWkf, #rejectWkf, #approveWkf, #sendBackWkf").show();
				
				$("#createVou").hide();
				$("#updateApprove").show();
			}
			else
			{
				$("#wkf-status").html("Workflow <span class='badge badge-outline-primary'>NA</span>");
				$("#editDataModal .form-control").prop("disabled", true);
				$("#editDataModal .cost-recon").prop("disabled", true);
				$("#glFormBtn").hide();
				$("#reviewWkf, #rejectWkf, #approveWkf, #sendBackWkf").hide();
				
				$("#createVou").hide();
				$("#updateApprove").hide();
			}
		}
		
		if(wkfId == 0 && wkfSts== 'A')
		{
			$("#wkf-status").html("Verification Status <span class='badge badge-outline-success c-pointer view-invc-hstry'>Approved</span>");
		
			$("#editDataModal .form-control").prop("disabled", false);
			$("#editDataModal .cost-recon").prop("disabled", false);
			$("#glFormBtn").show();
			$("#reviewWkf, #rejectWkf, #approveWkf, #sendBackWkf").hide();
			$("#createVou").show();
			$("#updateApprove").show();
		}
		
		if(wkfId == 0)
		{
			if(wkfSts == 'R')
			{
				$("#wkf-status").html("Verification Status <span class='badge badge-outline-danger c-pointer view-invc-hstry'>Rejected</span>");
			}
			else if(wkfSts == 'S')
			{
				$("#wkf-status").html("Verification Status <span class='badge badge-outline-warning c-pointer view-invc-hstry'>In Review</span>");
			}
		}
		
		
		if(wkfSts != 'A')
		{
			if(wkfExtSts == 0)
			{
				$("#reviewWkf").hide();
			}
			else
			{
				if(usrId == wkfAprvr)
				{
					$("#reviewWkf").show();
				}
			}
		}
		else
		{
			$("#reviewWkf").hide();
		}
		
		$("#venNm").prop("disabled", true);
		$("#payTerms").prop("disabled", true);
		$("#acctngPer").prop("disabled", true);
		$("#jrnlDt").prop("disabled", true);
		
		
		$("#editCtlNo").val( ctlNo );
		$("#editViewPdf").attr("src", rootAppName + "/rest/cstmdoc/downloadget?ctlNo=" + ctlNo);
	
		if( $this.hasClass("edit-hold-invc") ){
			$("#editDataModal .form-control").prop("disabled", true);
			$("#editDataModal .cost-recon").prop("disabled", true);
			$("#invcActnBtns, #glFormBtn").hide();
		}
		
		var inputData = { 
			ctlNo : ctlNo,
		};
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/cstmdoc/viewdoc',
			type: 'POST',
			dataType : 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					$("#glTbody").html("");
					$('#discDt, #trsNo').val("");
					$('#trsNo').removeAttr("data-trs-no");
					$('#trsNo').removeAttr("data-po-no");
					$('#trsNo').removeAttr("data-crcn-no");
					$('#trsNo').removeAttr("data-cost-no");
					$('#trsNo').removeAttr("data-cost-recon");
					
					$('#invDt, #entDt, #dueDt, #discDt, #jrnlDt').daterangepicker({
						singleDatePicker: true,
						showDropdowns: false,
						opens: 'left',
						locale: {
							format: 'MM/DD/YYYY'
						}
					});
					
					/*var vndrOptions = '<option value="">&nbsp;<option>', venId = '', venNm = '';
					var loopLimit = response.output.fldTblDoc[0].vendorList.length;
					for( var i=0; i<loopLimit; i++){
						venId = response.output.fldTblDoc[0].vendorList[i].venId.trim();
						venNm = response.output.fldTblDoc[0].vendorList[i].venNm.trim();
						vndrOptions += '<option value="'+ venId +'" data-vndr-nm="'+ venNm +'">'+ venId + "-" + venNm +'</option>';
					}
					$("#venId").html(vndrOptions);*/
					
					$("#venNm").val( response.output.fldTblDoc[0].vchrVenNm.trim() );
					$("#venId").val( response.output.fldTblDoc[0].vchrVenId.trim() ).trigger("change");
					$("#invNo").val( response.output.fldTblDoc[0].vchrInvNo.trim() );
					$("#extlref").val( response.output.fldTblDoc[0].vchrExtRef.trim() );
					$("#extlref").attr("data-saved", response.output.fldTblDoc[0].vchrExtRef.trim() );
					$("#apBrh").val( response.output.fldTblDoc[0].vchrBrh.trim() ).trigger("change");
					$("#amt").val( response.output.fldTblDoc[0].vchrAmt );
					$("#acctngPer").val( response.output.fldTblDoc[0].acctngPer );
					if( response.output.fldTblDoc[0].vchrCry != null ){
						$("#cry").val(response.output.fldTblDoc[0].vchrCry ).trigger("change");
					}else{
						$("#cry").val("").trigger("change");
					}
					
					if( response.output.fldTblDoc[0].vchrInvDtStr != undefined && response.output.fldTblDoc[0].vchrInvDtStr.trim() != "" ){
						$("#invDt").val( response.output.fldTblDoc[0].vchrInvDtStr.trim() );
					}else{
						$("#invDt").val("");
					}
					
				/*	if( response.output.fldTblDoc[0].crtDttsStr != undefined && response.output.fldTblDoc[0].crtDttsStr.trim() != "" ){
						$("#entDt").val( response.output.fldTblDoc[0].crtDttsStr.trim() );
					}else{
						$("#entDt").val("");
					}*/
					
					if( response.output.fldTblDoc[0].vchrDueDtStr != undefined && response.output.fldTblDoc[0].vchrDueDtStr.trim() != "" ){
						$("#dueDt").val( response.output.fldTblDoc[0].vchrDueDtStr.trim() );
					}else{
						$("#dueDt").val("");
					}
					
					if( response.output.fldTblDoc[0].vchrDiscDtStr != undefined && response.output.fldTblDoc[0].vchrDiscDtStr.trim() != "" ){
						$("#discDt").val( response.output.fldTblDoc[0].vchrDiscDtStr.trim() );
					}else{
						$("#discDt").val("");
					}
					
					$("#payTerms").val( response.output.fldTblDoc[0].vchrPayTerm.trim() );
					$("#poNo").val( response.output.fldTblDoc[0].vchrPoNo );
					$(".recon-amt").html( response.output.fldTblDoc[0].reconAmtStr );
					$(".recon-cnt").html( response.output.fldTblDoc[0].reconCount );
					$(".proof").html( response.output.fldTblDoc[0].vchrAmt );
					
					if( response.output.fldTblDoc[0].vchrRmk != undefined ){
						$("#voyageNo").html( response.output.fldTblDoc[0].voyage );
					}
					
					if( response.output.fldTblDoc[0].vchrRmk != undefined ){
						$("#vchrRmk").html( response.output.fldTblDoc[0].vchrRmk );
					}
					
					if( response.output.fldTblDoc[0].vchrCat != undefined ){
						
						if(response.output.fldTblDoc[0].vchrCat.length > 0)
						{
							$("#vchrCat").val( response.output.fldTblDoc[0].vchrCat.trim() ).trigger("change");
						}
						else
						{
							$("#vchrCat").val("").trigger("change");
						}
					}

					if( response.output.fldTblDoc[0].glData != undefined ){
						var loopLimit = response.output.fldTblDoc[0].glData.length;
						for( var i=0; i<loopLimit; i++){
							var d = new Date();
							var uniqueId = d.valueOf() + "-" + i;
							var glAcc = response.output.fldTblDoc[0].glData[i].glAcc;
							var glSubAcc = response.output.fldTblDoc[0].glData[i].glSubAcc;
							var drAmt = response.output.fldTblDoc[0].glData[i].drAmt;
							var crAmt = response.output.fldTblDoc[0].glData[i].crAmt;
							var remark = response.output.fldTblDoc[0].glData[i].remark;
							
							var trEdit = '';
							
							if($("#voyageNo").prop('disabled'))
							{
								trEdit = '<td> </td>';
							}
							else
							{	
								trEdit = '<td> <i class="icon-note menu-icon icon-pointer edit-gl-data"></i> &nbsp; <i class="icon-close menu-icon icon-pointer delete-gl-data"></i> </td>';
							}
							
							var tr = '<tr id="'+ uniqueId +'" data-glAcc="'+ glAcc +'" data-glSubAcc="'+ glSubAcc +'" data-drAmt="'+ drAmt +'" data-crAmt="'+ crAmt +'" data-remark="'+ remark +'">'+
										'<td>'+ glAcc +'</td>'+
										'<td>'+ glSubAcc +'</td>'+
										'<td>'+ drAmt +'</td>'+
										'<td>'+ crAmt +'</td>'+
										'<td>'+ remark +'</td>'+
										trEdit + 
										//'<td><i class="icon-close menu-icon icon-pointer delete-gl-data"></i> </td>'+
									'</tr>';
							$("#glTbody").append(tr);
						}
					}
					
					if( response.output.fldTblDoc[0].costRecon != undefined ){
						var loopLimit = response.output.fldTblDoc[0].costRecon.length;
						var trnNoStr = '', poNoStr = '', crcnNoStr = '', costNoStr = '';
						for( var i=0; i<loopLimit; i++){
							trnNoStr += (trnNoStr.length) ? ", " : "";
							trnNoStr += response.output.fldTblDoc[0].costRecon[i].trsNo;
							
							poNoStr += (poNoStr.length) ? ", " : "";
							poNoStr += response.output.fldTblDoc[0].costRecon[i].poNo;
							
							crcnNoStr += (crcnNoStr.length) ? ", " : "";
							crcnNoStr += response.output.fldTblDoc[0].costRecon[i].crcnNo;
							
							costNoStr += (costNoStr.length) ? ", " : "";
							costNoStr += response.output.fldTblDoc[0].costRecon[i].costNo;
						}
						
						$('#trsNo').val(trnNoStr);
						$('#trsNo').attr("data-trs-no", trnNoStr);
						$('#trsNo').attr("data-po-no", poNoStr);
						$('#trsNo').attr("data-irc-no", crcnNoStr);
						$('#trsNo').attr("data-cost-no", costNoStr);
						$('#trsNo').attr("data-cost-recon", JSON.stringify(response.output.fldTblDoc[0].costRecon));
					}
					
					
					if( response.output.fldTblDoc[0].referenceVoucherNo != undefined && response.output.fldTblDoc[0].referenceVoucherNo.trim() != "" ){
						var msg = "Voucher " + response.output.fldTblDoc[0].referenceVoucherNo.trim() + " is already created for Vendor ("+ response.output.fldTblDoc[0].vchrVenId.trim() +") and Invoice Number("+ response.output.fldTblDoc[0].vchrInvNo.trim() +")";
						$(".vchr-exist").html( msg );
						$("#createVou").attr("disabled", "disabled");
					}else{
						$(".vchr-exist").html("");
						$("#createVou").removeAttr("disabled");
					}
					
					calculateProof();
					
					$("#editDataModal").modal("show");
					
					
					$("#clearDiscDt, #clearDueDt").hide();
					$("#venId").focus();
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
				}
				
				$("#mainLoader").hide();
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});	
	}else{
		setTimeout(function(){
			editInvoice($this);
		}, 500);
	}
}