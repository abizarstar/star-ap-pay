$(function(){
	
	getUserProfile();
	
	$("#saveUser").click(function(){
		$("#editProfileCol .form-control").removeClass("border-danger");
		var errFlg = false;
		var usrName = $("#usrName").val();
		var usrEmail = $("#usrEmail").val();
		var email_reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    
	    if (usrEmail == '') {
	        errFlg = true;
	        $("#usrEmail").addClass("border-danger");
	    }else if (!email_reg.test(usrEmail)) {
	        errFlg = true;
	        $('#usrEmail').addClass("border-danger");
	    }
		
		if( usrName == "" ){
			errFlg = true;
			$("#usrName").addClass("border-danger");
		}
		
		if( errFlg == false ){
			$("#mainLoader").show();
			
			var inputData = {
				usrNm : usrName,
				eml : usrEmail,
				oldPass: "",
				newPass: "",
			};
								
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/user/UpdUsrDtls',
				type: 'POST',
				success: function(response){
					if( response.output.rtnSts == 0 ){
						localStorage.setItem("starOcrUserNm", usrName);
		            	localStorage.setItem("starOcrEmailId", usrEmail);
		            	
		            	setAjaxHeader();
		            	
		            	if( localStorage.getItem("starOcrUserNm") != null && localStorage.getItem("starOcrUserNm") != "" ){
					    	$("#loggedInUser").html( localStorage.getItem("starOcrUserNm") );
					    }else if( localStorage.getItem("starOcrUserId") != null && localStorage.getItem("starOcrUserId") != "" ){
					    	$("#loggedInUser").html( localStorage.getItem("starOcrUserId") );
					    }
		            	
						customAlert("#mainNotify", "alert-success", '<strong>Profile successfully updated.</strong>');
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
					}
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					console.log(jqXHR);
					console.log(textStatus);
					console.log(errorThrown);
					$("#mainLoader").hide();
				}
			});
		}
		
	});	
	
	$("#updatePassword").click(function(){
		$("#changePasswordCol .form-control").removeClass("border-danger");
		var errFlg = false;
		var usrOldPassword = $("#usrOldPassword").val();
		var usrPassword = $("#usrPassword").val();
		var usrCnfrmPassword = $("#usrCnfrmPassword").val();
		
	    if( usrOldPassword == "" ){
			errFlg = true;
			$("#usrOldPassword").addClass("border-danger");
		}
		
		if( usrPassword == "" ){
			errFlg = true;
			$("#usrPassword").addClass("border-danger");
		}else if( !$("#usrPassword").hasClass("border-success") ){
			errFlg = true;
			$("#usrPassword").addClass("border-danger");
		}
		
		if( usrCnfrmPassword == "" ){
			errFlg = true;
			$("#usrCnfrmPassword").addClass("border-danger");
		}else if( !$("#usrCnfrmPassword").hasClass("border-success") ){
			errFlg = true;
			$("#usrCnfrmPassword").addClass("border-danger");
		}
		
		if( errFlg == false ){
			$("#mainLoader").show();
			
			var inputData = {
				usrNm : "",
				eml : "",
				oldPass: usrOldPassword,
				newPass: usrPassword,
			};
								
			$.ajax({
				headers: ajaxHeader,
				data: JSON.stringify( inputData ),
				url: rootAppName + '/rest/user/UpdUsrDtls',
				type: 'POST',
				success: function(response){
					if( response.output.rtnSts == 0 ){		            	
						customAlert("#mainNotify", "alert-success", '<strong>Password successfully changed.</strong>');
						$("#changePasswordCol .form-control").val("");
						$("#changePasswordCol .form-control").removeClass("border-success");
					}else{
						var loopLimit = response.output.messages.length;
						var errList = '<ul class="m-0 pl-1">';
						for( var i=0; i<loopLimit; i++ ){
							errList += '<li>'+ response.output.messages[i] +'</li>';
						}
						errList += '</ul>';
						customAlert("#mainNotify", "alert-danger", errList);
					}
					$("#mainLoader").hide();
				},
				error: function( jqXHR, textStatus, errorThrown ){
					if( jqXHR.status == 401 ){
						validateAuthCode();
					}
					console.log(jqXHR);
					console.log(textStatus);
					console.log(errorThrown);
					$("#mainLoader").hide();
				}
			});
		}
		
	});	
	
});


function getUserProfile(){
	$("#mainLoader").show();

	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/user/get-sing-usr',
		type: 'POST',
		dataType: 'json',
		success: function(response){
			var loopSize = response.output.fldTblUsers.length;
			var tblRow = '', usrType = '';	
			for( var i=0; i<loopSize; i++ ){
			 	 $("#usrId").val(response.output.fldTblUsers[i].userId.trim());
				 $("#usrName").val(response.output.fldTblUsers[i].userNm.trim());
				 $("#usrEmail").val(response.output.fldTblUsers[i].emailId.trim());
				 
				 if( response.output.fldTblUsers[i].usrTyp.trim() == "STX" ){
				 	$("#changePasswordCol").remove();
				 }else if( response.output.fldTblUsers[i].usrTyp.trim() == "APP" ){
				 	$("#changePasswordCol").removeClass("display-none");
				 }
			}
			$("#mainLoader").hide();		
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}