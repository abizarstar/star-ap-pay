var glblReqIdCount;
var gblBatchTotalAmt=0.00;
var gblIssueDate="";
$(function(){

	getProposals();
	$("#aipPymntType").attr("data-selected", "CCD");
	getPaymentMethodList( $("#aipPymntType") );
	
	getApproverList( $("#aprv"), "PAY");
	
	getVendorListESP( $("#propVndrFltr"), glblCmpyId );
	//getVendorListESP( $("#aipVndrIdFrm, #aipVndrIdTo"), glblCmpyId );
	$("#batchTotalVal").html("0.00");
	$("#batchTotalCount").html("0");
	
	$("#propBankCode").change(function(){
	
		if($("#propBankCode").attr("disabled") == undefined)
		{
			var bnkCd = $(this).val();
			if( bnkCd != "" ){
				$("#propBankRouting").val( $("#propBankCode option:selected").attr("data-rout-no") );
				getBankAccountDetails($("#propBankAccNo"), bnkCd);
			}else{
				$("#propBankAccNo").html("");
			}
		}
	});
	
	$("#showAipCreateGroupArea").click(function(){
		$("#actnTyp").val("A");
		//$("#aipRqstId").removeAttr("disabled", "disabled");
		//$("#aipRqstId").val("");
		$("#aipRqstId").val("R"+glblReqIdCount);
		$("#batchTotalVal").html("0.00");
		$("#batchTotalCount").html("0");
		$("#aipVndrIdFrm, #aipVndrIdTo").val("").trigger("change");
		//$("#aipPymntType").val([]).trigger("change");
		
		$("#invcDrpActns").attr("data-prop-id", "");
		$("#invcDrpActns").attr("data-req-id", "");
		
		$("#aipCreateGroupArea .form-control").removeClass("border-danger");
		$("#stxEntryContent, #blockedVoucherTblWrapper").html("");
		$("#filterVoucherResults").hide();
		
		/*$('.date').daterangepicker({
			singleDatePicker: true,
			showDropdowns: true,
			opens: 'left',
			locale: {
				format: 'MM/DD/YYYY'
			}
		});*/
		
		var date = moment().format('MM/DD/YYYY');
		
		$("#aipInvcDt").val("");
		$("#aipDueDt").val("");
		$("#aipDiscDt").val("");
		
		$("#aipCreatedGroupArea").hide();
		$("#aipCreateGroupArea").slideDown();
		
		//$("#aipRqstId").focus();
	});
	
	$("#searchInvoice").click(function(){
		$("#aipPymntType").next().find(".select2-selection").removeClass("border-danger");
		
		var pymntMethods = $("#aipPymntType").val();
		
		if( pymntMethods.length == 0 || pymntMethods == "" || pymntMethods == undefined || pymntMethods == null ){
			$("#aipPymntType").next().find(".select2-selection").addClass("border-danger");
		}else if( pymntMethods.length > 1 && pymntMethods.indexOf("CHK") > -1 ){
			customAlert("#mainNotify", "alert-danger", "Check and electronic payments can not be searched together.");
		}else{
			searchInvoicesESP();
		}
	});
	
	$("#clearInvcFltr").click(function(){
		$(".invc-fltr-row .form-control, .invc-fltr-row .select2-selection").removeClass("border-danger");
		$("#aipInvcDt, #aipDueDt, #aipDiscDt").val("");
		$("#aipVndrIdFrm, #aipVndrIdTo").val("").trigger("change");
		//$("#aipPymntType").val([]).trigger("change");
	});
	
	$(".modal").on("click", ".view-doc", function(){
		var ctlNo = $(this).closest("tr").attr("data-ctl-no");
		ctlNo = (ctlNo != undefined) ? ctlNo : 0;
		
		var docUrl = rootAppName + "/rest/cstmdoc/downloadget?ctlNo=" + ctlNo;
		var modalId = $(this).parents(".modal").attr("id");
		
		$("#"+modalId+" .prop-data-wrapper").removeClass("col-md-12").addClass("col-md-6");
		$("#"+modalId+" .invoice-file-wrapper").removeClass("d-none");
		$("#"+modalId+" .invoice-file-iframe").attr("src", docUrl);
		
		var $table = $('#propTblWrapper table');
		$table.floatThead('reflow');
	});
	
	$("#closePreview").click(function(){
		var modalId = $(this).parents(".modal").attr("id");
		
		$("#"+modalId+" .prop-data-wrapper").removeClass("col-md-6").addClass("col-md-12");
		$("#"+modalId+" .invoice-file-wrapper").addClass("d-none");
		$("#"+modalId+" .invoice-file-iframe").attr("src", "");
		
		var $table = $('#propTblWrapper table');
		$table.floatThead('reflow');
	});
	
	$("#cancelInvcGrp, #backToAIP").click(function(){
		$("#actnTyp").val("A");
		$("#aipVndrIdFrm, #aipVndrIdTo").val("").trigger("change");
		//$("#aipPymntType").val([]).trigger("change");
		$("#aipRqstId, #aipInvcDt").val("");
		
		var $table = $('#stxEntryContent table');
		$table.floatThead('destroy');
		
		$("#stxEntryContent, #blockedVoucherTblWrapper").html("");
		$("#filterVoucherResults").hide();
		$("#aipCreateGroupArea, #invcDrpActns").hide();
		$("#aipCreatedGroupArea").slideDown();
	});
	
	$("#genPayBtn").click(function(){
		generatePaymentFile();
	});
	$("#saveInvcGrp, #sendToAprv, #finalSendToAprv").click(function(){
		$(".input-control").removeClass("border-danger");
		$("#aprv").next().find(".select2-selection").removeClass("border-danger");
		
		var errFlg = false;
		var reqId = $("#aipRqstId").val();
		var reqDt = mmddyyyyToDbFormat( $("#aipDt").val() );
		//var cmpyId = cmpyId;
		var cmpyId;
		var invDt = mmddyyyyToDbFormat( $("#aipInvcDt").val() );
		var venIdFrm = $("#aipVndrIdFrm").val();
		var venIdTo = $("#aipVndrIdTo").val();
		var ctlNoArr = [], vchrNoArr = [], chkNoArr=[], invcAddtnlData = [];
		var pymntMethods = [], cryArr = [];
		
		$("#stxEntryContent table tbody input[type='checkbox'][name='select-invoices']:checked").each(function(index, checkbox){
			ctlNoArr.push( $(checkbox).closest("tr").attr("data-ctl-no") );
			vchrNoArr.push( $(checkbox).closest("tr").attr("data-vchr-no") );
			chkNoArr.push( $(checkbox).closest("tr").attr("data-chk-no") );
			
			var jsonObj = {
				ctlNo: $(checkbox).closest("tr").attr("data-ctl-no"),
				vchrNo: $(checkbox).closest("tr").attr("data-vchr-no"),
				pymntMthd: $(checkbox).closest("tr").find(".vndr-pymnt-mthd").val(),
				note: $(checkbox).closest("tr").find(".payee-note").val(),
				chkNo: $(checkbox).closest("tr").attr("data-chk-no"),
			}
			pymntMethods.push($(checkbox).closest("tr").find(".vndr-pymnt-mthd option:selected").attr("data-mthd-cd"));
			cryArr.push($(checkbox).closest("tr").attr("data-cry"));
			invcAddtnlData.push(jsonObj);
		});
		
		console.log(cryArr);
		var uniquePymntMethods = pymntMethods.filter(function(itm, i, pymntMethods) {
								    return i == pymntMethods.indexOf(itm);
								});
								
		var uniqueCryArr = cryArr.filter(function(itm, i, cryArr) {
								    return i == cryArr.indexOf(itm);
								});
		
		if( uniquePymntMethods.length > 1 && uniquePymntMethods.indexOf("CHK") > -1 ){
			errFlg = true;
			customAlert("#mainNotify", "alert-danger", "Check and Electronic payments cannot be merged in a single proposal.");
		}else if( uniqueCryArr.length > 1 ){
			errFlg = true;
			customAlert("#mainNotify", "alert-danger", "Different currency payments cannot be merged in a single proposal.");
		}
		
		if( reqDt == "" ){
			errFlg = true;
			$("#aipDt").addClass("border-danger");
		}
		
		if( reqId == "" ){
			errFlg = true;
			$("#aipRqstId").addClass("border-danger");
		}
		
		if( ctlNoArr.length == 0 ){
			errFlg = true;
			customAlert("#mainNotify", "alert-danger", "Please select any invoice.");
		}
		
		if( errFlg == false ){
		
			if( $(this).attr("id") == "saveInvcGrp" ){
				var inputData = {
					cmpyId : glblCmpyId,
					reqId : reqId,
					//invList : ctlNoArr,
					//invList : vchrNoArr,
					invcAddtnlData: invcAddtnlData,
					reqUserBy : "",
					reqActnBy : "",
					paySts : "P",
					issueDate: gblIssueDate,
				};
				saveSendProposal( inputData );
			}else if( $(this).attr("id") == "sendToAprv" ){ 
				$("#aprv").val("").trigger("change");
				$("#sendToAprvModal").modal("show");
			}else if( $(this).attr("id") == "finalSendToAprv" ){
				 var aprv = $("#aprv").val();
				if( aprv != "" && aprv != null && aprv != undefined ){
					var inputData = {
						cmpyId : glblCmpyId,
						reqId : reqId,
						//invList : ctlNoArr,
						//invList : vchrNoArr,
						invcAddtnlData: invcAddtnlData,
						reqUserBy : "",
						reqActnBy : $("#aprv").val(),
						paySts : "S",
						issueDate: gblIssueDate,
					};
					saveSendProposal( inputData );
				}else{
					$("#aprv").next().find(".select2-selection").addClass("border-danger");
				}				
			}				
		}
	});
	
	
	$("#aipCreatedTbl").on("click", ".edit-auto-invc-pymnt", function(){
		$("#actnTyp").val("U");
		$("#mainLoader").show();
		
		var $table = $('#stxEntryContent table');
		$table.floatThead('destroy');
		
		var paySts = $(this).attr("data-pay-sts");
		var $tblRow = $(this).closest("tr");
		var reqId = $tblRow.attr("data-req-id");
		var reqDt = $tblRow.attr("data-req-dt");
		var payMthd = $tblRow.attr("data-pay-mthd");
		
		if( payMthd != "" && payMthd != "undefined" ){
			$("#aipPymntType").val( payMthd.split(",") ).trigger("changed");
		}
		
		$("#aipDt, #aipRqstId").attr("disabled", "disabled");
		$("#aipDt").val(reqDt);
		
		$("#aipCreateGroupArea .form-control, #aipCreateGroupArea .select2-selection").removeClass("border-danger");
		
		var inputData = { 
			reqId : reqId,
			paySts: paySts,
			venId: "",
			vchrNo: "",
			pageNo: -1,
			issueDate: gblIssueDate,
		};
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/payment/read',
			type: 'POST',
			dataType : 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					
					
					if( response.output.fldTblPayment.length ){
						var loopSize = response.output.fldTblPayment[0].paramInvList.length;
						$("#aipRqstId").val(response.output.fldTblPayment[0].reqId);
						/**
						  * Getting Pending Invoice Records
						  **/
						var poStr = "", vchrNoStr = "", vndrStr = "", payMthds = "";
						var tbl = '<table class="table">'+
								'<thead>'+
									'<tr class="thead-clr">'+
										'<th>#</th>'+
										'<th>Vendor ID</th>'+
										'<th>Invoice No.</th>'+
										'<th>Invoice Date</th>'+
										'<th>Due Date</th>'+
										'<th>PO</th>'+
										'<th>Voucher No.</th>'+
										'<th>Branch</th>'+
										'<th width="50px" class="text-right">Amount</th>'+
										'<th width="50px" class="text-right">Discount</th>'+
										'<th width="80px" class="text-right">Total</th>'+
										'<th>Entered Date</th>'+
										'<th width="100px">Disc Date</th>'+
										'<th width="100px">Pay Method</th>'+
										'<th width="">Payee Note</th>'+
									'</tr>'+
									'<tr class="filter-action-row hide-filter">'+
									'<th></th>'+
									'<th><input class="custom-text-filter form-control vendorFilter" type="text" /></th>'+
									'<th><input class="custom-text-filter form-control fieldsFilter" type="text" /></th>'+
									'<th><input type="text" name="daterange" class="form-control invcDtFilter"></th>'+
									'<th><input type="text" name="daterange" class="form-control dueDtFilter"></th>'+
									'<th></th>'+
									'<th><input class="custom-text-filter form-control voucherFilter" type="text" /></th>'+
									'<th></th>'+
									'<th></th>'+
									'<th></th>'+
									'<th></th>'+
									'<th></th>'+
									'<th></th>'+
									'<th></th>'+
									'<th></th>'+
								'</tr>'+
								'</thead>'+
								'<tbody>';
						for( var i=0; i<loopSize; i++ ){
							vchrNoStr = "", vndrStr = "", payMthds= "";
							if( response.output.fldTblPayment[0].paramInvList[i].poNo == 0 ){
								poStr = "";
							}else{
								poStr = response.output.fldTblPayment[0].paramInvList[i].poPfx +'-'+ response.output.fldTblPayment[0].paramInvList[i].poNo;
								
								if( response.output.fldTblPayment[0].paramInvList[i].poItm != "0" ){
									poStr += '-' + response.output.fldTblPayment[0].paramInvList[i].poItm;
								}
							}
							
							if( response.output.fldTblPayment[0].paramInvList[i].vchrPfx != undefined && response.output.fldTblPayment[0].paramInvList[i].vchrPfx.trim() != "" ){
								vchrNoStr = response.output.fldTblPayment[0].paramInvList[i].vchrPfx.trim() +'-'+ response.output.fldTblPayment[0].paramInvList[i].vchrNo.trim();
							}
							
							if( response.output.fldTblPayment[0].paramInvList[i].vchrVenId != undefined ){
								vndrStr = response.output.fldTblPayment[0].paramInvList[i].vchrVenId.trim() + "-" + response.output.fldTblPayment[0].paramInvList[i].vchrVenNm.trim();
							}
							
							for( var j=0; j<response.output.fldTblPayment[0].paramInvList[i].payMthds.length; j++){
								dfltOption = (response.output.fldTblPayment[0].paramInvList[i].payMthds[j].mthdDflt) ? "selected" : "";
								payMthds += '<option data-mthd-cd="'+ response.output.fldTblPayment[0].paramInvList[i].payMthds[j].mthdCd +'" value="'+ response.output.fldTblPayment[0].paramInvList[i].payMthds[j].mthdId +'" '+ dfltOption +'>'+ response.output.fldTblPayment[0].paramInvList[i].payMthds[j].mthdNm +'</option>';
							}
					
							tbl += '<tr data-cry="'+ response.output.fldTblPayment[0].paramInvList[i].vchrCry +'" data-invoice-no="'+ response.output.fldTblPayment[0].paramInvList[i].vchrInvNo +'" data-ctl-no="'+ response.output.fldTblPayment[0].paramInvList[i].vchrCtlNo + '" data-vchr-no="'+ vchrNoStr +'"  data-vchr-amt="'+ response.output.fldTblPayment[0].paramInvList[i].vchrAmt +'">'+
										'<td><div class="form-check form-check-flat m-0">'+
											'<label class="form-check-label">'+
											  '<input type="checkbox" class="form-check-input" name="select-invoices" checked>'+
											'<i class="input-helper"></i></label>'+
										 '</div></td>'+
										'<td>'+ vndrStr + '</td>'+
										'<td>'+ response.output.fldTblPayment[0].paramInvList[i].vchrInvNo +'</td>'+
										'<td>'+ response.output.fldTblPayment[0].paramInvList[i].vchrInvDtStr +'</td>'+
										'<td>'+ response.output.fldTblPayment[0].paramInvList[i].vchrDueDtStr +'</td>'+
										'<td>'+ poStr +'</td>'+
										'<td>'+ vchrNoStr +'</td>'+
										'<td>'+ response.output.fldTblPayment[0].paramInvList[i].vchrBrh +'</td>'+
										'<td class="text-right">'+ response.output.fldTblPayment[0].paramInvList[i].vchrAmtStr +'</td>'+
										'<td class="text-right">'+ response.output.fldTblPayment[0].paramInvList[i].discAmtStr +'</td>'+
										'<td class="text-right">'+ response.output.fldTblPayment[0].paramInvList[i].totAmtStr +' '+ response.output.fldTblPayment[0].paramInvList[i].vchrCry +'</td>'+
										'<td>'+ response.output.fldTblPayment[0].paramInvList[i].crtDttsStr +'</td>'+
										'<td>'+ response.output.fldTblPayment[0].paramInvList[i].vchrDiscDtStr +'</td>'+
										'<td>'+
											'<select class="form-control vndr-pymnt-mthd select2" style="width: 100%">'+
												payMthds +
											'</select>'+
										'</td>'+
										'<td><input typr="text" class="form-control payee-note" value="'+ response.output.fldTblPayment[0].paramInvList[i].note +'"/> </td>'+
									'</tr>';
						}
						
						tbl += '</tbody></table>';
						$("#stxEntryContent").html(tbl);
						$("#holdInvcWrapper").hide();
						$("#filterVoucherResults").slideDown();
		
						var $table = $('#stxEntryContent table');
								
						$table.floatThead({
							useAbsolutePositioning: true,
							scrollContainer: function($table) {
								return $table.closest('.table-responsive');
							}
						});
						$table.floatThead('reflow');
		
						$("#aipCreatedGroupArea").hide();
						$("#aipCreateGroupArea").slideDown();
						
						$(".select2").select2();
						
						$("#invcDrpActns").show();
						
						$("#invcDrpActns").attr("data-req-id", reqId);
					}
					calculateTotal();
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
				}
				
				$("#mainLoader").hide();
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});
		
	});
	
	$(document).on("change", "#selectAllInvc", function(){
		if( $(this).prop("checked") == true ){
			$("#stxEntryContent table tbody input[type='checkbox'][name='select-invoices']").prop("checked", "checked");
			$("#stxEntryContent table tbody .vndr-pymnt-mthd, #stxEntryContent table tbody .payee-note").removeAttr("disabled");
		}else{
			$("#stxEntryContent table tbody input[type='checkbox'][name='select-invoices']").prop("checked", false);
			$("#stxEntryContent table tbody .vndr-pymnt-mthd, #stxEntryContent table tbody .payee-note").attr("disabled", "disabled");
		}
		
		calculateTotal();
	});
	
	$("#stxEntryContent").on("change", "#selectAllInvc", function(){
		if( $(this).prop("checked") == true ){
			$("#stxEntryContent table tbody input[type='checkbox'][name='select-invoices']").prop("checked", "checked");
			$("#stxEntryContent table tbody .vndr-pymnt-mthd, #stxEntryContent table tbody .payee-note").removeAttr("disabled");
		}else{
			$("#stxEntryContent table tbody input[type='checkbox'][name='select-invoices']").prop("checked", false);
			$("#stxEntryContent table tbody .vndr-pymnt-mthd, #stxEntryContent table tbody .payee-note").attr("disabled", "disabled");
		}
		
		calculateTotal();
	});
	
	$("#stxEntryContent, .floatThead-table").on("change", "input[type='checkbox'][name='select-invoices']", function(){
		if( $(this).prop("checked") ){
			$(this).closest("tr").find(".vndr-pymnt-mthd, .payee-note").removeAttr("disabled");
		}else{
			$(this).closest("tr").find(".vndr-pymnt-mthd, .payee-note").attr("disabled", "disabled");
		}
		
		if( $("#stxEntryContent table tbody input[type='checkbox'][name='select-invoices']").length == $("#stxEntryContent table tbody input[type='checkbox'][name='select-invoices']:checked").length ){
			$("#selectAllInvc").prop("checked", true);
		}else{
			$("#selectAllInvc").prop("checked", false);
		}
		
		calculateTotal();
	});
	
	$("#aipCreatedTbl").on("click", ".view-auto-invc-pymnt", function(){
		$("#mainLoader").show();
		$(".bnk-info").hide();
		$(".modal .prop-data-wrapper").removeClass("col-md-6").addClass("col-md-12");
		$(".modal .invoice-file-wrapper").addClass("d-none");
		$(".modal .invoice-file-iframe").attr("src", "");
		
		var reqId = $(this).closest("tr").attr("data-req-id");
		var payMthd = $(this).closest("tr").attr("data-pay-mthd");
		var totAmt = $(this).closest("tr").attr("data-tot-amt");
		var disbChkNetAmt=0;
		var disbChkNetAmtTotal=0;
		$("#viewAIPSenderModal .request-no").html(reqId);
		$("#viewAIPSenderModal .pymnt-mthd").html(payMthd);
		$("#viewAIPSenderModal .total-amt").html(totAmt); 
		
		$("thead th input[type='text']").val('');
		$('#propTblWrapper table').floatThead('destroy');
		
		var inputData = { 
			reqId : reqId,
			paySts: "",
			venId: "",
			vchrNo: "",
			pageNo: -1,
			issueDate: gblIssueDate,
		};
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/payment/read',
			type: 'POST',
			dataType : 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					var poStr = "", vchrNoStr = "", rowPymntMthd = '';
					var tbl = '', prevCry = '', prevVenId = '', prevVendor = '', venTotal = 0;
					if( response.output.fldTblPayment.length ){
						var loopSize = response.output.fldTblPayment[0].paramInvList.length;
						for( var i=0; i<loopSize; i++ ){
							vchrNoStr = "", rowPymntMthd = "";
							if( response.output.fldTblPayment[0].paramInvList[i].poNo == 0 ){
								poStr = "";
							}else{
								
								if( response.output.fldTblPayment[0].paramInvList[i].poItm == 0 ){
									poStr = response.output.fldTblPayment[0].paramInvList[i].poPfx +'-'+ response.output.fldTblPayment[0].paramInvList[i].poNo;
								}
								else
								{
									poStr = response.output.fldTblPayment[0].paramInvList[i].poPfx +'-'+ response.output.fldTblPayment[0].paramInvList[i].poNo +'-'+ response.output.fldTblPayment[0].paramInvList[i].poItm;
								}
							}
							
							if( response.output.fldTblPayment[0].paramInvList[i].vchrPfx != undefined && response.output.fldTblPayment[0].paramInvList[i].vchrPfx.trim() != "" ){
								vchrNoStr = response.output.fldTblPayment[0].paramInvList[i].vchrPfx.trim() +'-'+ response.output.fldTblPayment[0].paramInvList[i].vchrNo.trim();
							}
							
							var glVal = '';
							if(response.output.fldTblPayment[0].paramInvList[i].glEntry.length > 0)
							{
								glVal = '<strong>' + response.output.fldTblPayment[0].paramInvList[i].glEntry + ' (' + response.output.fldTblPayment[0].paramInvList[i].glEntryDt + ')' + '</strong>';
							}
							else
							{
								glVal = '';
							}
							
							var discStr = "";
							if(response.output.fldTblPayment[0].paramInvList[i].discAmtStr != 0)
							{
								//discStr = response.output.fldTblPayment[0].paramInvList[i].discAmtStr +' '+ response.output.fldTblPayment[0].paramInvList[i].vchrCry;
								discStr = response.output.fldTblPayment[0].paramInvList[i].discAmtStr ;
							}
							
							if( response.output.fldTblPayment[0].paramInvList[i].payMthds.length ){	
								var innerLoopSize = response.output.fldTblPayment[0].paramInvList[i].payMthds.length;
								for( var j=0; j<innerLoopSize; j++ ){
									if( response.output.fldTblPayment[0].paramInvList[i].payMthds[j].mthdDflt){	
										rowPymntMthd = response.output.fldTblPayment[0].paramInvList[i].payMthds[j].mthdNm;
										break;
									}		
								}
							}
							
							if( prevVenId != "" && prevVenId != response.output.fldTblPayment[0].paramInvList[i].vchrVenId.trim() ){
														
								tbl += '<tr class="vndr-total font-weight-bold text-dark">'+
										'<td></td>'+
										'<td></td>'+
										'<td>'+ prevVendor +' (Total):</td>'+
										'<td></td>'+
										'<td></td>'+
										'<td></td>'+
										'<td></td>'+
										'<td></td>'+
										'<td class="text-right" colspan="2">'+ venTotal.toFixed(2) +' '+ prevCry +'</td>'+
										'<td class="text-right"></td>'+
										//'<td class="check-col"></td>'+
										'<td> '+ disbChkNetAmt.toFixed(2) +'</td>'+
										'<td class="pymnt-mthd-col"></td>'+
										'<td></td>'+
										'<td></td>'+
										/*'<td class="check-gl">'+ glVal +'</td>'+*/
									'</tr>';
								venTotal = 0;
								disbChkNetAmtTotal +=disbChkNetAmt;
							}
							
							var failedRowClass = '';
							
							if(response.output.fldTblPayment[0].paramInvList[i].vchrPrsSts == '' || response.output.fldTblPayment[0].paramInvList[i].vchrPrsSts == 'false')
							{
								failedRowClass = 'failed-payment-row';
							}
							
							tbl += '<tr class="'+ failedRowClass +'" data-invc-no="'+ response.output.fldTblPayment[0].paramInvList[i].vchrInvNo +'" data-vchr-no="'+ vchrNoStr +'" data-ctl-no="'+ response.output.fldTblPayment[0].paramInvList[i].ctlNo +'">'+
										'<td>'+ (i+1) +'</td>'+
										'<td><i class="icon-eye menu-icon view-doc text-info" aria-hidden="true"></i></td>'+
										'<td>'+ response.output.fldTblPayment[0].paramInvList[i].vchrVenId.trim() + "-" + response.output.fldTblPayment[0].paramInvList[i].vchrVenNm.trim() + '</td>'+
										'<td>'+ response.output.fldTblPayment[0].paramInvList[i].vchrInvNo +'</td>'+
										'<td>'+ response.output.fldTblPayment[0].paramInvList[i].vchrInvDtStr +'</td>'+
										'<td>'+ response.output.fldTblPayment[0].paramInvList[i].vchrDueDtStr +'</td>'+
										//'<td>'+ poStr +'</td>'+
										'<td>'+ '' +'</td>'+
										'<td>'+ vchrNoStr +'</td>'+
										'<td>'+ response.output.fldTblPayment[0].paramInvList[i].vchrBrh +'</td>'+
										//'<td class="text-right">'+ response.output.fldTblPayment[0].paramInvList[i].vchrAmtStr +' '+ response.output.fldTblPayment[0].paramInvList[i].vchrCry +'</td>'+
										'<td class="text-right">'+ response.output.fldTblPayment[0].paramInvList[i].vchrAmtStr +'' +'</td>'+
										'<td class="text-right">'+ discStr +'</td>'+
										//'<td class="check-col">'+ response.output.fldTblPayment[0].paramInvList[i].chkNo +'</td>'+
										'<td>'+ response.output.fldTblPayment[0].paramInvList[i].chkNo +'</td>'+
										'<td class="pymnt-mthd-col">'+ rowPymntMthd +'</td>'+
										'<td>'+ response.output.fldTblPayment[0].paramInvList[i].crtDttsStr +'</td>'+
										'<td>'+ response.output.fldTblPayment[0].paramInvList[i].vchrDiscDtStr +'</td>'+
										/*'<td class="check-gl">'+ glVal +'</td>'+*/
									'</tr>';
									
							prevVenId = response.output.fldTblPayment[0].paramInvList[i].vchrVenId.trim();
							prevCry = response.output.fldTblPayment[0].paramInvList[i].vchrCry;
							venTotal += response.output.fldTblPayment[0].paramInvList[i].vchrAmt;
							prevVendor = response.output.fldTblPayment[0].paramInvList[i].vchrVenId.trim() + "-" + response.output.fldTblPayment[0].paramInvList[i].vchrVenNm.trim();
							disbChkNetAmt = response.output.fldTblPayment[0].paramInvList[i].chkNetAmt;
							
						}
						
						if( prevVenId != "" ){
							tbl += '<tr class="vndr-total font-weight-bold text-dark">'+
									'<td></td>'+
									'<td></td>'+
									'<td>'+ prevVendor +' (Total):</td>'+
									'<td></td>'+
									'<td></td>'+
									'<td></td>'+
									'<td></td>'+
									'<td></td>'+
									//'<td class="text-right" colspan="2">'+ venTotal.toFixed(2) +' '+ prevCry +'</td>'+
									'<td class="text-right" colspan="2">'+ venTotal.toFixed(2) +'</td>'+
									'<td class="text-right"></td>'+
									//'<td class="check-col"></td>'+
									'<td> '+ disbChkNetAmt.toFixed(2) +'</td>'+
									'<td class="pymnt-mthd-col"></td>'+
									'<td></td>'+
									'<td></td>'+
									/*'<td class="check-gl">'+ glVal +'</td>'+*/
								'</tr>';
							venTotal = 0;
							disbChkNetAmtTotal +=disbChkNetAmt;
						}
						
						
						if( response.output.fldTblPayment[0].companyBnkList.length ){						
												
							loopSize = response.output.fldTblPayment[0].companyBnkList[0].accoutnList.length;
							
							for( var i=0; i<loopSize; i++ ){
								
								if(response.output.fldTblPayment[0].accountNo == response.output.fldTblPayment[0].companyBnkList[0].accoutnList[i].bnkAcctNo)
								{	
									$("#modalBnkInfo").html(response.output.fldTblPayment[0].companyBnkList[0].accoutnList[i].bnkAcctDesc + " (" + response.output.fldTblPayment[0].companyBnkList[0].accoutnList[i].bnkAcctNo + ")");
									$(".bnk-info").show();
								}		
							}
							
						}
						
						
					}
					
					if( tbl != "" ){
						$("#senderAIPSnglPropPndgData table tbody").html(tbl);
						$("#senderAIPSnglPropPndgData").show();				
					}else{
						$("#senderAIPSnglPropPndgData table tbody").html("");
						$("#senderAIPSnglPropPndgData").hide();
					}
					
					if( payMthd.indexOf("CHK") > -1 || payMthd.indexOf("CHECK") > -1 ){
						$(".check-col").show();
					}else{
						$(".check-col").hide();
					}
					
					if( payMthd.indexOf(",") > -1 ){
						$(".pymnt-mthd-col").show();
					}else{
						$(".pymnt-mthd-col").hide();
					}					
					
					$("#viewAIPSenderModal .total-amt").html(disbChkNetAmtTotal.toFixed(2));
					$("#viewAIPSenderModal").modal("show");
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
				}
				
				$("#mainLoader").hide();
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});
	});
	
	
	$("#viewAIPSenderModal").on("shown.bs.modal", function() {
		var $table = $('#propTblWrapper table');
								
		$table.floatThead({
			useAbsolutePositioning: true,
			scrollContainer: function($table) {
				return $table.closest('.table-responsive');
			}
		});
		$table.floatThead('reflow');
	});
	
	$("#aipCreatedTbl").on("click", ".dwnld-ach-file", function(){
		$("#mainLoader").show();
		
		var flNm = $(this).closest("tr").attr("data-fl-nm");
		var reqId = $(this).closest("tr").attr("data-req-id");
		var paySts = $(this).closest("tr").attr("data-pay-sts");
		
		var inputData = { 
			reqId : reqId,
			paySts: paySts,
			venId: "",
			vchrNo: "",
			pageNo: -1,
			issueDate: gblIssueDate,
		};
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/payment/read',
			type: 'POST',
			dataType : 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					if( response.output.fldTblPayment.length && response.output.fldTblPayment[0].flSentSts ){
						$("#dwnldAchFl").attr("data-file-nm", flNm);
						$("#dwnldAchFlModal").modal("show");
					}else{
						downloadAchFile(flNm);
					}
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
				}
				
				$("#mainLoader").hide();
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});
	});
	
	$("#dwnldAchFl").click(function(){
		var flNm = $(this).attr("data-file-nm");
		
		downloadAchFile(flNm, 1);
		
		$("#dwnldAchFlModal").modal("hide");
	});
	
	$("#aipCreatedTbl").on("click", ".sftp-ach-file", function(){		
		var flNm = $(this).closest("tr").attr("data-fl-nm");
		var reqId = $(this).closest("tr").attr("data-req-id");
		var paySts = $(this).closest("tr").attr("data-pay-sts");
		
		$("#sftpAchFl").attr("data-fl-nm", flNm);
		$("#sftpAchFl").attr("data-req-id", reqId);
		$("#sftpAchFl").attr("data-pay-sts", paySts);
		
		$("#sftpAchFlModal").modal("show");
	});
	
	$("#sftpAchFl").click(function(){
		$("#mainLoader").show();
		
		var flNm = $("#sftpAchFl").attr("data-fl-nm");
		var reqId = $("#sftpAchFl").attr("data-req-id");
		var paySts = $("#sftpAchFl").attr("data-pay-sts");
		
		var inputData = { 
			reqId : reqId,
			flNm: flNm,
		};
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/payment/sftp-ach',
			type: 'POST',
			dataType : 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					customAlert("#mainNotify", "alert-success", "ACH file scuccessfully uploaded to bank server.");
					
					getProposals();
					
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
				}
				
				$("#sftpAchFlModal").modal("hide");
				$("#mainLoader").hide();
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});
	});
	
	$("#showPropFltr").click(function(){
		$(".prop-fltr-wrapper").slideToggle(function(){
			if( $(".prop-fltr-wrapper").css("display") == "block" ){
				$("#aipCreatedTbl").parent().css("max-height", "calc(100vh - 269px)");
			}else if( $(".prop-fltr-wrapper").css("display") == "none" ){
				$("#aipCreatedTbl").parent().css("max-height", "calc(100vh - 214px)");
			}
		});
	});
	
	$("#searchProposals").click(function(){
		getProposals();
	});
	
	$("#resetTransFltr").click(function(){
		$("#propVndrFltr").val("").trigger("change");
		$("#propVchrNoFltr").val("");
	});
	
	$("#propVndrFltr").change(function(){
		$("#loadMoreAip").attr("disabled", "disabled");
		$("#loadMoreAip").addClass("disabled");
	});
	
	$("#propVchrNoFltr").focusout(function(){
		$("#loadMoreAip").attr("disabled", "disabled");
		$("#loadMoreAip").addClass("disabled");
	});
	
	$("#loadMoreAip").click(function(){
		getProposals( $(this).attr("data-page-no") );
	});
	
	$("#exportVchrPay").click(function(){
		var payMthd = '';
		var invcDt = $("#aipInvcDt").val();
		var dueDt = $("#aipDueDt").val();
		var discDt = $("#aipDiscDt").val();
		var venIdFrm = $("#aipVndrIdFrm").val();
		var venIdTo = $("#aipVndrIdTo").val();
	
		$("#aipPymntType option:selected").each(function(index, option){
			payMthd += (payMthd.length) ? "," : "";
			payMthd += $(option).attr("data-mthd-id");
		});
		
		invcDt = (invcDt == null || invcDt == undefined ) ? "" : invcDt;
		dueDt = (dueDt == null || dueDt == undefined ) ? "" : dueDt;
		discDt = (discDt == null || discDt == undefined ) ? "" : discDt;
		
		var invcStrtDate = "";
		var invcEndDate = "";
		if( invcDt != "" ){
			invcStrtDate = invcDt.split(" - ")[0];
			invcEndDate = invcDt.split(" - ")[1];
		}
		
		var dueStrtDate = "";
		var dueEndDate = "";
		
		if( dueDt != "" ){
			dueStrtDate = dueDt.split(" - ")[0];
			dueEndDate = dueDt.split(" - ")[1];
		}
		
		var discStrtDate = "";
		var discEndDate = "";
		
		if( discDt != "" ){
			discStrtDate = discDt.split(" - ")[0];
			discEndDate = discDt.split(" - ")[1];
		}
		
		var docUrl = rootAppName + "/rest/export/vch-pay-info?usrId="+  localStorage.getItem("starOcrUserId") + "&cmpyId="+ glblCmpyId +"&venIdFrm=" + venIdFrm +"&venIdTo=" + venIdTo + "&invcDt=" + invcStrtDate + "&invcDtTo=" + invcEndDate + "&dueDt=" + dueStrtDate+ "&dueDtTo=" + dueEndDate + "&discDt=" + discStrtDate + "&discDtTo=" + discEndDate + "&payMthd=" + payMthd; 

		window.open(docUrl, '_blank');
	});
	
	$("#aipCreatedTbl").on("click", ".delete-auto-invc-pymnt", function(){
		var $row = $(this).closest("tr");
		var reqId = $row.attr("data-req-id");
		var paySts = $row.attr("data-pay-sts");
		
		$("#deleteAipBtn").attr("data-req-id", reqId);
		$("#deleteAipBtn").attr("data-pay-sts", paySts);
		
		$("#deleteAipModal").modal("show");
	});
	
	$("#deleteAipBtn").click(function(){
		$("#mainLoader").show();
		
		var inputData = { 
			reqId : $("#deleteAipBtn").attr("data-req-id"),
			paySts: $("#deleteAipBtn").attr("data-pay-sts"),
		};
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/payment/delete',
			type: 'POST',
			dataType : 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					$("#deleteAipModal").modal("hide");
					
					getProposals();
					
					customAlert("#mainNotify", "alert-success", "Proposal deleted successfully.");
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#popupNotify", "alert-danger", errList);
					$("#mainLoader").hide();
				}
				
				
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});
		
	});
	
	$("#submitImvcPymnt").click(function(){
		$("#mainLoader").show();
		$("#viewAutoInvcPymntModal .select2-selection").removeClass("border-danger");
		
		var errFlg = false;
		var invcActnSts = $("#invcActnSts").val();
		var bankCd = $("#propBankCode").val();
		var acctNo = $("#propBankAccNo").val();
		
		if( bankCd == "" || bankCd == null || bankCd == undefined ){
			errFlg = true;
			$("#propBankCode").next().find(".select2-selection").addClass("border-danger");
		}
		
		if( acctNo == "" || acctNo == null || acctNo == undefined ){
			errFlg = true;
			$("#propBankAccNo").next().find(".select2-selection").addClass("border-danger");
		}
		
		if(errFlg){
			return;
		}else{
			if( invcActnSts == "A" ){
				aprvBlockPropInvc( invcActnSts);
			} else if( invcActnSts == "R" ){
				$("#blockRemark").removeClass("border-danger");
				$("#blockRemark").val("");
				$("#invcBlockConfirmModal").modal("show");
			}
		}
	});
	
});


function mmddyyyyToDbFormat(date=""){
	var returnDt = "";
	if( date != "" ){
		var dtArr = date.split("/");
		returnDt = dtArr[2] + "" + dtArr[0] + "" + dtArr[1];
	}
	return returnDt;
}
var count=0;
function getProposals(pageNo=0){
	$("#mainLoader").show();
	
	var venId = $("#propVndrFltr").val();
	var vchrNo = $("#propVchrNoFltr").val().trim();
	
	if( venId == null || venId == undefined ){
		venId = "";
	}
		
	var inputData = { 
		reqId : "",
		paySts: "",
		venId: venId,
		vchrNo: vchrNo,
		pageNo: pageNo,
		issueDate: gblIssueDate,
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/payment/read',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblPayment.length;
				var tbody = '', reqSts = "", actnBtn = "", reqRmk = "", actnOn = "", pymntMthdArr = [], pymntMthdStr = '', 
				pymntMthdCls = '', amtStr = '';
				if(loopSize!=0)
					glblReqIdCount=(response.output.fldTblPayment[0].totalReqId+1);
				else
					glblReqIdCount=(loopSize+1);
				for( var i=0; i<loopSize; i++ ){
					reqSts = "", actnBtn = "", reqRmk = "", actnOn = "", pymntMthdArr = [], pymntMthdStr = '';
					
					actnBtn = '<i class="icon-menu icon-info display-5 text-primary view-auto-invc-pymnt c-pointer font-weight-bold" title="View"></i>';
					
					if( response.output.fldTblPayment[i].paySts == "P" ){
						reqSts = '<span class="badge badge-warning">Pending</span>';
						actnBtn = '<i class="icon-menu icon-note text-warning edit-auto-invc-pymnt c-pointer font-weight-bold" data-pay-sts="P" title="Edit"></i>';
						actnBtn += '<i class="icon-menu icon-trash text-danger delete-auto-invc-pymnt c-pointer font-weight-bold ml-1 fs-16" title="Delete"></i>';
					}else if( response.output.fldTblPayment[i].paySts == "A" ){
						reqSts = '<span class="badge badge-success">Approved</span>';
						actnBtn += '<i class="icon-menu icon-cloud-download c-pointer font-weight-bold display-5 text-info ml-1 dwnld-ach-file" data-pay-sts="A" title="Download APLink File"></i>';
						if( response.output.fldTblPayment[i].flSentSts ){
							actnBtn += '<i class="icon-menu icon-cloud-upload display-5 text-muted ml-1" data-pay-sts="A" title="Sent to Bank Server"></i>';
						}else{
							actnBtn += '<i class="icon-menu icon-cloud-upload c-pointer font-weight-bold display-5 text-success ml-1 sftp-ach-file" data-pay-sts="A" title="Send to Bank Server"></i>';
						}
						//actnBtn += '<a href="assets/Remittance.pdf" target="_blank"> <i class="icon-menu   icon-notebook c-pointer font-weight-bold display-5 text-success ml-0 remit-file" data-pay-sts="A" title="Download Remittance"></i></a>';
					}else if( response.output.fldTblPayment[i].paySts == "R" ){
						reqSts = '<span class="badge badge-danger">Rejected</span>';
						actnBtn = '<i class="icon-menu icon-note text-warning edit-auto-invc-pymnt c-pointer font-weight-bold" data-pay-sts="R" title="Edit"></i>';
					}else if( response.output.fldTblPayment[i].paySts == "S" ){
						reqSts = '<span class="badge badge-primary">Sent For Approval</span>';
					}
					
					if( response.output.fldTblPayment[i].actnRmk != undefined ){
						reqRmk = response.output.fldTblPayment[i].actnRmk;
					}
					
					if( response.output.fldTblPayment[i].reqActnOnStr != undefined ){
						actnOn = response.output.fldTblPayment[i].reqActnOnStr;
					}
					
					if( response.output.fldTblPayment[i].payMthd != undefined ){
						pymntMthdArr = response.output.fldTblPayment[i].payMthd.split(",");
					}
					
					if( pymntMthdArr.length ){
						for( var y=0; y<pymntMthdArr.length; y++ ){
							var reminder = y % 5;
							if( reminder == 0 ){
								pymntMthdCls = "badge-primary";
							}else if( reminder == 1 ){
								pymntMthdCls = "badge-info";
							}else if( reminder == 2 ){
								pymntMthdCls = "badge-danger";
							}else if( reminder == 3 ){
								pymntMthdCls = "badge-success";
							}else if( reminder == 4 ){
								pymntMthdCls = "badge-warning";
							}
							
							pymntMthdStr += '<span class="badge '+ pymntMthdCls +'">'+ pymntMthdArr[y] +'</span> &nbsp;';
						}
					}
					
					//amtStr = parseFloat(response.output.fldTblPayment[i].amtTotalStr).toFixed(2);
					amtStr = response.output.fldTblPayment[i].amtTotalStr;
					if( response.output.fldTblPayment[i].cry != undefined ){
						amtStr += " " + response.output.fldTblPayment[i].cry;
					}
					count=count+1;
					tbody +='<tr data-pay-mthd="'+ response.output.fldTblPayment[i].payMthd +'" data-pay-sts="'+ response.output.fldTblPayment[i].paySts +'" data-fl-nm="' + response.output.fldTblPayment[i].flNm + '" data-req-id="' + response.output.fldTblPayment[i].reqId + '" data-req-dt="' + response.output.fldTblPayment[i].crtdDtStr + '" data-tot-amt="'+ amtStr +'">'+
								'<td>'+ (count) +'</td>'+
								'<td>'+
									'<h6 class="text-primary mb-1">'+ response.output.fldTblPayment[i].reqId +'</h6>'+
									'<p class="mb-1">'+ response.output.fldTblPayment[i].reqDtStr +'</p>'+
									'<p class="m-0">'+ response.output.fldTblPayment[i].crtdBy +'</p>'+
								'</td>'+
								'<td>'+ response.output.fldTblPayment[i].cmpyId +'</td>'+
								'<td>'+ amtStr +'</td>'+
								'<td>'+ response.output.fldTblPayment[i].reqActnBy +'</td>'+
								'<td>'+ reqRmk +'</td>'+
								'<td>'+ pymntMthdStr +'</td>'+
								'<td>'+ actnOn +'</td>'+
								'<td>'+ reqSts +'</td>'+
								'<td>'+ actnBtn +'</td>'+
							'<\tr>';
				}
				
				if( pageNo == 0 ){
					$("#aipCreatedTbl tbody").html( tbody );
				}else{
					$("#aipCreatedTbl tbody").append( tbody );
				}
				
				if( response.output.eof ){
					$("#loadMoreAip").attr("disabled", "disabled");
					$("#loadMoreAip").addClass("disabled");
				}else{
					$("#loadMoreAip").removeAttr("disabled");
					$("#loadMoreAip").removeClass("disabled");
				}
				
				$("#loadMoreAip").attr("data-page-no", parseInt(pageNo)+1);
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}


function searchInvoices(){
	$("#mainLoader").show();
		
	$("thead th input[type='text']").val('');
	$('#stxEntryContent table').floatThead('destroy');
	
	var payMthd = '';
	var invcDt = $("#aipInvcDt").val();
	var dueDt = $("#aipDueDt").val();
	var discDt = $("#aipDiscDt").val();
	var disbNo = $("#aipDbNoId").val();
	
	$("#aipPymntType option:selected").each(function(index, option){
		payMthd += (payMthd.length) ? "," : "";
		payMthd += $(option).attr("data-mthd-id");
	});
	
	invcDt = (invcDt == null || invcDt == undefined ) ? "" : invcDt;
	dueDt = (dueDt == null || dueDt == undefined ) ? "" : dueDt;
	discDt = (discDt == null || discDt == undefined ) ? "" : discDt;
	disbNo = (disbNo == null || disbNo == undefined ) ? "" : disbNo;
	
	var invcStrtDate = "";
	var invcEndDate = "";
	if( invcDt != "" ){
		invcStrtDate = invcDt.split(" - ")[0];
		invcEndDate = invcDt.split(" - ")[1];
	}
	
	var dueStrtDate = "";
	var dueEndDate = "";
	
	if( dueDt != "" ){
		dueStrtDate = dueDt.split(" - ")[0];
		dueEndDate = dueDt.split(" - ")[1];
	}
	
	var discStrtDate = "";
	var discEndDate = "";
	
	if( discDt != "" ){
		discStrtDate = discDt.split(" - ")[0];
		discEndDate = discDt.split(" - ")[1];
	}
	
	$("#batchTotalVal").html("0.00");
	$("#batchTotalCount").html("0");
	
	var inputData = { 
		invcDt : invcStrtDate,
		invcDtTo : invcEndDate,
		dueDt : dueStrtDate,
		dueDtTo : dueEndDate,
		discDt : discStrtDate,
		discDtTo : discEndDate,
		venIdFrm : $("#aipVndrIdFrm").val(),
		venIdTo: $("#aipVndrIdTo").val(),
		payMthd: payMthd,
		disbNo:disbNo,
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/payment/read-inv',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
			
			
				var loopSize = response.output.fldTblDoc.length;
				var poStr = "", checkboxStr = "", vchrNoStr = '', payMthds = "", dfltOption = "";
				var tbl = '<table class="table">'+
							'<thead>'+
								'<tr class="thead-clr">'+
									'<th width="50px">'+
										'<div class="form-check form-check-flat m-0">'+
											'<label class="form-check-label">'+
											  	'<input type="checkbox" class="form-check-input" id="selectAllInvc" >'+
												'<i class="input-helper"></i>'+
											'</label>'+
								 		'</div>'+
								 	'</th>'+
									'<th width="250px">Vendor ID</th>'+
									'<th width="150px">Invoice No.</th>'+
									'<th width="100px">Invoice Date</th>'+
									'<th width="50px">Due Date</th>'+
									'<th width="100px">PO</th>'+
									'<th width="100px">Voucher No.</th>'+
									'<th width="50px">Branch</th>'+
									'<th width="80px" class="text-right">Amount</th>'+
									'<th width="50px" class="text-right">Discount</th>'+
									'<th width="80px" class="text-right">Total</th>'+
									'<th width="100px">Entered Date</th>'+
									'<th width="100px">Disc Date</th>'+
									'<th width="100px">Pay Method</th>'+
									'<th width="">Payee Note</th>'+
								'</tr>'+
								'<tr class="filter-action-row hide-filter">'+
									'<th></th>'+
									'<th><input class="custom-text-filter form-control vendorFilter" type="text" /></th>'+
									'<th><input class="custom-text-filter form-control fieldsFilter" type="text" /></th>'+
									'<th><input type="text" name="daterange" class="form-control invcDtFilter"></th>'+
									'<th><input type="text" name="daterange" class="form-control dueDtFilter"></th>'+
									'<th></th>'+
									'<th><input class="custom-text-filter form-control voucherFilter" type="text" /></th>'+
									'<th></th>'+
									'<th></th>'+
									'<th></th>'+
									'<th></th>'+
									'<th></th>'+
									'<th></th>'+
									'<th></th>'+
									'<th></th>'+
								'</tr>'+
							'</thead>'+
							'<tbody>';
					
				for( var i=0; i<loopSize; i++ ){
					vchrNoStr = '', payMthds = "";
					checkboxStr = '<div class="form-check form-check-flat m-0">'+
									'<label class="form-check-label">'+
									  '<input type="checkbox" class="form-check-input" name="select-invoices">'+
									'<i class="input-helper"></i></label>'+
								 '</div>';
					if( response.output.fldTblDoc[i].poNo == 0 ){
						poStr = "";
					}else{
						poStr = response.output.fldTblDoc[i].poPfx +'-'+ response.output.fldTblDoc[i].poNo;
						
						if( response.output.fldTblDoc[i].poItm != "0" ){
							poStr += '-' + response.output.fldTblDoc[i].poItm;
						}
					}
					
					if( response.output.fldTblDoc[i].logSts == "N" ){
						checkboxStr = '';
					}
					
					if( response.output.fldTblDoc[i].vchrPfx != undefined && response.output.fldTblDoc[i].vchrPfx.trim() != "null" ){
						vchrNoStr = response.output.fldTblDoc[i].vchrPfx.trim() + "-" + response.output.fldTblDoc[i].vchrNo.trim();
					}
					
					for( var j=0; j<response.output.fldTblDoc[i].payMthds.length; j++){
						dfltOption = (response.output.fldTblDoc[i].payMthds[j].mthdDflt) ? "selected" : "";
						payMthds += '<option data-mthd-cd="'+ response.output.fldTblDoc[i].payMthds[j].mthdCd +'" value="'+ response.output.fldTblDoc[i].payMthds[j].mthdId +'" '+ dfltOption +'>'+ response.output.fldTblDoc[i].payMthds[j].mthdNm +'</option>';
					}
					
					var discStr = "";
					if(response.output.fldTblDoc[i].discAmtStr != 0)
					{
						discStr = response.output.fldTblDoc[i].discAmtStr;
					}
					
					//console.log(poStr);
					
					
						tbl += '<tr data-cry="'+ response.output.fldTblDoc[i].vchrCry +'" data-ctl-no="'+ response.output.fldTblDoc[i].ctlNo +'" data-invoice-no="'+ response.output.fldTblDoc[i].vchrInvNo +'" data-vchr-no="'+ vchrNoStr +'"   data-vchr-amt="'+ response.output.fldTblDoc[i].vchrAmt +'" data-vchr-disc-amt="'+ response.output.fldTblDoc[i].discAmt +'"  data-vchr-disc-flg="'+ response.output.fldTblDoc[i].discFlg +'">'+
								'<td>'+ checkboxStr +'</td>'+
								'<td>'+ response.output.fldTblDoc[i].vchrVenId + "-" + response.output.fldTblDoc[i].vchrVenNm +'</td>'+
								'<td>'+ response.output.fldTblDoc[i].vchrInvNo +'</td>'+
								'<td>'+ response.output.fldTblDoc[i].vchrInvDtStr +'</td>'+
								'<td>'+ response.output.fldTblDoc[i].vchrDueDtStr +'</td>'+
								'<td>'+ poStr +'</td>'+
								'<td>'+ vchrNoStr +'</td>'+
								'<td>'+ response.output.fldTblDoc[i].vchrBrh +'</td>'+
								'<td class="text-right">'+ response.output.fldTblDoc[i].vchrAmtStr +'</td>'+
								'<td class="text-right">'+ discStr +'</td>'+
								'<td class="text-right">'+ response.output.fldTblDoc[i].totAmtStr +' '+ response.output.fldTblDoc[i].vchrCry +'</td>'+
								'<td>'+ response.output.fldTblDoc[i].crtDttsStr +'</td>'+
								'<td>'+ response.output.fldTblDoc[i].vchrDiscDtStr +'</td>'+
								'<td>'+
									'<select class="form-control vndr-pymnt-mthd select2" style="width: 100%">'+
										payMthds +
									'</select>'+
								'</td>'+
								'<td><input typr="text" class="form-control payee-note" /> </td>'+
							'</tr>';
				}
				tbl += '</tbody></table>';
				$("#stxEntryContent").html(tbl);
				
				$("input[type='checkbox'][name='select-invoices']").trigger("change")
					
				var $table = $('#stxEntryContent table');
								
				$table.floatThead({
					useAbsolutePositioning: true,
					scrollContainer: function($table) {
						return $table.closest('.table-responsive');
					}
				});
				$table.floatThead('reflow');
				
				if( loopSize ){
					$("#invcDrpActns").show();
				}else{
					$("#invcDrpActns").hide();
				}
				
				
				/**
				  * Getting Hold Invoices 
				  */
				loopSize = response.output.fldTblInvHold.length;
				poStr = "", logStatus = "", logRsn = "", vchrNoStr = "";
				tbl = '<table class="table table-hover">'+
							'<thead>'+
								'<tr>'+
									'<th width="50px">Info</th>'+
									'<th>Vendor ID</th>'+
									'<th>Invoice No.</th>'+
									'<th>Invoice Date</th>'+
									'<th>Due Date</th>'+
									'<th>PO</th>'+
									'<th>Voucher No.</th>'+
									'<th>Branch</th>'+
									'<th class="text-right">Amount</th>'+
									'<th class="text-right">Discount</th>'+
									'<th>Entered Date</th>'+
									'<th>Disc Date</th>'+
								'</tr>'+
								'<tr class="filter-action-row hide-filter">'+
									'<th></th>'+
									'<th><input class="custom-text-filter form-control vendorFilter" type="text" /></th>'+
									'<th><input class="custom-text-filter form-control fieldsFilter" type="text" /></th>'+
									'<th><input type="text" name="daterange" class="form-control invcDtFilter"></th>'+
									'<th><input type="text" name="daterange" class="form-control dueDtFilter"></th>'+
									'<th></th>'+
									'<th><input class="custom-text-filter form-control voucherFilter" type="text" /></th>'+
									'<th></th>'+
									'<th></th>'+
									'<th></th>'+
									'<th></th>'+
									'<th></th>'+
								'</tr>'+
							'</thead>'+
							'<tbody>';
				
				for( var i=0; i<loopSize; i++ ){
					logStatus = "", logRsn = "";
					
					if( response.output.fldTblInvHold[i].poNo == 0 ){
						poStr = "";
					}else{
						poStr = response.output.fldTblInvHold[i].poPfx +'-'+ response.output.fldTblInvHold[i].poNo;
						
						if( response.output.fldTblInvHold[i].poItm != "0" ){
							poStr += '-' + response.output.fldTblInvHold[i].poItm;
						}
					}
										
					if( response.output.fldTblInvHold[i].pymntStsRsn != undefined && response.output.fldTblInvHold[i].pymntStsRsn.trim() != "" ){
						logRsn = response.output.fldTblInvHold[i].pymntStsRsn.trim();
					}
					if( response.output.fldTblInvHold[i].transStsRsn != undefined && response.output.fldTblInvHold[i].transStsRsn.trim() != "" ){
						logRsn += (logRsn.trim().length > 0) ? "<br>" : "";
						logRsn += response.output.fldTblInvHold[i].transStsRsn.trim();
					}
					if( response.output.fldTblInvHold[i].holdRsn != undefined && response.output.fldTblInvHold[i].holdRsn.trim() != "" ){
						logRsn += (logRsn.trim().length > 0) ? "<br>" : "";
						logRsn += response.output.fldTblInvHold[i].holdRsn.trim();
					}
					logStatus = '<i class="icon-info c-pointer text-danger font-weight-bold display-5" title="'+ logRsn +'" data-toggle="tooltip" data-html="true"></i>';
					
					if( response.output.fldTblInvHold[i].vchrPfx != undefined && response.output.fldTblInvHold[i].vchrPfx.trim() != "null" ){
						vchrNoStr = response.output.fldTblInvHold[i].vchrPfx.trim() + "-" + response.output.fldTblInvHold[i].vchrNo.trim();
					}
					
					var discStr = "";
					if(response.output.fldTblInvHold[i].discAmtStr != 0)
					{
						discStr = response.output.fldTblInvHold[i].discAmtStr +' '+ response.output.fldTblInvHold[i].vchrCry;
					}
					
					tbl += '<tr>'+
								'<td>'+ logStatus +'</td>'+
								'<td>'+ response.output.fldTblInvHold[i].vchrVenId +'-'+ response.output.fldTblInvHold[i].vchrVenNm +'</td>'+
								'<td>'+ response.output.fldTblInvHold[i].vchrInvNo +'</td>'+
								'<td>'+ response.output.fldTblInvHold[i].vchrInvDtStr +'</td>'+
								'<td>'+ response.output.fldTblInvHold[i].vchrDueDtStr +'</td>'+
								'<td>'+ poStr +'</td>'+
								'<td>'+ vchrNoStr +'</td>'+
								'<td>'+ response.output.fldTblInvHold[i].vchrBrh +'</td>'+
								'<td class="text-right">'+ response.output.fldTblInvHold[i].vchrAmt +' '+ response.output.fldTblInvHold[i].vchrCry +'</td>'+
								'<td>'+ discStr +'</td>'+
								'<td>'+ response.output.fldTblInvHold[i].crtDttsStr +'</td>'+
								'<td>'+ response.output.fldTblInvHold[i].vchrDiscDtStr +'</td>'+
							'</tr>';
				}
				tbl += '</tbody></table>';
				$("#blockedVoucherTblWrapper").html(tbl);
				var $table = $('#blockedVoucherTblWrapper table');
				//fixedTableHeader($table);
				
				$("#filterVoucherResults").slideDown();
				$("body").tooltip({ selector: '[data-toggle=tooltip]', placement: 'top' });
				$(".select2").select2();
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}

function generatePaymentFile(){
	$("#mainLoader").show();
	$(".input-control").removeClass("border-danger");
		$("#aprv").next().find(".select2-selection").removeClass("border-danger");
		
		var errFlg = false;
		var reqId = $("#aipRqstId").val();
		var reqDt = mmddyyyyToDbFormat( $("#aipDt").val() );
		//var cmpyId = cmpyId;
		var cmpyId;
		var invDt = mmddyyyyToDbFormat( $("#aipInvcDt").val() );
		var venIdFrm = $("#aipVndrIdFrm").val();
		var venIdTo = $("#aipVndrIdTo").val();
		var ctlNoArr = [], vchrNoArr = [], chkNoArr=[], invcAddtnlData = [];
		var pymntMethods = [], cryArr = [];
		
		$("#stxEntryContent table tbody input[type='checkbox'][name='select-invoices']:checked").each(function(index, checkbox){
			ctlNoArr.push( $(checkbox).closest("tr").attr("data-ctl-no") );
			vchrNoArr.push( $(checkbox).closest("tr").attr("data-vchr-no") );
			chkNoArr.push( $(checkbox).closest("tr").attr("data-chk-no") );
			
			var jsonObj = {
				ctlNo: $(checkbox).closest("tr").attr("data-ctl-no"),
				vchrNo: $(checkbox).closest("tr").attr("data-vchr-no"),
				pymntMthd: $(checkbox).closest("tr").find(".vndr-pymnt-mthd").val(),
				note: $(checkbox).closest("tr").find(".payee-note").val(),
				chkNo: $(checkbox).closest("tr").attr("data-chk-no"),
			}
			pymntMethods.push($(checkbox).closest("tr").find(".vndr-pymnt-mthd option:selected").attr("data-mthd-cd"));
			cryArr.push($(checkbox).closest("tr").attr("data-cry"));
			invcAddtnlData.push(jsonObj);
		});
		
		console.log(cryArr);
		var uniquePymntMethods = pymntMethods.filter(function(itm, i, pymntMethods) {
								    return i == pymntMethods.indexOf(itm);
								});
								
		var uniqueCryArr = cryArr.filter(function(itm, i, cryArr) {
								    return i == cryArr.indexOf(itm);
								});
		
		if( uniquePymntMethods.length > 1 && uniquePymntMethods.indexOf("CHK") > -1 ){
			errFlg = true;
			customAlert("#mainNotify", "alert-danger", "Check and Electronic payments cannot be merged in a single proposal.");
		}else if( uniqueCryArr.length > 1 ){
			errFlg = true;
			customAlert("#mainNotify", "alert-danger", "Different currency payments cannot be merged in a single proposal.");
		}
		
		if( reqDt == "" ){
			errFlg = true;
			$("#aipDt").addClass("border-danger");
		}
		
		if( reqId == "" ){
			errFlg = true;
			$("#aipRqstId").addClass("border-danger");
		}
		
		if( ctlNoArr.length == 0 ){
			errFlg = true;
			customAlert("#mainNotify", "alert-danger", "Please select any invoice.");
		}
		
		if( errFlg == false ){
				 
					var inputData = {
						cmpyId : glblCmpyId,
						reqId : reqId,
						//invList : ctlNoArr,
						//invList : vchrNoArr,
						invcAddtnlData: invcAddtnlData,
						reqUserBy : "",
						reqActnBy : "",
						paySts : "S",
						issueDate: gblIssueDate,
						
					};
					console.log("1527 saveSendProposal( inputData );	");
					saveSendProposal( inputData );								
		}
		else{
			$("#aprv").next().find(".select2-selection").addClass("border-danger");
			$("#mainLoader").hide();
		}
}

function searchInvoicesESP(){
	$("#mainLoader").show();
		
	$("thead th input[type='text']").val('');
	$('#stxEntryContent table').floatThead('destroy');
	
	var payMthd = '';
	var invcDt = $("#aipInvcDt").val();
	var dueDt = $("#aipDueDt").val();
	var discDt = $("#aipDiscDt").val();
	var disbNo = $("#aipDbNoId").val();
	var venIdF = $("#aipVndrIdFrm").val();
	var	venIdT = $("#aipVndrIdTo").val();
	var disbChkNetAmt=0;
	
	$("#aipPymntType option:selected").each(function(index, option){
		payMthd += (payMthd.length) ? "," : "";
		payMthd += $(option).attr("data-mthd-id");
	});
	
	invcDt = (invcDt == null || invcDt == undefined ) ? "" : invcDt;
	dueDt = (dueDt == null || dueDt == undefined ) ? "" : dueDt;
	discDt = (discDt == null || discDt == undefined ) ? "" : discDt;
	disbNo = (disbNo == null || disbNo == undefined ) ? "" : disbNo;
	venIdF = (venIdF == null || venIdF == undefined ) ? "" : venIdF;
	venIdT = (venIdT == null || venIdT == undefined ) ? "" : venIdT;
	
	var invcStrtDate = "";
	var invcEndDate = "";
	if( invcDt != "" ){
		invcStrtDate = invcDt.split(" - ")[0];
		invcEndDate = invcDt.split(" - ")[1];
	}
	
	var dueStrtDate = "";
	var dueEndDate = "";
	
	if( dueDt != "" ){
		dueStrtDate = dueDt.split(" - ")[0];
		dueEndDate = dueDt.split(" - ")[1];
	}
	
	var discStrtDate = "";
	var discEndDate = "";
	
	if( discDt != "" ){
		discStrtDate = discDt.split(" - ")[0];
		discEndDate = discDt.split(" - ")[1];
		gblIssueDate=discStrtDate;
	}
	
	$("#batchTotalVal").html("0.00");
	$("#batchTotalCount").html("0");
	var batchTotalAmt = 0.00;
	var batchTotalCount = 0;
	var inputData = { 
		invcDt : invcStrtDate,
		invcDtTo : invcEndDate,
		dueDt : dueStrtDate,
		dueDtTo : dueEndDate,
		discDt : discStrtDate,
		discDtTo : discEndDate,
		venIdFrm : venIdF,
		venIdTo: venIdT,
		payMthd: payMthd,
		disbNo:disbNo,
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/payment/read-inv-esp',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
			
			
				var loopSize = response.output.fldTblDoc.length;
				var poStr = "", checkboxStr = "", vchrNoStr = '', payMthds = "", dfltOption = "";
				var tbl = '<table class="table">'+
							'<thead>'+
								'<tr class="thead-clr">'+
									'<th width="50px" class="display-none">'+
										'<div class="form-check form-check-flat m-0 ">'+
											'<label class="form-check-label">'+
											  	'<input type="checkbox" class="form-check-input" id="selectAllInvc" >'+
												'<i class="input-helper"></i>'+
											'</label>'+
								 		'</div>'+
								 	'</th>'+
								 	'<th width="80px" class="text-right">Chk No</th>'+
								 	'<th width="80px" class="text-right">Disb No</th>'+
									'<th width="100px">Jrnl Dt</th>'+
									'<th width="100px">Issue Dt</th>'+
									'<th width="250px">Vendor ID</th>'+
									'<th width="150px">Invc No.</th>'+
									'<th width="100px">Invc Dt</th>'+
									'<th width="50px">Due Dt</th>'+
									'<th width="100px">PO</th>'+
									'<th width="100px">Voucher No.</th>'+
									'<th width="50px">Branch</th>'+
									'<th width="80px" class="text-right">Vou.Amt</th>'+
									'<th width="80px" class="text-right">Disbursed Amt</th>'+
									//'<th width="50px" class="text-right">Discount</th>'+
									//'<th width="80px" class="text-right">Total</th>'+
									//'<th width="100px">Pay Method</th>'+
									//'<th width="">Payee Note</th>'+
								'</tr>'+
								'<tr class="filter-action-row hide-filter">'+
									'<th class="display-none"></th>'+
									'<th><input class="custom-text-filter form-control chkNoFilter" type="text" /></th>'+
									'<th><input class="custom-text-filter form-control disbNoFilter" type="text" /></th>'+
									'<th><input type="text" name="daterange" class="form-control jrnlDtFilter"></th>'+
									'<th><input type="text" name="daterange" class="form-control issueDtFilter"></th>'+
									'<th><input class="custom-text-filter form-control vendorFilter" type="text" /></th>'+
									'<th><input class="custom-text-filter form-control fieldsFilter" type="text" /></th>'+
									'<th><input type="text" name="daterange" class="form-control invcDtFilter"></th>'+
									'<th><input type="text" name="daterange" class="form-control dueDtFilter"></th>'+
									'<th></th>'+
									'<th><input class="custom-text-filter form-control voucherFilter" type="text" /></th>'+
									'<th></th>'+
									'<th></th>'+
									'<th></th>'+
									//'<th></th>'+
									//'<th></th>'+
									//'<th></th>'+
								'</tr>'+
							'</thead>'+
							'<tbody>';

				var prevCheckNumber = '', prevVenId = '', prevCry='', prevVendor = '', venTotal = 0, venBalance = 0;
				var tblTotal='';
				for( var i=0; i<loopSize; i++ ){
					vchrNoStr = '', payMthds = "";
					checkboxStr = '<div class="form-check form-check-flat m-0">'+
									'<label class="form-check-label">'+
									  '<input type="checkbox" class="form-check-input" name="select-invoices">'+
									'<i class="input-helper"></i></label>'+
								 '</div>';
					if( response.output.fldTblDoc[i].poNo == 0 ){
						poStr = "";
					}else{
						poStr = response.output.fldTblDoc[i].poPfx +'-'+ response.output.fldTblDoc[i].poNo;
						
						if( response.output.fldTblDoc[i].poItm != "0" ){
							poStr += '-' + response.output.fldTblDoc[i].poItm;
						}
					}
					
					if( response.output.fldTblDoc[i].logSts == "N" ){
						checkboxStr = '';
					}
					
					if( response.output.fldTblDoc[i].vchrPfx != undefined && response.output.fldTblDoc[i].vchrPfx.trim() != "null" ){
						vchrNoStr = response.output.fldTblDoc[i].vchrPfx.trim() + "-" + response.output.fldTblDoc[i].vchrNo.trim();
					}
					
					for( var j=0; j<response.output.fldTblDoc[i].payMthds.length; j++){
						dfltOption = (response.output.fldTblDoc[i].payMthds[j].mthdDflt) ? "selected" : "";
						payMthds += '<option data-mthd-cd="'+ response.output.fldTblDoc[i].payMthds[j].mthdCd +'" value="'+ response.output.fldTblDoc[i].payMthds[j].mthdId +'" '+ dfltOption +'>'+ response.output.fldTblDoc[i].payMthds[j].mthdNm +'</option>';
					}
					
					var discStr = "";
					if(response.output.fldTblDoc[i].discAmtStr != 0)
					{
						discStr = response.output.fldTblDoc[i].discAmtStr;
					}
					
					//console.log(poStr);
					
						if( prevCheckNumber != "" && prevCheckNumber != response.output.fldTblDoc[i].chkNo.trim() ){
														
								tblTotal += '<tr class="vndr-total font-weight-bold text-dark">'+
										'<td class="display-none"></td>'+
										'<td></td>'+										
										'<td></td>'+
										'<td></td>'+
										'<td></td>'+
										'<td colspan="3">'+ prevVendor +' (Total & Balance):</td>'+
										//'<td></td>'+
										//'<td></td>'+
										
										//'<td class="check-col"></td>'+
										'<td></td>'+
										'<td class="pymnt-mthd-col"></td>'+
										'<td></td>'+
										//'<td></td>'+
										
										'<td class="text-right"></td>'+
										//'<td></td>'+
										'<td class="text-right">'+ venTotal.toFixed(2) +' '+ prevCry +'</td>'+
										//'<td class="text-right">'+ (venTotal-venBalance).toFixed(2) +' '+ prevCry +'</td>'+
										'<td class="text-right">'+ disbChkNetAmt.toFixed(2) +' '+ prevCry +'</td>'+
										
										/*'<td class="check-gl">'+ glVal +'</td>'+*/
									'</tr>';
								tbl += tblTotal;
								tblTotal='';
								venTotal = 0;
								batchTotalAmt +=venBalance;
							}
					
					
						tbl += '<tr data-cry="'+ response.output.fldTblDoc[i].vchrCry +'" data-ctl-no="'+ response.output.fldTblDoc[i].ctlNo +'" data-invoice-no="'+ response.output.fldTblDoc[i].vchrInvNo +'" data-vchr-no="'+ vchrNoStr +'"   data-vchr-amt="'+ response.output.fldTblDoc[i].vchrAmt +'" data-vchr-disc-amt="'+ response.output.fldTblDoc[i].discAmt +'"  data-vchr-disc-flg="'+ response.output.fldTblDoc[i].discFlg +'" data-chk-no="'+ response.output.fldTblDoc[i].chkNo +'" data-chkNet-amt="'+ response.output.fldTblDoc[i].chkNetAmt+'">'+
								'<td class="display-none">'+ checkboxStr +'</td>'+
								'<td class="text-right">'+ response.output.fldTblDoc[i].chkNo +'</td>'+
								'<td class="text-right">'+ response.output.fldTblDoc[i].disbNo +'</td>'+
								'<td>'+ response.output.fldTblDoc[i].crtDttsStr +'</td>'+
								'<td>'+ response.output.fldTblDoc[i].vchrDiscDtStr +'</td>'+
								'<td>'+ response.output.fldTblDoc[i].vchrVenId + "-" + response.output.fldTblDoc[i].vchrVenNm +'</td>'+
								'<td>'+ response.output.fldTblDoc[i].vchrInvNo +'</td>'+
								'<td>'+ response.output.fldTblDoc[i].vchrInvDtStr +'</td>'+
								'<td>'+ response.output.fldTblDoc[i].vchrDueDtStr +'</td>'+
								//'<td>'+ poStr +'</td>'+
								'<td>'+ '' +'</td>'+
								'<td>'+ vchrNoStr +'</td>'+
								'<td>'+ response.output.fldTblDoc[i].vchrBrh +'</td>'+
								'<td class="text-right">'+ response.output.fldTblDoc[i].vchrAmtStr +'</td>'+
								'<td class="text-right"></td>'+
								
								//'<td class="text-right">'+ discStr +'</td>'+
								//'<td class="text-right">'+ response.output.fldTblDoc[i].totAmtStr +' '+ response.output.fldTblDoc[i].vchrCry +'</td>'+
								//'<td class="text-right">'+ response.output.fldTblDoc[i].totAmtStr +'</td>'+
								
								/*'<td>'+
									'<select class="form-control vndr-pymnt-mthd select2" style="width: 100%">'+
										payMthds +
									'</select>'+
								'</td>'+
								'<td><input typr="text" class="form-control payee-note" /> </td>'+*/
							'</tr>';
							
							/*checkAmount=response.output.fldTblDoc[i].totAmtStr.replace(/,/g, '');
							checkAmount = parseFloat(checkAmount);
							if(checkNumber=='' || checkNumber==response.output.fldTblDoc[i].chkNo)
							{
								//checkNumberTotal+=parseFloat(response.output.fldTblDoc[i].totAmtStr.toString()).toFixed(2);
								checkNumberTotal= parseFloat(checkNumberTotal) + parseFloat(checkAmount);
								//checkNumber=response.output.fldTblDoc[i].chkNo;
							}
							else
							{
								console.log(checkNumber+': '+checkNumberTotal);
								//checkNumber=response.output.fldTblDoc[i].chkNo;
								checkNumberTotal=parseFloat("0.00");
							}
							checkNumber=response.output.fldTblDoc[i].chkNo;*/
							
							prevCheckNumber = response.output.fldTblDoc[i].chkNo.trim();
							//prevCry = response.output.fldTblPayment[0].paramInvList[i].vchrCry;
							venTotal += parseFloat(response.output.fldTblDoc[i].totAmtStr.replace(/,/g, ''));
							venBalance=response.output.fldTblDoc[i].chkNetAmt;							
							batchTotalCount +=1;
							prevVendor = response.output.fldTblDoc[i].vchrVenId.trim() + "-" + response.output.fldTblDoc[i].vchrVenNm.trim();
							disbChkNetAmt = response.output.fldTblDoc[i].chkNetAmt;
		
							
				}
				
				if( prevCheckNumber != ""){
														
								tblTotal += '<tr class="vndr-total font-weight-bold text-dark">'+
										'<td class="display-none"></td>'+
										'<td></td>'+										
										'<td></td>'+
										'<td></td>'+
										'<td></td>'+
										'<td colspan="3">'+ prevVendor +' (Total & Balance):</td>'+
										//'<td></td>'+
										//'<td></td>'+
										
										//'<td class="check-col"></td>'+
										'<td></td>'+
										'<td class="pymnt-mthd-col"></td>'+
										'<td></td>'+
										//'<td></td>'+
										
										'<td class="text-right"></td>'+
										//'<td></td>'+
										'<td class="text-right">'+ venTotal.toFixed(2) +' '+ prevCry +'</td>'+
										//'<td class="text-right">'+ (venTotal-venBalance).toFixed(2) +' '+ prevCry +'</td>'+
										'<td class="text-right">'+ disbChkNetAmt.toFixed(2) +' '+ prevCry +'</td>'+
										/*'<td class="check-gl">'+ glVal +'</td>'+*/
									'</tr>';
								tbl += tblTotal;
								tblTotal='';
								venTotal = 0;
								batchTotalAmt +=venBalance;
				}
				tbl += tblTotal;
				tbl += '</tbody></table>';
				$("#stxEntryContent").html(tbl);
				
				$("input[type='checkbox'][name='select-invoices']").trigger("change")
					
				var $table = $('#stxEntryContent table');
								
				$table.floatThead({
					useAbsolutePositioning: true,
					scrollContainer: function($table) {
						return $table.closest('.table-responsive');
					}
				});
				$table.floatThead('reflow');
				
				if( loopSize ){
					$("#invcDrpActns").show();
				}else{
					$("#invcDrpActns").hide();
				}
								
				/**
				  * Getting Hold Invoices 
				  */
				loopSize = response.output.fldTblInvHold.length;
				poStr = "", logStatus = "", logRsn = "", vchrNoStr = "";
				tbl = '<table class="table table-hover">'+
							'<thead>'+
								'<tr>'+
									'<th width="50px">Info</th>'+
									'<th>Vendor ID</th>'+
									'<th>Invoice No.</th>'+
									'<th>Invoice Date</th>'+
									'<th>Due Date</th>'+
									'<th>PO</th>'+
									'<th>Voucher No.</th>'+
									'<th>Branch</th>'+
									'<th class="text-right">Amount</th>'+
									'<th class="text-right">Discount</th>'+
									'<th>Entered Date</th>'+
									'<th>Disc Date</th>'+
								'</tr>'+
								'<tr class="filter-action-row hide-filter">'+
									'<th></th>'+
									'<th><input class="custom-text-filter form-control vendorFilter" type="text" /></th>'+
									'<th><input class="custom-text-filter form-control fieldsFilter" type="text" /></th>'+
									'<th><input type="text" name="daterange" class="form-control invcDtFilter"></th>'+
									'<th><input type="text" name="daterange" class="form-control dueDtFilter"></th>'+
									'<th></th>'+
									'<th><input class="custom-text-filter form-control voucherFilter" type="text" /></th>'+
									'<th></th>'+
									'<th></th>'+
									'<th></th>'+
									'<th></th>'+
									'<th></th>'+
								'</tr>'+
							'</thead>'+
							'<tbody>';
				
				for( var i=0; i<loopSize; i++ ){
					logStatus = "", logRsn = "";
					
					if( response.output.fldTblInvHold[i].poNo == 0 ){
						poStr = "";
					}else{
						poStr = response.output.fldTblInvHold[i].poPfx +'-'+ response.output.fldTblInvHold[i].poNo;
						
						if( response.output.fldTblInvHold[i].poItm != "0" ){
							poStr += '-' + response.output.fldTblInvHold[i].poItm;
						}
					}
										
					if( response.output.fldTblInvHold[i].pymntStsRsn != undefined && response.output.fldTblInvHold[i].pymntStsRsn.trim() != "" ){
						logRsn = response.output.fldTblInvHold[i].pymntStsRsn.trim();
					}
					if( response.output.fldTblInvHold[i].transStsRsn != undefined && response.output.fldTblInvHold[i].transStsRsn.trim() != "" ){
						logRsn += (logRsn.trim().length > 0) ? "<br>" : "";
						logRsn += response.output.fldTblInvHold[i].transStsRsn.trim();
					}
					if( response.output.fldTblInvHold[i].holdRsn != undefined && response.output.fldTblInvHold[i].holdRsn.trim() != "" ){
						logRsn += (logRsn.trim().length > 0) ? "<br>" : "";
						logRsn += response.output.fldTblInvHold[i].holdRsn.trim();
					}
					logStatus = '<i class="icon-info c-pointer text-danger font-weight-bold display-5" title="'+ logRsn +'" data-toggle="tooltip" data-html="true"></i>';
					
					if( response.output.fldTblInvHold[i].vchrPfx != undefined && response.output.fldTblInvHold[i].vchrPfx.trim() != "null" ){
						vchrNoStr = response.output.fldTblInvHold[i].vchrPfx.trim() + "-" + response.output.fldTblInvHold[i].vchrNo.trim();
					}
					
					var discStr = "";
					if(response.output.fldTblInvHold[i].discAmtStr != 0)
					{
						discStr = response.output.fldTblInvHold[i].discAmtStr +' '+ response.output.fldTblInvHold[i].vchrCry;
					}
					
					tbl += '<tr>'+
								'<td>'+ logStatus +'</td>'+
								'<td>'+ response.output.fldTblInvHold[i].vchrVenId +'-'+ response.output.fldTblInvHold[i].vchrVenNm +'</td>'+
								'<td>'+ response.output.fldTblInvHold[i].vchrInvNo +'</td>'+
								'<td>'+ response.output.fldTblInvHold[i].vchrInvDtStr +'</td>'+
								'<td>'+ response.output.fldTblInvHold[i].vchrDueDtStr +'</td>'+
								'<td>'+ poStr +'</td>'+
								'<td>'+ vchrNoStr +'</td>'+
								'<td>'+ response.output.fldTblInvHold[i].vchrBrh +'</td>'+
								'<td class="text-right">'+ response.output.fldTblInvHold[i].vchrAmt +' '+ response.output.fldTblInvHold[i].vchrCry +'</td>'+
								'<td>'+ discStr +'</td>'+
								'<td>'+ response.output.fldTblInvHold[i].crtDttsStr +'</td>'+
								'<td>'+ response.output.fldTblInvHold[i].vchrDiscDtStr +'</td>'+
							'</tr>';
				}
				tbl += '</tbody></table>';
				$("#blockedVoucherTblWrapper").html(tbl);
				var $table = $('#blockedVoucherTblWrapper table');
				//fixedTableHeader($table);
				
				$("#filterVoucherResults").slideDown();
				$("body").tooltip({ selector: '[data-toggle=tooltip]', placement: 'top' });
				$(".select2").select2();
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			
			
			$("#selectAllInvc").prop("checked", true);			
			$("#stxEntryContent table tbody input[type='checkbox'][name='select-invoices']").prop("checked", "checked");
			$("selectAllInvc").trigger("change");
			$("#batchTotalVal").html(batchTotalAmt.toFixed(2));
			$("#batchTotalCount").html(batchTotalCount);
			gblBatchTotalAmt=batchTotalAmt.toFixed(2);
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}


function aprvBlockPropInvc(invSts, remark = ""){	
	$("#mainLoader").show();
		
	var inputData = { 
		reqId : $("#viewAutoInvcPymntModal").attr("data-req-id"),
		reqUserBy : "",
		reqActnBy : "",
		paySts : invSts,
		rmk : remark,
		bankCd: $("#propBankCode").val(),
		acctNo: $("#propBankAccNo").val(),
		payMthd: $("#submitImvcPymnt").attr( "data-pay-mthd"),
		batchTotalAmt:gblBatchTotalAmt,
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/payment/upd-sts',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				//sendNotificationMail("prop", inputData.reqId );
				
				$("#invcBlockConfirmModal, #viewAutoInvcPymntModal").modal("hide");
				getProposals();
				//customAlert("#popupNotify", "alert-success", "Proposal status updated successfully.");
				customAlert("#popupNotify", "alert-success", "File generated successfully.");
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#popupNotify", "alert-danger", errList);
			}
			
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}

function checkFileTotal(inputData){
		$("#mainLoader").show();
		count=0;
		$("#previewCol").slideUp();
		$("#previewCol #previewIframe").attr("src", "");
		
		var reqId = inputData.reqId;
		var payMthd = 'ACH-CCD';
		var totAmt = '';
		
		$("#viewAutoInvcPymntModal .request-no").html(inputData.reqId);
		$("#viewAutoInvcPymntModal .pymnt-mthd").html(payMthd);
		$("#viewAutoInvcPymntModal .total-amt").html(totAmt);
		
		$("thead th input[type='text']").val('');
		$('#receiverAIPSnglPropData table').floatThead('destroy');
		
		$("#submitImvcPymnt").attr( "data-pay-mthd", "ACH-CCD" );
		
		$("#viewAutoInvcPymntModal").attr("data-req-id", inputData.reqId);
		
		console.log('prakash ******'+inputData.reqId);
		
		var inputData = { 
			reqId : reqId,
			paySts: "",
			venId: "",
			vchrNo: "",
			pageNo: -1,
			issueDate: gblIssueDate,
		};
		
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/payment/read',
			type: 'POST',
			dataType : 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					var poStr = "", vchrNo = "", amtStr = '', crtDttsStr = '', rowPymntMthd = '';
					var tbl = '', prevCry = '', prevVenId = '', prevVendor = '', venTotal = 0;
					if( response.output.fldTblPayment.length ){
						var loopSize = response.output.fldTblPayment[0].paramInvList.length;
						for( var i=0; i<loopSize; i++ ){
							vchrNo = "", rowPymntMthd = "";
							crtDttsStr = (response.output.fldTblPayment[0].paramInvList[i].crtDttsStr != undefined) ? response.output.fldTblPayment[0].paramInvList[i].crtDttsStr : "";
							
							if( response.output.fldTblPayment[0].paramInvList[i].poNo == 0 ){
								poStr = "";
							}else{
								
								if( response.output.fldTblPayment[0].paramInvList[i].poItm == 0 ){
									//poStr = response.output.fldTblPayment[0].paramInvList[i].poPfx +'-'+ response.output.fldTblPayment[0].paramInvList[i].poNo;
									poStr = '';
								}
								else
								{
									//poStr = response.output.fldTblPayment[0].paramInvList[i].poPfx +'-'+ response.output.fldTblPayment[0].paramInvList[i].poNo +'-'+ response.output.fldTblPayment[0].paramInvList[i].poItm;
									poStr = '';
								}
							}
							
							if( response.output.fldTblPayment[0].paramInvList[i].vchrPfx != undefined && response.output.fldTblPayment[0].paramInvList[i].vchrPfx.trim() != "" ){
								vchrNo = response.output.fldTblPayment[0].paramInvList[i].vchrPfx.trim() + "-" + response.output.fldTblPayment[0].paramInvList[i].vchrNo.trim();
							}
							
							amtStr = response.output.fldTblPayment[0].paramInvList[i].vchrAmtStr;
							if( response.output.fldTblPayment[0].paramInvList[i].vchrCry != undefined ){
								//amtStr += " " + response.output.fldTblPayment[0].paramInvList[i].vchrCry;
							}
							
							var glVal = '';
							if(response.output.fldTblPayment[0].paramInvList[i].glEntry.length > 0)
							{
								glVal = '<strong>' + response.output.fldTblPayment[0].paramInvList[i].glEntry + ' (' + response.output.fldTblPayment[0].paramInvList[i].glEntryDt + ')' + '</strong>';
							}
							else
							{
								glVal = '';
							}
							
							var discStr = "";
							if(response.output.fldTblPayment[0].paramInvList[i].discAmtStr != 0)
							{
								discStr = response.output.fldTblPayment[0].paramInvList[i].discAmtStr +' '+ response.output.fldTblPayment[0].paramInvList[i].vchrCry;
								discStr = response.output.fldTblPayment[0].paramInvList[i].discAmtStr;
							}
							
							if( response.output.fldTblPayment[0].paramInvList[i].payMthds.length ){	
								var innerLoopSize = response.output.fldTblPayment[0].paramInvList[i].payMthds.length;
								for( var j=0; j<innerLoopSize; j++ ){
									if( response.output.fldTblPayment[0].paramInvList[i].payMthds[j].mthdDflt){	
										rowPymntMthd = response.output.fldTblPayment[0].paramInvList[i].payMthds[j].mthdNm;
										break;
									}		
								}
							}
							
							if( prevVenId != "" && prevVenId != response.output.fldTblPayment[0].paramInvList[i].vchrVenId.trim() ){
								tbl += '<tr class="vndr-total font-weight-bold text-dark">'+
										'<td></td>'+
										'<td></td>'+
										'<td>'+ prevVendor +' (Total):</td>'+
										'<td></td>'+
										'<td></td>'+
										'<td></td>'+
										'<td></td>'+
										'<td></td>'+
										'<td class="text-right" colspan="2">'+ venTotal.toFixed(2) +' '+ prevCry +'</td>'+
										'<td class="text-right"></td>'+
										//'<td class="check-col"></td>'+
										'<td></td>'+
										'<td class="pymnt-mthd-col"></td>'+
										'<td></td>'+
										'<td></td>'+
										/*'<td class="check-gl">'+ glVal +'</td>'+*/
									'</tr>';
								venTotal = 0;
							}
							
							var failedRowClass = '';
							
							if(response.output.fldTblPayment[0].paramInvList[i].vchrPrsSts == '' || response.output.fldTblPayment[0].paramInvList[i].vchrPrsSts == 'false')
							{
								failedRowClass = 'failed-payment-row';
							}
							
							tbl += '<tr class="'+ failedRowClass +'" data-invc-no="'+ response.output.fldTblPayment[0].paramInvList[i].vchrInvNo +'" data-vchr-no="'+ vchrNo +'" data-ctl-no="'+ response.output.fldTblPayment[0].paramInvList[i].ctlNo +'">'+
										'<td>'+ (i+1) +'</td>'+
										'<td><i class="icon-eye menu-icon view-doc text-info" aria-hidden="true"></i></td>'+
										'<td>'+ response.output.fldTblPayment[0].paramInvList[i].vchrVenId.trim() + "-" + response.output.fldTblPayment[0].paramInvList[i].vchrVenNm.trim() + '</td>'+
										'<td>'+ response.output.fldTblPayment[0].paramInvList[i].vchrInvNo +'</td>'+
										'<td>'+ response.output.fldTblPayment[0].paramInvList[i].vchrInvDtStr +'</td>'+
										'<td>'+ response.output.fldTblPayment[0].paramInvList[i].vchrDueDtStr +'</td>'+
										'<td>'+ poStr +'</td>'+
										'<td>'+ response.output.fldTblPayment[0].paramInvList[i].vchrPfx + "-" + response.output.fldTblPayment[0].paramInvList[i].vchrNo +'</td>'+
										'<td>'+ response.output.fldTblPayment[0].paramInvList[i].vchrBrh +'</td>'+
										'<td class="text-right">'+ amtStr +'</td>'+
										'<td class="text-right">'+ discStr + '</td>'+
										//'<td class="check-col">'+ response.output.fldTblPayment[0].paramInvList[i].chkNo +'</td>'+
										'<td >'+ response.output.fldTblPayment[0].paramInvList[i].chkNo +'</td>'+
										'<td class="pymnt-mthd-col">'+ rowPymntMthd +'</td>'+
										'<td>'+ crtDttsStr +'</td>'+
										'<td>'+ response.output.fldTblPayment[0].paramInvList[i].vchrDiscDtStr +'</td>'
										/*'<td class="check-gl">'+ glVal +'</td>'+*/
									'</tr>';
									
							prevVenId = response.output.fldTblPayment[0].paramInvList[i].vchrVenId.trim();
							prevCry = response.output.fldTblPayment[0].paramInvList[i].vchrCry;
							venTotal += response.output.fldTblPayment[0].paramInvList[i].vchrAmt;
							prevVendor = response.output.fldTblPayment[0].paramInvList[i].vchrVenId.trim() + "-" + response.output.fldTblPayment[0].paramInvList[i].vchrVenNm.trim();
						}
						
						if( prevVenId != "" ){
							tbl += '<tr class="vndr-total font-weight-bold text-dark">'+
									'<td></td>'+
									'<td></td>'+
									'<td>'+ prevVendor +' (Total):</td>'+
									'<td></td>'+
									'<td></td>'+
									'<td></td>'+
									'<td></td>'+
									'<td></td>'+
									//'<td class="text-right" colspan="2">'+ venTotal.toFixed(2) +' '+ prevCry +'</td>'+
									'<td class="text-right" colspan="2">'+ venTotal.toFixed(2) +'</td>'+
									'<td class="text-right"></td>'+
									//'<td class="check-col"></td>'+
									'<td ></td>'+
									'<td class="pymnt-mthd-col"></td>'+
									'<td></td>'+
									'<td></td>'+
									/*'<td class="check-gl">'+ glVal +'</td>'+*/
								'</tr>';
							venTotal = 0;
						}
						
						/*if( response.output.fldTblPayment[0].companyBnkList[0] != undefined ){
							$("#propCmpyId").val( response.output.fldTblPayment[0].companyBnkList[0].cmpyId );
							$("#propBankCode").val( response.output.fldTblPayment[0].companyBnkList[0].bankCode );
							$("#propBankCty").val( response.output.fldTblPayment[0].companyBnkList[0].bankCty );
						}*/
						
						$("#propBankRouting").val("");
						
						if( response.output.fldTblPayment[0].companyBnkList.length ){						
							loopSize = response.output.fldTblPayment[0].companyBnkList.length;
							
							var bnkList = '';
							for( var i=0; i<loopSize; i++ ){
								bnkList += '<option value="'+ response.output.fldTblPayment[0].companyBnkList[i].bankCode +'" data-rout-no="'+ response.output.fldTblPayment[0].companyBnkList[i].bnkRoutNo +'">'+ response.output.fldTblPayment[0].companyBnkList[i].bankCode +'</option>';
							}
							$("#propBankCode").html( bnkList );
							
							loopSize = response.output.fldTblPayment[0].companyBnkList[0].accoutnList.length;
							$("#propBankRouting").val( response.output.fldTblPayment[0].companyBnkList[0].bnkRoutNo );
							
							var checked = '';
							var accNumbers = '';
							for( var i=0; i<loopSize; i++ ){
								checked = (response.output.fldTblPayment[0].companyBnkList[0].accoutnList[i].default) ? 'selected' : '';
								accNumbers += '<option '+ checked +' value="'+ response.output.fldTblPayment[0].companyBnkList[0].accoutnList[i].bnkAcctNo +'">'+ response.output.fldTblPayment[0].companyBnkList[0].accoutnList[i].bnkAcctNo +' ('+ response.output.fldTblPayment[0].companyBnkList[0].accoutnList[i].bnkAcctDesc +')</option>';
							}
							$("#propBankAccNo").html( accNumbers );
						}
						
					}
					
					$("#receiverAIPSnglPropData table tbody").html(tbl);
					
					if( response.output.fldTblPayment.length ){
						if( response.output.fldTblPayment[0].paySts != undefined && response.output.fldTblPayment[0].paySts == "A" ){
							$("#invcActnSts").val("A").trigger("change");
							$("#propBankAccNo, #propBankCode, #invcActnSts").attr("disabled", "disabled");
							$("#viewAutoInvcPymntModal .modal-footer").hide();
						}else{
							$("#invcActnSts").val("A").trigger("change");
							$("#propBankAccNo, #propBankCode, #invcActnSts").removeAttr("disabled");
							$("#viewAutoInvcPymntModal .modal-footer").show();
						}
					}
					
					if(response.output.fldTblPayment[0].accountNo != '' && response.output.fldTblPayment[0].accountNo != undefined)
					{
						$("#propBankAccNo").val(response.output.fldTblPayment[0].accountNo).trigger('change');
						$("#propBankCode").val(response.output.fldTblPayment[0].bnkCode).trigger('change');
					}
					
					if( payMthd.indexOf("CHK") > -1 || payMthd.indexOf("CHECK") > -1 ){
						$(".check-col").show();
					}else{
						$(".check-col").hide();
					}
					
					if( payMthd.indexOf(",") > -1 ){
						$(".pymnt-mthd-col").show();
					}else{
						$(".pymnt-mthd-col").hide();
					}	
					
					//$("#viewAutoInvcPymntModal").modal("show");
					$("#submitImvcPymnt").trigger("click");
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
				}
				
				$("#mainLoader").hide();
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				$("#mainLoader").hide();
			}
		});
}

function saveSendProposal( inputData ){
	$("#mainLoader").show();
	
	var reqMsgTxt = "saved.";
	var ajaxUrl = rootAppName + "/rest/payment/add";
	
	if( inputData.paySts == "S" ){
		reqMsgTxt = "sent for approval.";
		console.log("S reqMsgTxt = sent for approval.");
	}
	
	if( $("#actnTyp").val() == "U" ){
		ajaxUrl = rootAppName + "/rest/payment/upd-sts";
		console.log("U reqMsgTxt = sent for approval.");
	}
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: ajaxUrl,
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				$("#actnTyp").val("A");
				//customAlert("#mainNotify", "alert-success", "Request successfully " + reqMsgTxt);
				//$("#sendToAprvModal").modal("hide");
				$("#backToAIP").trigger("click");
				
				//getProposals();
				
				if( inputData.paySts == "S" ){
					//sendNotificationMail("prop", inputData.reqId );
				}
				console.log("checkFileTotal(inputData);");
				checkFileTotal(inputData);
			}else{
				console.log("else part checkFileTotal(inputData);");
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				
				if( inputData.paySts == "S" ){
					//customAlert("#sendToAprvErr", "alert-danger", errList);
					customAlert("#mainNotify", "alert-danger", errList);
				}else{
					customAlert("#mainNotify", "alert-danger", errList);
				}
				
				$("#mainLoader").hide();
			}
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}


	function calculateTotal(){
	
	var totAmt = 0;
	var discAmt = 0;
	var totChkNetAmt = 0;
	
	$("#stxEntryContent input[type='checkbox'][name='select-invoices']:checked")
	
	$("#stxEntryContent input[type='checkbox'][name='select-invoices']:checked").each(function(index, val){
		
		if($(val).closest('tr').attr('data-vchr-amt') == undefined || $(val).closest('tr').attr('data-vchr-amt') == "")
		{
			discAmt = 0;
		}
		else
		{
			discAmt = parseFloat($(val).closest('tr').attr('data-vchr-disc-amt'));
		}
		
		if($(val).closest('tr').attr('data-vchr-disc-flg') == 'Y')
		{
			totAmt = totAmt + (parseFloat($(val).closest('tr').attr('data-vchr-amt')) - discAmt);	
		}
		else
		{
			totAmt = totAmt + (parseFloat($(val).closest('tr').attr('data-vchr-amt')));
		}
		totChkNetAmt = totChkNetAmt + (parseFloat($(val).closest('tr').attr('data-chkNet-amt')));
		
	});
	 
	
	//$("#batchTotalVal").html(formatAmount(totAmt));
	$("#batchTotalVal").html(formatAmount(totChkNetAmt));
	
	$("#batchTotalCount").html($("#stxEntryContent input[type='checkbox'][name='select-invoices']:checked").length);

}
