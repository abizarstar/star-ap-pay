var ajaxHeader = { 
	'Accept' : 'application/json',
	'Content-Type' : 'application/json',
	'user-id': localStorage.getItem("starOcrUserId"),
	'auth-token': localStorage.getItem("starOcrAuthToken"),
	'app-host' : localStorage.getItem("starOcrAppHost"),
	'app-port' : localStorage.getItem("starOcrAppPort"),
	'cmpy-id' : localStorage.getItem("starOcrCmpyId"),
	'app-token' : localStorage.getItem("starOcrAppToken"),
	'user-grp' : localStorage.getItem("starOcrUsrGrp"),
	
};
var glblCmpyId = "";
var rootAppName = "/" + window.location.pathname.split("/")[1];

//if( location.pathname.indexOf("login.html") <= -1 && location.pathname.indexOf("index.html") <= -1 && location.pathname.indexOf(".html") > -1 ){
//if( location.pathname.indexOf("login.html") <= -1 && location.pathname.indexOf(".html") > -1 ){
if( location.pathname.indexOf("login.html") <= -1 ){
	$("#main-menu-navigation").html(localStorage.getItem("starOcrSidebarMenu"));
	activeSidebarMenu();
}
	
$(function(){
	
	$("body").addClass("sidebar-dark");
	
	setAjaxHeader();
	
	if( location.pathname.indexOf("login.html") <= -1 ){
		if( localStorage.getItem("starOcrAuthToken") != null && localStorage.getItem("starOcrAuthToken") != "" ){
			validateAuthCode();
	
			//getSidebarMenus();
		}else{
			window.location.replace("login.html");
		}
	}
	
	//if( location.pathname.indexOf("login.html") <= -1 && location.pathname.indexOf("index.html") <= -1 && location.pathname.indexOf(".html") > -1 ){
	if( location.pathname.indexOf("login.html") <= -1 && location.pathname.indexOf("upld-invc.html") <= -1 && location.pathname.indexOf(".html") > -1 ){
		$("#main-menu-navigation").html(localStorage.getItem("starOcrSidebarMenu"));
		activeSidebarMenu();
	}
	

	glblCmpyId = $("#headerCmpyId").val();
	
	if( glblCmpyId == "" || glblCmpyId == null || glblCmpyId == undefined ){
		glblCmpyId = localStorage.getItem("starOcrCmpyId");
	}
	
	$("#headerNavbar").load("includes/header.html", function(){
		headerLoaded();
	});
	
	/*$("#sidebarWrapper").load("includes/sidebar.html", function(){
		activeSidebarMenu();
	});*/
	
	$("#footerWrapper").load("includes/footer.html", function(){
		//console.log( "Footer Loaded" );
	});
		
	if( $.isFunction( $.fn.select2 ) ){
		$(".select2").select2();
	}
	
	$('[data-toggle="tooltip"]').tooltip();
	
	//setInputGroupSelect2Width();	
	
	if( $.isFunction( $.fn.daterangepicker ) ){
		$('.date').daterangepicker({
			singleDatePicker: true,
			showDropdowns: true,
			opens: 'left',
			locale: {
				format: 'MM/DD/YYYY'
			}
		});
		
		$('.date').change(function(){
			$(this).daterangepicker({
				singleDatePicker: true,
				showDropdowns: true,
				opens: 'left',
				locale: {
					format: 'MM/DD/YYYY'
				}
			});
		});
		
			
		$('body').on('focus',"input[name='daterange']", function(){
			$('input[name="daterange"]').daterangepicker({
				autoUpdateInput: false,
				opens: 'left',
				locale: {
					format: 'MM/DD/YYYY'
				}
			}, function(start, end, label) {
				console.log("A new date selection was made: " + start.format('MM/DD/YYYY') + ' to ' + end.format('MM/DD/YYYY'));
			});
			
			$('input[name="daterange"]').on('apply.daterangepicker', function(ev, picker) {
				$(this).val(picker.startDate.format('MM/DD/YYYY') + ' - ' + picker.endDate.format('MM/DD/YYYY'));
				$(this).trigger("change");
			});
	
			$('input[name="daterange"]').on('cancel.daterangepicker', function(ev, picker) {
				$(this).val('');
				$(this).trigger("change");
			});
	
		});
	}
	
	$(document).on("click", "#logout", function(e){
		e.preventDefault();
		
		localStorage.removeItem("starOcrAuthToken");
	    localStorage.removeItem("starOcrUserId");
	    localStorage.removeItem("starOcrUserNm");
	    localStorage.removeItem("starOcrEmailId");
	    localStorage.removeItem("starOcrUsrGrp");
	    localStorage.removeItem("starOcrAppHost");
	    localStorage.removeItem("starOcrAppPort");
	    localStorage.removeItem("starOcrAppToken");
	    localStorage.removeItem("starOcrCmpyId");
	    
	    localStorage.removeItem("starOcrSidebarMenu");
	    
	    $.removeCookie('auth-token');
	    $.removeCookie('app-token');
	    $.removeCookie('user-id');
		
		window.location.replace("login.html");
	});
	
	$("#headerNavbar").on("change", "#headerCmpyId", function(e){
		var cmpyId = $(this).val();
		
		if( cmpyId != "" ){
			localStorage.setItem("starOcrCmpyId", cmpyId);
			location.reload();
		}
	});
	
	$("#headerNavbar").on("click", ".navbar-toggler", function(e){
		$("body").toggleClass("sidebar-icon-only");
	});
	
	//Open submenu on hover in compact sidebar mode and horizontal menu mode
  $(document).on('mouseenter mouseleave', '.sidebar .nav-item', function (ev) {
      var body = $('body');
      var sidebarIconOnly = body.hasClass("sidebar-icon-only");
      var horizontalMenu = body.hasClass("horizontal-menu");
      var sidebarFixed = body.hasClass("sidebar-fixed");
      if(!('ontouchstart' in document.documentElement)) {
        if(sidebarIconOnly || horizontalMenu) {
          if(sidebarFixed) {
            if(ev.type === 'mouseenter') {
              body.removeClass('sidebar-icon-only');
            }
          }
          else {
            var $menuItem = $(this);
            if(ev.type === 'mouseenter') {
              $menuItem.addClass('hover-open')
            }
            else {
              $menuItem.removeClass('hover-open')
            }
          }
        }
      }
  });

	
	$('.alpha-numeric').keypress(function (e) {
		var regex = new RegExp("^[a-zA-Z0-9]+$");
	    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
	    if (regex.test(str)) {
	        return true;
	    }
	
	    e.preventDefault();
	    return false;
	});
	
	$(document).on("keypress", ".number-only", function(e){
		//if the letter is not digit then display error and don't type anything
		if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
			//display error message
			//$("#errmsg").html("Digits Only").show().fadeOut("slow");
			return false;
		}
	});
	
	$(document).on("keyup", ".decimal", function(e) {
        var val = 0;

		if ( $(this).val() == ".") {
			$(this).val("0.");
		}
		
		var afterDecimal = 2, beforeDecimal = 10;
		
		var count = $(this).val().split("-").length - 1;
		
		if(e.key == '-')
		{
			var resultStr = '';
			var firstOccuranceIndex = $(this).val().search(/\-/) + 1; // Index of first occurance of (.)
			
			if($(this).val().charAt(0) == '-')
			{
				resultStr = $(this).val().substr(0, firstOccuranceIndex) + $(this).val().slice(firstOccuranceIndex).replace(/\-/g, ''); // Splitting into two string and replacing all the dots (.'s) in the second string
			}
			else
			{
				resultStr = $(this).val().replace(/\-/g, ''); // Splitting into two string and replacing all the dots (.'s) in the second string
			}
			
			$(this).val(resultStr);
		}
		
		
		$(this).val(
			$(this).val()
          .replace(/[^-{1}\d.]/g, '')           
          .replace(new RegExp("(^[\\d]{" + beforeDecimal + "})[\\d]", "g"), '$1') 
          .replace(/(\..*)\./g, '$1')
          .replace(new RegExp("(\\.[\\d]{" + afterDecimal + "}).", "g"), '$1')
		)
    });
    
    $(document).on("keydown", ".decimal", function(e) {
		var afterDecimal = 2, beforeDecimal = 10;
		
		$(this).val(
			$(this).val()
          .replace(/[^-{1}\d.]/g, '')           
          .replace(new RegExp("(^[\\d]{" + beforeDecimal + "})[\\d]", "g"), '$1') 
          .replace(/(\..*)\./g, '$1')
          .replace(new RegExp("(\\.[\\d]{" + afterDecimal + "}).", "g"), '$1')
		)
    });
    
    $('.password-validate').keyup(function (e) {
		var number = /([0-9])/;
        var small_alphabets = /([a-z])/;
        var capital_alphabets = /([A-Z])/;
        var special_characters = /([~,!,@,#,$,%,^,&,*,-,_,+,=,?,>,<])/;
        var password = $(this).val().trim();
        
        if (password.length < 8) {
        	$(this).removeClass("border-success border-warning").addClass("border-danger");
        } else {
            if (password.match(number) && password.match(small_alphabets) && password.match(capital_alphabets) && password.match(special_characters)) {
        		$(this).removeClass("border-danger border-warning").addClass("border-success");
            } else {
                $(this).removeClass("border-success border-danger").addClass("border-warning");
            }
        }
	});
	
	$('.confirm-password').keyup(function (e) {
		if( ($('.confirm-password').val() == $('.password-validate').val()) && $('.password-validate').hasClass("border-success") ){
			$('.confirm-password').removeClass("border-danger").addClass("border-success");
		}else{
			$('.confirm-password').removeClass("border-success").addClass("border-danger");
		}
	});
	
	$(document).on("click", ".btn-clear", function(){
		if( $(this).closest("form")[0] != undefined ){
			$(this).closest("form")[0].reset();
		}
	});
	
	
	$(document).on('change',"input[name='daterange']", function(){
		var tblDivId = $(this).parents(".table-responsive").attr("id");
		
		if( tblDivId == undefined ){
			tblDivId = $(this).parents(".floatThead-container").next().attr("id");
		}
		
		filterTableData(tblDivId);
	});
	
	$(document).on("keyup", ".custom-text-filter", function(){
		var tblDivId = $(this).parents(".table-responsive").attr("id");
		
		if( tblDivId == undefined ){
			tblDivId = $(this).parents(".floatThead-container").next().attr("id");
		}
		
		filterTableData(tblDivId);
	});
	
	$(document).on("change", ".custom-select-filter", function(){
		var tblDivId = $(this).parents(".table-responsive").attr("id");
		
		if( tblDivId == undefined ){
			tblDivId = $(this).parents(".floatThead-container").next().attr("id");
		}
		
		filterTableData(tblDivId);
	});
	
	$("#main-menu-navigation").on("click", ".nav-link.has-child", function(){
		var curSubMenuId = $(this).next().attr("id");
		$("#main-menu-navigation .collapse.show:not(#"+ curSubMenuId +")").removeClass("show");
	});
	
	$("#headerNavbar").on("click", ".logout-btn", function(){
		$("#logoutModal").modal("show");
	});
	
	$(document).on('focus', '.select2.select2-container', function (e) {
  		var isOriginalEvent = e.originalEvent // don't re-open on closing focus event
	  	var isSingleSelect = $(this).find(".select2-selection--single").length > 0 // multi-select will pass focus to input
	
	  	if (isOriginalEvent && isSingleSelect) {
	  		$(this).siblings('select:enabled').select2('open');
	  	}
	});
	
	$("#headerNavbar").on("click", "#syncConfigEmails", function(){
		if( $(this).hasClass("c-not-allowed") ){
			return;
		}
		//$("#mainLoader").show();
		$(this).html("");
		$(this).attr("title", "Synchronizing Emails...");
		$(this).attr("disabled", "disabled");
		$(this).removeClass("c-pointer").addClass("spin-it c-not-allowed");
		
		var inputData = {};
	
		$.ajax({
			headers: ajaxHeader,
			data: JSON.stringify( inputData ),
			url: rootAppName + '/rest/config-eml/read',
			type: 'POST',
			dataType : 'json',
			success: function(response){
				/*if( response.output.rtnSts == 0 ){
					$("#syncConfigEmails").html(" Sync Emails");
					$("#syncConfigEmails").removeAttr("disabled", "disabled");
					$("#syncConfigEmails").removeClass("spin-it c-not-allowed").addClass("c-pointer");
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
				}*/
				
				$("#syncConfigEmails").html(" Sync Emails");
				$("#syncConfigEmails").attr("title", "");
				$("#syncConfigEmails").removeAttr("disabled", "disabled");
				$("#syncConfigEmails").removeClass("spin-it c-not-allowed").addClass("c-pointer");
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				
				$("#syncConfigEmails").html(" Sync Emails");
				$("#syncConfigEmails").attr("title", "");
				$("#syncConfigEmails").removeAttr("disabled", "disabled");
				$("#syncConfigEmails").removeClass("spin-it c-not-allowed").addClass("c-pointer");
				//$("#mainLoader").hide();
			}
		});
	});
	
	
	$("#crInvcNo").focusout(function(){
		var invNo = $(this).val().trim();
		
	    if( invNo != "" && invNo != null && invNo != undefined ){
	    	validateInvoiceNumber(invNo);
	    }else{
	    	$("#crCusId").val("").trigger("change");
	    	$("#crCusId").removeAttr("disabled");
	    }
	});
	
});

function setAjaxHeader(){
	ajaxHeader = { 
		'Accept' : 'application/json',
		'Content-Type' : 'application/json',
		'user-id': localStorage.getItem("starOcrUserId"),
		'auth-token': localStorage.getItem("starOcrAuthToken"),
		'app-host' : localStorage.getItem("starOcrAppHost"),
		'app-port' : localStorage.getItem("starOcrAppPort"),
		'cmpy-id' : localStorage.getItem("starOcrCmpyId"),
		'app-token' : localStorage.getItem("starOcrAppToken"),
		'user-grp' : localStorage.getItem("starOcrUsrGrp"),
	};
}

function headerLoaded(){
	$("#headerCmpyId").attr("data-selected", localStorage.getItem("starOcrCmpyId"));
	
	getCmpyIdList( $("#headerCmpyId") );
	
	if( $.isFunction( $.fn.select2 ) ){
		$(".select2").select2();
	}
	
	var pageExtn = ".html";
	var pageNm = window.location.pathname.split("/")[2];
	var extIndx = pageNm.indexOf(pageExtn);
	pageNm = pageNm.substr(0, extIndx + pageExtn.length);
	
	$("#headerNavbar .header-page-title").html( $("#sidebar").find('a[href="'+ pageNm +'"]').find("span.menu-page-title").html() );
}

var loopCount = 0;
function setInputGroupSelect2Width(){
	$(".custom-select2-wrapper").each(function(index, ele){
		var prevEleWidth = parseInt($(this).prev().css("width") ) + 1;
		console.log(prevEleWidth);
		if( prevEleWidth < 10 && loopCount < 10 ){
			setTimeout(function(){
				setInputGroupSelect2Width();
			}, 1000);
		}
		
		$(this).css("width", "calc(100% - "+ prevEleWidth +"px)");
	});
		
	loopCount++;
}
	
function activeSidebarMenu(){
	$("#sidebar li").removeClass("active");
	
	var pageExtn = ".html";
	var pageNm = window.location.pathname.split("/")[2];
	var extIndx = pageNm.indexOf(pageExtn);
	pageNm = pageNm.substr(0, extIndx + pageExtn.length);
	
	$("#sidebar").find('a[href="'+ pageNm +'"]').parent().addClass("active");
	
	if( $("#sidebar").find('a[href="'+ pageNm +'"]').parent().parent().hasClass("sub-menu") ){
		$("#sidebar").find('a[href="'+ pageNm +'"]').parent().parent().parent().addClass("show");
	}
	
	$("#headerNavbar .header-page-title").html( $("#sidebar").find('a[href="'+ pageNm +'"]').find("span.menu-page-title").html() );
	
	if( pageNm == "vldt-trans.html" ){
		$("body").addClass("sidebar-icon-only");
	}	
	
	//setDescription();
	
	/*if( location.pathname.indexOf("index.html") > -1 ){
		$("#sidebar").find('a[href="index.html"]').parent().addClass("active");
	}else if( location.pathname.indexOf("bank-setup.html") > -1 ){
		$("#sidebar").find('a[href="bank-setup.html"]').parent().addClass("active");
	}*/
}

function setDescription(){
	$("#main-menu-navigation li a").not('.has-child').each(function(index, element){
		var pageName = $(element).attr("href");
		
		if( pageName == "index.html" ){
			$(element).attr("title", "Dashboard Page");
		}else if( pageName == "upld-invc.html" ){
			$(element).attr("title", "Upload Invoice Page");
		}else if( pageName == "rcvd-invc.html" ){
			$(element).attr("title", "Received Invoice Page");
		}else if( pageName == "void-check.html" ){
			$(element).attr("title", "Void Check Page");
		}else if( pageName == "auto-invc-pymt.html" ){
			$(element).attr("title", "Auto Invoice Payment Page");
		}else if( pageName == "aprv-invc-pymt.html" ){
			$(element).attr("title", "Approve Invoice Payment Page");
		}
	});
	
	$("#tabOverviewInvc").attr("title", "Invoice Overview Dashboard");
	$("#tabToReviewInvc").attr("title", "Uploaded invoices will show here.");
	$("#tabConfirmInvc").attr("title", "Invoice which created vouchers will show here.");
	$("#tabPrcPymntInvc").attr("title", "Payment initiated invoices show here.");
	$("#tabCompInvc").attr("title", "Completed Invoices show here");
	$("#tabRejectedInvc").attr("title", "Rejected invoices show here");
	$("#tabHoldInvc").attr("title", "Hold invoices show here Hold invoices show here Hold invoices show here Hold invoices show here Hold invoices show here Hold invoices show here Hold invoices show here Hold invoices show here Hold invoices show hereHold invoices show here Hold invoices show here Hold invoices show here Hold invoices show here Hold invoices show here Hold invoices show here");
}

function setLoggedInUserName(){
	if( $("#loggedInUser").length ){	    
	    if( localStorage.getItem("starOcrUserNm") != null && localStorage.getItem("starOcrUserNm") != "" ){
	    	$("#loggedInUser").html( localStorage.getItem("starOcrUserNm") );
	    }else if( localStorage.getItem("starOcrUserId") != null && localStorage.getItem("starOcrUserId") != "" ){
	    	$("#loggedInUser").html( localStorage.getItem("starOcrUserId") );
	    }
	}else{
		setTimeout(function(){
			setLoggedInUserName();
		}, 500);
	}
}


function validateAuthCode(){
	$("#mainAppWrapper").show();
	
	$.ajax({
		headers: ajaxHeader,
		type: 'POST',
		url: rootAppName + '/rest/auth-page/valid',
		success: function(response, textStatus, jqXHR) {
			setLoggedInUserName();
		    
		    $("#mainAppWrapper").hide();
		},
		complete: function(jqXHR, textStatus) {

		},
		error: function( jqXHR, textStatus, errorThrown ){  
		    localStorage.removeItem("starOcrAuthToken");
		    localStorage.removeItem("starOcrUserId");
		    localStorage.removeItem("starOcrUserNm");
		    localStorage.removeItem("starOcrEmailId");
		    localStorage.removeItem("starOcrUsrGrp");
		    localStorage.removeItem("starOcrAppHost");
		    localStorage.removeItem("starOcrAppPort");
		    localStorage.removeItem("starOcrAppToken");
		    localStorage.removeItem("starOcrCmpyId");
		    
		    localStorage.removeItem("starOcrSidebarMenu");
		    
		    $.removeCookie('auth-token');
		    $.removeCookie('app-token');
		    $.removeCookie('user-id');
		    
		    window.location.replace("login.html");
		}
	});
}

function customAlert(alertArea, alertType, alertMessage){
	var uniqueId = new Date().valueOf();
	var message = '<div style="display:none;" class="alert ' + alertType + ' alert-dismissible mb-3" role="alert" id="cstmAlert-'+ uniqueId +'">' +
					'<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
						'<span aria-hidden="true">×</span>' +
					'</button>' + alertMessage +
				'</div>';
	$(alertArea).html( message );
	$("#cstmAlert-" + uniqueId).slideDown();
	
	$('html, body, .content-wrapper').animate({
        scrollTop: $(alertArea).offset().top - 100
    }, 'slow');
	
	setTimeout(function(){
		if( $("#cstmAlert-" + uniqueId).length ){
			$("#cstmAlert-" + uniqueId).slideUp(1000, function(){
				$(this).remove();
			})
		}
	}, 10000);
}


function getCountryList( $select ){
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/common/read-cty',
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblCountry.length;
				var cty = '', ctyDesc = '', options = '<option value="">&nbsp;</option>';	
				for( var i=0; i<loopSize; i++ ){
					cty = response.output.fldTblCountry[i].cty.trim();
					ctyDesc = response.output.fldTblCountry[i].ctyDesc.trim();
					
					options += '<option value="'+ cty +'">'+ cty + ' - ' + ctyDesc + '</option>';
				}
				$select.html( options );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			//$("#mainLoader").hide();
		}
	});
}

function getCmpyIdList( $select ){
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/vendor/read-cmpy',
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblCompany.length;
				var cmpyId = '', cmpyNm = '', options = '<option value="">&nbsp;</option>';	
				options = '';
				
				var dfltOption = "", selectOption = "";
				if( $select.attr("data-selected") != undefined && $select.attr("data-selected") != "" ){
					dfltOption = $select.attr("data-selected");
					
					$select.attr("data-selected", "");
				}
				
				for( var i=0; i<loopSize; i++ ){
					selectOption = "";
					
					cmpyId = response.output.fldTblCompany[i].cmpyId.trim();
					cmpyNm = response.output.fldTblCompany[i].cmpyNm.trim();
					
					if( dfltOption == cmpyId ){
						selectOption = "selected";
					}
					
					options += '<option value="'+ cmpyId +'" '+ selectOption +'>'+ cmpyId + ' - ' + cmpyNm + '</option>';
				}
				$select.html( options );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			//$("#mainLoader").hide();
		}
	});
}


function getVendorList( $select, cmpyId, allowBlank=true ){
	if( cmpyId != "" && cmpyId != null && cmpyId != undefined ){
		var inputData = {
			cmpyId : cmpyId,
		};
		
		$.ajax({
			headers: ajaxHeader,
			url: rootAppName + '/rest/vendor/read',
			type: 'POST',
			data : JSON.stringify( inputData ),
			dataType: 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					var loopSize = response.output.fldTblVendor.length;
					var venId = '', venNm = '', venLongNm = '', venCry = '', options = '';	
					
					if( allowBlank ){
						options = '<option value="">&nbsp;</option>';
					}
				
					for( var i=0; i<loopSize; i++ ){
						venId = response.output.fldTblVendor[i].venId.trim();
						venNm = response.output.fldTblVendor[i].venNm.trim();
						venLongNm = response.output.fldTblVendor[i].venLongNm.trim();
						venCry = response.output.fldTblVendor[i].venCry.trim();
						
						options += '<option value="'+ venId +'" data-vndr-nm="'+ venNm +'" data-vndr-long-nm="'+ venLongNm +'" data-vndr-cry="'+ venCry +'">'+ venId + ' - ' + venNm + '</option>';
					}
					$select.html( options );
					
					if( $select.attr("data-default-value") != undefined && $select.attr("data-default-value") != "" ){
						$select.val( $select.attr("data-default-value") ).trigger("change");
					}else{
						//$("#mainLoader").hide();
					}
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
					//$("#mainLoader").hide();
				}			
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				//$("#mainLoader").hide();
			}
		});
	}else{
		setTimeout(function(){
			getVendorList( $select, glblCmpyId );
		}, 500)
	}
}

function getVendorListESP( $select, cmpyId, allowBlank=true ){
	if( cmpyId != "" && cmpyId != null && cmpyId != undefined ){
		var inputData = {
			cmpyId : cmpyId,
		};
		
		$.ajax({
			headers: ajaxHeader,
			url: rootAppName + '/rest/vendor/readESP',
			type: 'POST',
			data : JSON.stringify( inputData ),
			dataType: 'json',
			success: function(response){
				if( response.output.rtnSts == 0 ){
					var loopSize = response.output.fldTblVendor.length;
					var venId = '', venNm = '', venLongNm = '', venCry = '', options = '';	
					
					if( allowBlank ){
						options = '<option value="">&nbsp;</option>';
					}
				
					for( var i=0; i<loopSize; i++ ){
						venId = response.output.fldTblVendor[i].venId.trim();
						venNm = response.output.fldTblVendor[i].venNm.trim();
						venLongNm = response.output.fldTblVendor[i].venLongNm.trim();
						venCry = response.output.fldTblVendor[i].venCry.trim();
						
						options += '<option value="'+ venId +'" data-vndr-nm="'+ venNm +'" data-vndr-long-nm="'+ venLongNm +'" data-vndr-cry="'+ venCry +'">'+ venId + ' - ' + venNm + '</option>';
					}
					$select.html( options );
					
					if( $select.attr("data-default-value") != undefined && $select.attr("data-default-value") != "" ){
						$select.val( $select.attr("data-default-value") ).trigger("change");
					}else{
						//$("#mainLoader").hide();
					}
				}else{
					var loopLimit = response.output.messages.length;
					var errList = '<ul class="m-0 pl-1">';
					for( var i=0; i<loopLimit; i++ ){
						errList += '<li>'+ response.output.messages[i] +'</li>';
					}
					errList += '</ul>';
					customAlert("#mainNotify", "alert-danger", errList);
					//$("#mainLoader").hide();
				}			
			},
			error: function( jqXHR, textStatus, errorThrown ){
				if( jqXHR.status == 401 ){
					validateAuthCode();
				}
				//$("#mainLoader").hide();
			}
		});
	}else{
		setTimeout(function(){
			getVendorList( $select, glblCmpyId );
		}, 500)
	}
}

function getStxUserList( $select ){
	//$("#mainLoader").show();
	
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/user/read-erp',
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblERPUser.length;
				var usrId = '', usrNm = '', usrEmail = '', options = '<option value="">&nbsp;</option>';	
				for( var i=0; i<loopSize; i++ ){
					usrId = response.output.fldTblERPUser[i].usrId.trim();
					usrNm = response.output.fldTblERPUser[i].usrNm.trim();
					usrEmail = response.output.fldTblERPUser[i].usrEmail.trim();
					
					options += '<option value="'+ usrId +'" data-usr-nm="'+ usrNm +'" data-usr-email="'+ usrEmail +'">('+ usrId + ') ' + usrNm + '</option>';
				}
				$select.html( options );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
				//$("#mainLoader").hide();
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			//$("#mainLoader").hide();
		}
	});
}

function getUserGroupList( $select ){
	$.ajax({
		headers: ajaxHeader,
		url : rootAppName + '/rest/usrgroup/read',
		data : JSON.stringify({ grp: "" }),
		type: 'POST',
		dataType: 'json',
		success: function(response){
			var loopSize = response.output.fldTblUsrGroup.length;
			var grpId = '', grpNm = '', options = '<option value="">&nbsp;</option>';	
			for( var i=0; i<loopSize; i++ ){
				grpId = response.output.fldTblUsrGroup[i].grpId;
				grpNm = response.output.fldTblUsrGroup[i].grpNm;
				
				//if( grpNm.trim().toLowerCase() != "admin" ){
					options += '<option value="'+ grpId +'">'+ grpNm + '</option>';
				//}
			}
			$select.html( options );			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			//$("#mainLoader").hide();
		}
	});
}


function getCustomerList( $select ){
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/common/cus-list',
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblCustomer.length;
				var cusId = '', cusNm = '', options = '<option value="">&nbsp;</option>';	
				
				var dfltOption = "", selectOption = "";
				if( $select.attr("data-selected") != undefined && $select.attr("data-selected") != "" ){
					dfltOption = $select.attr("data-selected");
					
					$select.attr("data-selected", "");
				}
				
				for( var i=0; i<loopSize; i++ ){
					selectOption = "";
					
					cusId = response.output.fldTblCustomer[i].cusId.trim();
					cusNm = response.output.fldTblCustomer[i].cusNm.trim();
					
					if( dfltOption == cusId ){
						selectOption = "selected";
					}
					
					options += '<option value="'+ cusId +'" '+ selectOption +'>'+ cusId + ' - ' + cusNm + '</option>';
				}
				$select.html( options );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			//$("#mainLoader").hide();
		}
	});
}


function getBranchList( $select, allowBlank=true ){
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/common/brh-list',
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblBranch.length;
				var brhBrh = '', brhBrhNm = '', options = '';	
				
				if( allowBlank ){
					options = '<option value="">&nbsp;</option>';
				}
				
				var dfltOption = "", selectOption = "";
				if( $select.attr("data-selected") != undefined && $select.attr("data-selected") != "" ){
					dfltOption = $select.attr("data-selected");
					
					$select.attr("data-selected", "");
				}
				
				for( var i=0; i<loopSize; i++ ){
					selectOption = "";
					
					brhBrh = response.output.fldTblBranch[i].brhBrh.trim();
					brhBrhNm = response.output.fldTblBranch[i].brhBrhNm.trim();
					
					if( dfltOption == brhBrh ){
						selectOption = "selected";
					}
					
					options += '<option value="'+ brhBrh +'" '+ selectOption +'>'+ brhBrh + ' - ' + brhBrhNm + '</option>';
				}
				$select.html( options );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			//$("#mainLoader").hide();
		}
	});
}


function getStxBankList( $select ){
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/common/bnk-list',
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblBank.length;
				var bnk = '', bnkDesc = '', bnkCry = '', options = '<option value="">&nbsp;</option>', excRt = 0.0;	
				
				var dfltOption = "", selectOption = "";
				if( $select.attr("data-selected") != undefined && $select.attr("data-selected") != "" ){
					dfltOption = $select.attr("data-selected");
					
					$select.attr("data-selected", "");
				}
				
				for( var i=0; i<loopSize; i++ ){
					selectOption = "";
					
					bnk = response.output.fldTblBank[i].bnk.trim();
					bnkDesc = response.output.fldTblBank[i].bnkDesc.trim();
					bnkCry = response.output.fldTblBank[i].bnkCry.trim();
					excRt = response.output.fldTblBank[i].excRt;
					
					if( dfltOption == bnk ){
						selectOption = "selected";
					}
					
					options += '<option value="'+ bnk +'" data-cry="'+ bnkCry +'" data-exrt="'+ excRt +'" '+ selectOption +'>'+ bnk + ' - ' + bnkDesc + '</option>';
				}
				$select.html( options );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			//$("#mainLoader").hide();
		}
	});
}


function getCurrencyList( $select ){
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/common/cry-list',
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblCry.length;
				var cry = '', cryDesc = '', options = '<option value="">&nbsp;</option>';	
				
				var dfltOption = "", selectOption = "";
				if( $select.attr("data-selected") != undefined && $select.attr("data-selected") != "" ){
					dfltOption = $select.attr("data-selected");
					
					$select.attr("data-selected", "");
				}
				
				for( var i=0; i<loopSize; i++ ){
					selectOption = "";
					
					cry = response.output.fldTblCry[i].cry.trim();
					cryDesc = response.output.fldTblCry[i].cryDesc.trim();
					
					if( dfltOption == cry ){
						selectOption = "selected";
					}
					
					options += '<option value="'+ cry +'" '+ selectOption +'>'+ cry + ' - ' + cryDesc + '</option>';
				}
				$select.html( options );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			//$("#mainLoader").hide();
		}
	});
}


function getPaymentMethodList( $select ){
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/common/pymt-mthd',
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblPymtMthd.length;
				var mthdId = '', mthdCd = '', mthdNm = '', options = '';	
				
				var dfltOption = "", selectOption = "";
				if( $select.attr("data-selected") != undefined && $select.attr("data-selected") != "" ){
					dfltOption = $select.attr("data-selected");
					
					$select.attr("data-selected", "");
				}
				
				for( var i=0; i<loopSize; i++ ){
					selectOption = "";
					
					mthdId = response.output.fldTblPymtMthd[i].mthdId.trim();
					mthdCd = response.output.fldTblPymtMthd[i].mthdCd.trim();
					mthdNm = response.output.fldTblPymtMthd[i].mthdNm.trim();
					
					if( dfltOption == mthdCd ){
						selectOption = "selected";
					}
					options += '<option value="'+ mthdCd +'" data-mthd-id="'+ mthdId +'" '+ selectOption +'>'+ mthdNm + '</option>';
				}
				$select.html( options );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			//$("#mainLoader").hide();
		}
	});
}


function getGlList( $select ){
	//$("#mainLoader").show();
	
	/*var inputData = {
		cmpyId : cmpyId,
	};*/
	
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/gl/read',
		type: 'POST',
		//data : JSON.stringify( inputData ),
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblGL.length;
				var acctTyp = '', desc = '', glAcct = '', options = '<option value="">&nbsp;</option>';	
				
				var dfltOption = "", selectOption = "";
				if( $select.attr("data-selected") != undefined && $select.attr("data-selected") != "" ){
					dfltOption = $select.attr("data-selected");
					
					$select.attr("data-selected", "");
				}
				
				for( var i=0; i<loopSize; i++ ){
					selectOption = "";
					
					glAcct = response.output.fldTblGL[i].glAcct.trim();
					desc = response.output.fldTblGL[i].desc.trim();
					acctTyp = response.output.fldTblGL[i].acctTyp.trim();
					
					if( dfltOption == glAcct ){
						selectOption = "selected";
					}
					
					options += '<option value="'+ glAcct +'" data-acct-typ="'+ acctTyp +'" data-acct-desc="'+ desc +'">('+ glAcct + ') ' + desc + '</option>';
				}
				$select.html( options );
				
				//$("#mainLoader").hide();
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
				//$("#mainLoader").hide();
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}


function getGlSubAccList( glAcct, $select ){
	$("#mainLoader").show();
	
	var inputData = {
		glAcct : glAcct,
	};
	
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/gl/sub-acct-read',
		type: 'POST',
		data : JSON.stringify( inputData ),
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblSubAcct.length;
				var subAcct = '',subAcctDesc = '', options = '<option value="">&nbsp;</option>';	
				
				var dfltOption = "", selectOption = "";
				if( $select.attr("data-selected") != undefined && $select.attr("data-selected") != "" ){
					dfltOption = $select.attr("data-selected");
					
					$select.attr("data-selected", "");
				}
				
				for( var i=0; i<loopSize; i++ ){
					selectOption = "";
				
					subAcct = response.output.fldTblSubAcct[i].subAcct.trim();
					subAcctDesc = response.output.fldTblSubAcct[i].subAcctDesc.trim();
					
					if( dfltOption == subAcct ){
						selectOption = "selected";
					}
					
					options += '<option value="'+ subAcct +'" '+ selectOption +'>' + subAcct + '</option>';
				}
				$select.html( options );
				$select.removeAttr("disabled");
				//$select.select2();
				
				/*$(".select2-multiline").select2({
  					templateResult: formatState
				});
	
				function formatState (state) {
				  if (!state.id) {
				    return state.text;
				  }
				  var $state = $(
				    '<span>' + state.id + '<br><strong>' + state.text + '</strong></span>'
				  );
				  return $state;
				};*/
				
				$("#mainLoader").hide();
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
				$("#mainLoader").hide();
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}


function getVoucherCategoryList( $select ){
	//$("#mainLoader").show();
	
	var inputData = {};
	
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/common/vchr-cat',
		type: 'POST',
		data : JSON.stringify( inputData ),
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblVouCategory.length;
				var vchrCat = '', desc = '', options = '<option value="">&nbsp;</option>';	
				
				var dfltOption = "", selectOption = "";
				if( $select.attr("data-selected") != undefined && $select.attr("data-selected") != "" ){
					dfltOption = $select.attr("data-selected");
					
					$select.attr("data-selected", "");
				}
				
				for( var i=0; i<loopSize; i++ ){
					selectOption = "";
				
					vchrCat = response.output.fldTblVouCategory[i].vchrCat.trim();
					desc = response.output.fldTblVouCategory[i].desc.trim();
					
					if( dfltOption == vchrCat ){
						selectOption = "selected";
					}
					else if ( vchrCat == "MAT" )
					{
						//selectOption = "selected";
					}
					
					options += '<option value="'+ vchrCat +'" '+ selectOption +'>'+ vchrCat + ' - ' + desc + '</option>';
				}
				$select.html( options );
				
				//$("#mainLoader").hide();
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
				//$("#mainLoader").hide();
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			//$("#mainLoader").hide();
		}
	});
}


function getTrnStsActnList( $select ){
	$("#mainLoader").show();
	
	var inputData = {};
	
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/common/trn-sts-actn',
		type: 'POST',
		data : JSON.stringify( inputData ),
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblTrnStsActn.length;
				var sts = '', desc = '', options = '<option value="">&nbsp;</option>';	
				
				var dfltOption = "", selectOption = "";
				if( $select.attr("data-selected") != undefined && $select.attr("data-selected") != "" ){
					dfltOption = $select.attr("data-selected");
					
					$select.attr("data-selected", "");
				}
				
				for( var i=0; i<loopSize; i++ ){
					selectOption = "";
				
					sts = response.output.fldTblTrnStsActn[i].sts.trim();
					desc = response.output.fldTblTrnStsActn[i].desc.trim();
					
					if( dfltOption == sts ){
						selectOption = "selected";
					}
					
					options += '<option value="'+ sts +'" '+ selectOption +'>'+ sts + ' - ' + desc + '</option>';
				}
				$select.html( options );
				
				$("#mainLoader").hide();
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
				$("#mainLoader").hide();
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}


function getTrnStsRsnList( $select, trnSts ){
	$("#mainLoader").show();
	
	var inputData = {
		trnSts: trnSts,
	};
	
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/common/trn-sts-rsn',
		type: 'POST',
		data : JSON.stringify( inputData ),
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblTrnStsRsn.length;
				var rsn = '', desc = '', options = '<option value="">&nbsp;</option>';	
				
				var dfltOption = "", selectOption = "";
				if( $select.attr("data-selected") != undefined && $select.attr("data-selected") != "" ){
					dfltOption = $select.attr("data-selected");
					
					$select.attr("data-selected", "");
				}
				
				for( var i=0; i<loopSize; i++ ){
					selectOption = "";
				
					rsn = response.output.fldTblTrnStsRsn[i].rsn.trim();
					desc = response.output.fldTblTrnStsRsn[i].desc.trim();
					
					if( dfltOption == rsn ){
						selectOption = "selected";
					}
					
					options += '<option value="'+ rsn +'" '+ selectOption +'>'+ rsn + ' - ' + desc + '</option>';
				}
				$select.html( options );
				
				$("#mainLoader").hide();
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
				$("#mainLoader").hide();
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}


function getTrnStsList( $select ){
	$("#mainLoader").show();
	
	var inputData = {};
	
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/common/trn-sts',
		type: 'POST',
		data : JSON.stringify( inputData ),
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblTrnStsActn.length;
				var sts = '', desc = '', options = '<option value="">&nbsp;</option>';	
				
				var dfltOption = "", selectOption = "";
				if( $select.attr("data-selected") != undefined && $select.attr("data-selected") != "" ){
					dfltOption = $select.attr("data-selected");
					
					$select.attr("data-selected", "");
				}
				
				for( var i=0; i<loopSize; i++ ){
					selectOption = "";
				
					sts = response.output.fldTblTrnStsActn[i].sts.trim();
					desc = response.output.fldTblTrnStsActn[i].desc.trim();
					
					if( dfltOption == sts ){
						selectOption = "selected";
					}
					
					options += '<option value="'+ sts +'" '+ selectOption +'>'+ sts + ' - ' + desc + '</option>';
				}
				$select.html( options );
				
				$("#mainLoader").hide();
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
				$("#mainLoader").hide();
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}


function downloadDocument(ctlNo){
	var docUrl = rootAppName + "/rest/cstmdoc/download?ctlNo=" + ctlNo;

	window.open(docUrl, '_blank');
}

function downloadDocumentAddtn(ctlNo){
	var docUrl = rootAppName + "/rest/cstmdoc/downloadAddtn?ctlNo=" + ctlNo;

	window.open(docUrl, '_blank');
}

function downloadAchFile(flNm, forceDwnld=0){
	var docUrl = rootAppName + "/rest/payment/dwnld-ach?flNm=" + flNm + "&forceDwnld=" + forceDwnld;

	window.open(docUrl, '_blank');
}


/*
 *	This function is used to filter data in table
 */
function filterTableData(tblDivId) {
	
	var modalId = $("#closePreview").parents(".modal").attr("id");
	if( $("#"+modalId+" .prop-data-wrapper").hasClass("col-md-6") || $("#"+modalId+" #previewCol").css("display") == "block" ){
		$("#closePreview").trigger("click");
	}
	
	$("#"+tblDivId+" table tbody tr").hide();
	
	var $hdrEle = $("#"+tblDivId);
		
	if( $("#"+tblDivId).parent().hasClass("floatThead-wrapper") ){
		$hdrEle = $($("#"+tblDivId).parent());
	}
	
	var fldFltr = invcDtFltr = dueDtFltr = jrnlDtFltr = issueDtFltr = discDtFltr = stsFltr = vchrFltr = vndrFltr = transFltr = pymntFltr = bankRefFltr = cusRefFltr = addtnTxtFltr = erpRefFltr = brhFltr = chkNoFltr = disbNoFltr= false;
	var fields = vendor = voucher = invcDt = dueDt = discDt = status = trans = pymnt = brh = jrnlDt = issueDt = chkNo= disbNo="";
	var bankRef = cusRef = addtnTxtRef = erpRef = "";
	
	if( $hdrEle.find(".fieldsFilter").length  ){
		fields = $hdrEle.find(".fieldsFilter").val().trim();
	}
	
	if( $hdrEle.find(".vendorFilter").length  ){
		vendor = $hdrEle.find(".vendorFilter").val().trim();
	}
	
	if( $hdrEle.find(".voucherFilter").length  ){
		voucher = $hdrEle.find(".voucherFilter").val().trim();
	}
	
	if( $hdrEle.find(".invcDtFilter").length  ){
		invcDt = $hdrEle.find(".invcDtFilter").val().trim();
	}
	
	if( $hdrEle.find(".dueDtFilter").length  ){
		dueDt = $hdrEle.find(".dueDtFilter").val().trim();
	}
	
	if( $hdrEle.find(".issueDtFilter").length  ){
		issueDt = $hdrEle.find(".issueDtFilter").val().trim();
	}
	
	if( $hdrEle.find(".jrnlDtFilter").length  ){
		jrnlDt = $hdrEle.find(".jrnlDtFilter").val().trim();
	}
	
	if( $hdrEle.find(".transFilter").length  ){
		trans = $hdrEle.find(".transFilter").val().trim();
	}
	
	if( $hdrEle.find(".brhFilter").length  ){
		brh = $hdrEle.find(".brhFilter").val().trim();
	}
	
	if( $hdrEle.find(".chkNoFilter").length  ){
		chkNo = $hdrEle.find(".chkNoFilter").val().trim();
	}
	
	if( $hdrEle.find(".disbNoFilter").length  ){
		disbNo = $hdrEle.find(".disbNoFilter").val().trim();
	}
	
	if( $hdrEle.find(".pymntFilter").length  ){
		pymnt = $hdrEle.find(".pymntFilter").val().trim();
	}
	
	if( $hdrEle.find(".discDtFilter").length  ){
		discDt = $hdrEle.find(".discDtFilter").val().trim();
	}
	
	if( $hdrEle.find(".bankRefFilter").length  ){
		bankRef = $hdrEle.find(".bankRefFilter").val().trim();
	}
	
	if( $hdrEle.find(".cusRefFilter").length  ){
		cusRef = $hdrEle.find(".cusRefFilter").val().trim();
	}
	
	if( $hdrEle.find(".addtnTxtFilter").length  ){
		addtnTxtRef = $hdrEle.find(".addtnTxtFilter").val().trim();
	}
	
	if( $hdrEle.find(".erpRefFilter").length  ){
		erpRef = $hdrEle.find(".erpRefFilter").val().trim();
	}
	
	if( fields != "" ){
		fldFltr = true;
		var fldIndx = $hdrEle.find(".fieldsFilter").parent().parent().find("th").index( $hdrEle.find(".fieldsFilter").parent() );
	}
	
	if( vendor != "" ){
		vndrFltr = true;
		var vndrIndx = $hdrEle.find(".vendorFilter").parent().parent().find("th").index( $hdrEle.find(".vendorFilter").parent() );
	}
	
	if( bankRef != "" ){
		bankRefFltr = true;
		var bankRefIndx = $hdrEle.find(".bankRefFilter").parent().parent().find("th").index( $hdrEle.find(".bankRefFilter").parent() );
	}
	
	if( cusRef != "" ){
		cusRefFltr = true;
		var cusRefIndx = $hdrEle.find(".cusRefFilter").parent().parent().find("th").index( $hdrEle.find(".cusRefFilter").parent() );
	}
	
	if( addtnTxtRef != "" ){
		addtnTxtFltr = true;
		var addtnTxtRefIndx = $hdrEle.find(".addtnTxtFilter").parent().parent().find("th").index( $hdrEle.find(".addtnTxtFilter").parent() );
	}
	
	if( erpRef != "" ){
		erpRefFltr = true;
		var erpRefIndx = $hdrEle.find(".erpRefFilter").parent().parent().find("th").index( $hdrEle.find(".erpRefFilter").parent() );
	}
	
	if( voucher != "" ){
		vchrFltr = true;
		var vchrIndx = $hdrEle.find(".voucherFilter").parent().parent().find("th").index( $hdrEle.find(".voucherFilter").parent() );
	}
	
	if( invcDt != "" ){
		invcDtFltr = true;
		var invcStrtDate = invcDt.split(" - ")[0];
		var invcEndDate = invcDt.split(" - ")[1];
		invcStrtDate = invcStrtDate.split('/').join('-');
		invcEndDate = invcEndDate.split('/').join('-');
		
		var invcDtIndx = $hdrEle.find(".invcDtFilter").parent().parent().find("th").index( $hdrEle.find(".invcDtFilter").parent() );
	}
	
	if( dueDt != "" ){
		dueDtFltr = true;
		var dueStrtDate = dueDt.split(" - ")[0];
		var dueEndDate = dueDt.split(" - ")[1];
		dueStrtDate = dueStrtDate.split('/').join('-');
		dueEndDate = dueEndDate.split('/').join('-');
		
		var dueDtIndx = $hdrEle.find(".dueDtFilter").parent().parent().find("th").index( $hdrEle.find(".dueDtFilter").parent() );
	}
	
	if( jrnlDt != "" ){
		jrnlDtFltr = true;
		var jrnlStrtDate = jrnlDt.split(" - ")[0];
		var jrnlEndDate = jrnlDt.split(" - ")[1];
		jrnlStrtDate = jrnlStrtDate.split('/').join('-');
		jrnlEndDate = jrnlEndDate.split('/').join('-');
		
		var jrnlDtIndx = $hdrEle.find(".jrnlDtFilter").parent().parent().find("th").index( $hdrEle.find(".jrnlDtFilter").parent() );
	}
	
	if( issueDt != "" ){
		issueDtFltr = true;
		var issueStrtDate = issueDt.split(" - ")[0];
		var issueEndDate = issueDt.split(" - ")[1];
		issueStrtDate = issueStrtDate.split('/').join('-');
		issueEndDate = issueEndDate.split('/').join('-');
		
		var issueDtIndx = $hdrEle.find(".issueDtFilter").parent().parent().find("th").index( $hdrEle.find(".issueDtFilter").parent() );
	}
	
	if( status != "" ){
		stsFltr = true;
		
		var stsIndx = $hdrEle.find(".statusFilter").parent().parent().find("th").index( $hdrEle.find(".statusFilter").parent() );
	} 
	
	if( trans != "" ){
		transFltr = true;
		
		var transIndx = $hdrEle.find(".transFilter").parent().parent().find("th").index( $hdrEle.find(".transFilter").parent() );
	} 
	
	if( brh != "" ){
		brhFltr = true;
		
		var brhIndx = $hdrEle.find(".brhFilter").parent().parent().find("th").index( $hdrEle.find(".brhFilter").parent() );
	} 
	
	if( chkNo != "" ){
		chkNoFltr = true;
		
		var chkNoIndx = $hdrEle.find(".chkNoFilter").parent().parent().find("th").index( $hdrEle.find(".chkNoFilter").parent() );
	} 
	
	if( disbNo != "" ){
		disbNoFltr = true;
		
		var disbNoIndx = $hdrEle.find(".disbNoFilter").parent().parent().find("th").index( $hdrEle.find(".disbNoFilter").parent() );
	} 
	
	if( pymnt != "" ){
		pymntFltr = true;
		
		var pymntIndx = $hdrEle.find(".pymntFilter").parent().parent().find("th").index( $hdrEle.find(".pymntFilter").parent() );
	}
	
	if( discDt != "" ){
		discDtFltr = true;
		var discStrtDate = discDt.split(" - ")[0];
		var discEndDate = discDt.split(" - ")[1];
		discStrtDate = discStrtDate.split('/').join('-');
		discEndDate = discEndDate.split('/').join('-');
		
		var discDtIndx = $hdrEle.find(".discDtFilter").parent().parent().find("th").index( $hdrEle.find(".discDtFilter").parent() );
	}
	
	$("#"+tblDivId+" table tbody tr").each(function(index, val){
		var fldShw = invcDtShw = dueDtShw = jrnlDtShw = issueDtShw = stsShw = vchrShw = vndrShw = transShw = pymntShw = discDtShw = bnkFltrShw = cusRefFltrShw = addtnTxtFltrShw = erpRefFltrShw = brhFltrShw = chkNoFltrShw = disbNoFltrShw = true;
		
		if( $(val).children().length == 1 ){
			return; //continue;
		}
		
		//Fields Filtering
		if( fldFltr ){
			var allStr = $(val).children()[fldIndx].textContent;
			if(allStr.toLowerCase().indexOf(fields.toLowerCase()) < 0){
				fldShw = false;
			}
		}
		
		if( vndrFltr ){
			var allStr = $(val).children()[vndrIndx].textContent;
			if(allStr.toLowerCase().indexOf(vendor.toLowerCase()) < 0){
				vndrShw = false;
			}
		}
		
		if( bankRefFltr ){
			var allStr = $(val).children()[bankRefIndx].textContent;
			if(allStr.toLowerCase().indexOf(bankRef.toLowerCase()) < 0){
				bnkFltrShw = false;
			}
		}
		
		if( cusRefFltr ){
			var allStr = $(val).children()[cusRefIndx].textContent;
			if(allStr.toLowerCase().indexOf(cusRef.toLowerCase()) < 0){
				cusRefFltrShw = false;
			}
		}
		
		if( addtnTxtFltr ){
			var allStr = $(val).children()[addtnTxtRefIndx].textContent;
			if(allStr.toLowerCase().indexOf(addtnTxtRef.toLowerCase()) < 0){
				addtnTxtFltrShw = false;
			}
		}
		
		if( erpRefFltr ){
			var allStr = $(val).children()[erpRefIndx].textContent;
			if(allStr.toLowerCase().indexOf(erpRef.toLowerCase()) < 0){
				erpRefFltrShw = false;
			}
		}
		
		//Voucher Filtering
		if( vchrFltr ){
			var allStr = $(val).children()[vchrIndx].textContent;
			if(allStr.toLowerCase().indexOf(voucher.toLowerCase()) < 0){
				vchrShw = false;
			}
		}
		
		//Invoice Date Filtering
		if( invcDtFltr ){
			var allStr = $(val).children()[invcDtIndx].textContent;
			allStr = allStr.split(" ")[0];
			if( allStr != "" ){
				if( (new Date(allStr) < new Date(invcStrtDate)) || (new Date(allStr) > new Date(invcEndDate)) ){
					invcDtShw = false;
				}
			}else{
				invcDtShw = false;
			}
		}
		
		//Due Date Filtering
		if( dueDtFltr ){
			var allStr = $(val).children()[dueDtIndx].textContent;
			allStr = allStr.split(" ")[0];
			if( allStr != "" ){
				if( (new Date(allStr) < new Date(dueStrtDate)) || (new Date(allStr) > new Date(dueEndDate)) ){
					dueDtShw = false;
				}
			}else{
				dueDtShw = false;
			}
		}
		
		//Journal Date Filtering
		if( jrnlDtFltr ){
			var allStr = $(val).children()[jrnlDtIndx].textContent;
			allStr = allStr.split(" ")[0];
			if( allStr != "" ){
				if( (new Date(allStr) < new Date(jrnlStrtDate)) || (new Date(allStr) > new Date(jrnlEndDate)) ){
					jrnlDtShw = false;
				}
			}else{
				jrnlDtShw = false;
			}
		}
		
		//Issue Date Filtering
		if( issueDtFltr ){
			var allStr = $(val).children()[issueDtIndx].textContent;
			allStr = allStr.split(" ")[0];
			if( allStr != "" ){
				if( (new Date(allStr) < new Date(issueStrtDate)) || (new Date(allStr) > new Date(issueEndDate)) ){
					issueDtShw = false;
				}
			}else{
				issueDtShw = false;
			}
		}
		
		//Discount Date Filtering
		if( discDtFltr ){
			var allStr = $(val).children()[discDtIndx].textContent;
			allStr = allStr.split(" ")[0];
			if( allStr != "" ){
				if( (new Date(allStr) < new Date(discStrtDate)) || (new Date(allStr) > new Date(discEndDate)) ){
					discDtShw = false;
				}
			}else{
				discDtShw = false;
			}
		}
		
		//Status Filtering
		/* if( stsFltr ){
			var allStr = $(val).children()[stsIndx].textContent;
			if(allStr.toLowerCase().indexOf(status.toLowerCase()) < 0){
				stsShw = false;
			}
		} */
		
		//Transaction Status Filtering
		if( transFltr ){
			var allStr = $(val).children()[transIndx].textContent;
			if(allStr.toLowerCase().indexOf(trans.toLowerCase()) < 0){
				transShw = false;
			}
		}
		
		if( brhFltr ){
			var allStr = $(val).children()[brhIndx].textContent;
			if(allStr.toLowerCase().indexOf(brh.toLowerCase()) < 0){
				brhFltrShw = false;
			}
		}
		
		if( chkNoFltr ){
			var allStr = $(val).children()[chkNoIndx].textContent;
			if(allStr.toLowerCase().indexOf(chkNo.toLowerCase()) < 0){
				chkNoFltrShw = false;
			}
		}
		
		if( disbNoFltr ){
			var allStr = $(val).children()[disbNoIndx].textContent;
			if(allStr.toLowerCase().indexOf(disbNo.toLowerCase()) < 0){
				disbNoFltrShw = false;
			}
		}
		
		//Payment Status Filtering
		if( pymntFltr ){
			var allStr = $(val).children()[pymntIndx].textContent;
			if(allStr.toLowerCase().indexOf(pymnt.toLowerCase()) < 0){
				pymntShw = false;
			}
		}
		
		if( fldShw && invcDtShw && dueDtShw && jrnlDtShw && issueDtShw && vchrShw && vndrShw && transShw && pymntShw && discDtShw && bnkFltrShw && cusRefFltrShw && addtnTxtFltrShw && erpRefFltrShw && brhFltrShw && chkNoFltrShw && disbNoFltrShw){
			$(val).show();
		}
		
	});
	
	$("#glDebitVchrListTbody tr:not(.vndr-total)").hide();
}


function fixedTableHeader($table){
	$table.floatThead('destroy');
	$table.floatThead({
		useAbsolutePositioning: true,
		scrollContainer: function($table) {
			return $table.closest('.table-responsive');
		}
	});
	$table.floatThead('reflow');
}




function getEbsList( $select, allowBlank=true ){
	//$("#mainLoader").show();
	
	/*var inputData = {
		cmpyId : cmpyId,
	};*/
	
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/ebsTypeCode/readEbs',
		type: 'POST',
		//data : JSON.stringify( inputData ),
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblEbs.length;
				var ebsId = '', ebsNm = '', options = '', selected = '';
				
				if( allowBlank ){
					options = '<option value="">&nbsp;</option>';
				}
				
				var dfltOption = "", selectOption = "";
				if( $select.attr("data-selected") != undefined && $select.attr("data-selected") != "" ){
					dfltOption = $select.attr("data-selected");
					
					$select.attr("data-selected", "");
				}
					
				for( var i=0; i<loopSize; i++ ){
					selectOption = "";
					
					ebsId = response.output.fldTblEbs[i].ebsId.trim();
					ebsNm = response.output.fldTblEbs[i].ebsNm.trim();
					
					if( dfltOption == ebsId ){
						selectOption = "selected";
					}
					
					options += '<option value="'+ ebsId +'" '+ selectOption +'>' + ebsNm + '</option>';
				}
				$select.html( options );
				
				//$("#mainLoader").hide();
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
				//$("#mainLoader").hide();
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			//$("#mainLoader").hide();
		}
	});
	
}





function getBankList($select){
	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/bank/list',
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblBank.length;
				var bnkCode = '', bnkNm = '', options = '<option value="">&nbsp;</option>';	
				for( var i=0; i<loopSize; i++ ){
					bnkCode = response.output.fldTblBank[i].bnkCode.trim();
					bnkNm = response.output.fldTblBank[i].bnkNm.trim();
					
					options += '<option value="'+ bnkCode +'">'+ bnkCode + ' - ' + bnkNm + '</option>';
				}
				$select.html( options );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			//$("#mainLoader").hide();
		}
	});
}




function getBankAccountDetails($select, bnkCd){
	$("#mainLoader").show();
	
	var inputData = { 
		bnkCode : bnkCd,
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/bank/read',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var acctList = '<option value="">&nbsp;</option>';
				if( response.output.fldTblBank.length ){					
					var loopSize = response.output.fldTblBank[0].bankAcctList.length;
					for( var i=0; i<loopSize; i++ ){
						acctList += '<option value="'+ response.output.fldTblBank[0].bankAcctList[i].bnkAcctNo +'">'+ response.output.fldTblBank[0].bankAcctList[i].bnkAcctNo +' ('+ response.output.fldTblBank[0].bankAcctList[i].bnkAcctDesc +')</option>';
					}
				}
				
				$select.html(acctList);
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}


function sendNotificationMail(trnTyp, reqId){
	var inputData = { 
		trnTyp: trnTyp,
		reqId : reqId,
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/sendmail/prop',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				customAlert("#popupNotify", "alert-success", "Notification mail sent successfully.");
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#popupNotify", "alert-danger", errList);
			}
			
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}


function getSidebarMenus(){
	var inputData = {
		grpId: localStorage.getItem("starOcrUsrGrp"),
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/menu/read',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){		            	
				var loopLimit = response.output.fldTblMenu.length;
				var innerLoopLimit = 0;
				var sidebarMenu = '';
				var redirectUrl = "";
				
				for( var i=0; i<loopLimit; i++){
					innerLoopLimit = response.output.fldTblMenu[i].subMenuList.length;
					if( innerLoopLimit ){
						if( i == 0 ){
							redirectUrl = response.output.fldTblMenu[i].subMenuList[0].menuHtml;
						}
					
						if( response.output.fldTblMenu[i].menuNm == "Settings" ){
							sidebarMenu += '<li class="nav-item d-none d-lg-block">'+
												'<div class="spacer">'+
													'<div class="progress progress-sm">'+
														'<div class="progress-bar bg-primary" role="progressbar" style="width: 100%" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100"></div>'+
													'</div>'+
												'</div>'+
											'</li>';
						}
						var d = new Date();
						var uniqueId = "subMenu" + d.valueOf() + "" + i;
						sidebarMenu += '<li class="nav-item has-child">'+
											'<a class="nav-link has-child" href="#'+ uniqueId +'" data-toggle="collapse" aria-expanded="false">'+
												'<i class="menu-icon '+ response.output.fldTblMenu[i].menuIcon +'"></i>'+
												'<span class="menu-title">'+ response.output.fldTblMenu[i].menuNm +'</span>'+
											'</a>'+
											'<div class="collapse" id="'+ uniqueId +'">'+
												'<ul class="nav flex-column sub-menu">';
											
						for( var j=0; j<innerLoopLimit; j++){
							sidebarMenu += 			'<li class="nav-item">'+
														'<a href="'+ response.output.fldTblMenu[i].subMenuList[j].menuHtml +'" class="nav-link">'+ 
															'<i class="menu-icon '+ response.output.fldTblMenu[i].subMenuList[j].menuIcon +'"></i>'+
															'<span class="menu-page-title">'+ response.output.fldTblMenu[i].subMenuList[j].menuNm +'</span>'+
														'</a>'+
													'</li>';
						}
						sidebarMenu += 			'</ul>'+
											'</div>'+
										'</li>';
					}else{
						if( i == 0 ){
							redirectUrl = response.output.fldTblMenu[i].menuHtml;
						}
						sidebarMenu += '<li class="nav-item">'+
											'<a href="'+ response.output.fldTblMenu[i].menuHtml +'" class="nav-link">'+
												'<i class="menu-icon '+ response.output.fldTblMenu[i].menuIcon +'"></i>'+
												'<span class="menu-title menu-page-title">'+ response.output.fldTblMenu[i].menuNm +'</span>'+
											'</a>'+
										'</li>';
					}
				}
				
				//$("#main-menu-navigation").html(sidebarMenu);
				//activeSidebarMenu();
				
				localStorage.setItem("starOcrSidebarMenu", sidebarMenu);
				
				redirectUrl = (redirectUrl=="") ? "profile.html" : redirectUrl;
				
				window.location.replace(redirectUrl);
	
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}


function getExchangeRate(origCry, eqvCry, $element){
	$("#mainLoader").show();
	
	var inputData = { 
		origCry : origCry,
		eqvCry: eqvCry,
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData ),
		url: rootAppName + '/rest/common/ex-rt',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				$element.val( response.output.excRt );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#popupNotify", "alert-danger", errList);
			}
			
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			$("#mainLoader").hide();
		}
	});
}


function stxAuthFailed(errMsg){
	localStorage.removeItem("starOcrAuthToken");
    localStorage.removeItem("starOcrUserId");
    localStorage.removeItem("starOcrUserNm");
    localStorage.removeItem("starOcrEmailId");
    localStorage.removeItem("starOcrUsrGrp");
    localStorage.removeItem("starOcrAppHost");
    localStorage.removeItem("starOcrAppPort");
    localStorage.removeItem("starOcrAppToken");
    localStorage.removeItem("starOcrCmpyId");
    
    localStorage.removeItem("starOcrSidebarMenu");
    
    $.removeCookie('auth-token');
    $.removeCookie('app-token');
    $.removeCookie('user-id');
    
	$("#authFailed .server-err-msg").html(errMsg);
	$("#authFailed").modal("show");
		    
	var counter = 4;
	setInterval(function(){
		$("#redirectTime").html(counter);
		
		counter--;
	}, 1000);
	
	setTimeout(function(){
		window.location.replace("login.html");
	}, 5000);
}




function getApproverList( $select, typ=''){

	var inputData = {
			typ: typ,
	};

	$.ajax({
		headers: ajaxHeader,
		url: rootAppName + '/rest/user/read-aprv',
		data: JSON.stringify( inputData ),
		type: 'POST',
		dataType: 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblERPUser.length;
				var usrId = '', usrNm = '', usrEmail = '', options = '<option value="">&nbsp;</option>';	
				for( var i=0; i<loopSize; i++ ){
					usrId = response.output.fldTblERPUser[i].usrId.trim();
					usrNm = response.output.fldTblERPUser[i].usrNm.trim();
					usrEmail = response.output.fldTblERPUser[i].usrEmail.trim();
					
					options += '<option value="'+ usrId +'" data-usr-nm="'+ usrNm +'">' + usrNm + '</option>';
				}
				$select.html( options );
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}			
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			//$("#mainLoader").hide();
		}
	});
}


function validateInvoiceNumber(invNo){
	$("#mainLoader").show();
	
	var inputData = {
		cusId: "",
		brh: "",
		dueDt: "",
		invNo: invNo,
		refPfx: "",
		refNo: "",
	};
	
	$.ajax({
		headers: ajaxHeader,
		data: JSON.stringify( inputData),
		url: rootAppName + '/rest/ar/read-arEnqry',
		type: 'POST',
		dataType : 'json',
		success: function(response){
			if( response.output.rtnSts == 0 ){
				var loopSize = response.output.fldTblAREnqry.length;
				if(loopSize){
					$("#crCusId").val( response.output.fldTblAREnqry[0].cusId ).trigger("change");
					$("#crCusId").attr("disabled", "disabled"); 
				}else{
					$("#crCusId").val("").trigger("change");
					$("#crCusId").removeAttr("disabled"); 
				}
			}else{
				var loopLimit = response.output.messages.length;
				var errList = '<ul class="m-0 pl-1">';
				for( var i=0; i<loopLimit; i++ ){
					errList += '<li>'+ response.output.messages[i] +'</li>';
				}
				errList += '</ul>';
				customAlert("#mainNotify", "alert-danger", errList);
			}
			$("#mainLoader").hide();
		},
		error: function( jqXHR, textStatus, errorThrown ){
			if( jqXHR.status == 401 ){
				validateAuthCode();
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);
			$("#mainLoader").hide();
		}
	});
}

function fixedHeader($table){
	$table.floatThead('destroy');
	
	$table.floatThead({
		useAbsolutePositioning: true,
		scrollContainer: function($table) {
			return $table.closest('.table-responsive');
		}
	});
	$table.floatThead('reflow');
}

function formatAmount(n, currency='') {
  return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,') + currency ;
}