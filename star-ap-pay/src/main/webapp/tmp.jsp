<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
        pageEncoding="ISO-8859-1"%>
<%@ page import="java.io.BufferedReader"%>
<%@ page import="java.io.InputStreamReader"%>
<%@ page import="java.io.InputStream"%>
<%@ page import="java.io.OutputStream"%>
<%@ page import="java.io.PrintWriter"%>
<%@ page import="java.io.IOException"%>
<%@ page import="java.io.FileOutputStream"%>
<%@ page import="java.io.FileOutputStream"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="java.io.*,java.util.*,javax.servlet.*"%>

<!DOCTYPE html>
<html>
	<head>
		<title>Metrics Dashboard </title>
		<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<!-- Font Awesome -->
		<link rel="stylesheet" href="css/font-awesome.min.css">
		<link rel="stylesheet" href="css/select2.min.css">
		<link rel="stylesheet" href="css/bootstrap-datepicker.min.css">
		<link rel="stylesheet" href="css/metricsDashboard.css">
		
		<script src="js/jquery-2.2.3.min.js"></script>
		<script src="js/bootstrap.min.js"></script>
		<script src="js/select2.full.min.js"></script>
		<script src="js/bootstrap-datepicker.min.js"></script>
		<script src="js/metricsDashboard.js"></script>
		<link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet">
		<script type="text/javascript"  src="js/tableHeadFixer.js"></script>
		
		<style>
		
			.table-head{
				text-align: center;
				vertical-align: middle !important;
				background:#e4e4e4;
			}
			body{
				font-family: 'Source Sans Pro', sans-serif;
				font-size: 14px;
			}
			.td-heading{
				font-weight:bold;
				background-color:#ededed;
			}
			.box-footer {
				border-top-left-radius: 0;
				border-top-right-radius: 0;
				border-bottom-right-radius: 3px;
				border-bottom-left-radius: 3px;
				border-top: 1px solid #f4f4f4;
				padding: 10px;
				background-color: #fff;
			}
			.d-block{
				display: block;
			}
			
			.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th {
				padding : 5px !important;
			}
			
			.transport-totals
			{
				font-size:20px;
				color: blue;
			}
			
			.box, .search-results {
				margin-bottom: 10px;
			}
			
			.heading-page
			{
				font-size: 30px;
				vertical-align: middle;
				line-height: 0px;
				color: white;
				font-weight: bold;
			}
			
			.header{
				text-align: center;
				position: relative;
				min-height: 65px;
			}
			
			.header img.logo{
				position: absolute;
				top: 18px;
				left: 15px;
			}
			.table-head{
				border: 5px solid red;
			}
			.datepicker {
				z-index: 1001 !important;
			}
			
			.form-group {
    margin-bottom: 4px;
}

#main-table
{
	border-collapse: separate;
}

#comments-tab2
{
	overflow:auto; min-height: calc(100vh - 375px);
}
<%
String stratixWebHome = "";
String server = "";
String pgmPath = "";
String cmdline = "uname -s";
String line;
Process prs;


server = System.getenv("SERVER");
stratixWebHome = System.getenv("STXWEB_HOME");

	prs = Runtime.getRuntime().exec(cmdline);
	BufferedReader input = new BufferedReader (new InputStreamReader(prs.getInputStream()));
	line = input.readLine();
	input.close();

	if (line != null)
	{
		if (line.equalsIgnoreCase("Linux"))
		{
			pgmPath = stratixWebHome + "/tomcat/webapps/customer/metricsDashboard/chg_inv_mtrcs_dshbrd_pg_linux";
		}
		else if (line.equalsIgnoreCase("AIX"))
		{
			if(System.getenv("SERVER").equals("POSTGRES"))
			{
					pgmPath = stratixWebHome + "/tomcat/webapps/customer/metricsDashboard/chg_inv_mtrcs_dshbrd_pg";
			}
			else if(System.getenv("SERVER").equals("INFORMIX"))
			{
					pgmPath = stratixWebHome + "/server/stratix-web/deploy/30customer.war/metricsDashboard/chg_inv_mtrcs_dshbrd";
			}
		}
	}


if(server.equals("INFORMIX"))
{
	pgmPath = stratixWebHome + "/server/stratix-web/deploy/30customer.war/metricsDashboard/chg_inv_mtrcs_dshbrd";
}
else if(server.equals("POSTGRES"))
{
	pgmPath = stratixWebHome + "/tomcat/webapps/customer/metricsDashboard/chg_inv_mtrcs_dshbrd_pg";
}	



System.err.println("ENV variable" + pgmPath);

%>
		</style>
	</head>
	<body onload="getUsrList('<%=pgmPath%>');">
		<div class="loader-container" id='loader-gif'>
			<div class="loader"></div>
		</div>
		
		<div class="container-fluid">	
			<div class="header">
				<div class="">
		  <div>
		  <img class="logo" src="images/stratix-logo.png"></img> <span class='heading-page'>Metrics Dashboard</span>
		  </div>
      
          </div>
				
				<div class="clearfix"></div>
			</div>
			
			<div class="content-wrapper">
				<div class="content">
			
					<div class="row">
						<div class="col-md-12">
							<div class="search-results">
								<div class="box box-primary">
									<div class="box-header with-border">
										<h3 class="box-title" id="list-title">Search</h3>
									 </div>
									 
									<div class="box-body">
										<div class="row">
										
											<div class="col-md-4">
												<div class="form-group">
													<label>Users</label>
													<select class="form-control select2 select2-hidden-accessible" multiple="" data-placeholder="Select User" style="width: 100%;" tabindex="-1" aria-hidden="true" id='usr-list'>
														
													</select>
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label>From Date</label>
													<div class="input-group date">
														<div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														</div>
														<input type="text" class="form-control pull-right" id="fromDate" onkeydown="return false">
													</div>
												</div>
											</div>
											<div class="col-md-3">
												<div class="form-group">
													<label>To Date</label>
													<div class="input-group date">
														<div class="input-group-addon">
															<i class="fa fa-calendar"></i>
														</div>
														<input type="text" class="form-control pull-right" id="toDate" onkeydown="return false">
													</div>
												</div>
											</div>
											<div class="col-md-2">
												<div class="form-group">
													<label class="d-block">&nbsp;</label>
													<button type="submit" class="btn btn-primary" onclick="getComments('<%=pgmPath%>')">Search</button>
												</div>
											</div>
										</div>
									</div>
									
								</div>
								
								<div class="box box-primary" id ='metricsDtls'>
									
									<div class="box-body">
									
									
									<div class="row" id='transportDtls'>
										<div class="col-md-2" id='transport1'>
										
										</div>
										<div class="col-md-5" id='transport2'>
										</div>
										<div class="col-md-5" id='transport3'>
										</div>
									</div>
									<div class="row" id='rtnMsg' style='margin-top: 4px;'>
									<div class="col-md-2" id='rtnMsgVal'>
										
										</div>
									</div>
									</div>
									<div class="box-body">
										<div id="comments-tab2" >
											<table class="table table-bordered table-hover table-striped" id='main-table'>

											</table>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="footer text-right">
				<img src="images/star_software.gif" />
			</div>
		</div>
		
	
		<script>
		
		
		
		
			$(document).ready(function(){
				
				
				
				var date = new Date();
				var dd = date.getDate();
				var mm = date.getMonth()+1; 
				var yyyy = date.getFullYear();
				
				if(dd<10) {
					dd = '0' + dd;
				} 

				if(mm<10){
					mm = '0' + mm;
				} 
				
				var today = mm + '/' + dd + '/' + yyyy;
				
				var fDate = new Date();
				fDate.setDate(fDate.getDate() + 4);
				var fDD = fDate.getDate();
				var fMM = fDate.getMonth()+1;
				var fYY = fDate.getFullYear();
				
				if(fDD<10) {
					fDD = '0' + fDD;
				} 

				if(fMM<10){
					fMM = '0' + fMM;
				} 
				
				var endDate = fMM + '/' + fDD + '/' + fYY;
				
				//console.log("start Date : "+ today);
				//console.log("end Date : "+ endDate);
				
				$('#fromDate, #toDate').val(today);
				
				
				//Initialize Select2 Elements
				$('.select2').select2();
	
				//Date picker
				$('#fromDate').datepicker({
					autoclose: true,
					format: "mm/dd/yyyy",
					todayHighlight: true,
					setDate: today,
				});
				
				$('#toDate').datepicker({
					autoclose: true,
					format: "mm/dd/yyyy",
					todayHighlight: true,
					setDate: today,
					startDate: today,
					endDate: endDate,
				});
				
				$( "#fromDate" ).change(function() {
					var fromDate = stringToDate($( "#fromDate" ).val(), "/");
					console.log("fromDate : "+ fromDate);
					var dd = fromDate.getDate();
					var mm = fromDate.getMonth()+1; 
					var yyyy = fromDate.getFullYear();
					
					if(dd<10) {
						dd = '0' + dd;
					} 

					if(mm<10){
						mm = '0' + mm;
					} 
					
					var startDate = mm + '/' + dd + '/' + yyyy;
					
					var fDate = new Date(yyyy + '/' + mm + '/' + dd );
					fDate.setDate(fromDate.getDate() + 10);
					var fDD = fDate.getDate();
					var fMM = fDate.getMonth()+1;
					var fYY = fDate.getFullYear();
					
					if(fDD<10) {
						fDD = '0' + fDD;
					} 

					if(fMM<10){
						fMM = '0' + fMM;
					} 
					
					var endDate = fMM + '/' + fDD + '/' + fYY;
					
					console.log("start Date : "+ startDate);
					console.log("end Date : "+ endDate);
					
					$("#toDate").datepicker('destroy');
					$('#toDate').val(endDate);
					$('#toDate').datepicker({
						autoclose: true,
						format: "mm/dd/yyyy",
						todayHighlight: true,
						setDate: startDate,
						startDate: startDate,
						endDate: endDate,
					});
				});
				
				
				
			}); 
			
			function stringToDate(string, separator){
				var strDate = string.split(separator);
				return new Date(strDate[2], strDate[0]-1, strDate[1] );
			}
		</script>
	</body>
</html>