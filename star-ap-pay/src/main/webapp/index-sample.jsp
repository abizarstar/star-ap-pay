<html>
<head>

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

<!-- jQuery library -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<!-- Latest compiled JavaScript -->
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<script src="assets/js/jquery.cookie.js"></script>
<script src="assets/js/jquery.fileDownload.js"></script>

<script>
	$(function() {
		$.cookie('auth-token', localStorage.getItem("docMgmtAuthToken"));
		$.cookie('user-id', localStorage.getItem("docMgmtUserId"));
	});
	$(document).on("click", "a.fileDownloadPromise", function() {

		$.cookie('auth-token', localStorage.getItem("docMgmtAuthToken"));
		$.cookie('user-id', localStorage.getItem("docMgmtUserId"));

		$.fileDownload($(this).prop('href')).done(function() {
			alert('File download a success!');
		}).fail(function() {
			alert(' Server Error, unable to downlaod the file');
		});

		//$.cookie("auth-token", null);
		//$.cookie("user-id", null);

		return false; //this is critical to stop the click event which will trigger a normal file download
	});
</script>

<script>
	function getData() {
		
		var usrNm = $("#usrName").val();
		
		$
				.ajax({
					url : rootAppName + '/rest/hello/' + usrNm,
					type : 'GET',
					success : function(data) {
						
						$("#output").html(data);
					},
					error : function(data) {
						console.log(data);
						$("#mainLoader").hide();
					}
				});
	}
	
	</script>
</head>
<body>
	<input type='text' class='form-control' id='usrName'/>
	<br>
	<button class="btn btn-dark" onclick='getData()'>Submit</button>
	<br>
	<span id='output'></span>
	
	
	</body>
</html>